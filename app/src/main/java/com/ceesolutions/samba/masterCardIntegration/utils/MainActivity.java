package com.ceesolutions.samba.masterCardIntegration.utils;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.masterCardIntegration.DecodeImageThread;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.ViewfinderView;
import com.mastercard.mpqr.pushpayment.exception.ConflictiveTagException;
import com.mastercard.mpqr.pushpayment.exception.FormatException;
import com.mastercard.mpqr.pushpayment.exception.InvalidTagValueException;
import com.mastercard.mpqr.pushpayment.exception.MissingTagException;
import com.mastercard.mpqr.pushpayment.exception.UnknownTagException;
import com.mastercard.mpqr.pushpayment.model.PushPaymentData;
import com.mastercard.mpqr.pushpayment.parser.Parser;
import com.nightonke.boommenu.BoomMenuButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public class MainActivity extends AppCompatActivity implements
        DecoratedBarcodeView.TorchListener, BarcodeCallback {

    public static final int REQUEST_CODE_IMAGE = 0x0000c0dd; // Only use bottom 16 bits
    public static final int MSG_DECODE_SUCCEED = 1;
    public static final int MSG_DECODE_FAIL = 2;
    public static double latitude;
    public static double longitude;
    public ProgressBar progress;
    public String longitude1;
    public String latitude1;
    ImageButton buttonFromImage;
    ImageButton buttonFromCamera;
    long cameraScanStartTime = 0;
    private Dialog dialog, dialog1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Utils utils;
    private ImageView backBtn, notificationBtn, sambaBtn, shareBtn;
    private Constants constants;
    private EditText editFeedBack, mEditTextMessage;
    private String feedBack;
    private TextView submit, errorMessageForFeedBack, progressTxt, textView, heading, btn, message, doneBtn;
    private String menu;
    private BoomMenuButton bmb;
    private AppPreferences appPreferences;
    private String location;
    private Executor mQrCodeExecutor;
    private Handler mHandler;
    private Dialog pd;
    private CaptureManager capture;
    private DecoratedBarcodeView barcodeScannerView;
    private ViewfinderView viewfinderView;
    private DecodeImageThread.DecodeImageCallback mDecodeImageCallback;
    private Handler handler;
    private JSONArray jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pp_capture_customized);
        getSupportActionBar().hide();

        try {

            initViews();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void initViews() {


        utils = new Utils(MainActivity.this);
        requestQueue = Volley.newRequestQueue(MainActivity.this);
        helper = Helper.getHelper(MainActivity.this);
        constants = Constants.getConstants(MainActivity.this);
        utils = new Utils(MainActivity.this);
        dialog = new Dialog(this);
        backBtn = findViewById(R.id.backBtn);
        notificationBtn = findViewById(R.id.notificationBtn);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        appPreferences = new AppPreferences(MainActivity.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        handler = new Handler();
        barcodeScannerView = findViewById(R.id.zxing_barcode_scanner);
        barcodeScannerView.setTorchListener(this);

        FirebaseAnalytics.getInstance(MainActivity.this).logEvent("Qr_Payment_scan_to_pay", new Bundle());

//        switchFlashlightButton = (Button) findViewById(R.id.switch_flashlight);

        viewfinderView = findViewById(R.id.zxing_viewfinder_view);

        // if the device does not have flashlight in its camera,
        // then remove the switch flashlight button...
//        if (!hasFlash()) {
//            switchFlashlightButton.setVisibility(View.GONE);
//        }

        capture = new CaptureManager(this, barcodeScannerView);
//        capture.initializeFromIntent(getIntent(), savedInstanceState);
//        capture.decode();

        barcodeScannerView.decodeSingle(this);
        GetQRGatewayDetails();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();

        Log.v("LogTrack", "onDestroy");

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }


    @Override
    public void onTorchOn() {

    }

    @Override
    public void onTorchOff() {

    }

    @Override
    public void barcodeResult(BarcodeResult result) {
        Log.v("LogTrack", "barcodeResult");
        if (result != null) {
            Log.v("LogTrack", "result = " + result.getResultMetadata().entrySet().toString());
            try {

                parseQRCode(result.getText());

            } catch (Exception e) {
                Log.v("LogTrack", "e = " + e.getMessage());
                e.printStackTrace();
            }
        }


    }

    private void parseQRCode(String code) {
        PushPaymentData pushData;
        try {
            pushData = Parser.parse(code);

            Constants.code = code;
            if (!pushData.getPayloadFormatIndicator().equals("01")) {

                showDilogForError("Invalid QR Merchant Payload.", "ERROR");

            } else {

                for (int i = 0; i < jsonArray.length(); i++) {

                    try {
                        JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                        String gatewayIdentifier = jsonObject.getString("Gatewayidentifier");
                        if (gatewayIdentifier != null && !TextUtils.isEmpty(gatewayIdentifier)) {

                            String merchantIdentifier = pushData.getValue(gatewayIdentifier).toString();

                            if (!TextUtils.isEmpty(merchantIdentifier) && !merchantIdentifier.equals(null)) {

                                Constants.Gateway = jsonObject.getString("GatewayName");
                                Constants.ProcCode = jsonObject.getString("ProcCode");
                                Constants.logoLink = jsonObject.getString("Logolink");
                                Constants.Android = jsonObject.getString("Android");
                                Constants.OrignalAcquiringID = jsonObject.getString("OrignalAcquiringID");
                                Constants.OriginalMT = jsonObject.getString("OriginalMT");
                                if(Constants.Android.equals("N")){
                                    showDilogForError("Service is currently unavailable. Please try again later.","ERROR");
                                }
                                Constants.MERCHANTIDENTIFIER = merchantIdentifier;
                                break;

                            }else{

                                showDilogForError("This merchant is not recognized.","ERROR");
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                Constants.pushPaymentData = pushData;
                startActivity(new Intent(MainActivity.this,QRPaymentDetails.class));
                capture.onDestroy();
            }

        } catch (ConflictiveTagException e) {
            System.out.println("ConflictiveTagException : " + e);
        } catch (InvalidTagValueException e) {
            System.out.println("InvalidTagValueException : " + e);
        } catch (MissingTagException e) {
            System.out.println("MissingTagException : " + e);
        } catch (UnknownTagException e) {
            System.out.println("UnknownTagException : " + e);
        } catch (FormatException e) {
            System.out.println("FormatException : " + e);
        }
    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }

    public void GetQRGatewayDetails() {


        try {
            dialog.show();
            if (helper.isNetworkAvailable()) {

                try {
                    Constants.filterGatewayName = "MPQR";
                    int stanNumber = 6;
                    String stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
                    int rrnNumber = 12;
                    String rrn = String.valueOf(Helper.generateRandom(rrnNumber));
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sSigns_LegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                    params.put("sServerSessionKey", md5);
                    params.put("sSTAN", stan);
                    params.put("sRRN", rrn);
                    params.put("sSigns_FilterGatewayName", Constants.filterGatewayName);
                    HttpsTrustManager.allowMySSL(MainActivity.this);
                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getQRGatewayDetails,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getString("Status_Code").equals("00")) {

                                                dialog.dismiss();
                                                jsonArray = jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getJSONArray("PaymentsGatewayList");
//                                                PPIntentIntegrator integrator = new PPIntentIntegrator(MainActivity.this);
//                                                integrator.setOrientationLocked(false);
//                                                integrator.setBeepEnabled(false);
//                                                cameraScanStartTime = System.nanoTime();
//                                                integrator.initiateScan();

                                            } else if (jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getString("Status_Code").equals("54")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getString("Status_Description"), "ERROR", MainActivity.this, new LoginScreen());


                                            } else {

                                                dialog.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getString("Status_Description"), "ERROR");
                                            }

                                        } else {

                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sSigns_LegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                            params.put("sSigns_CustomerFeedBack", feedBack);
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            dialog.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }


    }

    public void showDilogForError(String msg, String header) {

        dialog1 = new Dialog(MainActivity.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        textView = dialog1.findViewById(R.id.validatingTxt);
        btn = dialog1.findViewById(R.id.doneBtn);
        heading = dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
                finish();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog1.show();

    }

    public void showDilogForErrorForLogin(String msg, String header) {

        dialog1 = new Dialog(MainActivity.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        message = dialog1.findViewById(R.id.validatingTxt);
        doneBtn = dialog1.findViewById(R.id.doneBtn);
        heading = dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(true);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog1.show();


    }

    private void showDialog(String message) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Logout:");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

//                constants.editor.clear();
//                constants.editor.commit();
//                constants.editor.apply();
//                constants.nonSambaCredentialsEditor.clear();
//                constants.nonSambaCredentialsEditor.commit();
//                constants.nonSambaCredentialsEditor.apply();
//                constants.destinationAccountEditor.clear().commit();
//                constants.destinationAccountEditor.apply();
//                constants.sourceAccountEditor.clear().commit();
//                constants.sourceAccountEditor.apply();
//                constants.accountListEditor.clear().commit();
//                constants.accountListEditor.apply();
//                constants.accountsEditor.clear().commit();
//                constants.accountsEditor.apply();
//                constants.forgotPasswordEditor.clear().commit();
//                constants.forgotPasswordEditor.apply();
//                Handler handler1 = new Handler();
//                handler1.postDelayed(new Runnable() {
//                    public void run() {
//
//                        dialog.dismiss();
//                        Intent intent = new Intent(MainActivity.this, LoginScreen.class);
//                        startActivity(intent);
//                        overridePendingTransition(0, 0);
//                        finish();
//                        finishAffinity();
//
//                    }
//                }, 1000);
                Logout();
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog1, int which) {
                dialog1.cancel();
                dialog.dismiss();
            }
        });

        builder.show();
    }

    public void Logout() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSessionID", Constants.Shared_KEY);
                    HttpsTrustManager.allowMySSL(MainActivity.this);
                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.logoutURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Code").equals("00")) {
                                                constants.editor.clear();
                                                constants.editor.commit();
                                                constants.editor.apply();
                                                constants.nonSambaCredentialsEditor.clear();
                                                constants.nonSambaCredentialsEditor.commit();
                                                constants.nonSambaCredentialsEditor.apply();
                                                constants.destinationAccountEditor.clear().commit();
                                                constants.destinationAccountEditor.apply();
                                                constants.sourceAccountEditor.clear().commit();
                                                constants.sourceAccountEditor.apply();
                                                constants.accountListEditor.clear().commit();
                                                constants.accountListEditor.apply();
                                                constants.accountsEditor.clear().commit();
                                                constants.accountsEditor.apply();
                                                constants.forgotPasswordEditor.clear().commit();
                                                constants.forgotPasswordEditor.apply();
                                                constants.purposeBillList.clear().commit();
                                                constants.purposeBillList.apply();
                                                constants.purposePrepaidList.clear().commit();
                                                constants.purposePrepaidList.apply();
                                                constants.purposeListEditor.clear().commit();
                                                constants.purposeListEditor.apply();


                                                dialog.dismiss();
                                                Intent intent = new Intent(MainActivity.this, LoginScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                finish();
                                                finishAffinity();


                                            } else {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Description"), "ERROR", MainActivity.this, new LoginScreen());

                                            }

                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sSessionID", Constants.Shared_KEY);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                }
            } else {
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity", "WARNING");


            }
        } catch (Exception e) {

            dialog.dismiss();
            utils.showDilogForError("Please check your internet connectivity", "WARNING");


        }
    }


//    func parseQRCode(code:String) {
//
//        do {
//
//            print(" ==== QR [ \(code)]")
//
//            // Parse qr code
//
//            let pushData = try MPQRParser.parse(string:code)
//
//            // Print data in string format
//
//            let merchantName = pushData.merchantName
//
//            var merchantIdentifier = ""
//
//            let merchantCategoryCode = pushData.merchantCategoryCode
//
//            let countryCode = pushData.countryCode
//
//            var gatewayName = ""
//
//            var logoLink = ""
//
//            let qrString = code
//
//            if (pushData.payloadFormatIndicator != "01") {
//
//                showQRErrors(error:"Invalid QR Merchant Payload.")
//
//            } else {
//
//                let cityCode = pushData.merchantCity
//
//                let transactionAmount = pushData.transactionAmount
//
//                let tipIndicator = pushData.tipOrConvenienceIndicator
//
//                let tipFixed = pushData.valueOfConvenienceFeeFixed
//
//                let tipPercentage = pushData.valueOfConvenienceFeePercentage
//
//
//                let qrDetails : [NSDictionary] =UserConstants.QR_DETAILS.shuffled()
//
//                for qrDetail in qrDetails {
//
//                    let gatewayIdentifier = qrDetail.object(forKey:"Gatewayidentifier")as ? String
//
//                    if (gatewayIdentifier != nil && merchantIdentifier == "") {
//
//                        do {
//
//                            merchantIdentifier = try pushData.getTagInfoValue(forTagString:
//                            gatewayIdentifier !)as !String
//
//                            if (merchantIdentifier != "") {
//
//                                gatewayName = qrDetail.object(forKey:"GatewayName")as ? String ??
//                                "MasterPass"
//
//                                logoLink = qrDetail.object(forKey:"Logolink")as ? String ??""
//
//                            }
//
//                        }catch{
//
//                            //continue loop
//
//                        }
//
//                    }
//
//                }
//
//
////                merchantIdentifier = pushData.merchantIdentifierMastercard05 ?? ""
//
////                gatewayName = "MasterPass"
//
//
//                UserConstants.QR_MERCHANT_NAME = merchantName ??""
//
//                UserConstants.QR_MERCHANT_IDENTIFIER = merchantIdentifier ??""
//
//                UserConstants.QR_MERCHANT_CATEGORYCODE = merchantCategoryCode ??""
//
//                UserConstants.QR_COMPLETE_DATA = qrString ??""
//
////                UserConstants.QR_MERCHANT_CURRENCYCODE = pushData.transactionCurrencyCode ?? ""
//
//                UserConstants.QR_MERCHANT_CURRENCYCODE = "586"
//
//                UserConstants.QR_MERCHANT_LOGOLINK = logoLink ??""
//
//                UserConstants.QR_GATEWAY_IDENTIFIER = gatewayName ??""
//
//                UserConstants.QR_PAYMENT_TYPE = gatewayName ??""
//
//                UserConstants.QR_COUNTRY_CODE = countryCode ??""
//
//                UserConstants.QR_CITY_CODE = cityCode ??""
//
//                UserConstants.QR_TRANSACTION_AMOUNT = transactionAmount ??""
//
//                UserConstants.QR_FEE_INDICATOR = tipIndicator ??""
//
//                UserConstants.QR_FEE_FIXED = tipFixed ??""
//
//                UserConstants.QR_FEE_PERCENTAGE = tipFixed ??""
//
//
//                print(pushData.dumpData())
//
//
//                DispatchQueue.main.async(execute:{
//                    () -> Void in
//
//                    let initViewController:
//                    QRPaymentMainController = (self.storyboard ?.instantiateViewController(withIdentifier:
//                    "QRPaymentMainController")as ? QRPaymentMainController)!
//
//                            self.present(initViewController, animated:true, completion:nil)
//
//                })
//
//            }
//
//        } catch let error as MPQRError {
//
//            if let str = error.getString() {
//
//                showQRErrors(error:"Invalid QR Merchant validation.")
//
//                print("Error: \(str)")
//
//            }else
//
//            {
//
//                print("Unknown error occurred \(error)")
//
//            }
//
//        } catch{
//
//            print("Unknown error occurred \(error)")
//
//        }
//
//    }


}
