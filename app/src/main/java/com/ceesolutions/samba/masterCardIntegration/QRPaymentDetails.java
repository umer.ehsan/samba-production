package com.ceesolutions.samba.masterCardIntegration;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.transactions.SelectAccountScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class QRPaymentDetails extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    String res = "", res2 = "";
    String transactionAmountInteger;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private ExpandableLayout expandableLayout0;
    private CardView cardView;
    private Constants constants;
    private Dialog pd;
    private String remarks, amount, type;
    private TextInputLayout amountTxtInput4, amountTxtInput, remarksTxtInput5;
    private EditText editAmount, editRemarks, payabaleAmount, editAmount4, editRemarks5;
    private RelativeLayout upperPart1, upperPart2;
    private TextView message, consumerIDTxt, consumerNumberTxt, billingTxt, billingMonthTxt, dueDateTxt, payableBeforeTxt, payableAfterTxt, statusTxt, mainTxt, errorMessageForAmount, errorMessageForRemarks, errorMessageForSpinner, userNameTxt, accMaskTxt, amountTxt, friendNameTxt, itemName, validateBtn;
    private TextView additionalTxt, conversionHeading3, payableAfterTxt4, chargesHeading3, statusTxt4, conversionHeading4, payableAfterTxt5, chargesHeading4, statusTxt5;
    private Spinner purposesSpinner;
    private String purposeSelected;
    private String[] purpose;
    private ImageView arrowDestination, backBtn;
    private RelativeLayout conversion4, conversion3;
    private FrameLayout relativeLayout4, relativeLayout5;
    private ArrayList<String> purpStringArrayList;
    private String sourceBalance, sourceuserName, sourceAccount, sourceCurrency, distributorID, distributorAccountNumber, mobileNumber, distributorName, length;
    private Utils utils;
    private boolean isSource = false;
    private boolean isDestination = false;
    private boolean isFirst = false;
    private String identityValue, alias, stan, rrn;
    private String beneType = "SAMBA";
    private TextView doneBtn, textView, heading;
    private Dialog dialog;
    private String minAmount, categoryType, billConsumerNumber, sourceminAmount, amountAfterDue, amountBeforeDue, billStatus, billingMonth, dueDate, consumerName, companyName;
    private String maxAmount, branchCode, mobile, email, messageId, targetId, friendNumber;
    private float amountToInt, maxAmountToInt, minAmountToInt;
    private int bal;
    private boolean flag = false;
    private Intent intent1;
    private String isSelectSource, isSelectDestination;
    private boolean isVisible = false;
    private AppPreferences appPreferences;
    private String location;
    private View dueView4;
    private boolean isPostpaid = false;
    private Dialog dialog1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_detail_screen);
        getSupportActionBar().hide();

        initViews();


        editRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                remarks = editable.toString();
            }
        });


        editAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    res = charSequence.toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                try {

                    res = charSequence.toString();

                    String tipIndicator = Constants.pushPaymentData.getTipOrConvenienceIndicator();
                    Double tipFixed = Constants.pushPaymentData.getValueOfConvenienceFeeFixed();
                    Double tipPercentage = Constants.pushPaymentData.getValueOfConvenienceFeePercentage();


                    Double amount = Double.valueOf(res);

                    if (tipIndicator != null && !TextUtils.isEmpty(tipIndicator)) {
                        if (tipIndicator.equals("01")) {
                            Float tip = Float.valueOf(editAmount4.getText().toString());
                            amount = amount + tip;

                        } else if (tipIndicator.equals("02")) {
                            if (tipFixed != null && tipFixed != 0) {
                                Double tipAmount = (tipFixed);
                                amount = amount + tipAmount;

                            } else {
                                Double tipFixedInput = Double.valueOf(editAmount4.getText().toString());
                                if (tipFixedInput != null && tipFixedInput != 0) {
                                    Double tipAmount = tipFixedInput;
                                    amount = amount + tipAmount;

                                }
                            }
                        } else if (tipIndicator == "03") {
                            if (tipPercentage != null && tipPercentage != 0) {
                                Double tipAmount = tipPercentage;
                                amount = amount + (amount * tipAmount / 100);

                            }
                        }
                    }


                    transactionAmountInteger = String.valueOf(amount);
                    editRemarks5.setText(transactionAmountInteger);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });

        editAmount4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                res2 = charSequence.toString();

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                try {
                    if (!TextUtils.isEmpty(editAmount.getText().toString())) {
                        Double paymentAmount = Double.valueOf(editAmount.getText().toString());
                        res2 = charSequence.toString();
                        Double amount = Double.valueOf(res2);
                        Double totalAmount = amount + paymentAmount;
                        transactionAmountInteger = String.valueOf(totalAmount);
                        editRemarks5.setText(transactionAmountInteger);
                    } else {
                        Toast.makeText(QRPaymentDetails.this, "Please fill payment amount !", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });


        upperPart1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                upperPart1.setEnabled(false);
                isSource = true;
                isDestination = false;
                isFirst = true;
                Intent intent = new Intent(QRPaymentDetails.this, SelectAccountScreen.class);
                intent.putExtra("source", "source");
                startActivity(intent);


            }
        });

        upperPart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                upperPart2.setEnabled(false);
//                Intent intent = new Intent(QRPaymentDetails.this, ManageBillerScreen.class);
//                intent.putExtra("source", "destination");
//                startActivity(intent);
//                isDestination = true;
//                isSource = false;
//                isFirst = true;


            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAnalytics.getInstance(QRPaymentDetails.this).logEvent("Qr_Payment_select_account_add_details", new Bundle());

                validateBtn.setEnabled(false);

                if ((TextUtils.isEmpty(sourceAccount) || sourceAccount.equals("N/A")) || (TextUtils.isEmpty(sourceBalance) || sourceBalance.equals("N/A")) || (TextUtils.isEmpty(sourceuserName) || sourceuserName.equals("N/A"))) {

                    utils.showDilogForError("Please select Source account.", "WARNING");
                    validateBtn.setEnabled(true);

                }
                else {
                    if (!sourceCurrency.equals("PKR")) {

                        utils.showDilogForError("QR Payments is not allowed from FCY accounts.", "ERROR");
                        validateBtn.setEnabled(true);

                    } else {
                        if (TextUtils.isEmpty(editRemarks5.getText().toString()) || editRemarks5.getText().toString().equals("0.0") || editRemarks5.getText().toString().equals("0")) {

                            utils.showDilogForError("Please enter valid amount.", "ERROR");
                            validateBtn.setEnabled(true);
                        } else {

                            Verification();
                        }

                    }
                }

                }

        });


    }

    public void initViews() {

        try {

            Constants.currencyHashMap = new HashMap<>();
            Constants.currencyHashMap.put("784", "AED");
            Constants.currencyHashMap.put("971", "AFN");
            Constants.currencyHashMap.put("8", "ALL");
            Constants.currencyHashMap.put("51", "AMD");
            Constants.currencyHashMap.put("532", "ANG");
            Constants.currencyHashMap.put("973", "AOA");
            Constants.currencyHashMap.put("32", "ARS");
            Constants.currencyHashMap.put("36", "AUD");
            Constants.currencyHashMap.put("533", "AWG");
            Constants.currencyHashMap.put("944", "AZN");
            Constants.currencyHashMap.put("977", "BAM");
            Constants.currencyHashMap.put("52", "BBD");
            Constants.currencyHashMap.put("50", "BDT");
            Constants.currencyHashMap.put("975", "BGN");
            Constants.currencyHashMap.put("48", "BHD");
            Constants.currencyHashMap.put("108", "BIF");
            Constants.currencyHashMap.put("60", "BMD");
            Constants.currencyHashMap.put("96", "BND");
            Constants.currencyHashMap.put("68", "BOB");
            Constants.currencyHashMap.put("984", "BOV");
            Constants.currencyHashMap.put("986", "BRL");
            Constants.currencyHashMap.put("44", "BSD");
            Constants.currencyHashMap.put("64", "BTN");
            Constants.currencyHashMap.put("72", "BWP");
            Constants.currencyHashMap.put("933", "BYN");
            Constants.currencyHashMap.put("84", "BZD");
            Constants.currencyHashMap.put("124", "CAD");
            Constants.currencyHashMap.put("976", "CDF");
            Constants.currencyHashMap.put("947", "CHE");
            Constants.currencyHashMap.put("756", "CHF");
            Constants.currencyHashMap.put("948", "CHW");
            Constants.currencyHashMap.put("990", "CLF");
            Constants.currencyHashMap.put("152", "CLP");
            Constants.currencyHashMap.put("156", "CNY");
            Constants.currencyHashMap.put("170", "COP");
            Constants.currencyHashMap.put("970", "COU");
            Constants.currencyHashMap.put("188", "CRC");
            Constants.currencyHashMap.put("931", "CUC");
            Constants.currencyHashMap.put("192", "CUP");
            Constants.currencyHashMap.put("132", "CVE");
            Constants.currencyHashMap.put("203", "CZK");
            Constants.currencyHashMap.put("262", "DJF");
            Constants.currencyHashMap.put("208", "DKK");
            Constants.currencyHashMap.put("214", "DOP");
            Constants.currencyHashMap.put("12", "DZD");
            Constants.currencyHashMap.put("818", "EGP");
            Constants.currencyHashMap.put("232", "ERN");
            Constants.currencyHashMap.put("230", "ETB");
            Constants.currencyHashMap.put("978", "EUR");
            Constants.currencyHashMap.put("242", "FJD");
            Constants.currencyHashMap.put("238", "FKP");
            Constants.currencyHashMap.put("826", "GBP");
            Constants.currencyHashMap.put("981", "GEL");
            Constants.currencyHashMap.put("936", "GHS");
            Constants.currencyHashMap.put("292", "GIP");
            Constants.currencyHashMap.put("270", "GMD");
            Constants.currencyHashMap.put("324", "GNF");
            Constants.currencyHashMap.put("320", "GTQ");
            Constants.currencyHashMap.put("328", "GYD");
            Constants.currencyHashMap.put("344", "HKD");
            Constants.currencyHashMap.put("340", "HNL");
            Constants.currencyHashMap.put("191", "HRK");
            Constants.currencyHashMap.put("332", "HTG");
            Constants.currencyHashMap.put("348", "HUF");
            Constants.currencyHashMap.put("360", "IDR");
            Constants.currencyHashMap.put("376", "ILS");
            Constants.currencyHashMap.put("356", "INR");
            Constants.currencyHashMap.put("368", "IQD");
            Constants.currencyHashMap.put("364", "IRR");
            Constants.currencyHashMap.put("352", "ISK");
            Constants.currencyHashMap.put("388", "JMD");
            Constants.currencyHashMap.put("400", "JOD");
            Constants.currencyHashMap.put("392", "JPY");
            Constants.currencyHashMap.put("404", "KES");
            Constants.currencyHashMap.put("417", "KGS");
            Constants.currencyHashMap.put("116", "KHR");
            Constants.currencyHashMap.put("174", "KMF");
            Constants.currencyHashMap.put("408", "KPW");
            Constants.currencyHashMap.put("410", "KRW");
            Constants.currencyHashMap.put("414", "KWD");
            Constants.currencyHashMap.put("136", "KYD");
            Constants.currencyHashMap.put("398", "KZT");
            Constants.currencyHashMap.put("418", "LAK");
            Constants.currencyHashMap.put("422", "LBP");
            Constants.currencyHashMap.put("144", "LKR");
            Constants.currencyHashMap.put("430", "LRD");
            Constants.currencyHashMap.put("426", "LSL");
            Constants.currencyHashMap.put("434", "LYD");
            Constants.currencyHashMap.put("504", "MAD");
            Constants.currencyHashMap.put("498", "MDL");
            Constants.currencyHashMap.put("969", "MGA");
            Constants.currencyHashMap.put("807", "MKD");
            Constants.currencyHashMap.put("104", "MMK");
            Constants.currencyHashMap.put("496", "MNT");
            Constants.currencyHashMap.put("446", "MOP");
            Constants.currencyHashMap.put("929", "MRU[12]");
            Constants.currencyHashMap.put("480", "MUR");
            Constants.currencyHashMap.put("462", "MVR");
            Constants.currencyHashMap.put("454", "MWK");
            Constants.currencyHashMap.put("484", "MXN");
            Constants.currencyHashMap.put("979", "MXV");
            Constants.currencyHashMap.put("458", "MYR");
            Constants.currencyHashMap.put("943", "MZN");
            Constants.currencyHashMap.put("516", "NAD");
            Constants.currencyHashMap.put("566", "NGN");
            Constants.currencyHashMap.put("558", "NIO");
            Constants.currencyHashMap.put("578", "NOK");
            Constants.currencyHashMap.put("524", "NPR");
            Constants.currencyHashMap.put("554", "NZD");
            Constants.currencyHashMap.put("512", "OMR");
            Constants.currencyHashMap.put("590", "PAB");
            Constants.currencyHashMap.put("604", "PEN");
            Constants.currencyHashMap.put("598", "PGK");
            Constants.currencyHashMap.put("608", "PHP");
            Constants.currencyHashMap.put("586", "PKR");
            Constants.currencyHashMap.put("985", "PLN");
            Constants.currencyHashMap.put("600", "PYG");
            Constants.currencyHashMap.put("634", "QAR");
            Constants.currencyHashMap.put("946", "RON");
            Constants.currencyHashMap.put("941", "RSD");
            Constants.currencyHashMap.put("643", "RUB");
            Constants.currencyHashMap.put("646", "RWF");
            Constants.currencyHashMap.put("682", "SAR");
            Constants.currencyHashMap.put("90", "SBD");
            Constants.currencyHashMap.put("690", "SCR");
            Constants.currencyHashMap.put("938", "SDG");
            Constants.currencyHashMap.put("752", "SEK");
            Constants.currencyHashMap.put("702", "SGD");
            Constants.currencyHashMap.put("654", "SHP");
            Constants.currencyHashMap.put("694", "SLL");
            Constants.currencyHashMap.put("706", "SOS");
            Constants.currencyHashMap.put("968", "SRD");
            Constants.currencyHashMap.put("728", "SSP");
            Constants.currencyHashMap.put("930", "STN[14]");
            Constants.currencyHashMap.put("222", "SVC");
            Constants.currencyHashMap.put("760", "SYP");
            Constants.currencyHashMap.put("748", "SZL");
            Constants.currencyHashMap.put("764", "THB");
            Constants.currencyHashMap.put("972", "TJS");
            Constants.currencyHashMap.put("934", "TMT");
            Constants.currencyHashMap.put("788", "TND");
            Constants.currencyHashMap.put("776", "TOP");
            Constants.currencyHashMap.put("949", "TRY");
            Constants.currencyHashMap.put("780", "TTD");
            Constants.currencyHashMap.put("901", "TWD");
            Constants.currencyHashMap.put("834", "TZS");
            Constants.currencyHashMap.put("980", "UAH");
            Constants.currencyHashMap.put("800", "UGX");
            Constants.currencyHashMap.put("840", "USD");
            Constants.currencyHashMap.put("997", "USN");
            Constants.currencyHashMap.put("940", "UYI");
            Constants.currencyHashMap.put("858", "UYU");
            Constants.currencyHashMap.put("927", "UYW");
            Constants.currencyHashMap.put("860", "UZS");
            Constants.currencyHashMap.put("928", "VES");
            Constants.currencyHashMap.put("704", "VND");
            Constants.currencyHashMap.put("548", "VUV");
            Constants.currencyHashMap.put("882", "WST");
            Constants.currencyHashMap.put("950", "XAF");
            Constants.currencyHashMap.put("961", "XAG");
            Constants.currencyHashMap.put("959", "XAU");
            Constants.currencyHashMap.put("955", "XBA");
            Constants.currencyHashMap.put("956", "XBB");
            Constants.currencyHashMap.put("957", "XBC");
            Constants.currencyHashMap.put("958", "XBD");
            Constants.currencyHashMap.put("951", "XCD");
            Constants.currencyHashMap.put("960", "XDR");
            Constants.currencyHashMap.put("952", "XOF");
            Constants.currencyHashMap.put("964", "XPD");
            Constants.currencyHashMap.put("953", "XPF");
            Constants.currencyHashMap.put("962", "XPT");
            Constants.currencyHashMap.put("994", "XSU");
            Constants.currencyHashMap.put("963", "XTS");
            Constants.currencyHashMap.put("965", "XUA");
            Constants.currencyHashMap.put("999", "XXX");
            Constants.currencyHashMap.put("886", "YER");
            Constants.currencyHashMap.put("710", "ZAR");
            Constants.currencyHashMap.put("967", "ZMW");
            Constants.currencyHashMap.put("932", "ZWL");
            CryptLib _crypt = new CryptLib();
            purposesSpinner = findViewById(R.id.purposeSpinner);
            validateBtn = findViewById(R.id.validateBtn);
            arrowDestination = findViewById(R.id.arrowDestination);
            consumerIDTxt = findViewById(R.id.consumerIDTxt);
            dueDateTxt = findViewById(R.id.dueDateTxt);
            billingMonthTxt = findViewById(R.id.billingMonthTxt);
            consumerNumberTxt = findViewById(R.id.consumerNumberTxt);
            billingTxt = findViewById(R.id.billingTxt);
            payableAfterTxt = findViewById(R.id.payableAfterTxt);
            payableBeforeTxt = findViewById(R.id.payableBeforeTxt);
            statusTxt = findViewById(R.id.statusTxt);
            dueView4 = findViewById(R.id.dueView4);
            upperPart1 = findViewById(R.id.upperPart1);
            upperPart2 = findViewById(R.id.upperPart2);
            friendNameTxt = findViewById(R.id.friendNameTxt);
            payabaleAmount = findViewById(R.id.payabaleAmount);
            remarksTxtInput5 = findViewById(R.id.remarksTxtInput5);
            cardView = findViewById(R.id.card_view);
            userNameTxt = findViewById(R.id.userNameTxt);
            userNameTxt.setText("Select Source Account");
            accMaskTxt = findViewById(R.id.accMaskTxt);
            accMaskTxt.setVisibility(View.INVISIBLE);
            amountTxt = findViewById(R.id.amountTxt);
            amountTxt.setVisibility(View.INVISIBLE);
            itemName = findViewById(R.id.itemName);
            editRemarks5 = findViewById(R.id.editRemarks5);
            amountTxtInput4 = findViewById(R.id.amountTxtInput4);
            amountTxtInput = findViewById(R.id.amountTxtInput);
            editAmount4 = findViewById(R.id.editAmount4);
            relativeLayout4 = findViewById(R.id.relativeLayout4);
            conversion4 = findViewById(R.id.conversion4);
            conversion3 = findViewById(R.id.conversion3);
            backBtn = findViewById(R.id.backBtn);
            relativeLayout5 = findViewById(R.id.relativeLayout5);
            itemName.setText(Constants.pushPaymentData.getMerchantName());


            dueDateTxt.setText(Constants.pushPaymentData.getMerchantCity());
            payableBeforeTxt.setText(Constants.pushPaymentData.getCountryCode());
            billingTxt.setText(Constants.MERCHANTIDENTIFIER);
            additionalTxt = findViewById(R.id.additionalTxt);
            conversionHeading3 = findViewById(R.id.conversionHeading3);
            payableAfterTxt4 = findViewById(R.id.payableAfterTxt);
            chargesHeading3 = findViewById(R.id.chargesHeading3);
            statusTxt4 = findViewById(R.id.statusTxt);
            conversionHeading4 = findViewById(R.id.conversionHeading4);
            payableAfterTxt5 = findViewById(R.id.payableAfterTxt4);
            chargesHeading4 = findViewById(R.id.chargesHeading4);
            statusTxt5 = findViewById(R.id.statusTxt4);
            errorMessageForAmount = findViewById(R.id.errorMessageForAmount);
            errorMessageForRemarks = findViewById(R.id.errorMessageForRemarks);
            errorMessageForSpinner = findViewById(R.id.errorMessageForSpinner);
            editRemarks = findViewById(R.id.editRemarks);
            editAmount = findViewById(R.id.editAmount);
            expandableLayout0 = findViewById(R.id.expandable_layout_0);
            requestQueue = Volley.newRequestQueue(QRPaymentDetails.this);
            mainTxt = findViewById(R.id.mainTxt);
            helper = Helper.getHelper(QRPaymentDetails.this);
            constants = Constants.getConstants(QRPaymentDetails.this);
            pd = new Dialog(QRPaymentDetails.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);

            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onBackPressed();
                }
            });

//            if (Constants.currencyHashMap.get(Constants.pushPaymentData.getTransactionCurrencyCode()) != null) {
                if (Constants.currencyHashMap.get(Constants.pushPaymentData.getTransactionCurrencyCode()).equals("PKR")) {
                billingMonthTxt.setText(Constants.currencyHashMap.get(Constants.pushPaymentData.getTransactionCurrencyCode()));
                GetPurposeBillList();
                String billNumber = Constants.pushPaymentData.getAdditionalData().getBillNumber();
                String mobileNumber = Constants.pushPaymentData.getAdditionalData().getMobileNumber();
                String storeid = Constants.pushPaymentData.getAdditionalData().getStoreId();
                String loyaltyNumber = Constants.pushPaymentData.getAdditionalData().getLoyaltyNumber();
                String referenceId = Constants.pushPaymentData.getAdditionalData().getReferenceId();
                String consumerId = Constants.pushPaymentData.getAdditionalData().getConsumerId();
                String terminalId = Constants.pushPaymentData.getAdditionalData().getTerminalId();
                String purposeOfTrans = Constants.pushPaymentData.getAdditionalData().getPurpose();
                String consumerData = Constants.pushPaymentData.getAdditionalData().getAdditionalDataRequest();
                String additionalDataString = consumerData;
                int count = 0;
                if (billNumber != (null) && !TextUtils.isEmpty(billNumber)) {
                    conversionHeading3.setText("Bill Number");
                    payableAfterTxt4.setText(billNumber);
                    additionalTxt.setVisibility(View.VISIBLE);
                    conversionHeading3.setVisibility(View.VISIBLE);
                    payableAfterTxt4.setVisibility(View.VISIBLE);
                    count++;
                }

                if (mobileNumber != (null) && !TextUtils.isEmpty(mobileNumber)) {
                    if (count == 0) {
                        conversionHeading3.setText("Mobile Number");
                        payableAfterTxt4.setText(mobileNumber);
                        additionalTxt.setVisibility(View.VISIBLE);
                        conversionHeading3.setVisibility(View.VISIBLE);
                        payableAfterTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 1) {

                        chargesHeading3.setText("Mobile Number");
                        statusTxt4.setText(mobileNumber);
                        chargesHeading3.setVisibility(View.VISIBLE);
                        statusTxt4.setVisibility(View.VISIBLE);
                        count++;
                    }
                }

                if (storeid != (null) && !TextUtils.isEmpty(storeid)) {
                    if (count == 0) {
                        conversionHeading3.setText("Store ID");
                        payableAfterTxt4.setText(storeid);
                        additionalTxt.setVisibility(View.VISIBLE);
                        conversionHeading3.setVisibility(View.VISIBLE);
                        payableAfterTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 1) {

                        chargesHeading3.setText("Store ID");
                        statusTxt4.setText(storeid);
                        chargesHeading3.setVisibility(View.VISIBLE);
                        statusTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 2) {

                        conversionHeading4.setText("Store ID");
                        payableAfterTxt5.setText(storeid);
                        dueView4.setVisibility(View.VISIBLE);
                        conversionHeading4.setVisibility(View.VISIBLE);
                        payableAfterTxt5.setVisibility(View.VISIBLE);
                        count++;
                    }
                }


                if (loyaltyNumber != (null) && !TextUtils.isEmpty(loyaltyNumber)) {
                    if (count == 0) {
                        conversionHeading3.setText("Loyalty Number");
                        payableAfterTxt4.setText(loyaltyNumber);
                        payableAfterTxt4.setText(storeid);
                        additionalTxt.setVisibility(View.VISIBLE);
                        conversionHeading3.setVisibility(View.VISIBLE);
                        payableAfterTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 1) {

                        chargesHeading3.setText("Loyalty Number");
                        statusTxt4.setText(loyaltyNumber);
                        chargesHeading3.setVisibility(View.VISIBLE);
                        statusTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 2) {

                        conversionHeading4.setText("Loyalty Number");
                        payableAfterTxt5.setText(loyaltyNumber);
                        dueView4.setVisibility(View.VISIBLE);
                        conversionHeading4.setVisibility(View.VISIBLE);
                        payableAfterTxt5.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 3) {

                        chargesHeading4.setText("Loyalty Number");
                        statusTxt5.setText(loyaltyNumber);
                        chargesHeading4.setVisibility(View.VISIBLE);
                        statusTxt5.setVisibility(View.VISIBLE);
                        count++;
                    }
                }


                if (referenceId != (null) && !TextUtils.isEmpty(referenceId)) {
                    if (count == 0) {
                        conversionHeading3.setText("Reference ID");
                        payableAfterTxt4.setText(referenceId);
                        additionalTxt.setVisibility(View.VISIBLE);
                        conversionHeading3.setVisibility(View.VISIBLE);
                        payableAfterTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 1) {

                        chargesHeading3.setText("Reference ID");
                        statusTxt4.setText(referenceId);
                        chargesHeading3.setVisibility(View.VISIBLE);
                        statusTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 2) {

                        conversionHeading4.setText("Reference ID");
                        payableAfterTxt5.setText(referenceId);
                        dueView4.setVisibility(View.VISIBLE);
                        conversionHeading4.setVisibility(View.VISIBLE);
                        payableAfterTxt5.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 3) {

                        chargesHeading4.setText("Reference ID");
                        statusTxt5.setText(referenceId);
                        chargesHeading4.setVisibility(View.VISIBLE);
                        statusTxt5.setVisibility(View.VISIBLE);
                        count++;
                    }
                }


                if (consumerId != (null) && !TextUtils.isEmpty(consumerId)) {
                    if (count == 0) {
                        conversionHeading3.setText("Consumer ID");
                        payableAfterTxt4.setText(consumerId);
                        additionalTxt.setVisibility(View.VISIBLE);
                        conversionHeading3.setVisibility(View.VISIBLE);
                        payableAfterTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 1) {

                        chargesHeading3.setText("Consumer ID");
                        statusTxt4.setText(consumerId);
                        chargesHeading3.setVisibility(View.VISIBLE);
                        statusTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 2) {

                        conversionHeading4.setText("Consumer ID");
                        payableAfterTxt5.setText(consumerId);
                        dueView4.setVisibility(View.VISIBLE);
                        conversionHeading4.setVisibility(View.VISIBLE);
                        payableAfterTxt5.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 3) {

                        chargesHeading4.setText("Consumer ID");
                        statusTxt5.setText(consumerId);
                        chargesHeading4.setVisibility(View.VISIBLE);
                        statusTxt5.setVisibility(View.VISIBLE);
                        count++;
                    }
                }

                if (terminalId != (null) && !TextUtils.isEmpty(terminalId)) {
                    if (count == 0) {
                        conversionHeading3.setText("Terminal ID");
                        payableAfterTxt4.setText(terminalId);
                        additionalTxt.setVisibility(View.VISIBLE);
                        conversionHeading3.setVisibility(View.VISIBLE);
                        payableAfterTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 1) {

                        chargesHeading3.setText("Terminal ID");
                        statusTxt4.setText(terminalId);
                        chargesHeading3.setVisibility(View.VISIBLE);
                        statusTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 2) {

                        conversionHeading4.setText("Terminal ID");
                        payableAfterTxt5.setText(terminalId);
                        dueView4.setVisibility(View.VISIBLE);
                        conversionHeading4.setVisibility(View.VISIBLE);
                        payableAfterTxt5.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 3) {

                        chargesHeading4.setText("Terminal ID");
                        statusTxt5.setText(terminalId);
                        chargesHeading4.setVisibility(View.VISIBLE);
                        statusTxt5.setVisibility(View.VISIBLE);
                        count++;
                    }
                }


                if (consumerData != (null) && !TextUtils.isEmpty(consumerData)) {
                    if (count == 0) {
                        conversionHeading3.setText("Consumer Data");
                        payableAfterTxt4.setText(consumerData);
                        additionalTxt.setVisibility(View.VISIBLE);
                        conversionHeading3.setVisibility(View.VISIBLE);
                        payableAfterTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 1) {

                        chargesHeading3.setText("Consumer Data");
                        statusTxt4.setText(consumerData);
                        chargesHeading3.setVisibility(View.VISIBLE);
                        statusTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 2) {

                        conversionHeading4.setText("Consumer Data");
                        payableAfterTxt5.setText(consumerData);
                        dueView4.setVisibility(View.VISIBLE);
                        conversionHeading4.setVisibility(View.VISIBLE);
                        payableAfterTxt5.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 3) {

                        chargesHeading4.setText("Consumer Data");
                        statusTxt5.setText(consumerData);
                        chargesHeading4.setVisibility(View.VISIBLE);
                        statusTxt5.setVisibility(View.VISIBLE);
                        count++;
                    }
                }

                if (additionalDataString != (null) && !TextUtils.isEmpty(additionalDataString)) {
                    if (count == 0) {
                        conversionHeading3.setText("Additional Data");
                        payableAfterTxt4.setText(additionalDataString);
                        additionalTxt.setVisibility(View.VISIBLE);
                        conversionHeading3.setVisibility(View.VISIBLE);
                        payableAfterTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 1) {

                        chargesHeading3.setText("Additional Data");
                        statusTxt4.setText(additionalDataString);
                        chargesHeading3.setVisibility(View.VISIBLE);
                        statusTxt4.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 2) {

                        conversionHeading4.setText("Additional Data");
                        payableAfterTxt5.setText(additionalDataString);
                        dueView4.setVisibility(View.VISIBLE);
                        conversionHeading4.setVisibility(View.VISIBLE);
                        payableAfterTxt5.setVisibility(View.VISIBLE);
                        count++;
                    } else if (count == 3) {

                        chargesHeading4.setText("Additional Data");
                        statusTxt5.setText(additionalDataString);
                        chargesHeading4.setVisibility(View.VISIBLE);
                        statusTxt5.setVisibility(View.VISIBLE);
                        count++;
                    }
                }

                if (purposeOfTrans != (null) && !TextUtils.isEmpty(purposeOfTrans) && !Character.isDigit(purposeOfTrans.charAt(0))) {
                    spinnerAdapter purposeAdapter = new spinnerAdapter(QRPaymentDetails.this, R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(purposeOfTrans));
                    purposesSpinner.setClickable(false);
                }

                String QRTipIndicator = Constants.pushPaymentData.getTipOrConvenienceIndicator();
                String tipIndicator = Constants.pushPaymentData.getTipOrConvenienceIndicator();
                Double tipFixed = Constants.pushPaymentData.getValueOfConvenienceFeeFixed();
                Double tipPercentage = Constants.pushPaymentData.getValueOfConvenienceFeePercentage();
                boolean isPercentage = false;
                Double transactionAmountInteger = Double.valueOf(0);
                Double transactionAmount = Constants.pushPaymentData.getTransactionAmount();

                amountTxtInput.setHint("Payable Amount (" + Constants.currencyHashMap.get(Constants.pushPaymentData.getTransactionCurrencyCode()) + ")");
                if (transactionAmount != (null) && transactionAmount != 0) {
                    editAmount.setFocusableInTouchMode(false);
                    editAmount.setText(String.valueOf(transactionAmount));
                    transactionAmountInteger = transactionAmount;

//                self.PayableAmountLabel.text = transactionAmount as! String
                }
                if (tipIndicator != (null) && !TextUtils.isEmpty(tipIndicator)) {
                    if (tipIndicator.equals("01") && tipIndicator != (null)) {
                        amountTxtInput4.setHint("Tip Amount (" + Constants.currencyHashMap.get(Constants.pushPaymentData.getTransactionCurrencyCode()) + ")");
                    } else if (tipIndicator.equals("02")) {
                        if (tipFixed != (null) && tipFixed != 0) {
                            amountTxtInput4.setHint("Convenience Fee (" + Constants.currencyHashMap.get(Constants.pushPaymentData.getTransactionCurrencyCode()) + ")");
                            editAmount4.setText(String.valueOf(tipFixed));
                            editAmount4.setFocusableInTouchMode(false);
                            transactionAmountInteger = transactionAmountInteger + ((tipFixed));

                        } else {
                            amountTxtInput4.setHint("Convenience Fee (" + Constants.currencyHashMap.get(Constants.pushPaymentData.getTransactionCurrencyCode()) + ")");
                            editAmount4.setText("");
                            editAmount4.setFocusableInTouchMode(true);

                        }
                    } else if (tipIndicator.equals("03")) {
                        isPercentage = true;
                        if (tipPercentage != 0 && tipPercentage != (null)) {
                            amountTxtInput4.setHint("Convenience Fee Percentage (" + Constants.currencyHashMap.get(Constants.pushPaymentData.getTransactionCurrencyCode()) + ")");
                            editAmount4.setText(tipPercentage + "%");
                            editAmount4.setFocusableInTouchMode(false);
                            if (transactionAmount != 0 && transactionAmount != (null)) {

                                transactionAmountInteger = transactionAmountInteger + transactionAmountInteger * ((tipPercentage / 100));


                            }
                        } else {
                            amountTxtInput4.setHint("Convenience Fee (" + Constants.currencyHashMap.get(Constants.pushPaymentData.getTransactionCurrencyCode()) + ")");
                            editAmount4.setText("");
                            editAmount4.setFocusableInTouchMode(true);

                        }
                    }
                } else {
                    editAmount4.setText("Not Applicable");
                    editAmount4.setFocusableInTouchMode(false);
                }


                editRemarks5.setText(String.valueOf(transactionAmountInteger));

                pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                appPreferences = new AppPreferences(QRPaymentDetails.this);
                longitude1 = appPreferences.getString("longitude", "N/A");
                latitude1 = appPreferences.getString("latitude", "N/A");
                location = appPreferences.getString("location", "N/A");

                if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                    longitude = Double.valueOf(longitude1);
                    latitude = Double.valueOf(latitude1);
                    if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                    } else {
                        location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                    }
                } else {

                    longitude = 0;
                    latitude = 0;
                    location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
                }
                purpStringArrayList = new ArrayList<>();
                utils = new Utils(QRPaymentDetails.this);
                payabaleAmount.setText("");
                Calendar today = Calendar.getInstance();
                today.set(Calendar.MILLISECOND, 0);
                today.set(Calendar.SECOND, 0);
                today.set(Calendar.MINUTE, 0);
                today.set(Calendar.HOUR_OF_DAY, 0);
//                } else {
//                    showDilogForErrorQR(Constants.currencyHashMap.get(Constants.pushPaymentData.getTransactionCurrencyCode()) + " currency currently not supported !", "ERROR");
//                }

            } else {

                showDilogForErrorQR("This currency currently not supported !", "ERROR");
            }

//            showSpinner();


//            String menus = constants.sharedPreferences.getString("Menus", "N/A");
//
//            if (!TextUtils.isEmpty(menus) && !menus.equals("N/A") && !menus.equals("")) {
//
//                if (menus.toLowerCase().contains("pay bill")) {
//                    showDilogForErrorForMenu("This service is currently disabled. Please try again later.", "WARNING");
//                }
//            }


            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!flag) {
                        expandableLayout0.expand();
                        mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_less, 0);
                        flag = true;
                    } else {
                        expandableLayout0.collapse();
                        mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_more, 0);
                        flag = false;
                    }

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GetPurposeBillList() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    pd.show();
                    purpStringArrayList = new ArrayList<>();
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sPurposeGroupCode", "QR");
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(QRPaymentDetails.this);

                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getPuposesURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("POTList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        purpStringArrayList.add(jsonObj.getString("Name"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();

                                                    }

                                                }

                                                purpose = new String[purpStringArrayList.size()];
                                                purpose = purpStringArrayList.toArray(purpose);

//                                                Gson gson = new Gson();
//                                                String json = gson.toJson(purpStringArrayList);
//                                                constants.purposeBillList.putString("purposeList", json);
//                                                constants.purposeBillList.commit();
//                                                constants.purposeBillList.apply();
                                                showSpinner();
                                            } else if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR", QRPaymentDetails.this, new LoginScreen());
                                                validateBtn.setEnabled(true);

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                            validateBtn.setEnabled(true);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sPurposeGroupCode", "BP");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    validateBtn.setEnabled(true);
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
        }
    }

    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                if (!TextUtils.isEmpty(type) && type.equals("requestbill")) {
                    purposeSelected = constants.destinationAccount.getString("purpose", "N/A");
                    spinnerAdapter purposeAdapter = new spinnerAdapter(QRPaymentDetails.this, R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(constants.destinationAccount.getString("purpose", "N/A")));
                    purposesSpinner.setClickable(false);
                } else {
                    spinnerAdapter purposeAdapter = new spinnerAdapter(QRPaymentDetails.this, R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(0);
                    purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            try {


                                // Get select item
                                if (purposesSpinner.getSelectedItem() == "Purpose of Payment") {
                                    purposeSelected = purposesSpinner.getSelectedItem().toString();
                                    Log.d("identity", "---" + purposeSelected);

                                } else {

                                    purposeSelected = purposesSpinner.getSelectedItem().toString();

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });

                    pd.dismiss();

                }

            }
        }, 1000);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            CryptLib _crypt = new CryptLib();

            if (isFirst) {


                upperPart1.setEnabled(true);
                if (!TextUtils.isEmpty(type) && type.equals("requestbill")) {

                } else {
                    upperPart2.setEnabled(true);
                }
                if (isSource) {

                    isSelectSource = constants.sourceAccount.getString("isSelect", "N/A");
                    if (!isSelectSource.equals("N/A") && isSelectSource.equals("Yes")) {
                        sourceuserName = _crypt.decrypt(constants.sourceAccount.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        sourceBalance = _crypt.decrypt(constants.sourceAccount.getString("balance", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        sourceAccount = _crypt.decrypt(constants.sourceAccount.getString("accountNumber", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        branchCode = _crypt.decrypt(constants.sourceAccount.getString("branchCode", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        sourceCurrency = _crypt.decrypt(constants.sourceAccount.getString("currency", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        userNameTxt.setText(sourceuserName);
                        accMaskTxt.setVisibility(View.VISIBLE);
                        accMaskTxt.setText(utils.getMaskedString(sourceAccount));
                        amountTxt.setVisibility(View.VISIBLE);
                        amountTxt.setText(sourceCurrency + " " + sourceBalance);
                    }

                    constants.sourceAccountEditor.putString("isSelect", "No");
                    constants.sourceAccountEditor.commit();
                    constants.sourceAccountEditor.apply();
                    isSource = false;

                }

                if (isDestination) {

                    isSelectDestination = constants.destinationAccount.getString("isSelect", "N/A");
                    if (!isSelectDestination.equals("N/A") && isSelectDestination.equals("Yes")) {
                        distributorID = _crypt.decrypt(constants.destinationAccount.getString("DistributorID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        distributorAccountNumber = _crypt.decrypt(constants.destinationAccount.getString("distributorAccountNumber", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        amountAfterDue = _crypt.decrypt(constants.destinationAccount.getString("AmountAfterDueDate", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        amountBeforeDue = _crypt.decrypt(constants.destinationAccount.getString("AmountWithinDueDate", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        billStatus = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        billingMonth = _crypt.decrypt(constants.destinationAccount.getString("BillingMonth", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        dueDate = _crypt.decrypt(constants.destinationAccount.getString("DueDate", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        consumerName = _crypt.decrypt(constants.destinationAccount.getString("ConsumerName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        billConsumerNumber = _crypt.decrypt(constants.destinationAccount.getString("BillConsumerNumber", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        companyName = _crypt.decrypt(constants.destinationAccount.getString("CompanyName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        if (companyName.toLowerCase().contains("postpaid")) {
                            isPostpaid = true;
                        }
                        alias = _crypt.decrypt(constants.destinationAccount.getString("alias", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        String cat = constants.destinationAccount.getString("categoryType", "N/A");
                        if (!TextUtils.isEmpty(cat) && !cat.equals("N/A") && !cat.equals("null") && !cat.equals(null)) {
                            categoryType = _crypt.decrypt(constants.destinationAccount.getString("categoryType", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        } else {
                            categoryType = "N/A";
                        }
                        //                itemName.setText(distributorName);


                        payableBeforeTxt.setText(amountBeforeDue);
                        payableAfterTxt.setText(amountAfterDue);
                        billingMonthTxt.setText(billingMonth);
                        billingTxt.setText(companyName);
                        dueDateTxt.setText(dueDate);
                        statusTxt.setText(billStatus);
                        consumerIDTxt.setText(billConsumerNumber);
                        consumerNumberTxt.setText(consumerName);
                        itemName.setText(alias);


                        if (billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("unpaid") || billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("blocked")) {

                            statusTxt.setTextColor(getResources().getColor(R.color.tabunderline));
                        } else
                            statusTxt.setTextColor(getResources().getColor(R.color.lgreen));

                        Calendar today = Calendar.getInstance();
                        today.set(Calendar.MILLISECOND, 0);
                        today.set(Calendar.SECOND, 0);
                        today.set(Calendar.MINUTE, 0);
                        today.set(Calendar.HOUR_OF_DAY, 0);
                        Date currentDate = today.getTime();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date mDate;
                        long timeInMilliseconds = 0, currentTimeToMili = 0;
                        try {
                            if (!dueDate.equals("N/A")) {
                                mDate = sdf.parse(dueDate);
                                timeInMilliseconds = mDate.getTime();
                                currentTimeToMili = currentDate.getTime();
                                System.out.println("Date in milli :: " + timeInMilliseconds);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        String billStatus1 = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        if (!TextUtils.isEmpty(billStatus1) && !billStatus1.equals("N/A")) {

                            if (billStatus1.toLowerCase().equals("partial payment")) {

                                editAmount.setText("");
                                editAmount.setFocusableInTouchMode(true);
                                editAmount.setEnabled(true);
                            } else {

                                if (currentTimeToMili <= timeInMilliseconds) {


                                    payabaleAmount.setText(amountBeforeDue);
                                    editAmount.setText(amountBeforeDue.split("\\.")[0]);
                                    editAmount.setFocusableInTouchMode(false);
                                    editAmount.setEnabled(false);


                                } else if (currentTimeToMili > timeInMilliseconds) {

                                    payabaleAmount.setText(amountAfterDue);
                                    editAmount.setText(amountAfterDue.split("\\.")[0]);
                                    editAmount.setFocusableInTouchMode(false);
                                    editAmount.setEnabled(false);

                                }

                            }
                        }
                        if (currentTimeToMili <= timeInMilliseconds) {


                            payabaleAmount.setText(amountBeforeDue);

                        } else if (currentTimeToMili > timeInMilliseconds) {

                            payabaleAmount.setText(amountAfterDue);

                        }

                    }

                    constants.destinationAccountEditor.putString("isSelect", "No");
                    constants.destinationAccountEditor.commit();
                    constants.destinationAccountEditor.apply();
                    isDestination = false;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Verification() {

        try {

            CryptLib _crypt = new CryptLib();
            Intent intent = new Intent(QRPaymentDetails.this, QRPaymentReviewDetails.class);
            intent.putExtra("userName", sourceuserName);
            intent.putExtra("balance", sourceBalance);
            intent.putExtra("accountNumber", sourceAccount);
            intent.putExtra("purpose", purposeSelected);
            intent.putExtra("currency", sourceCurrency);
            intent.putExtra("distributorAccountNumber", distributorAccountNumber);
            intent.putExtra("mobile", Constants.MERCHANTIDENTIFIER);
            intent.putExtra("remarks", remarks);
            intent.putExtra("stan", stan);
            intent.putExtra("rrn", rrn);
            intent.putExtra("amount", editRemarks5.getText().toString());
            intent.putExtra("branchCode", branchCode);
            intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            intent.putExtra("distributorName", alias);
            intent.putExtra("distributorID", distributorID);
            intent.putExtra("categoryType", categoryType);
            intent.putExtra("ftType", "QRPayment");
            intent.putExtra("transactionAmount", editRemarks5.getText().toString());
            intent.putExtra("tipAmount", editAmount4.getText().toString());
            intent.putExtra("friendNumber", friendNumber);
            intent.putExtra("messageId", messageId);
            intent.putExtra("targetId", targetId);
            intent.putExtra("companyName", Constants.pushPaymentData.getMerchantName());
            overridePendingTransition(0, 0);
            startActivity(intent);
            validateBtn.setEnabled(true);


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void showDilogForErrorQR(String msg, String header) {

        dialog1 = new Dialog(QRPaymentDetails.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        message = dialog1.findViewById(R.id.validatingTxt);
        doneBtn = dialog1.findViewById(R.id.doneBtn);
        heading = dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(true);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog1.show();


    }
}
