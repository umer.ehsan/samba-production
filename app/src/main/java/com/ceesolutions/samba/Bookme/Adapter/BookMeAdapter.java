package com.ceesolutions.samba.Bookme.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ceesolutions.samba.Bookme.Fragments.BusesFragment;
import com.ceesolutions.samba.Bookme.Fragments.EventsFragment;
import com.ceesolutions.samba.Bookme.Fragments.FlightsFragment;
import com.ceesolutions.samba.Bookme.Fragments.MoviesFragment;

public class BookMeAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public BookMeAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MoviesFragment moviesFragment = new MoviesFragment();
                return moviesFragment;
            case 1:
                BusesFragment busesFragment = new BusesFragment();
                return busesFragment;
            case 2:
                EventsFragment eventsFragment = new EventsFragment();
                return eventsFragment;
            case 3:
                FlightsFragment flightsFragment = new FlightsFragment();
                return flightsFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}