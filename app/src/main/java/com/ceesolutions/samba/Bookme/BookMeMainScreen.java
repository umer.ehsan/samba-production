package com.ceesolutions.samba.Bookme;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.Bookme.Adapter.BookMeAdapter;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class BookMeMainScreen extends AppCompatActivity {

    public static ViewPager viewPager;
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private TextView textView, doneBtn, heading;
    private ImageView backBtn, notificationBtn, helpBtn;
    private String type;
    private Constants constants;
    private boolean isFirst = false;
    private Dialog dialog, dialogPopup;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Utils utils;
    private AppPreferences appPreferences;
    private String location, stan;
    private JSONArray jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bookme_main_screen);
        getSupportActionBar().hide();
//        TextView textView = (TextView) findViewById(R.id.text);
//        textView.bringToFront();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {


//
                dialog = new Dialog(BookMeMainScreen.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_layout);
                dialog.setCancelable(false);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                requestQueue = Volley.newRequestQueue(BookMeMainScreen.this);
                constants = Constants.getConstants(BookMeMainScreen.this);
                helper = Helper.getHelper(BookMeMainScreen.this);
                utils = new Utils(BookMeMainScreen.this);
                appPreferences = new AppPreferences(BookMeMainScreen.this);

                longitude1 = appPreferences.getString("longitude", "N/A");
                latitude1 = appPreferences.getString("latitude", "N/A");
                location = appPreferences.getString("location", "N/A");

                FirebaseAnalytics.getInstance(BookMeMainScreen.this).logEvent("Bookme_Tickets", new Bundle());

                if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                    longitude = Double.valueOf(longitude1);
                    latitude = Double.valueOf(latitude1);
                    if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                    } else {
                        location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                    }
                } else {

                    longitude = 0;
                    latitude = 0;
                    location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
                }
                try {
                    CryptLib _crypt = new CryptLib();
                    backBtn = findViewById(R.id.backBtn);
                    helpBtn = findViewById(R.id.helpBtn);
                    constants = Constants.getConstants(BookMeMainScreen.this);
                    type = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    notificationBtn = findViewById(R.id.notificationBtn);
                    backBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

//                Intent intent = new Intent(BookMeMainScreen.this, HomeScreen.class);
//                overridePendingTransition(0, 0);
//                startActivity(intent);
                            finish();
                        }
                    });

                    notificationBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {


                                Intent intent = new Intent(BookMeMainScreen.this, HomeScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                                finish();
                                finishAffinity();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                finish();
                        }
                    });
                    TabLayout tabLayout = findViewById(R.id.tab_layout);
                    tabLayout.addTab(tabLayout.newTab().setText("Movies"));
                    tabLayout.addTab(tabLayout.newTab().setText("Buses"));
                    tabLayout.addTab(tabLayout.newTab().setText("Events"));
                    tabLayout.addTab(tabLayout.newTab().setText("Flights"));
                    tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                    Intent intent = getIntent();
                    viewPager = findViewById(R.id.pager);
                    final BookMeAdapter adapter = new BookMeAdapter
                            (getSupportFragmentManager(), tabLayout.getTabCount());
                    viewPager.setAdapter(adapter);
                    viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                    tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                        @Override
                        public void onTabSelected(TabLayout.Tab tab) {

                            if (!TextUtils.isEmpty(type) && type.equals("SAMBA")) {
                                viewPager.setCurrentItem(tab.getPosition());
                            } else {
                                viewPager.setCurrentItem(tab.getPosition());
                            }

                        }

                        @Override
                        public void onTabUnselected(TabLayout.Tab tab) {

                        }

                        @Override
                        public void onTabReselected(TabLayout.Tab tab) {

                        }
                    });


                    GetQRGatewayDetails();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }, 1000);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void GetQRGatewayDetails() {


        try {
            dialog.show();
            if (helper.isNetworkAvailable()) {

                try {
                    Constants.filterGatewayName = "BMOTP";
                    int stanNumber = 6;
                    String stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
                    int rrnNumber = 12;
                    String rrn = String.valueOf(Helper.generateRandom(rrnNumber));
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sSigns_LegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                    params.put("sServerSessionKey", md5);
                    params.put("sSTAN", stan);
                    params.put("sRRN", rrn);
                    params.put("sSigns_FilterGatewayName", Constants.filterGatewayName);
                    HttpsTrustManager.allowMySSL(BookMeMainScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getQRGatewayDetails,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getString("Status_Code").equals("00")) {

                                                dialog.dismiss();
                                                jsonArray = jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getJSONArray("PaymentsGatewayList");
                                                for (int i = 0; i < jsonArray.length(); i++) {

                                                    JSONObject bookMeObj = new JSONObject(jsonArray.get(i).toString());
                                                    if (bookMeObj.getString("GatewayName").equals("BMOTP")) {

                                                        if (bookMeObj.getString("Android").equals("Y")) {
                                                            Constants.GatewayName = bookMeObj.getString("GatewayName");
                                                            Constants.Currency = bookMeObj.getString("Currency");
                                                            Constants.CollectionAccount = bookMeObj.getString("CollectionAccount");
                                                        } else {
                                                            showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        }
                                                        break;
                                                    }
                                                }
//                                                PPIntentIntegrator integrator = new PPIntentIntegrator(MainActivity.this);
//                                                integrator.setOrientationLocked(false);
//                                                integrator.setBeepEnabled(false);
//                                                cameraScanStartTime = System.nanoTime();
//                                                integrator.initiateScan();

                                            } else if (jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getString("Status_Code").equals("54")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getString("Status_Description"), "ERROR", BookMeMainScreen.this, new LoginScreen());


                                            } else {

                                                dialog.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getString("Status_Description"), "ERROR");
                                            }

                                        } else {

                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sSigns_LegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                            params.put("sSigns_CustomerFeedBack", feedBack);
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            dialog.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }


    }

    public void showDilogForError(String msg, String header) {

        try {
            dialogPopup = new Dialog(BookMeMainScreen.this);
            dialogPopup.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogPopup.setContentView(R.layout.error_dialog);
            textView = dialog.findViewById(R.id.validatingTxt);
            doneBtn = dialog.findViewById(R.id.doneBtn);
            heading = dialog.findViewById(R.id.heading);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    finish();

                }
            });
            textView.setText(msg);
            heading.setText(header);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}