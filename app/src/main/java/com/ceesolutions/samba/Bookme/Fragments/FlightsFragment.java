package com.ceesolutions.samba.Bookme.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.Bookme.BookMeDetails;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class FlightsFragment extends Fragment {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private Constants constants;
    private WebView mWebView;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Dialog pd;
    private Utils utils;
    private AppPreferences appPreferences;
    private String location;


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.movies_fragment, container, false);

        initViews(convertView);


        return convertView;
    }

    public void initViews(View convertView) {

        try {
            mWebView = convertView.findViewById(R.id.webview);
            constants = Constants.getConstants(getActivity());
            utils = new Utils(getActivity());
            requestQueue = Volley.newRequestQueue(getActivity());
            helper = Helper.getHelper(getActivity());
            constants = Constants.getConstants(getActivity());
            pd = new Dialog(getActivity());
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            appPreferences = new AppPreferences(getActivity());
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");

            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }


            GetMoviesURL();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            try {

                CryptLib _crypt = new CryptLib();

                constants = Constants.getConstants(getActivity());
                String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                if (!TextUtils.isEmpty(userType) && userType.equals("NON_SAMBA")) {

//                    isVisible = true;
                    onAttach(getActivity());


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            //Do Nothing
        }
    }

    public void GetMoviesURL() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    pd.show();
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sCNIC", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                    params.put("sEmail", _crypt.decrypt(constants.sharedPreferences.getString("EmailAddress", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                    params.put("sPhone_Number", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                    params.put("sWidget_Type", "flights");
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(getActivity());

                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getUrlForMoviesURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("GetWidgetLink_Result").getString("Status_Code").equals("00")) {


//                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("GetWidgetLink_Result").getString("POTList"));
//                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                try {

                                                    String flightsUrl = jsonObject.getJSONObject("GetWidgetLink_Result").getString("url");


                                                    // Enable Javascript
                                                    mWebView.getSettings().setJavaScriptEnabled(true);
                                                    mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                                                    mWebView.addJavascriptInterface(new WebAppInterface(getActivity()), "controlTransfer");
                                                    mWebView.addJavascriptInterface(new WebAppInterface(getActivity()), "keepAlive");
                                                    mWebView.setWebViewClient(new WebViewClient() {
                                                        public void onReceivedSslError(WebView webView, SslErrorHandler handler, SslError error) {
                                                            handler.proceed();
                                                        }

                                                        @Override
                                                        public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                                            if (checkForGoogleMaps(url))
                                                                return true;
                                                            view.loadUrl(url);
                                                            return true;
                                                        }
                                                    });

                                                    mWebView.loadUrl(flightsUrl);
                                                    Handler handler = new Handler();
                                                    handler.postDelayed(new Runnable() {
                                                        public void run() {
                                                            pd.dismiss();
                                                        }
                                                    }, 1000);


                                                } catch (Exception ex) {
                                                    ex.printStackTrace();
                                                    pd.dismiss();
                                                }

//                                                }


                                            } else if (jsonObject.getJSONObject("GetWidgetLink_Result").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("GetWidgetLink_Result").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());


                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("GetWidgetLink_Result").getString("Status_Description"), "ERROR");

                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");

                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sPurposeGroupCode", "BP");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");

            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }

    public void KeepAlive() {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(getActivity());

                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.bookMeAlive,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);



                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sPurposeGroupCode", "BP");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();

                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();

        }
    }

    public class WebAppInterface {

        public Context mContext;

        WebAppInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void postMessage(String json) {
            Log.v("", "message ----" + json);

            try {


                JSONObject jsonObject = new JSONObject(json);
                if (jsonObject.has("USERNAME") && jsonObject.has("URL") && jsonObject.has("TYPE")) {
                    KeepAlive();
                } else {

                    FirebaseAnalytics.getInstance(getContext()).logEvent("Bookme_Button_Pressed_" + jsonObject.getString("TYPE"), new Bundle());

                    Constants.OrderDetails = json;
                    startActivity(new Intent(getActivity(), BookMeDetails.class));
                    getActivity().finish();
                }
//            return new JSONObject();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkForGoogleMaps(String url) {

        try {

            if (url.contains("https://www.google.com/maps/")) {
                try {
                    Uri IntentUri = Uri.parse(url);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, IntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");

                    if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
                        startActivity(mapIntent);
                    } else {
                        Toast.makeText(getContext(), "Please check if google maps is installed on you device", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

}