package com.ceesolutions.samba.Bookme;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;

import com.android.volley.VolleyLog;
import com.ceesolutions.samba.eatMubarak.EatMubarakDetailsReview;
import com.ceesolutions.samba.eatMubarak.EatMubarkDetails;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.transactions.SelectAccountScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class BookMeDetails extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    String res = "", res2 = "";
    String transactionAmountInteger;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private ExpandableLayout expandableLayout0;
    private CardView cardView;
    private Constants constants;
    private Dialog pd;
    private String remarks, amount, type;
    private TextInputLayout amountTxtInput4, amountTxtInput, remarksTxtInput5;
    private EditText editAmount, editRemarks, payabaleAmount, editAmount4, editRemarks5;
    private RelativeLayout upperPart1, upperPart2;
    private TextView consumerIDTxt, consumerNumberTxt, billingTxt, billingMonthTxt, dueDateTxt, payableBeforeTxt, payableAfterTxt, statusTxt, mainTxt, errorMessageForAmount, errorMessageForRemarks, errorMessageForSpinner, userNameTxt, accMaskTxt, amountTxt, friendNameTxt, itemName, validateBtn;
    private TextView additionalTxt, conversionHeading, chargesHeading, conversionHeading2, chargesHeading2, conversionHeading3, payableAfterTxt4, chargesHeading3, statusTxt4, conversionHeading4, payableAfterTxt5, chargesHeading4, statusTxt5;
    private Spinner purposesSpinner;
    private String purposeSelected;
    private String[] purpose;
    private ImageView arrowDestination, backBtn;
    private RelativeLayout conversion4, conversion3;
    private FrameLayout relativeLayout4, relativeLayout5;
    private ArrayList<String> purpStringArrayList;
    private String sourceBalance, sourceuserName, sourceAccount, sourceCurrency, distributorID, distributorAccountNumber, mobileNumber, distributorName, length;
    private Utils utils;
    private boolean isSource = false;
    private boolean isDestination = false;
    private boolean isFirst = false;
    private String identityValue, alias, stan, rrn;
    private String beneType = "SAMBA";
    private TextView doneBtn, textView, heading;
    private Dialog dialog;
    private String minAmount, categoryType, billConsumerNumber, sourceminAmount, amountAfterDue, amountBeforeDue, billStatus, billingMonth, dueDate, consumerName, companyName;
    private String maxAmount, branchCode, mobile, email, messageId, targetId, friendNumber;
    private float amountToInt, maxAmountToInt, minAmountToInt;
    private int bal;
    private boolean flag = false;
    private Intent intent1;
    private String isSelectSource, isSelectDestination;
    private boolean isVisible = false;
    private AppPreferences appPreferences;
    private String location;
    private View dueView4;
    private boolean isPostpaid = false;
    private JSONObject jsonObject;
    private TextView headingTxt;

    public static String toTitleCase(String str) {

        if (str == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }

        return builder.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.bookme_review);
        getSupportActionBar().hide();

        initViews();


        editRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                remarks = editable.toString();
            }
        });


        editAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                res = charSequence.toString();

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                res = charSequence.toString();

                String tipIndicator = Constants.pushPaymentData.getTipOrConvenienceIndicator();
                Double tipFixed = Constants.pushPaymentData.getValueOfConvenienceFeeFixed();
                Double tipPercentage = Constants.pushPaymentData.getValueOfConvenienceFeePercentage();


                Double amount = Double.valueOf(res);

                if (tipIndicator != null && !TextUtils.isEmpty(tipIndicator)) {
                    if (tipIndicator.equals("01")) {
                        Float tip = Float.valueOf(editAmount4.getText().toString());
                        amount = amount + tip;

                    } else if (tipIndicator.equals("02")) {
                        if (tipFixed != null && tipFixed != 0) {
                            Double tipAmount = (tipFixed);
                            amount = amount + tipAmount;

                        } else {
                            Double tipFixedInput = Double.valueOf(editAmount4.getText().toString());
                            if (tipFixedInput != null && tipFixedInput != 0) {
                                Double tipAmount = tipFixedInput;
                                amount = amount + tipAmount;

                            }
                        }
                    } else if (tipIndicator == "03") {
                        if (tipPercentage != null && tipPercentage != 0) {
                            Double tipAmount = tipPercentage;
                            amount = amount + (amount * tipAmount / 100);

                        }
                    }
                }


                transactionAmountInteger = String.valueOf(amount);
                editRemarks5.setText(transactionAmountInteger);
            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });

        editAmount4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                res2 = charSequence.toString();

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                Double paymentAmount = Double.valueOf(editAmount.getText().toString());
                res2 = charSequence.toString();
                Double amount = Double.valueOf(res2);
                Double totalAmount = amount + paymentAmount;
                transactionAmountInteger = String.valueOf(totalAmount);
                editRemarks5.setText(transactionAmountInteger);


            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });


        upperPart1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                upperPart1.setEnabled(false);
                isSource = true;
                isDestination = false;
                isFirst = true;
                Intent intent = new Intent(BookMeDetails.this, SelectAccountScreen.class);
                intent.putExtra("source", "source");
                startActivity(intent);


            }
        });

        upperPart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                upperPart2.setEnabled(false);
//                Intent intent = new Intent(QRPaymentDetails.this, ManageBillerScreen.class);
//                intent.putExtra("source", "destination");
//                startActivity(intent);
//                isDestination = true;
//                isSource = false;
//                isFirst = true;


            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateBtn.setEnabled(false);


                try {
                    FirebaseAnalytics.getInstance(BookMeDetails.this).logEvent("Bookme_Review_Btn_Pressed_"+ jsonObject.getString("TYPE") , new Bundle());
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if ((TextUtils.isEmpty(sourceAccount) || sourceAccount.equals("N/A")) || (TextUtils.isEmpty(sourceBalance) || sourceBalance.equals("N/A")) || (TextUtils.isEmpty(sourceuserName) || sourceuserName.equals("N/A"))) {

                    utils.showDilogForError("Please select Source account.", "WARNING");
                    validateBtn.setEnabled(true);

                }
//                else {
//                    if (!sourceCurrency.equals("PKR")) {
//
//                        utils.showDilogForError("E-Tickets not available for FCY accounts.", "ERROR");
//                        validateBtn.setEnabled(true);
//
//                    }
                    else {
                        if (TextUtils.isEmpty(payabaleAmount.getText().toString()) || payabaleAmount.getText().toString().equals("0.0") || editRemarks5.getText().toString().equals("0")) {

                            utils.showDilogForError("Please enter valid amount.", "ERROR");
                            validateBtn.setEnabled(true);
                        } else {

                            //Verification();
                            getTitleFetch();
                        }

                    }

                }
        });


    }

    public void initViews() {

        try {

            CryptLib _crypt = new CryptLib();
            purposesSpinner = findViewById(R.id.purposeSpinner);
            validateBtn = findViewById(R.id.validateBtn);
            arrowDestination = findViewById(R.id.arrowDestination);
            consumerIDTxt = findViewById(R.id.consumerIDTxt);
            dueDateTxt = findViewById(R.id.dueDateTxt);
            billingMonthTxt = findViewById(R.id.billingMonthTxt);
            consumerNumberTxt = findViewById(R.id.consumerNumberTxt);
            billingTxt = findViewById(R.id.billingTxt);
            payableAfterTxt = findViewById(R.id.payableAfterTxt);
            payableBeforeTxt = findViewById(R.id.payableBeforeTxt);
            statusTxt = findViewById(R.id.statusTxt);
            dueView4 = findViewById(R.id.dueView4);
            upperPart1 = findViewById(R.id.upperPart1);
            upperPart2 = findViewById(R.id.upperPart2);
            constants = Constants.getConstants(BookMeDetails.this);
            friendNameTxt = findViewById(R.id.friendNameTxt);
            payabaleAmount = findViewById(R.id.payabaleAmount);
            remarksTxtInput5 = findViewById(R.id.remarksTxtInput5);
            cardView = findViewById(R.id.card_view);
            userNameTxt = findViewById(R.id.userNameTxt);
            userNameTxt.setText("Select Source Account");
            accMaskTxt = findViewById(R.id.accMaskTxt);
            accMaskTxt.setVisibility(View.INVISIBLE);
            amountTxt = findViewById(R.id.amountTxt);
            amountTxt.setVisibility(View.INVISIBLE);
            itemName = findViewById(R.id.itemName);
            editRemarks5 = findViewById(R.id.editRemarks5);
            amountTxtInput4 = findViewById(R.id.amountTxtInput4);
            amountTxtInput = findViewById(R.id.amountTxtInput);
            editAmount4 = findViewById(R.id.editAmount4);
            relativeLayout4 = findViewById(R.id.relativeLayout4);
            conversion4 = findViewById(R.id.conversion4);
            conversion3 = findViewById(R.id.conversion3);
            backBtn = findViewById(R.id.backBtn);
            headingTxt = findViewById(R.id.headingTxt);
            relativeLayout5 = findViewById(R.id.relativeLayout5);
            conversionHeading = findViewById(R.id.conversionHeading);
            jsonObject = new JSONObject(Constants.OrderDetails);
            chargesHeading = findViewById(R.id.chargesHeading);
            headingTxt.setText(toTitleCase(jsonObject.getString("TYPE").toUpperCase() + " Tickets"));
            conversionHeading2 = findViewById(R.id.conversionHeading2);
            chargesHeading2 = findViewById(R.id.chargesHeading2);
            if (jsonObject.getString("TYPE").equals("airline")) {
                conversionHeading2.setText("Flight From");
                chargesHeading.setText("Ticket Date");
                conversionHeading.setText("Order ID");
                chargesHeading2.setText("Flight To");
//                Constants.BookingDetail = jsonObject.getString("cnic") + "|" + jsonObject.getString("name") + "|" + jsonObject.getString("phone_number") + "|" + jsonObject.getString("email");
                itemName.setText(jsonObject.getString("SERVICE_PROVIDER"));
                payabaleAmount.setText(jsonObject.getString("AMOUNT") + " PKR");
                billingMonthTxt.setText(jsonObject.getJSONObject("ORDER_DETAILS").getString("initial_departure_datetime"));
                dueDateTxt.setText(jsonObject.getJSONObject("ORDER_DETAILS").getString("flight_from"));
                payableBeforeTxt.setText(jsonObject.getJSONObject("ORDER_DETAILS").getString("flight_to"));
                billingTxt.setText(jsonObject.getString("ORDER_REF_ID"));
                additionalTxt = findViewById(R.id.additionalTxt);
                conversionHeading3 = findViewById(R.id.conversionHeading3);
                conversionHeading3.setText("Ticket Class");
                payableAfterTxt4 = findViewById(R.id.payableAfterTxt);
                payableAfterTxt.setText((jsonObject.getJSONObject("ORDER_DETAILS").getString("class")));
                chargesHeading3 = findViewById(R.id.chargesHeading3);
                chargesHeading3.setText("No of Seat(s)");
                statusTxt4 = findViewById(R.id.statusTxt);
                int totalSeats = Integer.valueOf(jsonObject.getJSONObject("ORDER_DETAILS").getString("total_adults")) + Integer.valueOf(jsonObject.getJSONObject("ORDER_DETAILS").getString("total_children")) + Integer.valueOf(jsonObject.getJSONObject("ORDER_DETAILS").getString("total_infants"));
                statusTxt4.setText((totalSeats) + " Seat(s)");
                conversionHeading4 = findViewById(R.id.conversionHeading4);
                payableAfterTxt5 = findViewById(R.id.payableAfterTxt4);
                chargesHeading4 = findViewById(R.id.chargesHeading4);
                statusTxt5 = findViewById(R.id.statusTxt4);
                errorMessageForAmount = findViewById(R.id.errorMessageForAmount);
                errorMessageForRemarks = findViewById(R.id.errorMessageForRemarks);
                errorMessageForSpinner = findViewById(R.id.errorMessageForSpinner);
                editRemarks = findViewById(R.id.editRemarks);
                editAmount = findViewById(R.id.editAmount);
                expandableLayout0 = findViewById(R.id.expandable_layout_0);
                requestQueue = Volley.newRequestQueue(BookMeDetails.this);
                mainTxt = findViewById(R.id.mainTxt);
                Constants.BookingDetail = jsonObject.getJSONObject("ORDER_DETAILS").getString("cnic") + "|" + jsonObject.getJSONObject("ORDER_DETAILS").getString("name") + "|" + jsonObject.getJSONObject("ORDER_DETAILS").getString("phone_number") + "|" + jsonObject.getJSONObject("ORDER_DETAILS").getString("email");
            }
            if (jsonObject.getString("TYPE").equals("transport")) {
                conversionHeading2.setText("Departure");
                chargesHeading.setText("Ticket Date");
                conversionHeading.setText("Order ID");
                chargesHeading2.setText("Arrival");
                itemName.setText(jsonObject.getString("SERVICE_PROVIDER"));
                payabaleAmount.setText(jsonObject.getString("AMOUNT"));
                billingMonthTxt.setText(jsonObject.getJSONObject("ORDER_DETAILS").getString("dep_date"));
                if (jsonObject.getJSONObject("ORDER_DETAILS").has("departure_city"))
                    dueDateTxt.setText(jsonObject.getJSONObject("ORDER_DETAILS").getJSONObject("departure_city").getString("city_name"));
                else
                    dueDateTxt.setText("-");
                if (jsonObject.getJSONObject("ORDER_DETAILS").has("arrival_city"))
                    payableBeforeTxt.setText(jsonObject.getJSONObject("ORDER_DETAILS").getJSONObject("arrival_city").getString("city_name"));
                else
                    payableBeforeTxt.setText("-");
                billingTxt.setText(jsonObject.getString("ORDER_REF_ID"));
                additionalTxt = findViewById(R.id.additionalTxt);
                conversionHeading3 = findViewById(R.id.conversionHeading3);
                conversionHeading3.setText("No of Seat(s)");
                payableAfterTxt4 = findViewById(R.id.payableAfterTxt);
                int totalSeats = Integer.valueOf(jsonObject.getJSONObject("ORDER_DETAILS").getString("number_of_seats"));
                payableAfterTxt.setText((totalSeats) + " Seat(s)");
                chargesHeading3 = findViewById(R.id.chargesHeading3);
                chargesHeading3.setText("Payment Status");
                statusTxt4 = findViewById(R.id.statusTxt);
                statusTxt4.setText(jsonObject.getJSONObject("ORDER_DETAILS").getString("payment").toUpperCase());
                conversionHeading4 = findViewById(R.id.conversionHeading4);
                payableAfterTxt5 = findViewById(R.id.payableAfterTxt4);
                chargesHeading4 = findViewById(R.id.chargesHeading4);
                statusTxt5 = findViewById(R.id.statusTxt4);
                errorMessageForAmount = findViewById(R.id.errorMessageForAmount);
                errorMessageForRemarks = findViewById(R.id.errorMessageForRemarks);
                errorMessageForSpinner = findViewById(R.id.errorMessageForSpinner);
                editRemarks = findViewById(R.id.editRemarks);
                editAmount = findViewById(R.id.editAmount);
                expandableLayout0 = findViewById(R.id.expandable_layout_0);
                requestQueue = Volley.newRequestQueue(BookMeDetails.this);
                mainTxt = findViewById(R.id.mainTxt);
                Constants.BookingDetail = jsonObject.getJSONObject("ORDER_DETAILS").getString("cnic") + "|" + jsonObject.getJSONObject("ORDER_DETAILS").getString("name") + "|" + jsonObject.getJSONObject("ORDER_DETAILS").getString("contact_no") + "|" + jsonObject.getJSONObject("ORDER_DETAILS").getString("email");
            }
            if (jsonObject.getString("TYPE").equals("event")) {
                conversionHeading2.setText("City");
                chargesHeading.setText("Event Date");
                conversionHeading.setText("Order ID");
                chargesHeading2.setText("No of Ticket(s)");
                itemName.setText(jsonObject.getString("SERVICE_PROVIDER"));
                payabaleAmount.setText(jsonObject.getString("AMOUNT"));
                billingMonthTxt.setText(jsonObject.getJSONObject("ORDER_DETAILS").getString("event_date"));
                if (jsonObject.getJSONObject("ORDER_DETAILS").has("event_name")) {
                    if (jsonObject.getJSONObject("ORDER_DETAILS").getJSONObject("event_name").has("city")) {
                        dueDateTxt.setText(((jsonObject.getJSONObject("ORDER_DETAILS").getJSONObject("event_name").getJSONObject("city").getString("city_name").equals("")) ? "-" : jsonObject.getJSONObject("ORDER_DETAILS").getJSONObject("event_name").getJSONObject("city").getString("city_name")));
                    } else {
                        dueDateTxt.setText("-");
                    }

                } else {
                    dueDateTxt.setText("-");
                }

                payableBeforeTxt.setText(Integer.valueOf(jsonObject.getJSONObject("ORDER_DETAILS").getString("seats")) + " Ticket(s)");
                billingTxt.setText(jsonObject.getString("ORDER_REF_ID"));
                additionalTxt = findViewById(R.id.additionalTxt);
                conversionHeading3 = findViewById(R.id.conversionHeading3);
                conversionHeading3.setText("Venue");
                payableAfterTxt4 = findViewById(R.id.payableAfterTxt);
                payableAfterTxt.setText(jsonObject.getJSONObject("ORDER_DETAILS").getJSONObject("event_name").getString("venue"));
                chargesHeading3 = findViewById(R.id.chargesHeading3);
                chargesHeading3.setText("Duration");
                statusTxt4 = findViewById(R.id.statusTxt);
                statusTxt4.setText(jsonObject.getJSONObject("ORDER_DETAILS").getJSONObject("event_name").getString("duration").toUpperCase());
                conversionHeading4 = findViewById(R.id.conversionHeading4);
                payableAfterTxt5 = findViewById(R.id.payableAfterTxt4);
                chargesHeading4 = findViewById(R.id.chargesHeading4);
                statusTxt5 = findViewById(R.id.statusTxt4);
                errorMessageForAmount = findViewById(R.id.errorMessageForAmount);
                errorMessageForRemarks = findViewById(R.id.errorMessageForRemarks);
                errorMessageForSpinner = findViewById(R.id.errorMessageForSpinner);
                editRemarks = findViewById(R.id.editRemarks);
                editAmount = findViewById(R.id.editAmount);
                expandableLayout0 = findViewById(R.id.expandable_layout_0);
                requestQueue = Volley.newRequestQueue(BookMeDetails.this);
                mainTxt = findViewById(R.id.mainTxt);
                Constants.BookingDetail = jsonObject.getJSONObject("ORDER_DETAILS").getString("cnic") + "|" + jsonObject.getJSONObject("ORDER_DETAILS").getString("name") + "|" + jsonObject.getJSONObject("ORDER_DETAILS").getString("contact_no") + "|" + jsonObject.getJSONObject("ORDER_DETAILS").getString("email");
            }
            if (jsonObject.getString("TYPE").equals("cinema")) {
                conversionHeading2.setText("Movie");
                chargesHeading.setText("Show Time");
                conversionHeading.setText("Order ID");
                chargesHeading2.setText("Seat(s)");
                itemName.setText(jsonObject.getString("SERVICE_PROVIDER"));
                payabaleAmount.setText(jsonObject.getString("AMOUNT"));
                billingMonthTxt.setText(jsonObject.getJSONObject("ORDER_DETAILS").getString("show_time"));
                dueDateTxt.setText(((jsonObject.getJSONObject("ORDER_DETAILS").has("cinema_movie")) ? jsonObject.getJSONObject("ORDER_DETAILS").getJSONObject("cinema_movie").getString("title") : "-"));
                payableBeforeTxt.setText(jsonObject.getJSONObject("ORDER_DETAILS").getString("number_of_seats") + " Seat(s)");
                billingTxt.setText(jsonObject.getString("ORDER_REF_ID"));
                additionalTxt = findViewById(R.id.additionalTxt);
                conversionHeading3 = findViewById(R.id.conversionHeading3);
                conversionHeading3.setText("Ticket Number");
                payableAfterTxt4 = findViewById(R.id.payableAfterTxt);
                payableAfterTxt.setText(jsonObject.getJSONObject("ORDER_DETAILS").getString("ticket_no"));
                chargesHeading3 = findViewById(R.id.chargesHeading3);
                chargesHeading3.setText("Seat Number(s)");
                statusTxt4 = findViewById(R.id.statusTxt);
                statusTxt4.setText(jsonObject.getJSONObject("ORDER_DETAILS").getString("seat_numbers"));
                conversionHeading4 = findViewById(R.id.conversionHeading4);
                payableAfterTxt5 = findViewById(R.id.payableAfterTxt4);
                chargesHeading4 = findViewById(R.id.chargesHeading4);
                statusTxt5 = findViewById(R.id.statusTxt4);
                errorMessageForAmount = findViewById(R.id.errorMessageForAmount);
                errorMessageForRemarks = findViewById(R.id.errorMessageForRemarks);
                errorMessageForSpinner = findViewById(R.id.errorMessageForSpinner);
                editRemarks = findViewById(R.id.editRemarks);
                editAmount = findViewById(R.id.editAmount);
                expandableLayout0 = findViewById(R.id.expandable_layout_0);
                requestQueue = Volley.newRequestQueue(BookMeDetails.this);
                mainTxt = findViewById(R.id.mainTxt);
                Constants.BookingDetail = jsonObject.getJSONObject("ORDER_DETAILS").getString("cnic") + "|" + jsonObject.getJSONObject("ORDER_DETAILS").getString("name") + "|" + jsonObject.getJSONObject("ORDER_DETAILS").getString("contact_no") + "|" + jsonObject.getJSONObject("ORDER_DETAILS").getString("email");
            }


            helper = Helper.getHelper(BookMeDetails.this);
            constants = Constants.getConstants(BookMeDetails.this);
            pd = new Dialog(BookMeDetails.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);

            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onBackPressed();
                }
            });


            GetPurposeBillList();
//            showSpinner();

//            String billNumber = Constants.pushPaymentData.getAdditionalData().getBillNumber();
//            String mobileNumber = Constants.pushPaymentData.getAdditionalData().getMobileNumber();
//            String storeid = Constants.pushPaymentData.getAdditionalData().getStoreId();
//            String loyaltyNumber = Constants.pushPaymentData.getAdditionalData().getLoyaltyNumber();
//            String referenceId = Constants.pushPaymentData.getAdditionalData().getReferenceId();
//            String consumerId = Constants.pushPaymentData.getAdditionalData().getConsumerId();
//            String terminalId = Constants.pushPaymentData.getAdditionalData().getTerminalId();
//            String purposeOfTrans = Constants.pushPaymentData.getAdditionalData().getPurpose();
//            String consumerData = Constants.pushPaymentData.getAdditionalData().getAdditionalDataRequest();
//            String additionalDataString = String.valueOf(Constants.pushPaymentData.getValue("62"));
//            int count = 0;
//            if (!TextUtils.isEmpty(billNumber) && !billNumber.equals(null)) {
//                conversionHeading3.setText("Bill Number");
//                payableAfterTxt4.setText(billNumber);
//                additionalTxt.setVisibility(View.VISIBLE);
//                conversionHeading3.setVisibility(View.VISIBLE);
//                payableAfterTxt4.setVisibility(View.VISIBLE);
//                count++;
//            }
//
//            if (!TextUtils.isEmpty(mobileNumber) && !mobileNumber.equals(null)) {
//                if (count == 0) {
//                    conversionHeading3.setText("Mobile Number");
//                    payableAfterTxt4.setText(mobileNumber);
//                    additionalTxt.setVisibility(View.VISIBLE);
//                    conversionHeading3.setVisibility(View.VISIBLE);
//                    payableAfterTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 1) {
//
//                    chargesHeading3.setText("Mobile Number");
//                    statusTxt4.setText(mobileNumber);
//                    chargesHeading3.setVisibility(View.VISIBLE);
//                    statusTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                }
//            }
//
//            if (!TextUtils.isEmpty(storeid) && !storeid.equals(null)) {
//                if (count == 0) {
//                    conversionHeading3.setText("Store ID");
//                    payableAfterTxt4.setText(storeid);
//                    additionalTxt.setVisibility(View.VISIBLE);
//                    conversionHeading3.setVisibility(View.VISIBLE);
//                    payableAfterTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 1) {
//
//                    chargesHeading3.setText("Store ID");
//                    statusTxt4.setText(storeid);
//                    chargesHeading3.setVisibility(View.VISIBLE);
//                    statusTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 2) {
//
//                    conversionHeading4.setText("Store ID");
//                    payableAfterTxt5.setText(storeid);
//                    dueView4.setVisibility(View.VISIBLE);
//                    conversionHeading4.setVisibility(View.VISIBLE);
//                    payableAfterTxt5.setVisibility(View.VISIBLE);
//                    count++;
//                }
//            }
//
//
//            if (!TextUtils.isEmpty(loyaltyNumber) && !loyaltyNumber.equals(null)) {
//                if (count == 0) {
//                    conversionHeading3.setText("Loyalty Number");
//                    payableAfterTxt4.setText(loyaltyNumber);
//                    payableAfterTxt4.setText(storeid);
//                    additionalTxt.setVisibility(View.VISIBLE);
//                    conversionHeading3.setVisibility(View.VISIBLE);
//                    payableAfterTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 1) {
//
//                    chargesHeading3.setText("Loyalty Number");
//                    statusTxt4.setText(loyaltyNumber);
//                    chargesHeading3.setVisibility(View.VISIBLE);
//                    statusTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 2) {
//
//                    conversionHeading4.setText("Loyalty Number");
//                    payableAfterTxt5.setText(loyaltyNumber);
//                    dueView4.setVisibility(View.VISIBLE);
//                    conversionHeading4.setVisibility(View.VISIBLE);
//                    payableAfterTxt5.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 3) {
//
//                    chargesHeading4.setText("Loyalty Number");
//                    statusTxt5.setText(loyaltyNumber);
//                    chargesHeading4.setVisibility(View.VISIBLE);
//                    statusTxt5.setVisibility(View.VISIBLE);
//                    count++;
//                }
//            }
//
//
//            if (!TextUtils.isEmpty(referenceId) && !referenceId.equals(null)) {
//                if (count == 0) {
//                    conversionHeading3.setText("Reference ID");
//                    payableAfterTxt4.setText(referenceId);
//                    additionalTxt.setVisibility(View.VISIBLE);
//                    conversionHeading3.setVisibility(View.VISIBLE);
//                    payableAfterTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 1) {
//
//                    chargesHeading3.setText("Reference ID");
//                    statusTxt4.setText(referenceId);
//                    chargesHeading3.setVisibility(View.VISIBLE);
//                    statusTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 2) {
//
//                    conversionHeading4.setText("Reference ID");
//                    payableAfterTxt5.setText(referenceId);
//                    dueView4.setVisibility(View.VISIBLE);
//                    conversionHeading4.setVisibility(View.VISIBLE);
//                    payableAfterTxt5.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 3) {
//
//                    chargesHeading4.setText("Reference ID");
//                    statusTxt5.setText(referenceId);
//                    chargesHeading4.setVisibility(View.VISIBLE);
//                    statusTxt5.setVisibility(View.VISIBLE);
//                    count++;
//                }
//            }
//
//
//            if (!TextUtils.isEmpty(consumerId) && !consumerId.equals(null)) {
//                if (count == 0) {
//                    conversionHeading3.setText("Consumer ID");
//                    payableAfterTxt4.setText(consumerId);
//                    additionalTxt.setVisibility(View.VISIBLE);
//                    conversionHeading3.setVisibility(View.VISIBLE);
//                    payableAfterTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 1) {
//
//                    chargesHeading3.setText("Consumer ID");
//                    statusTxt4.setText(consumerId);
//                    chargesHeading3.setVisibility(View.VISIBLE);
//                    statusTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 2) {
//
//                    conversionHeading4.setText("Consumer ID");
//                    payableAfterTxt5.setText(consumerId);
//                    dueView4.setVisibility(View.VISIBLE);
//                    conversionHeading4.setVisibility(View.VISIBLE);
//                    payableAfterTxt5.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 3) {
//
//                    chargesHeading4.setText("Consumer ID");
//                    statusTxt5.setText(consumerId);
//                    chargesHeading4.setVisibility(View.VISIBLE);
//                    statusTxt5.setVisibility(View.VISIBLE);
//                    count++;
//                }
//            }
//
//            if (!TextUtils.isEmpty(terminalId) && !terminalId.equals(null)) {
//                if (count == 0) {
//                    conversionHeading3.setText("Terminal ID");
//                    payableAfterTxt4.setText(terminalId);
//                    additionalTxt.setVisibility(View.VISIBLE);
//                    conversionHeading3.setVisibility(View.VISIBLE);
//                    payableAfterTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 1) {
//
//                    chargesHeading3.setText("Terminal ID");
//                    statusTxt4.setText(terminalId);
//                    chargesHeading3.setVisibility(View.VISIBLE);
//                    statusTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 2) {
//
//                    conversionHeading4.setText("Terminal ID");
//                    payableAfterTxt5.setText(terminalId);
//                    dueView4.setVisibility(View.VISIBLE);
//                    conversionHeading4.setVisibility(View.VISIBLE);
//                    payableAfterTxt5.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 3) {
//
//                    chargesHeading4.setText("Terminal ID");
//                    statusTxt5.setText(terminalId);
//                    chargesHeading4.setVisibility(View.VISIBLE);
//                    statusTxt5.setVisibility(View.VISIBLE);
//                    count++;
//                }
//            }
//
//
//            if (!TextUtils.isEmpty(consumerData) && !consumerData.equals(null)) {
//                if (count == 0) {
//                    conversionHeading3.setText("Consumer Data");
//                    payableAfterTxt4.setText(consumerData);
//                    additionalTxt.setVisibility(View.VISIBLE);
//                    conversionHeading3.setVisibility(View.VISIBLE);
//                    payableAfterTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 1) {
//
//                    chargesHeading3.setText("Consumer Data");
//                    statusTxt4.setText(consumerData);
//                    chargesHeading3.setVisibility(View.VISIBLE);
//                    statusTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 2) {
//
//                    conversionHeading4.setText("Consumer Data");
//                    payableAfterTxt5.setText(consumerData);
//                    dueView4.setVisibility(View.VISIBLE);
//                    conversionHeading4.setVisibility(View.VISIBLE);
//                    payableAfterTxt5.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 3) {
//
//                    chargesHeading4.setText("Consumer Data");
//                    statusTxt5.setText(consumerData);
//                    chargesHeading4.setVisibility(View.VISIBLE);
//                    statusTxt5.setVisibility(View.VISIBLE);
//                    count++;
//                }
//            }
//
//            if (!TextUtils.isEmpty(additionalDataString) && !additionalDataString.equals(null)) {
//                if (count == 0) {
//                    conversionHeading3.setText("Additional Data");
//                    payableAfterTxt4.setText(additionalDataString);
//                    additionalTxt.setVisibility(View.VISIBLE);
//                    conversionHeading3.setVisibility(View.VISIBLE);
//                    payableAfterTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 1) {
//
//                    chargesHeading3.setText("Additional Data");
//                    statusTxt4.setText(additionalDataString);
//                    chargesHeading3.setVisibility(View.VISIBLE);
//                    statusTxt4.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 2) {
//
//                    conversionHeading4.setText("Additional Data");
//                    payableAfterTxt5.setText(additionalDataString);
//                    dueView4.setVisibility(View.VISIBLE);
//                    conversionHeading4.setVisibility(View.VISIBLE);
//                    payableAfterTxt5.setVisibility(View.VISIBLE);
//                    count++;
//                } else if (count == 3) {
//
//                    chargesHeading4.setText("Additional Data");
//                    statusTxt5.setText(additionalDataString);
//                    chargesHeading4.setVisibility(View.VISIBLE);
//                    statusTxt5.setVisibility(View.VISIBLE);
//                    count++;
//                }
//            }
//
//            if (!TextUtils.isEmpty(purposeOfTrans) && !purposeOfTrans.equals(null)) {
//                spinnerAdapter purposeAdapter = new spinnerAdapter(BookMeDetails.this, R.layout.custom_textview_fp);
//                purposeAdapter.addAll(purpose);
//                purposeAdapter.add("Purpose of Payment");
//                purposesSpinner.setAdapter(purposeAdapter);
//                purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(purposeOfTrans));
//                purposesSpinner.setClickable(false);
//            }
//
//            String QRTipIndicator = Constants.pushPaymentData.getTipOrConvenienceIndicator();
//            String tipIndicator = Constants.pushPaymentData.getTipOrConvenienceIndicator();
//            Double tipFixed = Constants.pushPaymentData.getValueOfConvenienceFeeFixed();
//            Double tipPercentage = Constants.pushPaymentData.getValueOfConvenienceFeePercentage();
//            boolean isPercentage = false;
//            Double transactionAmountInteger = Double.valueOf(0);
//            Double transactionAmount = Constants.pushPaymentData.getTransactionAmount();
//
//
//            if (transactionAmount != (null) && transactionAmount != 0) {
//                editAmount.setFocusableInTouchMode(false);
//                editAmount.setText(String.valueOf(transactionAmount));
//                transactionAmountInteger = transactionAmount;
////                self.PayableAmountLabel.text = transactionAmount as! String
//            }
//            if (tipIndicator != (null) && !TextUtils.isEmpty(tipIndicator)) {
//                if (tipIndicator.equals("01") && tipIndicator != (null)) {
//                    amountTxtInput4.setHint("Tip Amount (PKR)");
//                } else if (tipIndicator.equals("02")) {
//                    if (tipFixed != (null) && tipFixed != 0) {
//                        amountTxtInput4.setHint("Convenience Fee (PKR)");
//                        editAmount4.setText(String.valueOf(tipFixed));
//                        editAmount4.setFocusableInTouchMode(false);
//                        transactionAmountInteger = transactionAmountInteger + ((tipFixed));
//
//                    } else {
//                        amountTxtInput4.setHint("Convenience Fee (PKR)");
//                        editAmount4.setText("");
//                        editAmount4.setFocusableInTouchMode(true);
//
//                    }
//                } else if (tipIndicator.equals("03")) {
//                    isPercentage = true;
//                    if (tipPercentage != 0 && tipPercentage != (null)) {
//                        amountTxtInput4.setHint("Convenience Fee Percentage (PKR)");
//                        editAmount4.setText(tipPercentage + "%");
//                        editAmount4.setFocusableInTouchMode(false);
//                        if (transactionAmount != 0 && transactionAmount != (null)) {
//
//                            transactionAmountInteger = transactionAmountInteger + transactionAmountInteger * ((tipPercentage / 100));
//
//
//                        }
//                    } else {
//                        amountTxtInput4.setHint("Convenience Fee (PKR)");
//                        editAmount4.setText("");
//                        editAmount4.setFocusableInTouchMode(true);
//
//                    }
//                }
//            } else {
//                editAmount4.setText("Not Applicable");
//                editAmount4.setFocusableInTouchMode(false);
//            }
//
//
//            editRemarks5.setText(String.valueOf(transactionAmountInteger));

            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            appPreferences = new AppPreferences(BookMeDetails.this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");

            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }
            purpStringArrayList = new ArrayList<>();
            utils = new Utils(BookMeDetails.this);
//            payabaleAmount.setText("");
//            Calendar today = Calendar.getInstance();
//            today.set(Calendar.MILLISECOND, 0);
//            today.set(Calendar.SECOND, 0);
//            today.set(Calendar.MINUTE, 0);
//            today.set(Calendar.HOUR_OF_DAY, 0);


//            String menus = constants.sharedPreferences.getString("Menus", "N/A");
//
//            if (!TextUtils.isEmpty(menus) && !menus.equals("N/A") && !menus.equals("")) {
//
//                if (menus.toLowerCase().contains("pay bill")) {
//                    showDilogForErrorForMenu("This service is currently disabled. Please try again later.", "WARNING");
//                }
//            }


            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!flag) {
                        expandableLayout0.expand();
                        mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_less, 0);
                        flag = true;
                    } else {
                        expandableLayout0.collapse();
                        mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_more, 0);
                        flag = false;
                    }

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GetPurposeBillList() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    pd.show();
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sPurposeGroupCode", "BMOTP");
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(BookMeDetails.this);

                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getPuposesURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("POTList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        purpStringArrayList.add(jsonObj.getString("Name"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();

                                                    }

                                                }

                                                purpose = new String[purpStringArrayList.size()];
                                                purpose = purpStringArrayList.toArray(purpose);

                                                Gson gson = new Gson();
                                                String json = gson.toJson(purpStringArrayList);
                                                constants.purposeBillList.putString("purposeList", json);
                                                constants.purposeBillList.commit();
                                                constants.purposeBillList.apply();
                                                showSpinner();
                                            } else if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR", BookMeDetails.this, new LoginScreen());
                                                validateBtn.setEnabled(true);

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                            validateBtn.setEnabled(true);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sPurposeGroupCode", "BP");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    validateBtn.setEnabled(true);
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
        }
    }

    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                if (!TextUtils.isEmpty(type) && type.equals("requestbill")) {
                    purposeSelected = constants.destinationAccount.getString("purpose", "N/A");
                    spinnerAdapter purposeAdapter = new spinnerAdapter(BookMeDetails.this, R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(constants.destinationAccount.getString("purpose", "N/A")));
                    purposesSpinner.setClickable(false);
                } else {
                    spinnerAdapter purposeAdapter = new spinnerAdapter(BookMeDetails.this, R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(0);
                    purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            try {


                                // Get select item
                                if (purposesSpinner.getSelectedItem() == "Purpose of Payment") {
                                    purposeSelected = purposesSpinner.getSelectedItem().toString();
                                    Log.d("identity", "---" + purposeSelected);

                                } else {

                                    purposeSelected = purposesSpinner.getSelectedItem().toString();

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });

                    pd.dismiss();

                }

            }
        }, 1000);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            CryptLib _crypt = new CryptLib();

            if (isFirst) {


                upperPart1.setEnabled(true);
                if (!TextUtils.isEmpty(type) && type.equals("requestbill")) {

                } else {
                    upperPart2.setEnabled(true);
                }
                if (isSource) {

                    isSelectSource = constants.sourceAccount.getString("isSelect", "N/A");
                    if (!isSelectSource.equals("N/A") && isSelectSource.equals("Yes")) {
                        sourceuserName = _crypt.decrypt(constants.sourceAccount.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        sourceBalance = _crypt.decrypt(constants.sourceAccount.getString("balance", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        sourceAccount = _crypt.decrypt(constants.sourceAccount.getString("accountNumber", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        branchCode = _crypt.decrypt(constants.sourceAccount.getString("branchCode", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        sourceCurrency = _crypt.decrypt(constants.sourceAccount.getString("currency", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        userNameTxt.setText(sourceuserName);
                        accMaskTxt.setVisibility(View.VISIBLE);
                        accMaskTxt.setText(utils.getMaskedString(sourceAccount));
                        amountTxt.setVisibility(View.VISIBLE);
                        amountTxt.setText(sourceCurrency + " " + sourceBalance);
                    }

                    constants.sourceAccountEditor.putString("isSelect", "No");
                    constants.sourceAccountEditor.commit();
                    constants.sourceAccountEditor.apply();
                    isSource = false;

                }

                if (isDestination) {

                    isSelectDestination = constants.destinationAccount.getString("isSelect", "N/A");
                    if (!isSelectDestination.equals("N/A") && isSelectDestination.equals("Yes")) {
                        distributorID = _crypt.decrypt(constants.destinationAccount.getString("DistributorID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        distributorAccountNumber = _crypt.decrypt(constants.destinationAccount.getString("distributorAccountNumber", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        amountAfterDue = _crypt.decrypt(constants.destinationAccount.getString("AmountAfterDueDate", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        amountBeforeDue = _crypt.decrypt(constants.destinationAccount.getString("AmountWithinDueDate", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        billStatus = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        billingMonth = _crypt.decrypt(constants.destinationAccount.getString("BillingMonth", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        dueDate = _crypt.decrypt(constants.destinationAccount.getString("DueDate", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        consumerName = _crypt.decrypt(constants.destinationAccount.getString("ConsumerName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        billConsumerNumber = _crypt.decrypt(constants.destinationAccount.getString("BillConsumerNumber", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        companyName = _crypt.decrypt(constants.destinationAccount.getString("CompanyName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        if (companyName.toLowerCase().contains("postpaid")) {
                            isPostpaid = true;
                        }
                        alias = _crypt.decrypt(constants.destinationAccount.getString("alias", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        String cat = constants.destinationAccount.getString("categoryType", "N/A");
                        if (!TextUtils.isEmpty(cat) && !cat.equals("N/A") && !cat.equals("null") && !cat.equals(null)) {
                            categoryType = _crypt.decrypt(constants.destinationAccount.getString("categoryType", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        } else {
                            categoryType = "N/A";
                        }
                        //                itemName.setText(distributorName);


                        payableBeforeTxt.setText(amountBeforeDue);
                        payableAfterTxt.setText(amountAfterDue);
                        billingMonthTxt.setText(billingMonth);
                        billingTxt.setText(companyName);
                        dueDateTxt.setText(dueDate);
                        statusTxt.setText(billStatus);
                        consumerIDTxt.setText(billConsumerNumber);
                        consumerNumberTxt.setText(consumerName);
                        itemName.setText(alias);


                        if (billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("unpaid") || billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("blocked")) {

                            statusTxt.setTextColor(getResources().getColor(R.color.tabunderline));
                        } else
                            statusTxt.setTextColor(getResources().getColor(R.color.lgreen));

                        Calendar today = Calendar.getInstance();
                        today.set(Calendar.MILLISECOND, 0);
                        today.set(Calendar.SECOND, 0);
                        today.set(Calendar.MINUTE, 0);
                        today.set(Calendar.HOUR_OF_DAY, 0);
                        Date currentDate = today.getTime();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date mDate;
                        long timeInMilliseconds = 0, currentTimeToMili = 0;
                        try {
                            if (!dueDate.equals("N/A")) {
                                mDate = sdf.parse(dueDate);
                                timeInMilliseconds = mDate.getTime();
                                currentTimeToMili = currentDate.getTime();
                                System.out.println("Date in milli :: " + timeInMilliseconds);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        String billStatus1 = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        if (!TextUtils.isEmpty(billStatus1) && !billStatus1.equals("N/A")) {

                            if (billStatus1.toLowerCase().equals("partial payment")) {

                                editAmount.setText("");
                                editAmount.setFocusableInTouchMode(true);
                                editAmount.setEnabled(true);
                            } else {

                                if (currentTimeToMili <= timeInMilliseconds) {


                                    payabaleAmount.setText(amountBeforeDue);
                                    editAmount.setText(amountBeforeDue.split("\\.")[0]);
                                    editAmount.setFocusableInTouchMode(false);
                                    editAmount.setEnabled(false);


                                } else if (currentTimeToMili > timeInMilliseconds) {

                                    payabaleAmount.setText(amountAfterDue);
                                    editAmount.setText(amountAfterDue.split("\\.")[0]);
                                    editAmount.setFocusableInTouchMode(false);
                                    editAmount.setEnabled(false);

                                }

                            }
                        }
                        if (currentTimeToMili <= timeInMilliseconds) {


                            payabaleAmount.setText(amountBeforeDue);

                        } else if (currentTimeToMili > timeInMilliseconds) {

                            payabaleAmount.setText(amountAfterDue);

                        }

                    }

                    constants.destinationAccountEditor.putString("isSelect", "No");
                    constants.destinationAccountEditor.commit();
                    constants.destinationAccountEditor.apply();
                    isDestination = false;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getTitleFetch() {

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                int stanNumber = 6;
                stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
                int rrnNumber = 12;
                rrn = String.valueOf(helper.generateRandom(rrnNumber));
                CryptLib _crypt = new CryptLib();

                String bankCode="69";

                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sToAccountNumber","2000701923");
                params.put("sBankID", bankCode);
                params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sSTAN", stan);
                params.put("sRRN", rrn);
                params.put("sBeneType", beneType);
                params.put("sFromAccountNumber", sourceAccount);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sBeneCurrencyCode",sourceCurrency);
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.titleFetchURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObjects = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObjects.getString("ServerKey").equals(md5)) {
                                        if (jsonObjects.getJSONObject("Signs_TitleFetchResult").getString("Status_Code").equals("00")) {

                                            pd.dismiss();
                                            try {

                                                CryptLib _crypt = new CryptLib();
                                                Intent intent = new Intent(BookMeDetails.this, BookMeDetailsReview.class);
                                                intent.putExtra("userName", sourceuserName);
                                                intent.putExtra("balance", sourceBalance);
                                                intent.putExtra("accountNumber", sourceAccount);
                                                intent.putExtra("purpose", purposeSelected);
                                                intent.putExtra("currency", sourceCurrency);
                                                intent.putExtra("distributorAccountNumber", distributorAccountNumber);
                                                intent.putExtra("mobile", jsonObject.getString("ORDER_REF_ID"));
                                                intent.putExtra("remarks", remarks);
                                                intent.putExtra("stan", stan);
                                                intent.putExtra("rrn", rrn);
                                                intent.putExtra("type", jsonObject.getString("TYPE"));
                                                intent.putExtra("amount", jsonObject.getString("AMOUNT"));
                                                intent.putExtra("branchCode", branchCode);
                                                intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                intent.putExtra("distributorName", alias);
                                                intent.putExtra("distributorID", distributorID);
                                                intent.putExtra("categoryType", categoryType);
                                                intent.putExtra("ftType", "QRPayment");
                                                intent.putExtra("transactionAmount", jsonObject.getString("AMOUNT"));
                                                intent.putExtra("tipAmount", editAmount4.getText().toString());
                                                intent.putExtra("friendNumber", friendNumber);
                                                intent.putExtra("messageId", messageId);
                                                intent.putExtra("targetId", targetId);
                                                intent.putExtra("bookingDate", billingMonthTxt.getText().toString());
                                                intent.putExtra("companyName", itemName.getText().toString());
                                                intent.putExtra("service_provider", jsonObject.getString("SERVICE_PROVIDER"));

                                                intent.putExtra("chargeCode", jsonObjects.getJSONObject("Signs_TitleFetchResult").getString("ChargeCode"));
                                                intent.putExtra("conversionRate", jsonObjects.getJSONObject("Signs_TitleFetchResult").getString("ConversionRate"));
                                                intent.putExtra("titleFetch", jsonObjects.getJSONObject("Signs_TitleFetchResult").getString("AccountTitle"));

                                                overridePendingTransition(0, 0);
                                                startActivity(intent);
                                                validateBtn.setEnabled(true);






                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
//
//
                                        } else if (jsonObjects.getJSONObject("Signs_TitleFetchResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObjects.getJSONObject("Signs_TitleFetchResult").getString("Status_Description"), "ERROR",getApplicationContext(), new LoginScreen());
                                            validateBtn.setEnabled(true);

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObjects.getJSONObject("Signs_TitleFetchResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                        }

                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }
                            }
                        })
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);

        }

    }

    public void Verification() {

        try {

            CryptLib _crypt = new CryptLib();
            Intent intent = new Intent(BookMeDetails.this, BookMeDetailsReview.class);
            intent.putExtra("userName", sourceuserName);
            intent.putExtra("balance", sourceBalance);
            intent.putExtra("accountNumber", sourceAccount);
            intent.putExtra("purpose", purposeSelected);
            intent.putExtra("currency", sourceCurrency);
            intent.putExtra("distributorAccountNumber", distributorAccountNumber);
            intent.putExtra("mobile", jsonObject.getString("ORDER_REF_ID"));
            intent.putExtra("remarks", remarks);
            intent.putExtra("stan", stan);
            intent.putExtra("rrn", rrn);
            intent.putExtra("type", jsonObject.getString("TYPE"));
            intent.putExtra("amount", jsonObject.getString("AMOUNT"));
            intent.putExtra("branchCode", branchCode);
            intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            intent.putExtra("distributorName", alias);
            intent.putExtra("distributorID", distributorID);
            intent.putExtra("categoryType", categoryType);
            intent.putExtra("ftType", "QRPayment");
            intent.putExtra("transactionAmount", jsonObject.getString("AMOUNT"));
            intent.putExtra("tipAmount", editAmount4.getText().toString());
            intent.putExtra("friendNumber", friendNumber);
            intent.putExtra("messageId", messageId);
            intent.putExtra("targetId", targetId);
            intent.putExtra("bookingDate", billingMonthTxt.getText().toString());
            intent.putExtra("companyName", itemName.getText().toString());
            intent.putExtra("service_provider", jsonObject.getString("SERVICE_PROVIDER"));
            overridePendingTransition(0, 0);
            startActivity(intent);
            validateBtn.setEnabled(true);


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}