package com.ceesolutions.samba.billPayments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.CircleImageView;
import com.ceesolutions.samba.views.EnglishNumberToWords;
import com.ceesolutions.samba.views.RoundImageView;
import com.google.firebase.analytics.FirebaseAnalytics;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ceeayaz on 4/16/18.
 */

public class RequestPrepaidVoucherReviewScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    boolean flag = false;
    private ExpandableLayout expandableLayout0;
    private RelativeLayout firstRow;
    private TextView message, billerTxt, mainTxt, textView, doneBtn, heading, date, userNameTxt, accMaskTxt, friendNameTxt, accFrindMaskTxt, accFrindEmailTxt, accFrindMobileTxt, totalAmountTxt, amountInWordsTxt, purposeHeadingTxt, remarksHeadingTxt, conversionHeadingTxt, chargesHeadingTxt, reviewBtn;
    private ImageView backBtn, cancelBtn, notificationBtn, profile_image;
    private Dialog dialog;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private String amountFinal, amountWords, finalDate, currentTime, dueDate, formatedDate;
    private Intent intent1;
    private Date currentDate;
    private CircleImageView user_image;
    private RoundImageView titleImage;
    private TextView titleTxt;
    private int color;
    private AppPreferences appPreferences;
    private String location;
    private String userType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_prepaid_review);
        initViews();
        getSupportActionBar().hide();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");
                Intent intent = new Intent(RequestPrepaidVoucherReviewScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
                finishAffinity();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        reviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reviewBtn.setEnabled(false);

                if (userType.equals("NON_SAMBA")) {
                    FirebaseAnalytics.getInstance(RequestPrepaidVoucherReviewScreen.this).logEvent("Non_Samba_Mobile_Top_up_Request_Pressed", new Bundle());
                } else {
                    FirebaseAnalytics.getInstance(RequestPrepaidVoucherReviewScreen.this).logEvent("Mobile_Top_up_Request_Pressed", new Bundle());
                }

                SendRequest();

            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RequestPrepaidVoucherReviewScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
            }
        });
    }

    public void initViews() {

        titleTxt = (TextView) findViewById(R.id.titleTxt);
        titleImage = (RoundImageView) findViewById(R.id.titleImage);
        titleTxt.bringToFront();
        date = (TextView) findViewById(R.id.date);
        userNameTxt = (TextView) findViewById(R.id.userNameTxt);
        friendNameTxt = (TextView) findViewById(R.id.friendNameTxt);
        accFrindMaskTxt = (TextView) findViewById(R.id.accFrindMaskTxt);
        totalAmountTxt = (TextView) findViewById(R.id.totalAmountTxt);
        billerTxt = (TextView) findViewById(R.id.billerTxt);
        user_image = (CircleImageView) findViewById(R.id.profile_image);
        accFrindMobileTxt = (TextView) findViewById(R.id.accFrindMobileTxt);
        amountInWordsTxt = (TextView) findViewById(R.id.amountInWordsTxt);
        purposeHeadingTxt = (TextView) findViewById(R.id.purposeHeadingTxt);
        remarksHeadingTxt = (TextView) findViewById(R.id.remarksHeadingTxt);
        conversionHeadingTxt = (TextView) findViewById(R.id.conversionHeadingTxt);
        chargesHeadingTxt = (TextView) findViewById(R.id.chargesHeadingTxt);
        reviewBtn = (TextView) findViewById(R.id.reviewBtn);
        cancelBtn = (ImageView) findViewById(R.id.cancelBtn);
        profile_image = (ImageView) findViewById(R.id.profile_image1);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        mainTxt = (TextView) findViewById(R.id.mainTxt);
        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        firstRow = (RelativeLayout) findViewById(R.id.firstRow);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(RequestPrepaidVoucherReviewScreen.this);
        webService = new WebService();
        helper = Helper.getHelper(this);
        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.US);
        currentDate = Calendar.getInstance().getTime();
        formatter = new SimpleDateFormat("dd-MMM-yyyy");
        date.setText(formatter.format(currentDate));
        finalDate = formatter.format(currentDate);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
        currentTime = sdf.format(currentDate);
        utils = new Utils(RequestPrepaidVoucherReviewScreen.this);
        firstRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!flag) {
                    expandableLayout0.expand();
                    mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_less, 0);
                    flag = true;
                } else {
                    expandableLayout0.collapse();
                    mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_more, 0);
                    flag = false;
                }

            }
        });

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Intent intent = getIntent();
        intent1 = intent;

        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");


        try {
            CryptLib _crypt = new CryptLib();
            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        userNameTxt.setText(intent.getStringExtra("userName"));
        billerTxt.setText(intent.getStringExtra("distributorName"));
        purposeHeadingTxt.setText(intent.getStringExtra("purpose"));
        friendNameTxt.setText(intent.getStringExtra("distributorName"));
        accFrindMaskTxt.setText(intent.getStringExtra("mobile"));
        remarksHeadingTxt.setText(intent.getStringExtra("remarks"));
        int amount = Integer.valueOf(intent.getStringExtra("amount"));
        String amountCode = intent.getStringExtra("currency");

        String s = intent.getStringExtra("userName").substring(0, 1);

        switch (s.toUpperCase()) {

            case "A":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "B":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "C":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "D":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "E":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "F":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "G":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "H":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "I":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "J":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "K":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "L":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "M":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "N":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "O":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "P":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "Q":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "R":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "S":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "T":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "U":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "V":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "W":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "X":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "Y":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "Z":
                color = getResources().getColor(R.color.friendColor4);
                break;

            default:
                color = getResources().getColor(R.color.defaultColor);
                break;
        }

        totalAmountTxt.setText(amountCode + " " + getFormatedAmount(amount) + ".00");
        amountFinal = amountCode + " " + getFormatedAmount(amount) + ".00";
        String amountInWords = EnglishNumberToWords.convert(amount);
        amountInWordsTxt.setText(amountCode + " " + amountInWords + " only");
        amountWords = amountCode + " " + amountInWords + " only";
        String friendImage = intent.getStringExtra("friendImg");
//        if (!TextUtils.isEmpty(friendImage)) {
//            titleTxt.setVisibility(View.INVISIBLE);
//            titleImage.setVisibility(View.INVISIBLE);
//            byte[] imageByteArray = Base64.decode(friendImage, Base64.DEFAULT);
//            RequestOptions options = new RequestOptions();
//            options.centerCrop();
//            Glide.with(RequestPrepaidVoucherReviewScreen.this)
//                    .load(imageByteArray)
//                    .apply(options)
//                    .into(user_image);
//
//        } else {
//
//            user_image.setVisibility(View.INVISIBLE);
//            titleImage.setBackgroundTintList(ColorStateList.valueOf((color)));
//            titleTxt.setText(s);
//        }

        conversionHeadingTxt.setText(intent.getStringExtra("conversionRate"));
        chargesHeadingTxt.setText(intent.getStringExtra("chargeCode"));

        String name = intent.getStringExtra("distributorName");


        if (name.toLowerCase().contains("mobilink")) {

            profile_image.setBackground(ContextCompat.getDrawable(RequestPrepaidVoucherReviewScreen.this, R.drawable.mobilink));

        } else if (name.toLowerCase().contains("telenor")) {
            profile_image.setBackground(ContextCompat.getDrawable(RequestPrepaidVoucherReviewScreen.this, R.drawable.telenor));

        } else if (name.toLowerCase().contains("ufone")) {
            profile_image.setBackground(ContextCompat.getDrawable(RequestPrepaidVoucherReviewScreen.this, R.drawable.ufone));

        } else if (name.toLowerCase().contains("warid")) {
            profile_image.setBackground(ContextCompat.getDrawable(RequestPrepaidVoucherReviewScreen.this, R.drawable.warid));

        } else if (name.toLowerCase().contains("zong")) {
            profile_image.setBackground(ContextCompat.getDrawable(RequestPrepaidVoucherReviewScreen.this, R.drawable.zong));

        }

    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(RequestPrepaidVoucherReviewScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
//        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


    private String getFormatedAmount(int amount) {
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }


    public void SendRequest() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                dialog.show();

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sRequester_id", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sRequester_number", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sFriend_id", intent1.getStringExtra("friendID"));
                    params.put("sFriend_name", intent1.getStringExtra("userName"));
                    params.put("sFriend_number", intent1.getStringExtra("friendNumber"));
                    params.put("sRequest_amount", intent1.getStringExtra("amount"));
                    params.put("sRequest_purpose", intent1.getStringExtra("purpose"));
                    params.put("sRequest_remarks", intent1.getStringExtra("remarks"));
                    params.put("sRequest_consumer_name", intent1.getStringExtra("distributorName"));
                    params.put("sShow_account_number", "-");
                    params.put("sDistributorID", intent1.getStringExtra("distributorID"));
                    params.put("sRequest_max_amount", intent1.getStringExtra("maxAmount"));
                    params.put("sRequest_min_amount", intent1.getStringExtra("minAmount"));
                    params.put("sBillConsumerNumber", intent1.getStringExtra("mobile"));
                    params.put("sDistributor_account_number", intent1.getStringExtra("distributorAccountNumber"));
                    params.put("sCurrency", intent1.getStringExtra("currency"));
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSigns_Username", user);
                    HttpsTrustManager.allowMySSL(RequestPrepaidVoucherReviewScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.requestprepaidPaymentURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("request_PrepaidVoucherResult").getString("Status_Code").equals("00")) {


                                                Intent intent = new Intent(RequestPrepaidVoucherReviewScreen.this, RequestPrepaidVoucherConfirmationScreen.class);
                                                intent.putExtra("userName", intent1.getStringExtra("userName"));
                                                intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                                                intent.putExtra("purpose", intent1.getStringExtra("purpose"));
                                                intent.putExtra("distributorID", intent1.getStringExtra("distributorID"));
                                                intent.putExtra("distributorName", intent1.getStringExtra("distributorName"));
                                                intent.putExtra("remarks", intent1.getStringExtra("remarks"));
                                                intent.putExtra("amount", intent1.getStringExtra("amount"));
                                                intent.putExtra("branchCode", intent1.getStringExtra("branchCode"));
                                                intent.putExtra("bankCode", intent1.getStringExtra("bankCode"));
                                                intent.putExtra("amountFinal", amountFinal);
                                                intent.putExtra("amountWords", amountWords);
                                                intent.putExtra("currency", intent1.getStringExtra("currency"));
                                                intent.putExtra("stan", intent1.getStringExtra("stan"));
                                                intent.putExtra("rrn", intent1.getStringExtra("rrn"));
                                                intent.putExtra("currentDate", finalDate);
                                                intent.putExtra("currentTime", currentTime);
                                                intent.putExtra("companyName", intent1.getStringExtra("companyName"));
                                                intent.putExtra("type", "BP");
                                                intent.putExtra("mobile", intent1.getStringExtra("mobile"));
                                                intent.putExtra("categoryType", intent1.getStringExtra("categoryType"));
                                                intent.putExtra("distributorAccountNumber", intent1.getStringExtra("distributorAccountNumber"));
                                                intent.putExtra("friendImg", intent1.getStringExtra("friendImg"));
                                                overridePendingTransition(0, 0);
                                                dialog.dismiss();
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                                constants.accountsEditor.clear();
                                                constants.accountsEditor.commit();
                                                constants.accountsEditor.apply();
                                                constants.destinationAccountEditor.clear();
                                                constants.destinationAccountEditor.commit();
                                                constants.destinationAccountEditor.apply();
                                                dialog.dismiss();
                                                finish();
                                                finishAffinity();
                                                reviewBtn.setEnabled(true);
                                            } else if (jsonObject.getJSONObject("request_PrepaidVoucherResult").getString("Status_Code").equals("54")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("request_PrepaidVoucherResult").getString("Status_Description"), "ERROR", RequestPrepaidVoucherReviewScreen.this, new LoginScreen());
                                                reviewBtn.setEnabled(true);

                                            } else {
                                                dialog.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("request_PrepaidVoucherResult").getString("Status_Description"), "ERROR");
                                                reviewBtn.setEnabled(true);

                                            }
                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            reviewBtn.setEnabled(true);

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        reviewBtn.setEnabled(true);

                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        reviewBtn.setEnabled(true);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        reviewBtn.setEnabled(true);

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sRequester_id", constants.sharedPreferences.getString("LegalID", "N/A"));
//                            params.put("sRequester_number", constants.sharedPreferences.getString("MobileNumber", "N/A"));
//                            params.put("sFriend_id", intent1.getStringExtra("friendID"));
//                            params.put("sFriend_name", intent1.getStringExtra("userName"));
//                            params.put("sFriend_number", intent1.getStringExtra("friendNumber"));
//                            params.put("sRequest_amount", intent1.getStringExtra("amount"));
//                            params.put("sRequest_purpose", intent1.getStringExtra("purpose"));
//                            params.put("sRequest_remarks", intent1.getStringExtra("remarks"));
//                            params.put("sRequest_consumer_name", intent1.getStringExtra("distributorName"));
//                            params.put("sShow_account_number", "-");
//                            params.put("sDistributorID", intent1.getStringExtra("distributorID"));
//                            params.put("sRequest_max_amount", intent1.getStringExtra("maxAmount"));
//                            params.put("sRequest_min_amount", intent1.getStringExtra("minAmount"));
//                            params.put("sBillConsumerNumber", intent1.getStringExtra("mobile"));
//                            params.put("sDistributor_account_number", intent1.getStringExtra("distributorAccountNumber"));
//                            params.put("sCurrency",intent1.getStringExtra("currency"));
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    reviewBtn.setEnabled(true);

                }

            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                reviewBtn.setEnabled(true);


            }
        } catch (Exception e) {
            dialog.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            reviewBtn.setEnabled(true);


        }
    }


//    public void showDilogForErrorForLogin(String msg, String header) {
//
//        dialog = new Dialog(RequestPrepaidVoucherReviewScreen.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.error_dialog);
//        message = (TextView) dialog.findViewById(R.id.validatingTxt);
//        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
//        heading = (TextView) dialog.findViewById(R.id.heading);
//        dialog.setCancelable(true);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        doneBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                dialog.dismiss();
//                finish();
//
//            }
//        });
//        message.setText(msg);
//        heading.setText(header);
//        dialog.show();
//
//
//    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
//        showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");
    }
}
