package com.ceesolutions.samba.billPayments.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.billPayments.EditBillerDetailsScreen;
import com.ceesolutions.samba.billPayments.Model.Biller;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/1/18.
 */

public class CustomHorizontalListAdapter extends BaseAdapter {
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    ArrayList<Biller> billerList;
    LayoutInflater layoutInflater;
    View row;
    Context context;
    TextView userName;
    ImageView title;
    int[] androidColors;
    private String isType;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private AppPreferences appPreferences;
    private String location;

    public CustomHorizontalListAdapter(ArrayList<Biller> billerArrayList, Context mContext, String type) {
        billerList = billerArrayList;
        context = mContext;
        constants = Constants.getConstants(context);
        isType = type;
        appPreferences = new AppPreferences(mContext);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return billerList.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.biller_horizontal_item, parent, false);
        }


        title = (ImageView) convertView.findViewById(R.id.title);
        userName = (TextView) convertView.findViewById(R.id.userName);
        helper = Helper.getHelper(context);
        userName.setText(billerList.get(position).getAlias());
        requestQueue = Volley.newRequestQueue(context);
        constants = Constants.getConstants(context);
        utils = new Utils(context);
        pd = new Dialog(context);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        String name = billerList.get(position).getCategoryType();

        switch (name) {

            case "Electricity": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_electric));

                break;
            }

            case "Gas": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_gas));

                break;

            }

            case "Water and Sewerage": {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_water));

                break;
            }

            case "Telecommunication": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_telecom));

                break;

            }

            case "Mobile Prepaid": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_ph));

                break;

            }

            case "Mobile Postpaid": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_ph));

                break;

            }

            case "ISP Payment": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_isp));

                break;

            }

            case "Education": {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_edu));

                break;
            }

            case "Airlines": {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon));

                break;
            }

            case "TAX": {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_tax));

                break;
            }

            default: {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.default1));

                break;
            }

        }


//        userName.setText(billerList.get(position).getUserName());

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                title.setEnabled(false);
                if (!TextUtils.isEmpty(isType) && isType.equals("destination")) {

                    BillInquiry(position);

                } else {
//
                    Intent intent = new Intent(((Activity) context), EditBillerDetailsScreen.class);
                    intent.putExtra("alias", billerList.get(position).getAlias());
                    intent.putExtra("consumerNumber", billerList.get(position).getConsumerNumber());
                    intent.putExtra("consumerName", billerList.get(position).getConsumerName());
                    intent.putExtra("billerID", billerList.get(position).getBillerID());
                    intent.putExtra("companyName", billerList.get(position).getCompanyName());
                    intent.putExtra("companyID", billerList.get(position).getCompanyID());
                    intent.putExtra("accountNumber", billerList.get(position).getAccountNumber());
                    intent.putExtra("categoryType", billerList.get(position).getCategoryType());
                    intent.putExtra("isFav", billerList.get(position).getIsFavourite());
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(0, 0);
                    title.setEnabled(true);
                    ((Activity) context).finish();
//                    finalConvertView.setEnabled(true);
                }
            }
        });


        return convertView;
    }

    public void BillInquiry(final int position) {

        final int stanNumber = 6;

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sBillConsumerNumber", billerList.get(position).getConsumerNumber());
                    params.put("sDistributorID", billerList.get(position).getCompanyID());
                    params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(context);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.billInquiryURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            try {

                                                CryptLib _crypt = new CryptLib();
                                                if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("00")) {

                                                    constants.destinationAccountEditor.putString("isSelect", "Yes");
                                                    constants.destinationAccountEditor.putString("AmountAfterDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountAfterDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("AmountWithinDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountWithinDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillStatus", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillStatus"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillingMonth", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillingMonth"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("CompanyID", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("DueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("DueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("ConsumerName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("ConsumerName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("CompanyName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("RRN", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("RRN"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillConsumerNumber", _crypt.encryptForParams(billerList.get(position).getConsumerNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("DistributorID", _crypt.encryptForParams(billerList.get(position).getCompanyID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("distributorAccountNumber", _crypt.encryptForParams(billerList.get(position).getAccountNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("alias", _crypt.encryptForParams(billerList.get(position).getAlias(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("categoryType", _crypt.encryptForParams(billerList.get(position).getCategoryType(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.commit();
                                                    constants.destinationAccountEditor.apply();
                                                    pd.dismiss();
                                                    ((Activity) context).finish();
                                                    title.setEnabled(true);


                                                } else if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("54")) {
                                                    pd.dismiss();
                                                    utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR", context, new LoginScreen());
                                                    title.setEnabled(true);

                                                } else {

                                                    pd.dismiss();
                                                    utils.showDilogForError(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR");
                                                    title.setEnabled(true);

                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            title.setEnabled(true);

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        title.setEnabled(true);

                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        title.setEnabled(true);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        title.setEnabled(true);

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sBillConsumerNumber", billerList.get(position).getConsumerNumber());
//                            params.put("sDistributorID", billerList.get(position).getCompanyID());
//                            params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    title.setEnabled(true);

                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                title.setEnabled(true);

            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            title.setEnabled(true);

        }
    }
}