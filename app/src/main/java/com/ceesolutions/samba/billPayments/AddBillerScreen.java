package com.ceesolutions.samba.billPayments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/10/18.
 */

public class AddBillerScreen extends AppCompatActivity {


    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    public String edit;
    private EditText editConsumerNumber1, editAlias, editAccountNumber, editConsumerNumber, editEmailAddress, editAccountTitle, editBankName;
    private TextView headingTxt, validateBtn, validateBtn1, errorMessageForAlias, errorMessageForConsumerNumber, addToFavBtn, deleteBeneficiaryBtn, requestFundsBtn, textView, doneBtn, heading, billerTxt, btn;
    private Dialog dialog;
    private WebService webService;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Dialog pd, dialog1;
    private Utils utils;
    private Intent intent1;
    private ImageView backBtn;
    private RelativeLayout upperPart1;
    private String alias, consumerNumber, stan;
    private AppPreferences appPreferences;
    private String location;
    private FrameLayout frameLayout;
    private View spaceView;
    private int length;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_biller);
        getSupportActionBar().hide();
        initViews();

        editAlias.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForAlias.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForAlias.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                alias = editable.toString();
            }
        });


        editConsumerNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForConsumerNumber.setVisibility(View.VISIBLE);
                errorMessageForConsumerNumber.setText(intent1.getStringExtra("display"));
                errorMessageForConsumerNumber.setTextColor(getResources().getColor(R.color.lgreen));
                errorMessageForConsumerNumber.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForConsumerNumber.setVisibility(View.VISIBLE);
                errorMessageForConsumerNumber.setText(intent1.getStringExtra("display"));
                errorMessageForConsumerNumber.setTextColor(getResources().getColor(R.color.lgreen));
                errorMessageForConsumerNumber.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                consumerNumber = editable.toString();

            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(edit) && edit.equals("edit")) {

                    Intent intent = new Intent(AddBillerScreen.this, ManageBillerScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                    finish();
                } else
                    finish();
            }
        });

        validateBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateBtn1.setEnabled(false);

                FirebaseAnalytics.getInstance(AddBillerScreen.this).logEvent("Add_Biller_Review_Pressed", new Bundle());

                EditBiller();
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(edit) && edit.equals("edit")) {

            Intent intent = new Intent(AddBillerScreen.this, ManageBillerScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);
            finish();
        } else
            finish();
    }

    public void initViews() {


        intent1 = getIntent();
        spaceView = (View) findViewById(R.id.spaceView);
        upperPart1 = (RelativeLayout) findViewById(R.id.upperPart1);
        editAlias = (EditText) findViewById(R.id.editAlias);
        errorMessageForAlias = (TextView) findViewById(R.id.errorMessageForAlias);
        editConsumerNumber1 = (EditText) findViewById(R.id.editConsumerNumber1);
        frameLayout = (FrameLayout) findViewById(R.id.relativeLayout6);
        errorMessageForConsumerNumber = (TextView) findViewById(R.id.errorMessageForConsumerNumber);
        headingTxt = (TextView) findViewById(R.id.headingTxt);
        editConsumerNumber = (EditText) findViewById(R.id.editConsumerNumber);
        validateBtn = (TextView) findViewById(R.id.validateBtn);
        validateBtn1 = (TextView) findViewById(R.id.validateBtn1);
        helper = Helper.getHelper(this);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(AddBillerScreen.this);
        utils = new Utils(AddBillerScreen.this);
        pd = new Dialog(AddBillerScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        billerTxt = (TextView) findViewById(R.id.billerTxt);

        billerTxt.setText(intent1.getStringExtra("companyName"));

        try {

            if (!TextUtils.isEmpty(intent1.getStringExtra("length")) && !intent1.getStringExtra("length").equals(null) && !intent1.getStringExtra("length").equals("null")) {
                length = Integer.valueOf(intent1.getStringExtra("length"));
                editConsumerNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(length)});
                errorMessageForConsumerNumber.setText(intent1.getStringExtra("display"));
                errorMessageForConsumerNumber.setTextColor(getResources().getColor(R.color.lgreen));
                errorMessageForConsumerNumber.setError(null);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        edit = intent1.getStringExtra("edit");
        if (!TextUtils.isEmpty(edit) && edit.equals("edit")) {

            frameLayout.setVisibility(View.VISIBLE);
            editConsumerNumber1.setVisibility(View.VISIBLE);
            editAlias.setText(intent1.getStringExtra("alias"));
            if (!TextUtils.isEmpty(intent1.getStringExtra("consumerName")) && !intent1.getStringExtra("consumerName").equals("N/A"))
                editConsumerNumber.setText(intent1.getStringExtra("consumerName"));
            else
                editConsumerNumber.setText("N/A");
//            editConsumerNumber.setText(intent1.getStringExtra("consumerName"));
            editConsumerNumber.setFocusableInTouchMode(false);
            editConsumerNumber1.setText(intent1.getStringExtra("consumerNumber"));
            editConsumerNumber1.setFocusableInTouchMode(false);
            billerTxt.setText(intent1.getStringExtra("companyName"));
            headingTxt.setText("Edit Biller");
            validateBtn.setVisibility(View.INVISIBLE);
            validateBtn1.setVisibility(View.VISIBLE);
            spaceView.setVisibility(View.VISIBLE);
            errorMessageForConsumerNumber.setVisibility(View.GONE);

        } else {

            int maxLengthofEditText = Integer.valueOf(intent1.getStringExtra("length"));
            editConsumerNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});
            errorMessageForConsumerNumber.setVisibility(View.VISIBLE);
            errorMessageForConsumerNumber.setText(intent1.getStringExtra("display"));
            errorMessageForConsumerNumber.setTextColor(getResources().getColor(R.color.lgreen));
            errorMessageForConsumerNumber.setError(null);
        }

        int stanNumber = 6;
        stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {


                    alias = editAlias.getText().toString().trim();
                    consumerNumber = editConsumerNumber.getText().toString().trim();

                    if (TextUtils.isEmpty(alias)) {

                        errorMessageForAlias.setVisibility(View.VISIBLE);
                        errorMessageForAlias.bringToFront();
                        errorMessageForAlias.setError("");
                        errorMessageForAlias.setText("Please enter valid alias");

                    } else if (!helper.validateInputForSC(alias)) {

                        errorMessageForAlias.setVisibility(View.VISIBLE);
                        errorMessageForAlias.bringToFront();
                        errorMessageForAlias.setError("");
                        errorMessageForAlias.setText("Alias cannot contains <,>,\",',%,(,),&,+,\\,~");


                    } else
                        alias = editAlias.getText().toString().trim();


                    if (TextUtils.isEmpty(consumerNumber)) {

                        errorMessageForConsumerNumber.setVisibility(View.VISIBLE);
                        errorMessageForConsumerNumber.bringToFront();
                        errorMessageForConsumerNumber.setTextColor(getResources().getColor(R.color.tabunderline));
                        errorMessageForConsumerNumber.setError("");
                        errorMessageForConsumerNumber.setText("Please enter valid alias");


                    } else if (!helper.validateInputForSC(consumerNumber) || consumerNumber.contains(" ")) {

                        errorMessageForConsumerNumber.setVisibility(View.VISIBLE);
                        errorMessageForConsumerNumber.bringToFront();
                        errorMessageForConsumerNumber.setTextColor(getResources().getColor(R.color.tabunderline));
                        errorMessageForConsumerNumber.setError("");
                        errorMessageForConsumerNumber.setText("Amount cannot contains <,>,\",',%,(,),&,+,\\,~,space");


                    } else
                        consumerNumber = editConsumerNumber.getText().toString().trim();


                    if (billerTxt.getText().toString().equals("Biller")) {

                        utils.showDilogForError("Please select Biller Company", "WARNING");
                    }

                    if (!billerTxt.getText().toString().equals("Biller") && (!TextUtils.isEmpty(consumerNumber) && helper.validateInputForSC(consumerNumber) && !consumerNumber.contains(" ")) && (!TextUtils.isEmpty(alias) && helper.validateInputForSC(alias))) {

                        validateBtn.setEnabled(false);
                        BillInquiry();
                    }

                } catch (Exception e) {


                    e.printStackTrace();
                }
            }
        });

    }


    public void BillInquiry() {


        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    int stanNumber = 6;
                    stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sBillConsumerNumber", consumerNumber);
                    params.put("sDistributorID", intent1.getStringExtra("id"));
                    params.put("sSTAN", stan);
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(AddBillerScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.billInquiryURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("00")) {


                                                pd.dismiss();
                                                Intent intent = new Intent(AddBillerScreen.this, ReviewBillerDetailsScreen.class);
                                                intent.putExtra("AmountAfterDueDate", jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountAfterDueDate"));
                                                intent.putExtra("AmountWithinDueDate", jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountWithinDueDate"));
                                                intent.putExtra("BillStatus", jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillStatus"));
                                                intent.putExtra("BillingMonth", jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillingMonth"));
                                                intent.putExtra("CompanyID", jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyID"));
                                                intent.putExtra("ConsumerName", jsonObject.getJSONObject("Signs_BillInquiryResult").getString("ConsumerName"));
                                                intent.putExtra("DueDate", jsonObject.getJSONObject("Signs_BillInquiryResult").getString("DueDate"));
                                                intent.putExtra("CompanyName", jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyName"));
                                                intent.putExtra("RRN", jsonObject.getJSONObject("Signs_BillInquiryResult").getString("RRN"));
                                                intent.putExtra("categoryID", intent1.getStringExtra("categoryID"));
                                                intent.putExtra("categoryName", intent1.getStringExtra("categoryName"));
                                                intent.putExtra("consumerNumber", consumerNumber);
                                                intent.putExtra("alias", alias);
                                                intent.putExtra("distributorAccount", intent1.getStringExtra("distributorAccount"));
                                                overridePendingTransition(0, 0);
                                                startActivity(intent);
                                                validateBtn.setEnabled(true);

                                            } else if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR", AddBillerScreen.this, new LoginScreen());
                                                validateBtn.setEnabled(true);
                                                clearViews();

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                                clearViews();
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            validateBtn.setEnabled(true);
                                            clearViews();
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                        clearViews();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                        clearViews();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                        clearViews();
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sBillConsumerNumber", consumerNumber);
//                            params.put("sDistributorID", intent1.getStringExtra("id"));
//                            params.put("sSTAN", stan);
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    validateBtn.setEnabled(true);
                    clearViews();
                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
                clearViews();
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
            clearViews();

        }
    }

    public void clearViews() {

        editAlias.getText().clear();
        editConsumerNumber.getText().clear();
        editAlias.requestFocus();
    }

    public void EditBiller() {


        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sBillerID", intent1.getStringExtra("billerID"));
                    params.put("sSigns_Alias", editAlias.getText().toString());
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(AddBillerScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.editBillerURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_EditBillerResult").getString("Status_Code").equals("00")) {

                                                pd.dismiss();

                                                FirebaseAnalytics.getInstance(AddBillerScreen.this).logEvent("Add_Biller_Confirm_Pressed", new Bundle());

//                                            utils.showDilogForError("Biller has successfully marked as favourite","SUCCESS");
                                                showDilogForErrorForMenu("Biller has been updated successfully.", "SUCCESS");
                                                validateBtn1.setEnabled(true);


                                            } else if (jsonObject.getJSONObject("Signs_EditBillerResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_EditBillerResult").getString("Status_Description"), "ERROR", AddBillerScreen.this, new LoginScreen());
                                                validateBtn1.setEnabled(true);

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_EditBillerResult").getString("Status_Description"), "ERROR");
                                                validateBtn1.setEnabled(true);
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            validateBtn1.setEnabled(true);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn1.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn1.setEnabled(true);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn1.setEnabled(true);
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sBillerID", intent1.getStringExtra("billerID"));
//                            params.put("sSigns_Alias", editAlias.getText().toString());
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    validateBtn1.setEnabled(true);
                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn1.setEnabled(true);
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn1.setEnabled(true);
        }


    }

    public void showDilogForErrorForMenu(String msg, String header) {

        dialog1 = new Dialog(AddBillerScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog1.findViewById(R.id.validatingTxt);
        btn = (TextView) dialog1.findViewById(R.id.doneBtn);
        heading = (TextView) dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
                Intent intent = new Intent(AddBillerScreen.this, ManageBillerScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();


            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog1.show();

    }

}
