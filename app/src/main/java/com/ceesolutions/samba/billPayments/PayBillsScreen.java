package com.ceesolutions.samba.billPayments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.billPayments.Adapter.PayBillsAdapter;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.TapTarget;
import com.ceesolutions.samba.utils.TapTargetView;


/**
 * Created by ceeayaz on 4/10/18.
 */

public class PayBillsScreen extends AppCompatActivity {

    public static ViewPager viewPager;
    private ImageView backBtn, notificationBtn, helpBtn;
    private String type;
    private Constants constants;
    private boolean isFirst = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_bills);
        getSupportActionBar().hide();
//        TextView textView = (TextView) findViewById(R.id.text);
//        textView.bringToFront();

        try {
            CryptLib _crypt = new CryptLib();
            backBtn = findViewById(R.id.backBtn);
            helpBtn = findViewById(R.id.helpBtn);
            constants = Constants.getConstants(PayBillsScreen.this);
            type = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
            notificationBtn = findViewById(R.id.notificationBtn);
            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                Intent intent = new Intent(PayBillsScreen.this, HomeScreen.class);
//                overridePendingTransition(0, 0);
//                startActivity(intent);
                    finish();
                }
            });

            notificationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PayBillsScreen.this, MessageCenter.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
//                finish();
                }
            });
            TabLayout tabLayout = findViewById(R.id.tab_layout);
            tabLayout.addTab(tabLayout.newTab().setText("Pay Bill"));
            tabLayout.addTab(tabLayout.newTab().setText("Mobile Top-up"));
            tabLayout.addTab(tabLayout.newTab().setText("Request Bill Payment"));
            tabLayout.addTab(tabLayout.newTab().setText("Request Mobile Top-up"));
            tabLayout.addTab(tabLayout.newTab().setText("1Bill Payment"));
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
            Intent intent = getIntent();
            viewPager = findViewById(R.id.pager);
            final PayBillsAdapter adapter = new PayBillsAdapter
                    (getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapter);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {

                    if (!TextUtils.isEmpty(type) && type.equals("SAMBA")) {
                        viewPager.setCurrentItem(tab.getPosition());
                    } else {
                        viewPager.setCurrentItem(tab.getPosition());
                    }

                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

            String request = intent.getStringExtra("requestbill");
            if (!TextUtils.isEmpty(request)) {
                if (intent.getStringExtra("requestbill").equals("requestbill")) {
                    viewPager.setCurrentItem(0);
                } else if (intent.getStringExtra("requestbill").equals("requestvoucher")) {
                    viewPager.setCurrentItem(1);
                } else if (intent.getStringExtra("requestbill").equals("bill")) {
                    viewPager.setCurrentItem(0);
                } else if (intent.getStringExtra("requestbill").equals("nonSamba")) {
                    viewPager.setCurrentItem(2);
                } else if (intent.getStringExtra("requestbill").equals("voucher")) {
                    viewPager.setCurrentItem(3);
                } else if (intent.getStringExtra("requestbill").equals("payBill")) {
                    viewPager.setCurrentItem(2);
                } else if (intent.getStringExtra("requestbill").equals("payBillFromHome")) {
                    viewPager.setCurrentItem(0);
                } else if (intent.getStringExtra("requestbill").equals("topup")) {
                    viewPager.setCurrentItem(1);
                } else if (intent.getStringExtra("requestbill").equals("requesttopup")) {
                    viewPager.setCurrentItem(3);
                }
            }

            String coachMarks = constants.coachMarks.getString("payBill", "N/A");

//        if (!TextUtils.isEmpty(coachMarks) && coachMarks.equals("N/A")) {
//            constants.coachMarksEditor.putString("payBill","1");
//            constants.coachMarksEditor.commit();
//            constants.coachMarksEditor.apply();
//            int mainTxt = getResources().getDimensionPixelSize(R.dimen._7sdp);
//            int des = getResources().getDimensionPixelSize(R.dimen._4sdp);
//            TapTargetView.showFor(PayBillsScreen.this,                 // `this` is an Activity
//                    TapTarget.forView(findViewById(R.id.helpBtn), "About this Screen", "- Bill Payment: make payments of your registered bills\n\n" +
//                            "- Mobile Topup: topup or load balance to any prepaid mobile\n\n" +
//                            "- Request Bill Payment:,  a unique feature that gives you a facility to request your friend to pay your registered bill on your behalf. In this case, your friend has to be a Samba Customer / account holder\n\n" +
//                            "- Request Mobile Topup:  another unique feature that gives you a facility to request your friend to mobile topup or load balance on your behalf. In this case, your friend has to be a Samba Customer as well")
//                            // All options below are optional
//                            .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
//                            .outerCircleAlpha(0.90f)
//                            // Specify the alpha amount for the outer circle
//                            // Specify a color for the target circle
//                            .titleTextSize(mainTxt)// Specify the size (in sp) of the title text
//                            .titleTextColor(R.color.white)      // Specify the color of the title text
//                            .descriptionTextSize(des)            // Specify the size (in sp) of the description text
//                            .descriptionTextColor(R.color.white)  // Specify the color of the description text
//                            .textColor(R.color.white)            // Specify a color for both the title and description text
//                            .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
//                            // If set, will dim behind the view with 30% opacity of the given color
//                            .drawShadow(true)                   // Whether to draw a drop shadow or not
//                            .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
//                            .tintTarget(true)                   // Whether to tint the target view's color
//                            .transparentTarget(false),           // Specify whether the target is transparent (displays the content underneath)
//                    // Specify a custom drawable to draw as the target
//                    // Specify the target radius (in dp)
//                    new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
//                        @Override
//                        public void onTargetClick(TapTargetView view) {
//                            super.onTargetClick(view);      // This call is optional
////                        doSomething();
//                        }
//                    });
//        }

            helpBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int mainTxt = getResources().getDimensionPixelSize(R.dimen._7sdp);
                    int des = getResources().getDimensionPixelSize(R.dimen._4sdp);

                    TapTargetView.showFor(PayBillsScreen.this,                 // `this` is an Activity
                            TapTarget.forView(findViewById(R.id.helpBtn), "About this Screen!!!", "- Bill Payment: make payments of your registered bills\n\n" +
                                    "- Mobile Topup: topup or load balance to any prepaid mobile\n\n" +
                                    "- Request Bill Payment:,  a unique feature that gives you a facility to request your friend to pay your registered bill on your behalf. In this case, your friend has to be a Samba Customer / account holder\n\n" +
                                    "- Request Mobile Topup:  another unique feature that gives you a facility to request your friend to mobile topup or load balance on your behalf. In this case, your friend has to be a Samba Customer as well")
                                    // All options below are optional
                                    .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                    .outerCircleAlpha(0.90f)
                                    // Specify the alpha amount for the outer circle
                                    // Specify a color for the target circle
                                    .titleTextSize(mainTxt)// Specify the size (in sp) of the title text
                                    .titleTextColor(R.color.white)      // Specify the color of the title text
                                    .descriptionTextSize(des)            // Specify the size (in sp) of the description text
                                    .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                    .textColor(R.color.white)            // Specify a color for both the title and description text
                                    .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                                    // If set, will dim behind the view with 30% opacity of the given color
                                    .drawShadow(true)                   // Whether to draw a drop shadow or not
                                    .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                    .tintTarget(true)                   // Whether to tint the target view's color
                                    .transparentTarget(false),           // Specify whether the target is transparent (displays the content underneath)
                            // Specify a custom drawable to draw as the target
                            // Specify the target radius (in dp)
                            new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                                @Override
                                public void onTargetClick(TapTargetView view) {
                                    super.onTargetClick(view);      // This call is optional
//                        doSomething();

                                }
                            });
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        String fromPin = getIntent().getStringExtra("fromPin");
        if (!TextUtils.isEmpty(fromPin) && fromPin.equals("fromPin")) {

            Intent intent = new Intent(PayBillsScreen.this, HomeScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);
            finish();
        } else {
//        Intent intent = new Intent(PayBillsScreen.this, Hom.class);
//        overridePendingTransition(0, 0);
//        startActivity(intent);
            finish();
        }
    }

}
