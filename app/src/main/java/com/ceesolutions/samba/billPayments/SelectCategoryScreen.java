package com.ceesolutions.samba.billPayments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;

import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.billPayments.Adapter.CustomListAdapter;
import com.ceesolutions.samba.billPayments.Model.Category;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/9/18.
 */

public class SelectCategoryScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private ListView listView;
    private CustomListAdapter customListAdapter;
    private ArrayList<Category> categoryList;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private ImageView backBtn;
    private String type;
    private TextView headingTxt;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_bank);
        initViews();
        getSupportActionBar().hide();

        headingTxt.setText("Select Category");

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }

    public void initViews() {

        Intent intent = getIntent();

        listView = (ListView) findViewById(R.id.listView);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        headingTxt = (TextView) findViewById(R.id.headingTxt);
        categoryList = new ArrayList<>();
        utils = new Utils(SelectCategoryScreen.this);
        requestQueue = Volley.newRequestQueue(SelectCategoryScreen.this);
        helper = Helper.getHelper(SelectCategoryScreen.this);
        constants = Constants.getConstants(SelectCategoryScreen.this);
        pd = new Dialog(SelectCategoryScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        GetCategoryList();

        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
    }

    public void GetCategoryList() {


        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(SelectCategoryScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getCategoryListURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_BillCategoryResult").getString("Status_Code").equals("00")) {

                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_BillCategoryResult").getString("Local_BillCategoryList"));

                                                if (jsonArray.length() > 0) {

                                                    for (int i = 0; i < jsonArray.length(); i++) {

                                                        try {

                                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                            Category category = new Category();
                                                            category.setName(jsonObj.getString("Category_Name"));
                                                            category.setCategoryCompanyType(jsonObj.getString("Category_CompanyType"));
                                                            category.setId(jsonObj.getString("Category_ID"));
                                                            category.setType(jsonObj.getString("Category_CompanyType"));
                                                            category.setBillingType(jsonObj.getString("Category_BillingType"));
                                                            categoryList.add(category);

                                                        } catch (Exception ex) {

                                                            ex.printStackTrace();
                                                            pd.dismiss();
                                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        }

                                                    }

                                                    customListAdapter = new CustomListAdapter(categoryList, SelectCategoryScreen.this, type);
                                                    listView.setAdapter(customListAdapter);
                                                    pd.dismiss();
                                                } else {

                                                    pd.dismiss();

                                                }


                                            } else if (jsonObject.getJSONObject("Signs_BillCategoryResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillCategoryResult").getString("Status_Description"), "ERROR", SelectCategoryScreen.this, new LoginScreen());


                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_BillCategoryResult").getString("Status_Description"), "ERROR");

                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }


    }

}