package com.ceesolutions.samba.billPayments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;

import com.ceesolutions.samba.eatMubarak.EatMubarakMainScreen;
import com.google.android.material.navigation.NavigationView;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.Bookme.BookMeMainScreen;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.ATMLocatorScreen;
import com.ceesolutions.samba.accessControl.ContactUsScreen;
import com.ceesolutions.samba.accessControl.EditProfileScreen;
import com.ceesolutions.samba.accessControl.FeedbackScreen;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.accessControl.SettingsScreen;
import com.ceesolutions.samba.beneficiariesManagement.ManageBeneficiaresScreen;
import com.ceesolutions.samba.cardManagement.ManageCardsScreen;
import com.ceesolutions.samba.friends.ManageFriends;
import com.ceesolutions.samba.masterCardIntegration.MainActivity;
import com.ceesolutions.samba.transactions.AccountsScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.views.RoundImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/10/18.
 */

public class AddBillerConfirmationScreen extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static double latitude;
    public static double longitude;
    public ProgressBar progress;
    public String longitude1;
    public String latitude1;
    private TextView logout, btn, refTxt, payBillBtn, textView, doneBtn, heading, progressTxt, aliasTxt, billerTxt, timeTxt, consumerNameTxt, consumerNumberTxt, accFrindMaskTxt, totalAmountTxt, amountInWordsTxt, reviewBtn, addToFavBtn, dateTxt;
    private ImageView backBtn, cameraBtn;
    private File mFile, mFile1;
    private ImageView profile_image;
    private String billerID;
    private RequestQueue requestQueue;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private Dialog dialog, dialog1;
    private String menu, userType;
    private Intent intent1;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.biller_nav);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        View view = findViewById(R.id.confirmation);
        initViews(view);
        try {
            CryptLib _crypt = new CryptLib();
            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }
            menu = constants.sharedPreferences.getString("Menus", "N/A");
            if (!TextUtils.isEmpty(menu) && !menu.equals("N/A") && !menu.equals("")) {


            } else {

                menu = "N/A";
            }

            NavigationView navigationView = findViewById(R.id.nav_view);
            Menu navMenu = navigationView.getMenu();
            navigationView.setItemIconTintList(null);
//        navMenu.add(0,R.id.home,0,)
            navigationView.setNavigationItemSelectedListener(this);

            if (!menu.equals("N/A")) {

                if (menu.toLowerCase().contains("home")) {


                } else {
                    MenuItem home = navMenu.add(0, R.id.home, 0, "Home");
                    home.setIcon(R.drawable.home);

                }

                if (menu.toLowerCase().contains("accounts")) {

                } else {

                    MenuItem accounts = navMenu.add(0, R.id.accounts, 0, "Accounts");
                    accounts.setIcon(R.drawable.accounts);

                    MenuItem qrPayment = navMenu.add(0, R.id.qr_payment, 0, "QR Payment");
                    qrPayment.setIcon(R.drawable.qr);
                }

                if (menu.toLowerCase().contains("beneficiary management")) {

                } else {

                    MenuItem beneManagement = navMenu.add(0, R.id.manageBene, 0, "Beneficiary Management");
                    beneManagement.setIcon(R.drawable.beneficiary);
                }

                if (menu.toLowerCase().contains("friends management")) {

                } else {

                    MenuItem friends = navMenu.add(0, R.id.manageFriends, 0, "Friends Management");
                    friends.setIcon(R.drawable.friends);
                }

                if (menu.toLowerCase().contains("biller management")) {

                } else {

                    MenuItem billPayments = navMenu.add(0, R.id.billPayments, 0, "Biller Management");
                    billPayments.setIcon(R.drawable.receipt);

                }

                if (menu.toLowerCase().contains("cards management")) {

                } else {

                    MenuItem cardManagement = navMenu.add(0, R.id.cardManagement, 0, "Cards Management");
                    cardManagement.setIcon(R.drawable.credit_card);

                }

                if (menu.toLowerCase().contains("feedback")) {

                } else {

                    MenuItem feedback = navMenu.add(0, R.id.feedback, 0, "Customer Feedback");
                    feedback.setIcon(R.drawable.chat_smiley);

                }

            } else {

                Menu firstGroup = navigationView.getMenu();
                MenuItem home = firstGroup.add(R.id.first_group, R.id.home, 0, "Home");
                home.setIcon(R.drawable.home);

                MenuItem accounts = firstGroup.add(R.id.first_group, R.id.accounts, 0, "Accounts");
                accounts.setIcon(R.drawable.accounts);

                MenuItem qr = firstGroup.add(R.id.first_group, R.id.qr_payment, 0, "QR Payment");
                qr.setIcon(R.drawable.qr);

                MenuItem cardManagement = firstGroup.add(R.id.first_group, R.id.cardManagement, 0, "Cards Management");
                cardManagement.setIcon(R.drawable.credit_card);

                firstGroup.setGroupCheckable(R.id.first_group, true, true);
                firstGroup.setGroupVisible(R.id.first_group, true);

                Menu secondGroup = navigationView.getMenu();
                MenuItem ticket = secondGroup.add(R.id.second_group, R.id.ticket, 0, "E-Tickets");
                ticket.setIcon(R.drawable.ticket);

                MenuItem food = secondGroup.add(R.id.second_group, R.id.food, 0, "Food");
                food.setIcon(R.drawable.eat_mubarak_icon_left);


                MenuItem beneManagement = secondGroup.add(R.id.second_group, R.id.manageBene, 0, "Beneficiary Management");
                beneManagement.setIcon(R.drawable.beneficiary);

                MenuItem friends = secondGroup.add(R.id.second_group, R.id.manageFriends, 0, "Friends Management");
                friends.setIcon(R.drawable.friends);

                MenuItem billPayments = secondGroup.add(R.id.second_group, R.id.billPayments, 0, "Biller Management");
                billPayments.setIcon(R.drawable.receipt);
                secondGroup.setGroupCheckable(R.id.second_group, true, true);
                secondGroup.setGroupVisible(R.id.second_group, true);

                Menu thirdGroup = navigationView.getMenu();
                MenuItem location = thirdGroup.add(R.id.third_group, R.id.location, 0, "Locate Us");
                location.setIcon(R.drawable.location);

                Menu fourthGroup = navigationView.getMenu();
                MenuItem legal = fourthGroup.add(R.id.fourth_group, R.id.legal, 0, "Settings");
                legal.setIcon(R.drawable.settingsblack);

                MenuItem feedback = fourthGroup.add(R.id.fourth_group, R.id.feedback, 0, "Customer Feedback");
                feedback.setIcon(R.drawable.chat_smiley);

                MenuItem contact = fourthGroup.add(R.id.fourth_group, R.id.contact, 0, "Contact Us");
                contact.setIcon(R.drawable.phone);
            }
            final DrawerLayout drawer = findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            ImageView notificationBtn = toolbar.findViewById(R.id.notificationBtn);

            notificationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    takeScreenshot();
                }
            });


            navigationView.setNavigationItemSelectedListener(this);
            View headerLayout = navigationView.getHeaderView(0);
            TextView textView = headerLayout.findViewById(R.id.profileName);
            ImageView profile = headerLayout.findViewById(R.id.profile_image);
            RelativeLayout relativeLayout = headerLayout.findViewById(R.id.relativeLayout);
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                    Intent intent = new Intent(AddBillerConfirmationScreen.this, EditProfileScreen.class);
                    intent.putExtra("view", "view");
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }
            });

            profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                    Intent intent = new Intent(AddBillerConfirmationScreen.this, EditProfileScreen.class);
                    intent.putExtra("view", "edit");
                    overridePendingTransition(0, 0);
                    startActivity(intent);

                }
            });
            progress = headerLayout.findViewById(R.id.progress);
            progressTxt = headerLayout.findViewById(R.id.progressTxt);

            String str = _crypt.decrypt(constants.sharedPreferences.getString("UserProgress", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());

            progressTxt.setText(str + "% Profile Completed");

            int pro = Integer.valueOf(str);
            progress.setProgress(pro);

            String image = _crypt.decrypt(constants.sharedPreferences.getString("ProfilePic", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());

            int color;
            TextView titleTxt = headerLayout.findViewById(R.id.titleTxt);
            RoundImageView titleImage = headerLayout.findViewById(R.id.titleImage);
            String T24_FullName = _crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
            String s = T24_FullName.substring(0, 1).toUpperCase();
            switch (s.toUpperCase()) {

                case "A":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "B":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "C":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "D":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "E":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "F":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "G":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "H":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "I":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "J":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "K":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "L":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "M":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "N":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "O":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "P":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "Q":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "R":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "S":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "T":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "U":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "V":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "W":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "X":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "Y":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "Z":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                default:
                    color = getResources().getColor(R.color.defaultColor);
                    break;
            }

            try {

                Log.d("---0", "" + image);
                if (!TextUtils.isEmpty(image) && !image.equals("N/A")) {
                    image = image.replaceAll("%2B", "+");
                    titleTxt.setVisibility(View.INVISIBLE);
                    titleImage.setVisibility(View.INVISIBLE);
                    byte[] imageByteArray = Base64.decode(image, Base64.DEFAULT);
                    RequestOptions options = new RequestOptions();
                    options.centerCrop();
                    Glide.with(AddBillerConfirmationScreen.this)
                            .load(imageByteArray)
                            .apply(options)
                            .into(profile);
                } else {

                    profile.setVisibility(View.INVISIBLE);
                    titleImage.setBackgroundTintList(ColorStateList.valueOf((color)));
                    titleTxt.setText(s);
                }
            } catch (Exception e) {

                e.printStackTrace();
            }


            titleTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                    Intent intent = new Intent(AddBillerConfirmationScreen.this, EditProfileScreen.class);
                    intent.putExtra("view", "edit");
                    overridePendingTransition(0, 0);
                    startActivity(intent);

                }
            });
            ImageView imageView = headerLayout.findViewById(R.id.settings);
            textView.setText(_crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                    Intent intent = new Intent(AddBillerConfirmationScreen.this, SettingsScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);


                }
            });

            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                    dialog.show();
                    showDialog("Are you sure you want to Logout?");
                }
            });

            reviewBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(AddBillerConfirmationScreen.this, ManageBillerScreen.class);
                    intent.putExtra("addBiller", "addBiller");
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                    finish();
                }
            });

            addToFavBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showDilogForError("Are you sure you want to mark the biller as favourite?", "CONFIRMATION", "fav");
                }
            });

            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                finish();
                }
            });

            cameraBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    takeScreenshot();
                }
            });

            payBillBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    payBillBtn.setEnabled(false);
                    BillInquiry();

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void initViews(View view) {

        intent1 = getIntent();
        billerTxt = view.findViewById(R.id.billerTxt);
        consumerNameTxt = view.findViewById(R.id.consumerNameTxt);
        consumerNumberTxt = view.findViewById(R.id.consumerNumberTxt);
        reviewBtn = view.findViewById(R.id.reviewBtn);
        profile_image = view.findViewById(R.id.profile_image);
        addToFavBtn = view.findViewById(R.id.addToFavBtn);
        payBillBtn = view.findViewById(R.id.payBillBtn);
        timeTxt = view.findViewById(R.id.timeTxt);
        dateTxt = view.findViewById(R.id.dateTxt);
        dateTxt.setText(intent1.getStringExtra("finalDate"));
        timeTxt.setText(intent1.getStringExtra("currentTime"));
        aliasTxt = view.findViewById(R.id.aliasTxt);
        logout = findViewById(R.id.logout);
        addToFavBtn.bringToFront();
        requestQueue = Volley.newRequestQueue(AddBillerConfirmationScreen.this);
        helper = Helper.getHelper(AddBillerConfirmationScreen.this);
        constants = Constants.getConstants(AddBillerConfirmationScreen.this);
        backBtn = view.findViewById(R.id.backBtn);
        cameraBtn = view.findViewById(R.id.cameraBtn);
        aliasTxt.setText(intent1.getStringExtra("alias"));
        billerTxt.setText(intent1.getStringExtra("CompanyName"));
        String name = intent1.getStringExtra("CompanyName");
        utils = new Utils(AddBillerConfirmationScreen.this);
        consumerNameTxt.setText(intent1.getStringExtra("ConsumerName"));
        consumerNumberTxt.setText(intent1.getStringExtra("consumerNumber"));
        billerID = intent1.getStringExtra("BillerID");
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        mFile1 = new File(Environment.
                getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Samba");

        if (!mFile1.exists() && !mFile1.mkdirs()) {
//
//            Log.d("---", "Can't create directory to save image");
//            Toast.makeText(this, "Can't make path to save pic.",
//                    Toast.LENGTH_LONG).show();
            mFile1.mkdir();
            return;

        }


        switch (name.toLowerCase()) {

            case "lesco": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.lesco));

                break;
            }

            case "k-electric": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.kesc));

                break;

            }

            case "qesco": {


                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.qesco));

                break;
            }

            case "hesco": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.hesco));

                break;

            }

            case "ajked": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.ajked));

                break;

            }

            case "kwsb": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.kwsb));

                break;

            }

            case "sco": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.sco));

                break;

            }

            case "fesco": {


                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.fesco));

                break;
            }

            case "sepco": {


                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.sepco));

                break;
            }

            case "fwasa": {


                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.fwasa));

                break;
            }

            case "iesco": {


                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.iesco));

                break;
            }

            case "ptcl": {


                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.ptcl));

                break;
            }

            case "sngpl": {


                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.sngpl));

                break;
            }

            case "ssgc": {


                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.ssgc));

                break;
            }

            case "pesco": {


                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.pesco));

                break;
            }

            case "mesco": {


                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.mesco));

                break;
            }

            case "gepco": {


                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.gepco));

                break;
            }

            case "mobilink prepaid": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.mobilink));

                break;
            }

            case "mobilink postpaid": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.mobilink));

                break;
            }

            case "telenor postpaid": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.telenor));

                break;
            }

            case "telenor prepaid": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.telenor));

                break;
            }

            case "ufone prepaid": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.ufone));

                break;
            }

            case "ufone postpaid": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.ufone));

                break;
            }

            case "warid prepaid": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.warid));

                break;
            }

            case "warid postpaid": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.warid));

                break;
            }

            case "kuickpay": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.quick));

                break;
            }
            case "fbr": {

                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.fbr));

                break;
            }

            default: {


                profile_image.setBackground(ContextCompat.getDrawable(AddBillerConfirmationScreen.this, R.drawable.default1));

                break;
            }

        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            Intent intent = new Intent(AddBillerConfirmationScreen.this, HomeScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);
            finish();
        } else if (id == R.id.accounts) {

            Intent intent = new Intent(AddBillerConfirmationScreen.this, AccountsScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);
//            finish();

        } else if (id == R.id.manageBene) {

            if (userType.equals("NON_SAMBA")) {
                showDilogForErrorForMenu("This feature is only available for Samba Users.", "WARNING");
            } else {
                Intent intent1 = new Intent(AddBillerConfirmationScreen.this, ManageBeneficiaresScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent1);
//            finish();
            }
        } else if (id == R.id.manageFriends) {

            Intent intent2 = new Intent(AddBillerConfirmationScreen.this, ManageFriends.class);
            overridePendingTransition(0, 0);
            startActivity(intent2);
//            finish();

        } else if (id == R.id.billPayments) {

            Intent intent2 = new Intent(AddBillerConfirmationScreen.this, ManageBillerScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent2);
//            finish();

        } else if (id == R.id.cardManagement) {

            if (userType.equals("NON_SAMBA")) {
                showDilogForErrorForMenu("This feature is only available for Samba Users.", "WARNING");
            } else {
                Intent intent2 = new Intent(AddBillerConfirmationScreen.this, ManageCardsScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent2);
//            finish();
            }

        } else if (id == R.id.feedback) {

            Intent intent2 = new Intent(AddBillerConfirmationScreen.this, FeedbackScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent2);
//            finish();
        } else if (id == R.id.contact) {

            Intent intent2 = new Intent(AddBillerConfirmationScreen.this, ContactUsScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent2);
//            finish();
        } else if (id == R.id.location) {

            Intent intent2 = new Intent(AddBillerConfirmationScreen.this, ATMLocatorScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent2);
//            finish();
        } else if (id == R.id.legal) {

            Intent intent = new Intent(AddBillerConfirmationScreen.this, SettingsScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);


//            finish();
        } else if (id == R.id.ticket) {
            if (userType.equals("NON_SAMBA"))
                showDilogForErrorForMenu("This feature is only available for Samba Users.", "WARNING");
            else {
                Intent intent = new Intent(AddBillerConfirmationScreen.this, BookMeMainScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);

            }
//            finish();
        }
        else if (id == R.id.food) {
            if (userType.equals("NON_SAMBA"))
                showDilogForErrorForMenu("This feature is only available for Samba Users.", "WARNING");
            else {
                Intent intent = new Intent(AddBillerConfirmationScreen.this, EatMubarakMainScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);

            }
//            finish();
        }
        else if (id == R.id.qr_payment) {
            if (userType.equals("NON_SAMBA"))
                showDilogForErrorForMenu("This feature is only available for Samba Users.", "WARNING");
            else {
                Intent intent = new Intent(AddBillerConfirmationScreen.this, MainActivity.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
            }
//            finish();

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
//            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";
            String filename = mFile1.getPath() + File.separator + System.currentTimeMillis() + ".jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

//            File imageFile = new File(mPath);
            mFile = new File(filename);

            FileOutputStream outputStream = new FileOutputStream(mFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            utils.showDilogForError("Screen shot has been taken successfully.", "CONFIRMATION");

            helper.galleryAddPic(filename);
//            openScreenshot(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }

    public void AddBillerToFav() {


        try {
            dialog.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());

                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sSigns_BillerFavoriteFlag", "1");
                    params.put("sBillerID", billerID);
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(AddBillerConfirmationScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.favBillerURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_BillerFavorite_Mark_UnMarkResult").getString("Status_Code").equals("00")) {

                                                dialog.dismiss();
                                                Intent intent = new Intent(AddBillerConfirmationScreen.this, ManageBillerScreen.class);
                                                intent.putExtra("addBiller", "addBiller");
                                                overridePendingTransition(0, 0);
                                                startActivity(intent);
                                                finish();

                                            } else if (jsonObject.getJSONObject("Signs_BillerFavorite_Mark_UnMarkResult").getString("Status_Code").equals("54")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillerFavorite_Mark_UnMarkResult").getString("Status_Description"), "ERROR", AddBillerConfirmationScreen.this, new LoginScreen());


                                            } else {

                                                dialog.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_BillerFavorite_Mark_UnMarkResult").getString("Status_Description"), "ERROR");
                                            }
                                        } else {

                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sSigns_BillerFavoriteFlag", "1");
//                            params.put("sBillerID", billerID);
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };
                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            dialog.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }


    }

    public void showDilogForError(String msg, String header, final String type) {

        dialog1 = new Dialog(AddBillerConfirmationScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        textView = dialog1.findViewById(R.id.validatingTxt);
        doneBtn = dialog1.findViewById(R.id.doneBtn);
        heading = dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(true);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (type.equals("fav")) {
                    dialog1.dismiss();
                    AddBillerToFav();
                }

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog1.show();

    }

    public void BillInquiry() {

        final int stanNumber = 6;

        try {
            dialog.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());

                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sBillConsumerNumber", intent1.getStringExtra("consumerNumber"));
                    params.put("sDistributorID", intent1.getStringExtra("companyID"));
                    params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(AddBillerConfirmationScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.billInquiryURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            try {

                                                CryptLib _crypt = new CryptLib();
                                                if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("00")) {

                                                    Intent intent = new Intent(AddBillerConfirmationScreen.this, PayBillsScreen.class);
                                                    intent.putExtra("requestbill", "bill");
                                                    constants.destinationAccountEditor.putString("isSelect", "Yes");
                                                    constants.destinationAccountEditor.putString("AmountAfterDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountAfterDueDate"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("AmountWithinDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountWithinDueDate"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillStatus", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillStatus"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillingMonth", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillingMonth"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("CompanyID", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyID"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("DueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("DueDate"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("ConsumerName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("ConsumerName"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("CompanyName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyName"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("RRN", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("RRN"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillConsumerNumber", _crypt.encryptForParams(intent1.getStringExtra("consumerNumber"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("DistributorID", _crypt.encryptForParams(intent1.getStringExtra("companyID"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("distributorAccountNumber", _crypt.encryptForParams(intent1.getStringExtra("distributorAccount"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("alias", _crypt.encryptForParams(intent1.getStringExtra("alias"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("categoryType", _crypt.encryptForParams(intent1.getStringExtra("categoryName"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.commit();
                                                    constants.destinationAccountEditor.apply();
                                                    dialog.dismiss();
                                                    payBillBtn.setEnabled(true);
                                                    overridePendingTransition(0, 0);
                                                    startActivity(intent);

                                                } else if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("54")) {
                                                    dialog.dismiss();
                                                    utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR", AddBillerConfirmationScreen.this, new LoginScreen());
                                                    payBillBtn.setEnabled(true);

                                                } else {

                                                    dialog.dismiss();
                                                    utils.showDilogForError(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR");
                                                    payBillBtn.setEnabled(true);

                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                        } else {

                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                            payBillBtn.setEnabled(true);

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        payBillBtn.setEnabled(true);

                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        payBillBtn.setEnabled(true);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        payBillBtn.setEnabled(true);

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sBillConsumerNumber", intent1.getStringExtra("consumerNumber"));
//                            params.put("sDistributorID", intent1.getStringExtra("companyID"));
//                            params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    payBillBtn.setEnabled(true);

                }
            } else {
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                payBillBtn.setEnabled(true);

            }
        } catch (Exception e) {


            dialog.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            payBillBtn.setEnabled(true);

        }
    }

    public void showDilogForErrorForMenu(String msg, String header) {

        dialog1 = new Dialog(AddBillerConfirmationScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        textView = dialog1.findViewById(R.id.validatingTxt);
        btn = dialog1.findViewById(R.id.doneBtn);
        heading = dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog1.show();

    }

    private void showDialog(String message) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Logout:");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialogInterface, int i) {

//                constants.editor.clear();
//                constants.editor.commit();
//                constants.editor.apply();
//                constants.nonSambaCredentialsEditor.clear();
//                constants.nonSambaCredentialsEditor.commit();
//                constants.nonSambaCredentialsEditor.apply();
//                constants.destinationAccountEditor.clear().commit();
//                constants.destinationAccountEditor.apply();
//                constants.sourceAccountEditor.clear().commit();
//                constants.sourceAccountEditor.apply();
//                constants.accountListEditor.clear().commit();
//                constants.accountListEditor.apply();
//                constants.accountsEditor.clear().commit();
//                constants.accountsEditor.apply();
//                constants.forgotPasswordEditor.clear().commit();
//                constants.forgotPasswordEditor.apply();
//                constants.purposeBillList.clear().commit();
//                constants.purposeBillList.apply();
//                constants.purposePrepaidList.clear().commit();
//                constants.purposePrepaidList.apply();
//                constants.purposeListEditor.clear().commit();
//                constants.purposeListEditor.apply();
//                Handler handler1 = new Handler();
//                handler1.postDelayed(new Runnable() {
//                    public void run() {
//
//                        dialogInterface.cancel();
//                        dialog.dismiss();
//                        Intent intent = new Intent(AddBillerConfirmationScreen.this, LoginScreen.class);
//                        startActivity(intent);
//                        overridePendingTransition(0, 0);
//                        finish();
//                        finishAffinity();
//
//                    }
//                }, 1000);
                Logout();
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog1, int which) {
                dialog1.cancel();
                dialog.dismiss();
            }
        });

        builder.show();
    }

    public void Logout() {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());

                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSessionID", Constants.Shared_KEY);
                    HttpsTrustManager.allowMySSL(AddBillerConfirmationScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.logoutURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Code").equals("00")) {
                                                constants.editor.clear();
                                                constants.editor.commit();
                                                constants.editor.apply();
                                                constants.nonSambaCredentialsEditor.clear();
                                                constants.nonSambaCredentialsEditor.commit();
                                                constants.nonSambaCredentialsEditor.apply();
                                                constants.destinationAccountEditor.clear().commit();
                                                constants.destinationAccountEditor.apply();
                                                constants.sourceAccountEditor.clear().commit();
                                                constants.sourceAccountEditor.apply();
                                                constants.accountListEditor.clear().commit();
                                                constants.accountListEditor.apply();
                                                constants.accountsEditor.clear().commit();
                                                constants.accountsEditor.apply();
                                                constants.forgotPasswordEditor.clear().commit();
                                                constants.forgotPasswordEditor.apply();
                                                constants.purposeBillList.clear().commit();
                                                constants.purposeBillList.apply();
                                                constants.purposePrepaidList.clear().commit();
                                                constants.purposePrepaidList.apply();
                                                constants.purposeListEditor.clear().commit();
                                                constants.purposeListEditor.apply();
                                                dialog.dismiss();
                                                Intent intent = new Intent(AddBillerConfirmationScreen.this, LoginScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                finish();
                                                finishAffinity();


                                            } else {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Description"), "ERROR", AddBillerConfirmationScreen.this, new LoginScreen());

                                            }

                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sSessionID", Constants.Shared_KEY);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                }
            } else {
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity", "WARNING");


            }
        } catch (Exception e) {

            dialog.dismiss();
            utils.showDilogForError("Please check your internet connectivity", "WARNING");


        }
    }
}

