package com.ceesolutions.samba.billPayments.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.beneficiariesManagement.BeneficiaryFormScreen;
import com.ceesolutions.samba.beneficiariesManagement.Model.BankModel;
import com.ceesolutions.samba.billPayments.AddBillerScreen;
import com.ceesolutions.samba.billPayments.Model.Biller;
import com.ceesolutions.samba.billPayments.Model.Distributor;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.views.CircularTextView;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ceeayaz on 4/9/18.
 */

public class CustomListBillerAdapter extends BaseAdapter {

    ArrayList<Distributor> billerDistributorList;
    LayoutInflater layoutInflater;
    View row;
    Context context;
    TextView companyName;
    CircularTextView title;
    int[] androidColors;
    String type;
    private Constants constants;

    public CustomListBillerAdapter(ArrayList<Distributor> billerDistributorArrayList, Context mContext, String isType) {
        billerDistributorList = billerDistributorArrayList;
        context = mContext;
        constants = Constants.getConstants(context);
        type = isType;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return billerDistributorList.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.category_biller_list_item, parent, false);
        }

        androidColors = context.getResources().getIntArray(R.array.androidcolors);
        title = (CircularTextView) convertView.findViewById(R.id.title);
        companyName = (TextView) convertView.findViewById(R.id.bankName);

//        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];


        title.setSolidColor(String.format("#%06X", (0xFFFFFF & billerDistributorList.get(position).getDistributorColor())));
        title.setText(billerDistributorList.get(position).getFirstLetter());

        companyName.setText(billerDistributorList.get(position).getDistributorName());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    FirebaseAnalytics.getInstance(context).logEvent("Distributor_Selected", new Bundle());

                    CryptLib _crypt = new CryptLib();
                    if (!TextUtils.isEmpty(type) && type.equals("destination")) {

                        constants.destinationAccountEditor.putString("isSelect", "Yes");
                        constants.destinationAccountEditor.putString("distributorID", _crypt.encryptForParams(billerDistributorList.get(position).getDistributorID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("distributorAccountNumber", _crypt.encryptForParams(billerDistributorList.get(position).getAccountNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("maxAmount", _crypt.encryptForParams(billerDistributorList.get(position).getAllowedMaxAmount(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("minAmount", _crypt.encryptForParams(billerDistributorList.get(position).getAllowedMinAmount(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("distributorName", _crypt.encryptForParams(billerDistributorList.get(position).getDistributorName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("length", _crypt.encryptForParams(billerDistributorList.get(position).getConsumerNoLength(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.commit();
                        constants.destinationAccountEditor.apply();
                        ((Activity) context).finish();

                    } else {

                        Intent intent = new Intent(context, AddBillerScreen.class);
                        intent.putExtra("length", billerDistributorList.get(position).getConsumerNoLength());
                        intent.putExtra("companyName", billerDistributorList.get(position).getDistributorName());
                        intent.putExtra("id", billerDistributorList.get(position).getDistributorID());
                        intent.putExtra("categoryID", billerDistributorList.get(position).getCategoryID());
                        intent.putExtra("categoryName", billerDistributorList.get(position).getCategoryName());
                        intent.putExtra("distributorAccount", billerDistributorList.get(position).getAccountNumber());
                        intent.putExtra("length", billerDistributorList.get(position).getConsumerNoLength());
                        intent.putExtra("display", billerDistributorList.get(position).getDisplayText());
                        ((Activity) context).overridePendingTransition(0, 0);
                        context.startActivity(intent);


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        return convertView;
    }
}