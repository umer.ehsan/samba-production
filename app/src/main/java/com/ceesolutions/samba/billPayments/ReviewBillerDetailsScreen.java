package com.ceesolutions.samba.billPayments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/10/18.
 */

public class ReviewBillerDetailsScreen extends AppCompatActivity {


    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    boolean flag = false;
    private ExpandableLayout expandableLayout0;
    private RelativeLayout firstRow;
    private TextView message, mainTxt, textView, doneBtn, heading, date, billerTxt, accMaskTxt, aliasTxt, accFrindMaskTxt, accFrindEmailTxt, accFrindMobileTxt, consumerNameTxt, consumerNumberTxt, purposeHeadingTxt, remarksHeadingTxt, conversionHeadingTxt, chargesHeadingTxt, reviewBtn, statusHeadingTxt;
    private ImageView backBtn, cancelBtn;
    private Dialog dialog;
    private RequestQueue requestQueue;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private String amountFinal, amountWords, finalDate, currentTime;
    private Intent intent1;
    private Date currentDate;
    private ImageView profile_image;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review_biller);
        initViews();
        getSupportActionBar().hide();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");
                Intent intent = new Intent(ReviewBillerDetailsScreen.this, ManageBillerScreen.class);
                intent.putExtra("addBiller", "addBiller");
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
                finishAffinity();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                finish();
            }
        });

        reviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reviewBtn.setEnabled(false);
                AddBiller();
            }
        });
    }

    public void initViews() {

        intent1 = getIntent();
        date = (TextView) findViewById(R.id.date);
        billerTxt = (TextView) findViewById(R.id.billerTxt);
        profile_image = (ImageView) findViewById(R.id.profile_image);
        aliasTxt = (TextView) findViewById(R.id.aliasTxt);
        consumerNameTxt = (TextView) findViewById(R.id.consumerNameTxt);
        consumerNumberTxt = (TextView) findViewById(R.id.consumerNumberTxt);
        purposeHeadingTxt = (TextView) findViewById(R.id.purposeHeadingTxt);
        statusHeadingTxt = (TextView) findViewById(R.id.statusHeadingTxt);
        remarksHeadingTxt = (TextView) findViewById(R.id.remarksHeadingTxt);
        conversionHeadingTxt = (TextView) findViewById(R.id.conversionHeadingTxt);
        chargesHeadingTxt = (TextView) findViewById(R.id.chargesHeadingTxt);
        reviewBtn = (TextView) findViewById(R.id.reviewBtn);
        cancelBtn = (ImageView) findViewById(R.id.cancelBtn);
        requestQueue = Volley.newRequestQueue(ReviewBillerDetailsScreen.this);
        helper = Helper.getHelper(ReviewBillerDetailsScreen.this);
        constants = Constants.getConstants(ReviewBillerDetailsScreen.this);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        utils = new Utils(ReviewBillerDetailsScreen.this);
        mainTxt = (TextView) findViewById(R.id.mainTxt);
        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        firstRow = (RelativeLayout) findViewById(R.id.firstRow);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        currentDate = Calendar.getInstance().getTime();
        formatter = new SimpleDateFormat("dd-MMM-yyyy");
        date.setText(formatter.format(currentDate));
        finalDate = formatter.format(currentDate);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
        currentTime = sdf.format(currentDate);
        Intent intent = getIntent();
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        String name = intent.getStringExtra("CompanyName");
        Log.d("---", "---" + name);
        switch (name.toLowerCase()) {

            case "lesco": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.lesco));

                break;
            }

            case "k-electric": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.kesc));

                break;

            }

            case "qesco": {


                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.qesco));

                break;
            }

            case "hesco": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.hesco));

                break;

            }

            case "ajked": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.ajked));

                break;

            }

            case "kwsb": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.kwsb));

                break;

            }

            case "sco": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.sco));

                break;

            }

            case "fesco": {


                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.fesco));

                break;
            }

            case "sepco": {


                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.sepco));

                break;
            }

            case "fwasa": {


                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.fwasa));

                break;
            }

            case "iesco": {


                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.iesco));

                break;
            }

            case "ptcl": {


                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.ptcl));

                break;
            }

            case "sngpl": {


                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.sngpl));

                break;
            }

            case "ssgc": {


                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.ssgc));

                break;
            }

            case "pesco": {


                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.pesco));

                break;
            }

            case "mesco": {


                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.mesco));

                break;
            }

            case "gepco": {


                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.gepco));

                break;
            }

            case "mobilink prepaid": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.mobilink));

                break;
            }

            case "mobilink postpaid": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.mobilink));

                break;
            }

            case "telenor postpaid": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.telenor));

                break;
            }

            case "telenor prepaid": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.telenor));

                break;
            }

            case "ufone prepaid": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.ufone));

                break;
            }

            case "ufone postpaid": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.ufone));

                break;
            }
            case "warid prepaid": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.warid));

                break;
            }

            case "warid postpaid": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.warid));

                break;
            }
            case "kuickpay": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.quick));

                break;
            }
            case "fbr": {

                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.fbr));

                break;
            }
            default: {


                profile_image.setBackground(ContextCompat.getDrawable(ReviewBillerDetailsScreen.this, R.drawable.default1));

                break;
            }

        }
        firstRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!flag) {
                    expandableLayout0.expand();
                    mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_less, 0);
                    flag = true;
                } else {
                    expandableLayout0.collapse();
                    mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_more, 0);
                    flag = false;
                }

            }
        });

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        intent1 = intent;
        billerTxt.setText(intent.getStringExtra("CompanyName"));
        purposeHeadingTxt.setText(intent.getStringExtra("AmountWithinDueDate"));
        aliasTxt.setText(intent.getStringExtra("alias"));
        remarksHeadingTxt.setText(intent.getStringExtra("AmountAfterDueDate"));
        conversionHeadingTxt.setText(intent.getStringExtra("BillingMonth"));
        chargesHeadingTxt.setText(intent.getStringExtra("DueDate"));
        statusHeadingTxt.setText(intent.getStringExtra("BillStatus"));
        consumerNumberTxt.setText(intent.getStringExtra("consumerNumber"));
        consumerNameTxt.setText(intent.getStringExtra("ConsumerName"));

    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(ReviewBillerDetailsScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
//        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


    public void AddBiller() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {
            dialog.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sCategoryID", intent1.getStringExtra("categoryID"));
                    params.put("sSigns_Alias", intent1.getStringExtra("alias"));
                    params.put("sCompanyID", intent1.getStringExtra("CompanyID"));
                    params.put("sCategoryName", intent1.getStringExtra("categoryName"));
                    params.put("sCompanyName", intent1.getStringExtra("CompanyName"));
                    params.put("sConsumerNo", intent1.getStringExtra("consumerNumber"));
                    params.put("sConsumerName", intent1.getStringExtra("ConsumerName"));
                    params.put("sBillingMonth", intent1.getStringExtra("BillingMonth"));
                    params.put("sAmtBefore", intent1.getStringExtra("AmountWithinDueDate").replaceAll(",", ""));
                    params.put("sAmtAfter", intent1.getStringExtra("AmountAfterDueDate").replaceAll(",", ""));
                    params.put("sDueDate", intent1.getStringExtra("DueDate"));
                    params.put("sBillStatus", intent1.getStringExtra("BillStatus"));
                    params.put("sComments", "-");
                    params.put("sDistAccountNumber", intent1.getStringExtra("distributorAccount"));
                    params.put("sSigns_MobileNumber", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sSigns_EmailAddress", _crypt.decrypt(constants.sharedPreferences.getString("EmailAddress", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(ReviewBillerDetailsScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.addBillerURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_AddBillerResult").getString("Status_Code").equals("00")) {


                                                dialog.dismiss();
                                                Intent intent = new Intent(ReviewBillerDetailsScreen.this, AddBillerConfirmationScreen.class);
                                                intent.putExtra("alias", intent1.getStringExtra("alias"));
                                                intent.putExtra("CompanyName", intent1.getStringExtra("CompanyName"));
                                                intent.putExtra("consumerNumber", intent1.getStringExtra("consumerNumber"));
                                                intent.putExtra("ConsumerName", intent1.getStringExtra("ConsumerName"));
                                                intent.putExtra("categoryName", intent1.getStringExtra("categoryName"));
                                                intent.putExtra("distributorAccount", intent1.getStringExtra("distributorAccount"));
                                                intent.putExtra("companyID", intent1.getStringExtra("CompanyID"));
                                                intent.putExtra("BillerID", jsonObject.getJSONObject("Signs_AddBillerResult").getString("BillerID"));
                                                intent.putExtra("finalDate", finalDate);
                                                intent.putExtra("currentTime", currentTime);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                overridePendingTransition(0, 0);
                                                startActivity(intent);
                                                finish();
                                                finishAffinity();
                                                reviewBtn.setEnabled(false);
//                                           
                                            } else if (jsonObject.getJSONObject("Signs_AddBillerResult").getString("Status_Code").equals("54")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_AddBillerResult").getString("Status_Description"), "ERROR", ReviewBillerDetailsScreen.this, new LoginScreen());
                                                reviewBtn.setEnabled(true);

                                            } else {

                                                dialog.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_AddBillerResult").getString("Status_Description"), "ERROR");
                                                reviewBtn.setEnabled(true);

                                            }
                                        } else {

                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            reviewBtn.setEnabled(true);

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        reviewBtn.setEnabled(true);

                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        reviewBtn.setEnabled(true);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        reviewBtn.setEnabled(true);

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sCategoryID", intent1.getStringExtra("categoryID"));
//                            params.put("sSigns_Alias", intent1.getStringExtra("alias"));
//                            params.put("sCompanyID", intent1.getStringExtra("CompanyID"));
//                            params.put("sCategoryName", intent1.getStringExtra("categoryName"));
//                            params.put("sCompanyName", intent1.getStringExtra("CompanyName"));
//                            params.put("sConsumerNo", intent1.getStringExtra("consumerNumber"));
//                            params.put("sConsumerName", intent1.getStringExtra("ConsumerName"));
//                            params.put("sBillingMonth", intent1.getStringExtra("BillingMonth"));
//                            params.put("sAmtBefore", intent1.getStringExtra("AmountWithinDueDate").replaceAll(",", ""));
//                            params.put("sAmtAfter", intent1.getStringExtra("AmountAfterDueDate").replaceAll(",", ""));
//                            params.put("sDueDate", intent1.getStringExtra("DueDate"));
//                            params.put("sBillStatus", intent1.getStringExtra("BillStatus"));
//                            params.put("sComments", "-");
//                            params.put("sDistAccountNumber", intent1.getStringExtra("distributorAccount"));
//                            params.put("sSigns_MobileNumber", constants.sharedPreferences.getString("MobileNumber","N/A"));
//                            params.put("sSigns_EmailAddress", constants.sharedPreferences.getString("EmailAddress","N/A"));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    reviewBtn.setEnabled(true);

                }
            } else {
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                reviewBtn.setEnabled(true);

            }
        } catch (Exception e) {


            dialog.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            reviewBtn.setEnabled(true);


        }
    }


    public void showDilogForErrorForLogin(String msg, String header) {

        dialog = new Dialog(ReviewBillerDetailsScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(ReviewBillerDetailsScreen.this, ManageBillerScreen.class);
                intent.putExtra("addBiller", "addBiller");
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
                finishAffinity();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();


    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(ReviewBillerDetailsScreen.this, ManageBillerScreen.class);
        intent.putExtra("addBiller", "addBiller");
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
        finishAffinity();
    }
}