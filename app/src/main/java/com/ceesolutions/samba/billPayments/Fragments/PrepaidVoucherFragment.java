package com.ceesolutions.samba.billPayments.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.billPayments.PayBillsScreen;
import com.ceesolutions.samba.billPayments.PrepaidReviewScreen;
import com.ceesolutions.samba.billPayments.SelectPrepaidScreen;
import com.ceesolutions.samba.transactions.SelectAccountScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/11/18.
 */

public class PrepaidVoucherFragment extends Fragment {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private String remarks, amount, isFP;
    private EditText editAmount, editRemarks, editMobileNumber;
    private RelativeLayout upperPart1, upperPart2;
    private TextView errorMessageForMobile, errorMessageForAmount, errorMessageForRemarks, errorMessageForSpinner, userNameTxt, accMaskTxt, amountTxt, friendNameTxt, itemName, validateBtn;
    private Spinner purposesSpinner;
    private String purposeSelected;
    private String[] purpose;
    private ArrayList<String> purpStringArrayList;
    private String sourceBalance, sourceuserName, sourceAccount, sourceCurrency, distributorID, distributorAccountNumber, mobileNumber, distributorName, length;
    private Utils utils;
    private boolean isSource = false;
    private boolean isDestination = false;
    private boolean isFirst = false;
    private String identityValue, stan, rrn;
    private String beneType = "SAMBA";
    private TextView doneBtn, textView, heading;
    private Dialog dialog;
    private String minAmount, sourceminAmount, friendNumber, messageId, targetId;
    private String maxAmount, branchCode, mobile, email, type;
    private ImageView arrowDestination;
    private float amountToInt, maxAmountToInt, minAmountToInt;
    private int bal;
    private String isSelectSource, isSelectDestination;
    private boolean isVisible = false;
    private AppPreferences appPreferences;
    private String location;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.prepaid_voucher, container, false);

        initViews(convertView);
        // new log
        FirebaseAnalytics.getInstance(getContext()).logEvent("Mobile_Top_up_Pressed", new Bundle());



        editRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                remarks = editable.toString();
            }
        });

        editMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForMobile.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForMobile.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                mobileNumber = editable.toString();
            }
        });


        editAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (minAmount != null && maxAmount != null) {
                    errorMessageForAmount.setVisibility(View.VISIBLE);
                    errorMessageForAmount.setTextColor(getActivity().getResources().getColor(R.color.lgreen));
                    errorMessageForAmount.setText("Amount must be between " + minAmount + " and " + maxAmount);
                    errorMessageForAmount.setError(null);
                }

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (minAmount != null && maxAmount != null) {
                    errorMessageForAmount.setVisibility(View.VISIBLE);
                    errorMessageForAmount.setTextColor(getActivity().getResources().getColor(R.color.lgreen));
                    errorMessageForAmount.setText("Amount must be between " + minAmount + " and " + maxAmount);
                    errorMessageForAmount.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

                amount = editable.toString();

            }
        });


        upperPart1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                upperPart1.setEnabled(false);
                isSource = true;
                isDestination = false;
                isFirst = true;
                Intent intent = new Intent(getActivity(), SelectAccountScreen.class);
                intent.putExtra("source", "source");
                startActivity(intent);


            }
        });

        upperPart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                upperPart2.setEnabled(false);
                Intent intent = new Intent(getActivity(), SelectPrepaidScreen.class);
                intent.putExtra("source", "destination");
                startActivity(intent);
                isDestination = true;
                isSource = false;
                isFirst = true;


            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                FirebaseAnalytics.getInstance(getContext()).logEvent("Mobile_Top_up_Review_Pressed", new Bundle());


                try {
                    int stanNumber = 6;
                    stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
                    int rrnNumber = 12;
                    rrn = String.valueOf(helper.generateRandom(rrnNumber));
                    CryptLib _crypt = new CryptLib();
                    remarks = editRemarks.getText().toString().trim();
                    amount = editAmount.getText().toString().trim();
                    mobileNumber = editMobileNumber.getText().toString().trim();


                    if (TextUtils.isEmpty(amount)) {

                        errorMessageForAmount.setVisibility(View.VISIBLE);
                        errorMessageForAmount.bringToFront();
                        errorMessageForAmount.setTextColor(getActivity().getResources().getColor(R.color.tabunderline));
                        errorMessageForAmount.setError("");
                        errorMessageForAmount.setText("Please enter valid amount");

                    } else if (!helper.validateInputForSC(amount) || amount.contains(" ")) {

                        errorMessageForAmount.setVisibility(View.VISIBLE);
                        errorMessageForAmount.setTextColor(getActivity().getResources().getColor(R.color.tabunderline));
                        errorMessageForAmount.bringToFront();
                        errorMessageForAmount.setError("");
                        errorMessageForAmount.setText("Amount cannot contains <,>,\",',%,(,),&,+,\\,~,space");


                    } else if (amount.startsWith("0")) {

                        amount = amount.replaceFirst("^0+(?!$)", "");

                        if (!TextUtils.isEmpty(amount) && !amount.equals("")) {

                            if (amount.equals("0")) {
                                errorMessageForAmount.setVisibility(View.VISIBLE);
                                errorMessageForAmount.bringToFront();
                                errorMessageForAmount.setError("");
                                errorMessageForAmount.setText("Please enter valid amount");
                            }

                        } else {

                            errorMessageForAmount.setVisibility(View.VISIBLE);
                            errorMessageForAmount.bringToFront();
                            errorMessageForAmount.setError("");
                            errorMessageForAmount.setText("Please enter valid amount");
                        }


                    }

                    if (TextUtils.isEmpty(mobileNumber)) {

                        errorMessageForMobile.setVisibility(View.VISIBLE);
                        errorMessageForMobile.bringToFront();
                        errorMessageForMobile.setError("");
                        errorMessageForMobile.setText("Please enter valid mobile number");

                    } else if (!helper.validateInputForSC(mobileNumber) || mobileNumber.contains(" ")) {

                        errorMessageForMobile.setVisibility(View.VISIBLE);
                        errorMessageForMobile.bringToFront();
                        errorMessageForMobile.setError("");
                        errorMessageForMobile.setText("Mobile Number cannot contains <,>,\",',%,(,),&,+,\\,~,space");


                    } else if (mobileNumber.length() < 11) {

                        errorMessageForMobile.setVisibility(View.VISIBLE);
                        errorMessageForMobile.bringToFront();
                        errorMessageForMobile.setError("");
                        errorMessageForMobile.setText("Please enter a valid 11 digit mobile number");
                    }

                    if (TextUtils.isEmpty(remarks)) {

                        remarks = "-";

                    } else if (!helper.validateInputForSC(remarks)) {

                        errorMessageForRemarks.setVisibility(View.VISIBLE);
                        errorMessageForRemarks.bringToFront();
                        errorMessageForRemarks.setError("");
                        errorMessageForRemarks.setText("Amount cannot contains <,>,\",',%,(,),&,+,\\,~");


                    }

                    if (purposeSelected.equals("Purpose of Payment")) {

                        errorMessageForSpinner.setVisibility(View.VISIBLE);
                        errorMessageForSpinner.bringToFront();
                        errorMessageForSpinner.setError("");
                        errorMessageForSpinner.setText("Please select purpose of payment");
                    }

                    if ((TextUtils.isEmpty(sourceAccount) || sourceAccount.equals("N/A")) || (TextUtils.isEmpty(sourceBalance) || sourceBalance.equals("N/A")) || (TextUtils.isEmpty(sourceuserName) || sourceuserName.equals("N/A"))) {

                        utils.showDilogForError("Please select Source account", "WARNING");

                    } else if ((TextUtils.isEmpty(distributorID) || distributorID.equals("N/A")) || (TextUtils.isEmpty(distributorAccountNumber) || distributorAccountNumber.equals("N/A"))) {

                        showDilogForError("Please select Prepaid voucher", "WARNING");

                    } else if (!TextUtils.isEmpty(amount) && !amount.startsWith("0")) {

                        sourceBalance = sourceBalance.replaceAll(",", "");
                        bal = new BigDecimal(sourceBalance).intValue();

                        if (!TextUtils.isEmpty(sourceBalance) && !sourceBalance.equals("N/A")) {
                            try {

                                amountToInt = Integer.valueOf(amount);
                                maxAmountToInt = Integer.valueOf(maxAmount);
                                minAmountToInt = Integer.valueOf(minAmount);

                            } catch (Exception e) {

                                e.printStackTrace();
                            }

                            if (amountToInt > bal) {

                                utils.showDilogForError("Amount cannot be greater than avaliable balance.", "WARNING");

                            } else if (amountToInt > maxAmountToInt) {

                                utils.showDilogForError("Amount cannot be greater than Maximum Amount Limit.", "WARNING");
                            } else if (amountToInt < minAmountToInt) {

                                utils.showDilogForError("Amount cannot be less than Minimum Amount Limit.", "WARNING");
                            }
                        }
                    }

//                    if (!sourceCurrency.equals("PKR")) {
//
//
//                        utils.showDilogForError("Prepaid Voucher can only be buy from PKR account.", "ERROR");
//                    }


                    if ((!TextUtils.isEmpty(amount) && !amount.startsWith("0") && amountToInt <= bal && amountToInt <= maxAmountToInt && amountToInt >= minAmountToInt) && !TextUtils.isEmpty(remarks) && helper.validateInputForSC(remarks) && !purposeSelected.equals("Purpose of Payment") && (!TextUtils.isEmpty(sourceAccount) && !sourceAccount.equals("N/A")) && (!TextUtils.isEmpty(sourceBalance) && !sourceBalance.equals("N/A")) && (!TextUtils.isEmpty(sourceuserName) && !sourceuserName.equals("N/A")) && (!TextUtils.isEmpty(distributorID) && !distributorID.equals("N/A")) && (!TextUtils.isEmpty(distributorAccountNumber) && !distributorAccountNumber.equals("N/A")) && (!TextUtils.isEmpty(mobileNumber) && !mobileNumber.contains(" ") && helper.validateInputForSC(mobileNumber) && mobileNumber.length() > 10))
//                    if ((!TextUtils.isEmpty(amount) && !amount.startsWith("0") && amountToInt <= bal && amountToInt <= maxAmountToInt && amountToInt >= minAmountToInt) && !TextUtils.isEmpty(remarks) && helper.validateInputForSC(remarks) && !purposeSelected.equals("Purpose of Payment") && (!TextUtils.isEmpty(sourceAccount) && !sourceAccount.equals("N/A")) && (!TextUtils.isEmpty(sourceBalance) && !sourceBalance.equals("N/A")) && (!TextUtils.isEmpty(sourceuserName) && !sourceuserName.equals("N/A")) && (!TextUtils.isEmpty(distributorID) && !distributorID.equals("N/A")) && (!TextUtils.isEmpty(distributorAccountNumber) && !distributorAccountNumber.equals("N/A")) && sourceCurrency.equals("PKR") && (!TextUtils.isEmpty(mobileNumber) && !mobileNumber.contains(" ") && helper.validateInputForSC(mobileNumber) && mobileNumber.length() > 10))
                    {

                        //Send to confirm

//                        validateBtn.setEnabled(false);

                        Intent intent = new Intent(getActivity(), PrepaidReviewScreen.class);
                        intent.putExtra("userName", sourceuserName);
                        intent.putExtra("balance", sourceBalance);
                        intent.putExtra("accountNumber", sourceAccount);
                        intent.putExtra("purpose", purposeSelected);
                        intent.putExtra("currency", sourceCurrency);
                        intent.putExtra("distributorAccountNumber", distributorAccountNumber);
                        intent.putExtra("mobile", mobileNumber);
                        intent.putExtra("remarks", remarks);
                        intent.putExtra("stan", stan);
                        intent.putExtra("rrn", rrn);
                        intent.putExtra("amount", amount);
                        intent.putExtra("branchCode", branchCode);
                        intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        intent.putExtra("distributorName", distributorName);
                        intent.putExtra("distributorID", distributorID);
                        intent.putExtra("friendNumber", friendNumber);
                        intent.putExtra("messageId", messageId);
                        intent.putExtra("targetId", targetId);
                        intent.putExtra("ftType", type);
                        getActivity().overridePendingTransition(0, 0);
                        startActivity(intent);
                        validateBtn.setEnabled(true);


                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        return convertView;
    }

    public void initViews(View convertView) {

        try {

            CryptLib _crypt = new CryptLib();
            purposesSpinner = (Spinner) convertView.findViewById(R.id.purposeSpinner);
            validateBtn = (TextView) convertView.findViewById(R.id.validateBtn);
            upperPart1 = (RelativeLayout) convertView.findViewById(R.id.upperPart1);
            upperPart2 = (RelativeLayout) convertView.findViewById(R.id.upperPart2);
            friendNameTxt = (TextView) convertView.findViewById(R.id.friendNameTxt);
            arrowDestination = (ImageView) convertView.findViewById(R.id.arrowDestination);
            userNameTxt = (TextView) convertView.findViewById(R.id.userNameTxt);
            userNameTxt.setText("Select Source Account");
            accMaskTxt = (TextView) convertView.findViewById(R.id.accMaskTxt);
            accMaskTxt.setVisibility(View.INVISIBLE);
            amountTxt = (TextView) convertView.findViewById(R.id.amountTxt);
            amountTxt.setVisibility(View.INVISIBLE);
            itemName = (TextView) convertView.findViewById(R.id.itemName);
            itemName.setText("Select Prepaid Voucher");
            errorMessageForAmount = (TextView) convertView.findViewById(R.id.errorMessageForAmount);
            errorMessageForMobile = (TextView) convertView.findViewById(R.id.errorMessageForMobile);
            errorMessageForRemarks = (TextView) convertView.findViewById(R.id.errorMessageForRemarks);
            errorMessageForSpinner = (TextView) convertView.findViewById(R.id.errorMessageForSpinner);
            editRemarks = (EditText) convertView.findViewById(R.id.editRemarks);
            editAmount = (EditText) convertView.findViewById(R.id.editAmount);
            editMobileNumber = (EditText) convertView.findViewById(R.id.editMobileNumber);
            requestQueue = Volley.newRequestQueue(getActivity());
            helper = Helper.getHelper(getActivity());
            constants = Constants.getConstants(getActivity());
            pd = new Dialog(getActivity());
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            purpStringArrayList = new ArrayList<>();
            utils = new Utils(getActivity());
            appPreferences = new AppPreferences(getActivity());
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");

            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }

//        String userType = constants.sharedPreferences.getString("type", "N/A");
//
//        if (!TextUtils.isEmpty(userType) && userType.equals("NON_SAMBA")) {
//
//
//            showDilogForErrorForNonSamba("This feature is only available for Samba Users.", "WARNING");
//
//
//        }

            String menus = constants.sharedPreferences.getString("Menus", "N/A");

            if (!TextUtils.isEmpty(menus) && !menus.equals("N/A") && !menus.equals("")) {

                if (menus.toLowerCase().contains("prepaid voucher")) {
                    showDilogForErrorForMenu("This service is currently disabled. Please try again later.", "WARNING");
                }
            }

            String list = constants.puposePrepaid.getString("purposeList", "N/A");


            int stanNumber = 6;
            stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
            int rrnNumber = 12;
            rrn = String.valueOf(helper.generateRandom(rrnNumber));

            Intent intent = getActivity().getIntent();

            type = intent.getStringExtra("requestbill");

            if (!TextUtils.isEmpty(type) && type.equals("requestvoucher")) {

                errorMessageForAmount.setVisibility(View.VISIBLE);
                errorMessageForAmount.setTextColor(getActivity().getResources().getColor(R.color.lgreen));
                distributorID = _crypt.decrypt(constants.destinationAccount.getString("distributorID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                distributorAccountNumber = _crypt.decrypt(constants.destinationAccount.getString("distributorAccountNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                distributorName = _crypt.decrypt(constants.destinationAccount.getString("consumerName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                itemName.setText(distributorName);
                editMobileNumber.setText(_crypt.decrypt(constants.destinationAccount.getString("consumerNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                editMobileNumber.setFocusableInTouchMode(false);
                editAmount.setText(_crypt.decrypt(constants.destinationAccount.getString("amount", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                editAmount.setFocusableInTouchMode(false);
                editRemarks.setText(_crypt.decrypt(constants.destinationAccount.getString("remarks", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                editRemarks.setFocusableInTouchMode(false);
                maxAmount = _crypt.decrypt(constants.destinationAccount.getString("maxAmount", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                minAmount = _crypt.decrypt(constants.destinationAccount.getString("minAmount", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                errorMessageForAmount.setText("Amount must be between " + minAmount + " and " + maxAmount);
                messageId = _crypt.decrypt(constants.destinationAccount.getString("messageId", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                targetId = _crypt.decrypt(constants.destinationAccount.getString("targetFormId", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                friendNumber = _crypt.decrypt(constants.destinationAccount.getString("friendNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                constants.destinationAccountEditor.putString("isSelect", "No");
                constants.destinationAccountEditor.commit();
                constants.destinationAccountEditor.apply();
                upperPart2.setEnabled(false);
                arrowDestination.setVisibility(View.INVISIBLE);
            }


            if (list.equals("N/A")) {
                GetPurposePrepaidList();
            } else {
                Gson gson = new Gson();
                Type type1 = new TypeToken<ArrayList<String>>() {
                }.getType();

                purpStringArrayList = gson.fromJson(list, type1);
                purpose = new String[purpStringArrayList.size()];
                purpose = purpStringArrayList.toArray(purpose);
                if (!TextUtils.isEmpty(type) && type.equals("requestvoucher")) {
                    purposeSelected = _crypt.decrypt(constants.destinationAccount.getString("purpose", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(purposeSelected));
                    purposesSpinner.setClickable(false);
                    purposesSpinner.setEnabled(false);
                } else {
                    spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(0);
                    purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            try {


                                // Get select item
                                if (purposesSpinner.getSelectedItem() == "Purpose of Payment") {
                                    purposeSelected = purposesSpinner.getSelectedItem().toString();
                                    Log.d("identity", "---" + purposeSelected);

                                } else {

                                    purposeSelected = purposesSpinner.getSelectedItem().toString();

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });
                }
            }

            Intent intent2 = getActivity().getIntent();

            String paybill1 = intent2.getStringExtra("paybill");

            if (!TextUtils.isEmpty(paybill1) && paybill1.equals("paybill")) {

                userNameTxt.setText(intent2.getStringExtra("accountName"));
                accMaskTxt.setVisibility(View.VISIBLE);
                accMaskTxt.setText(utils.getMaskedString(intent2.getStringExtra("accountNumber")));
                amountTxt.setVisibility(View.VISIBLE);
                amountTxt.setText(intent2.getStringExtra("currency") + " " + intent.getStringExtra("balance"));
                sourceAccount = intent2.getStringExtra("accountNumber");
                sourceCurrency = intent2.getStringExtra("currency");
                sourceBalance = intent2.getStringExtra("balance");
                sourceuserName = intent2.getStringExtra("accountName");
                branchCode = intent2.getStringExtra("branchCode");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GetPurposePrepaidList() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    pd.show();
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sPurposeGroupCode", "PV");
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(getActivity());
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getPuposesURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("POTList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        purpStringArrayList.add(jsonObj.getString("Name"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();

                                                    }

                                                }

                                                purpose = new String[purpStringArrayList.size()];
                                                purpose = purpStringArrayList.toArray(purpose);

                                                Gson gson = new Gson();
                                                String json = gson.toJson(purpStringArrayList);
                                                constants.purposePrepaidList.putString("purposeList", json);
                                                constants.purposePrepaidList.commit();
                                                constants.purposePrepaidList.apply();
                                                showSpinner();
                                            } else if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());
                                                validateBtn.setEnabled(true);

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                            }

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            validateBtn.setEnabled(true);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sPurposeGroupCode", "PV");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    validateBtn.setEnabled(true);
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            try {
                CryptLib _crypt = new CryptLib();

                constants = Constants.getConstants(getActivity());
                String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                if (!TextUtils.isEmpty(userType) && userType.equals("NON_SAMBA")) {

                    isVisible = true;
                    onAttach(getActivity());


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            //Do Nothing
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//
//        if (isVisible) {
//            dialog = new Dialog((Activity) context);
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.setContentView(R.layout.error_dialog);
//            textView = (TextView) dialog.findViewById(R.id.validatingTxt);
//            doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
//            heading = (TextView) dialog.findViewById(R.id.heading);
//            dialog.setCancelable(false);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//            doneBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    dialog.dismiss();
//                    PayBillsScreen.viewPager.setCurrentItem(2);
//                }
//            });
//            textView.setText("This feature is only available for Samba Users.");
//            heading.setText("WARNING");
//            dialog.show();
//            isVisible = false;
//
//        }
//    }

    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                pd.dismiss();
                if (!TextUtils.isEmpty(type) && type.equals("requestvoucher")) {
                    try {
                        CryptLib _crypt = new CryptLib();

                        purposeSelected = _crypt.decrypt(constants.destinationAccount.getString("purpose", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                        purposeAdapter.addAll(purpose);
                        purposeAdapter.add("Purpose of Payment");
                        purposesSpinner.setAdapter(purposeAdapter);
                        purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(constants.destinationAccount.getString("purpose", "N/A")));
                        purposesSpinner.setClickable(false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(0);
                    purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            try {


                                // Get select item
                                if (purposesSpinner.getSelectedItem() == "Purpose of Payment") {
                                    purposeSelected = purposesSpinner.getSelectedItem().toString();
                                    Log.d("identity", "---" + purposeSelected);

                                } else {

                                    purposeSelected = purposesSpinner.getSelectedItem().toString();

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });
                }
            }

        }, 5000);
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            CryptLib _crypt = new CryptLib();

            if (isFirst) {

                upperPart1.setEnabled(true);
                if (!TextUtils.isEmpty(type) && type.equals("requestvoucher")) {

                } else {
                    upperPart2.setEnabled(true);
                }
                if (isSource) {
                    isSelectSource = constants.sourceAccount.getString("isSelect", "N/A");
                    if (!isSelectSource.equals("N/A") && isSelectSource.equals("Yes")) {
                        sourceuserName = _crypt.decrypt(constants.sourceAccount.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        sourceBalance = _crypt.decrypt(constants.sourceAccount.getString("balance", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        sourceAccount = _crypt.decrypt(constants.sourceAccount.getString("accountNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        branchCode = _crypt.decrypt(constants.sourceAccount.getString("branchCode", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        sourceCurrency = _crypt.decrypt(constants.sourceAccount.getString("currency", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        userNameTxt.setText(sourceuserName);
                        accMaskTxt.setVisibility(View.VISIBLE);
                        accMaskTxt.setText(utils.getMaskedString(sourceAccount));
                        amountTxt.setVisibility(View.VISIBLE);
                        amountTxt.setText(sourceCurrency + " " + sourceBalance);
                    }

                    constants.sourceAccountEditor.putString("isSelect", "No");
                    constants.sourceAccountEditor.commit();
                    constants.sourceAccountEditor.apply();
                    isSource = false;
                }


                if (isDestination) {

                    isSelectDestination = constants.destinationAccount.getString("isSelect", "N/A");
                    if (!isSelectDestination.equals("N/A") && isSelectDestination.equals("Yes")) {
                        distributorID = _crypt.decrypt(constants.destinationAccount.getString("distributorID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        distributorAccountNumber = _crypt.decrypt(constants.destinationAccount.getString("distributorAccountNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        maxAmount = _crypt.decrypt(constants.destinationAccount.getString("maxAmount", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        minAmount = _crypt.decrypt(constants.destinationAccount.getString("minAmount", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        distributorName = _crypt.decrypt(constants.destinationAccount.getString("distributorName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        length = _crypt.decrypt(constants.destinationAccount.getString("length", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        itemName.setText(distributorName);
                        int maxLengthofEditText = Integer.valueOf(length);
                        editMobileNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});
                        errorMessageForAmount.setVisibility(View.VISIBLE);
                        errorMessageForAmount.setTextColor(getActivity().getResources().getColor(R.color.lgreen));
                        errorMessageForAmount.setText("Amount must be between " + minAmount + " and " + maxAmount);
                        errorMessageForAmount.setError(null);
                    }

                    constants.destinationAccountEditor.putString("isSelect", "No");
                    constants.destinationAccountEditor.commit();
                    constants.destinationAccountEditor.apply();
                    isDestination = false;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public void showDilogForErrorForMenu(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(getActivity(), HomeScreen.class);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).startActivity(intent);
                ((Activity) getActivity()).finish();
                ((Activity) getActivity()).finishAffinity();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public void showDilogForErrorForNonSamba(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                PayBillsScreen.viewPager.setCurrentItem(2);
            }
        });

        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


}