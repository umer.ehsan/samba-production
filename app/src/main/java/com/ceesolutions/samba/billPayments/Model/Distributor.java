package com.ceesolutions.samba.billPayments.Model;

import proguard.annotation.Keep;

/**
 * Created by ceeayaz on 4/9/18.
 */
@Keep
public class Distributor {

    private String allowedMaxAmount;
    private String allowedMinAmount;
    private String chargeCode;
    private String consumerNoLayout;
    private String categoryID;
    private String categoryName;
    private String consumerNoLength;
    private String deductCharges;
    private String accountNumber;
    private String distributorID;
    private String distributorName;
    private String transactionCode;
    private int distributorColor;
    private String displayText;

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public int getDistributorColor() {
        return distributorColor;
    }

    public void setDistributorColor(int distributorColor) {
        this.distributorColor = distributorColor;
    }

    private String firstLetter;

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getFirstLetter() {
        return firstLetter;
    }

    public void setFirstLetter(String firstLetter) {
        this.firstLetter = firstLetter;
    }

    public String getConsumerNoLength() {
        return consumerNoLength;
    }

    public void setConsumerNoLength(String consumerNoLength) {
        this.consumerNoLength = consumerNoLength;
    }


    public String getAllowedMaxAmount() {
        return allowedMaxAmount;
    }

    public void setAllowedMaxAmount(String allowedMaxAmount) {
        this.allowedMaxAmount = allowedMaxAmount;
    }

    public String getAllowedMinAmount() {
        return allowedMinAmount;
    }

    public void setAllowedMinAmount(String allowedMinAmount) {
        this.allowedMinAmount = allowedMinAmount;
    }

    public String getChargeCode() {
        return chargeCode;
    }

    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    public String getConsumerNoLayout() {
        return consumerNoLayout;
    }

    public void setConsumerNoLayout(String consumerNoLayout) {
        this.consumerNoLayout = consumerNoLayout;
    }


    public String getDeductCharges() {
        return deductCharges;
    }

    public void setDeductCharges(String deductCharges) {
        this.deductCharges = deductCharges;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getDistributorID() {
        return distributorID;
    }

    public void setDistributorID(String distributorID) {
        this.distributorID = distributorID;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }
}
