package com.ceesolutions.samba.billPayments.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.billPayments.ManageBillerScreen;
import com.ceesolutions.samba.billPayments.RequestBillPaymentReviewScreen;
import com.ceesolutions.samba.friends.ManageFriends;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.ceesolutions.samba.views.CircleImageView;
import com.ceesolutions.samba.views.RoundImageView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/13/18.
 */

public class RequestBillPaymentFragment extends Fragment {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    boolean flag = false;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private ExpandableLayout expandableLayout0;
    private CardView cardView;
    private Dialog pd;
    private String remarks, amount, isFP, type;
    private EditText editAmount, editRemarks, editShareAccount;
    private RelativeLayout upperPart1, upperPart2;
    private TextView errorMessageForAmount, errorMessageForRemarks, errorMessageForSpinner, userNameTxt, accMaskTxt, amountTxt, friendNameTxt, accFrindMaskTxt, validateBtn, reviewBtn;
    private Spinner purposesSpinner;
    private String purposeSelected;
    private String stan, rrn;
    private String sourceBalance, sourceuserName, sourceAccount, friendNumber, destinationName, friendImage;
    private ArrayList<String> purpStringArrayList;
    private String[] purpose = {
            "School Fees",
            "School Fees",
            "School Fees",
            "School Fees",
            "School Fees"

    };
    private Utils utils;
    private SwitchCompat switchCompat;
    private boolean isSource = false;
    private boolean isDestination = false;
    private boolean isFirst = false;
    private String isFav = "SAMBA";
    private String userID;
    private TextView doneBtn, mainTxt, textView, heading, consumerIDTxt, dueDateTxt, billingMonthTxt, consumerNumberTxt, billingTxt, payableAfterTxt, payableBeforeTxt, statusTxt, payabaleAmount, itemName;
    private Dialog dialog;
    private String friendID, sourceCurrency;
    private String bankCode;
    private String minAmount, categoryType, billConsumerNumber, sourceminAmount, amountAfterDue, amountBeforeDue, billStatus, billingMonth, dueDate, consumerName, companyName;
    private String maxAmount, branchCode, mobile, email, distributorID, distributorAccountNumber, alias;
    private float bal, amountToInt, maxAmountToInt, minAmountToInt;
    private String isSelectSource, isSelectDestination;
    private CircleImageView profile_image;
    private RoundImageView title;
    private TextView titleTxt;
    private int color;
    private TextView friendNameText;
    private AppPreferences appPreferences;
    private String location;
    private boolean isCheck = false;
    String userType;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.request_bill_payment, container, false);

        initViews(convertView);
        // new log
        FirebaseAnalytics.getInstance(getContext()).logEvent("Request_Bill_Payment_Pressed", new Bundle());



        editRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                remarks = editable.toString();
            }
        });


        editAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForAmount.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForAmount.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                amount = editable.toString();
            }
        });


        upperPart1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                upperPart1.setEnabled(false);
                isSource = true;
                isDestination = false;
                isFirst = true;
                Intent intent = new Intent(getActivity(), ManageFriends.class);
                intent.putExtra("source", "destination");
                startActivity(intent);


            }
        });

        upperPart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                upperPart2.setEnabled(false);
                Intent intent = new Intent(getActivity(), ManageBillerScreen.class);
                intent.putExtra("source", "destination");
                startActivity(intent);
                isDestination = true;
                isSource = false;
                isFirst = true;


            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (userType.equals("NON_SAMBA")) {

                    FirebaseAnalytics.getInstance(getContext()).logEvent("Non_Samba_Request_Bill_Review_Pressed", new Bundle());

                } else {
                    FirebaseAnalytics.getInstance(getContext()).logEvent("Request_Bill_Review_Pressed", new Bundle());
                }


                try {
                    int stanNumber = 6;
                    stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
                    int rrnNumber = 12;
                    rrn = String.valueOf(helper.generateRandom(rrnNumber));
                    CryptLib _crypt = new CryptLib();
                    remarks = editRemarks.getText().toString().trim();
                    amount = editAmount.getText().toString().trim().replaceAll("\\,", "");
                    maxAmount = payabaleAmount.getText().toString().replaceAll(",", "").trim();
                    maxAmount = maxAmount.substring(0, maxAmount.indexOf("."));
                    String billStatus1 = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());


                    if (TextUtils.isEmpty(amount)) {

                        errorMessageForAmount.setVisibility(View.VISIBLE);
                        errorMessageForAmount.bringToFront();
                        errorMessageForAmount.setError("");
                        errorMessageForAmount.setText("Please enter valid amount");

                    } else if (!helper.validateInputForSC(amount) || amount.contains(" ")) {

                        errorMessageForAmount.setVisibility(View.VISIBLE);
                        errorMessageForAmount.bringToFront();
                        errorMessageForAmount.setError("");
                        errorMessageForAmount.setText("Amount cannot contains <,>,\",',%,(,),&,+,\\,~,space");


                    } else if (amount.startsWith("0")) {

                        amount = amount.replaceFirst("^0+(?!$)", "");

                        if (!TextUtils.isEmpty(amount) && !amount.equals("")) {

                            if (amount.equals("0")) {
                                errorMessageForAmount.setVisibility(View.VISIBLE);
                                errorMessageForAmount.bringToFront();
                                errorMessageForAmount.setError("");
                                errorMessageForAmount.setText("Please enter valid amount");
                            }

                        } else {

                            errorMessageForAmount.setVisibility(View.VISIBLE);
                            errorMessageForAmount.bringToFront();
                            errorMessageForAmount.setError("");
                            errorMessageForAmount.setText("Please enter valid amount");
                        }


                    } else {

                        amount = amount.split("\\.")[0];

                    }


                    if (TextUtils.isEmpty(remarks)) {

                        remarks = "-";

                    } else if (!helper.validateInputForSC(remarks)) {

                        errorMessageForRemarks.setVisibility(View.VISIBLE);
                        errorMessageForRemarks.bringToFront();
                        errorMessageForRemarks.setError("");
                        errorMessageForRemarks.setText("Amount cannot contains <,>,\",',%,(,),&,+,\\,~");


                    }

                    if (purposeSelected.equals("Purpose of Payment")) {

                        errorMessageForSpinner.setVisibility(View.VISIBLE);
                        errorMessageForSpinner.bringToFront();
                        errorMessageForSpinner.setError("");
                        errorMessageForSpinner.setText("Please select purpose of payment");
                    }

                    if (TextUtils.isEmpty(destinationName) || destinationName.equals("N/A") || TextUtils.isEmpty(friendNumber) || friendNumber.equals("N/A")) {

                        showDilogForError("Please select a Friend", "WARNING");

                    } else if ((TextUtils.isEmpty(distributorID) || distributorID.equals("N/A")) || (TextUtils.isEmpty(distributorAccountNumber) || distributorAccountNumber.equals("N/A"))) {

                        showDilogForError("Please select Biller", "WARNING");

                    } else if (!TextUtils.isEmpty(amount) && !amount.startsWith("0")) {


                        amountToInt = Integer.valueOf(amount);
                        maxAmountToInt = Integer.valueOf(maxAmount);


                        if (!TextUtils.isEmpty(billStatus1) && !billStatus1.equals("N/A")) {


                            if (billStatus1.toLowerCase().equals("partial payment")) {

                                if (amountToInt > maxAmountToInt) {

                                    utils.showDilogForError("Amount cannot be greater than Payable Amount.", "WARNING");

                                }

                            } else if (amountToInt != maxAmountToInt) {
                                utils.showDilogForError("Amount should be equal to Payable Amount.", "WARNING");

                            }


                        } else if (amountToInt != maxAmountToInt) {
                            utils.showDilogForError("Amount should be equal to Payable Amount.", "WARNING");
                        }
                    }

//                    if (!sourceCurrency.equals("PKR")) {
//
//
//                        utils.showDilogForError("Prepaid Voucher can only be buy from PKR account.", "ERROR");
//                    }

                    if (!TextUtils.isEmpty(billStatus1) && !billStatus1.equals("N/A")) {


                        if (billStatus1.toLowerCase().equals("partial payment")) {


                            if ((!TextUtils.isEmpty(amount) && !amount.startsWith("0") && amountToInt <= maxAmountToInt) && !TextUtils.isEmpty(remarks) && helper.validateInputForSC(remarks) && !purposeSelected.equals("Purpose of Payment") && (!TextUtils.isEmpty(distributorID) && !distributorID.equals("N/A")) && (!TextUtils.isEmpty(distributorAccountNumber) && !distributorAccountNumber.equals("N/A")) && (!TextUtils.isEmpty(destinationName) && !destinationName.equals("N/A"))) {

                                //Send to confirm

//                        validateBtn.setEnabled(false);

                                if (billStatus.toLowerCase().equals("paid") || billStatus.toLowerCase().equals("blocked")) {

                                    utils.showDilogForError("Bill has already been Paid or is Blocked.", "ERROR");
                                } else {

                                    Intent intent = new Intent(getActivity(), RequestBillPaymentReviewScreen.class);
                                    intent.putExtra("userName", destinationName);
                                    intent.putExtra("friendNumber", friendNumber);
                                    intent.putExtra("friendID", friendID);
                                    intent.putExtra("purpose", purposeSelected);
                                    intent.putExtra("currency", "PKR");
                                    intent.putExtra("distributorAccountNumber", distributorAccountNumber);
                                    intent.putExtra("mobile", billConsumerNumber);
                                    intent.putExtra("remarks", remarks);
                                    intent.putExtra("stan", stan);
                                    intent.putExtra("rrn", rrn);
                                    intent.putExtra("amount", amount);
                                    intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                    intent.putExtra("distributorName", alias);
                                    intent.putExtra("distributorID", distributorID);
                                    intent.putExtra("categoryType", categoryType);
                                    intent.putExtra("companyName", companyName);
                                    intent.putExtra("billStatus", billStatus);
                                    intent.putExtra("billingMonth", billingMonth);
                                    intent.putExtra("dueDate", dueDate);
                                    intent.putExtra("amountAfterDue", amountAfterDue);
                                    intent.putExtra("amountBeforeDue", amountBeforeDue);
                                    intent.putExtra("consumerName", consumerName);
                                    intent.putExtra("friendImg", friendImage);
                                    getActivity().overridePendingTransition(0, 0);
                                    startActivity(intent);
                                    validateBtn.setEnabled(true);
                                }

                            }

                        } else {


                            if ((!TextUtils.isEmpty(amount) && !amount.startsWith("0") && amountToInt == maxAmountToInt) && !TextUtils.isEmpty(remarks) && helper.validateInputForSC(remarks) && !purposeSelected.equals("Purpose of Payment") && (!TextUtils.isEmpty(distributorID) && !distributorID.equals("N/A")) && (!TextUtils.isEmpty(distributorAccountNumber) && !distributorAccountNumber.equals("N/A")) && (!TextUtils.isEmpty(destinationName) && !destinationName.equals("N/A"))) {

                                //Send to confirm

//                        validateBtn.setEnabled(false);

                                if (billStatus.toLowerCase().equals("paid") || billStatus.toLowerCase().equals("blocked")) {

                                    utils.showDilogForError("Bill has already been Paid or is Blocked.", "ERROR");
                                } else {

                                    Intent intent = new Intent(getActivity(), RequestBillPaymentReviewScreen.class);
                                    intent.putExtra("userName", destinationName);
                                    intent.putExtra("friendNumber", friendNumber);
                                    intent.putExtra("friendID", friendID);
                                    intent.putExtra("purpose", purposeSelected);
                                    intent.putExtra("currency", "PKR");
                                    intent.putExtra("distributorAccountNumber", distributorAccountNumber);
                                    intent.putExtra("mobile", billConsumerNumber);
                                    intent.putExtra("remarks", remarks);
                                    intent.putExtra("stan", stan);
                                    intent.putExtra("rrn", rrn);
                                    intent.putExtra("amount", amount);
                                    intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                    intent.putExtra("distributorName", alias);
                                    intent.putExtra("distributorID", distributorID);
                                    intent.putExtra("categoryType", categoryType);
                                    intent.putExtra("companyName", companyName);
                                    intent.putExtra("billStatus", billStatus);
                                    intent.putExtra("billingMonth", billingMonth);
                                    intent.putExtra("dueDate", dueDate);
                                    intent.putExtra("amountAfterDue", amountAfterDue);
                                    intent.putExtra("amountBeforeDue", amountBeforeDue);
                                    intent.putExtra("consumerName", consumerName);
                                    intent.putExtra("friendImg", friendImage);
                                    getActivity().overridePendingTransition(0, 0);
                                    startActivity(intent);
                                    validateBtn.setEnabled(true);
                                }

                            }

                        }
                    } else {
                        if ((!TextUtils.isEmpty(amount) && !amount.startsWith("0") && amountToInt == maxAmountToInt) && !TextUtils.isEmpty(remarks) && helper.validateInputForSC(remarks) && !purposeSelected.equals("Purpose of Payment") && (!TextUtils.isEmpty(distributorID) && !distributorID.equals("N/A")) && (!TextUtils.isEmpty(distributorAccountNumber) && !distributorAccountNumber.equals("N/A")) && (!TextUtils.isEmpty(destinationName) && !destinationName.equals("N/A"))) {

                            //Send to confirm

//                        validateBtn.setEnabled(false);

                            if (billStatus.toLowerCase().equals("paid") || billStatus.toLowerCase().equals("blocked")) {

                                utils.showDilogForError("Bill has already been Paid or is Blocked.", "ERROR");
                            } else {

                                Intent intent = new Intent(getActivity(), RequestBillPaymentReviewScreen.class);
                                intent.putExtra("userName", destinationName);
                                intent.putExtra("friendNumber", friendNumber);
                                intent.putExtra("friendID", friendID);
                                intent.putExtra("purpose", purposeSelected);
                                intent.putExtra("currency", "PKR");
                                intent.putExtra("distributorAccountNumber", distributorAccountNumber);
                                intent.putExtra("mobile", billConsumerNumber);
                                intent.putExtra("remarks", remarks);
                                intent.putExtra("stan", stan);
                                intent.putExtra("rrn", rrn);
                                intent.putExtra("amount", amount);
                                intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                intent.putExtra("distributorName", alias);
                                intent.putExtra("distributorID", distributorID);
                                intent.putExtra("categoryType", categoryType);
                                intent.putExtra("companyName", companyName);
                                intent.putExtra("billStatus", billStatus);
                                intent.putExtra("billingMonth", billingMonth);
                                intent.putExtra("dueDate", dueDate);
                                intent.putExtra("amountAfterDue", amountAfterDue);
                                intent.putExtra("amountBeforeDue", amountBeforeDue);
                                intent.putExtra("consumerName", consumerName);
                                intent.putExtra("friendImg", friendImage);
                                getActivity().overridePendingTransition(0, 0);
                                startActivity(intent);
                                validateBtn.setEnabled(true);
                            }

                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        return convertView;

    }

    public void initViews(View convertView) {

        try {
            CryptLib _crypt = new CryptLib();

            titleTxt = (TextView) convertView.findViewById(R.id.titleTxt);
            title = (RoundImageView) convertView.findViewById(R.id.title);
            titleTxt.bringToFront();
            titleTxt.setVisibility(View.INVISIBLE);
            title.setVisibility(View.INVISIBLE);
            friendNameText = (TextView) convertView.findViewById(R.id.userNameTxt1);
            purposesSpinner = (Spinner) convertView.findViewById(R.id.purposeSpinner);
            profile_image = (CircleImageView) convertView.findViewById(R.id.profile_image);
            validateBtn = (TextView) convertView.findViewById(R.id.validateBtn);
            consumerIDTxt = (TextView) convertView.findViewById(R.id.consumerIDTxt);
            dueDateTxt = (TextView) convertView.findViewById(R.id.dueDateTxt);
            billingMonthTxt = (TextView) convertView.findViewById(R.id.billingMonthTxt);
            consumerNumberTxt = (TextView) convertView.findViewById(R.id.consumerNumberTxt);
            billingTxt = (TextView) convertView.findViewById(R.id.billingTxt);
            payableAfterTxt = (TextView) convertView.findViewById(R.id.payableAfterTxt);
            payableBeforeTxt = (TextView) convertView.findViewById(R.id.payableBeforeTxt);
            statusTxt = (TextView) convertView.findViewById(R.id.statusTxt);
            upperPart1 = (RelativeLayout) convertView.findViewById(R.id.upperPart1);
            upperPart2 = (RelativeLayout) convertView.findViewById(R.id.upperPart2);
            friendNameTxt = (TextView) convertView.findViewById(R.id.friendNameTxt);
            payabaleAmount = (EditText) convertView.findViewById(R.id.payabaleAmount);
            payabaleAmount.setText("");
            cardView = (CardView) convertView.findViewById(R.id.card_view);
            userNameTxt = (TextView) convertView.findViewById(R.id.userNameTxt);
            userNameTxt.setText("Select Friend");
            itemName = (TextView) convertView.findViewById(R.id.itemName);
            itemName.setText("Select Biller");
            errorMessageForAmount = (TextView) convertView.findViewById(R.id.errorMessageForAmount);
            errorMessageForRemarks = (TextView) convertView.findViewById(R.id.errorMessageForRemarks);
            errorMessageForSpinner = (TextView) convertView.findViewById(R.id.errorMessageForSpinner);
            editRemarks = (EditText) convertView.findViewById(R.id.editRemarks);
            editAmount = (EditText) convertView.findViewById(R.id.editAmount);
            expandableLayout0 = (ExpandableLayout) convertView.findViewById(R.id.expandable_layout_0);
            requestQueue = Volley.newRequestQueue(getActivity());
            mainTxt = (TextView) convertView.findViewById(R.id.mainTxt);
            helper = Helper.getHelper(getActivity());
            constants = Constants.getConstants(getActivity());
            pd = new Dialog(getActivity());
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            purpStringArrayList = new ArrayList<>();
            utils = new Utils(getActivity());
            String menus = constants.sharedPreferences.getString("Menus", "N/A");
            payabaleAmount.setText("");
            Intent intent = getActivity().getIntent();
            Calendar today = Calendar.getInstance();
            today.set(Calendar.MILLISECOND, 0);
            today.set(Calendar.SECOND, 0);
            today.set(Calendar.MINUTE, 0);
            today.set(Calendar.HOUR_OF_DAY, 0);
            appPreferences = new AppPreferences(getActivity());
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");

            try {
                userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


                } else {

                    userType = "N/A";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }

            type = intent.getStringExtra("requestbill");

            if (!TextUtils.isEmpty(type) && type.equals("payBill")) {
                String billStatus1 = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                isCheck = true;
//            arrowDestination.setVisibility(View.INVISIBLE);
//            upperPart2.setEnabled(false);
//            editAmount.setText(constants.destinationAccount.getString("amount", "N/A"));
//            editAmount.setFocusableInTouchMode(false);
//            editRemarks.setText(constants.destinationAccount.getString("remarks", "N/A"));
//            editRemarks.setFocusableInTouchMode(false);
                distributorID = _crypt.decrypt(constants.destinationAccount.getString("DistributorID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                distributorAccountNumber = _crypt.decrypt(constants.destinationAccount.getString("distributorAccountNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                amountAfterDue = _crypt.decrypt(constants.destinationAccount.getString("AmountAfterDueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                amountBeforeDue = _crypt.decrypt(constants.destinationAccount.getString("AmountWithinDueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                billStatus = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                billingMonth = _crypt.decrypt(constants.destinationAccount.getString("BillingMonth", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                dueDate = _crypt.decrypt(constants.destinationAccount.getString("DueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                consumerName = _crypt.decrypt(constants.destinationAccount.getString("ConsumerName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                billConsumerNumber = _crypt.decrypt(constants.destinationAccount.getString("BillConsumerNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                companyName = _crypt.decrypt(constants.destinationAccount.getString("CompanyName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                alias = _crypt.decrypt(constants.destinationAccount.getString("alias", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                categoryType = _crypt.decrypt(constants.destinationAccount.getString("categoryType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
//            messageId = constants.destinationAccount.getString("messageId", "N/A");
//            targetId = constants.destinationAccount.getString("targetId", "N/A");
//            friendNumber = constants.destinationAccount.getString("friendNumber", "N/A");
//                itemName.setText(distributorName);
                constants.destinationAccountEditor.putString("isSelect", "No");
                constants.destinationAccountEditor.commit();
                constants.destinationAccountEditor.apply();
                payableBeforeTxt.setText(amountBeforeDue);
                payableAfterTxt.setText(amountAfterDue);
                billingMonthTxt.setText(billingMonth);
                billingTxt.setText(companyName);
                dueDateTxt.setText(dueDate);

                if (billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("unpaid") || billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("partialpayment")) {

                    statusTxt.setText("Un-Paid");
                } else if (billStatus.toLowerCase().equals("paid")) {

                    statusTxt.setText("Paid");
                } else if (billStatus.toLowerCase().equals("blocked")) {

                    statusTxt.setText("Blocked");
                }

                consumerIDTxt.setText(billConsumerNumber);
                consumerNumberTxt.setText(consumerName);
                itemName.setText(alias);

                if (billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("unpaid") || billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("blocked") || billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("partialpayment")) {

                    statusTxt.setTextColor(getActivity().getResources().getColor(R.color.tabunderline));
                } else
                    statusTxt.setTextColor(getActivity().getResources().getColor(R.color.lgreen));

                Date currentDate = today.getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date mDate;
                long timeInMilliseconds = 0, currentTimeToMili = 0;
                try {
                    mDate = sdf.parse(dueDate);
                    timeInMilliseconds = mDate.getTime();
                    currentTimeToMili = currentDate.getTime();
                    System.out.println("Date in milli :: " + timeInMilliseconds);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (!TextUtils.isEmpty(billStatus) && !billStatus.equals("N/A")) {

                    if (billStatus.toLowerCase().equals("partial payment")) {


                    } else {

                        if (currentTimeToMili <= timeInMilliseconds) {


                            payabaleAmount.setText(amountBeforeDue);
                            editAmount.setText(amountBeforeDue.split("\\.")[0]);
                            editAmount.setFocusableInTouchMode(false);

                        } else if (currentTimeToMili > timeInMilliseconds) {

                            payabaleAmount.setText(amountAfterDue);
                            editAmount.setText(amountAfterDue.split("\\.")[0]);
                            editAmount.setFocusableInTouchMode(false);

                        }
                    }
                }

                if (currentTimeToMili <= timeInMilliseconds) {


                    payabaleAmount.setText(amountBeforeDue);

                } else if (currentTimeToMili > timeInMilliseconds) {

                    payabaleAmount.setText(amountAfterDue);

                }


            }
            if (!TextUtils.isEmpty(menus) && !menus.equals("N/A") && !menus.equals("")) {
                if (menus.toLowerCase().contains("request bill")) {
                    showDilogForErrorForMenu("This service is currently disabled. Please try again later.", "WARNING");
                }
            }
            String list = constants.purposeBill.getString("purposeList", "N/A");

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!flag) {
                        expandableLayout0.expand();
                        mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_less, 0);
                        flag = true;
                    } else {
                        expandableLayout0.collapse();
                        mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_more, 0);
                        flag = false;
                    }

                }
            });

            if (list.equals("N/A")) {
                GetPurposeBillList();
            } else {
                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<String>>() {
                }.getType();

                purpStringArrayList = gson.fromJson(list, type);
                purpose = new String[purpStringArrayList.size()];
                purpose = purpStringArrayList.toArray(purpose);
                spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                purposeAdapter.addAll(purpose);
                purposeAdapter.add("Purpose of Payment");
                purposesSpinner.setAdapter(purposeAdapter);
                purposesSpinner.setSelection(0);
                purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


                            // Get select item
                            if (purposesSpinner.getSelectedItem() == "Purpose of Payment") {
                                purposeSelected = purposesSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + purposeSelected);

                            } else {

                                purposeSelected = purposesSpinner.getSelectedItem().toString();

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });
            }
            int stanNumber = 6;
            stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
            int rrnNumber = 12;
            rrn = String.valueOf(helper.generateRandom(rrnNumber));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void GetPurposeBillList() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    pd.show();
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sPurposeGroupCode", "BP");
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(getActivity());

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getPuposesURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("POTList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        purpStringArrayList.add(jsonObj.getString("Name"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();

                                                    }

                                                }

                                                purpose = new String[purpStringArrayList.size()];
                                                purpose = purpStringArrayList.toArray(purpose);

                                                Gson gson = new Gson();
                                                String json = gson.toJson(purpStringArrayList);
                                                constants.purposeBillList.putString("purposeList", json);
                                                constants.purposeBillList.commit();
                                                constants.purposeBillList.apply();
                                                showSpinner();
                                            } else if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());
                                                validateBtn.setEnabled(true);

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            validateBtn.setEnabled(true);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sPurposeGroupCode", "BP");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    validateBtn.setEnabled(true);
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
        }
    }

    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                pd.dismiss();
                spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                purposeAdapter.addAll(purpose);
                purposeAdapter.add("Purpose of Payment");
                purposesSpinner.setAdapter(purposeAdapter);
                purposesSpinner.setSelection(0);
                purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


                            // Get select item
                            if (purposesSpinner.getSelectedItem() == "Purpose of Payment") {
                                purposeSelected = purposesSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + purposeSelected);

                            } else {

                                purposeSelected = purposesSpinner.getSelectedItem().toString();

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });

            }

        }, 5000);
    }


    @Override
    public void onResume() {
        super.onResume();
        try {

            CryptLib _crypt = new CryptLib();

            if (isFirst) {

                upperPart1.setEnabled(true);
                upperPart2.setEnabled(true);
                if (isSource) {

                    isSelectSource = constants.sourceAccount.getString("isSelect", "N/A");
                    if (!isSelectSource.equals("N/A") && isSelectSource.equals("Yes")) {
                        friendNumber = _crypt.decrypt(constants.sourceAccount.getString("friendNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        destinationName = _crypt.decrypt(constants.sourceAccount.getString("friendName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        userID = _crypt.decrypt(constants.sourceAccount.getString("userID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        friendID = _crypt.decrypt(constants.sourceAccount.getString("friendID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        String em = constants.sourceAccount.getString("email", "N/A");
                        if (!TextUtils.isEmpty(em) && !em.equals("N/A")) {
                            email = _crypt.decrypt(constants.sourceAccount.getString("email", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        } else {
                            email = "N/A";
                        }
                        String mobb = constants.sourceAccount.getString("mobileNumber", "N/A");
                        if (!TextUtils.isEmpty(mobb) && !mobb.equals("N/A")) {
                            mobile = _crypt.decrypt(constants.sourceAccount.getString("mobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        } else {
                            mobile = "N/A";
                        }
                        isFav = _crypt.decrypt(constants.sourceAccount.getString("isFav", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        friendImage = _crypt.decrypt(constants.sourceAccount.getString("friendImage", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        String s = destinationName.substring(0, 1);

                        switch (s.toUpperCase()) {

                            case "A":
                                color = getResources().getColor(R.color.friendColor1);
                                break;

                            case "B":
                                color = getResources().getColor(R.color.friendColor1);
                                break;

                            case "C":
                                color = getResources().getColor(R.color.friendColor1);
                                break;

                            case "D":
                                color = getResources().getColor(R.color.friendColor1);
                                break;

                            case "E":
                                color = getResources().getColor(R.color.friendColor1);
                                break;

                            case "F":
                                color = getResources().getColor(R.color.friendColor1);
                                break;

                            case "G":
                                color = getResources().getColor(R.color.friendColor1);
                                break;

                            case "H":
                                color = getResources().getColor(R.color.friendColor2);
                                break;

                            case "I":
                                color = getResources().getColor(R.color.friendColor2);
                                break;

                            case "J":
                                color = getResources().getColor(R.color.friendColor2);
                                break;

                            case "K":
                                color = getResources().getColor(R.color.friendColor2);
                                break;

                            case "L":
                                color = getResources().getColor(R.color.friendColor2);
                                break;

                            case "M":
                                color = getResources().getColor(R.color.friendColor2);
                                break;

                            case "N":
                                color = getResources().getColor(R.color.friendColor3);
                                break;

                            case "O":
                                color = getResources().getColor(R.color.friendColor3);
                                break;

                            case "P":
                                color = getResources().getColor(R.color.friendColor3);
                                break;

                            case "Q":
                                color = getResources().getColor(R.color.friendColor3);
                                break;

                            case "R":
                                color = getResources().getColor(R.color.friendColor3);
                                break;

                            case "S":
                                color = getResources().getColor(R.color.friendColor3);
                                break;

                            case "T":
                                color = getResources().getColor(R.color.friendColor4);
                                break;

                            case "U":
                                color = getResources().getColor(R.color.friendColor4);
                                break;

                            case "V":
                                color = getResources().getColor(R.color.friendColor4);
                                break;

                            case "W":
                                color = getResources().getColor(R.color.friendColor4);
                                break;

                            case "X":
                                color = getResources().getColor(R.color.friendColor4);
                                break;

                            case "Y":
                                color = getResources().getColor(R.color.friendColor4);
                                break;

                            case "Z":
                                color = getResources().getColor(R.color.friendColor4);
                                break;

                            default:
                                color = getResources().getColor(R.color.defaultColor);
                                break;
                        }
                        userNameTxt.setVisibility(View.INVISIBLE);
                        friendNameText.setVisibility(View.VISIBLE);
                        friendNameText.setText(destinationName);
//                    userNameTxt.setText(destinationName);
                        if (!TextUtils.isEmpty(friendImage) && !friendImage.equals("N/A")) {
                            titleTxt.setVisibility(View.INVISIBLE);
                            title.setVisibility(View.INVISIBLE);
                            profile_image.setVisibility(View.VISIBLE);
                            byte[] imageByteArray = Base64.decode(friendImage, Base64.DEFAULT);
                            RequestOptions options = new RequestOptions();
                            options.centerCrop();
                            Glide.with(getActivity())
                                    .load(imageByteArray)
                                    .apply(options)
                                    .into(profile_image);

                        } else {
                            titleTxt.setVisibility(View.VISIBLE);
                            title.setVisibility(View.VISIBLE);
                            profile_image.setVisibility(View.INVISIBLE);
                            title.setBackgroundTintList(ColorStateList.valueOf((color)));
                            titleTxt.setText(s.toUpperCase());
                        }
                    }

                    constants.sourceAccountEditor.putString("isSelect", "No");
                    constants.sourceAccountEditor.commit();
                    constants.sourceAccountEditor.apply();
                    isSource = false;
                    isFirst = false;
                }

                if (isDestination) {

                    isSelectDestination = constants.destinationAccount.getString("isSelect", "N/A");
                    if (!isSelectDestination.equals("N/A") && isSelectDestination.equals("Yes")) {
                        distributorID = _crypt.decrypt(constants.destinationAccount.getString("DistributorID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        distributorAccountNumber = _crypt.decrypt(constants.destinationAccount.getString("distributorAccountNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        amountAfterDue = _crypt.decrypt(constants.destinationAccount.getString("AmountAfterDueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        amountBeforeDue = _crypt.decrypt(constants.destinationAccount.getString("AmountWithinDueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        billStatus = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        billingMonth = _crypt.decrypt(constants.destinationAccount.getString("BillingMonth", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        dueDate = _crypt.decrypt(constants.destinationAccount.getString("DueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        consumerName = _crypt.decrypt(constants.destinationAccount.getString("ConsumerName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        billConsumerNumber = _crypt.decrypt(constants.destinationAccount.getString("BillConsumerNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        companyName = _crypt.decrypt(constants.destinationAccount.getString("CompanyName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        alias = _crypt.decrypt(constants.destinationAccount.getString("alias", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        categoryType = _crypt.decrypt(constants.destinationAccount.getString("categoryType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
//                itemName.setText(distributorName);

                        payableBeforeTxt.setText(amountBeforeDue);
                        payableAfterTxt.setText(amountAfterDue);
                        billingMonthTxt.setText(billingMonth);
                        billingTxt.setText(companyName);
                        dueDateTxt.setText(dueDate);
                        statusTxt.setText(billStatus);
                        consumerIDTxt.setText(billConsumerNumber);
                        consumerNumberTxt.setText(consumerName);
                        itemName.setText(alias);

                        if (billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("unpaid") || billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("blocked")) {

                            statusTxt.setTextColor(getActivity().getResources().getColor(R.color.tabunderline));
                        } else
                            statusTxt.setTextColor(getActivity().getResources().getColor(R.color.lgreen));

                        Date currentDate = Calendar.getInstance().getTime();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date mDate;
                        long timeInMilliseconds = 0, currentTimeToMili = 0;
                        try {
                            mDate = sdf.parse(dueDate);
                            timeInMilliseconds = mDate.getTime();
                            currentTimeToMili = currentDate.getTime();
                            System.out.println("Date in milli :: " + timeInMilliseconds);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        String billStatus1 = constants.destinationAccount.getString("BillStatus", "N/A");
                        if (!TextUtils.isEmpty(billStatus) && !billStatus.equals("N/A")) {

                            if (billStatus.toLowerCase().equals("partial payment")) {

                                editAmount.setText("");
                                editAmount.setFocusableInTouchMode(true);
                                editAmount.setEnabled(true);
                            } else {

                                if (currentTimeToMili <= timeInMilliseconds) {


                                    payabaleAmount.setText(amountBeforeDue);
                                    editAmount.setText(amountBeforeDue.split("\\.")[0]);
                                    editAmount.setFocusableInTouchMode(false);
                                    editAmount.setEnabled(false);


                                } else if (currentTimeToMili > timeInMilliseconds) {

                                    payabaleAmount.setText(amountAfterDue);
                                    editAmount.setText(amountAfterDue.split("\\.")[0]);
                                    editAmount.setFocusableInTouchMode(false);
                                    editAmount.setEnabled(false);

                                }

                            }
                        }


                        if (currentTimeToMili <= timeInMilliseconds) {


                            payabaleAmount.setText(amountBeforeDue);

                        } else if (currentTimeToMili > timeInMilliseconds) {

                            payabaleAmount.setText(amountAfterDue);

                        }
                    }

                    constants.destinationAccountEditor.putString("isSelect", "No");
                    constants.destinationAccountEditor.commit();
                    constants.destinationAccountEditor.apply();
                    isDestination = false;
                    isFirst = false;
                }
            } else {

                if (isCheck) {

                } else {
                    payabaleAmount.setText("");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public void showDilogForErrorForMenu(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(getActivity(), HomeScreen.class);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).startActivity(intent);
                ((Activity) getActivity()).finish();
                ((Activity) getActivity()).finishAffinity();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


}