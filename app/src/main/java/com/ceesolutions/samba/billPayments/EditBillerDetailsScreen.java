package com.ceesolutions.samba.billPayments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/10/18.
 */

public class EditBillerDetailsScreen extends AppCompatActivity {
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private TextInputEditText editAlias, editConsumerName, editConsumerNumber, editEmailAddress, editAccountTitle, editBankName;
    private TextView headingTxt, validateBtn, transferFundsBtn, addToFavBtn, deleteBeneficiaryBtn, textView, doneBtn, heading, billerTxt, btn;
    private Dialog dialog, dialog1;
    private WebService webService;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private Intent intent1;
    private ImageView backBtn, notificationBtn;
    private String billerID, bankCode, isFav, type, edit;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_biller);
        getSupportActionBar().hide();
        initViews();

        transferFundsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                transferFundsBtn.setEnabled(false);

                FirebaseAnalytics.getInstance(EditBillerDetailsScreen.this).logEvent("Biller_Pay_Bill_Pressed", new Bundle());

                BillInquiry();
//                finish();
            }
        });

        addToFavBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isFav.equals("1")) {
                    showDilogForError("Are you sure you want to remove the biller from favourite?", "CONFIRMATION", "fav", "0");

                } else {
                    showDilogForError("Are you sure you want to mark the biller as favourite?", "CONFIRMATION", "fav", "1");


                }
            }
        });

        deleteBeneficiaryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDilogForError("Are you sure you want to delete the biller?", "CONFIRMATION", "delete", "");
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EditBillerDetailsScreen.this, ManageBillerScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
            }
        });


        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EditBillerDetailsScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(EditBillerDetailsScreen.this, ManageBillerScreen.class);
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
    }

    private void initViews() {

        intent1 = getIntent();
        billerTxt = (TextView) findViewById(R.id.billerTxt);
        editAlias = (TextInputEditText) findViewById(R.id.editAlias);
        editConsumerName = (TextInputEditText) findViewById(R.id.editConsumerName);
        editConsumerNumber = (TextInputEditText) findViewById(R.id.editConsumerNumber);
        transferFundsBtn = (TextView) findViewById(R.id.transferFundsBtn);
        validateBtn = (TextView) findViewById(R.id.validateBtn);
        addToFavBtn = (TextView) findViewById(R.id.addToFavBtn);
        headingTxt = (TextView) findViewById(R.id.headingTxt);
        deleteBeneficiaryBtn = (TextView) findViewById(R.id.deleteBeneficiaryBtn);
        webService = new WebService();
        backBtn = (ImageView) findViewById(R.id.backBtn);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        helper = Helper.getHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(EditBillerDetailsScreen.this);
        utils = new Utils(EditBillerDetailsScreen.this);
        pd = new Dialog(EditBillerDetailsScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        try {
            CryptLib _crypt = new CryptLib();
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            type = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            editAlias.setText(intent1.getStringExtra("alias"));
            editConsumerName.setText(intent1.getStringExtra("consumerNumber"));
            if (!TextUtils.isEmpty(intent1.getStringExtra("consumerName")) && !intent1.getStringExtra("consumerName").equals("N/A"))
                editConsumerNumber.setText(intent1.getStringExtra("consumerName"));
            else
                editConsumerNumber.setText("N/A");

            billerTxt.setText(intent1.getStringExtra("companyName"));
            billerID = intent1.getStringExtra("billerID");
            isFav = intent1.getStringExtra("isFav");
            appPreferences = new AppPreferences(this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            if (isFav.equals("1")) {

                addToFavBtn.setText("Remove Favourite");
                addToFavBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.favoritefill, 0, 0, 0);
                addToFavBtn.setBackgroundResource(R.drawable.border_blue);
                addToFavBtn.setTextColor(getResources().getColor(R.color.notiheader));

            } else {

            }
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void showDilogForError(String msg, String header, final String type, final String sAction) {

        dialog = new Dialog(EditBillerDetailsScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doneBtn.setEnabled(false);
                if (type.equals("fav")) {
                    dialog.dismiss();
                    AddBillerToFav(sAction);
                } else {
                    dialog.dismiss();
                    DeleteBiller();
                }

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public void AddBillerToFav(final String sAction) {


        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sSigns_BillerFavoriteFlag", sAction);
                    params.put("sBillerID", billerID);
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(EditBillerDetailsScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.favBillerURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_BillerFavorite_Mark_UnMarkResult").getString("Status_Code").equals("00")) {

                                                pd.dismiss();
//                                            utils.showDilogForError("Biller has successfully marked as favourite","SUCCESS");
                                                Intent intent = new Intent(EditBillerDetailsScreen.this, ManageBillerScreen.class);
                                                overridePendingTransition(0, 0);
                                                intent.putExtra("addBiller", "addBiller");
                                                doneBtn.setEnabled(true);
                                                startActivity(intent);
                                                finish();


                                            } else if (jsonObject.getJSONObject("Signs_BillerFavorite_Mark_UnMarkResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillerFavorite_Mark_UnMarkResult").getString("Status_Description"), "ERROR", EditBillerDetailsScreen.this, new LoginScreen());
                                                doneBtn.setEnabled(true);

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_BillerFavorite_Mark_UnMarkResult").getString("Status_Description"), "ERROR");
                                                doneBtn.setEnabled(true);
                                            }

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            doneBtn.setEnabled(true);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        doneBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        doneBtn.setEnabled(true);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        doneBtn.setEnabled(true);
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sSigns_BillerFavoriteFlag", sAction);
//                            params.put("sBillerID", billerID);
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    doneBtn.setEnabled(true);
                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                doneBtn.setEnabled(true);
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            doneBtn.setEnabled(true);
        }


    }


    public void DeleteBiller() {


        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sBillerID", billerID);
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(EditBillerDetailsScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.deleteBillerURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_RemoveBillerResult").getString("Status_Code").equals("00")) {

                                                pd.dismiss();
                                                Intent intent = new Intent(EditBillerDetailsScreen.this, ManageBillerScreen.class);
                                                overridePendingTransition(0, 0);
                                                intent.putExtra("addBiller", "addBiller");
                                                doneBtn.setEnabled(true);
                                                startActivity(intent);
                                                finish();

                                            } else if (jsonObject.getJSONObject("Signs_RemoveBillerResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_RemoveBillerResult").getString("Status_Description"), "ERROR", EditBillerDetailsScreen.this, new LoginScreen());
                                                doneBtn.setEnabled(true);

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_RemoveBillerResult").getString("Status_Description"), "ERROR");
                                                doneBtn.setEnabled(true);
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            doneBtn.setEnabled(true);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        doneBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        doneBtn.setEnabled(true);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        doneBtn.setEnabled(true);
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sBillerID", billerID);
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    doneBtn.setEnabled(true);
                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                doneBtn.setEnabled(true);
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            doneBtn.setEnabled(true);
        }


    }

    public void BillInquiry() {

        final int stanNumber = 6;

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sBillConsumerNumber", intent1.getStringExtra("consumerNumber"));
                    params.put("sDistributorID", intent1.getStringExtra("companyID"));
                    params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(EditBillerDetailsScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.billInquiryURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            try {

                                                CryptLib _crypt = new CryptLib();
                                                if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("00")) {

                                                    if (!TextUtils.isEmpty(type) && type.equals("SAMBA")) {
                                                        Intent intent = new Intent(EditBillerDetailsScreen.this, PayBillsScreen.class);
                                                        intent.putExtra("requestbill", "bill");
                                                        constants.destinationAccountEditor.putString("isSelect", "Yes");
                                                        constants.destinationAccountEditor.putString("AmountAfterDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountAfterDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("AmountWithinDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountWithinDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("BillStatus", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillStatus"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("BillingMonth", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillingMonth"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("CompanyID", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("DueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("DueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("ConsumerName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("ConsumerName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("CompanyName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("RRN", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("RRN"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("BillConsumerNumber", _crypt.encryptForParams(intent1.getStringExtra("consumerNumber"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("DistributorID", _crypt.encryptForParams(intent1.getStringExtra("companyID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("distributorAccountNumber", _crypt.encryptForParams(intent1.getStringExtra("accountNumber"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("alias", _crypt.encryptForParams(intent1.getStringExtra("alias"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("categoryType", _crypt.encryptForParams(intent1.getStringExtra("categoryType"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.commit();
                                                        constants.destinationAccountEditor.apply();
                                                        pd.dismiss();
                                                        transferFundsBtn.setEnabled(true);
                                                        overridePendingTransition(0, 0);
                                                        startActivity(intent);
                                                    } else {

                                                        Intent intent = new Intent(EditBillerDetailsScreen.this, PayBillsScreen.class);
                                                        intent.putExtra("requestbill", "payBill");
                                                        constants.destinationAccountEditor.putString("isSelect", "Yes");
                                                        constants.destinationAccountEditor.putString("AmountAfterDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountAfterDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("AmountWithinDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountWithinDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("BillStatus", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillStatus"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("BillingMonth", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillingMonth"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("CompanyID", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("DueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("DueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("ConsumerName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("ConsumerName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("CompanyName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("RRN", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("RRN"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("BillConsumerNumber", _crypt.encryptForParams(intent1.getStringExtra("consumerNumber"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("DistributorID", _crypt.encryptForParams(intent1.getStringExtra("companyID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("distributorAccountNumber", _crypt.encryptForParams(intent1.getStringExtra("accountNumber"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("alias", _crypt.encryptForParams(intent1.getStringExtra("alias"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.putString("categoryType", _crypt.encryptForParams(intent1.getStringExtra("categoryType"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.destinationAccountEditor.commit();
                                                        constants.destinationAccountEditor.apply();
                                                        pd.dismiss();
                                                        transferFundsBtn.setEnabled(true);
                                                        overridePendingTransition(0, 0);
                                                        startActivity(intent);
                                                    }

                                                } else if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("54")) {
                                                    pd.dismiss();
                                                    utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR", EditBillerDetailsScreen.this, new LoginScreen());
                                                    transferFundsBtn.setEnabled(true);

                                                } else {

                                                    pd.dismiss();
                                                    utils.showDilogForError(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR");
                                                    transferFundsBtn.setEnabled(true);

                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            transferFundsBtn.setEnabled(true);

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        transferFundsBtn.setEnabled(true);

                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        transferFundsBtn.setEnabled(true);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        transferFundsBtn.setEnabled(true);

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sBillConsumerNumber", intent1.getStringExtra("consumerNumber"));
//                            params.put("sDistributorID", intent1.getStringExtra("companyID"));
//                            params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    transferFundsBtn.setEnabled(true);

                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                transferFundsBtn.setEnabled(true);

            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            transferFundsBtn.setEnabled(true);

        }
    }


}
