package com.ceesolutions.samba.billPayments.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.billPayments.Model.Category;
import com.ceesolutions.samba.billPayments.SelectBillerDistributorScreen;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

/**
 * Created by ceeayaz on 3/6/18.
 */

public class CustomListAdapter extends BaseAdapter {
    ArrayList<Category> categoryList;
    LayoutInflater layoutInflater;
    View row;
    Context context;
    TextView categoryName;
    ImageView title;
    String type;

    public CustomListAdapter(ArrayList<Category> categoryArrayList, Context mContext, String isType) {
        categoryList = categoryArrayList;
        context = mContext;
        type = isType;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return categoryList.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.category_item, parent, false);
        }

        title = (ImageView) convertView.findViewById(R.id.title);
        categoryName = (TextView) convertView.findViewById(R.id.categoryName);
        String name = categoryList.get(position).getName();

        switch (name) {

            case "Electricity": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_electric));
                categoryName.setText(name);
                break;
            }

            case "Gas": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_gas));
                categoryName.setText(name);
                break;


            }


            case "Water and Sewerage": {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_water));
                categoryName.setText(name);
                break;
            }

            case "Telecommunication": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_telecom));
                categoryName.setText(name);
                break;

            }

            case "Mobile Prepaid": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_ph));
                categoryName.setText(name);
                break;

            }

            case "Mobile Postpaid": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_ph));
                categoryName.setText(name);
                break;

            }

            case "ISP Payment": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_isp));
                categoryName.setText(name);
                break;

            }

            case "Education": {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_edu));
                categoryName.setText(name);
                break;
            }

            case "Airlines": {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon));
                categoryName.setText(name);
                break;
            }

            case "TAX": {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_tax));
                categoryName.setText(name);
                break;
            }

            default: {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.default1));
                categoryName.setText(name);
                break;
            }

        }


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if(!TextUtils.isEmpty(type) && type.equals("addAccount")){
//
//                    Intent intent = new Intent(context, BeneficiaryFormScreen.class);
//                    intent.putExtra("categoryName",categoryList.get(position).getcategoryName());
//                    intent.putExtra("bankCode",categoryList.get(position).getBankId());
//                    intent.putExtra("type","addAccount");
//                    context.startActivity(intent);
//                    ((Activity)context).finish();
//
//
//                }else{
//


                FirebaseAnalytics.getInstance(context).logEvent("Bill_Category_Selected", new Bundle());

                Intent intent = new Intent(context, SelectBillerDistributorScreen.class);
                intent.putExtra("categoryID", categoryList.get(position).getId());
                intent.putExtra("categoryName", categoryList.get(position).getName());
                intent.putExtra("categoryCompanyType", categoryList.get(position).getCategoryCompanyType());
                context.startActivity(intent);
//                ((Activity) context).finish();
//
//                }

            }
        });

        return convertView;
    }
}