package com.ceesolutions.samba.billPayments.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ceesolutions.samba.billPayments.Fragments.BillPaymentFragment;
import com.ceesolutions.samba.billPayments.Fragments.PayBillFragment;
import com.ceesolutions.samba.billPayments.Fragments.PrepaidVoucherFragment;
import com.ceesolutions.samba.billPayments.Fragments.RequestBillPaymentFragment;
import com.ceesolutions.samba.billPayments.Fragments.RequestPrepaidVoucherFragment;

/**
 * Created by ceeayaz on 3/9/18.
 */

public class PayBillsAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PayBillsAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                PayBillFragment payBillFragment = new PayBillFragment();
                return payBillFragment;
            case 1:
                PrepaidVoucherFragment prepaidVoucherFragment = new PrepaidVoucherFragment();
                return prepaidVoucherFragment;
            case 2:
                RequestBillPaymentFragment requestBillPaymentFragment = new RequestBillPaymentFragment();
                return requestBillPaymentFragment;
            case 3:
                RequestPrepaidVoucherFragment requestPrepaidVoucherFragment = new RequestPrepaidVoucherFragment();
                return requestPrepaidVoucherFragment;
            case 4:
                BillPaymentFragment billPaymentFragment = new BillPaymentFragment();
                return billPaymentFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}