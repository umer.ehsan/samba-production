package com.ceesolutions.samba.billPayments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import com.ceesolutions.samba.eatMubarak.EatMubarakMainScreen;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.Bookme.BookMeMainScreen;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.beneficiariesManagement.ManageBeneficiaresScreen;
import com.ceesolutions.samba.billPayments.Adapter.CustomHorizontalListAdapter;
import com.ceesolutions.samba.billPayments.Adapter.CustomListManageBillerAdapter;
import com.ceesolutions.samba.billPayments.Model.Biller;
import com.ceesolutions.samba.cardManagement.ManageCardsScreen;
import com.ceesolutions.samba.masterCardIntegration.MainActivity;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.transactions.AccountsScreen;
import com.ceesolutions.samba.transactions.FundTransferScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.TapTarget;
import com.ceesolutions.samba.utils.TapTargetView;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.HorizontalListView;
import com.daimajia.swipe.util.Attributes;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.OnBoomListenerAdapter;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/9/18.
 */

public class ManageBillerScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private ListView listView;
    private CustomListManageBillerAdapter customListAdapter;
    private HorizontalListView horizontalListView;
    private CustomHorizontalListAdapter customHorizontalListAdapter;
    private ArrayList<Biller> billerList;
    private ArrayList<Biller> favouritesBillers;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd, dialog;
    private Utils utils;
    private ImageView backBtn, notificationBtn, sambaBtn, shareBtn, helpBtn;
    private String menu, userType;
    private BoomMenuButton bmb;
    private EditText chatBotText;
    private String type, backType;
    private TextView headingTxt, allText;
    private Intent intent;
    private FloatingActionButton floatingAction;
    private TextView heading, textView, btn;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_bill);
        initViews();
        try {
            CryptLib _crypt = new CryptLib();
            getSupportActionBar().hide();
            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }
            menu = constants.sharedPreferences.getString("Menus", "N/A");
            if (!TextUtils.isEmpty(menu) && !menu.equals("N/A") && !menu.equals("")) {


            } else {

                menu = "N/A";
            }
            headingTxt.setText("Manage Billers");
            allText.setText("All Billers");

            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                Intent intent = new Intent(ManageBillerScreen.this, HomeScreen.class);
//                overridePendingTransition(0, 0);
//                startActivity(intent);
                    if (!TextUtils.isEmpty(backType) && backType.equals("addBiller")) {

                        Intent intent = new Intent(ManageBillerScreen.this, HomeScreen.class);
                        overridePendingTransition(0, 0);
                        startActivity(intent);
                        finish();

                    } else
                        finish();
                }
            });

            notificationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(ManageBillerScreen.this, MessageCenter.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
//                finish();
                }
            });
            floatingAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FirebaseAnalytics.getInstance(ManageBillerScreen.this).logEvent("Add_Bill_Pressed", new Bundle());

                    Intent intent = new Intent(ManageBillerScreen.this, SelectCategoryScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void initViews() {

        intent = getIntent();

        type = intent.getStringExtra("source");
        backType = intent.getStringExtra("addBiller");

        if (!TextUtils.isEmpty(type) && type.equals("destination")) {


        } else if (!TextUtils.isEmpty(backType) && backType.equals("addBiller")) {


        } else
            type = "noDes";

        sambaBtn = (ImageView) findViewById(R.id.sambaBtn);
        shareBtn = (ImageView) findViewById(R.id.shareBtn);
        chatBotText = (EditText) findViewById(R.id.chatBoxText);
        allText = (TextView) findViewById(R.id.allText);
        listView = (ListView) findViewById(R.id.listView);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        helpBtn = (ImageView) findViewById(R.id.helpBtn);
        headingTxt = (TextView) findViewById(R.id.headingTxt);
        floatingAction = (FloatingActionButton) findViewById(R.id.floatingAction);
        horizontalListView = (HorizontalListView) findViewById(R.id.horizontalListView);
        billerList = new ArrayList<>();
        favouritesBillers = new ArrayList<>();
        utils = new Utils(ManageBillerScreen.this);
        requestQueue = Volley.newRequestQueue(ManageBillerScreen.this);
        helper = Helper.getHelper(ManageBillerScreen.this);
        constants = Constants.getConstants(ManageBillerScreen.this);
        pd = new Dialog(ManageBillerScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                GetbillerList();

            }
        }, 500);
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");





        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        bmb = (BoomMenuButton) findViewById(R.id.bmb);
        assert bmb != null;
        bmb.setButtonEnum(ButtonEnum.TextOutsideCircle);
        bmb.setBackgroundColor(Color.TRANSPARENT);
        bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_9_1);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_9_1);


        String coachMarks = constants.coachMarks.getString("manageBillers", "N/A");

//        if (!TextUtils.isEmpty(coachMarks) && coachMarks.equals("N/A")) {
//            constants.coachMarksEditor.putString("manageBillers", "1");
//            constants.coachMarksEditor.commit();
//            constants.coachMarksEditor.apply();
//            int mainTxt = getResources().getDimensionPixelSize(R.dimen._7sdp);
//            int des = getResources().getDimensionPixelSize(R.dimen._4sdp);
//            TapTargetView.showFor(ManageBillerScreen.this,                 // `this` is an Activity
//                    TapTarget.forView(findViewById(R.id.helpBtn), "", "- Use plus (+) button to register a new biller\n\n" +
//                            "- Swipe your registered biller in the list on left to explore more options")
//                            // All options below are optional
//                            .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
//                            .outerCircleAlpha(0.90f)
//                            // Specify the alpha amount for the outer circle
//                            // Specify a color for the target circle
//                            .titleTextSize(mainTxt)// Specify the size (in sp) of the title text
//                            .titleTextColor(R.color.white)      // Specify the color of the title text
//                            .descriptionTextSize(des)            // Specify the size (in sp) of the description text
//                            .descriptionTextColor(R.color.white)  // Specify the color of the description text
//                            .textColor(R.color.white)            // Specify a color for both the title and description text
//                            .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
//                            // If set, will dim behind the view with 30% opacity of the given color
//                            .drawShadow(true)                   // Whether to draw a drop shadow or not
//                            .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
//                            .tintTarget(true)                   // Whether to tint the target view's color
//                            .transparentTarget(false),           // Specify whether the target is transparent (displays the content underneath)
//                    // Specify a custom drawable to draw as the target
//                    // Specify the target radius (in dp)
//                    new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
//                        @Override
//                        public void onTargetClick(TapTargetView view) {
//                            super.onTargetClick(view);      // This call is optional
////                        doSomething();
//                        }
//                    });
//        }

        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int mainTxt = getResources().getDimensionPixelSize(R.dimen._7sdp);
                int des = getResources().getDimensionPixelSize(R.dimen._4sdp);

                TapTargetView.showFor(ManageBillerScreen.this,                 // `this` is an Activity
                        TapTarget.forView(findViewById(R.id.helpBtn), "About this Screen!!!", "- Use plus (+) button to register a new biller\n\n" +
                                "- Swipe your registered biller in the list on left to explore more options")
                                // All options below are optional
                                .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                .outerCircleAlpha(0.90f)
                                // Specify the alpha amount for the outer circle
                                // Specify a color for the target circle
                                .titleTextSize(mainTxt)// Specify the size (in sp) of the title text
                                .titleTextColor(R.color.white)      // Specify the color of the title text
                                .descriptionTextSize(des)            // Specify the size (in sp) of the description text
                                .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                .textColor(R.color.white)            // Specify a color for both the title and description text
                                .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                                // If set, will dim behind the view with 30% opacity of the given color
                                .drawShadow(true)                   // Whether to draw a drop shadow or not
                                .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                .tintTarget(true)                   // Whether to tint the target view's color
                                .transparentTarget(false),           // Specify whether the target is transparent (displays the content underneath)
                        // Specify a custom drawable to draw as the target
                        // Specify the target radius (in dp)
                        new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                            @Override
                            public void onTargetClick(TapTargetView view) {
                                super.onTargetClick(view);      // This call is optional
//                        doSomething();

                            }
                        });
            }
        });
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {


                    Intent intent = new Intent("android.intent.action.VIEW");

                    /** creates an sms uri */
                    Uri data = Uri.parse("sms:");

                    /** Setting sms uri to the intent */
                    intent.setData(data);
                    intent.putExtra("sms_body", "Download Samba Mobile App and register yourself. Click https://www.samba.com.pk/samba/overviews/samba-smart-app for details");

                    /** Initiates the SMS compose screen, because the activity contain ACTION_VIEW and sms uri */
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        chatBotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDilogForErrorForLogin(getResources().getString(R.string.message), "WARNING");
                Intent i = new Intent(ManageBillerScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(i);
                finish();
                finishAffinity();
            }
        });

        sambaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                bmb.boom();
            }
        });

        bmb.setOnBoomListener(new OnBoomListenerAdapter() {
            @Override
            public void onBoomWillShow() {
                super.onBoomWillShow();
                // logic here
            }
        });

        bmb.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {
                // If you have implement listeners for boom-buttons in builders,
                // then you shouldn't add any listener here for duplicate callbacks.

                switch (index) {


                    case 0:

                        Intent home = new Intent(ManageBillerScreen.this, HomeScreen.class);
                        overridePendingTransition(0, 0);
                        startActivity(home);
                        finish();
                        break;

                    case 1:

                        if (!menu.equals("N/A")) {

                            if (menu.toLowerCase().contains("accounts")) {
                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                            } else {

                                Intent intent = new Intent(ManageBillerScreen.this, AccountsScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Intent intent = new Intent(ManageBillerScreen.this, AccountsScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(intent);
                            finish();

                        }

                        break;

                    case 2:

                        try {

                            CryptLib _crypt = new CryptLib();
                            Intent transfers = new Intent(ManageBillerScreen.this, FundTransferScreen.class);
                            String user = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            if (!TextUtils.isEmpty(user) && !user.equals("N/A")) {

                                if (user.equals("NON_SAMBA")) {

                                    transfers.putExtra("requestFunds", "nonSamba");
                                }
                            }
                            overridePendingTransition(0, 0);
                            startActivity(transfers);
//                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;


                    case 3:

                        try {

                            CryptLib _crypt = new CryptLib();
                            if (!menu.equals("N/A")) {

                                if (menu.toLowerCase().contains("biller management")) {
                                    showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                } else {
                                    String user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                    Intent payments = new Intent(ManageBillerScreen.this, PayBillsScreen.class);
                                    overridePendingTransition(0, 0);
                                    user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                    if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                        if (user1.equals("NON_SAMBA")) {

                                            payments.putExtra("requestbill", "nonSamba");
                                        }
                                    }
                                    startActivity(payments);
//                                    finish();
                                }
                            } else {
                                Intent payments = new Intent(ManageBillerScreen.this, PayBillsScreen.class);
                                overridePendingTransition(0, 0);
                                String user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                    if (user1.equals("NON_SAMBA")) {

                                        payments.putExtra("requestbill", "nonSamba");
                                    }
                                }
                                startActivity(payments);
//                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;


                    case 4:

                        if (!menu.equals("N/A")) {

                            if (menu.toLowerCase().contains("beneficiary management")) {
                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                            } else {

                                if (userType.equals("NON_SAMBA")) {
                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                } else {
                                    Intent bene = new Intent(ManageBillerScreen.this, ManageBeneficiaresScreen.class);
                                    overridePendingTransition(0, 0);
                                    startActivity(bene);
//                                    finish();
                                }
                            }
                        } else {
                            if (userType.equals("NON_SAMBA")) {
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            } else {
                                Intent bene = new Intent(ManageBillerScreen.this, ManageBeneficiaresScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(bene);
//                                    finish();
                            }
//                                finish();
                        }

                        break;


                    case 5:

                        if (userType.equals("NON_SAMBA"))
                            showDilogForError("This feature is only available for Samba Users.", "WARNING");
                        else {
                            Intent intent = new Intent(ManageBillerScreen.this, MainActivity.class);
                            overridePendingTransition(0, 0);
                            startActivity(intent);
                        }

//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("friends management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//
//                                Intent friends = new Intent(ManageBillerScreen.this, ManageFriends.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(friends);
////                                    finish();
//                            }
//                        } else {
//                            Intent friends = new Intent(ManageBillerScreen.this, ManageFriends.class);
//                            overridePendingTransition(0, 0);
//                            startActivity(friends);
////                                finish();
//                        }

                        break;


                    case 6:

                        try {

                            CryptLib _crypt = new CryptLib();

                            String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                            if (userType.equals("NON_SAMBA"))
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            else {
                                Intent intent = new Intent(ManageBillerScreen.this, EatMubarakMainScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        break;


//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("cards management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//                                if (userType.equals("NON_SAMBA")) {
//                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                                } else {
//                                    Intent cards = new Intent(ManageBillerScreen.this, EatMubarakMainScreen.class);
//                                    overridePendingTransition(0, 0);
//                                    startActivity(cards);
////                                    finish();
//                                }
//
////                                    finish();
//                            }
//                        } else {
//                            if (userType.equals("NON_SAMBA")) {
//                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                            } else {
//                                Intent cards = new Intent(ManageBillerScreen.this, EatMubarakMainScreen.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(cards);
////                                    finish();
//                            }
////                                finish();
//                        }


                    case 7:
                        if (userType.equals("NON_SAMBA")) {
                            showDilogForError("This feature is only available for Samba Users.", "WARNING");
                        } else {
                            Intent atm = new Intent(ManageBillerScreen.this, BookMeMainScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(atm);
//                        finish();
                        }
                        break;

                    case 8:

                        pd.show();
                        showDialog("Are you sure you want to Logout?");
                        break;


                    default: {

                        break;
                    }
                }
            }

            @Override
            public void onBackgroundClick() {
//                textViewForAnimation.setText("Click background!!!");
            }

            @Override
            public void onBoomWillHide() {
                Log.d("BMB", "onBoomWillHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will RE-BOOM!!!");
            }

            @Override
            public void onBoomDidHide() {
                Log.d("BMB", "onBoomDidHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did RE-BOOM!!!");
            }

            @Override
            public void onBoomWillShow() {
                Log.d("BMB", "onBoomWillShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will BOOM!!!");
            }

            @Override
            public void onBoomDidShow() {
                Log.d("BMB", "onBoomDidShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did BOOM!!!");
            }
        });

//        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++)
//            bmb.addBuilder(BuilderManager.getSimpleCircleButtonBuilder());

        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder()
                    .listener(new OnBMClickListener() {
                        @Override
                        public void onBoomButtonClick(int index) {
                            // When the boom-button corresponding this builder is clicked.
//                            Toast.makeText(HomeScreen.this, "Clicked " + index, Toast.LENGTH_SHORT).show();
                        }
                    });

//            int margin = getResources().getDimensionPixelSize(R.dimen._4sdp);
            builder.textSize(12);

            switch (i) {

                case 0:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Home");
                    builder.unableText("Home");
                    builder.normalText("Home");
                    break;

                case 1:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Accounts");
                    builder.unableText("Accounts");
                    builder.normalText("Accounts");
                    break;

                case 2:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Transfers");
                    builder.unableText("Transfers");
                    builder.normalText("Transfers");
                    break;


                case 3:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Payments");
                    builder.unableText("Payments");
                    builder.normalText("Payments");
                    break;


                case 4:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Beneficiaries");
                    builder.unableText("Beneficiaries");
                    builder.normalText("Beneficiaries");
                    break;


                case 5:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("QR Payments");
                    builder.unableText("QR Payments");
                    builder.normalText("QR Payments");


//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Friends");
//                    builder.unableText("Friends");
//                    builder.normalText("Friends");
                    break;


                case 6:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Food");
                    builder.unableText("Food");
                    builder.normalText("Food");


//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Cards");
//                    builder.unableText("Cards");
//                    builder.normalText("Cards");
                    break;


                case 7:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("E-Tickets");
                    builder.unableText("E-Tickets");
                    builder.normalText("E-Tickets");
                    break;

                case 8:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Logout");
                    builder.unableText("Logout");
                    builder.normalText("Logout");
                    break;


                default: {

                    break;
                }

            }
//            builder.normalImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.unableImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.normalColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedColor(getResources().getColor(R.color.notiheader));
//            builder.unableColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedText("Cards");
//            builder.unableText("Cards");
//            builder.normalText("Cards");
//            builder.imagePadding(new Rect(40, 40, 40, 40));
            bmb.addBuilder(builder);
        }

    }

    public void GetbillerList() {


        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(ManageBillerScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getBillerListURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_BillerListResult").getString("Status_Code").equals("00")) {

                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_BillerListResult").getString("Local_BillerList"));

                                                if (jsonArray.length() > 0) {

                                                    for (int i = 0; i < jsonArray.length(); i++) {

                                                        try {

                                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                            Biller biller = new Biller();
                                                            biller.setAlias(jsonObj.getString("Biller_Alias"));
                                                            biller.setAmountAfterDueDate(jsonObj.getString("Biller_AmountAfterDueDate"));
                                                            biller.setAmountBeforeDueDate(jsonObj.getString("Biller_AmountBeforeDueDate"));
                                                            biller.setBillStatus(jsonObj.getString("Biller_BillStatus"));
                                                            if (jsonObj.has("Biller_BillStatus")) {
                                                                String status = jsonObj.getString("Biller_BillStatus");
                                                                status = status.toLowerCase().replaceAll("-", "").replaceAll(" ", "");

                                                                if (status.equals("partialpayment") || status.equals("unpaid")) {

                                                                    biller.setStatus("UNPAID");
                                                                    biller.setBackground(R.drawable.item_unpaid);
                                                                } else if (status.equals("paid")) {

                                                                    biller.setStatus(status.toUpperCase());
                                                                    biller.setBackground(R.drawable.item_paid);
                                                                } else {

                                                                    biller.setStatus(status.toUpperCase());
                                                                    biller.setBackground(R.drawable.item_blocked);
                                                                }
                                                            }
                                                            biller.setBillingMonth(jsonObj.getString("Biller_BillingMonth"));
                                                            biller.setAccountNumber(jsonObj.getString("Biller_Settlement_Accountno"));
                                                            biller.setCategoryName(jsonObj.getString("Biller_CategoryName"));
                                                            biller.setComments(jsonObj.getString("Biller_Comments"));
                                                            biller.setCompanyID(jsonObj.getString("Biller_CompanyID"));
                                                            biller.setCompanyName(jsonObj.getString("Biller_CompanyName"));
                                                            biller.setConsumerName(jsonObj.getString("Biller_ConsumerName"));
                                                            biller.setConsumerNumber(jsonObj.getString("Biller_ConsumerNumber"));
                                                            biller.setDueDate(jsonObj.getString("Biller_DueDate"));
                                                            biller.setBillerID(jsonObj.getString("Biller_ID"));
                                                            biller.setCategoryType(jsonObj.getString("Biller_TypeName"));
                                                            biller.setUserName(jsonObj.getString("Signs_Username"));
                                                            biller.setIsFavourite(jsonObj.getString("Biller_Favorite"));

                                                            String firstName = jsonObj.getString("Biller_CompanyName");
                                                            String s = firstName.substring(0, 1);
                                                            switch (s.toUpperCase()) {

                                                                case "A":
                                                                    biller.setBillerColor("#379022");
                                                                    break;

                                                                case "B":
                                                                    biller.setBillerColor("#379022");
                                                                    break;

                                                                case "C":
                                                                    biller.setBillerColor("#379022");
                                                                    break;

                                                                case "D":
                                                                    biller.setBillerColor("#379022");
                                                                    break;

                                                                case "E":
                                                                    biller.setBillerColor("#379022");
                                                                    break;

                                                                case "F":
                                                                    biller.setBillerColor("#379022");
                                                                    break;

                                                                case "G":
                                                                    biller.setBillerColor("#379022");
                                                                    break;

                                                                case "H":
                                                                    biller.setBillerColor("#0099cb");
                                                                    break;

                                                                case "I":
                                                                    biller.setBillerColor("#0099cb");
                                                                    break;

                                                                case "J":
                                                                    biller.setBillerColor("#0099cb");
                                                                    break;

                                                                case "K":
                                                                    biller.setBillerColor("#0099cb");
                                                                    break;

                                                                case "L":
                                                                    biller.setBillerColor("#0099cb");
                                                                    break;

                                                                case "M":
                                                                    biller.setBillerColor("#0099cb");
                                                                    break;

                                                                case "N":
                                                                    biller.setBillerColor("#cc0001");
                                                                    break;

                                                                case "O":
                                                                    biller.setBillerColor("#cc0001");
                                                                    break;

                                                                case "P":
                                                                    biller.setBillerColor("#cc0001");
                                                                    break;

                                                                case "Q":
                                                                    biller.setBillerColor("#cc0001");
                                                                    break;

                                                                case "R":
                                                                    biller.setBillerColor("#cc0001");
                                                                    break;

                                                                case "S":
                                                                    biller.setBillerColor("#cc0001");
                                                                    break;

                                                                case "T":
                                                                    biller.setBillerColor("#ffbb34");
                                                                    break;

                                                                case "U":
                                                                    biller.setBillerColor("#ffbb34");
                                                                    break;

                                                                case "V":
                                                                    biller.setBillerColor("#ffbb34");
                                                                    break;

                                                                case "W":
                                                                    biller.setBillerColor("#ffbb34");
                                                                    break;

                                                                case "X":
                                                                    biller.setBillerColor("#ffbb34");
                                                                    break;

                                                                case "Y":
                                                                    biller.setBillerColor("#ffbb34");
                                                                    break;

                                                                case "Z":
                                                                    biller.setBillerColor("#ffbb34");
                                                                    break;

                                                                default:
                                                                    biller.setBillerColor("#9a34c6");
                                                                    break;
                                                            }
                                                            biller.setFirstName(s.toUpperCase());

                                                            billerList.add(biller);

                                                            if (jsonObj.getString("Biller_Favorite").equals("1")) {


                                                                favouritesBillers.add(biller);
                                                            }


                                                        } catch (Exception ex) {

                                                            ex.printStackTrace();
                                                            pd.dismiss();
                                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        }

                                                    }

                                                    customHorizontalListAdapter = new CustomHorizontalListAdapter(favouritesBillers, ManageBillerScreen.this, type);
                                                    horizontalListView.setAdapter(customHorizontalListAdapter);

                                                    customListAdapter = new CustomListManageBillerAdapter(billerList, ManageBillerScreen.this, type);
                                                    listView.setAdapter(customListAdapter);

                                                    customListAdapter.setMode(Attributes.Mode.Multiple);
                                                    listView.setAdapter(customListAdapter);
                                                    listView.setSelected(false);
                                                    pd.dismiss();


                                                } else {

                                                    pd.dismiss();
                                                }


                                            } else if (jsonObject.getJSONObject("Signs_BillerListResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillerListResult").getString("Status_Description"), "ERROR", ManageBillerScreen.this, new LoginScreen());


                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_BillerListResult").getString("Status_Description"), "ERROR");
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }


    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (!TextUtils.isEmpty(backType) && backType.equals("addBiller")) {

            Intent intent = new Intent(ManageBillerScreen.this, HomeScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);
            finish();

        } else
            finish();
    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(ManageBillerScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        btn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


    private void showDialog(String message) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Logout:");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

//                constants.editor.clear();
//                constants.editor.commit();
//                constants.editor.apply();
//                constants.nonSambaCredentialsEditor.clear();
//                constants.nonSambaCredentialsEditor.commit();
//                constants.nonSambaCredentialsEditor.apply();
//                constants.destinationAccountEditor.clear().commit();
//                constants.destinationAccountEditor.apply();
//                constants.sourceAccountEditor.clear().commit();
//                constants.sourceAccountEditor.apply();
//                constants.accountListEditor.clear().commit();
//                constants.accountListEditor.apply();
//                constants.accountsEditor.clear().commit();
//                constants.accountsEditor.apply();
//                constants.forgotPasswordEditor.clear().commit();
//                constants.forgotPasswordEditor.apply();
//                Handler handler1 = new Handler();
//                handler1.postDelayed(new Runnable() {
//                    public void run() {
//
//                        pd.dismiss();
//                        Intent intent = new Intent(ManageBillerScreen.this, LoginScreen.class);
//                        startActivity(intent);
//                        overridePendingTransition(0, 0);
//                        finish();
//                        finishAffinity();
//
//                    }
//                }, 1000);
                Logout();
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                pd.dismiss();
            }
        });

        builder.show();
    }

    public void Logout() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSessionID", Constants.Shared_KEY);
                    HttpsTrustManager.allowMySSL(ManageBillerScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.logoutURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Code").equals("00")) {
                                                constants.editor.clear();
                                                constants.editor.commit();
                                                constants.editor.apply();
                                                constants.nonSambaCredentialsEditor.clear();
                                                constants.nonSambaCredentialsEditor.commit();
                                                constants.nonSambaCredentialsEditor.apply();
                                                constants.destinationAccountEditor.clear().commit();
                                                constants.destinationAccountEditor.apply();
                                                constants.sourceAccountEditor.clear().commit();
                                                constants.sourceAccountEditor.apply();
                                                constants.accountListEditor.clear().commit();
                                                constants.accountListEditor.apply();
                                                constants.accountsEditor.clear().commit();
                                                constants.accountsEditor.apply();
                                                constants.forgotPasswordEditor.clear().commit();
                                                constants.forgotPasswordEditor.apply();
                                                constants.purposeBillList.clear().commit();
                                                constants.purposeBillList.apply();
                                                constants.purposePrepaidList.clear().commit();
                                                constants.purposePrepaidList.apply();
                                                constants.purposeListEditor.clear().commit();
                                                constants.purposeListEditor.apply();


                                                pd.dismiss();
                                                Intent intent = new Intent(ManageBillerScreen.this, LoginScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                finish();
                                                finishAffinity();


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Description"), "ERROR", ManageBillerScreen.this, new LoginScreen());

                                            }

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sSessionID", Constants.Shared_KEY);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity", "WARNING");


            }
        } catch (Exception e) {

            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity", "WARNING");


        }
    }

}