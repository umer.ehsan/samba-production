package com.ceesolutions.samba.billPayments.Model;

import proguard.annotation.Keep;

/**
 * Created by ceeayaz on 4/9/18.
 */
@Keep
public class Category {

    private String name;
    private String id;
    private String type;
    private String billingType;
    private String categoryCompanyType;

    public String getCategoryCompanyType() {
        return categoryCompanyType;
    }

    public void setCategoryCompanyType(String categoryCompanyType) {
        this.categoryCompanyType = categoryCompanyType;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
