package com.ceesolutions.samba.billPayments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.billPayments.Adapter.CustomListBillerAdapter;
import com.ceesolutions.samba.billPayments.Model.Distributor;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/11/18.
 */

public class SelectPrepaidScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private ListView listView;
    private CustomListBillerAdapter customListAdapter;
    private ArrayList<Distributor> billerDistributorList;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private ImageView backBtn;
    private String type;
    private TextView headingTxt;
    private Intent intent;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_bank);
        initViews();
        getSupportActionBar().hide();

        headingTxt.setText("Select Prepaid Voucher");

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent(SelectPrepaidScreen.this, SelectCategoryScreen.class);
//                overridePendingTransition(0, 0);
//                startActivity(intent);
                finish();
            }
        });

    }

    public void initViews() {

        intent = getIntent();

        type = intent.getStringExtra("source");

        if (!TextUtils.isEmpty(type) && type.equals("destination")) {


        } else
            type = "nobill";

        listView = (ListView) findViewById(R.id.listView);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        headingTxt = (TextView) findViewById(R.id.headingTxt);
        billerDistributorList = new ArrayList<>();
        utils = new Utils(SelectPrepaidScreen.this);
        requestQueue = Volley.newRequestQueue(SelectPrepaidScreen.this);
        helper = Helper.getHelper(SelectPrepaidScreen.this);
        constants = Constants.getConstants(SelectPrepaidScreen.this);
        pd = new Dialog(SelectPrepaidScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        GetbillerDistributorList();
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

    }

    public void GetbillerDistributorList() {


        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(SelectPrepaidScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.prepaidListURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PrePaidVoucherListResult").getString("Status_Code").equals("00")) {

                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_PrePaidVoucherListResult").getString("Local_BillDistributorList"));

                                                if (jsonArray.length() > 0) {

                                                    for (int i = 0; i < jsonArray.length(); i++) {

                                                        try {

                                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
                                                            Distributor distributor = new Distributor();
                                                            distributor.setAllowedMaxAmount(jsonObj.getString("Distributor_AllowedMaxAmount"));
                                                            distributor.setAllowedMinAmount(jsonObj.getString("Distributor_AllowedMinAmount"));
                                                            distributor.setChargeCode(jsonObj.getString("Distributor_ChargeCode"));
                                                            distributor.setConsumerNoLayout(jsonObj.getString("Distributor_ConsumerNoLayout"));
                                                            distributor.setConsumerNoLength(jsonObj.getString("Distributor_ConsumerNoLength"));
                                                            distributor.setDeductCharges(jsonObj.getString("Distributor_DeductCharges"));
                                                            distributor.setAccountNumber(jsonObj.getString("Distributor_DistributorAccountNumber"));
                                                            distributor.setDistributorID(jsonObj.getString("Distributor_ID"));
                                                            distributor.setDistributorName(jsonObj.getString("Distributor_Name"));
                                                            distributor.setTransactionCode(jsonObj.getString("Distributor_TransactionCode"));
                                                            distributor.setCategoryID(intent.getStringExtra("categoryID"));
                                                            distributor.setCategoryName(intent.getStringExtra("categoryName"));
                                                            String firstName = jsonObj.getString("Distributor_Name");
                                                            String s = firstName.substring(0, 1);
                                                            distributor.setFirstLetter(s.toUpperCase());
                                                            switch (s.toUpperCase()) {

                                                                case "A":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor1));
                                                                    break;

                                                                case "B":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor2));
                                                                    break;

                                                                case "C":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor3));
                                                                    break;

                                                                case "D":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor4));
                                                                    break;

                                                                case "E":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor1));
                                                                    break;

                                                                case "F":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor2));
                                                                    break;

                                                                case "G":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor3));
                                                                    break;

                                                                case "H":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor4));
                                                                    break;

                                                                case "I":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor1));
                                                                    break;

                                                                case "J":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor2));
                                                                    break;

                                                                case "K":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor3));
                                                                    break;

                                                                case "L":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor4));
                                                                    break;

                                                                case "M":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor1));
                                                                    break;

                                                                case "N":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor2));
                                                                    break;

                                                                case "O":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor3));
                                                                    break;

                                                                case "P":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor4));
                                                                    break;

                                                                case "Q":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor1));
                                                                    break;

                                                                case "R":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor2));
                                                                    break;

                                                                case "S":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor3));
                                                                    break;

                                                                case "T":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor4));
                                                                    break;

                                                                case "U":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor1));
                                                                    break;

                                                                case "V":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor2));
                                                                    break;

                                                                case "W":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor3));
                                                                    break;

                                                                case "X":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor4));
                                                                    break;

                                                                case "Y":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor1));
                                                                    break;

                                                                case "Z":
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.friendColor2));
                                                                    break;

                                                                default:
                                                                    distributor.setDistributorColor(getResources().getColor(R.color.defaultColor));
                                                                    break;
                                                            }
                                                            billerDistributorList.add(distributor);

                                                        } catch (Exception ex) {

                                                            ex.printStackTrace();
                                                            pd.dismiss();
                                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        }

                                                    }

                                                    customListAdapter = new CustomListBillerAdapter(billerDistributorList, SelectPrepaidScreen.this, type);
                                                    listView.setAdapter(customListAdapter);
                                                    pd.dismiss();
                                                } else {

                                                    pd.dismiss();

                                                }


                                            } else if (jsonObject.getJSONObject("Signs_BillDistributorResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillDistributorResult").getString("Status_Description"), "ERROR", SelectPrepaidScreen.this, new LoginScreen());


                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_BillDistributorResult").getString("Status_Description"), "ERROR");
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }


    }

}