package com.ceesolutions.samba.billPayments.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.billPayments.AddBillerScreen;
import com.ceesolutions.samba.billPayments.EditBillerDetailsScreen;
import com.ceesolutions.samba.billPayments.ManageBillerScreen;
import com.ceesolutions.samba.billPayments.Model.Biller;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/10/18.
 */

public class CustomListManageBillerAdapter extends BaseSwipeAdapter {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private ArrayList<Biller> billerList;
    private LayoutInflater layoutInflater;
    private View row;
    private Context context;
    private TextView categoryName, aliasTxtInput, payment, heading, doneBtn, textView;
    private ImageView title, edit, trash, fav, click, arrow;
    private int[] androidColors;
    private String type;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Dialog pd, dialog;
    private Utils utils;
    private boolean isPaid = false;
    private AppPreferences appPreferences;
    private String location;

    public CustomListManageBillerAdapter(ArrayList<Biller> billerArrayList, Context mContext, String isType) {
        billerList = billerArrayList;
        context = mContext;
        type = isType;
        appPreferences = new AppPreferences(mContext);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return billerList.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View generateView(final int position, ViewGroup parent) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.category_item_list, null);


        requestQueue = Volley.newRequestQueue(context);
        constants = Constants.getConstants(context);
        helper = Helper.getHelper(context);
        utils = new Utils(context);
        pd = new Dialog(context);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        return convertView;
    }


    @Override
    public void fillValues(final int position, View convertView) {

        edit = (ImageView) convertView.findViewById(R.id.edit);
        arrow = (ImageView) convertView.findViewById(R.id.image);
        trash = (ImageView) convertView.findViewById(R.id.trash);
        fav = (ImageView) convertView.findViewById(R.id.fav);
        title = (ImageView) convertView.findViewById(R.id.title);
        categoryName = (TextView) convertView.findViewById(R.id.categoryName);
        aliasTxtInput = (TextView) convertView.findViewById(R.id.aliasTxtInput);
        payment = (TextView) convertView.findViewById(R.id.payment);
        click = (ImageView) convertView.findViewById(R.id.click);
//        if (billerList.get(position).getBillStatus().equals("Un-Paid") || billerList.get(position).getBillStatus().equals("UnPaid") || billerList.get(position).getBillStatus().equals("Partial Payment")) {
//
//            payment.setBackground(ContextCompat.getDrawable(context, R.drawable.item_unpaid));
//            payment.setText("UNPAID");
//
//        } else if (billerList.get(position).getBillStatus().equals("Paid")) {
//
//            payment.setBackground(ContextCompat.getDrawable(context, R.drawable.item_paid));
//            payment.setText(billerList.get(position).getBillStatus().toUpperCase());
//
//        } else if (billerList.get(position).getBillStatus().equals("Blocked")) {
//
//            payment.setBackground(ContextCompat.getDrawable(context, R.drawable.item_blocked));
//            payment.setText(billerList.get(position).getBillStatus().toUpperCase());
//        }

        payment.setBackground(ContextCompat.getDrawable(context, billerList.get(position).getBackground()));
        payment.setText(billerList.get(position).getStatus().toUpperCase());

        if (billerList.get(position).getIsFavourite().equals("1")) {

            fav.setImageResource(R.drawable.fav_);
        } else {

            fav.setImageResource(R.drawable.fav);
        }

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(((Activity) context), AddBillerScreen.class);
                intent.putExtra("alias", billerList.get(position).getAlias());
                intent.putExtra("edit", "edit");
                intent.putExtra("consumerNumber", billerList.get(position).getConsumerNumber());
                intent.putExtra("consumerName", billerList.get(position).getConsumerName());
                intent.putExtra("billerID", billerList.get(position).getBillerID());
                intent.putExtra("companyName", billerList.get(position).getCompanyName());
                intent.putExtra("companyID", billerList.get(position).getCompanyID());
                intent.putExtra("accountNumber", billerList.get(position).getAccountNumber());
                intent.putExtra("categoryType", billerList.get(position).getCategoryType());
                intent.putExtra("isFav", billerList.get(position).getIsFavourite());
                ((Activity) context).overridePendingTransition(0, 0);
                context.startActivity(intent);
                ((Activity) context).finish();

            }
        });
        trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDilogForError("Are you sure you want to delete the biller?", "CONFIRMATION", "Delete", position, "");
            }
        });
        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (billerList.get(position).getIsFavourite().equals("1")) {
                    showDilogForError("Are you sure you want to remove the biller from favourite?", "CONFIRMATION", "Fav", position, "0");
                } else {
                    showDilogForError("Are you sure you want to mark the biller as favourite?", "CONFIRMATION", "Fav", position, "1");
                }
            }
        });


        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click.setEnabled(false);
//                finalConvertView.setEnabled(false);

//                if (billerList.get(position).getBillStatus().equals("Un-Paid") || billerList.get(position).getBillStatus().equals("UnPaid") || billerList.get(position).getBillStatus().equals("Partial Payment")) {
//
                if (!TextUtils.isEmpty(type) && type.equals("destination")) {

                    BillInquiry(position);
                    click.setEnabled(true);

                } else {

                    FirebaseAnalytics.getInstance(context).logEvent("Bill_Selection_Pressed", new Bundle());

                    Intent intent = new Intent(((Activity) context), EditBillerDetailsScreen.class);
                    intent.putExtra("alias", billerList.get(position).getAlias());
                    intent.putExtra("consumerNumber", billerList.get(position).getConsumerNumber());
                    intent.putExtra("consumerName", billerList.get(position).getConsumerName());
                    intent.putExtra("billerID", billerList.get(position).getBillerID());
                    intent.putExtra("companyName", billerList.get(position).getCompanyName());
                    intent.putExtra("companyID", billerList.get(position).getCompanyID());
                    intent.putExtra("accountNumber", billerList.get(position).getAccountNumber());
                    intent.putExtra("categoryType", billerList.get(position).getCategoryType());
                    intent.putExtra("isFav", billerList.get(position).getIsFavourite());
                    ((Activity) context).overridePendingTransition(0, 0);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                    click.setEnabled(true);
//                ((Activity) context).finish();

//
                }
//
//                } else if (billerList.get(position).getBillStatus().equals("Paid")) {
//
//                    // Do Nothing
//                    utils.showDilogForError("Bill status is currently Paid.", "WARNING");
//                } else if (billerList.get(position).getBillStatus().equals("Blocked")) {
//
//                    utils.showDilogForError("Bill status is currently Blocked.", "WARNING");
//
//                    // Do Nothing
//                }


            }
        });

        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrow.setEnabled(false);

//                if (billerList.get(position).getBillStatus().equals("Un-Paid") || billerList.get(position).getBillStatus().equals("UnPaid") || billerList.get(position).getBillStatus().equals("Partial Payment")) {
//
                if (!TextUtils.isEmpty(type) && type.equals("destination")) {

                    BillInquiry(position);
                    arrow.setEnabled(true);
                } else {

                    FirebaseAnalytics.getInstance(context).logEvent("Bill_Selection_Pressed", new Bundle());

                    Intent intent = new Intent(((Activity) context), EditBillerDetailsScreen.class);
                    intent.putExtra("alias", billerList.get(position).getAlias());
                    intent.putExtra("consumerNumber", billerList.get(position).getConsumerNumber());
                    intent.putExtra("consumerName", billerList.get(position).getConsumerName());
                    intent.putExtra("billerID", billerList.get(position).getBillerID());
                    intent.putExtra("companyName", billerList.get(position).getCompanyName());
                    intent.putExtra("companyID", billerList.get(position).getCompanyID());
                    intent.putExtra("accountNumber", billerList.get(position).getAccountNumber());
                    intent.putExtra("categoryType", billerList.get(position).getCategoryType());
                    intent.putExtra("isFav", billerList.get(position).getIsFavourite());
                    ((Activity) context).overridePendingTransition(0, 0);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                    arrow.setEnabled(true);
//                ((Activity) context).finish();

//
                }
//
//                } else if (billerList.get(position).getBillStatus().equals("Paid")) {
//
//                    // Do Nothing
//                    utils.showDilogForError("Bill status is currently Paid.", "WARNING");
//                } else if (billerList.get(position).getBillStatus().equals("Blocked")) {
//
//                    utils.showDilogForError("Bill status is currently Blocked.", "WARNING");
//
//                    // Do Nothing
//                }


            }
        });

        String name = billerList.get(position).getCategoryType();

        switch (name) {

            case "Electricity": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_electric));
                categoryName.setText(billerList.get(position).getAlias());
                aliasTxtInput.setText(billerList.get(position).getCompanyName());
                break;
            }

            case "Gas": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_gas));
                categoryName.setText(billerList.get(position).getAlias());
                aliasTxtInput.setText(billerList.get(position).getCompanyName());
                break;

            }

            case "Water and Sewerage": {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_water));
                categoryName.setText(billerList.get(position).getAlias());
                aliasTxtInput.setText(billerList.get(position).getCompanyName());
                break;
            }

            case "Telecommunication": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_telecom));
                categoryName.setText(billerList.get(position).getAlias());
                aliasTxtInput.setText(billerList.get(position).getCompanyName());
                break;

            }

            case "Mobile Prepaid": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_ph));
                categoryName.setText(billerList.get(position).getAlias());
                aliasTxtInput.setText(billerList.get(position).getCompanyName());
                break;

            }

            case "Mobile Postpaid": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_ph));
                categoryName.setText(billerList.get(position).getAlias());
                aliasTxtInput.setText(billerList.get(position).getCompanyName());
                break;

            }

            case "ISP Payment": {

                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_isp));
                categoryName.setText(billerList.get(position).getAlias());
                aliasTxtInput.setText(billerList.get(position).getCompanyName());
                break;

            }

            case "Education": {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_edu));
                categoryName.setText(billerList.get(position).getAlias());
                aliasTxtInput.setText(billerList.get(position).getCompanyName());
                break;
            }

            case "Airlines": {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon));
                categoryName.setText(billerList.get(position).getAlias());
                aliasTxtInput.setText(billerList.get(position).getCompanyName());
                break;
            }

            case "TAX": {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.icon_tax));
                categoryName.setText(billerList.get(position).getAlias());
                aliasTxtInput.setText(billerList.get(position).getCompanyName());
                break;
            }

            default: {


                title.setBackground(ContextCompat.getDrawable(context, R.drawable.default1));
                categoryName.setText(billerList.get(position).getAlias());
                aliasTxtInput.setText(billerList.get(position).getCompanyName());
                break;
            }

        }
    }


    public void BillInquiry(final int position) {

        final int stanNumber = 6;

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sBillConsumerNumber", billerList.get(position).getConsumerNumber());
                    params.put("sDistributorID", billerList.get(position).getCompanyID());
                    params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(context);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.billInquiryURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            try {

                                                CryptLib _crypt = new CryptLib();
                                                if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("00")) {

                                                    constants.destinationAccountEditor.putString("isSelect", "Yes");
                                                    constants.destinationAccountEditor.putString("AmountAfterDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountAfterDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("AmountWithinDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountWithinDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillStatus", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillStatus"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillingMonth", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillingMonth"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("CompanyID", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("DueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("DueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("ConsumerName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("ConsumerName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("CompanyName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("RRN", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("RRN"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillConsumerNumber", _crypt.encryptForParams(billerList.get(position).getConsumerNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("DistributorID", _crypt.encryptForParams(billerList.get(position).getCompanyID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("distributorAccountNumber", _crypt.encryptForParams(billerList.get(position).getAccountNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("alias", _crypt.encryptForParams(billerList.get(position).getAlias(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("categoryType", _crypt.encryptForParams(billerList.get(position).getCategoryType(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.commit();
                                                    constants.destinationAccountEditor.apply();
                                                    pd.dismiss();
                                                    ((Activity) context).finish();


                                                } else if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("54")) {
                                                    pd.dismiss();
                                                    utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR", context, new LoginScreen());


                                                } else {

                                                    pd.dismiss();
                                                    utils.showDilogForError(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR");


                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sBillConsumerNumber", billerList.get(position).getConsumerNumber());
//                            params.put("sDistributorID", billerList.get(position).getCompanyID());
//                            params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    public void DeleteBiller(final int position) {


        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sBillerID", billerList.get(position).getBillerID());
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(context);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.deleteBillerURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_RemoveBillerResult").getString("Status_Code").equals("00")) {

                                                pd.dismiss();
                                                Intent intent = new Intent(context, ManageBillerScreen.class);
                                                ((Activity) context).overridePendingTransition(0, 0);
                                                ((Activity) context).startActivity(intent);
                                                ((Activity) context).finish();
//                                            Intent intent = new Intent(EditBillerDetailsScreen.this, ManageBillerScreen.class);
//                                            overridePendingTransition(0, 0);
//                                            startActivity(intent);
//                                            finish();

                                            } else if (jsonObject.getJSONObject("Signs_RemoveBillerResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_RemoveBillerResult").getString("Status_Description"), "ERROR", context, new LoginScreen());


                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_RemoveBillerResult").getString("Status_Description"), "ERROR");
                                            }

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sBillerID", billerList.get(position).getBillerID());
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }


    }

    public void AddBillerToFav(final int position, final String sAction) {


        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sSigns_BillerFavoriteFlag", sAction);
                    params.put("sBillerID", billerList.get(position).getBillerID());
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(context);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.favBillerURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_BillerFavorite_Mark_UnMarkResult").getString("Status_Code").equals("00")) {

                                                pd.dismiss();
                                                Intent intent = new Intent(context, ManageBillerScreen.class);
                                                ((Activity) context).overridePendingTransition(0, 0);
                                                ((Activity) context).startActivity(intent);
                                                ((Activity) context).finish();
//                                            utils.showDilogForError("Biller has successfully marked as favourite","SUCCESS");
//                                            Intent intent = new Intent(EditBillerDetailsScreen.this, ManageBillerScreen.class);
//                                            overridePendingTransition(0, 0);
//                                            startActivity(intent);
//                                            finish();

                                            } else if (jsonObject.getJSONObject("Signs_BillerFavorite_Mark_UnMarkResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillerFavorite_Mark_UnMarkResult").getString("Status_Description"), "ERROR", context, new LoginScreen());


                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_BillerFavorite_Mark_UnMarkResult").getString("Status_Description"), "ERROR");
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sSigns_BillerFavoriteFlag", sAction);
//                            params.put("sBillerID", billerList.get(position).getBillerID());
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }


    }

    public void showDilogForError(String msg, String header, final String type, final int position, final String sAction) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (type.equals("Delete")) {
                    dialog.dismiss();
                    DeleteBiller(position);
                } else {
                    dialog.dismiss();
                    AddBillerToFav(position, sAction);
                }

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

}