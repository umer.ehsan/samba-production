package com.ceesolutions.samba.billPayments.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.billPayments.BillReviewScreen;
import com.ceesolutions.samba.billPayments.PayBillsScreen;
import com.ceesolutions.samba.transactions.SelectAccountScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class BillPaymentFragment extends Fragment {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private ExpandableLayout expandableLayout0;
    private CardView cardView;
    private Constants constants;
    private TextInputLayout amountTxtInput, remarksTxtInput;
    private Dialog pd;
    private String remarks, amount, type;
    private EditText editAmount, editRemarks, payabaleAmount;
    private RelativeLayout upperPart1, upperPart2;
    private TextView consumerIDTxt, consumerNumberTxt, billingTxt, billingMonthTxt, dueDateTxt, payableBeforeTxt, payableAfterTxt, statusTxt, mainTxt, errorMessageForAmount, errorMessageForRemarks, errorMessageForSpinner, userNameTxt, accMaskTxt, amountTxt, friendNameTxt, itemName, validateBtn;
    private Spinner purposesSpinner, payableSpinner;
    private String purposeSelected, payableSelected;
    private String[] purpose;
    private ImageView arrowDestination;
    private ArrayList<String> purpStringArrayList, purposeArrayList;
    private HashMap<String, String> billerMap;
    private String sourceBalance, sourceuserName, sourceAccount, sourceCurrency, distributorID, distributorAccountNumber, mobileNumber, distributorName, length;
    private Utils utils;
    private boolean isSource = false;
    private boolean isDestination = false;
    private boolean isFirst = false;
    private String identityValue, alias, stan, rrn;
    private String beneType = "SAMBA";
    private TextView doneBtn, textView, heading;
    private Dialog dialog;
    private String minAmount, categoryType, billConsumerNumber, sourceminAmount, amountAfterDue, amountBeforeDue, billStatus, billingMonth, dueDate, consumerName, companyName;
    private String maxAmount, branchCode, mobile, email, messageId, targetId, friendNumber;
    private float amountToInt, maxAmountToInt, minAmountToInt;
    private int bal;
    private boolean flag = false;
    private Intent intent1;
    private String isSelectSource, isSelectDestination;
    private HashMap<String, String> billerDistributorList;
    private boolean isVisible = false;
    private AppPreferences appPreferences;
    private String location;
    private boolean isPostpaid = false;
    private String[] oneBillTypes = new String[]{"Credit Card Bill Payment", "Invoice/Voucher Payment", "Top Up"};
    private String[] oneBillCodes = new String[]{"CC01BILL", "IN01BILL", "TP01BILL"};

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.bill1_payment_fragment, container, false);

        initViews(convertView);
        // new log
        FirebaseAnalytics.getInstance(getContext()).logEvent("OneBill_Payment_Pressed", new Bundle());


        editRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                remarks = editable.toString();
            }
        });


        editAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForAmount.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForAmount.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                amount = editable.toString();

            }
        });


        upperPart1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                upperPart1.setEnabled(false);
                isSource = true;
                isDestination = false;
                isFirst = true;
                Intent intent = new Intent(getActivity(), SelectAccountScreen.class);
                intent.putExtra("source", "source");
                startActivity(intent);


            }
        });

        upperPart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                upperPart2.setEnabled(false);
//                Intent intent = new Intent(getActivity(), ManageBillerScreen.class);
//                intent.putExtra("source", "destination");
//                startActivity(intent);
//                isDestination = true;
//                isSource = false;
//                isFirst = true;


            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAnalytics.getInstance(getContext()).logEvent("OneBill_Payment_Review_Pressed", new Bundle());
                Verification();

            }
        });

        return convertView;
    }

    public void initViews(View convertView) {

        try {

            CryptLib _crypt = new CryptLib();
            billerMap = new HashMap<>();
            purposeArrayList = new ArrayList<>();
            purposesSpinner = (Spinner) convertView.findViewById(R.id.purposeSpinner);
            payableSpinner = (Spinner) convertView.findViewById(R.id.payableSpinner);
            amountTxtInput = (TextInputLayout) convertView.findViewById(R.id.amountTxtInput);
            remarksTxtInput = (TextInputLayout) convertView.findViewById(R.id.remarksTxtInput);
            validateBtn = (TextView) convertView.findViewById(R.id.validateBtn);
            arrowDestination = (ImageView) convertView.findViewById(R.id.arrowDestination);
            consumerIDTxt = (TextView) convertView.findViewById(R.id.consumerIDTxt);
            dueDateTxt = (TextView) convertView.findViewById(R.id.dueDateTxt);
            billingMonthTxt = (TextView) convertView.findViewById(R.id.billingMonthTxt);
            consumerNumberTxt = (TextView) convertView.findViewById(R.id.consumerNumberTxt);
            billingTxt = (TextView) convertView.findViewById(R.id.billingTxt);
            payableAfterTxt = (TextView) convertView.findViewById(R.id.payableAfterTxt);
            payableBeforeTxt = (TextView) convertView.findViewById(R.id.payableBeforeTxt);
            statusTxt = (TextView) convertView.findViewById(R.id.statusTxt);
            upperPart1 = (RelativeLayout) convertView.findViewById(R.id.upperPart1);
            upperPart2 = (RelativeLayout) convertView.findViewById(R.id.upperPart2);
            friendNameTxt = (TextView) convertView.findViewById(R.id.friendNameTxt);
            payabaleAmount = (EditText) convertView.findViewById(R.id.payabaleAmount);
            cardView = (CardView) convertView.findViewById(R.id.card_view);
            userNameTxt = (TextView) convertView.findViewById(R.id.userNameTxt);
            userNameTxt.setText("Select Source Account");
            accMaskTxt = (TextView) convertView.findViewById(R.id.accMaskTxt);
            accMaskTxt.setVisibility(View.INVISIBLE);
            amountTxt = (TextView) convertView.findViewById(R.id.amountTxt);
            amountTxt.setVisibility(View.INVISIBLE);
            itemName = (TextView) convertView.findViewById(R.id.itemName);
            itemName.setText("Select Biller");
            errorMessageForAmount = (TextView) convertView.findViewById(R.id.errorMessageForAmount);
            errorMessageForRemarks = (TextView) convertView.findViewById(R.id.errorMessageForRemarks);
            errorMessageForSpinner = (TextView) convertView.findViewById(R.id.errorMessageForSpinner);
            editRemarks = (EditText) convertView.findViewById(R.id.editRemarks);
            editAmount = (EditText) convertView.findViewById(R.id.editAmount);
            expandableLayout0 = (ExpandableLayout) convertView.findViewById(R.id.expandable_layout_0);
            requestQueue = Volley.newRequestQueue(getActivity());
            mainTxt = (TextView) convertView.findViewById(R.id.mainTxt);
            billerDistributorList = new HashMap<>();
            helper = Helper.getHelper(getActivity());
            constants = Constants.getConstants(getActivity());
            pd = new Dialog(getActivity());
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            appPreferences = new AppPreferences(getActivity());
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            String billerList = constants.billerList.getString("billerListCategoryFor1Bill", "N/A");
            if (billerList.equals("N/A")) {
                GetbillerDistributorList();
            } else {

                Gson gson = new Gson();
                Type type1 = new TypeToken<HashMap<String, String>>() {
                }.getType();

                billerMap = gson.fromJson(billerList, type1);
                Iterator it = billerMap.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    purposeArrayList.add(pair.getKey().toString());
                }


                purpose = new String[purposeArrayList.size()];
                purpose = purposeArrayList.toArray(purpose);


                spinnerAdapter payableAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_1bill);
                payableAdapter.addAll(purpose);
                payableAdapter.add("Select Option");
                payableSpinner.setAdapter(payableAdapter);
                payableSpinner.setSelection(0);
                payableSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


                            if (((TextView) parent.getChildAt(0)) != null) {
                                ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#ffffff"));
                                ((TextView) parent.getChildAt(0)).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.item_white));

                            }
                            // Get select item
                            if (payableSpinner.getSelectedItem() == "Select Option") {
                                payableSelected = payableSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + payableSelected);

                            } else {
                                payableSelected = payableSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + payableSelected);
                                switch (payableSelected) {

                                    case "Credit Card Bill Payment":
                                        amountTxtInput.setHint("Credit Card Number");
                                        editAmount.setText("");
                                        remarksTxtInput.setHint("Amount");
                                        remarksTxtInput.setVisibility(View.VISIBLE);
                                        editRemarks.setText("");
                                        errorMessageForAmount.setVisibility(View.GONE);
                                        errorMessageForSpinner.setVisibility(View.GONE);
                                        break;

                                    case "Invoice/ Voucher Payment":
                                        amountTxtInput.setHint("Invoice/Voucher Number");
                                        editAmount.setText("");
                                        remarksTxtInput.setVisibility(View.GONE);
                                        errorMessageForAmount.setVisibility(View.GONE);
                                        errorMessageForSpinner.setVisibility(View.GONE);
                                        break;

                                    case "Top Up":
                                        amountTxtInput.setHint("Mobile Number");
                                        editAmount.setText("");
                                        remarksTxtInput.setHint("Amount");
                                        editRemarks.setText("");
                                        remarksTxtInput.setVisibility(View.VISIBLE);
                                        errorMessageForAmount.setVisibility(View.GONE);
                                        errorMessageForSpinner.setVisibility(View.GONE);
                                        break;


                                }


                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });
            }


            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }

            purpStringArrayList = new ArrayList<>();
            utils = new Utils(getActivity());
            payabaleAmount.setText("");
            Calendar today = Calendar.getInstance();
            today.set(Calendar.MILLISECOND, 0);
            today.set(Calendar.SECOND, 0);
            today.set(Calendar.MINUTE, 0);
            today.set(Calendar.HOUR_OF_DAY, 0);


            String menus = constants.sharedPreferences.getString("Menus", "N/A");

            if (!TextUtils.isEmpty(menus) && !menus.equals("N/A") && !menus.equals("")) {

                if (menus.toLowerCase().contains("pay bill")) {
                    showDilogForErrorForMenu("This service is currently disabled. Please try again later.", "WARNING");
                }
            }

            String list = constants.purposeBill.getString("purposeList", "N/A");

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!flag) {
                        expandableLayout0.expand();
                        mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_less, 0);
                        flag = true;
                    } else {
                        expandableLayout0.collapse();
                        mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_more, 0);
                        flag = false;
                    }

                }
            });


            int stanNumber = 6;
            stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
            int rrnNumber = 12;
            rrn = String.valueOf(helper.generateRandom(rrnNumber));

            Intent intent = getActivity().getIntent();

            intent1 = intent;

            String paybill = intent.getStringExtra("paybill");

            if (!TextUtils.isEmpty(paybill) && paybill.equals("paybill")) {

                userNameTxt.setText(intent.getStringExtra("accountName"));
                accMaskTxt.setVisibility(View.VISIBLE);
                accMaskTxt.setText(utils.getMaskedString(intent.getStringExtra("accountNumber")));
                amountTxt.setVisibility(View.VISIBLE);
                amountTxt.setText(intent.getStringExtra("currency") + " " + intent.getStringExtra("balance"));
                sourceAccount = intent.getStringExtra("accountNumber");
                sourceCurrency = intent.getStringExtra("currency");
                sourceBalance = intent.getStringExtra("balance");
                sourceuserName = intent.getStringExtra("accountName");
                branchCode = intent.getStringExtra("branchCode");

            }

            type = intent.getStringExtra("requestbill");

            if (!TextUtils.isEmpty(type) && type.equals("requestbill")) {

                arrowDestination.setVisibility(View.INVISIBLE);
                upperPart2.setEnabled(false);
//            editAmount.setText(constants.destinationAccount.getString("amount", "N/A"));
//            editAmount.setFocusableInTouchMode(false);
                editRemarks.setText(_crypt.decrypt(constants.destinationAccount.getString("remarks", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                editRemarks.setFocusableInTouchMode(false);
                distributorID = _crypt.decrypt(constants.destinationAccount.getString("DistributorID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                distributorAccountNumber = _crypt.decrypt(constants.destinationAccount.getString("distributorAccountNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                amountAfterDue = _crypt.decrypt(constants.destinationAccount.getString("AmountAfterDueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                amountBeforeDue = _crypt.decrypt(constants.destinationAccount.getString("AmountWithinDueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                billStatus = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                billingMonth = _crypt.decrypt(constants.destinationAccount.getString("BillingMonth", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                dueDate = _crypt.decrypt(constants.destinationAccount.getString("DueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                consumerName = _crypt.decrypt(constants.destinationAccount.getString("ConsumerName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                billConsumerNumber = _crypt.decrypt(constants.destinationAccount.getString("BillConsumerNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                companyName = _crypt.decrypt(constants.destinationAccount.getString("CompanyName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                if (companyName.toLowerCase().contains("postpaid")) {
                    isPostpaid = true;
                }
                alias = _crypt.decrypt(constants.destinationAccount.getString("alias", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                String cat = constants.destinationAccount.getString("categoryType", "N/A");
                if (!TextUtils.isEmpty(cat) && !cat.equals("N/A") && !cat.equals("null") && !cat.equals(null)) {
                    categoryType = _crypt.decrypt(constants.destinationAccount.getString("categoryType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                } else {
                    categoryType = "N/A";
                }
                messageId = _crypt.decrypt(constants.destinationAccount.getString("messageId", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                targetId = _crypt.decrypt(constants.destinationAccount.getString("targetId", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                friendNumber = _crypt.decrypt(constants.destinationAccount.getString("friendNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
//                itemName.setText(distributorName);
                constants.destinationAccountEditor.putString("isSelect", "No");
                constants.destinationAccountEditor.commit();
                constants.destinationAccountEditor.apply();
                payableBeforeTxt.setText(amountBeforeDue);
                payableAfterTxt.setText(amountAfterDue);
                billingMonthTxt.setText(billingMonth);
                billingTxt.setText(companyName);
                dueDateTxt.setText(dueDate);
                statusTxt.setText(billStatus);
                consumerIDTxt.setText(billConsumerNumber);
                consumerNumberTxt.setText(consumerName);
                itemName.setText(alias);

                if (billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("unpaid") || billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("blocked")) {

                    statusTxt.setTextColor(getActivity().getResources().getColor(R.color.tabunderline));
                } else
                    statusTxt.setTextColor(getActivity().getResources().getColor(R.color.lgreen));

                Date currentDate = today.getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date mDate;
                long timeInMilliseconds = 0, currentTimeToMili = 0;
                try {
                    if (!dueDate.equals("N/A")) {
                        mDate = sdf.parse(dueDate);
                        timeInMilliseconds = mDate.getTime();
                        currentTimeToMili = currentDate.getTime();
                        System.out.println("Date in milli :: " + timeInMilliseconds);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (!TextUtils.isEmpty(billStatus) && !billStatus.equals("N/A")) {

                    if (billStatus.toLowerCase().equals("partial payment")) {

                        if (currentTimeToMili <= timeInMilliseconds) {


                            payabaleAmount.setText(amountBeforeDue);
                            String amnt = _crypt.decrypt(constants.destinationAccount.getString("amount", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            editAmount.setText(amnt.split("\\.")[0]);
                            editAmount.setFocusableInTouchMode(false);

                        } else if (currentTimeToMili > timeInMilliseconds) {

                            payabaleAmount.setText(amountAfterDue);
                            String amnt = _crypt.decrypt(constants.destinationAccount.getString("amount", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            editAmount.setText(amnt.split("\\.")[0]);
                            editAmount.setFocusableInTouchMode(false);

                        }
                    } else {

                        if (currentTimeToMili <= timeInMilliseconds) {


                            payabaleAmount.setText(amountBeforeDue);
                            editAmount.setText(amountBeforeDue.split("\\.")[0]);
                            editAmount.setFocusableInTouchMode(false);

                        } else if (currentTimeToMili > timeInMilliseconds) {

                            payabaleAmount.setText(amountAfterDue);
                            editAmount.setText(amountAfterDue.split("\\.")[0]);
                            editAmount.setFocusableInTouchMode(false);

                        }
                    }
                }

                if (currentTimeToMili <= timeInMilliseconds) {


                    payabaleAmount.setText(amountBeforeDue);

                } else if (currentTimeToMili > timeInMilliseconds) {

                    payabaleAmount.setText(amountAfterDue);

                }


            } else if (!TextUtils.isEmpty(type) && type.equals("payBillFromHome")) {

                arrowDestination.setVisibility(View.INVISIBLE);
                upperPart2.setEnabled(false);
//            editAmount.setText(constants.destinationAccount.getString("amount", "N/A"));
//            editAmount.setFocusableInTouchMode(false);
                String re = constants.destinationAccount.getString("remarks", "N/A");
                if (!TextUtils.isEmpty(re) && !re.equals("N/A") && !re.equals("null") && !re.equals(null)) {
                    editRemarks.setText(_crypt.decrypt(constants.destinationAccount.getString("remarks", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    editRemarks.setFocusableInTouchMode(false);
                }
                distributorID = _crypt.decrypt(constants.destinationAccount.getString("DistributorID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                distributorAccountNumber = _crypt.decrypt(constants.destinationAccount.getString("distributorAccountNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                amountAfterDue = _crypt.decrypt(constants.destinationAccount.getString("AmountAfterDueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                amountBeforeDue = _crypt.decrypt(constants.destinationAccount.getString("AmountWithinDueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                billStatus = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                billingMonth = _crypt.decrypt(constants.destinationAccount.getString("BillingMonth", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                dueDate = _crypt.decrypt(constants.destinationAccount.getString("DueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                consumerName = _crypt.decrypt(constants.destinationAccount.getString("ConsumerName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                billConsumerNumber = _crypt.decrypt(constants.destinationAccount.getString("BillConsumerNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                companyName = _crypt.decrypt(constants.destinationAccount.getString("CompanyName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                if (companyName.toLowerCase().contains("postpaid")) {
                    isPostpaid = true;
                }
                alias = _crypt.decrypt(constants.destinationAccount.getString("alias", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                String cat = constants.destinationAccount.getString("categoryType", "N/A");
                if (!TextUtils.isEmpty(cat) && !cat.equals("N/A") && !cat.equals("null") && !cat.equals(null)) {
                    categoryType = _crypt.decrypt(constants.destinationAccount.getString("categoryType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                } else {
                    categoryType = "N/A";
                }

//                categoryType = _crypt.decrypt(constants.destinationAccount.getString("categoryType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                String msg = constants.destinationAccount.getString("messageId", "N/A");
                if (!TextUtils.isEmpty(msg) && !msg.equals("N/A") && !msg.equals("null") && !msg.equals(null)) {
                    messageId = _crypt.decrypt(constants.destinationAccount.getString("messageId", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                } else
                    messageId = "N/A";
//                messageId = _crypt.decrypt(constants.destinationAccount.getString("messageId", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                String tar = constants.destinationAccount.getString("targetId", "N/A");
                if (!TextUtils.isEmpty(tar) && !tar.equals("N/A") && !tar.equals("null") && !tar.equals(null)) {
                    targetId = _crypt.decrypt(constants.destinationAccount.getString("targetId", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                } else
                    targetId = "N/A";
//                targetId = _crypt.decrypt(constants.destinationAccount.getString("targetId", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                String frd = constants.destinationAccount.getString("friendNumber", "N/A");
                if (!TextUtils.isEmpty(frd) && !frd.equals("N/A") && !frd.equals("null") && !frd.equals(null)) {
                    friendNumber = _crypt.decrypt(constants.destinationAccount.getString("friendNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                } else
                    friendNumber = "N/A";
//                friendNumber = _crypt.decrypt(constants.destinationAccount.getString("friendNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
//                itemName.setText(distributorName);
                constants.destinationAccountEditor.putString("isSelect", "No");
                constants.destinationAccountEditor.commit();
                constants.destinationAccountEditor.apply();
                payableBeforeTxt.setText(amountBeforeDue);
                payableAfterTxt.setText(amountAfterDue);
                billingMonthTxt.setText(billingMonth);
                billingTxt.setText(companyName);
                dueDateTxt.setText(dueDate);
                statusTxt.setText(billStatus);
                consumerIDTxt.setText(billConsumerNumber);
                consumerNumberTxt.setText(consumerName);
                itemName.setText(alias);

                userNameTxt.setText(intent.getStringExtra("sourceTile"));
                accMaskTxt.setVisibility(View.VISIBLE);
                accMaskTxt.setText(utils.getMaskedString(intent.getStringExtra("sourceNumber")));
                amountTxt.setVisibility(View.VISIBLE);
                amountTxt.setText(intent.getStringExtra("sourceCurrency") + " " + intent.getStringExtra("sourceAmount"));
                sourceAccount = intent.getStringExtra("sourceNumber");
                sourceCurrency = intent.getStringExtra("sourceCurrency");
                sourceBalance = intent.getStringExtra("sourceAmount");
                sourceuserName = intent.getStringExtra("sourceTile");
                branchCode = intent.getStringExtra("sourceBranchCode");

                if (billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("unpaid") || billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("blocked")) {

                    statusTxt.setTextColor(getActivity().getResources().getColor(R.color.tabunderline));
                } else
                    statusTxt.setTextColor(getActivity().getResources().getColor(R.color.lgreen));

                Date currentDate = today.getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date mDate;
                long timeInMilliseconds = 0, currentTimeToMili = 0;
                try {
                    if (!dueDate.equals("N/A")) {
                        mDate = sdf.parse(dueDate);
                        timeInMilliseconds = mDate.getTime();
                        currentTimeToMili = currentDate.getTime();
                        System.out.println("Date in milli :: " + timeInMilliseconds);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

//            if (!TextUtils.isEmpty(billStatus) && !billStatus.equals("N/A")) {
//
//                if (billStatus.toLowerCase().equals("partial payment")) {
//
//                    if (currentTimeToMili <= timeInMilliseconds) {
//
//
//                        payabaleAmount.setText(amountBeforeDue);
//                        editAmount.setText(constants.destinationAccount.getString("amount", "N/A"));
//                        editAmount.setFocusableInTouchMode(false);
//
//                    } else if (currentTimeToMili > timeInMilliseconds) {
//
//                        payabaleAmount.setText(amountAfterDue);
//                        editAmount.setText(constants.destinationAccount.getString("amount", "N/A"));
//                        editAmount.setFocusableInTouchMode(false);
//
//                    }
//                } else {
//
//                    if (currentTimeToMili <= timeInMilliseconds) {
//
//
//                        payabaleAmount.setText(amountBeforeDue);
//                        editAmount.setText(amountBeforeDue);
//                        editAmount.setFocusableInTouchMode(false);
//
//                    } else if (currentTimeToMili > timeInMilliseconds) {
//
//                        payabaleAmount.setText(amountAfterDue);
//                        editAmount.setText(amountAfterDue);
//                        editAmount.setFocusableInTouchMode(false);
//
//                    }
//                }
//            }


                editAmount.setText(intent.getStringExtra("amount").split("\\.")[0]);
                if (billStatus.toLowerCase().contains("partial")) {

                } else
                    editAmount.setFocusableInTouchMode(false);

                if (currentTimeToMili <= timeInMilliseconds) {


                    payabaleAmount.setText(amountBeforeDue);

                } else if (currentTimeToMili > timeInMilliseconds) {

                    payabaleAmount.setText(amountAfterDue);

                }
                if (list.equals("N/A")) {
                    GetPurposeBillList();
//            showSpinner();
                } else {
                    Gson gson = new Gson();
                    Type type1 = new TypeToken<ArrayList<String>>() {
                    }.getType();

                    purpStringArrayList = gson.fromJson(list, type1);
                    purpose = new String[purpStringArrayList.size()];
                    purpose = purpStringArrayList.toArray(purpose);
                    purposeSelected = intent.getStringExtra("remarks");
                    spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(purposeSelected));
                    purposesSpinner.setClickable(false);
                    purposesSpinner.setEnabled(false);
                }

                Verification();


            }


            if (list.equals("N/A")) {
                GetPurposeBillList();
//            showSpinner();
            } else {
                Gson gson = new Gson();
                Type type1 = new TypeToken<ArrayList<String>>() {
                }.getType();

                purpStringArrayList = gson.fromJson(list, type1);
                purpose = new String[purpStringArrayList.size()];
                purpose = purpStringArrayList.toArray(purpose);
                if (!TextUtils.isEmpty(type) && type.equals("requestbill")) {
                    purposeSelected = _crypt.decrypt(constants.destinationAccount.getString("purpose", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(purposeSelected));
                    purposesSpinner.setClickable(false);
                    purposesSpinner.setEnabled(false);
                } else {

                    spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(0);
                    purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            try {


                                // Get select item
                                if (purposesSpinner.getSelectedItem() == "Purpose of Payment") {
                                    purposeSelected = purposesSpinner.getSelectedItem().toString();
                                    Log.d("identity", "---" + purposeSelected);

                                } else {

                                    purposeSelected = purposesSpinner.getSelectedItem().toString();

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });
                }
            }


            String type4 = intent.getStringExtra("requestbill");

            if (!TextUtils.isEmpty(type4) && type4.equals("bill")) {
                String billStatus1 = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

//            arrowDestination.setVisibility(View.INVISIBLE);
//            upperPart2.setEnabled(false);
//            editAmount.setText(constants.destinationAccount.getString("amount", "N/A"));
//            editAmount.setFocusableInTouchMode(false);
//            editRemarks.setText(constants.destinationAccount.getString("remarks", "N/A"));
//            editRemarks.setFocusableInTouchMode(false);
                distributorID = _crypt.decrypt(constants.destinationAccount.getString("DistributorID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                distributorAccountNumber = _crypt.decrypt(constants.destinationAccount.getString("distributorAccountNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                amountAfterDue = _crypt.decrypt(constants.destinationAccount.getString("AmountAfterDueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                amountBeforeDue = _crypt.decrypt(constants.destinationAccount.getString("AmountWithinDueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                billStatus = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                billingMonth = _crypt.decrypt(constants.destinationAccount.getString("BillingMonth", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                dueDate = _crypt.decrypt(constants.destinationAccount.getString("DueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                consumerName = _crypt.decrypt(constants.destinationAccount.getString("ConsumerName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                billConsumerNumber = _crypt.decrypt(constants.destinationAccount.getString("BillConsumerNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                companyName = _crypt.decrypt(constants.destinationAccount.getString("CompanyName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                if (companyName.toLowerCase().contains("postpaid")) {
                    isPostpaid = true;
                }
                alias = _crypt.decrypt(constants.destinationAccount.getString("alias", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                String cat = constants.destinationAccount.getString("categoryType", "N/A");
                if (!TextUtils.isEmpty(cat) && !cat.equals("N/A") && !cat.equals("null") && !cat.equals(null)) {
                    categoryType = _crypt.decrypt(constants.destinationAccount.getString("categoryType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                } else {
                    categoryType = "N/A";

                }
                //            messageId = constants.destinationAccount.getString("messageId", "N/A");
//            targetId = constants.destinationAccount.getString("targetId", "N/A");
//            friendNumber = constants.destinationAccount.getString("friendNumber", "N/A");
//                itemName.setText(distributorName);
                constants.destinationAccountEditor.putString("isSelect", "No");
                constants.destinationAccountEditor.commit();
                constants.destinationAccountEditor.apply();
                payableBeforeTxt.setText(amountBeforeDue);
                payableAfterTxt.setText(amountAfterDue);
                billingMonthTxt.setText(billingMonth);
                billingTxt.setText(companyName);
                dueDateTxt.setText(dueDate);

                if (billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("unpaid") || billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("partialpayment")) {

                    statusTxt.setText("Un-Paid");
                } else if (billStatus.toLowerCase().equals("paid")) {

                    statusTxt.setText("Paid");
                } else if (billStatus.toLowerCase().equals("blocked")) {

                    statusTxt.setText("Blocked");
                }

                consumerIDTxt.setText(billConsumerNumber);
                consumerNumberTxt.setText(consumerName);
                itemName.setText(alias);

                if (billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("unpaid") || billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("blocked") || billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("partialpayment")) {

                    statusTxt.setTextColor(getActivity().getResources().getColor(R.color.tabunderline));
                } else
                    statusTxt.setTextColor(getActivity().getResources().getColor(R.color.lgreen));

                Date currentDate = today.getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date mDate;
                long timeInMilliseconds = 0, currentTimeToMili = 0;
                try {
                    if (!dueDate.equals("N/A")) {
                        mDate = sdf.parse(dueDate);
                        timeInMilliseconds = mDate.getTime();
                        currentTimeToMili = currentDate.getTime();
                        System.out.println("Date in milli :: " + timeInMilliseconds);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (!TextUtils.isEmpty(billStatus1) && !billStatus1.equals("N/A")) {

                    if (billStatus1.toLowerCase().equals("partial payment")) {


                    } else {

                        if (currentTimeToMili <= timeInMilliseconds) {


                            payabaleAmount.setText(amountBeforeDue);
                            editAmount.setText(amountBeforeDue.split("\\.")[0]);
                            editAmount.setFocusableInTouchMode(false);

                        } else if (currentTimeToMili > timeInMilliseconds) {

                            payabaleAmount.setText(amountAfterDue);
                            editAmount.setText(amountAfterDue.split("\\.")[0]);
                            editAmount.setFocusableInTouchMode(false);

                        }
                    }
                }

                if (currentTimeToMili <= timeInMilliseconds) {


                    payabaleAmount.setText(amountBeforeDue);

                } else if (currentTimeToMili > timeInMilliseconds) {

                    payabaleAmount.setText(amountAfterDue);

                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            try {

                CryptLib _crypt = new CryptLib();

                constants = Constants.getConstants(getActivity());
                String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                if (!TextUtils.isEmpty(userType) && userType.equals("NON_SAMBA")) {

                    isVisible = true;
                    onAttach(getActivity());


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            //Do Nothing
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        constants = Constants.getConstants(context);
        if (isVisible) {
            dialog = new Dialog((Activity) context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.error_dialog);
            textView = (TextView) dialog.findViewById(R.id.validatingTxt);
            doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
            heading = (TextView) dialog.findViewById(R.id.heading);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    PayBillsScreen.viewPager.setCurrentItem(2);
                }
            });
            textView.setText("This feature is only available for Samba Users.");
            heading.setText("WARNING");
            dialog.show();
//            isVisible = false;

        } else {
            try {
                CryptLib _crypt = new CryptLib();

                String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                if (!TextUtils.isEmpty(userType) && userType.equals("NON_SAMBA")) {
                    dialog = new Dialog((Activity) context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.error_dialog);
                    textView = (TextView) dialog.findViewById(R.id.validatingTxt);
                    doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
                    heading = (TextView) dialog.findViewById(R.id.heading);
                    dialog.setCancelable(false);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    doneBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            dialog.dismiss();
                            PayBillsScreen.viewPager.setCurrentItem(2);
                        }
                    });
                    textView.setText("This feature is only available for Samba Users.");
                    heading.setText("WARNING");
                    dialog.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void GetPurposeBillList() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    pd.show();
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sPurposeGroupCode", "BP");
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(getActivity());

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getPuposesURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("POTList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        purpStringArrayList.add(jsonObj.getString("Name"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();

                                                    }

                                                }

                                                purpose = new String[purpStringArrayList.size()];
                                                purpose = purpStringArrayList.toArray(purpose);

                                                Gson gson = new Gson();
                                                String json = gson.toJson(purpStringArrayList);
                                                constants.purposeBillList.putString("purposeList", json);
                                                constants.purposeBillList.commit();
                                                constants.purposeBillList.apply();
                                                showSpinner();
                                            } else if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());
                                                validateBtn.setEnabled(true);

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            validateBtn.setEnabled(true);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sPurposeGroupCode", "BP");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    validateBtn.setEnabled(true);
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
        }
    }


//    public void GetPurposeList() {
//
//        try {
//            pd.show();
//            if (helper.isNetworkAvailable()) {
//                webService.GetPurposeOfTransfer(constants.sharedPreferences.getString("userName", "N/A"), "BP", constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getGetPurposeList();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Information is not valid!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
////                                ProfileSetupScreen.pager.setCurrentItem(4);
////                                ProfileSetupScreen.currentItem++;
//
//
//                            } else {
//
//                                try {
//
//
//                                    JSONArray jsonArray = new JSONArray(jsonObject.getString("POTList"));
//                                    for (int i = 0; i < jsonArray.length(); i++) {
//                                        try {
//
//                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
//
//                                            purpStringArrayList.add(jsonObj.getString("Name"));
//
//                                        } catch (Exception ex) {
//                                            ex.printStackTrace();
//                                            pd.dismiss();
//                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                            validateBtn.setEnabled(true);
//                                        }
//
//                                    }
//
//                                    purpose = new String[purpStringArrayList.size()];
//                                    purpose = purpStringArrayList.toArray(purpose);
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    validateBtn.setEnabled(true);
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//                        }
//                    }
//                }, 3000);
//            } else {
////                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
//                pd.dismiss();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
//                validateBtn.setEnabled(true);
//            }
//        } catch (Exception e) {
//
////            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();
//            pd.dismiss();
//            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//            validateBtn.setEnabled(true);
//        }
//    }


    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                if (!TextUtils.isEmpty(type) && type.equals("requestbill")) {
                    purposeSelected = constants.destinationAccount.getString("purpose", "N/A");
                    spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(constants.destinationAccount.getString("purpose", "N/A")));
                    purposesSpinner.setClickable(false);
                } else {
                    spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(0);
                    purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            try {


                                // Get select item
                                if (purposesSpinner.getSelectedItem() == "Purpose of Payment") {
                                    purposeSelected = purposesSpinner.getSelectedItem().toString();
                                    Log.d("identity", "---" + purposeSelected);

                                } else {

                                    purposeSelected = purposesSpinner.getSelectedItem().toString();

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });

                    pd.dismiss();

                }

            }
        }, 1000);
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            CryptLib _crypt = new CryptLib();

            if (isFirst) {


                upperPart1.setEnabled(true);
                if (!TextUtils.isEmpty(type) && type.equals("requestbill")) {

                } else {
                    upperPart2.setEnabled(true);
                }
                if (isSource) {

                    isSelectSource = constants.sourceAccount.getString("isSelect", "N/A");
                    if (!isSelectSource.equals("N/A") && isSelectSource.equals("Yes")) {
                        sourceuserName = _crypt.decrypt(constants.sourceAccount.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        sourceBalance = _crypt.decrypt(constants.sourceAccount.getString("balance", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        sourceAccount = _crypt.decrypt(constants.sourceAccount.getString("accountNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        branchCode = _crypt.decrypt(constants.sourceAccount.getString("branchCode", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        sourceCurrency = _crypt.decrypt(constants.sourceAccount.getString("currency", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        userNameTxt.setText(sourceuserName);
                        accMaskTxt.setVisibility(View.VISIBLE);
                        accMaskTxt.setText(utils.getMaskedString(sourceAccount));
                        amountTxt.setVisibility(View.VISIBLE);
                        amountTxt.setText(sourceCurrency + " " + sourceBalance);
                    }

                    constants.sourceAccountEditor.putString("isSelect", "No");
                    constants.sourceAccountEditor.commit();
                    constants.sourceAccountEditor.apply();
                    isSource = false;
                }

                if (isDestination) {

                    isSelectDestination = constants.destinationAccount.getString("isSelect", "N/A");
                    if (!isSelectDestination.equals("N/A") && isSelectDestination.equals("Yes")) {
                        distributorID = _crypt.decrypt(constants.destinationAccount.getString("DistributorID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        distributorAccountNumber = _crypt.decrypt(constants.destinationAccount.getString("distributorAccountNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        amountAfterDue = _crypt.decrypt(constants.destinationAccount.getString("AmountAfterDueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        amountBeforeDue = _crypt.decrypt(constants.destinationAccount.getString("AmountWithinDueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        billStatus = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        billingMonth = _crypt.decrypt(constants.destinationAccount.getString("BillingMonth", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        dueDate = _crypt.decrypt(constants.destinationAccount.getString("DueDate", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        consumerName = _crypt.decrypt(constants.destinationAccount.getString("ConsumerName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        billConsumerNumber = _crypt.decrypt(constants.destinationAccount.getString("BillConsumerNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        companyName = _crypt.decrypt(constants.destinationAccount.getString("CompanyName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        if (companyName.toLowerCase().contains("postpaid")) {
                            isPostpaid = true;
                        }
                        alias = _crypt.decrypt(constants.destinationAccount.getString("alias", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        String cat = constants.destinationAccount.getString("categoryType", "N/A");
                        if (!TextUtils.isEmpty(cat) && !cat.equals("N/A") && !cat.equals("null") && !cat.equals(null)) {
                            categoryType = _crypt.decrypt(constants.destinationAccount.getString("categoryType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        } else {
                            categoryType = "N/A";
                        }
                        //                itemName.setText(distributorName);


                        payableBeforeTxt.setText(amountBeforeDue);
                        payableAfterTxt.setText(amountAfterDue);
                        billingMonthTxt.setText(billingMonth);
                        billingTxt.setText(companyName);
                        dueDateTxt.setText(dueDate);
                        statusTxt.setText(billStatus);
                        consumerIDTxt.setText(billConsumerNumber);
                        consumerNumberTxt.setText(consumerName);
                        itemName.setText(alias);


                        if (billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("unpaid") || billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("blocked")) {

                            statusTxt.setTextColor(getActivity().getResources().getColor(R.color.tabunderline));
                        } else
                            statusTxt.setTextColor(getActivity().getResources().getColor(R.color.lgreen));

                        Calendar today = Calendar.getInstance();
                        today.set(Calendar.MILLISECOND, 0);
                        today.set(Calendar.SECOND, 0);
                        today.set(Calendar.MINUTE, 0);
                        today.set(Calendar.HOUR_OF_DAY, 0);
                        Date currentDate = today.getTime();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date mDate;
                        long timeInMilliseconds = 0, currentTimeToMili = 0;
                        try {
                            if (!dueDate.equals("N/A")) {
                                mDate = sdf.parse(dueDate);
                                timeInMilliseconds = mDate.getTime();
                                currentTimeToMili = currentDate.getTime();
                                System.out.println("Date in milli :: " + timeInMilliseconds);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        String billStatus1 = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        if (!TextUtils.isEmpty(billStatus1) && !billStatus1.equals("N/A")) {

                            if (billStatus1.toLowerCase().equals("partial payment")) {

                                editAmount.setText("");
                                editAmount.setFocusableInTouchMode(true);
                                editAmount.setEnabled(true);
                            } else {

                                if (currentTimeToMili <= timeInMilliseconds) {


                                    payabaleAmount.setText(amountBeforeDue);
                                    editAmount.setText(amountBeforeDue.split("\\.")[0]);
                                    editAmount.setFocusableInTouchMode(false);
                                    editAmount.setEnabled(false);


                                } else if (currentTimeToMili > timeInMilliseconds) {

                                    payabaleAmount.setText(amountAfterDue);
                                    editAmount.setText(amountAfterDue.split("\\.")[0]);
                                    editAmount.setFocusableInTouchMode(false);
                                    editAmount.setEnabled(false);

                                }

                            }
                        }
                        if (currentTimeToMili <= timeInMilliseconds) {


                            payabaleAmount.setText(amountBeforeDue);

                        } else if (currentTimeToMili > timeInMilliseconds) {

                            payabaleAmount.setText(amountAfterDue);

                        }

                    }

                    constants.destinationAccountEditor.putString("isSelect", "No");
                    constants.destinationAccountEditor.commit();
                    constants.destinationAccountEditor.apply();
                    isDestination = false;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public void showDilogForErrorForMenu(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(getActivity(), HomeScreen.class);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).startActivity(intent);
                ((Activity) getActivity()).finish();
                ((Activity) getActivity()).finishAffinity();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public void showDilogForErrorForNonSamba(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                PayBillsScreen.viewPager.setCurrentItem(2);
            }
        });

        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


    public void Verification() {

        try {
            CryptLib _crypt = new CryptLib();

            remarks = editAmount.getText().toString().trim();
            amount = editRemarks.getText().toString().trim().replaceAll("\\,", "");
            String billStatus1 = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());


            if (payableSelected.equals("Invoice/ Voucher Payment")) {

                if ((TextUtils.isEmpty(sourceAccount) || sourceAccount.equals("N/A")) || (TextUtils.isEmpty(sourceBalance) || sourceBalance.equals("N/A")) || (TextUtils.isEmpty(sourceuserName) || sourceuserName.equals("N/A"))) {

                    utils.showDilogForError("Please select Source account", "WARNING");

                } else if (TextUtils.isEmpty(remarks)) {

                    errorMessageForAmount.setVisibility(View.VISIBLE);
                    errorMessageForAmount.bringToFront();
                    errorMessageForAmount.setError("");
                    if (payableSelected.equals("Credit Card Bill Payment"))
                        errorMessageForAmount.setText("Please enter valid Credit Card Number");
                    if (payableSelected.equals("Invoice/Voucher Payment"))
                        errorMessageForAmount.setText("Please enter valid Invoice/Voucher Number");
                    if (payableSelected.equals("Top Up"))
                        errorMessageForAmount.setText("Please enter valid Mobile Number");

                } else BillInquiry();
            } else {
                if (TextUtils.isEmpty(amount)) {

                    errorMessageForRemarks.setVisibility(View.VISIBLE);
                    errorMessageForRemarks.bringToFront();
                    errorMessageForRemarks.setError("");
                    errorMessageForRemarks.setText("Please enter valid amount");

                } else if (!helper.validateInputForSC(amount) || amount.contains(" ")) {

                    errorMessageForRemarks.setVisibility(View.VISIBLE);
                    errorMessageForRemarks.bringToFront();
                    errorMessageForRemarks.setError("");
                    errorMessageForRemarks.setText("Amount cannot contains <,>,\",',%,(,),&,+,\\,~,space");


                } else if (amount.startsWith("0")) {

                    amount = amount.replaceFirst("^0+(?!$)", "");

                    if (!TextUtils.isEmpty(amount) && !amount.equals("")) {

                        if (amount.equals("0")) {
                            errorMessageForRemarks.setVisibility(View.VISIBLE);
                            errorMessageForRemarks.bringToFront();
                            errorMessageForRemarks.setError("");
                            errorMessageForRemarks.setText("Please enter valid amount");
                        }

                    } else {

                        errorMessageForRemarks.setVisibility(View.VISIBLE);
                        errorMessageForRemarks.bringToFront();
                        errorMessageForRemarks.setError("");
                        errorMessageForRemarks.setText("Please enter valid amount");
                    }

                } else {

                    amount = amount.split("\\.")[0];

                }


                if (TextUtils.isEmpty(remarks)) {

                    errorMessageForAmount.setVisibility(View.VISIBLE);
                    errorMessageForAmount.bringToFront();
                    errorMessageForAmount.setError("");
                    if (payableSelected.equals("Credit Card Bill Payment"))
                        errorMessageForAmount.setText("Please enter valid Credit Card Number");
                    if (payableSelected.equals("Invoice/Voucher Payment"))
                        errorMessageForAmount.setText("Please enter valid Invoice/Voucher Number");
                    if (payableSelected.equals("Top Up"))
                        errorMessageForAmount.setText("Please enter valid Mobile Number");

                } else if (!helper.validateInputForSC(amount)) {

                    errorMessageForRemarks.setVisibility(View.VISIBLE);
                    errorMessageForRemarks.bringToFront();
                    errorMessageForRemarks.setError("");
                    errorMessageForRemarks.setText("Amount cannot contains <,>,\",',%,(,),&,+,\\,~");


                }

                if (TextUtils.isEmpty(payableSelected) || payableSelected.equals("Purpose of Payment") || payableSelected.equals("N/A")) {

//                errorMessageForSpinner.setVisibility(View.VISIBLE);
//                errorMessageForSpinner.bringToFront();
//                errorMessageForSpinner.setError("");
//                errorMessageForSpinner.setText("Please select purpose of payment");
                }

                if ((TextUtils.isEmpty(sourceAccount) || sourceAccount.equals("N/A")) || (TextUtils.isEmpty(sourceBalance) || sourceBalance.equals("N/A")) || (TextUtils.isEmpty(sourceuserName) || sourceuserName.equals("N/A"))) {

                    utils.showDilogForError("Please select Source account", "WARNING");

                } else if (!TextUtils.isEmpty(amount) && !amount.startsWith("0")) {

                    sourceBalance = sourceBalance.replaceAll(",", "");
                    bal = new BigDecimal(sourceBalance).intValue();


                    if (!TextUtils.isEmpty(sourceBalance) && !sourceBalance.equals("N/A")) {

//                    maxAmount = payabaleAmount.getText().toString().replaceAll(",", "").trim();
//                    maxAmount = maxAmount.substring(0, maxAmount.indexOf("."));
                        amountToInt = Integer.valueOf(amount);
//                    maxAmountToInt = Integer.valueOf(maxAmount);
                        //amountToInt != maxAmountToInt

                        if (amountToInt > bal) {

                            utils.showDilogForAmount("Amount cannot be greater than avaliable balance.", "WARNING");

//                    } else if (!TextUtils.isEmpty(billStatus1) && !billStatus1.equals("N/A")) {
//
//
//                        if (billStatus1.toLowerCase().equals("partial payment")) {
//
//
//                            if (amountToInt > maxAmountToInt) {
//
//                                if (!isPostpaid)
//                                    utils.showDilogForAmount("Amount cannot be greater than Payable Amount.", "WARNING");
//
//                            }
//
//                        } else if (amountToInt != maxAmountToInt) {
//
//                            if (!isPostpaid)
//                                utils.showDilogForAmount("Amount should be equal to Payable Amount.", "WARNING");
//
//                        }
//
//
//                    } else if (amountToInt != maxAmountToInt) {
//
//                        if (!isPostpaid)
//                            utils.showDilogForAmount("Amount should be equal to Payable Amount.", "WARNING");
//                    }
                        } else BillInquiry();
                    }
                }
            }


//            if (!sourceCurrency.equals("PKR")) {
//
//
//                utils.showDilogForError("1Bill payment can only be made from PKR account.", "ERROR");
//            }
//
//            if (!TextUtils.isEmpty(billStatus1) && !billStatus1.equals("N/A") && !billStatus.toLowerCase().equals("paid") && !billStatus.toLowerCase().equals("blocked")) {
//
//
//                if (billStatus1.toLowerCase().equals("partial payment")) {
//
//                    if (!isPostpaid) {
//
//                        if ((!TextUtils.isEmpty(amount) && !amount.startsWith("0") && amountToInt <= bal && amountToInt <= maxAmountToInt) && !TextUtils.isEmpty(remarks) && helper.validateInputForSC(remarks) && !purposeSelected.equals("Purpose of Payment") && (!TextUtils.isEmpty(sourceAccount) && !sourceAccount.equals("N/A")) && (!TextUtils.isEmpty(sourceBalance) && !sourceBalance.equals("N/A")) && (!TextUtils.isEmpty(sourceuserName) && !sourceuserName.equals("N/A")) && (!TextUtils.isEmpty(distributorID) && !distributorID.equals("N/A")) && (!TextUtils.isEmpty(distributorAccountNumber) && !distributorAccountNumber.equals("N/A")) && sourceCurrency.equals("PKR")) {
//
//                            //Send to confirm
//
////                        validateBtn.setEnabled(false);
//
//                            Intent intent = new Intent(getActivity(), BillReviewScreen.class);
//                            intent.putExtra("userName", sourceuserName);
//                            intent.putExtra("balance", sourceBalance);
//                            intent.putExtra("accountNumber", sourceAccount);
//                            intent.putExtra("purpose", purposeSelected);
//                            intent.putExtra("currency", sourceCurrency);
//                            intent.putExtra("distributorAccountNumber", distributorAccountNumber);
//                            intent.putExtra("mobile", billConsumerNumber);
//                            intent.putExtra("remarks", remarks);
//                            intent.putExtra("stan", stan);
//                            intent.putExtra("rrn", rrn);
//                            intent.putExtra("amount", amount);
//                            intent.putExtra("branchCode", branchCode);
//                            intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                            intent.putExtra("distributorName", alias);
//                            intent.putExtra("distributorID", distributorID);
//                            intent.putExtra("categoryType", categoryType);
//                            intent.putExtra("ftType", type);
//
//                            intent.putExtra("friendNumber", friendNumber);
//                            intent.putExtra("messageId", messageId);
//                            intent.putExtra("targetId", targetId);
//                            intent.putExtra("companyName", companyName);
//                            getActivity().overridePendingTransition(0, 0);
//                            startActivity(intent);
//                            validateBtn.setEnabled(true);
//
//
//                        }
//                    } else {
//
//
//                        if ((!TextUtils.isEmpty(amount) && !amount.startsWith("0") && amountToInt <= bal) && !TextUtils.isEmpty(remarks) && helper.validateInputForSC(remarks) && !purposeSelected.equals("Purpose of Payment") && (!TextUtils.isEmpty(sourceAccount) && !sourceAccount.equals("N/A")) && (!TextUtils.isEmpty(sourceBalance) && !sourceBalance.equals("N/A")) && (!TextUtils.isEmpty(sourceuserName) && !sourceuserName.equals("N/A")) && (!TextUtils.isEmpty(distributorID) && !distributorID.equals("N/A")) && (!TextUtils.isEmpty(distributorAccountNumber) && !distributorAccountNumber.equals("N/A")) && sourceCurrency.equals("PKR")) {
//
//                            //Send to confirm
//
////                        validateBtn.setEnabled(false);
//
//                            Intent intent = new Intent(getActivity(), BillReviewScreen.class);
//                            intent.putExtra("userName", sourceuserName);
//                            intent.putExtra("balance", sourceBalance);
//                            intent.putExtra("accountNumber", sourceAccount);
//                            intent.putExtra("purpose", purposeSelected);
//                            intent.putExtra("currency", sourceCurrency);
//                            intent.putExtra("distributorAccountNumber", distributorAccountNumber);
//                            intent.putExtra("mobile", billConsumerNumber);
//                            intent.putExtra("remarks", remarks);
//                            intent.putExtra("stan", stan);
//                            intent.putExtra("rrn", rrn);
//                            intent.putExtra("amount", amount);
//                            intent.putExtra("branchCode", branchCode);
//                            intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                            intent.putExtra("distributorName", alias);
//                            intent.putExtra("distributorID", distributorID);
//                            intent.putExtra("categoryType", categoryType);
//                            intent.putExtra("ftType", type);
//
//                            intent.putExtra("friendNumber", friendNumber);
//                            intent.putExtra("messageId", messageId);
//                            intent.putExtra("targetId", targetId);
//                            intent.putExtra("companyName", companyName);
//                            getActivity().overridePendingTransition(0, 0);
//                            startActivity(intent);
//                            validateBtn.setEnabled(true);
//
//
//                        }
//                    }
//
//                } else {
//
//                    if ((!TextUtils.isEmpty(amount) && !amount.startsWith("0") && amountToInt <= bal && amountToInt == maxAmountToInt) && !TextUtils.isEmpty(remarks) && helper.validateInputForSC(remarks) && !purposeSelected.equals("Purpose of Payment") && (!TextUtils.isEmpty(sourceAccount) && !sourceAccount.equals("N/A")) && (!TextUtils.isEmpty(sourceBalance) && !sourceBalance.equals("N/A")) && (!TextUtils.isEmpty(sourceuserName) && !sourceuserName.equals("N/A")) && (!TextUtils.isEmpty(distributorID) && !distributorID.equals("N/A")) && (!TextUtils.isEmpty(distributorAccountNumber) && !distributorAccountNumber.equals("N/A")) && sourceCurrency.equals("PKR")) {
//
//                        //Send to confirm
//
////                        validateBtn.setEnabled(false);
//
//
//                        Intent intent = new Intent(getActivity(), BillReviewScreen.class);
//                        intent.putExtra("userName", sourceuserName);
//                        intent.putExtra("balance", sourceBalance);
//                        intent.putExtra("accountNumber", sourceAccount);
//                        intent.putExtra("purpose", purposeSelected);
//                        intent.putExtra("currency", sourceCurrency);
//                        intent.putExtra("distributorAccountNumber", distributorAccountNumber);
//                        intent.putExtra("mobile", billConsumerNumber);
//                        intent.putExtra("remarks", remarks);
//                        intent.putExtra("stan", stan);
//                        intent.putExtra("rrn", rrn);
//                        intent.putExtra("amount", amount);
//                        intent.putExtra("branchCode", branchCode);
//                        intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                        intent.putExtra("distributorName", alias);
//                        intent.putExtra("distributorID", distributorID);
//                        intent.putExtra("categoryType", categoryType);
//                        intent.putExtra("ftType", type);
//                        intent.putExtra("friendNumber", friendNumber);
//                        intent.putExtra("messageId", messageId);
//                        intent.putExtra("targetId", targetId);
//                        intent.putExtra("companyName", companyName);
//                        getActivity().overridePendingTransition(0, 0);
//                        startActivity(intent);
//                        validateBtn.setEnabled(true);
//
//
//                    }
//                }
//            } else {
//
//                if ((!TextUtils.isEmpty(amount) && !amount.startsWith("0") && amountToInt <= bal && amountToInt == maxAmountToInt) && !TextUtils.isEmpty(remarks) && helper.validateInputForSC(remarks) && !purposeSelected.equals("Purpose of Payment") && (!TextUtils.isEmpty(sourceAccount) && !sourceAccount.equals("N/A")) && (!TextUtils.isEmpty(sourceBalance) && !sourceBalance.equals("N/A")) && (!TextUtils.isEmpty(sourceuserName) && !sourceuserName.equals("N/A")) && (!TextUtils.isEmpty(distributorID) && !distributorID.equals("N/A")) && (!TextUtils.isEmpty(distributorAccountNumber) && !distributorAccountNumber.equals("N/A")) && sourceCurrency.equals("PKR")) {
//
//                    //Send to confirm
//
////                        validateBtn.setEnabled(false);
//
//                    if (billStatus.toLowerCase().equals("paid") || billStatus.toLowerCase().equals("blocked")) {
//
//                        utils.showDilogForError("Bill has already been Paid or is Blocked.", "ERROR");
//                    } else {
//
//                        Intent intent = new Intent(getActivity(), BillReviewScreen.class);
//                        intent.putExtra("userName", sourceuserName);
//                        intent.putExtra("balance", sourceBalance);
//                        intent.putExtra("accountNumber", sourceAccount);
//                        intent.putExtra("purpose", purposeSelected);
//                        intent.putExtra("currency", sourceCurrency);
//                        intent.putExtra("distributorAccountNumber", distributorAccountNumber);
//                        intent.putExtra("mobile", billConsumerNumber);
//                        intent.putExtra("remarks", remarks);
//                        intent.putExtra("stan", stan);
//                        intent.putExtra("rrn", rrn);
//                        intent.putExtra("amount", amount);
//                        intent.putExtra("branchCode", branchCode);
//                        intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                        intent.putExtra("distributorName", alias);
//                        intent.putExtra("distributorID", distributorID);
//                        intent.putExtra("categoryType", categoryType);
//                        intent.putExtra("ftType", type);
//                        intent.putExtra("friendNumber", friendNumber);
//                        intent.putExtra("messageId", messageId);
//                        intent.putExtra("targetId", targetId);
//                        intent.putExtra("companyName", companyName);
//                        getActivity().overridePendingTransition(0, 0);
//                        startActivity(intent);
//                        validateBtn.setEnabled(true);
//
//                    }
//                }
//            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void GetbillerDistributorList() {


        try {

            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sCompanyID", Constants.companyID);
                    params.put("sCompanyType", Constants.companyType);
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(getActivity());

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getBillerDistributorListURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_BillDistributorResult").getString("Status_Code").equals("00")) {

                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_BillDistributorResult").getString("Local_BillDistributorList"));

                                                if (jsonArray.length() > 0) {

                                                    for (int i = 0; i < jsonArray.length(); i++) {

                                                        try {

                                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                            billerDistributorList.put(jsonObj.getString("Distributor_Name"), jsonObj.getString("Distributor_ID") + "~" + jsonObj.getString("Distributor_DistributorAccountNumber"));

                                                        } catch (Exception ex) {

                                                            ex.printStackTrace();
                                                            pd.dismiss();
                                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        }

                                                    }

                                                    Gson gson = new Gson();
                                                    String json = gson.toJson(billerDistributorList);
                                                    constants.billerListEditor.putString("billerListCategoryFor1Bill", json);
                                                    constants.billerListEditor.commit();
                                                    constants.billerListEditor.apply();

                                                    String billerList = constants.billerList.getString("billerListCategoryFor1Bill", "N/A");
                                                    Type type1 = new TypeToken<HashMap<String, String>>() {
                                                    }.getType();

                                                    billerMap = gson.fromJson(billerList, type1);
                                                    Iterator it = billerMap.entrySet().iterator();
                                                    while (it.hasNext()) {
                                                        Map.Entry pair = (Map.Entry) it.next();
                                                        purposeArrayList.add(pair.getKey().toString());
                                                    }


                                                    purpose = new String[purpStringArrayList.size()];
                                                    purpose = purpStringArrayList.toArray(purpose);
                                                    spinnerAdapter payableAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_1bill);
                                                    payableAdapter.addAll(purpose);
                                                    payableAdapter.add("Select Option");
                                                    payableSpinner.setAdapter(payableAdapter);
                                                    payableSpinner.setSelection(0);
                                                    payableSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                                                        @Override
                                                        public void onItemSelected(AdapterView<?> parent, View view,
                                                                                   int position, long id) {
                                                            try {


                                                                if (((TextView) parent.getChildAt(0)) != null) {
                                                                    ((TextView) parent.getChildAt(0)).setTextColor(Color.parseColor("#ffffff"));
                                                                    ((TextView) parent.getChildAt(0)).setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.item_white));

                                                                }
                                                                // Get select item
                                                                if (payableSpinner.getSelectedItem() == "Select Option") {
                                                                    payableSelected = payableSpinner.getSelectedItem().toString();
                                                                    Log.d("identity", "---" + payableSelected);

                                                                } else {
                                                                    payableSelected = payableSpinner.getSelectedItem().toString();
                                                                    Log.d("identity", "---" + payableSelected);
                                                                    switch (payableSelected) {

                                                                        case "Credit Card Bill Payment":
                                                                            amountTxtInput.setHint("Credit Card Number");
                                                                            remarksTxtInput.setHint("Amount");
                                                                            remarksTxtInput.setVisibility(View.VISIBLE);
                                                                            break;

                                                                        case "Invoice/Voucher Payment":
                                                                            amountTxtInput.setHint("Invoice/Voucher Number");
                                                                            remarksTxtInput.setVisibility(View.GONE);
                                                                            break;

                                                                        case "Top Up":
                                                                            amountTxtInput.setHint("Mobile Number");
                                                                            remarksTxtInput.setHint("Amount");
                                                                            remarksTxtInput.setVisibility(View.VISIBLE);
                                                                            break;


                                                                    }


                                                                }


                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }


                                                        @Override
                                                        public void onNothingSelected(AdapterView<?> parent) {
                                                            // TODO Auto-generated method stub
                                                        }
                                                    });
                                                    pd.dismiss();
                                                } else {

                                                    pd.dismiss();

                                                }


                                            } else if (jsonObject.getJSONObject("Signs_BillDistributorResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillDistributorResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());


                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_BillDistributorResult").getString("Status_Description"), "ERROR");
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sCompanyID",intent.getStringExtra("categoryID"));
//                            params.put("sCompanyType",intent.getStringExtra("categoryCompanyType"));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }


    }

    public void BillInquiry() {

        final int stanNumber = 6;

        try {
            pd.show();
            stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
            int rrnNumber = 12;
            rrn = String.valueOf(helper.generateRandom(rrnNumber));
            final String val = billerMap.get(payableSelected);
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sBillConsumerNumber", editAmount.getText().toString());
                    params.put("sDistributorID", val.split("~")[0]);
                    params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(getActivity());

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.billInquiryURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            try {

                                                CryptLib _crypt = new CryptLib();
                                                if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("00")) {

//                                                    constants.destinationAccountEditor.putString("isSelect", "Yes");
                                                    dueDate = jsonObject.getJSONObject("Signs_BillInquiryResult").getString("DueDate");
                                                    amountBeforeDue = jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountWithinDueDate");
                                                    amountAfterDue = jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountAfterDueDate");
                                                    billingMonth = jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillingMonth");
                                                    billStatus = jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillStatus");
//                                                    constants.destinationAccountEditor.putString("AmountAfterDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountAfterDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                                                    constants.destinationAccountEditor.putString("AmountWithinDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountWithinDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                                                    constants.destinationAccountEditor.putString("BillStatus", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillStatus"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                                                    constants.destinationAccountEditor.putString("BillingMonth", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillingMonth"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                                                    constants.destinationAccountEditor.putString("CompanyID", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                                                    constants.destinationAccountEditor.putString("DueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("DueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                                                    constants.destinationAccountEditor.putString("ConsumerName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("ConsumerName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                                                    constants.destinationAccountEditor.putString("CompanyName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                                                    constants.destinationAccountEditor.putString("RRN", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("RRN"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                                                    constants.destinationAccountEditor.commit();
//                                                    constants.destinationAccountEditor.apply();


                                                    if (payableSelected.equals("Invoice/ Voucher Number")) {

                                                        Calendar today = Calendar.getInstance();
                                                        today.set(Calendar.MILLISECOND, 0);
                                                        today.set(Calendar.SECOND, 0);
                                                        today.set(Calendar.MINUTE, 0);
                                                        today.set(Calendar.HOUR_OF_DAY, 0);
                                                        Date currentDate = today.getTime();
                                                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                                        Date mDate;
                                                        long timeInMilliseconds = 0, currentTimeToMili = 0;
                                                        try {
                                                            if (!dueDate.equals("N/A")) {
                                                                mDate = sdf.parse(dueDate);
                                                                timeInMilliseconds = mDate.getTime();
                                                                currentTimeToMili = currentDate.getTime();
                                                                System.out.println("Date in milli :: " + timeInMilliseconds);
                                                            }
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }


                                                        String billStatus1 =  billStatus;
                                                        if (!TextUtils.isEmpty(billStatus1) && !billStatus1.equals("N/A")) {


                                                            if (currentTimeToMili <= timeInMilliseconds) {


                                                                payabaleAmount.setText(amountBeforeDue);
                                                                editAmount.setText(amountBeforeDue.split("\\.")[0]);
                                                                editAmount.setFocusableInTouchMode(false);
                                                                editAmount.setEnabled(false);
                                                                amount = editAmount.getText().toString();


                                                            } else if (currentTimeToMili > timeInMilliseconds) {

                                                                payabaleAmount.setText(amountAfterDue);
                                                                editAmount.setText(amountAfterDue.split("\\.")[0]);
                                                                editAmount.setFocusableInTouchMode(false);
                                                                editAmount.setEnabled(false);
                                                                amount = editAmount.getText().toString();

                                                            }

                                                        }

                                                        Intent intent = new Intent(getActivity(), BillReviewScreen.class);
                                                        intent.putExtra("userName", sourceuserName);
                                                        intent.putExtra("balance", sourceBalance);
                                                        intent.putExtra("accountNumber", sourceAccount);
                                                        intent.putExtra("purpose", payableSelected);
                                                        intent.putExtra("currency", sourceCurrency);
                                                        intent.putExtra("distributorAccountNumber", val.split("~")[1]);
                                                        intent.putExtra("mobile", remarks);
                                                        intent.putExtra("remarks", "-");
                                                        intent.putExtra("stan", stan);
                                                        intent.putExtra("rrn", rrn);
                                                        intent.putExtra("amount", amount);
                                                        intent.putExtra("branchCode", branchCode);
                                                        intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        intent.putExtra("distributorName", "1Bill Payment");
                                                        intent.putExtra("distributorID", val.split("~")[0]);
                                                        intent.putExtra("categoryType", "-");
                                                        intent.putExtra("ftType", "-");
                                                        intent.putExtra("friendNumber", "-");
                                                        intent.putExtra("messageId", "-");
                                                        intent.putExtra("targetId", "-");
                                                        intent.putExtra("companyName", jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyName"));
                                                        getActivity().overridePendingTransition(0, 0);
                                                        startActivity(intent);
                                                        validateBtn.setEnabled(true);
                                                        pd.dismiss();
                                                    } else {
                                                        Intent intent = new Intent(getActivity(), BillReviewScreen.class);
                                                        intent.putExtra("userName", sourceuserName);
                                                        intent.putExtra("balance", sourceBalance);
                                                        intent.putExtra("accountNumber", sourceAccount);
                                                        intent.putExtra("purpose", payableSelected);
                                                        intent.putExtra("currency", sourceCurrency);
                                                        intent.putExtra("distributorAccountNumber", val.split("~")[1]);
                                                        intent.putExtra("mobile", remarks);
                                                        intent.putExtra("remarks", "-");
                                                        intent.putExtra("stan", stan);
                                                        intent.putExtra("rrn", rrn);
                                                        intent.putExtra("amount", amount);
                                                        intent.putExtra("branchCode", branchCode);
                                                        intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        intent.putExtra("distributorName", "1Bill Payment");
                                                        intent.putExtra("distributorID", val.split("~")[0]);
                                                        intent.putExtra("categoryType", "-");
                                                        intent.putExtra("ftType", "-");
                                                        intent.putExtra("friendNumber", "-");
                                                        intent.putExtra("messageId", "-");
                                                        intent.putExtra("targetId", "-");
                                                        intent.putExtra("companyName", jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyName"));
                                                        getActivity().overridePendingTransition(0, 0);
                                                        startActivity(intent);
                                                        validateBtn.setEnabled(true);
                                                        pd.dismiss();
                                                    }

                                                } else if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("54")) {
                                                    pd.dismiss();
                                                    utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());


                                                } else {

                                                    pd.dismiss();
                                                    utils.showDilogForError(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR");


                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sBillConsumerNumber", billerList.get(position).getConsumerNumber());
//                            params.put("sDistributorID", billerList.get(position).getCompanyID());
//                            params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }
}
