package com.ceesolutions.samba.chatMessenger;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.ATMLocatorScreen;
import com.ceesolutions.samba.accessControl.ContactUsScreen;
import com.ceesolutions.samba.beneficiariesManagement.Model.UserModel;
import com.ceesolutions.samba.chatMessenger.Adapter.ChatMessageAdapter;
import com.ceesolutions.samba.chatMessenger.Model.ChatMessage;
import com.ceesolutions.samba.transactions.Model.Account;
import com.ceesolutions.samba.transactions.Model.TransactionDetails;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.HorizontalListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ayaz on 1/18/18.
 */

public class ChatMessengerScreen extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private Button mButtonSend;
    private EditText mEditTextMessage, editText;
    private ImageView mImageView, sambaBtn, doneBtn;
    private ChatMessageAdapter mAdapter;
    private RelativeLayout relativeLayout, relativeLayout2, tagshorizontalListViewLayout, horizontalListViewLayout;
    public ArrayList<String> stringArrayList;
    private HorizontalListView listView, accountListView;
    public CustomHorizontalListAdapter horizontalListAdapter;
    public AccoutsCustomHorizontalListAdapter accoutsCustomHorizontalListAdapter;
    public DestinationAccountHorizontalListAdapter destinationAccountHorizontalListAdapter;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private ArrayList<Account> accounts;
    private ArrayList<UserModel> userModels;
    private ArrayList<TransactionDetails> transactionDetailsList;
    private ArrayList<String> sameDate;
    public boolean isMini = false;
    public boolean isFt = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_view);
        initView();


        editText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    userMessage();

                }
                return false;
            }
        });

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userMessage();
            }
        });

        sambaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage("Hello", "");
            }
        });
    }

    private void sendMessage(String message) {
        ChatMessage chatMessage = new ChatMessage(message, true, false);
        mAdapter.add(chatMessage);
        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
//        mimicOtherMessage(message);
    }

    private void mimicOtherMessage(String message) {
        ChatMessage chatMessage = new ChatMessage(message, false, false);
        mAdapter.add(chatMessage);
//        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);

    }

    private void sendMessage(String messag, String messsage) {
//        ChatMessage chatMessage = new ChatMessage("Hello", false, true);
//        mAdapter.add(chatMessage);
//
//        mimicOtherMessage();
        ChatMessage chatMessage = new ChatMessage(messag, false, true);
        mAdapter.add(chatMessage);

        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    private void mimicOtherMessage() {
        ChatMessage chatMessage = new ChatMessage(null, false, true);
        mAdapter.add(chatMessage);

        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
    }


    public void initView() {

        stringArrayList = new ArrayList<>();
        accounts = new ArrayList<>();
        userModels = new ArrayList<>();
        transactionDetailsList = new ArrayList<>();
        sameDate = new ArrayList<>();
        stringArrayList.add("Own Accounts");
        stringArrayList.add("Transfer within Samba");
        stringArrayList.add("Other Banks");
        requestQueue = Volley.newRequestQueue(ChatMessengerScreen.this);
        utils = new Utils(ChatMessengerScreen.this);
        helper = Helper.getHelper(ChatMessengerScreen.this);
        constants = Constants.getConstants(ChatMessengerScreen.this);
        pd = new Dialog(ChatMessengerScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        listView = (HorizontalListView) findViewById(R.id.tagshorizontalListView);
        accountListView = (HorizontalListView) findViewById(R.id.horizontalListView);
        horizontalListAdapter = new CustomHorizontalListAdapter(stringArrayList, ChatMessengerScreen.this);

        listView.setAdapter(horizontalListAdapter);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
//        mButtonSend = (Button) findViewById(R.id.btn_send);
        mEditTextMessage = (EditText) findViewById(R.id.chatBoxText);
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        relativeLayout2 = (RelativeLayout) findViewById(R.id.relativeLayout2);
        editText = (EditText) findViewById(R.id.edittext);
        mImageView = (ImageView) findViewById(R.id.shareBtn);
        sambaBtn = (ImageView) findViewById(R.id.sambaBtn);
        tagshorizontalListViewLayout = (RelativeLayout) findViewById(R.id.tagshorizontalListViewLayout);
        horizontalListViewLayout = (RelativeLayout) findViewById(R.id.horizontalListViewLayout);
        doneBtn = (ImageView) findViewById(R.id.doneBtn);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mEditTextMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                relativeLayout.setVisibility(View.VISIBLE);
                relativeLayout2.setVisibility(View.INVISIBLE);
                editText.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
            }
        });
        mAdapter = new ChatMessageAdapter(this, new ArrayList<ChatMessage>());
        mRecyclerView.setAdapter(mAdapter);

        sendMessage("Hey " + constants.sharedPreferences.getString("T24_FullName", "N/A") + "\n" +
                "Good afternoon, welcome back to SIGNS.\n" +
                "\n" +
                "What can I do for you today?", "");
    }


    public void userMessage() {

        String message = editText.getText().toString();
        if (TextUtils.isEmpty(message)) {
            return;
        }
        sendMessage(message);
        editText.setText("");
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        if (message.contains("Fund Transfer") || message.contains("Funds Transfer") || message.contains("fund transfer") || message.contains("funds transfer")) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage("Okay. On which Account you wana transfer?", "");
                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {
                    tagshorizontalListViewLayout.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);
                    relativeLayout2.setVisibility(View.VISIBLE);
                }
            }, 1500);

        } else if (message.contains("balance") || message.contains("Balance")) {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage("Ok. Please select an account.", "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    GetAccountsList();
                }
            }, 2000);


        } else if (message.contains("mini statement") || message.contains("Mini statement") || message.contains("statement") || message.contains("latest transaction")) {

            isMini = true;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage("Ok. Please select an account.", "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    GetAccountsList();
                }
            }, 2000);


        } else if (message.contains("atm locator") || message.contains("atm locations") || message.contains("atm location")) {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage("Ok. Please wait...", "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    Intent intent = new Intent(ChatMessengerScreen.this, ATMLocatorScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("chat", "chat");
                    startActivity(intent);

                }
            }, 2000);


        } else if (message.contains("contact us") || message.contains("contact center") || message.contains("samba contact")) {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage("Ok. Please wait...", "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    Intent intent = new Intent(ChatMessengerScreen.this, ContactUsScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("chat", "chat");
                    startActivity(intent);

                }
            }, 2000);


        } else if (message.contains("About this bot") || message.contains("about this bot") || message.toLowerCase().contains("about bot")) {

            relativeLayout2.setVisibility(View.VISIBLE);
            relativeLayout.setVisibility(View.GONE);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage("Here's what I can help you with:\n" +
                            "\n" +
                            " - View account balances and recent transaction\n" +
                            " - Move money within own account or send money to your  beneficaries within Samba or other bank\n" +
                            " - Find a bank branch or ATM \n" +
                            "-  Inquire about product\n" +
                            "\n" +
                            "You can type your questions, but using my quick reply and menu navigation is sometimes more reliable.\n" +
                            "\n" +
                            "\n" +
                            "I am still learning and i can provide you few features. But you can also access additional banking facilties quickly from burger menu on the top left of the application.", "");


                }
            }, 1000);


        }


    }

    public class CustomHorizontalListAdapter extends BaseAdapter {
        ArrayList<String> billerList;
        LayoutInflater layoutInflater;
        View row;
        Context context;
        TextView userName;


        public CustomHorizontalListAdapter(ArrayList<String> billerArrayList, Context mContext) {
            billerList = billerArrayList;
            context = mContext;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return billerList.size();
        }

        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.hori_list_item, parent, false);
            }


            userName = (TextView) convertView.findViewById(R.id.userName);
            userName.setText(billerList.get(position));


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tagshorizontalListViewLayout.setVisibility(View.GONE);
                    sendMessage(billerList.get(position));

                    isFt = true;
                    Handler handler2 = new Handler();
                    handler2.postDelayed(new Runnable() {
                        public void run() {
                            sendMessage("Ok. Please select a Source Account.", "");
                            relativeLayout2.setVisibility(View.VISIBLE);
                            relativeLayout.setVisibility(View.GONE);

                        }
                    }, 1000);

                    Handler handler1 = new Handler();
                    handler1.postDelayed(new Runnable() {
                        public void run() {


                            GetAccountsList();
                        }
                    }, 2000);


                }
            });


            return convertView;
        }


    }


    public class DestinationAccountHorizontalListAdapter extends BaseAdapter {
        ArrayList<UserModel> userModels;
        LayoutInflater layoutInflater;
        View row;
        Context context;
        TextView userName;
        TextView accountTitleTxt, accountNumberTxt, branchTxt, accountType, samba;

        public DestinationAccountHorizontalListAdapter(ArrayList<UserModel> userModelArrayList, Context mContext) {
            userModels = userModelArrayList;
            context = mContext;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return userModels.size();
        }

        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.account_list_item, parent, false);
            }

            samba = (TextView) convertView.findViewById(R.id.samba);
            accountTitleTxt = (TextView) convertView.findViewById(R.id.accountTitleTxt);
            accountNumberTxt = (TextView) convertView.findViewById(R.id.accountNumberTxt);
            branchTxt = (TextView) convertView.findViewById(R.id.branchTxt);
            accountType = (TextView) convertView.findViewById(R.id.accountType);
            accountType.setVisibility(View.INVISIBLE);
            samba.setText(userModels.get(position).getBankName());
            accountTitleTxt.setText(userModels.get(position).getAccountTitle());
            accountNumberTxt.setText(utils.getMaskedString(userModels.get(position).getAccountNumber()));
            branchTxt.setText(userModels.get(position).getBeneType());
//            accountType.setText(accountArrayList.get(position).getProductType());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    sendMessage(utils.getMaskedString(userModels.get(position).getAccountNumber()) + "\n" + userModels.get(position).getAccountTitle() + "\n" + userModels.get(position).getBankName());
                    horizontalListViewLayout.setVisibility(View.GONE);
                    relativeLayout2.setVisibility(View.VISIBLE);

                    Handler handler2 = new Handler();
                    handler2.postDelayed(new Runnable() {
                        public void run() {
                            sendMessage("Ok. Please enter an amount.", "");
                            relativeLayout2.setVisibility(View.VISIBLE);
                            relativeLayout.setVisibility(View.GONE);

                        }
                    }, 1000);

//                    Handler handler1 = new Handler();
//                    handler1.postDelayed(new Runnable() {
//                        public void run() {
//
//
//                            GetAccountsList();
//                        }
//                    }, 2000);


                }
            });


            return convertView;
        }


    }

    public class AccoutsCustomHorizontalListAdapter extends BaseAdapter {
        ArrayList<Account> accountArrayList;
        LayoutInflater layoutInflater;
        View row;
        Context context;
        TextView accountTitleTxt, accountNumberTxt, branchTxt, accountType;


        public AccoutsCustomHorizontalListAdapter(ArrayList<Account> accounts, Context mContext) {
            accountArrayList = accounts;
            context = mContext;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return accountArrayList.size();
        }

        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.account_list_item, parent, false);
            }


            accountTitleTxt = (TextView) convertView.findViewById(R.id.accountTitleTxt);
            accountNumberTxt = (TextView) convertView.findViewById(R.id.accountNumberTxt);
            branchTxt = (TextView) convertView.findViewById(R.id.branchTxt);
            accountType = (TextView) convertView.findViewById(R.id.accountType);

            accountTitleTxt.setText(accountArrayList.get(position).getAccountName());
            accountNumberTxt.setText(utils.getMaskedString(accountArrayList.get(position).getAccountNumber()));
            branchTxt.setText(accountArrayList.get(position).getBranchName());
            accountType.setText(accountArrayList.get(position).getProductType());


            final View finalConvertView = convertView;
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendMessage(utils.getMaskedString(accountArrayList.get(position).getAccountNumber()) + "\n" + accountArrayList.get(position).getAccountName() + "\n" + accountArrayList.get(position).getBranchName() + "\n" + accountArrayList.get(position).getProductType());
                    horizontalListViewLayout.setVisibility(View.GONE);
                    relativeLayout2.setVisibility(View.VISIBLE);
                    if (!isMini) {

                        if (isFt) {

                            isFt = false;
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {

                                    sendMessage("Ok. Please select a Destination Account.", "");
                                }
                            }, 1000);

                            Handler handler1 = new Handler();
                            handler1.postDelayed(new Runnable() {
                                public void run() {

                                    GetAccountsAndBeneList();
                                }
                            }, 2000);


                        } else {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {

                                    sendMessage("Ok. Just a second", "");
                                }
                            }, 700);

                            Handler handler1 = new Handler();
                            handler1.postDelayed(new Runnable() {
                                public void run() {

                                    LatestBalance(position);

                                }
                            }, 2000);
                        }
                    } else {


                        isMini = false;
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {

                                sendMessage("Ok. Just a second", "");
                            }
                        }, 700);

                        Handler handler1 = new Handler();
                        handler1.postDelayed(new Runnable() {
                            public void run() {

                                AccountDetails(accountArrayList.get(position).getAccountNumber(), accountArrayList.get(position).getBranchCode());

                            }
                        }, 2000);
                    }
                }
            });


            return convertView;
        }


    }

    public void GetAccountsList() {

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {
                //135821 ammar
                //181936 hasan
                //constants.sharedPreferences.getString("T24_CIF", "N/A")

                try {

                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAccountListURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("00")) {

                                            int count = 0;
                                            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("AccountList"));

                                            if (jsonArray.length() > 0) {

                                                accounts = new ArrayList<>();
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
                                                        Account account = new Account();
                                                        account.setAccountNumber(jsonObj.getString("AccountID"));
                                                        account.setAccountName(jsonObj.getString("AccountTitle"));
                                                        account.setUserName(jsonObj.getString("AccountTitle"));
                                                        account.setBranchName(jsonObj.getString("BranchName"));
                                                        account.setAccountType(jsonObj.getString("Type"));
                                                        account.setAvailBal(jsonObj.getString("AvailBal"));
                                                        account.setBankCode(jsonObj.getString("BankCode"));
                                                        account.setBankID(jsonObj.getString("BankCode"));
                                                        account.setBranchCode(jsonObj.getString("BranchCode"));
                                                        account.setCurrency(jsonObj.getString("Currency"));
                                                        account.setCurrencyType(jsonObj.getString("Currency"));
                                                        account.setProductType(jsonObj.getString("Product"));
                                                        account.setBankColor(jsonObj.getString("BankColorCode"));
                                                        account.setIsActive(jsonObj.getString("Active"));
                                                        account.setId(jsonObj.getString("ID"));
                                                        account.setBranchShortName(jsonObj.getString("BranchNameShort"));
                                                        accounts.add(account);

                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        pd.dismiss();
                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                    }

                                                }

                                                accoutsCustomHorizontalListAdapter = new AccoutsCustomHorizontalListAdapter(accounts, ChatMessengerScreen.this);
                                                accountListView.setAdapter(accoutsCustomHorizontalListAdapter);
                                                horizontalListViewLayout.setVisibility(View.VISIBLE);
                                                pd.dismiss();
                                            } else {
                                                pd.dismiss();

                                            }
                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "WARNING");
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                    }
                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("sService_Username", constants.signsUserName);
                            params.put("sService_Password", constants.signsPassword);
                            params.put("sIPAddress", Utils.getIPAddress(true));
                            params.put("sMacAddress", constants.address);
                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
                            params.put("sOS_Version", constants.version);
                            params.put("sDeviceName", constants.model);
                            params.put("sAppVersion", constants.appVersion);
                            params.put("sService_SharedKey", Constants.Shared_KEY);
                            params.put("sTimeStamp", constants.dateTime);
                            params.put("sDeviceLatitude", "0");
                            params.put("sDeviceLongitude", "0");
                            params.put("sT24_CIF", constants.sharedPreferences.getString("T24_CIF", "N/A"));
                            params.put("sSigns_UserType", constants.sharedPreferences.getString("type", "N/A"));
                            params.put("sIsAllAccounts", "1");
                            return params;
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                }

            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }

    public void GetAccountsAndBeneList() {

        try {

            pd.show();
            if (helper.isNetworkAvailable()) {

                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAccountNBenelistURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Status_Code").equals("00")) {

                                        try {

                                            JSONArray beneArray = new JSONArray(jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Local_BenefeciaryList"));

                                            if (beneArray.length() > 0) {

                                                userModels = new ArrayList<>();

                                                for (int i = 0; i < beneArray.length(); i++) {

                                                    try {


                                                        JSONObject jsonObj = new JSONObject(beneArray.get(i).toString());


                                                        UserModel userModel = new UserModel();
                                                        userModel.setBankName(jsonObj.getString("Bene_BankName"));
                                                        userModel.setAccountNumber(jsonObj.getString("Bene_AccountNumber"));
                                                        userModel.setUserName(jsonObj.getString("Bene_Alias"));
                                                        userModel.setBankCode(jsonObj.getString("Bene_BankCode"));
                                                        userModel.setCurrency(jsonObj.getString("Bene_CurrencyCode"));
                                                        userModel.setBeneType(jsonObj.getString("Bene_Type"));
                                                        userModel.setUserTitle(jsonObj.getString("Bene_AccountTitle"));
                                                        userModel.setMobileNumber(jsonObj.getString("Bene_Mobile"));
                                                        userModel.setEmail(jsonObj.getString("Bene_Email"));
                                                        userModel.setBeneID(jsonObj.getString("Bene_ID"));
                                                        userModel.setAccountTitle(jsonObj.getString("Bene_AccountTitle"));
                                                        String firstName = jsonObj.getString("Bene_Alias");
                                                        String s = firstName.substring(0, 1);
                                                        userModel.setFirstLetter(s.toUpperCase());
                                                        userModels.add(userModel);


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        pd.dismiss();
                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                                    }

                                                }


                                                destinationAccountHorizontalListAdapter = new DestinationAccountHorizontalListAdapter(userModels, ChatMessengerScreen.this);
                                                accountListView.setAdapter(destinationAccountHorizontalListAdapter);
                                                horizontalListViewLayout.setVisibility(View.VISIBLE);
                                                relativeLayout2.setVisibility(View.GONE);
                                                relativeLayout.setVisibility(View.GONE);
                                                pd.dismiss();
                                            } else {

                                                pd.dismiss();

                                            }

                                        } catch (JSONException ex) {

                                            ex.printStackTrace();
                                            pd.dismiss();
                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForError(jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Status_Description"), "ERROR");
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {

                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("sService_Username", constants.signsUserName);
                        params.put("sService_Password", constants.signsPassword);
                        params.put("sIPAddress", Utils.getIPAddress(true));
                        params.put("sMacAddress", constants.address);
                        params.put("sOS_Version", constants.version);
                        params.put("sDeviceName", constants.model);
                        params.put("sAppVersion", constants.appVersion);
                        params.put("sService_SharedKey", Constants.Shared_KEY);
                        params.put("sTimeStamp", constants.dateTime);
                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
                        params.put("sT24_CIF", constants.sharedPreferences.getString("T24_CIF", "N/A"));

                        return params;
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }

    public void AccountDetails(final String accountNumber, final String branchCode) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();


                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.miniStatementURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("Status_Code").equals("00")) {

                                        JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("MiniStatement"));
                                        String checkDate;
                                        int count = 0;
                                        if (jsonArray.length() > 0) {

                                            sameDate = new ArrayList<>();
                                            transactionDetailsList = new ArrayList<>();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                try {
                                                    JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                    TransactionDetails transactionDetails = new TransactionDetails();
                                                    transactionDetails.setTitle(jsonObj.getString("Description"));
                                                    Long date = Long.valueOf(jsonObj.getString("DateTime"));
                                                    String value = jsonObj.getString("ValueDate");
                                                    String year = value.substring(0, 4);
                                                    String month = value.substring(4, 6);
                                                    String datetype = value.substring(6, 8);
                                                    String formatedDate = year + "-" + month + "-" + datetype;
                                                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                                                    Date mDate;
                                                    long timeInMilliseconds = 0;
                                                    try {

                                                        mDate = sdf1.parse(formatedDate);
                                                        timeInMilliseconds = mDate.getTime();

                                                    } catch (Exception e) {

                                                        e.printStackTrace();
                                                    }

                                                    Long valueDate = Long.valueOf(jsonObj.getString("ValueDate"));
                                                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                                                    String date1 = getDate(date, "dd-MMM-yyyy");
                                                    transactionDetails.setDate(date1);
                                                    transactionDetails.setCurrency(jsonObj.getString("Currency"));
                                                    transactionDetails.setAccNumber(jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("AccountID"));
                                                    transactionDetails.setValueDate(getDate(timeInMilliseconds, "dd-MMM-yyyy"));
                                                    transactionDetails.setAfterAmount(jsonObj.getString("Balance"));
                                                    transactionDetails.setDeposit(jsonObj.getString("Deposits"));
                                                    transactionDetails.setWithDraw(jsonObj.getString("Withdrawals"));
                                                    transactionDetails.setRequestNumber(jsonObj.getString("Reference"));
                                                    checkDate = getDate(timeInMilliseconds, "dd-MMM-yyyy");
                                                    if (count == 0) {
                                                        if (transactionDetails.getRequestNumber().startsWith("FT")) {
                                                            if (!TextUtils.isEmpty(transactionDetails.getWithDraw())) {

                                                                sameDate.add("FT DEBIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getWithDraw() + " on " + transactionDetails.getValueDate());
                                                            } else {

                                                                sameDate.add("FT CREDIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getDeposit() + " on " + transactionDetails.getValueDate());

                                                            }
                                                        } else {

                                                            if (!TextUtils.isEmpty(transactionDetails.getWithDraw())) {

                                                                sameDate.add("IBFT DEBIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getWithDraw() + " on " + transactionDetails.getValueDate());
                                                            } else {

                                                                sameDate.add("IBFT CREDIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getDeposit() + " on " + transactionDetails.getValueDate());

                                                            }
                                                        }

                                                    } else {

                                                        if (transactionDetailsList.get(0).getValueDate().equals(transactionDetails.getValueDate())) {


                                                            if (transactionDetails.getRequestNumber().startsWith("FT")) {
                                                                if (!TextUtils.isEmpty(transactionDetails.getWithDraw())) {

                                                                    sameDate.add("FT DEBIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getWithDraw() + " on " + transactionDetails.getValueDate());
                                                                } else {

                                                                    sameDate.add("FT CREDIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getDeposit() + " on " + transactionDetails.getValueDate());

                                                                }
                                                            } else {

                                                                if (!TextUtils.isEmpty(transactionDetails.getWithDraw())) {

                                                                    sameDate.add("IBFT DEBIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getWithDraw() + " on " + transactionDetails.getValueDate());
                                                                } else {

                                                                    sameDate.add("IBFT CREDIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getDeposit() + " on " + transactionDetails.getValueDate());

                                                                }
                                                            }
                                                        }

                                                    }
                                                    transactionDetailsList.add(transactionDetails);
                                                    count++;
                                                } catch (JSONException ex) {

                                                    ex.printStackTrace();
                                                    pd.dismiss();
                                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                                }

                                            }
                                            pd.dismiss();

                                            String combined = "";
                                            int sameCount = 0;
                                            for (int i = 0; i < sameDate.size(); i++) {

                                                if (sameCount == 0) {

                                                    combined = sameDate.get(i);
                                                    sameCount++;
                                                } else {


                                                    combined = combined + "\n" + sameDate.get(i);
                                                }

                                            }

                                            sendMessage("Ok, here are your most recent transaction\n" +
                                                    "of  " + accountNumber + "\n\n" + combined, "");
                                        } else {

                                            pd.dismiss();
                                        }

                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForError(jsonObject.getJSONObject("Signs_T24MiniStatement").getString("Status_Decription"), "ERROR");


                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("sService_Username", constants.signsUserName);
                        params.put("sService_Password", constants.signsPassword);
                        params.put("sIPAddress", Utils.getIPAddress(true));
                        params.put("sMacAddress", constants.address);
                        params.put("sOS_Version", constants.version);
                        params.put("sDeviceName", constants.model);
                        params.put("sAppVersion", constants.appVersion);
                        params.put("sDeviceLatitude", "0");
                        params.put("sDeviceLongitude", "0");
                        params.put("sService_SharedKey", Constants.Shared_KEY);
                        params.put("sTimeStamp", constants.dateTime);
                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
                        params.put("sT24_AccountNumber", accountNumber);
                        params.put("sBranchCode", branchCode);


                        return params;
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                pd.dismiss();
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");

            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");

        }
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public void LatestBalance(final int position) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();
                try {

                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getLatestBalURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code").equals("00")) {


                                            pd.dismiss();
//                                            int amount = Integer.valueOf(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"));
                                            sendMessage("Balance of a/c is " + accounts.get(position).getCurrency() + " " + jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"), "");

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Description"), "ERROR");

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("sService_Username", constants.signsUserName);
                            params.put("sService_Password", constants.signsPassword);
                            params.put("sIPAddress", Utils.getIPAddress(true));
                            params.put("sMacAddress", constants.address);
                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
                            params.put("sOS_Version", constants.version);
                            params.put("sDeviceName", constants.model);
                            params.put("sAppVersion", constants.appVersion);
                            params.put("sService_SharedKey", Constants.Shared_KEY);
                            params.put("sTimeStamp", constants.dateTime);
                            params.put("sT24_AccountNumber", accounts.get(position).getAccountNumber());
                            return params;
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }

            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    private String getFormatedAmount(int amount) {
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }


}
