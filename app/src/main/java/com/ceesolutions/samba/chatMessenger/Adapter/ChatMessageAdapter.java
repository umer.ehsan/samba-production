package com.ceesolutions.samba.chatMessenger.Adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.chatMessenger.views.ChatMessageView;
import com.ceesolutions.samba.chatMessenger.Model.ChatMessage;
import com.ceesolutions.samba.chatMessenger.views.ChatMessageViewLeft;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.views.RoundImageView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.MessageHolder> {

    private static final int MY_MESSAGE = 0, OTHER_MESSAGE = 1;
    private final String TAG = "ChatMessageAdapter";
    private Constants constants;
    private List<ChatMessage> mMessages;
    private Context mContext;
    private int color;

    public ChatMessageAdapter(Context context, List<ChatMessage> data) {
        mContext = context;
        mMessages = data;
        constants = Constants.getConstants(context);

    }

    @Override
    public int getItemCount() {
        return mMessages == null ? 0 : mMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        ChatMessage item = mMessages.get(position);

        if (item.isMine()) return MY_MESSAGE;
        else return OTHER_MESSAGE;
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == MY_MESSAGE) {
            return new MessageHolder(LayoutInflater.from(mContext).inflate(R.layout.item_mine_message, parent, false));
        } else {
            return new MessageHolder(LayoutInflater.from(mContext).inflate(R.layout.item_other_message, parent, false));
        }
    }

    public void add(ChatMessage message) {
        mMessages.add(message);
        notifyItemInserted(mMessages.size() - 1);
    }

    @Override
    public void onBindViewHolder(final MessageHolder holder, final int position) {
        ChatMessage chatMessage = mMessages.get(position);

        try {

            CryptLib _crypt = new CryptLib();
            if (chatMessage.isImage()) {
//            holder.ivImage.setVisibility(View.VISIBLE);
                holder.tvMessage.setVisibility(View.VISIBLE);
                String date = new SimpleDateFormat("hh:mm aa", Locale.getDefault()).format(new Date());
                holder.tvTime.setText(date);
                holder.tvMessage.setText(chatMessage.getContent());
//            holder.sambaImage.setImageResource(R.drawable.samba);
            } else {
//            holder.ivImage.setVisibility(View.GONE);
                holder.tvMessage.setVisibility(View.VISIBLE);
                String T24_FullName = _crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                String s = T24_FullName.substring(0, 1).toUpperCase();
                switch (s.toUpperCase()) {

                    case "A":
                        color = mContext.getResources().getColor(R.color.friendColor1);
                        break;

                    case "B":
                        color = mContext.getResources().getColor(R.color.friendColor1);
                        break;

                    case "C":
                        color = mContext.getResources().getColor(R.color.friendColor1);
                        break;

                    case "D":
                        color = mContext.getResources().getColor(R.color.friendColor1);
                        break;

                    case "E":
                        color = mContext.getResources().getColor(R.color.friendColor1);
                        break;

                    case "F":
                        color = mContext.getResources().getColor(R.color.friendColor1);
                        break;

                    case "G":
                        color = mContext.getResources().getColor(R.color.friendColor1);
                        break;

                    case "H":
                        color = mContext.getResources().getColor(R.color.friendColor2);
                        break;

                    case "I":
                        color = mContext.getResources().getColor(R.color.friendColor2);
                        break;

                    case "J":
                        color = mContext.getResources().getColor(R.color.friendColor2);
                        break;

                    case "K":
                        color = mContext.getResources().getColor(R.color.friendColor2);
                        break;

                    case "L":
                        color = mContext.getResources().getColor(R.color.friendColor2);
                        break;

                    case "M":
                        color = mContext.getResources().getColor(R.color.friendColor2);
                        break;

                    case "N":
                        color = mContext.getResources().getColor(R.color.friendColor3);
                        break;

                    case "O":
                        color = mContext.getResources().getColor(R.color.friendColor3);
                        break;

                    case "P":
                        color = mContext.getResources().getColor(R.color.friendColor3);
                        break;

                    case "Q":
                        color = mContext.getResources().getColor(R.color.friendColor3);
                        break;

                    case "R":
                        color = mContext.getResources().getColor(R.color.friendColor3);
                        break;

                    case "S":
                        color = mContext.getResources().getColor(R.color.friendColor3);
                        break;

                    case "T":
                        color = mContext.getResources().getColor(R.color.friendColor4);
                        break;

                    case "U":
                        color = mContext.getResources().getColor(R.color.friendColor4);
                        break;

                    case "V":
                        color = mContext.getResources().getColor(R.color.friendColor4);
                        break;

                    case "W":
                        color = mContext.getResources().getColor(R.color.friendColor4);
                        break;

                    case "X":
                        color = mContext.getResources().getColor(R.color.friendColor4);
                        break;

                    case "Y":
                        color = mContext.getResources().getColor(R.color.friendColor4);
                        break;

                    case "Z":
                        color = mContext.getResources().getColor(R.color.friendColor4);
                        break;

                    default:
                        color = mContext.getResources().getColor(R.color.defaultColor);
                        break;
                }

                holder.tvMessage.setText(chatMessage.getContent());
                String date = new SimpleDateFormat("hh:mm aa", Locale.getDefault()).format(new Date());
                holder.tvTime.setText(date);
                String image = _crypt.decrypt(constants.sharedPreferences.getString("ProfilePic", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                try {

                    if (!TextUtils.isEmpty(image) && !image.equals("N/A")) {
                        image = image.replaceAll("%2B", "+");
                        byte[] imageByteArray = Base64.decode(image, Base64.DEFAULT);
                        RequestOptions options = new RequestOptions();
                        options.centerCrop();
                        Glide.with(mContext)
                                .load(imageByteArray)
                                .apply(options)
                                .into(holder.ivImage);
                        holder.ivImage.setVisibility(View.VISIBLE);
                        holder.titleImage.setVisibility(View.INVISIBLE);
                        holder.titleTxt.setVisibility(View.INVISIBLE);
                    } else {

                        holder.ivImage.setVisibility(View.INVISIBLE);
                        holder.titleImage.setVisibility(View.VISIBLE);
                        holder.titleTxt.setVisibility(View.VISIBLE);
                        holder.titleTxt.setText(s);
                        holder.titleImage.setBackgroundTintList(ColorStateList.valueOf((color)));
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


//        holder.chatMessageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
//
//        holder.chatMessageViewLeft.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
    }

    class MessageHolder extends RecyclerView.ViewHolder {
        TextView tvMessage, tvTime, titleTxt;
        ImageView ivImage;
        ChatMessageView chatMessageView;
        ChatMessageViewLeft chatMessageViewLeft;
        ImageView sambaImage;
        RoundImageView titleImage;


        MessageHolder(View itemView) {
            super(itemView);
            chatMessageView = (ChatMessageView) itemView.findViewById(R.id.chatMessageView);
            chatMessageViewLeft = (ChatMessageViewLeft) itemView.findViewById(R.id.chatMessageViewleft);
            tvMessage = (TextView) itemView.findViewById(R.id.tv_message);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            ivImage = (ImageView) itemView.findViewById(R.id.image);
            sambaImage = (ImageView) itemView.findViewById(R.id.image1);
            titleTxt = (TextView) itemView.findViewById(R.id.titleTxt);
            titleImage = (RoundImageView) itemView.findViewById(R.id.title);


        }
    }
}