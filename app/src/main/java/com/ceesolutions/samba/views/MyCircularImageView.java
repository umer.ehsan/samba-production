package com.ceesolutions.samba.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

/**
 * Created by ceeayaz on 5/8/18.
 */

public class MyCircularImageView extends androidx.appcompat.widget.AppCompatImageView {

    private Paint paint;

    public MyCircularImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.RED);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        Drawable drawable = getDrawable();
        if (drawable != null && drawable instanceof BitmapDrawable)
        {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;

            Bitmap bitmap = bitmapDrawable.getBitmap();
            canvas.saveLayer(0,0,getWidth(),getHeight(),null,0);
            int bitmapWidth = bitmap.getWidth();
            int bitmapHeight = bitmap.getHeight();
            int minR = (bitmapHeight<bitmapWidth?bitmapHeight:bitmapWidth)/2;
            canvas.drawCircle(bitmapWidth/2,bitmapHeight/2,minR,paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

            //            canvas.drawBitmap(bitmap,0,0,paint);
            Rect srcRect = new Rect(0,0,bitmapWidth,bitmapHeight);
            Rect dstRect = new Rect(bitmapWidth/2-minR,0,bitmapWidth/2+minR,bitmapHeight);

            canvas.drawBitmap(bitmap,srcRect,dstRect,paint);

            paint.setXfermode(null);

            canvas.restore();

        }else {
            super.onDraw(canvas);
        }
    }
}
