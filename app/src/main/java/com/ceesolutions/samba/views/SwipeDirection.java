package com.ceesolutions.samba.views;

/**
 * Created by ayaz on 1/11/18.
 */

public enum SwipeDirection {
    all, left, right, none ;
}
