package com.ceesolutions.samba.stix;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.ceesolutions.samba.R;

import java.util.List;

public class MainActivity extends Activity implements FetchInstalledAppsTask.FetchInstalledAppsTaskListener {
    private List<AppInfo> apps;
    protected GridView aGridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitymain);

        initiateInstalledAppsFetch();
    }

    private void initiateInstalledAppsFetch() {
        FetchInstalledAppsTask fetchTask = new FetchInstalledAppsTask(this);
        fetchTask.execute(1);
    }

    public void handleInstalledAppsFetch(List<AppInfo> apps) {
        this.apps = apps;

        setUpGridView();
    }

    public void setUpGridView() {
        aGridView = (GridView) this.findViewById(R.id.app_gridview);

        aGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                AppInfo selectedApp = apps.get(position);
                selectedApp.launch(getApplicationContext());
            }
        });

        GridArrayAdapter gAdapter = new GridArrayAdapter(this, apps);
        aGridView.setAdapter(gAdapter);

        int cols = (int) Math.ceil((double) this.apps.size() / 3);
        aGridView.setNumColumns(cols);
    }
}
