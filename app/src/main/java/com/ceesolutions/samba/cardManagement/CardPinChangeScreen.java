package com.ceesolutions.samba.cardManagement;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.cardManagement.Model.Card;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.CircleIndicator;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ceeayaz on 4/18/18.
 */

public class CardPinChangeScreen extends AppCompatActivity {

    private static final Integer[] IMAGES = {R.drawable.group_8, R.drawable.group_8};
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private Dialog pd, dialog;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Utils utils;
    private CircleIndicator indicator;
    private ViewPager mPager, pagerNonSamba;
    private ArrayList<Card> cards;
    private ArrayList<Integer> ImagesArray;
    private ImageView backBtn, settings;
    private ImageView notificationBtn;
    private TextView doneBtn, message, heading, validateBtn, errorMessageForOldPin, errorMessageForNewPin, holderName, cardStatus, accountNumberTxt, errorMessageForConfirmPin;
    private String type, stan, rrn, oldPin, newPin, confirmPin, currentStatus, accountNumber, accountTitle, cardType, cardStatus1;
    private EditText chatBotText;
    private ArrayList<String> statusArraylist;
    private EditText editReason, editOldPin, editNewPin, editConfirmPin;
    private Spinner statusSpinner;
    private String[] status;
    private String reason, finalDate, currentTime;
    private RelativeLayout main;
    private Intent intent1;
    private Date currentDate;
    private AppPreferences appPreferences;
    private String location;
    private String oldPinEncrypted = "";
    private String newPinEncrypted = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_pin);
        initViews();
        getSupportActionBar().hide();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editConfirmPin.getWindowToken(), 0);
                InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm1.hideSoftInputFromWindow(editOldPin.getWindowToken(), 0);
                InputMethodManager imm2 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm2.hideSoftInputFromWindow(editNewPin.getWindowToken(), 0);
            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(CardPinChangeScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAnalytics.getInstance(CardPinChangeScreen.this).logEvent("Card_Pin_Update_Pressed", new Bundle());

                newPin = editNewPin.getText().toString();
                oldPin = editOldPin.getText().toString();
                confirmPin = editConfirmPin.getText().toString();


                if (TextUtils.isEmpty(newPin) || newPin.length() < 4) {

                    errorMessageForNewPin.setVisibility(View.VISIBLE);
                    errorMessageForNewPin.bringToFront();
                    errorMessageForNewPin.setError("");
                    errorMessageForNewPin.setText("Please enter valid pin");

                } else if (!helper.validateInputForSC(newPin) || newPin.contains(" ")) {

                    errorMessageForNewPin.setVisibility(View.VISIBLE);
                    errorMessageForNewPin.bringToFront();
                    errorMessageForNewPin.setError("");
                    errorMessageForNewPin.setText("New pin cannot contains <,>,\",',%,(,),&,+,\\,~");


                }

                if (TextUtils.isEmpty(oldPin) || oldPin.length() < 4) {

                    errorMessageForOldPin.setVisibility(View.VISIBLE);
                    errorMessageForOldPin.bringToFront();
                    errorMessageForOldPin.setError("");
                    errorMessageForOldPin.setText("Please enter valid pin");

                } else if (!helper.validateInputForSC(oldPin) || oldPin.contains(" ")) {

                    errorMessageForOldPin.setVisibility(View.VISIBLE);
                    errorMessageForOldPin.bringToFront();
                    errorMessageForOldPin.setError("");
                    errorMessageForOldPin.setText("Old pin cannot contains <,>,\",',%,(,),&,+,\\,~");


                }


                if (TextUtils.isEmpty(confirmPin) || confirmPin.length() < 4) {

                    errorMessageForConfirmPin.setVisibility(View.VISIBLE);
                    errorMessageForConfirmPin.bringToFront();
                    errorMessageForConfirmPin.setError("");
                    errorMessageForConfirmPin.setText("Please enter valid pin");

                } else if (!helper.validateInputForSC(confirmPin) || confirmPin.contains(" ")) {

                    errorMessageForConfirmPin.setVisibility(View.VISIBLE);
                    errorMessageForConfirmPin.bringToFront();
                    errorMessageForConfirmPin.setError("");
                    errorMessageForConfirmPin.setText("Confirm pin cannot contains <,>,\",',%,(,),&,+,\\,~");


                }


                if (oldPin.equals(newPin)) {

                    utils.showDilogForError("Old Pin and New Pin cannot be same.", "ERROR");
                } else if (!newPin.equals(confirmPin)) {

                    utils.showDilogForError("New Pin and Confirm Pin does not match.", "ERROR");
                }


                if ((!TextUtils.isEmpty(newPin) && helper.validateInputForSC(newPin) && !newPin.contains(" ") && newPin.length() == 4) && (!TextUtils.isEmpty(oldPin) && helper.validateInputForSC(oldPin) && !oldPin.contains(" ") && oldPin.length() == 4) && (!TextUtils.isEmpty(confirmPin) && helper.validateInputForSC(confirmPin) && !confirmPin.contains(" ") && confirmPin.length() == 4) && !oldPin.equals(newPin) && newPin.equals(confirmPin)) {

                    validateBtn.setEnabled(false);
                    GenerateiPIN();

                }
            }
        });

        editOldPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessageForOldPin.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessageForOldPin.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

                oldPin = s.toString();

            }
        });

        editNewPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessageForNewPin.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessageForNewPin.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

                newPin = s.toString();

            }
        });


        editConfirmPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessageForConfirmPin.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessageForConfirmPin.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

                confirmPin = s.toString();

            }
        });


    }


    public void initViews() {

        try {

            CryptLib _crypt = new CryptLib();
            Intent intent = getIntent();

            intent1 = intent;
            currentStatus = intent.getStringExtra("status");
            accountNumber = intent.getStringExtra("accountNumber");
            accountTitle = intent.getStringExtra("title");
            cardType = intent.getStringExtra("cardType");
            cardStatus1 = intent.getStringExtra("cardStatus");


            main = (RelativeLayout) findViewById(R.id.main);

            if (cardType.contains("Gold")) {

                main.setBackgroundResource(R.drawable.group_12);
            }else if (cardType.contains("PayPak")) {

                main.setBackgroundResource(R.drawable.paypak);
            }
            statusArraylist = new ArrayList<>();
            helper = Helper.getHelper(this);
            validateBtn = (TextView) findViewById(R.id.reviewBtn);
            requestQueue = Volley.newRequestQueue(this);
            constants = Constants.getConstants(CardPinChangeScreen.this);
            utils = new Utils(CardPinChangeScreen.this);
            notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
            settings = (ImageView) findViewById(R.id.settings);
            pd = new Dialog(CardPinChangeScreen.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        chatBotText = (EditText) findViewById(R.id.chatBoxText);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            statusSpinner = (Spinner) findViewById(R.id.statusSpinner);
            backBtn = (ImageView) findViewById(R.id.backBtn);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            int stanNumber = 6;
            stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
            int rrnNumber = 12;
            rrn = String.valueOf(helper.generateRandom(rrnNumber));
            editOldPin = (EditText) findViewById(R.id.editOldPin);
            editNewPin = (EditText) findViewById(R.id.editNewPin);
            editConfirmPin = (EditText) findViewById(R.id.editConfirmPin);
            errorMessageForOldPin = (TextView) findViewById(R.id.errorMessageForOldPin);
            errorMessageForConfirmPin = (TextView) findViewById(R.id.errorMessageForConfirmPin);
            errorMessageForNewPin = (TextView) findViewById(R.id.errorMessageForNewPin);
            cardStatus = (TextView) findViewById(R.id.cardStatus);
            holderName = (TextView) findViewById(R.id.holderName);
            accountNumberTxt = (TextView) findViewById(R.id.accountNumber);
            String firstFour = accountNumber.substring(0, 4);
            String lastFour = accountNumber.substring(12, 16);
            String secondFour = accountNumber.substring(4, 8);
            String thirdFour = accountNumber.substring(8, 12);
            cardStatus.setText(cardStatus1.toUpperCase());
            holderName.setText(accountTitle);
            accountNumberTxt.setText(firstFour + "    " + secondFour + "    " + thirdFour + "    " + lastFour);


            type = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            String list = constants.status.getString("statusList", "N/A");
//        chatBotText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                showDilogForErrorForLogin("Do you want to cancel this activity?", "WARNING");
//            }
//        });
            DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.US);
            currentDate = Calendar.getInstance().getTime();
            formatter = new SimpleDateFormat("dd-MMM-yyyy");
            finalDate = formatter.format(currentDate);
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
            currentTime = sdf.format(currentDate);

            appPreferences = new AppPreferences(CardPinChangeScreen.this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showDilogForErrorForLogin(String msg, String header) {

        dialog = new Dialog(CardPinChangeScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();


    }

    public void GenerateiPIN() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                try {
                    CryptLib _crypt = new CryptLib();


                    String Id = _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    Log.d("Key", "---" + utils.createKey(Id, constants.Shared_KEY));
                    Log.d("IV", "---" + utils.createIV(Id, constants.Shared_KEY));
                    oldPinEncrypted = _crypt.encrypt(oldPin, utils.createKey(Id, constants.Shared_KEY), utils.createIV(Id, constants.Shared_KEY)); //encrypt
//                    Log.d("oldPinEncrypted=", "" + oldPinEncrypted);
                    newPinEncrypted = _crypt.encrypt(newPin, utils.createKey(Id, constants.Shared_KEY), utils.createIV(Id, constants.Shared_KEY)); //encrypt
//                    Log.d("newPinEncrypted=", "" + newPinEncrypted);

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Username", user);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                HttpsTrustManager.allowMySSL(CardPinChangeScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);

                                    if (jsonObject.getString("ServerKey").equals(md5)) {


                                        if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("00")) {


                                            pd.dismiss();
                                            Intent intent = new Intent(CardPinChangeScreen.this, PinVerificationScreen.class);
                                            overridePendingTransition(0, 0);
                                            intent.putExtra("accountNumber", accountNumber);
                                            intent.putExtra("title", accountTitle);
                                            intent.putExtra("cardType", cardType);
                                            intent.putExtra("stan", stan);
                                            intent.putExtra("rrn", rrn);
                                            intent.putExtra("oldPin", oldPinEncrypted);
                                            intent.putExtra("newPin", newPinEncrypted);
                                            intent.putExtra("finalDate", finalDate);
                                            intent.putExtra("currentTime", currentTime);
                                            intent.putExtra("changePin", "changePin");
                                            overridePendingTransition(0, 0);
                                            startActivity(intent);
                                            validateBtn.setEnabled(true);
                                            clearViews();

                                        } else if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR", CardPinChangeScreen.this, new LoginScreen());
                                            validateBtn.setEnabled(true);
                                            clearViews();

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                            clearViews();
                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                        clearViews();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    clearViews();

                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    clearViews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    clearViews();
                                }
                            }
                        }
                )
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();


            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
                clearViews();

            }
        } catch (Exception e) {
            pd.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            validateBtn.setEnabled(true);
            clearViews();


        }
    }


    public void clearViews() {

        editConfirmPin.setText("");
        editNewPin.setText("");
        editOldPin.setText("");
    }

}
