package com.ceesolutions.samba.cardManagement.Model;

import proguard.annotation.Keep;

/**
 * Created by ceeayaz on 4/17/18.
 */
@Keep
public class Card {

    private String cardActivationStatus;
    private String cardNumber;
    private String cardProduct;
    private String cardStatus;
    private String cardTitle;
    private String cardType;
    private String cardExpiryDate;
    private String cardIssueDate;
    private String linkedAccounts;
    private String isNrp;

    public String getIsNrp() {
        return isNrp;
    }

    public void setIsNrp(String isNrp) {
        this.isNrp = isNrp;
    }

    public String getCardExpiryDate() {
        return cardExpiryDate;
    }

    public void setCardExpiryDate(String cardExpiryDate) {
        this.cardExpiryDate = cardExpiryDate;
    }

    public String getCardIssueDate() {
        return cardIssueDate;
    }

    public void setCardIssueDate(String cardIssueDate) {
        this.cardIssueDate = cardIssueDate;
    }

    public String getLinkedAccounts() {
        return linkedAccounts;
    }

    public void setLinkedAccounts(String linkedAccounts) {
        this.linkedAccounts = linkedAccounts;
    }

    public String getCardActivationStatus() {
        return cardActivationStatus;
    }

    public void setCardActivationStatus(String cardActivationStatus) {
        this.cardActivationStatus = cardActivationStatus;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardProduct() {
        return cardProduct;
    }

    public void setCardProduct(String cardProduct) {
        this.cardProduct = cardProduct;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getCardTitle() {
        return cardTitle;
    }

    public void setCardTitle(String cardTitle) {
        this.cardTitle = cardTitle;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
}
