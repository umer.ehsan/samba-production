package com.ceesolutions.samba.cardManagement.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcelable;
import androidx.viewpager.widget.PagerAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.cardManagement.CardPinChangeScreen;
import com.ceesolutions.samba.cardManagement.CardPosActivitation;
import com.ceesolutions.samba.cardManagement.CardStatusScreen;
import com.ceesolutions.samba.cardManagement.ECommerceActivation;
import com.ceesolutions.samba.cardManagement.InternationalTransactions;
import com.ceesolutions.samba.cardManagement.Model.Card;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/4/18.
 */

public class SlidingAdapter extends PagerAdapter {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private ArrayList<Integer> IMAGES;
    private LayoutInflater inflater;
    private Context mContext;
    ArrayList<Card> cards;
    private RequestQueue requestQueue;
    private AppPreferences appPreferences;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    String type;
    private Dialog pd;
    private Utils utils;
    private TextView issueTxt, expiryTxt, cardTxt, linkTxt, accountNumber, cardStatus, holderName, changeStatus, changePin, cardStatusTxt, more;
    private String userType;
    private RelativeLayout main;
    private BoomMenuButton bmb;
    public int pos = 0;
    private String location;

    public SlidingAdapter(Context context, ArrayList<Integer> IMAGES, ArrayList<Card> cardsArrayList) {

        this.IMAGES = IMAGES;
        mContext = context;
        cards = cardsArrayList;
        inflater = LayoutInflater.from(context);
        constants = Constants.getConstants(context);
        requestQueue = Volley.newRequestQueue(context);
        utils = new Utils(context);
        helper = Helper.getHelper(context);
        pd = new Dialog(context);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        appPreferences = new AppPreferences(context);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return cards.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        final View convertView = inflater.inflate(R.layout.card_item, view, false);
        try {

            assert convertView != null;
            main = (RelativeLayout) convertView
                    .findViewById(R.id.main);


            if (cards.get(position).getCardType().contains("Gold")) {

                main.setBackgroundResource(R.drawable.group_12);

            } else if (cards.get(position).getCardType().contains("PayPak")) {

                main.setBackgroundResource(R.drawable.paypak);
            }

            issueTxt = (TextView) convertView.findViewById(R.id.issueTxt);
            bmb = (BoomMenuButton) convertView.findViewById(R.id.listBoomMenu);
            assert bmb != null;
            bmb.setButtonEnum(ButtonEnum.Ham);
            bmb.setBackgroundColor(Color.TRANSPARENT);
            bmb.setPiecePlaceEnum(PiecePlaceEnum.HAM_3);
            bmb.setButtonPlaceEnum(ButtonPlaceEnum.HAM_3);
            expiryTxt = (TextView) convertView.findViewById(R.id.expiryTxt);
            cardTxt = (TextView) convertView.findViewById(R.id.cardTxt);
            linkTxt = (TextView) convertView.findViewById(R.id.linkTxt);
            accountNumber = (TextView) convertView.findViewById(R.id.accountNumber);
            cardStatus = (TextView) convertView.findViewById(R.id.cardStatus);
            holderName = (TextView) convertView.findViewById(R.id.holderName);
            changeStatus = (TextView) convertView.findViewById(R.id.changeStatus);
            changePin = (TextView) convertView.findViewById(R.id.changePin);
            cardStatusTxt = (TextView) convertView.findViewById(R.id.cardStatusTxt);
            more = (TextView) convertView.findViewById(R.id.more);
            accountNumber.setText(cards.get(position).getCardNumber());
            cardTxt.setText(cards.get(position).getCardType());
            holderName.setText(cards.get(position).getCardTitle());
            linkTxt.setText(cards.get(position).getLinkedAccounts());
            expiryTxt.setText(cards.get(position).getCardExpiryDate());
            issueTxt.setText(cards.get(position).getCardIssueDate());
            cardStatus.setText(cards.get(position).getCardStatus().toUpperCase());
            if (cards.get(position).getCardActivationStatus().equals("Active")) {
                cardStatusTxt.setText("Yes");
            } else {

                cardStatusTxt.setText("No");
            }


            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pos = position;
                    bmb.boom();
                }
            });

            bmb.setOnBoomListener(new OnBoomListener() {
                @Override
                public void onClicked(int index, BoomButton boomButton) {
                    // If you have implement listeners for boom-buttons in builders,
                    // then you shouldn't add any listener here for duplicate callbacks.
                    try {
                        pos = position;
                        switch (index) {


                            case 0:
                                try {
                                    if (cards.get(position).getCardStatus().toUpperCase().equals("HOT")) {

                                        utils.showDilogForError("POS status cannot be changed on BLOCKED Card.", "ERROR");

                                    } else {
                                        Intent intent = new Intent(mContext, CardPosActivitation.class);
                                        intent.putExtra("title", cards.get(pos).getCardTitle());
                                        intent.putExtra("status", cards.get(pos).getCardActivationStatus());
                                        intent.putExtra("accountNumber", cards.get(pos).getCardNumber());
                                        intent.putExtra("cardType", cards.get(pos).getCardType());
                                        intent.putExtra("cardStatus", cards.get(pos).getCardStatus());
                                        intent.putExtra("cardPos", cards.get(pos).getCardProduct());
                                        intent.putExtra("linkedAccounts", cards.get(pos).getLinkedAccounts());
                                        ((Activity) mContext).overridePendingTransition(0, 0);
                                        ((Activity) mContext).startActivity(intent);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;

                            case 1:
                                try {
                                    if (cards.get(position).getCardStatus().toUpperCase().equals("HOT")) {

                                        utils.showDilogForError("International Transactions status cannot be changed on BLOCKED Card.", "ERROR");

                                    } else if (cards.get(position).getCardType().contains("PayPak")) {
                                        utils.showDilogForError("International Transactions are not available for PayPak Card.", "ERROR");

                                    } else
                                        CardLimitInternational(pos, cards.get(pos).getLinkedAccounts(), cards.get(pos).getCardNumber());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;

                            case 2:

                                try {
                                    if (cards.get(position).getCardStatus().toUpperCase().equals("HOT")) {

                                        utils.showDilogForError("ECommerce status cannot be changed on BLOCKED Card.", "ERROR");

                                    } else if (cards.get(position).getCardType().contains("PayPak")) {

                                        utils.showDilogForError("ECommerce Activation is not available for PayPak Card.", "ERROR");

                                    } else {
                                        int stanNumber = 6;
                                        String stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
                                        int rrnNumber = 12;
                                        String rrn = String.valueOf(helper.generateRandom(rrnNumber));
                                        ECommerceActivation(pos, stan, cards.get(pos).getCardNumber(), rrn, cards.get(pos).getLinkedAccounts());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//                            finish();
                                break;


                            default: {

                                break;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onBackgroundClick() {
//                textViewForAnimation.setText("Click background!!!");
                }

                @Override
                public void onBoomWillHide() {
                    Log.d("BMB", "onBoomWillHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will RE-BOOM!!!");
                }

                @Override
                public void onBoomDidHide() {
                    Log.d("BMB", "onBoomDidHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did RE-BOOM!!!");
                }

                @Override
                public void onBoomWillShow() {
                    Log.d("BMB", "onBoomWillShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will BOOM!!!");
                }

                @Override
                public void onBoomDidShow() {
                    Log.d("BMB", "onBoomDidShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did BOOM!!!");
                }
            });

            setBoomMenus();
            String firstFour = cards.get(position).getCardNumber().substring(0, 4);
            String secondFour = cards.get(position).getCardNumber().substring(4, 8);
            String thirdFour = cards.get(position).getCardNumber().substring(8, 12);
            String lastFour = cards.get(position).getCardNumber().substring(12, 16);

            accountNumber.setText(firstFour + "    " + "XXXX" + "    " + "XXXX" + "    " + lastFour);


            changeStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (cards.get(position).getCardStatus().toUpperCase().equals("HOT")) {

                        utils.showDilogForError("Status of BLOCKED Card cannot be changed.", "ERROR");

                    } else {
                        Intent intent = new Intent(mContext, CardStatusScreen.class);
                        intent.putExtra("title", cards.get(position).getCardTitle());
                        intent.putExtra("status", cards.get(position).getCardActivationStatus());
                        intent.putExtra("accountNumber", cards.get(position).getCardNumber());
                        intent.putExtra("cardType", cards.get(position).getCardType());
                        intent.putExtra("cardStatus", cards.get(position).getCardStatus());
                        ((Activity) mContext).overridePendingTransition(0, 0);
                        ((Activity) mContext).startActivity(intent);
                    }

                }
            });


            changePin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (cards.get(position).getCardStatus().toUpperCase().equals("HOT")) {

                        utils.showDilogForError("ATM PIN of BLOCKED Card cannot be changed.", "ERROR");


                    } else {
                        Intent intent = new Intent(mContext, CardPinChangeScreen.class);
                        intent.putExtra("title", cards.get(position).getCardTitle());
                        intent.putExtra("status", cards.get(position).getCardActivationStatus());
                        intent.putExtra("accountNumber", cards.get(position).getCardNumber());
                        intent.putExtra("cardType", cards.get(position).getCardType());
                        intent.putExtra("cardStatus", cards.get(position).getCardStatus());
                        ((Activity) mContext).overridePendingTransition(0, 0);
                        ((Activity) mContext).startActivity(intent);
                    }
                }
            });


            view.addView(convertView, 0);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return convertView;

    }

    public void setBoomMenus() {

        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            HamButton.Builder builder = new HamButton.Builder()
                    .listener(new OnBMClickListener() {
                        @Override
                        public void onBoomButtonClick(int index) {
                            // When the boom-button corresponding this builder is clicked.
//                            Toast.makeText(HomeScreen.this, "Clicked " + index, Toast.LENGTH_SHORT).show();
                        }
                    });

//            int margin = getResources().getDimensionPixelSize(R.dimen._4sdp);
            builder.textSize(12);

            switch (i) {

                case 0:
                    builder.normalImageDrawable(mContext.getResources().getDrawable(R.drawable.pos, null));
                    builder.highlightedImageDrawable(mContext.getResources().getDrawable(R.drawable.pos, null));
                    builder.unableImageDrawable(mContext.getResources().getDrawable(R.drawable.pos, null));
                    builder.normalColor(mContext.getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(mContext.getResources().getColor(R.color.notiheader));
                    builder.unableColor(mContext.getResources().getColor(R.color.notiheader));
                    builder.pieceColor(mContext.getResources().getColor(R.color.white));
                    builder.highlightedText("POS Activation");
                    builder.subNormalText("Enable/disable Local POS");
                    builder.normalText("POS Activation");
                    break;

                case 1:
                    builder.normalImageDrawable(mContext.getResources().getDrawable(R.drawable.intl, null));
                    builder.highlightedImageDrawable(mContext.getResources().getDrawable(R.drawable.intl, null));
                    builder.unableImageDrawable(mContext.getResources().getDrawable(R.drawable.intl, null));
                    builder.normalColor(mContext.getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(mContext.getResources().getColor(R.color.notiheader));
                    builder.unableColor(mContext.getResources().getColor(R.color.notiheader));
                    builder.pieceColor(mContext.getResources().getColor(R.color.white));
                    builder.highlightedText("International Transactions");
                    builder.subNormalText("Enable/disable International Transactions");
                    builder.normalText("International Transactions");
                    break;

                case 2:
                    builder.normalImageDrawable(mContext.getResources().getDrawable(R.drawable.ecommerce, null));
                    builder.highlightedImageDrawable(mContext.getResources().getDrawable(R.drawable.ecommerce, null));
                    builder.unableImageDrawable(mContext.getResources().getDrawable(R.drawable.ecommerce, null));
                    builder.normalColor(mContext.getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(mContext.getResources().getColor(R.color.notiheader));
                    builder.unableColor(mContext.getResources().getColor(R.color.notiheader));
                    builder.pieceColor(mContext.getResources().getColor(R.color.white));
                    builder.highlightedText("E-Commerce Activation");
                    builder.subNormalText("Enable/disable E-Commerce");
                    builder.normalText("E-Commerce Activation");
                    break;


                default: {

                    break;
                }

            }

            bmb.addBuilder(builder);
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public void CardLimitInternational(final int position, final String accounts, final String cardNumber) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sAccountNumber", accounts);
                    params.put("sCardNumber", cardNumber);
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(mContext);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.cardlimitURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_GetCustomAccountLimit_InternationalTransactionResult").getString("Status_Code").equals("00")) {

                                                pd.dismiss();
                                                Intent intent = new Intent(mContext, InternationalTransactions.class);
                                                intent.putExtra("LimitAllowed_CW", jsonObject.getJSONObject("Signs_GetCustomAccountLimit_InternationalTransactionResult").getString("LimitAllowed_CW"));
                                                intent.putExtra("LimitAllowed_POS", jsonObject.getJSONObject("Signs_GetCustomAccountLimit_InternationalTransactionResult").getString("LimitAllowed_POS"));
                                                intent.putExtra("title", cards.get(position).getCardTitle());
                                                intent.putExtra("status", cards.get(position).getCardActivationStatus());
                                                intent.putExtra("accountNumber", cards.get(position).getCardNumber());
                                                intent.putExtra("cardType", cards.get(position).getCardType());
                                                intent.putExtra("cardStatus", cards.get(position).getCardStatus());
                                                intent.putExtra("isNrp", cards.get(position).getIsNrp());
                                                intent.putExtra("linkedAccounts", accounts);
                                                mContext.startActivity(intent);


                                            } else if (jsonObject.getJSONObject("Signs_GetCustomAccountLimit_InternationalTransactionResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_GetCustomAccountLimit_InternationalTransactionResult").getString("Status_Description"), "ERROR", mContext, new LoginScreen());


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_GetCustomAccountLimit_InternationalTransactionResult").getString("Status_Description"), "ERROR");

                                            }
                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sAccountNumber", accounts.get(position).getAccountNumber());
//                            params.put("sBranchCode", accounts.get(position).getBranchCode());
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }

            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    public void ECommerceActivation(final int position, final String stan, final String cardNumber, final String rrn, final String accounts) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sSTAN", stan);
                    params.put("sRRN", rrn);
                    params.put("sCardNumber", cardNumber);
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(mContext);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.ecommerceURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_EcommerceActivationValidateResult").getString("Status_Code").equals("00")) {

                                                pd.dismiss();
                                                Intent intent = new Intent(mContext, ECommerceActivation.class);
                                                intent.putExtra("Activated", jsonObject.getJSONObject("Signs_EcommerceActivationValidateResult").getString("Activated"));
                                                intent.putExtra("ActivationTime", jsonObject.getJSONObject("Signs_EcommerceActivationValidateResult").getString("ActivationTime"));
                                                intent.putExtra("title", cards.get(position).getCardTitle());
                                                intent.putExtra("status", cards.get(position).getCardActivationStatus());
                                                intent.putExtra("accountNumber", cards.get(position).getCardNumber());
                                                intent.putExtra("cardType", cards.get(position).getCardType());
                                                intent.putExtra("cardStatus", cards.get(position).getCardStatus());
                                                intent.putExtra("linkedAccounts", accounts);
                                                mContext.startActivity(intent);


                                            } else if (jsonObject.getJSONObject("Signs_EcommerceActivationValidateResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_EcommerceActivationValidateResult").getString("Status_Description"), "ERROR", mContext, new LoginScreen());


                                            } else if (jsonObject.getJSONObject("Signs_EcommerceActivationValidateResult").getString("Status_Code").equals("75")) {
                                                pd.dismiss();
                                                Intent intent = new Intent(mContext, ECommerceActivation.class);
                                                intent.putExtra("Activated", jsonObject.getJSONObject("Signs_EcommerceActivationValidateResult").getString("Activated"));
                                                intent.putExtra("title", cards.get(position).getCardTitle());
                                                intent.putExtra("status", cards.get(position).getCardActivationStatus());
                                                intent.putExtra("accountNumber", cards.get(position).getCardNumber());
                                                intent.putExtra("cardType", cards.get(position).getCardType());
                                                intent.putExtra("cardStatus", cards.get(position).getCardStatus());
                                                intent.putExtra("linkedAccounts", accounts);
                                                mContext.startActivity(intent);
                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_EcommerceActivationValidateResult").getString("Status_Description"), "ERROR");

                                            }
                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sAccountNumber", accounts.get(position).getAccountNumber());
//                            params.put("sBranchCode", accounts.get(position).getBranchCode());
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }

            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }
}