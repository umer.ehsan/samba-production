package com.ceesolutions.samba.cardManagement;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.cardManagement.Model.Card;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.CircleIndicator;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/17/18.
 */

public class CardStatusScreen extends AppCompatActivity {

    private static final Integer[] IMAGES = {R.drawable.group_8, R.drawable.group_8};
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private Dialog pd, dialog;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Utils utils;
    private CircleIndicator indicator;
    private ViewPager mPager, pagerNonSamba;
    private ArrayList<Card> cards;
    private ArrayList<Integer> ImagesArray;
    private ImageView backBtn, settings;
    private ImageView notificationBtn;
    private TextView doneBtn, message, heading, validateBtn, errorMessageForAmount, errorMessageForSpinner, holderName, cardStatus, accountNumberTxt;
    private String type, stan, rrn, statusSelected, currentStatus, accountNumber, accountTitle, cardType, cardStatus1;
    private EditText chatBotText;
    private ArrayList<String> statusArraylist, reasonArrayList;
    //    private EditText editReason;
    private Spinner statusSpinner, reasonSpinner;
    private String[] status, reasons;
    private String reason;
    private RelativeLayout main;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_status);
        initViews();
        getSupportActionBar().hide();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(editReason.getWindowToken(), 0);
            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(CardStatusScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                reason = editReason.getText().toString();

                FirebaseAnalytics.getInstance(CardStatusScreen.this).logEvent("Status_Update_Pressed", new Bundle());

                if (TextUtils.isEmpty(reason)) {

                    errorMessageForAmount.setVisibility(View.VISIBLE);
                    errorMessageForAmount.bringToFront();
                    errorMessageForAmount.setError("");
                    errorMessageForAmount.setText("Please enter valid reason");

                } else if (!helper.validateInputForSC(reason)) {

                    errorMessageForAmount.setVisibility(View.VISIBLE);
                    errorMessageForAmount.bringToFront();
                    errorMessageForAmount.setError("");
                    errorMessageForAmount.setText("Reason cannot contains <,>,\",',%,(,),&,+,\\,~");


                }


                if (TextUtils.isEmpty(statusSelected) || statusSelected.equals("Card Status")) {

                    errorMessageForSpinner.setVisibility(View.VISIBLE);
                    errorMessageForSpinner.bringToFront();
                    errorMessageForSpinner.setError("");
                    errorMessageForSpinner.setText("Please select valid card status");

                } else if (statusSelected.equals(cardStatus1)) {
                    utils.showDilogForError("Your requested status is already defined for your card.", "ERROR");

                }


                if ((!TextUtils.isEmpty(reason) && helper.validateInputForSC(reason)) && !TextUtils.isEmpty(statusSelected) && !statusSelected.equals("Card Status") && !statusSelected.equals(cardStatus1)) {


                    Intent intent = new Intent(CardStatusScreen.this, CardStatusReviewScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("accountNumber", accountNumber);
                    intent.putExtra("title", accountTitle);
                    intent.putExtra("status", statusSelected);
                    intent.putExtra("cardType", cardType);
                    intent.putExtra("stan", stan);
                    intent.putExtra("rrn", rrn);
                    startActivity(intent);


                }
            }
        });

//        editReason.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                errorMessageForAmount.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                errorMessageForAmount.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                reason = s.toString();
//
//            }
//        });


    }


    public void initViews() {

        try {

            CryptLib _crypt = new CryptLib();
            Intent intent = getIntent();

            currentStatus = intent.getStringExtra("status");
            accountNumber = intent.getStringExtra("accountNumber");
            accountTitle = intent.getStringExtra("title");
            cardType = intent.getStringExtra("cardType");
            cardStatus1 = intent.getStringExtra("cardStatus");


            main = (RelativeLayout) findViewById(R.id.main);

            if (cardType.contains("Gold")) {

                main.setBackgroundResource(R.drawable.group_12);
            }else if (cardType.contains("PayPak")) {

                main.setBackgroundResource(R.drawable.paypak);
            }
            statusArraylist = new ArrayList<>();
            reasonArrayList = new ArrayList<>();
            helper = Helper.getHelper(this);
            validateBtn = (TextView) findViewById(R.id.validateBtn);
            requestQueue = Volley.newRequestQueue(this);
            constants = Constants.getConstants(CardStatusScreen.this);
            utils = new Utils(CardStatusScreen.this);
            notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
            settings = (ImageView) findViewById(R.id.settings);
            pd = new Dialog(CardStatusScreen.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            chatBotText = (EditText) findViewById(R.id.chatBoxText);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            statusSpinner = (Spinner) findViewById(R.id.statusSpinner);
            reasonSpinner = (Spinner) findViewById(R.id.reasonSpinner);
            backBtn = (ImageView) findViewById(R.id.backBtn);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            int stanNumber = 6;
            stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
            int rrnNumber = 12;
            rrn = String.valueOf(helper.generateRandom(rrnNumber));
//        editReason = (EditText) findViewById(R.id.editReason);
            errorMessageForAmount = (TextView) findViewById(R.id.errorMessageForAmount);
            cardStatus = (TextView) findViewById(R.id.cardStatus);
            holderName = (TextView) findViewById(R.id.holderName);
            accountNumberTxt = (TextView) findViewById(R.id.accountNumber);
            String firstFour = accountNumber.substring(0, 4);
            String lastFour = accountNumber.substring(12, 16);
            String secondFour = accountNumber.substring(4, 8);
            String thirdFour = accountNumber.substring(8, 12);
            cardStatus.setText(cardStatus1.toUpperCase());
            holderName.setText(accountTitle);
            accountNumberTxt.setText(firstFour + "    " + secondFour + "    " + thirdFour + "    " + lastFour);

            errorMessageForSpinner = (TextView) findViewById(R.id.errorMessageForSpinner);
            type = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            String list = constants.status.getString("statusList", "N/A");
            String reasonlist = constants.reasonList.getString("reasonList", "N/A");
            chatBotText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showDilogForErrorForLogin("Do you want to cancel this activity?", "WARNING");
                }
            });

            if (list.equals("N/A")) {
                GetStatusList();
//            showSpinner();
            } else {
                Gson gson = new Gson();
                Type type1 = new TypeToken<ArrayList<String>>() {
                }.getType();

                statusArraylist = gson.fromJson(list, type1);
                status = new String[statusArraylist.size()];
                status = statusArraylist.toArray(status);


                spinnerAdapter statusAdapter = new spinnerAdapter(CardStatusScreen.this, R.layout.custom_textview_fp);
                statusAdapter.addAll(status);
                statusAdapter.add("Card Status");
                statusSpinner.setAdapter(statusAdapter);
                statusSpinner.setSelection(0);
                statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


                            errorMessageForSpinner.setVisibility(View.GONE);
                            // Get select item
                            if (statusSpinner.getSelectedItem() == "Card Status") {
                                statusSelected = statusSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + statusSelected);

                            } else {

                                statusSelected = statusSpinner.getSelectedItem().toString();

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });

            }

            if (reasonlist.equals("N/A")) {
                GetReasonList();
//            showSpinner();
            } else {
                Gson gson = new Gson();
                Type type1 = new TypeToken<ArrayList<String>>() {
                }.getType();

                reasonArrayList = gson.fromJson(reasonlist, type1);
                reasons = new String[reasonArrayList.size()];
                reasons = reasonArrayList.toArray(reasons);


                spinnerAdapter statusAdapter = new spinnerAdapter(CardStatusScreen.this, R.layout.custom_textview_fp);
                statusAdapter.addAll(reasons);
                statusAdapter.add("Reason");
                reasonSpinner.setAdapter(statusAdapter);
                reasonSpinner.setSelection(0);
                reasonSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


//                        errorMessageForSpinner.setVisibility(View.GONE);
                            // Get select item
                            if (reasonSpinner.getSelectedItem() == "Card Status") {
                                reason = reasonSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + reason);

                            } else {

                                reason = reasonSpinner.getSelectedItem().toString();

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });

            }

            appPreferences = new AppPreferences(CardStatusScreen.this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    public void GetStatusList() {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    pd.show();
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                    HttpsTrustManager.allowMySSL(CardStatusScreen.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.cardListStatusURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();

                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_SambaCardStatusListResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_SambaCardStatusListResult").getString("CardStatusList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        statusArraylist.add(jsonObj.getString("Description"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        pd.dismiss();
                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        validateBtn.setEnabled(true);

                                                    }

                                                }
                                                status = new String[statusArraylist.size()];
                                                status = statusArraylist.toArray(status);


                                                Gson gson = new Gson();
                                                String json = gson.toJson(statusArraylist);
                                                constants.statusListEditor.putString("statusList", json);
                                                constants.statusListEditor.commit();
                                                constants.statusListEditor.apply();
                                                showSpinner();

                                            } else if (jsonObject.getJSONObject("Signs_SambaCardStatusListResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_SambaCardStatusListResult").getString("Status_Description"), "ERROR", CardStatusScreen.this, new LoginScreen());
                                                validateBtn.setEnabled(true);

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_SambaCardStatusListResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            validateBtn.setEnabled(true);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    validateBtn.setEnabled(true);
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "ERROR");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);

        }
    }


    public void GetReasonList() {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    pd.show();
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);


                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                    HttpsTrustManager.allowMySSL(CardStatusScreen.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.reasonURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();

                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_CardStatusReasonListResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_CardStatusReasonListResult").getString("CardStatusList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        reasonArrayList.add(jsonObj.getString("Name"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        pd.dismiss();
                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        validateBtn.setEnabled(true);

                                                    }

                                                }
                                                reasons = new String[reasonArrayList.size()];
                                                reasons = reasonArrayList.toArray(reasons);


                                                Gson gson = new Gson();
                                                String json = gson.toJson(reasonArrayList);
                                                constants.reasonListEditor.putString("reasonList", json);
                                                constants.reasonListEditor.commit();
                                                constants.reasonListEditor.apply();
                                                showSpinnerReason();

                                            } else if (jsonObject.getJSONObject("Signs_CardStatusReasonListResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_CardStatusReasonListResult").getString("Status_Description"), "ERROR", CardStatusScreen.this, new LoginScreen());
                                                validateBtn.setEnabled(true);

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_CardStatusReasonListResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            validateBtn.setEnabled(true);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    validateBtn.setEnabled(true);
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "ERROR");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);

        }
    }

    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {


                spinnerAdapter statusAdapter = new spinnerAdapter(CardStatusScreen.this, R.layout.custom_textview_fp);
                statusAdapter.addAll(status);
                statusAdapter.add("Card Status");
                statusSpinner.setAdapter(statusAdapter);
                statusSpinner.setSelection(0);
                statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


                            // Get select item
                            if (statusSpinner.getSelectedItem() == "Card Status") {
                                statusSelected = statusSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + statusSelected);

                            } else {

                                statusSelected = statusSpinner.getSelectedItem().toString();

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });

                pd.dismiss();


            }
        }, 1000);
    }

    public void showSpinnerReason() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {


                spinnerAdapter statusAdapter = new spinnerAdapter(CardStatusScreen.this, R.layout.custom_textview_fp);
                statusAdapter.addAll(reasons);
                statusAdapter.add("Reason");
                reasonSpinner.setAdapter(statusAdapter);
                reasonSpinner.setSelection(0);
                reasonSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


                            // Get select item
                            if (reasonSpinner.getSelectedItem() == "Card Status") {
                                reason = reasonSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + reason);

                            } else {

                                reason = reasonSpinner.getSelectedItem().toString();

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });

                pd.dismiss();


            }
        }, 1000);
    }

    public void showDilogForErrorForLogin(String msg, String header) {

        dialog = new Dialog(CardStatusScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();


    }

}
