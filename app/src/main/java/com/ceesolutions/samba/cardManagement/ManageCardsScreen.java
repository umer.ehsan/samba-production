package com.ceesolutions.samba.cardManagement;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.Bookme.BookMeMainScreen;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.beneficiariesManagement.ManageBeneficiaresScreen;
import com.ceesolutions.samba.billPayments.PayBillsScreen;
import com.ceesolutions.samba.cardManagement.Adapter.SlidingAdapter;
import com.ceesolutions.samba.cardManagement.Model.Card;
import com.ceesolutions.samba.eatMubarak.EatMubarakMainScreen;
import com.ceesolutions.samba.masterCardIntegration.MainActivity;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.transactions.AccountsScreen;
import com.ceesolutions.samba.transactions.FundTransferScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.CircleIndicator;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.OnBoomListenerAdapter;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/17/18.
 */

public class ManageCardsScreen extends AppCompatActivity {

    private static final Integer[] IMAGES = {R.drawable.group_8, R.drawable.group_8};
    public static double latitude;
    public static double longitude;
    public ProgressBar progress;
    public String longitude1;
    public String latitude1;
    private Dialog pd, dialog;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Utils utils;
    private CircleIndicator indicator;
    private ViewPager mPager, pagerNonSamba;
    private ArrayList<Card> cards;
    private ArrayList<Integer> ImagesArray;
    private ImageView backBtn, settings;
    private ImageView notificationBtn;
    private TextView doneBtn, message, heading, progressTxt, textView, btn;
    private String type, stan, rrn;
    private ImageView sambaBtn, shareBtn;
    private BoomMenuButton bmb;
    private EditText chatBotText;
    private String menu, userType;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_management);
        getSupportActionBar().hide();
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        View view = findViewById(R.id.confirmation);
        initViews();
        try {

            CryptLib _crypt = new CryptLib();
            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }
            menu = constants.sharedPreferences.getString("Menus", "N/A");
            if (!TextUtils.isEmpty(menu) && !menu.equals("N/A") && !menu.equals("")) {


            } else {

                menu = "N/A";
            }

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        Menu navMenu = navigationView.getMenu();
//        navigationView.setItemIconTintList(null);
////        navMenu.add(0,R.id.home,0,)
//        navigationView.setNavigationItemSelectedListener(this);
//
//        if (!menu.equals("N/A")) {
//
//            if (menu.toLowerCase().contains("home")) {
//
//
//            } else {
//                MenuItem home = navMenu.add(0, R.id.home, 0, "Home");
//                home.setIcon(R.drawable.home);
//
//            }
//
//            if (menu.toLowerCase().contains("accounts")) {
//
//            } else {
//
//                MenuItem accounts = navMenu.add(0, R.id.accounts, 0, "Accounts");
//                accounts.setIcon(R.drawable.accounts);
//            }
//
//            if (menu.toLowerCase().contains("beneficiary management")) {
//
//            } else {
//
//                MenuItem beneManagement = navMenu.add(0, R.id.manageBene, 0, "Beneficiary Management");
//                beneManagement.setIcon(R.drawable.beneficiary);
//            }
//
//            if (menu.toLowerCase().contains("friends management")) {
//
//            } else {
//
//                MenuItem friends = navMenu.add(0, R.id.manageFriends, 0, "Friends Management");
//                friends.setIcon(R.drawable.friends);
//            }
//
//            if (menu.toLowerCase().contains("biller management")) {
//
//            } else {
//
//                MenuItem billPayments = navMenu.add(0, R.id.billPayments, 0, "Biller Management");
//                billPayments.setIcon(R.drawable.receipt);
//
//            }
//
//            if (menu.toLowerCase().contains("cards management")) {
//
//            } else {
//
//                MenuItem cardManagement = navMenu.add(0, R.id.cardManagement, 0, "Cards Management");
//                cardManagement.setIcon(R.drawable.credit_card);
//
//            }
//
//            if (menu.toLowerCase().contains("feedback")) {
//
//            } else {
//
//                MenuItem feedback = navMenu.add(0, R.id.feedback, 0, "Customer Feedback");
//                feedback.setIcon(R.drawable.chat_smiley);
//
//            }
//
//        } else {
//
//            MenuItem home = navMenu.add(0, R.id.home, 0, "Home");
//            home.setIcon(R.drawable.home);
//
//            MenuItem accounts = navMenu.add(0, R.id.accounts, 0, "Accounts");
//            accounts.setIcon(R.drawable.accounts);
//
//            MenuItem beneManagement = navMenu.add(0, R.id.manageBene, 0, "Beneficiary Management");
//            beneManagement.setIcon(R.drawable.beneficiary);
//
//            MenuItem friends = navMenu.add(0, R.id.manageFriends, 0, "Friends Management");
//            friends.setIcon(R.drawable.friends);
//
//            MenuItem billPayments = navMenu.add(0, R.id.billPayments, 0, "Biller Management");
//            billPayments.setIcon(R.drawable.receipt);
//
//
//            MenuItem cardManagement = navMenu.add(0, R.id.cardManagement, 0, "Cards Management");
//            cardManagement.setIcon(R.drawable.credit_card);
//
//            MenuItem feedback = navMenu.add(0, R.id.feedback, 0, "Customer Feedback");
//            feedback.setIcon(R.drawable.chat_smiley);
//
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
//        ImageView notificationBtn = (ImageView) toolbar.findViewById(R.id.notificationBtn);
//
//        notificationBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(ManageCardsScreen.this, MessageCenter.class);
//                overridePendingTransition(0,0);
//                startActivity(intent);
//            }
//        });
//
//        navigationView.setNavigationItemSelectedListener(this);
//        View headerLayout = navigationView.getHeaderView(0);
//        TextView textView = (TextView) headerLayout.findViewById(R.id.profileName);
//        ImageView profile = (ImageView) headerLayout.findViewById(R.id.profile_image);
//        progress = (ProgressBar) headerLayout.findViewById(R.id.progress);
//        progressTxt = (TextView) headerLayout.findViewById(R.id.progressTxt);
//
//        String str = constants.sharedPreferences.getString("UserProgress","N/A");
//
//        progressTxt.setText(str+"% Profile Completed");
//
//        int pro = Integer.valueOf(str);
//        progress.setProgress(pro);
//
//        String image = constants.sharedPreferences.getString("ProfilePic", "N/A");
//
//
//        try {
//
//            if (!image.equals("N/A")) {
//                byte[] imageByteArray = Base64.decode(image, Base64.DEFAULT);
//                RequestOptions options = new RequestOptions();
//                options.centerCrop();
//                Glide.with(ManageCardsScreen.this)
//                        .load(imageByteArray)
//                        .apply(options)
//                        .into(profile);
//            } else {
//
//
//            }
//        } catch (Exception e) {
//
//
//        }
//
//
//        ImageView imageView = (ImageView) headerLayout.findViewById(R.id.settings);
//        textView.setText(constants.sharedPreferences.getString("T24_FullName", "N/A"));
//
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                Intent intent = new Intent(ManageCardsScreen.this, SettingsScreen.class);
//                overridePendingTransition(0, 0);
//                startActivity(intent);
//                finish();
//
//            }
//        });

            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent1 = getIntent();
                    type = intent1.getStringExtra("addCard");
                    if (!TextUtils.isEmpty(type) && type.equals("addCard")) {
                        Intent intent = new Intent(ManageCardsScreen.this, HomeScreen.class);
                        overridePendingTransition(0, 0);
                        startActivity(intent);
                        finish();
                        finishAffinity();
                    } else
                        finish();
                }
            });

            notificationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(ManageCardsScreen.this, MessageCenter.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
//                finish();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void initViews() {

        try {

            CryptLib _crypt = new CryptLib();
            helper = Helper.getHelper(this);
            requestQueue = Volley.newRequestQueue(this);
            constants = Constants.getConstants(ManageCardsScreen.this);
            utils = new Utils(ManageCardsScreen.this);
            notificationBtn = findViewById(R.id.notificationBtn);
            settings = findViewById(R.id.settings);
            pd = new Dialog(ManageCardsScreen.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            chatBotText = findViewById(R.id.chatBoxText);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            backBtn = findViewById(R.id.backBtn);
            sambaBtn = findViewById(R.id.sambaBtn);
            chatBotText = findViewById(R.id.chatBoxText);
            shareBtn = findViewById(R.id.shareBtn);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            indicator = findViewById(R.id.indicator);
            mPager = findViewById(R.id.pager);
            cards = new ArrayList<>();
            ImagesArray = new ArrayList<>();
            int stanNumber = 6;
            stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
            int rrnNumber = 12;
            rrn = String.valueOf(Helper.generateRandom(rrnNumber));
            pd = new Dialog(this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            type = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
            appPreferences = new AppPreferences(ManageCardsScreen.this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }

            bmb = findViewById(R.id.bmb);
            assert bmb != null;
            bmb.setButtonEnum(ButtonEnum.TextOutsideCircle);
            bmb.setBackgroundColor(Color.TRANSPARENT);
            bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_9_1);
            bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_9_1);

            chatBotText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                showDilogForErrorForLogin1(getResources().getString(R.string.message), "WARNING");

                    Intent i = new Intent(ManageCardsScreen.this, HomeScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(i);
                    finish();
                    finishAffinity();
                }
            });


            sambaBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    bmb.boom();
                }
            });

            shareBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {


                        Intent intent = new Intent("android.intent.action.VIEW");

                        /** creates an sms uri */
                        Uri data = Uri.parse("sms:");

                        /** Setting sms uri to the intent */
                        intent.setData(data);
                        intent.putExtra("sms_body", "Download Samba Mobile App and register yourself. Click https://bit.ly/2KB0nfG for details");

                        /** Initiates the SMS compose screen, because the activity contain ACTION_VIEW and sms uri */
                        startActivity(intent);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            bmb.setOnBoomListener(new OnBoomListenerAdapter() {
                @Override
                public void onBoomWillShow() {
                    super.onBoomWillShow();
                    // logic here
                }
            });

            bmb.setOnBoomListener(new OnBoomListener() {
                @Override
                public void onClicked(int index, BoomButton boomButton) {
                    // If you have implement listeners for boom-buttons in builders,
                    // then you shouldn't add any listener here for duplicate callbacks.

                    switch (index) {


                        case 0:

                            Intent home = new Intent(ManageCardsScreen.this, HomeScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(home);
                            finish();
                            break;

                        case 1:

                            if (!menu.equals("N/A")) {

                                if (menu.toLowerCase().contains("accounts")) {
                                    showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                } else {

                                    Intent intent = new Intent(ManageCardsScreen.this, AccountsScreen.class);
                                    overridePendingTransition(0, 0);
                                    startActivity(intent);
//                                finish();
                                }
                            } else {
                                Intent intent = new Intent(ManageCardsScreen.this, AccountsScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
//                            finish();

                            }

                            break;

                        case 2:

                            try {

                                CryptLib _crypt = new CryptLib();
                                Intent transfers = new Intent(ManageCardsScreen.this, FundTransferScreen.class);
                                String user = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                                if (!TextUtils.isEmpty(user) && !user.equals("N/A")) {

                                    if (user.equals("NON_SAMBA")) {

                                        transfers.putExtra("requestFunds", "nonSamba");
                                    }
                                }
                                overridePendingTransition(0, 0);
                                startActivity(transfers);
//                            finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;


                        case 3:

                            try {

                                CryptLib _crypt;
                                if (!menu.equals("N/A")) {
                                    _crypt = new CryptLib();
                                    if (menu.toLowerCase().contains("biller management")) {
                                        showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                    } else {
                                        String user1 = constants.sharedPreferences.getString("type", "N/A");
                                        Intent payments = new Intent(ManageCardsScreen.this, PayBillsScreen.class);
                                        overridePendingTransition(0, 0);
                                        user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                                        if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                            if (user1.equals("NON_SAMBA")) {

                                                payments.putExtra("requestbill", "nonSamba");
                                            }
                                        }
                                        startActivity(payments);
//                                    finish();
                                    }
                                } else {
                                    _crypt = new CryptLib();
                                    Intent payments = new Intent(ManageCardsScreen.this, PayBillsScreen.class);
                                    overridePendingTransition(0, 0);
                                    String user1 = constants.sharedPreferences.getString("type", "N/A");
                                    user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                                    if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                        if (user1.equals("NON_SAMBA")) {

                                            payments.putExtra("requestbill", "nonSamba");
                                        }
                                    }
                                    startActivity(payments);
//                                finish();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;


                        case 4:

                            if (!menu.equals("N/A")) {

                                if (menu.toLowerCase().contains("beneficiary management")) {
                                    showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                } else {

                                    if (userType.equals("NON_SAMBA")) {
                                        showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                    } else {
                                        Intent bene = new Intent(ManageCardsScreen.this, ManageBeneficiaresScreen.class);
                                        overridePendingTransition(0, 0);
                                        startActivity(bene);
//                                    finish();
                                    }
                                }
                            } else {
                                if (userType.equals("NON_SAMBA")) {
                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                } else {
                                    Intent bene = new Intent(ManageCardsScreen.this, ManageBeneficiaresScreen.class);
                                    overridePendingTransition(0, 0);
                                    startActivity(bene);
//                                    finish();
                                }
//                                finish();
                            }

                            break;


                        case 5:

                            if (userType.equals("NON_SAMBA"))
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            else {
                                Intent intent = new Intent(ManageCardsScreen.this, MainActivity.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                            }

//                            if (!menu.equals("N/A")) {
//
//                                if (menu.toLowerCase().contains("friends management")) {
//                                    showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                                } else {
//
//                                    Intent friends = new Intent(ManageCardsScreen.this, ManageFriends.class);
//                                    overridePendingTransition(0, 0);
//                                    startActivity(friends);
////                                    finish();
//                                }
//                            } else {
//
//                                Intent friends = new Intent(ManageCardsScreen.this, ManageFriends.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(friends);
////                                finish();
//                            }

                            break;


                        case 6:

                            try {

                                CryptLib _crypt = new CryptLib();

                                String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                                if (userType.equals("NON_SAMBA"))
                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                else {
                                    Intent intent = new Intent(ManageCardsScreen.this, EatMubarakMainScreen.class);
                                    overridePendingTransition(0, 0);
                                    startActivity(intent);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("cards management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//                                if (userType.equals("NON_SAMBA")) {
//                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                                } else {
//                                    Intent cards = new Intent(ManageAccountsScreen.this, ManageCardsScreen.class);
//                                    overridePendingTransition(0, 0);
//                                    startActivity(cards);
////                                    finish();
//                                }
//
////                                    finish();
//                            }
//                        } else {
//                            if (userType.equals("NON_SAMBA")) {
//                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                            } else {
//                                Intent cards = new Intent(ManageAccountsScreen.this, ManageCardsScreen.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(cards);
////                                    finish();
//                            }
////                                finish();
//                        }
                            break;


                        case 7:
                            if (userType.equals("NON_SAMBA")) {
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            } else {
                                Intent atm = new Intent(ManageCardsScreen.this, BookMeMainScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(atm);
//                        finish();
                            }
                            break;

                        case 8:

                            pd.show();
                            showDialog("Are you sure you want to Logout?");
                            break;


                        default: {

                            break;
                        }
                    }
                }

                @Override
                public void onBackgroundClick() {
//                textViewForAnimation.setText("Click background!!!");
                }

                @Override
                public void onBoomWillHide() {
                    Log.d("BMB", "onBoomWillHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will RE-BOOM!!!");
                }

                @Override
                public void onBoomDidHide() {
                    Log.d("BMB", "onBoomDidHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did RE-BOOM!!!");
                }

                @Override
                public void onBoomWillShow() {
                    Log.d("BMB", "onBoomWillShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will BOOM!!!");
                }

                @Override
                public void onBoomDidShow() {
                    Log.d("BMB", "onBoomDidShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did BOOM!!!");
                }
            });

//        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++)
//            bmb.addBuilder(BuilderManager.getSimpleCircleButtonBuilder());

            for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
                TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder()
                        .listener(new OnBMClickListener() {
                            @Override
                            public void onBoomButtonClick(int index) {
                                // When the boom-button corresponding this builder is clicked.
//                            Toast.makeText(HomeScreen.this, "Clicked " + index, Toast.LENGTH_SHORT).show();
                            }
                        });

//            int margin = getResources().getDimensionPixelSize(R.dimen._4sdp);
                builder.textSize(12);

                switch (i) {

                    case 0:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Home");
                        builder.unableText("Home");
                        builder.normalText("Home");
                        break;

                    case 1:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Accounts");
                        builder.unableText("Accounts");
                        builder.normalText("Accounts");
                        break;

                    case 2:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Transfers");
                        builder.unableText("Transfers");
                        builder.normalText("Transfers");
                        break;


                    case 3:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Payments");
                        builder.unableText("Payments");
                        builder.normalText("Payments");
                        break;


                    case 4:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Beneficiaries");
                        builder.unableText("Beneficiaries");
                        builder.normalText("Beneficiaries");
                        break;


                    case 5:

                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("QR Payments");
                        builder.unableText("QR Payments");
                        builder.normalText("QR Payments");


//                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                        builder.normalColor(getResources().getColor(R.color.notiheader));
//                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                        builder.unableColor(getResources().getColor(R.color.notiheader));
//                        builder.highlightedText("Friends");
//                        builder.unableText("Friends");
//                        builder.normalText("Friends");
                        break;


                    case 6:

                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Food");
                        builder.unableText("Food");
                        builder.normalText("Food");

//                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                        builder.normalColor(getResources().getColor(R.color.notiheader));
//                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                        builder.unableColor(getResources().getColor(R.color.notiheader));
//                        builder.highlightedText("Cards");
//                        builder.unableText("Cards");
//                        builder.normalText("Cards");
                        break;


                    case 7:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("E-Tickets");
                        builder.unableText("E-Tickets");
                        builder.normalText("E-Tickets");
                        break;

                    case 8:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Logout");
                        builder.unableText("Logout");
                        builder.normalText("Logout");
                        break;


                    default: {

                        break;
                    }

                }
//            builder.normalImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.unableImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.normalColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedColor(getResources().getColor(R.color.notiheader));
//            builder.unableColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedText("Cards");
//            builder.unableText("Cards");
//            builder.normalText("Cards");
//            builder.imagePadding(new Rect(40, 40, 40, 40));
                bmb.addBuilder(builder);
            }

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    GetCardsList();

                }
            }, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GetCardsList() {

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {
                //135821 ammar
                //181936 hasan
                //constants.sharedPreferences.getString("T24_CIF", "N/A")

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sSTAN", stan);
                    params.put("sRRN", rrn);
                    params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                    params.put("sServerSessionKey", md5);
                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    HttpsTrustManager.allowMySSL(ManageCardsScreen.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.cardListURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {

                                            if (jsonObject.getJSONObject("Signs_SambaCardResult").getString("Status_Code").equals("00")) {

                                                int count = 0;
                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_SambaCardResult").getString("CardList"));

                                                if (jsonArray.length() > 0) {


                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                        try {

                                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
                                                            Card card = new Card();
                                                            card.setCardActivationStatus(jsonObj.getString("CardActivationStatus"));
                                                            card.setCardNumber(jsonObj.getString("CardNumber"));
                                                            card.setCardProduct(jsonObj.getString("CardProduct"));
                                                            card.setCardStatus(jsonObj.getString("CardStatus"));
                                                            card.setCardTitle(jsonObj.getString("CardTitle"));
                                                            card.setCardType(jsonObj.getString("CardType"));
                                                            card.setCardExpiryDate(jsonObj.getString("CardExpiryDate"));
                                                            card.setCardIssueDate(jsonObj.getString("CardIssueDate"));
                                                            card.setLinkedAccounts(jsonObj.getString("LinkedAccounts"));
                                                            card.setIsNrp(jsonObj.getString("IsNrpCustomer"));
                                                            cards.add(card);
                                                            ImagesArray.add(count);
                                                            count++;

                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                            pd.dismiss();
                                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        }

                                                    }


//                                                if (type.equals("SAMBA")) {
//                                                    Gson gson = new Gson();
//                                                    String json = gson.toJson(cards);
//                                                    constants.accountListEditor.putString("cards", json);
//                                                    constants.accountListEditor.commit();
//                                                    constants.accountListEditor.apply();
//                                                    mPager.setAdapter(new SlidingAdapter(ManageCardsScreen.this, ImagesArray, cards));
//                                                } else {
//                                                    Gson gson = new Gson();
//                                                    String json = gson.toJson(cards);
//                                                    constants.accountListEditor.putString("cards", json);
//                                                    constants.accountListEditor.commit();
//                                                    constants.accountListEditor.apply();
                                                    mPager.setAdapter(new SlidingAdapter(ManageCardsScreen.this, ImagesArray, cards));
//                                                }

                                                    if (jsonArray.length() == 1) {
//                                                    indicator.setBackgroundColor(Color.parseColor();
                                                    } else {
                                                        indicator.setViewPager(mPager);
                                                    }

                                                    pd.dismiss();
                                                } else {
                                                    pd.dismiss();


                                                }
                                            } else if (jsonObject.getJSONObject("Signs_SambaCardResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_SambaCardResult").getString("Status_Description"), "ERROR", ManageCardsScreen.this, new LoginScreen());


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_SambaCardResult").getString("Status_Description"), "WARNING");
                                            }
                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sSTAN", stan);
//                            params.put("sRRN", rrn);
//                            params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }

            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }

    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
////            super.onBackPressed();
//        }
        Intent intent1 = getIntent();
        type = intent1.getStringExtra("addCard");
        if (!TextUtils.isEmpty(type) && type.equals("addCard")) {
            Intent intent = new Intent(ManageCardsScreen.this, HomeScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);
            finish();
            finishAffinity();
        } else
            finish();
    }

//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.home) {
//            Intent intent = new Intent(ManageCardsScreen.this, HomeScreen.class);
//            overridePendingTransition(0, 0);
//            startActivity(intent);
//            finish();
//
//        } else if (id == R.id.accounts) {
//
//            Intent intent = new Intent(ManageCardsScreen.this, AccountsScreen.class);
//            overridePendingTransition(0, 0);
//            startActivity(intent);
//            finish();
//
//        } else if (id == R.id.manageBene) {
//
//            Intent intent1 = new Intent(ManageCardsScreen.this, ManageBeneficiaresScreen.class);
//            overridePendingTransition(0, 0);
//            startActivity(intent1);
//            finish();
//
//        } else if (id == R.id.manageFriends) {
//
//            Intent intent2 = new Intent(ManageCardsScreen.this, ManageFriends.class);
//            overridePendingTransition(0, 0);
//            startActivity(intent2);
//            finish();
//
//        } else if (id == R.id.billPayments) {
//
//            Intent intent2 = new Intent(ManageCardsScreen.this, ManageBillerScreen.class);
//            overridePendingTransition(0, 0);
//            startActivity(intent2);
//            finish();
//
//        } else if (id == R.id.cardManagement) {
//
////            Intent intent2 = new Intent(ManageCardsScreen.this, ManageCardsScreen.class);
////            overridePendingTransition(0, 0);
////            startActivity(intent2);
////            finish();
//
//        } else if (id == R.id.feedback) {
//
//            Intent intent2 = new Intent(ManageCardsScreen.this, FeedbackScreen.class);
//            overridePendingTransition(0, 0);
//            startActivity(intent2);
//            finish();
//        }
//
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }


    public void showDilogForErrorForLogin(String msg, String header) {

        dialog = new Dialog(ManageCardsScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = dialog.findViewById(R.id.validatingTxt);
        doneBtn = dialog.findViewById(R.id.doneBtn);
        heading = dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();


    }


    public void showDilogForErrorForLogin1(String msg, String header) {

        dialog = new Dialog(ManageCardsScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = dialog.findViewById(R.id.validatingTxt);
        doneBtn = dialog.findViewById(R.id.doneBtn);
        heading = dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent i = new Intent(ManageCardsScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(i);
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();


    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(ManageCardsScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = dialog.findViewById(R.id.validatingTxt);
        btn = dialog.findViewById(R.id.doneBtn);
        heading = dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    private void showDialog(String message) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Alert:");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

//                constants.editor.clear();
//                constants.editor.commit();
//                constants.editor.apply();
//                constants.nonSambaCredentialsEditor.clear();
//                constants.nonSambaCredentialsEditor.commit();
//                constants.nonSambaCredentialsEditor.apply();
//                constants.destinationAccountEditor.clear().commit();
//                constants.destinationAccountEditor.apply();
//                constants.sourceAccountEditor.clear().commit();
//                constants.sourceAccountEditor.apply();
//                constants.accountListEditor.clear().commit();
//                constants.accountListEditor.apply();
//                constants.accountsEditor.clear().commit();
//                constants.accountsEditor.apply();
//                constants.forgotPasswordEditor.clear().commit();
//                constants.forgotPasswordEditor.apply();
//                Handler handler1 = new Handler();
//                handler1.postDelayed(new Runnable() {
//                    public void run() {
//
//                        pd.dismiss();
//                        Intent intent = new Intent(ManageCardsScreen.this, LoginScreen.class);
//                        startActivity(intent);
//                        overridePendingTransition(0, 0);
//                        finish();
//                        finishAffinity();
//
//                    }
//                }, 1000);
                Logout();
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                pd.dismiss();
            }
        });

        builder.show();
    }

    public void Logout() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSessionID", Constants.Shared_KEY);
                    HttpsTrustManager.allowMySSL(ManageCardsScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.logoutURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Code").equals("00")) {
                                                constants.editor.clear();
                                                constants.editor.commit();
                                                constants.editor.apply();
                                                constants.nonSambaCredentialsEditor.clear();
                                                constants.nonSambaCredentialsEditor.commit();
                                                constants.nonSambaCredentialsEditor.apply();
                                                constants.destinationAccountEditor.clear().commit();
                                                constants.destinationAccountEditor.apply();
                                                constants.sourceAccountEditor.clear().commit();
                                                constants.sourceAccountEditor.apply();
                                                constants.accountListEditor.clear().commit();
                                                constants.accountListEditor.apply();
                                                constants.accountsEditor.clear().commit();
                                                constants.accountsEditor.apply();
                                                constants.forgotPasswordEditor.clear().commit();
                                                constants.forgotPasswordEditor.apply();
                                                constants.purposeBillList.clear().commit();
                                                constants.purposeBillList.apply();
                                                constants.purposePrepaidList.clear().commit();
                                                constants.purposePrepaidList.apply();
                                                constants.purposeListEditor.clear().commit();
                                                constants.purposeListEditor.apply();


                                                pd.dismiss();
                                                Intent intent = new Intent(ManageCardsScreen.this, LoginScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                finish();
                                                finishAffinity();


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Description"), "ERROR", ManageCardsScreen.this, new LoginScreen());

                                            }

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sSessionID", Constants.Shared_KEY);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity", "WARNING");


            }
        } catch (Exception e) {

            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity", "WARNING");


        }
    }

}

