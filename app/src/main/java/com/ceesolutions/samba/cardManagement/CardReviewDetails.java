package com.ceesolutions.samba.cardManagement;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class CardReviewDetails extends AppCompatActivity {

    private ExpandableLayout expandableLayout0;
    private RelativeLayout firstRow;
    private TextView message, mainTxt, textView, doneBtn, heading, date, userNameTxt, accMaskTxt, friendNameTxt, accFrindMaskTxt, accFrindEmailTxt, accFrindMobileTxt, totalAmountTxt, amountInWordsTxt, purposeHeadingTxt, remarksHeadingTxt, conversionHeadingTxt, chargesHeadingTxt, reviewBtn;
    private ImageView backBtn, cancelBtn;
    boolean flag = false;
    private Dialog dialog;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private String amountFinal, amountWords, finalDate, currentTime, accountNumber;
    private Intent intent1;
    private Date currentDate;
    private String status, code;
    private AppPreferences appPreferences;
    private String location;
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private TextView amountTxt;
    private String className, limitAllowed_CW, limitAllowed_POS, actStatus, forever;
    private Calendar calendar;
    private DateFormat dateFormat;
    private SimpleDateFormat timeFormat;
    private static final String TIME_PATTERN = "hh:mm aa";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_review_details);
        initViews();
        getSupportActionBar().hide();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");

            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                finish();
            }
        });

        reviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reviewBtn.setEnabled(false);
                if (className.equals("pos")) {
                    FirebaseAnalytics.getInstance(CardReviewDetails.this).logEvent("POS_Status_Confirmation_Pressed", new Bundle());
                }

                if (className.equals("inter")) {
                    FirebaseAnalytics.getInstance(CardReviewDetails.this).logEvent("Int_Transaction_Confirmation_Press", new Bundle());
                }


                if (className.equals("ecommerce")) {
                    FirebaseAnalytics.getInstance(CardReviewDetails.this).logEvent("ECommerce_Status_Confirmation_Pressed", new Bundle());
                }


                if (className.equals("pos")) {

                    Intent intent = new Intent(CardReviewDetails.this, PasscodeVerificationScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("accountNumber", accountNumber);
                    intent.putExtra("title", intent1.getStringExtra("title"));
                    intent.putExtra("statusFor", intent1.getStringExtra("statusForPos"));
                    intent.putExtra("code", code);
                    intent.putExtra("finalDate", finalDate);
                    intent.putExtra("status", intent1.getStringExtra("posStatus"));
                    intent.putExtra("linkedAccounts", intent1.getStringExtra("linkedAccounts"));
                    intent.putExtra("currentTime", currentTime);
                    intent.putExtra("className", "pos");
                    intent.putExtra("cardType", intent1.getStringExtra("cardType"));
                    intent.putExtra("stan", intent1.getStringExtra("stan"));
                    intent.putExtra("rrn", intent1.getStringExtra("rrn"));
                    startActivity(intent);
                    reviewBtn.setEnabled(true);
                } else if (className.equals("inter")) {

                    Intent intent = new Intent(CardReviewDetails.this, PasscodeVerificationScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("accountNumber", accountNumber);
                    intent.putExtra("title", intent1.getStringExtra("title"));
                    intent.putExtra("status", status);
                    intent.putExtra("code", code);
                    intent.putExtra("finalDate", finalDate);
                    intent.putExtra("status", status);
                    intent.putExtra("className", "inter");
                    intent.putExtra("currentTime", currentTime);
                    intent.putExtra("limitAllowed_CW", limitAllowed_CW);
                    intent.putExtra("limitAllowed_POS", limitAllowed_POS);
                    intent.putExtra("linkedAccounts", intent1.getStringExtra("linkedAccounts"));
                    intent.putExtra("cardType", intent1.getStringExtra("cardType"));
                    intent.putExtra("stan", intent1.getStringExtra("stan"));
                    intent.putExtra("rrn", intent1.getStringExtra("rrn"));
                    startActivity(intent);
                    reviewBtn.setEnabled(true);
                } else if (className.equals("ecommerce")) {

                    Intent intent = new Intent(CardReviewDetails.this, PasscodeVerificationScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("accountNumber", accountNumber);
                    intent.putExtra("title", intent1.getStringExtra("title"));
                    intent.putExtra("status", status);
                    intent.putExtra("code", code);
                    intent.putExtra("finalDate", finalDate);
                    intent.putExtra("status", status);
                    intent.putExtra("className", "ecom");
                    intent.putExtra("currentTime", currentTime);
                    intent.putExtra("actStatus", actStatus);
                    intent.putExtra("date", intent1.getStringExtra("date"));
                    intent.putExtra("time", intent1.getStringExtra("time"));
                    intent.putExtra("sendTime", intent1.getStringExtra("sendTime"));
                    intent.putExtra("forever", intent1.getStringExtra("forever"));
                    intent.putExtra("linkedAccounts", intent1.getStringExtra("linkedAccounts"));
                    intent.putExtra("cardType", intent1.getStringExtra("cardType"));
                    intent.putExtra("stan", intent1.getStringExtra("stan"));
                    intent.putExtra("rrn", intent1.getStringExtra("rrn"));
                    startActivity(intent);
                    reviewBtn.setEnabled(true);
                } else {
                    GenerateiPIN();
                }

            }
        });
    }

    public void initViews() {

        date = (TextView) findViewById(R.id.date);
        userNameTxt = (TextView) findViewById(R.id.userNameTxt);
        accMaskTxt = (TextView) findViewById(R.id.accMaskTxt);
        totalAmountTxt = (TextView) findViewById(R.id.totalAmountTxt);
        amountTxt = (TextView) findViewById(R.id.amountTxt);
        reviewBtn = (TextView) findViewById(R.id.reviewBtn);
        cancelBtn = (ImageView) findViewById(R.id.cancelBtn);
        requestQueue = Volley.newRequestQueue(CardReviewDetails.this);
        helper = Helper.getHelper(CardReviewDetails.this);
        constants = Constants.getConstants(CardReviewDetails.this);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        utils = new Utils(CardReviewDetails.this);
        mainTxt = (TextView) findViewById(R.id.mainTxt);
        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.US);
        currentDate = Calendar.getInstance().getTime();
        formatter = new SimpleDateFormat("dd-MMM-yyyy");
        date.setText(formatter.format(currentDate));
        finalDate = formatter.format(currentDate);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
        currentTime = sdf.format(currentDate);

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Intent intent = getIntent();
        intent1 = intent;
        accountNumber = intent.getStringExtra("accountNumber");
        String firstFour = accountNumber.substring(0, 4);
        String lastFour = accountNumber.substring(12, 16);
        String secondFour = accountNumber.substring(4, 8);
        String thirdFour = accountNumber.substring(8, 12);
        userNameTxt.setText(intent.getStringExtra("title"));
        accMaskTxt.setText(firstFour + " " + secondFour + " " + thirdFour + " " + lastFour);

        className = intent.getStringExtra("className");

        if (className.equals("pos")) {

            amountTxt.setText("POS Status");
            status = intent.getStringExtra("posStatus");

            if (status.equals("Y")) {

                totalAmountTxt.setText("Activate");
            } else if (status.equals("N")) {

                totalAmountTxt.setText("Deactivate");
            }
        }

        if (className.equals("inter")) {

            amountTxt.setText("Transactions Status");
            limitAllowed_CW = intent.getStringExtra("limitAllowed_CW");
            limitAllowed_POS = intent.getStringExtra("limitAllowed_POS");

            if (!TextUtils.isEmpty(limitAllowed_CW) && !TextUtils.isEmpty(limitAllowed_POS)) {

                if (limitAllowed_CW.equals("Y") && limitAllowed_POS.equals("Y")) {

                    totalAmountTxt.setText("Int. POS: Activate\nInt. C/W: Activate");

                } else if (limitAllowed_CW.equals("N") && limitAllowed_POS.equals("N")) {

                    totalAmountTxt.setText("Int. POS: Deactivate\nInt. C/W: Deactivate");

                } else if (limitAllowed_CW.equals("Y") && limitAllowed_POS.equals("N")) {

                    totalAmountTxt.setText("Int. POS: Deactivate\nInt. C/W: Activate");

                } else if (limitAllowed_CW.equals("N") && limitAllowed_POS.equals("Y")) {

                    totalAmountTxt.setText("Int. POS: Activate\nInt. C/W: Deactivate");
                }


            } else if (!TextUtils.isEmpty(limitAllowed_CW)) {

                if (limitAllowed_CW.equals("Y")) {
                    totalAmountTxt.setText("Int. C/W: Activate");
                } else totalAmountTxt.setText("Int. C/W: Deactivate");

            } else if (!TextUtils.isEmpty(limitAllowed_POS)) {

                if (limitAllowed_POS.equals("Y")) {
                    totalAmountTxt.setText("Int. POS: Activate");
                } else totalAmountTxt.setText("Int. POS: Deactivate");
            }

        }

        if (className.equals("ecommerce")) {

            amountTxt.setText("E-Commerce Status");
            forever = intent.getStringExtra("forever");
            status = intent.getStringExtra("actStatus");

            if (!TextUtils.isEmpty(forever) && !forever.equals("null") && !forever.equals(null) && !TextUtils.isEmpty(status) && !status.equals("null") && !status.equals(null)) {

                if (status.equals("Y")) {

                    totalAmountTxt.setText("Activate for Unlimited time period");
                } else if (status.equals("N")) {

                    totalAmountTxt.setText("Deactivate");
                }

            } else if (!TextUtils.isEmpty(status) && !status.equals("null") && !status.equals(null)) {

                if (status.equals("Y")) {

                    totalAmountTxt.setText("Activate till " + intent.getStringExtra("date") + " " + intent.getStringExtra("time"));
                } else if (status.equals("N")) {

                    totalAmountTxt.setText("Deactivate");
                }
            }

        }
        appPreferences = new AppPreferences(CardReviewDetails.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(CardReviewDetails.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
//        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


    private String getFormatedAmount(int amount) {
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }


    public void showDilogForErrorForLogin(String msg, String header) {

        dialog = new Dialog(CardReviewDetails.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();


    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");
    }

    public void GenerateiPIN() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                dialog.show();
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);

                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Username", user);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);

                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                HttpsTrustManager.allowMySSL(CardReviewDetails.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md5)) {


                                        if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("00")) {


                                            dialog.dismiss();
                                            Intent intent = new Intent(CardReviewDetails.this, PinVerificationScreen.class);
                                            overridePendingTransition(0, 0);
                                            intent.putExtra("accountNumber", accountNumber);
                                            intent.putExtra("title", intent1.getStringExtra("title"));
                                            intent.putExtra("status", status);
                                            intent.putExtra("code", code);
                                            intent.putExtra("finalDate", finalDate);
                                            intent.putExtra("currentTime", currentTime);
                                            intent.putExtra("cardType", intent1.getStringExtra("cardType"));
                                            intent.putExtra("stan", intent1.getStringExtra("stan"));
                                            intent.putExtra("rrn", intent1.getStringExtra("rrn"));
                                            startActivity(intent);
                                            reviewBtn.setEnabled(true);

                                        } else if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("54")) {
                                            dialog.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR", CardReviewDetails.this, new LoginScreen());
                                            reviewBtn.setEnabled(true);

                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR");
                                            reviewBtn.setEnabled(true);
                                        }
                                    } else {
                                        dialog.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        reviewBtn.setEnabled(true);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    reviewBtn.setEnabled(true);

                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    reviewBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    reviewBtn.setEnabled(true);
                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();


            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                reviewBtn.setEnabled(true);


            }
        } catch (Exception e) {
            dialog.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            reviewBtn.setEnabled(true);


        }
    }
}