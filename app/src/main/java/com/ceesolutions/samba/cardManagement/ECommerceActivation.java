package com.ceesolutions.samba.cardManagement;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.accessControl.TermsAndConditionsForPOS;
import com.ceesolutions.samba.cardManagement.Model.Card;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.CircleIndicator;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.SwitchCompat;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ECommerceActivation extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private static final Integer[] IMAGES = {R.drawable.group_8, R.drawable.group_8};
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private Dialog pd, dialog;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Utils utils;
    private CircleIndicator indicator;
    private ViewPager mPager, pagerNonSamba;
    private ArrayList<Card> cards;
    private boolean isChecked = true;
    private ArrayList<Integer> ImagesArray;
    private ImageView backBtn, settings, imageBtn;
    private ImageView notificationBtn;
    private TextView agreementTxt, lblDate, lblTime, doneBtn, message, heading, validateBtn, errorMessageForAmount, errorMessageForSpinner, holderName, cardStatus, accountNumberTxt;
    private String type, stan, rrn, statusSelected, currentStatus, accountNumber, accountTitle, cardType, cardStatus1;
    private EditText chatBotText;
    private ArrayList<String> statusArraylist, reasonArrayList;
    //    private EditText editReason;
    private Spinner statusSpinner, reasonSpinner;
    private String[] status, reasons;
    private String reason;
    private RelativeLayout main;
    private AppPreferences appPreferences;
    private SwitchCompat posCompactBtn, posCompactBtn1;
    private String location, actStatus, activationTime;
    private EditText editShareAccount1, editShareAccount2;
    private boolean changeCompact = false, posCompact = false;
    private boolean cashStatus = false, ecommerceStatus = false;
    private Calendar calendar;
    private DateFormat dateFormat;
    private SimpleDateFormat timeFormat;
    private static final String TIME_PATTERN = "hh:mm aa";
    private Intent intent1;
    private String time;
    private boolean isTouched = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ecommerce_activation);
        initViews();
        getSupportActionBar().hide();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(editReason.getWindowToken(), 0);
            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ECommerceActivation.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
            }
        });

        posCompactBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

//                Toast.makeText(getActivity(),"Test---"+isChecked,Toast.LENGTH_SHORT).show();
                isTouched = false;
                posCompact = !posCompact;
                if (posCompact) {


                    if (ecommerceStatus) {
                        DisableViews();
                        posCompactBtn1.setChecked(false, false);
                        editShareAccount1.setText("Deactivated");
                    } else {
                        EnableViews();
                        editShareAccount1.setText("Activated");
                    }
                } else {

                    if (ecommerceStatus) {
                        EnableViews();
                        posCompactBtn1.setChecked(false, false);
                        editShareAccount2.setText("Deactivated");
                        editShareAccount1.setText("Activated");
                    } else {
                        DisableViews();
                        editShareAccount1.setText("Deactivated");
                    }
                }

            }
        });

        posCompactBtn1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

//                Toast.makeText(getActivity(),"Test---"+isChecked,Toast.LENGTH_SHORT).show();
                changeCompact = !changeCompact;

                if (changeCompact) {


                    DisableViewsForever();
                    if (cashStatus) {

                        EnableViewsForever();
                        editShareAccount2.setText("Deactivated");
                    } else
                        editShareAccount2.setText("Activated");
                } else {
                    EnableViewsForever();
                    if (cashStatus) {
                        DisableViewsForever();
                        editShareAccount2.setText("Activated");
                    } else
                        editShareAccount2.setText("Deactivated");
                }
            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                reason = editReason.getText().toString();

                FirebaseAnalytics.getInstance(ECommerceActivation.this).logEvent("ECommerce_Status_Update_Pressed", new Bundle());
                FirebaseAnalytics.getInstance(ECommerceActivation.this).logEvent("ECommerce_Activation_"+ editShareAccount1.getText().toString() , new Bundle());
                FirebaseAnalytics.getInstance(ECommerceActivation.this).logEvent("ECommerce_Enable_Forever_"+ editShareAccount2.getText().toString() , new Bundle());

                if (posCompact == false && actStatus.equals("Y")) {

                    isTouched = true;
                }
                if (changeCompact && posCompact) {

                    Intent intent = new Intent(ECommerceActivation.this, CardReviewDetails.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("accountNumber", accountNumber);
                    intent.putExtra("linkedAccounts", intent1.getStringExtra("linkedAccounts"));
                    intent.putExtra("forever", "99999999999999");
                    intent.putExtra("className", "ecommerce");
                    if (actStatus.toUpperCase().equals("Y"))
                        intent.putExtra("actStatus", "N");
                    else
                        intent.putExtra("actStatus", "Y");
                    intent.putExtra("title", accountTitle);
                    intent.putExtra("status", statusSelected);
                    intent.putExtra("cardType", cardType);
                    intent.putExtra("stan", stan);
                    intent.putExtra("rrn", rrn);
                    startActivity(intent);

                } else {

                    if (posCompact) {
                        try {
                            String inputPattern = "MMMM dd, yyyy";
                            String outputPattern = "ddMMyyyy";
                            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
                            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);
                            Date date = null;
                            String str = null;

                            date = inputFormat.parse(lblDate.getText().toString());// it's format should be same as inputPattern
                            str = outputFormat.format(date);

                            Intent intent = new Intent(ECommerceActivation.this, CardReviewDetails.class);
                            overridePendingTransition(0, 0);
                            intent.putExtra("accountNumber", accountNumber);
                            intent.putExtra("linkedAccounts", intent1.getStringExtra("linkedAccounts"));
                            if (actStatus.toUpperCase().equals("Y"))
                                intent.putExtra("actStatus", "N");
                            else
                                intent.putExtra("actStatus", "Y");

                            String finalTime = str + time;
                            intent.putExtra("date", lblDate.getText());
                            intent.putExtra("time", lblTime.getText());
                            intent.putExtra("sendTime", finalTime);
                            intent.putExtra("title", accountTitle);
                            intent.putExtra("status", statusSelected);
                            intent.putExtra("cardType", cardType);
                            intent.putExtra("className", "ecommerce");
                            intent.putExtra("stan", stan);
                            intent.putExtra("rrn", rrn);
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (!posCompact) {

                        if (actStatus.toUpperCase().equals("Y")) {

                            utils.showDilogForError("Please deactivate first and than activate with new modifications.", "WARNING");
                            validateBtn.setEnabled(true);

                        } else {

                            utils.showDilogForError("E-Commerce is already deactivated", "WARNING");
                            validateBtn.setEnabled(true);
                        }
                    } else if (isTouched) {

                        try {

                            if (!changeCompact && !posCompact) {
                                if (!TextUtils.isEmpty(actStatus) && !actStatus.equals("") && !actStatus.equals("null")) {


                                    if (actStatus.toUpperCase().equals("Y")) {

                                        utils.showDilogForError("Please deactivate first and than activate with new modifications.", "WARNING");
                                        validateBtn.setEnabled(true);

                                    } else {

                                        utils.showDilogForError("E-Commerce is already deactivated", "WARNING");
                                        validateBtn.setEnabled(true);
                                    }
                                }
                            } else {

                                String inputPattern = "MMMM dd, yyyy";
                                String outputPattern = "ddMMyyyy";
                                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
                                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);
                                Date date = null;
                                String str = null;

                                date = inputFormat.parse(lblDate.getText().toString());// it's format should be same as inputPattern
                                str = outputFormat.format(date);

                                Intent intent = new Intent(ECommerceActivation.this, CardReviewDetails.class);
                                overridePendingTransition(0, 0);
                                intent.putExtra("accountNumber", accountNumber);
                                intent.putExtra("linkedAccounts", intent1.getStringExtra("linkedAccounts"));
                                if (actStatus.toUpperCase().equals("Y"))
                                    intent.putExtra("actStatus", actStatus);
                                else
                                    intent.putExtra("actStatus", actStatus);

                                String finalTime = str + time;
                                intent.putExtra("date", lblDate.getText());
                                intent.putExtra("time", lblTime.getText());
                                intent.putExtra("sendTime", finalTime);
                                intent.putExtra("title", accountTitle);
                                intent.putExtra("status", statusSelected);
                                intent.putExtra("cardType", cardType);
                                intent.putExtra("className", "ecommerce");
                                intent.putExtra("stan", stan);
                                intent.putExtra("rrn", rrn);
                                startActivity(intent);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {

                        if (!changeCompact && !posCompact) {
                            if (!TextUtils.isEmpty(actStatus) && !actStatus.equals("") && !actStatus.equals("null")) {


                                if (actStatus.toUpperCase().equals("Y")) {

                                    utils.showDilogForError("Please deactivate first and than activate with new modifications.", "WARNING");
                                    validateBtn.setEnabled(true);

                                } else {

                                    utils.showDilogForError("E-Commerce is already deactivated", "WARNING");
                                    validateBtn.setEnabled(true);
                                }
                            }
                        }
                    }


                }
            }
        });

    }


    public void initViews() {

        try {

            CryptLib _crypt = new CryptLib();
            Intent intent = getIntent();
            intent1 = intent;
            calendar = Calendar.getInstance();
            dateFormat = DateFormat.getDateInstance(DateFormat.LONG, Locale.US);
            timeFormat = new SimpleDateFormat(TIME_PATTERN, Locale.getDefault());
            currentStatus = intent.getStringExtra("status");
            accountNumber = intent.getStringExtra("accountNumber");
            accountTitle = intent.getStringExtra("title");
            cardType = intent.getStringExtra("cardType");
            cardStatus1 = intent.getStringExtra("cardStatus");
            posCompactBtn = (SwitchCompat) findViewById(R.id.posCompactBtn);
            lblDate = (TextView) findViewById(R.id.lblDate);
            lblTime = (TextView) findViewById(R.id.lblTime);
            imageBtn = (ImageView) findViewById(R.id.imageBtn);
            agreementTxt = (TextView) findViewById(R.id.agreementTxt);

            main = (RelativeLayout) findViewById(R.id.main);

            if (cardType.contains("Gold")) {

                main.setBackgroundResource(R.drawable.group_12);
            } else if (cardType.contains("PayPak")) {

                main.setBackgroundResource(R.drawable.paypak);
            }
            statusArraylist = new ArrayList<>();
            reasonArrayList = new ArrayList<>();
            helper = Helper.getHelper(this);
            validateBtn = (TextView) findViewById(R.id.validateBtn);
            requestQueue = Volley.newRequestQueue(this);
            constants = Constants.getConstants(ECommerceActivation.this);
            utils = new Utils(ECommerceActivation.this);
            notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
            settings = (ImageView) findViewById(R.id.settings);
            pd = new Dialog(ECommerceActivation.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            chatBotText = (EditText) findViewById(R.id.chatBoxText);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            statusSpinner = (Spinner) findViewById(R.id.statusSpinner);
            reasonSpinner = (Spinner) findViewById(R.id.reasonSpinner);
            backBtn = (ImageView) findViewById(R.id.backBtn);
            editShareAccount2 = (EditText) findViewById(R.id.editShareAccount2);
            editShareAccount1 = (EditText) findViewById(R.id.editShareAccount1);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            int stanNumber = 6;
            stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
            int rrnNumber = 12;
            rrn = String.valueOf(helper.generateRandom(rrnNumber));
            posCompactBtn1 = (SwitchCompat) findViewById(R.id.posCompactBtn1);
//        editReason = (EditText) findViewById(R.id.editReason);
            errorMessageForAmount = (TextView) findViewById(R.id.errorMessageForAmount);
            cardStatus = (TextView) findViewById(R.id.cardStatus);
            holderName = (TextView) findViewById(R.id.holderName);
            accountNumberTxt = (TextView) findViewById(R.id.accountNumber);
            String firstFour = accountNumber.substring(0, 4);
            String lastFour = accountNumber.substring(12, 16);
            String secondFour = accountNumber.substring(4, 8);
            String thirdFour = accountNumber.substring(8, 12);
            cardStatus.setText(cardStatus1.toUpperCase());
            holderName.setText(accountTitle);
            accountNumberTxt.setText(firstFour + "    " + secondFour + "    " + thirdFour + "    " + lastFour);

            errorMessageForSpinner = (TextView) findViewById(R.id.errorMessageForSpinner);
            type = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            String list = constants.status.getString("statusList", "N/A");
            String reasonlist = constants.reasonList.getString("reasonList", "N/A");
            chatBotText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showDilogForErrorForLogin("Do you want to cancel this activity?", "WARNING");
                }
            });


            appPreferences = new AppPreferences(ECommerceActivation.this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }

            actStatus = intent.getStringExtra("Activated");
            activationTime = intent.getStringExtra("ActivationTime");
            if (!TextUtils.isEmpty(actStatus) && !actStatus.equals(null) && !actStatus.equals("null")) {

                if (actStatus.toUpperCase().equals("Y")) {


                    editShareAccount1.setText("Activated");
                    posCompactBtn.setChecked(true, false);
                    ecommerceStatus = true;
                    posCompactBtn1.setEnabled(true);
                    isTouched = true;

                } else {
                    editShareAccount1.setText("Deactivated");
                    posCompactBtn.setChecked(false, false);
                    ecommerceStatus = false;
                    DisableViews();

                }


            }

            if (!TextUtils.isEmpty(activationTime) && !activationTime.equals(null) && !activationTime.equals("null")) {


                if (activationTime.contains("99999999999999")) {

                    posCompactBtn1.setChecked(true, false);
                    editShareAccount2.setText("Activated");
                    DisableViewsForever();
                    cashStatus = true;
                    update();
                } else {

                    String upToNCharacters = activationTime.substring(0, Math.min(activationTime.length(), 8));
                    String timeNCharacters = activationTime.substring(8, Math.min(activationTime.length(), 14));
                    String outputPattern = "MMMM dd, yyyy";
                    String inputPattern = "ddMMyyyy";
                    SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
                    SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.US);
                    Date date = null;
                    String str = null;

                    date = inputFormat.parse(upToNCharacters);// it's format should be same as inputPattern
                    str = outputFormat.format(date);
                    lblDate.setText(str);
//                    final SimpleDateFormat sdf = new SimpleDateFormat(TIME_PATTERN);
//                    final Date dateObj = sdf.parse(timeNCharacters);
                    final SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
                    final Date dateObj = sdf.parse(timeNCharacters);
                    String setTime = new SimpleDateFormat("hh:mm aa").format(dateObj);
                    lblTime.setText(setTime);
                    posCompactBtn1.setChecked(false, false);
                    editShareAccount2.setText("Deactivated");
                    EnableViewsForever();
                    cashStatus = false;
                }

//                if (activationTime.toUpperCase().equals("Y")) {
//
//
//                    editShareAccount1.setText("Activated");
//                    posCompactBtn.setChecked(true, false);
//                    ecommerceStatus = true;
//
//
//                } else {
//                    editShareAccount1.setText("Deactivated");
//                    posCompactBtn.setChecked(false, false);
//                    ecommerceStatus = false;
//                }


            } else {
                posCompactBtn1.setChecked(false, false);
                editShareAccount2.setText("Deactivated");
                EnableViewsForever();
                cashStatus = false;
                update();
            }

            String txt = "<font COLOR=\'#73808a\'>" + "I agree to the " + "</font>"
                    + "<font COLOR=\'#0064b2\'><u>" + "Terms & Conditions" + "</u></font>";
            agreementTxt.setText(Html.fromHtml(txt));


            agreementTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    agreementTxt.setEnabled(false);
                    Intent intent = new Intent(ECommerceActivation.this, TermsAndConditionsForPOS.class);
                    intent.putExtra("Login", "login");
                    startActivity(intent);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            agreementTxt.setEnabled(true);
                        }
                    }, 500);
//                finish();
                }
            });

//            imageBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    if (isChecked) {
//                        imageBtn.setBackground(getResources().getDrawable(R.drawable.normal));
//                        isChecked = false;
//                    } else {
//                        imageBtn.setBackground(getResources().getDrawable(R.drawable.checked));
//                        isChecked = true;
//                    }
//
//                }
//            });


        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    public void GetStatusList() {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    pd.show();
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                    HttpsTrustManager.allowMySSL(ECommerceActivation.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.cardListStatusURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();

                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_SambaCardStatusListResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_SambaCardStatusListResult").getString("CardStatusList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        statusArraylist.add(jsonObj.getString("Description"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        pd.dismiss();
                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        validateBtn.setEnabled(true);

                                                    }

                                                }
                                                status = new String[statusArraylist.size()];
                                                status = statusArraylist.toArray(status);


                                                Gson gson = new Gson();
                                                String json = gson.toJson(statusArraylist);
                                                constants.statusListEditor.putString("statusList", json);
                                                constants.statusListEditor.commit();
                                                constants.statusListEditor.apply();
                                                showSpinner();

                                            } else if (jsonObject.getJSONObject("Signs_SambaCardStatusListResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_SambaCardStatusListResult").getString("Status_Description"), "ERROR", ECommerceActivation.this, new LoginScreen());
                                                validateBtn.setEnabled(true);

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_SambaCardStatusListResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            validateBtn.setEnabled(true);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    validateBtn.setEnabled(true);
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "ERROR");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);

        }
    }


    public void GetReasonList() {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    pd.show();
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);


                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                    HttpsTrustManager.allowMySSL(ECommerceActivation.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.reasonURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();

                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_CardStatusReasonListResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_CardStatusReasonListResult").getString("CardStatusList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        reasonArrayList.add(jsonObj.getString("Name"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        pd.dismiss();
                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        validateBtn.setEnabled(true);

                                                    }

                                                }
                                                reasons = new String[reasonArrayList.size()];
                                                reasons = reasonArrayList.toArray(reasons);


                                                Gson gson = new Gson();
                                                String json = gson.toJson(reasonArrayList);
                                                constants.reasonListEditor.putString("reasonList", json);
                                                constants.reasonListEditor.commit();
                                                constants.reasonListEditor.apply();
                                                showSpinnerReason();

                                            } else if (jsonObject.getJSONObject("Signs_CardStatusReasonListResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_CardStatusReasonListResult").getString("Status_Description"), "ERROR", ECommerceActivation.this, new LoginScreen());
                                                validateBtn.setEnabled(true);

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_CardStatusReasonListResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            validateBtn.setEnabled(true);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    validateBtn.setEnabled(true);
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "ERROR");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);

        }
    }

    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {


                spinnerAdapter statusAdapter = new spinnerAdapter(ECommerceActivation.this, R.layout.custom_textview_fp);
                statusAdapter.addAll(status);
                statusAdapter.add("Card Status");
                statusSpinner.setAdapter(statusAdapter);
                statusSpinner.setSelection(0);
                statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


                            // Get select item
                            if (statusSpinner.getSelectedItem() == "Card Status") {
                                statusSelected = statusSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + statusSelected);

                            } else {

                                statusSelected = statusSpinner.getSelectedItem().toString();

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });

                pd.dismiss();


            }
        }, 1000);
    }

    public void showSpinnerReason() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {


                spinnerAdapter statusAdapter = new spinnerAdapter(ECommerceActivation.this, R.layout.custom_textview_fp);
                statusAdapter.addAll(reasons);
                statusAdapter.add("Reason");
                reasonSpinner.setAdapter(statusAdapter);
                reasonSpinner.setSelection(0);
                reasonSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


                            // Get select item
                            if (reasonSpinner.getSelectedItem() == "Card Status") {
                                reason = reasonSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + reason);

                            } else {

                                reason = reasonSpinner.getSelectedItem().toString();

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });

                pd.dismiss();


            }
        }, 1000);
    }

    public void showDilogForErrorForLogin(String msg, String header) {

        dialog = new Dialog(ECommerceActivation.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();


    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lblDate:
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//                new DatePickerDialog(this, this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                datePickerDialog.show();
                break;
            case R.id.lblTime:
                new TimePickerDialog(this, this, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true).show();
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        calendar.set(year, monthOfYear, dayOfMonth);
        update();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        final Calendar mcurrentTime = Calendar.getInstance();


        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        if (calendar.getTimeInMillis() > mcurrentTime.getTimeInMillis()) {
            update();
        } else {

            Toast.makeText(this, "Please select the time greater than the current time.", Toast.LENGTH_SHORT).show();
        }
    }

    private void update() {
        lblDate.setText(dateFormat.format(calendar.getTime()));
        lblTime.setText(timeFormat.format(calendar.getTime()));
        time = new SimpleDateFormat("HHmmss").format(calendar.getTime());
    }

    public void DisableViews() {

        posCompactBtn1.setEnabled(false);
        lblDate.setEnabled(false);
        lblTime.setEnabled(false);
        lblDate.setTextColor(getResources().getColor(R.color.circle));
        lblTime.setTextColor(getResources().getColor(R.color.circle));
    }

    public void DisableViewsForever() {

        lblDate.setEnabled(false);
        lblTime.setEnabled(false);
        lblDate.setTextColor(getResources().getColor(R.color.circle));
        lblTime.setTextColor(getResources().getColor(R.color.circle));
    }

    public void EnableViewsForever() {

        lblDate.setEnabled(true);
        lblTime.setEnabled(true);
        lblDate.setTextColor(getResources().getColor(R.color.iPinText));
        lblTime.setTextColor(getResources().getColor(R.color.iPinText));
    }

    public void EnableViews() {

        posCompactBtn1.setEnabled(true);
        lblDate.setEnabled(true);
        lblTime.setEnabled(true);
        lblDate.setTextColor(getResources().getColor(R.color.iPinText));
        lblTime.setTextColor(getResources().getColor(R.color.iPinText));
    }
}