package com.ceesolutions.samba.eatMubarak;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.beneficiariesManagement.BeneficiaryFormScreen;
import com.ceesolutions.samba.beneficiariesManagement.SelectBankScreen;
import com.ceesolutions.samba.transactions.ManageAccountsScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.CircularTextView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.techworks.eatmubaraklibrary.EatMubarakWidget;
import com.techworks.eatmubaraklibrary.UserInfo;

import java.util.ArrayList;

/**
 * Created by ceeayaz on 4/1/18.
 */

public class EatMubarakMainScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private ListView listView;
    private CustomListAdapter customListAdapter;
    private ArrayList<String> stringArrayList;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private ImageView backBtn;
    private AppPreferences appPreferences;
    private String location;
    CryptLib _crypt;
    private int intentCode = 101;
    private int intentCodeOrderList = 102;
    String userNumber = "";
    String userEmail = "";
    String userName = "";
    boolean restartActivity = false;

    BroadcastReceiver analyticsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBundleExtra("EVENT_LOGIN") != null) {
                String userName = intent.getBundleExtra("EVENT_LOGIN")
                        .getString("UserName");
                String userPhone = intent.getBundleExtra("EVENT_LOGIN")
                        .getString("UserPhone");
                String timeStamp = intent.getBundleExtra("EVENT_LOGIN")
                        .getString("TimeStamp");

                try {
                    Bundle bundle = new Bundle();
                    bundle.putString("userName", userName);
                    bundle.putString("userPhone", userPhone);
                    bundle.putString("timeStamp", timeStamp);
                    FirebaseAnalytics.getInstance(context).logEvent("EM_EVENT_LOGIN_", bundle);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.d("EVENT_LOGIN", "userName : " + userName + "---- userPhone : " + userPhone + "---- timeStamp : " + timeStamp);
                //Add your code...
            } else if (intent.getBundleExtra("EVENT_BEGINCHECKOUT") != null) {
                String restName = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("RestName");
                String branchAddress = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("BranchAddress");
                String userName = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("UserName");
                String userPhone = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("UserPhone");
                String subtotal = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("Subtotal");
                String timeStamp = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("TimeStamp");
                //Add your code...

                try {
                    Bundle bundle = new Bundle();
                    bundle.putString("restName", restName);
                    bundle.putString("branchAddress", branchAddress);
                    bundle.putString("userName", userName);
                    bundle.putString("userPhone", userPhone);
                    bundle.putString("subtotal", subtotal);
                    bundle.putString("timeStamp", timeStamp);
                    FirebaseAnalytics.getInstance(context).logEvent("EVENT_BEGINCHECKOUT", bundle);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else if (intent.getBundleExtra("EVENT_RESTAURANTVIEWED") != null) {
                String restName = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("RestName");
                String branchAddress = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("BranchAddress");
                String userName = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("UserName");
                String userPhone = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("UserPhone");
                String timeStamp = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("TimeStamp");
                //Add your code...
                try {
                    Bundle bundle = new Bundle();
                    bundle.putString("restName", restName);
                    bundle.putString("branchAddress", branchAddress);
                    bundle.putString("userName", userName);
                    bundle.putString("userPhone", userPhone);
                    bundle.putString("timeStamp", timeStamp);
                    FirebaseAnalytics.getInstance(context).logEvent("EVENT_RESTAURANTVIEWED", bundle);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.eat_mubarak_main_layout);
        getSupportActionBar().hide();

        //initViews();

    }

    public void initViews() {

        try {
            _crypt = new CryptLib();


            listView = (ListView) findViewById(R.id.listView);
            backBtn = (ImageView) findViewById(R.id.backBtn);
            stringArrayList = new ArrayList<>();
            utils = new Utils(EatMubarakMainScreen.this);
            constants = Constants.getConstants(EatMubarakMainScreen.this);
            pd = new Dialog(EatMubarakMainScreen.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);

            userNumber = _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            userEmail = _crypt.decrypt(constants.sharedPreferences.getString("EmailAddress", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
            userName = _crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    finish();
                }
            });


            initEatMubarakList();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initEatMubarakList() {

        try {
            stringArrayList = new ArrayList<>();
            stringArrayList.add("Order Food");
            stringArrayList.add("Order History");
//            stringArrayList.add("Feedback");

            customListAdapter = new CustomListAdapter(stringArrayList, EatMubarakMainScreen.this);
            listView.setAdapter(customListAdapter);


        } catch (Exception e) {

            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }

    public class CustomListAdapter extends BaseAdapter {
        ArrayList<String> stringArrayList;
        Context context;
        TextView bankName;
        ImageView image;


        public CustomListAdapter(ArrayList<String> userModelArrayList, Context mContext) {
            stringArrayList = userModelArrayList;
            context = mContext;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return stringArrayList.size();
        }

        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.eat_mubark_list_item, parent, false);
            }


            bankName = (TextView) convertView.findViewById(R.id.bankName);
            image = (ImageView) convertView.findViewById(R.id.image);
            bankName.setText(stringArrayList.get(position));


            if (position == 0) {
                image.setImageResource(R.drawable.eat_mubarak_icon_left);
            } else if (position == 1) {
                image.setImageResource(R.drawable.eat_mubarak_icon_left);
            }


            View finalConvertView = convertView;
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {


                        finalConvertView.setEnabled(false);
                        if (position == 0) {
                            pd.show();
                            openEatMubarakSDK();
                        } else if (position == 1) {
                            pd.show();
                            openEatMubarakOrderList();
                        }

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                finalConvertView.setEnabled(true);
                            }
                        }, 1000);

                    } catch (Exception e) {
                        finalConvertView.setEnabled(true);
                        e.printStackTrace();
                    }
                }
            });

            return convertView;
        }
    }

    private void openEatMubarakOrderList() {
        try {
            LocalBroadcastManager.getInstance(EatMubarakMainScreen.this).registerReceiver(analyticsReceiver,
                    new IntentFilter("EM_ANALYTICS"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        latitude = 0.0;
        longitude = 0.0;


        try {

            UserInfo userInfo = new UserInfo();
            userInfo.setUserContact("+92" + userNumber); // User's Phone Number
            userInfo.setUserEmail(userEmail); // User's Email Address
            userInfo.setUserName(userName); // User's Name
            Intent intent = EatMubarakWidget.Builder.getBuilder()
                    .setUserInfo(userInfo) // User Information Object
                    .setApiKey("h510SGLaSB")
                    .setStaging(false)
                    .getOrderList(true)
                    .setTimeOut(120) // Set Time in Seconds
                    .build(this); // Your Current Context

            startActivityForResult(intent, intentCodeOrderList);


        } catch (Exception e) {
            if (pd.isShowing())
                pd.dismiss();
            e.printStackTrace();
        }


    }

    private void openEatMubarakSDK() {

        try {
            LocalBroadcastManager.getInstance(EatMubarakMainScreen.this).registerReceiver(analyticsReceiver,
                    new IntentFilter("EM_ANALYTICS"));
        } catch (Exception e) {
            e.printStackTrace();
        }


        latitude = 0.0;
        longitude = 0.0;

        try {
            appPreferences = new AppPreferences(this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");


            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                latitude = Double.valueOf(latitude1);
                longitude = Double.valueOf(longitude1);
            }
        } catch (Exception e) {
            if (pd.isShowing())
                pd.dismiss();
            e.printStackTrace();
        }


        try {
            UserInfo userInfo = new UserInfo();
            userInfo.setUserContact("+92" + userNumber); // User's Phone Number
            userInfo.setUserEmail(userEmail); // User's Email Address
            userInfo.setUserName(userName); // User's Name
            Intent intent = EatMubarakWidget.Builder.getBuilder()
                    .setUserInfo(userInfo) // User Information Object
                    .setLocation(longitude, latitude) //SetLocation(Longitude,Latitude)
                    .setApiKey("h510SGLaSB")
                    .setStaging(false)
                    .setTimeOut(120) // Set Time in Seconds
                    .build(this); // Your Current Context

            startActivityForResult(intent, intentCode);

        } catch (Exception e) {
            if (pd.isShowing())
                pd.dismiss();
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == intentCode) {

                try {

                    String orderId = data.getStringExtra("orderId");
                    String amount = data.getStringExtra("orderAmount");
                    String restaurantName = data.getStringExtra("restaurantName");
                    String deliveryAddress = data.getStringExtra("deliveryAddress");
                    String deliveryEta = data.getStringExtra("deliveryEta");
                    String isCod = data.getStringExtra("isCod");
                    String discount = data.getStringExtra("discount");
                    String promoCode = data.getStringExtra("promoCode");

                    //Don’t forget to add this line if receiver is registered to catch
                    //EM_ANALYTICS.

                    try {
                        LocalBroadcastManager.getInstance(this)
                                .unregisterReceiver(analyticsReceiver);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    //Add your code

                    Intent intent = new Intent(EatMubarakMainScreen.this, EatMubarkDetails.class);
                    if (orderId != null)
                        intent.putExtra("orderId", orderId);
                    if (amount != null)
                        intent.putExtra("amount", amount);
                    if (restaurantName != null)
                        intent.putExtra("restaurantName", restaurantName);
                    if (deliveryAddress != null)
                        intent.putExtra("deliveryAddress", deliveryAddress);
                    if (deliveryEta != null)
                        intent.putExtra("deliveryEta", deliveryEta);
                    if (isCod != null)
                        intent.putExtra("isCod", isCod);
                    if (discount != null)
                        intent.putExtra("discount", discount);
                    if (promoCode != null)
                        intent.putExtra("promoCode", promoCode);
                    startActivity(intent);
//                    finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        } else {
            if (requestCode == intentCode) {

                try {
                    LocalBroadcastManager.getInstance(this)
                            .unregisterReceiver(analyticsReceiver);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                restartActivity = true;
                onStop();
                finish();


            } else if (requestCode == intentCodeOrderList) {

                try {
                    LocalBroadcastManager.getInstance(this)
                            .unregisterReceiver(analyticsReceiver);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                restartActivity = true;
                onStop();
                finish();


            }
        }
    }

    @Override
    protected void onStop() {
        if (pd.isShowing())
            pd.dismiss();

        if(restartActivity)
        {
            restartActivity = false;
            Intent intent = new Intent(EatMubarakMainScreen.this, EatMubarakMainScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);
        }


        super.onStop();
    }

    @Override
    protected void onResume() {
        //initViews();
        pd = new Dialog(EatMubarakMainScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pd.dismiss();
                initViews();
            }
        }, 1000);
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        if (pd.isShowing())
            pd.dismiss();


        super.onDestroy();
    }
}

