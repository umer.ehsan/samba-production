package com.ceesolutions.samba.eatMubarak;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.billPayments.PinVerificationScreen;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.EnglishNumberToWords;
import com.google.firebase.analytics.FirebaseAnalytics;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class  EatMubarakDetailsReview extends AppCompatActivity {
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    boolean flag = false;
    private ExpandableLayout expandableLayout0;
    private RelativeLayout firstRow;
    private TextView message, billerTxt, mainTxt, textView, doneBtn, heading, date, userNameTxt, accMaskTxt, friendNameTxt, accFrindMaskTxt, accFrindEmailTxt, accFrindMobileTxt, totalAmountTxt, amountInWordsTxt, purposeHeadingTxt, remarksHeadingTxt, conversionHeadingTxt, chargesHeadingTxt, reviewBtn,RateText;
    private ImageView backBtn, cancelBtn, notificationBtn, profile_image;
    private Dialog dialog;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private String amountFinal, amountWords, finalDate, currentTime;
    private Intent intent1;
    private Date currentDate;
    private AppPreferences appPreferences;
    private String location, stan,conversionRate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.eatmubarak_detail_review);
        initViews();
        getSupportActionBar().hide();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");
                Intent intent = new Intent(EatMubarakDetailsReview.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
                finishAffinity();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        reviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reviewBtn.setEnabled(false);
                try {
                    FirebaseAnalytics.getInstance(EatMubarakDetailsReview.this).logEvent("EM_iPin_Pressed_" + getIntent().getStringExtra("type"), new Bundle());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                GenerateiPIN();

            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EatMubarakDetailsReview.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
            }
        });
    }

    public void initViews() {


        date = (TextView) findViewById(R.id.date);
        userNameTxt = (TextView) findViewById(R.id.userNameTxt);
        accMaskTxt = (TextView) findViewById(R.id.accMaskTxt);
        friendNameTxt = (TextView) findViewById(R.id.friendNameTxt);
        accFrindMaskTxt = (TextView) findViewById(R.id.accFrindMaskTxt);
        totalAmountTxt = (TextView) findViewById(R.id.totalAmountTxt);
        billerTxt = (TextView) findViewById(R.id.billerTxt);
        accFrindMobileTxt = (TextView) findViewById(R.id.accFrindMobileTxt);
        amountInWordsTxt = (TextView) findViewById(R.id.amountInWordsTxt);
        purposeHeadingTxt = (TextView) findViewById(R.id.purposeHeadingTxt);
        remarksHeadingTxt = (TextView) findViewById(R.id.remarksHeadingTxt);
        conversionHeadingTxt = (TextView) findViewById(R.id.conversionHeadingTxt);
        chargesHeadingTxt = (TextView) findViewById(R.id.chargesHeadingTxt);
        reviewBtn = (TextView) findViewById(R.id.reviewBtn);
        cancelBtn = (ImageView) findViewById(R.id.cancelBtn);
        profile_image = (ImageView) findViewById(R.id.profile_image);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        mainTxt = (TextView) findViewById(R.id.mainTxt);
        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        firstRow = (RelativeLayout) findViewById(R.id.firstRow);
        requestQueue = Volley.newRequestQueue(this);
        RateText=findViewById(R.id.rateText);
        constants = Constants.getConstants(EatMubarakDetailsReview.this);
        webService = new WebService();
        helper = Helper.getHelper(this);
        int stanNumber = 6;
        stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.US);
        currentDate = Calendar.getInstance().getTime();
        formatter = new SimpleDateFormat("dd-MMM-yyyy");
        date.setText(formatter.format(currentDate));
        finalDate = formatter.format(currentDate);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
        currentTime = sdf.format(currentDate);
        utils = new Utils(EatMubarakDetailsReview.this);
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        firstRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!flag) {
                    expandableLayout0.expand();
                    mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_less, 0);
                    flag = true;
                } else {
                    expandableLayout0.collapse();
                    mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_more, 0);
                    flag = false;
                }

            }
        });


        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Intent intent = getIntent();
        intent1 = intent;
        userNameTxt.setText(intent.getStringExtra("userName"));
        billerTxt.setText(getIntent().getStringExtra("service_provider"));
        accMaskTxt.setText((intent.getStringExtra("accountNumber")));
        purposeHeadingTxt.setText(intent.getStringExtra("purpose"));
        friendNameTxt.setText(intent.getStringExtra("companyName"));
        accFrindMaskTxt.setText(intent.getStringExtra("mobile"));
        remarksHeadingTxt.setText(intent.getStringExtra("remarks"));
        Double amount = Double.valueOf(intent.getStringExtra("amount"));
        String amountCode = "PKR";

        totalAmountTxt.setText(amountCode + " " + (amount));
        amountFinal = amountCode + " " + (amount);
        String amountInWords = EnglishNumberToWords.convert(Integer.valueOf(String.valueOf(amount).split("\\.")[0]));
        amountInWordsTxt.setText(amountCode + " " + amountInWords + " rupees only");
        amountWords = amountCode + " " + amountInWords + " rupees only";


        conversionHeadingTxt.setText(intent.getStringExtra("conversionRate"));
        chargesHeadingTxt.setText(intent.getStringExtra("chargeCode"));
       // RateText.setText(intent.getStringExtra("ConversionRate"));

    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(EatMubarakDetailsReview.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
//        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


    private String getFormatedAmount(int amount) {
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }


    public void GenerateiPIN() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                dialog.show();

                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Username", user);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(EatMubarakDetailsReview.this);

                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("00")) {
                                            {
                                                Intent intent = new Intent(EatMubarakDetailsReview.this, PinVerificationScreen.class);
                                                intent.putExtra("userName", intent1.getStringExtra("userName"));
                                                intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                                                intent.putExtra("purpose", intent1.getStringExtra("purpose"));
                                                intent.putExtra("distributorID", intent1.getStringExtra("distributorID"));
                                                intent.putExtra("distributorName", intent1.getStringExtra("distributorName"));
                                                intent.putExtra("remarks", intent1.getStringExtra("remarks"));
                                                intent.putExtra("amount", intent1.getStringExtra("amount"));
                                                intent.putExtra("branchCode", intent1.getStringExtra("branchCode"));
                                                intent.putExtra("amountFinal", amountFinal);
                                                intent.putExtra("amountWords", amountWords);
                                                intent.putExtra("currency", intent1.getStringExtra("currency"));
                                                intent.putExtra("transactionAmount", intent1.getStringExtra("transactionAmount"));
//                                                intent.putExtra("tipAmount", intent1.getStringExtra("tipAmount"));
                                                intent.putExtra("stan", stan);
                                                intent.putExtra("rrn", intent1.getStringExtra("rrn"));
                                                intent.putExtra("currentDate", finalDate);
                                                intent.putExtra("currentTime", currentTime);
                                                intent.putExtra("companyName", intent1.getStringExtra("companyName"));
                                                intent.putExtra("type", getIntent().getStringExtra("type"));
//                                                intent.putExtra("ftType", intent1.getStringExtra("ftType"));
//                                                intent.putExtra("friendNumber", intent1.getStringExtra("friendNumber"));
//                                                intent.putExtra("messageId", intent1.getStringExtra("messageId"));
//                                                intent.putExtra("targetId", intent1.getStringExtra("targetId"));
                                                intent.putExtra("mobile", intent1.getStringExtra("mobile"));
//                                                intent.putExtra("bookingDate", intent1.getStringExtra("bookingDate"));
                                                intent.putExtra("categoryType", intent1.getStringExtra("service_provider"));
                                                intent.putExtra("distributorAccountNumber", intent1.getStringExtra("distributorAccountNumber"));
                                                intent.putExtra("orderId", intent1.getStringExtra("orderId"));
                                                intent.putExtra("discount", intent1.getStringExtra("discount"));
                                                intent.putExtra("deliveryAddress", intent1.getStringExtra("deliveryAddress"));
                                                intent.putExtra("deliveryEta", intent1.getStringExtra("deliveryEta"));
                                                intent.putExtra("promoCode", intent1.getStringExtra("promoCode"));
                                                intent.putExtra("amount", intent1.getStringExtra("amount"));
                                                intent.putExtra("restaurantName", intent1.getStringExtra("restaurantName"));
                                                overridePendingTransition(0, 0);
                                                dialog.dismiss();
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
//                                constants.accountsEditor.clear();
//                                constants.accountsEditor.commit();
//                                constants.accountsEditor.apply();
//                                constants.destinationAccountEditor.clear();
//                                constants.destinationAccountEditor.commit();
//                                constants.destinationAccountEditor.apply();
//                                dialog.dismiss();
//                                finish();
//                                finishAffinity();
                                                reviewBtn.setEnabled(true);
                                            }
                                        } else if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("54")) {
                                            dialog.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR", EatMubarakDetailsReview.this, new LoginScreen());
                                            reviewBtn.setEnabled(true);

                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR");
                                            reviewBtn.setEnabled(true);
                                        }
                                    } else {
                                        dialog.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        reviewBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    reviewBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    reviewBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    reviewBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                reviewBtn.setEnabled(true);


            }
        } catch (Exception e) {
            dialog.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            reviewBtn.setEnabled(true);


        }
    }


//    public void showDilogForErrorForLogin(String msg, String header) {
//
//        dialog = new Dialog(BookMeDetailsReview.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.error_dialog);
//        message = (TextView) dialog.findViewById(R.id.validatingTxt);
//        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
//        heading = (TextView) dialog.findViewById(R.id.heading);
//        dialog.setCancelable(true);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        doneBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                dialog.dismiss();
//                finish();
//
//            }
//        });
//        message.setText(msg);
//        heading.setText(header);
//        dialog.show();
//
//
//    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
//        showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");
    }
}


