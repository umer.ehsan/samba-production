package com.ceesolutions.samba.eatMubarak;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.billPayments.BillReviewScreen;
import com.ceesolutions.samba.transactions.SelectAccountScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class EatMubarkDetails extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    String res = "", res2 = "";
    String transactionAmountInteger;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private ExpandableLayout expandableLayout0;
    private CardView cardView;
    private Constants constants;
    private Dialog pd;
    private String remarks, type;
    private TextInputLayout amountTxtInput4, amountTxtInput, remarksTxtInput5;
    private EditText editAmount, editRemarks, payabaleAmount, editAmount4, editRemarks5;
    private RelativeLayout upperPart1, upperPart2;
    private TextView consumerIDTxt, consumerNumberTxt, billingTxt, billingMonthTxt, dueDateTxt, payableBeforeTxt, payableAfterTxt, statusTxt, mainTxt, errorMessageForAmount, errorMessageForRemarks, errorMessageForSpinner, userNameTxt, accMaskTxt, amountTxt, friendNameTxt, itemName, validateBtn;
    private TextView additionalTxt, conversionHeading, chargesHeading, conversionHeading2, chargesHeading2, conversionHeading3, payableAfterTxt4, chargesHeading3, statusTxt4, conversionHeading4, payableAfterTxt5, chargesHeading4, statusTxt5;
    private Spinner purposesSpinner;
    private String purposeSelected;
    private String[] purpose;
    private ImageView arrowDestination, backBtn;
    private RelativeLayout conversion4, conversion3;
    private FrameLayout relativeLayout4, relativeLayout5;
    private ArrayList<String> purpStringArrayList;
    private String sourceBalance, sourceuserName, sourceAccount, sourceCurrency, distributorID, distributorAccountNumber, mobileNumber, distributorName, length;
    private Utils utils;
    private boolean isSource = false;
    private boolean isDestination = false;
    private boolean isFirst = false;
    private String identityValue, alias, stan, rrn;
    private String beneType = "SAMBA";
    private TextView doneBtn, textView, heading;
    private Dialog dialog;
    private String minAmount, categoryType, billConsumerNumber, sourceminAmount, amountAfterDue, amountBeforeDue, billStatus, billingMonth, dueDate, consumerName, companyName;
    private String maxAmount, branchCode, mobile, email, messageId, targetId, friendNumber;
    private float amountToInt, maxAmountToInt, minAmountToInt;
    private int bal;
    private boolean flag = false;
    private Intent intent1;
    private String isSelectSource, isSelectDestination;
    private boolean isVisible = false;
    private AppPreferences appPreferences;
    private String location;
    private View dueView4;
    private boolean isPostpaid = false;
    private JSONObject jsonObject;
    private TextView headingTxt;
    String eatMubarakConversion;
    ImageView notificationBtn;
    String restaurantName = "";
    String orderId = "";
    String promoCode = "";
    String discount = "";
    String deliveryEta = "";
    String deliveryAddress = "";
    String amount = "";
    private Dialog dialogPopup;
    float Sourcecurrency,Amountpayble;
    double discountAmountcheck;



    public static String toTitleCase(String str) {

        if (str == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }

        return builder.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.eatmubark_review);
        getSupportActionBar().hide();

        initViews();

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        editRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                remarks = editable.toString();
            }
        });


        editAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                res = charSequence.toString();

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                res = charSequence.toString();

                String tipIndicator = Constants.pushPaymentData.getTipOrConvenienceIndicator();
                Double tipFixed = Constants.pushPaymentData.getValueOfConvenienceFeeFixed();
                Double tipPercentage = Constants.pushPaymentData.getValueOfConvenienceFeePercentage();


                Double amount = Double.valueOf(res);

                if (tipIndicator != null && !TextUtils.isEmpty(tipIndicator)) {
                    if (tipIndicator.equals("01")) {
                        Float tip = Float.valueOf(editAmount4.getText().toString());
                        amount = amount + tip;

                    } else if (tipIndicator.equals("02")) {
                        if (tipFixed != null && tipFixed != 0) {
                            Double tipAmount = (tipFixed);
                            amount = amount + tipAmount;

                        } else {
                            Double tipFixedInput = Double.valueOf(editAmount4.getText().toString());
                            if (tipFixedInput != null && tipFixedInput != 0) {
                                Double tipAmount = tipFixedInput;
                                amount = amount + tipAmount;

                            }
                        }
                    } else if (tipIndicator == "03") {
                        if (tipPercentage != null && tipPercentage != 0) {
                            Double tipAmount = tipPercentage;
                            amount = amount + (amount * tipAmount / 100);

                        }
                    }
                }


                transactionAmountInteger = String.valueOf(amount);
                editRemarks5.setText(transactionAmountInteger);
            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });

        editAmount4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                res2 = charSequence.toString();

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                Double paymentAmount = Double.valueOf(editAmount.getText().toString());
                res2 = charSequence.toString();
                Double amount = Double.valueOf(res2);
                Double totalAmount = amount + paymentAmount;
                transactionAmountInteger = String.valueOf(totalAmount);
                editRemarks5.setText(transactionAmountInteger);


            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });


        upperPart1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                upperPart1.setEnabled(false);
                isSource = true;
                isDestination = false;
                isFirst = true;
                Intent intent = new Intent(EatMubarkDetails.this, SelectAccountScreen.class);
                intent.putExtra("source", "source");
                startActivity(intent);


            }
        });

        upperPart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                upperPart2.setEnabled(false);
//                Intent intent = new Intent(QRPaymentDetails.this, ManageBillerScreen.class);
//                intent.putExtra("source", "destination");
//                startActivity(intent);
//                isDestination = true;
//                isSource = false;
//                isFirst = true;


            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateBtn.setEnabled(false);


                try {
                    FirebaseAnalytics.getInstance(EatMubarkDetails.this).logEvent("EM_Review_Btn_Pressed", new Bundle());
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if ((TextUtils.isEmpty(sourceAccount) || sourceAccount.equals("N/A")) || (TextUtils.isEmpty(sourceBalance) || sourceBalance.equals("N/A")) || (TextUtils.isEmpty(sourceuserName) || sourceuserName.equals("N/A"))) {

                    utils.showDilogForError("Please select Source account.", "WARNING");
                    validateBtn.setEnabled(true);

                }
//                else {
//                    if (!sourceCurrency.equals("PKR")) {
//
//                        utils.showDilogForError("EatMubarak not available for FCY accounts.", "ERROR");
//                        validateBtn.setEnabled(true);
//
//                    }
                   else {
                        if (TextUtils.isEmpty(payabaleAmount.getText().toString()) || payabaleAmount.getText().toString().equals("0.0") || editRemarks5.getText().toString().equals("0")) {

                            utils.showDilogForError("Please enter valid amount.", "ERROR");
                            validateBtn.setEnabled(true);
                        } else if (Amountpayble>Sourcecurrency)
                        {
                            utils.showDilogForError("Order amount cannot exceed account balance", "ERROR");
                            validateBtn.setEnabled(true);
                        }

                        else {
                            getTitleFetch();

                        }

                    }


            }
        });


    }

    public void initViews() {

        try {

            CryptLib _crypt = new CryptLib();
            purposesSpinner = findViewById(R.id.purposeSpinner);
            notificationBtn = findViewById(R.id.notificationBtn);
            validateBtn = findViewById(R.id.validateBtn);
            arrowDestination = findViewById(R.id.arrowDestination);
            consumerIDTxt = findViewById(R.id.consumerIDTxt);
            dueDateTxt = findViewById(R.id.dueDateTxt);
            billingMonthTxt = findViewById(R.id.billingMonthTxt);
            consumerNumberTxt = findViewById(R.id.consumerNumberTxt);
            billingTxt = findViewById(R.id.billingTxt);
            payableAfterTxt = findViewById(R.id.payableAfterTxt);
            payableBeforeTxt = findViewById(R.id.payableBeforeTxt);
            statusTxt = findViewById(R.id.statusTxt);
            dueView4 = findViewById(R.id.dueView4);
            upperPart1 = findViewById(R.id.upperPart1);
            upperPart2 = findViewById(R.id.upperPart2);
            constants = Constants.getConstants(EatMubarkDetails.this);
            friendNameTxt = findViewById(R.id.friendNameTxt);
            payabaleAmount = findViewById(R.id.payabaleAmount);
            remarksTxtInput5 = findViewById(R.id.remarksTxtInput5);
            cardView = findViewById(R.id.card_view);
            userNameTxt = findViewById(R.id.userNameTxt);
            userNameTxt.setText("Select Source Account");
            accMaskTxt = findViewById(R.id.accMaskTxt);
            accMaskTxt.setVisibility(View.INVISIBLE);
            amountTxt = findViewById(R.id.amountTxt);
            amountTxt.setVisibility(View.INVISIBLE);
            itemName = findViewById(R.id.itemName);
            editRemarks5 = findViewById(R.id.editRemarks5);
            amountTxtInput4 = findViewById(R.id.amountTxtInput4);
            amountTxtInput = findViewById(R.id.amountTxtInput);
            editAmount4 = findViewById(R.id.editAmount4);
            relativeLayout4 = findViewById(R.id.relativeLayout4);
            conversion4 = findViewById(R.id.conversion4);
            conversion3 = findViewById(R.id.conversion3);
            backBtn = findViewById(R.id.backBtn);
            headingTxt = findViewById(R.id.headingTxt);
            relativeLayout5 = findViewById(R.id.relativeLayout5);
            conversionHeading = findViewById(R.id.conversionHeading);
            chargesHeading = findViewById(R.id.chargesHeading);
            conversionHeading2 = findViewById(R.id.conversionHeading2);
            chargesHeading2 = findViewById(R.id.chargesHeading2);
            additionalTxt = findViewById(R.id.additionalTxt);
            conversionHeading3 = findViewById(R.id.conversionHeading3);
            payableAfterTxt4 = findViewById(R.id.payableAfterTxt);
            chargesHeading3 = findViewById(R.id.chargesHeading3);
            statusTxt4 = findViewById(R.id.statusTxt);
            conversionHeading4 = findViewById(R.id.conversionHeading4);
            payableAfterTxt5 = findViewById(R.id.payableAfterTxt4);
            chargesHeading4 = findViewById(R.id.chargesHeading4);
            statusTxt5 = findViewById(R.id.statusTxt4);
            errorMessageForAmount = findViewById(R.id.errorMessageForAmount);
            errorMessageForRemarks = findViewById(R.id.errorMessageForRemarks);
            errorMessageForSpinner = findViewById(R.id.errorMessageForSpinner);
            editRemarks = findViewById(R.id.editRemarks);
            editAmount = findViewById(R.id.editAmount);
            expandableLayout0 = findViewById(R.id.expandable_layout_0);
            requestQueue = Volley.newRequestQueue(EatMubarkDetails.this);
            mainTxt = findViewById(R.id.mainTxt);

            pd = new Dialog(EatMubarkDetails.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);


            headingTxt.setText(toTitleCase("food payment"));

            if (getIntent().hasExtra("restaurantName")) {
                restaurantName = getIntent().getStringExtra("restaurantName");
                itemName.setText(restaurantName);
            }

            if (getIntent().hasExtra("amount")) {
                amount = (getIntent().getStringExtra("amount"));
                payabaleAmount.setText(amount + " PKR");

                Amountpayble=Float.parseFloat(amount);

            }
            if (getIntent().hasExtra("discount")) {
                discount = getIntent().getStringExtra("discount");
                billingMonthTxt.setText(discount);
                discountAmountcheck =Double.parseDouble(discount);

            }

            if (getIntent().hasExtra("promoCode")) {

                if (getIntent().getStringExtra("promoCode").equalsIgnoreCase(""))
                    billingTxt.setText("-");
                else {
                    promoCode = getIntent().getStringExtra("promoCode");
                    if(discountAmountcheck==0)
                    {
                        billingTxt.setText("");
                    }
                    else {
                        billingTxt.setText(promoCode);
                    }
                }
            }



            if (getIntent().hasExtra("orderId")) {
                orderId = getIntent().getStringExtra("orderId");
                dueDateTxt.setText(orderId);
            }

            if (getIntent().hasExtra("deliveryEta")) {
                deliveryEta = getIntent().getStringExtra("deliveryEta");
                payableBeforeTxt.setText(deliveryEta);
            }

            if (getIntent().hasExtra("deliveryAddress")) {
                deliveryAddress = getIntent().getStringExtra("deliveryAddress");
                payableAfterTxt.setText(deliveryAddress);
            }


            helper = Helper.getHelper(EatMubarkDetails.this);
            constants = Constants.getConstants(EatMubarkDetails.this);
            pd = new Dialog(EatMubarkDetails.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);

            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onBackPressed();
                }
            });

            purpStringArrayList = new ArrayList<>();
            GetQRGatewayDetails();
           // getTitleFetch();


            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            appPreferences = new AppPreferences(EatMubarkDetails.this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");

            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

            }
            purpStringArrayList = new ArrayList<>();
            utils = new Utils(EatMubarkDetails.this);


            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!flag) {
                        expandableLayout0.expand();
                        mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_less, 0);
                        flag = true;
                    } else {
                        expandableLayout0.collapse();
                        mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_more, 0);
                        flag = false;
                    }

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getTitleFetch() {

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                int stanNumber = 6;
                stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
                int rrnNumber = 12;
                rrn = String.valueOf(helper.generateRandom(rrnNumber));
                CryptLib _crypt = new CryptLib();

                String bankCode="69";

                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sToAccountNumber","2000701923");
                params.put("sBankID", bankCode);
                params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sSTAN", stan);
                params.put("sRRN", rrn);
                params.put("sBeneType", beneType);
                params.put("sFromAccountNumber", sourceAccount);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sBeneCurrencyCode",sourceCurrency);
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(EatMubarkDetails.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.titleFetchURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Code").equals("00")) {

                                            eatMubarakConversion=jsonObject.getJSONObject("Signs_TitleFetchResult").getString("ConversionRate");

                                            Verification();



//
//
                                        } else if (jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Description"), "ERROR", EatMubarkDetails.this, new LoginScreen());
                                            validateBtn.setEnabled(true);

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                        }

                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }
                            }
                        })
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);

        }

    }


    public void GetQRGatewayDetails() {


        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    Constants.filterGatewayName = "EMOTP";
                    int stanNumber = 6;
                    String stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
                    int rrnNumber = 12;
                    String rrn = String.valueOf(Helper.generateRandom(rrnNumber));
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sSigns_LegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                    params.put("sServerSessionKey", md5);
                    params.put("sSTAN", stan);
                    params.put("sRRN", rrn);
                    params.put("sSigns_FilterGatewayName", Constants.filterGatewayName);
                    HttpsTrustManager.allowMySSL(EatMubarkDetails.this);
                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getQRGatewayDetails,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getString("Status_Code").equals("00")) {

                                                JSONArray jsonArray = jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getJSONArray("PaymentsGatewayList");
                                                for (int i = 0; i < jsonArray.length(); i++) {

                                                    JSONObject eatMubarakObj = new JSONObject(jsonArray.get(i).toString());
                                                    if (eatMubarakObj.getString("GatewayName").equals("EMOTP")) {

                                                        if (eatMubarakObj.getString("Android").equals("Y")) {
                                                            Constants.GatewayName = eatMubarakObj.getString("GatewayName");
                                                            Constants.Currency = eatMubarakObj.getString("Currency");
                                                            Constants.CollectionAccount = eatMubarakObj.getString("CollectionAccount");
                                                            Constants.logoLink = eatMubarakObj.getString("Logolink");

                                                            GetPurposeBillList();


                                                        } else {
                                                            showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        }
                                                        break;
                                                    }
                                                }
//                                                PPIntentIntegrator integrator = new PPIntentIntegrator(MainActivity.this);
//                                                integrator.setOrientationLocked(false);
//                                                integrator.setBeepEnabled(false);
//                                                cameraScanStartTime = System.nanoTime();
//                                                integrator.initiateScan();

                                            } else if (jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getString("Status_Description"), "ERROR", EatMubarkDetails.this, new LoginScreen());


                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_PaymentsGatewayDetailResult").getString("Status_Description"), "ERROR");
                                            }

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sSigns_LegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                            params.put("sSigns_CustomerFeedBack", feedBack);
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }


    }
    

    public void GetPurposeBillList() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    pd.show();
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sPurposeGroupCode", "EMOTP");
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(EatMubarkDetails.this);

                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getPuposesURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("POTList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        purpStringArrayList.add(jsonObj.getString("Name"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();

                                                    }

                                                }

                                                purpose = new String[purpStringArrayList.size()];
                                                purpose = purpStringArrayList.toArray(purpose);

                                                Gson gson = new Gson();
                                                String json = gson.toJson(purpStringArrayList);
                                                constants.purposeBillList.putString("purposeList", json);
                                                constants.purposeBillList.commit();
                                                constants.purposeBillList.apply();
                                                showSpinner();
                                            } else if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR", EatMubarkDetails.this, new LoginScreen());
                                                validateBtn.setEnabled(true);

                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                            validateBtn.setEnabled(true);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sPurposeGroupCode", "BP");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    validateBtn.setEnabled(true);
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
        }
    }

    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                if (!TextUtils.isEmpty(type) && type.equals("requestbill")) {
                    purposeSelected = constants.destinationAccount.getString("purpose", "N/A");
                    spinnerAdapter purposeAdapter = new spinnerAdapter(EatMubarkDetails.this, R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(constants.destinationAccount.getString("purpose", "N/A")));
                    purposesSpinner.setClickable(false);
                } else {
                    spinnerAdapter purposeAdapter = new spinnerAdapter(EatMubarkDetails.this, R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Payment");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(0);
                    purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            try {


                                // Get select item
                                if (purposesSpinner.getSelectedItem() == "Purpose of Payment") {
                                    purposeSelected = purposesSpinner.getSelectedItem().toString();
                                    Log.d("identity", "---" + purposeSelected);

                                } else {

                                    purposeSelected = purposesSpinner.getSelectedItem().toString();

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });

                    pd.dismiss();

                }

            }
        }, 1000);
    }

    @Override
    public void onBackPressed() {
        showDialog("Do you want to exit this order?");
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            CryptLib _crypt = new CryptLib();

            if (isFirst) {


                upperPart1.setEnabled(true);
                if (!TextUtils.isEmpty(type) && type.equals("requestbill")) {

                } else {
                    upperPart2.setEnabled(true);
                }
                if (isSource) {

                    isSelectSource = constants.sourceAccount.getString("isSelect", "N/A");
                    if (!isSelectSource.equals("N/A") && isSelectSource.equals("Yes")) {
                        sourceuserName = _crypt.decrypt(constants.sourceAccount.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        sourceBalance = _crypt.decrypt(constants.sourceAccount.getString("balance", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        sourceAccount = _crypt.decrypt(constants.sourceAccount.getString("accountNumber", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        branchCode = _crypt.decrypt(constants.sourceAccount.getString("branchCode", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        sourceCurrency = _crypt.decrypt(constants.sourceAccount.getString("currency", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        userNameTxt.setText(sourceuserName);
                        accMaskTxt.setVisibility(View.VISIBLE);
                        accMaskTxt.setText(utils.getMaskedString(sourceAccount));
                        amountTxt.setVisibility(View.VISIBLE);
                        amountTxt.setText(sourceCurrency + " " + sourceBalance);
                        sourceBalance=sourceBalance.replaceAll(",","");
                        sourceBalance=sourceBalance.replaceAll("\"", "");
                        Sourcecurrency=Float.parseFloat(sourceBalance);


                    }

                    constants.sourceAccountEditor.putString("isSelect", "No");
                    constants.sourceAccountEditor.commit();
                    constants.sourceAccountEditor.apply();
                    isSource = false;

                }

                if (isDestination) {

                    isSelectDestination = constants.destinationAccount.getString("isSelect", "N/A");
                    if (!isSelectDestination.equals("N/A") && isSelectDestination.equals("Yes")) {
                        distributorID = _crypt.decrypt(constants.destinationAccount.getString("DistributorID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        distributorAccountNumber = _crypt.decrypt(constants.destinationAccount.getString("distributorAccountNumber", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        amountAfterDue = _crypt.decrypt(constants.destinationAccount.getString("AmountAfterDueDate", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        amountBeforeDue = _crypt.decrypt(constants.destinationAccount.getString("AmountWithinDueDate", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        billStatus = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        billingMonth = _crypt.decrypt(constants.destinationAccount.getString("BillingMonth", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        dueDate = _crypt.decrypt(constants.destinationAccount.getString("DueDate", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        consumerName = _crypt.decrypt(constants.destinationAccount.getString("ConsumerName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        billConsumerNumber = _crypt.decrypt(constants.destinationAccount.getString("BillConsumerNumber", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        companyName = _crypt.decrypt(constants.destinationAccount.getString("CompanyName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        if (companyName.toLowerCase().contains("postpaid")) {
                            isPostpaid = true;
                        }
                        alias = _crypt.decrypt(constants.destinationAccount.getString("alias", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        String cat = constants.destinationAccount.getString("categoryType", "N/A");
                        if (!TextUtils.isEmpty(cat) && !cat.equals("N/A") && !cat.equals("null") && !cat.equals(null)) {
                            categoryType = _crypt.decrypt(constants.destinationAccount.getString("categoryType", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        } else {
                            categoryType = "N/A";
                        }
                        //                itemName.setText(distributorName);


                        payableBeforeTxt.setText(amountBeforeDue);
                        payableAfterTxt.setText(amountAfterDue);
                        billingMonthTxt.setText(billingMonth);
                        billingTxt.setText(companyName);
                        dueDateTxt.setText(dueDate);
                        statusTxt.setText(billStatus);
                        consumerIDTxt.setText(billConsumerNumber);
                        consumerNumberTxt.setText(consumerName);
                        itemName.setText(alias);


                        if (billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("unpaid") || billStatus.replaceAll("-", "").replaceAll(" ", "").trim().toLowerCase().equals("blocked")) {

                            statusTxt.setTextColor(getResources().getColor(R.color.tabunderline));
                        } else
                            statusTxt.setTextColor(getResources().getColor(R.color.lgreen));

                        Calendar today = Calendar.getInstance();
                        today.set(Calendar.MILLISECOND, 0);
                        today.set(Calendar.SECOND, 0);
                        today.set(Calendar.MINUTE, 0);
                        today.set(Calendar.HOUR_OF_DAY, 0);
                        Date currentDate = today.getTime();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        Date mDate;
                        long timeInMilliseconds = 0, currentTimeToMili = 0;
                        try {
                            if (!dueDate.equals("N/A")) {
                                mDate = sdf.parse(dueDate);
                                timeInMilliseconds = mDate.getTime();
                                currentTimeToMili = currentDate.getTime();
                                System.out.println("Date in milli :: " + timeInMilliseconds);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        String billStatus1 = _crypt.decrypt(constants.destinationAccount.getString("BillStatus", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                        if (!TextUtils.isEmpty(billStatus1) && !billStatus1.equals("N/A")) {

                            if (billStatus1.toLowerCase().equals("partial payment")) {

                                editAmount.setText("");
                                editAmount.setFocusableInTouchMode(true);
                                editAmount.setEnabled(true);
                            } else {

                                if (currentTimeToMili <= timeInMilliseconds) {


                                    payabaleAmount.setText(amountBeforeDue);
                                    editAmount.setText(amountBeforeDue.split("\\.")[0]);
                                    editAmount.setFocusableInTouchMode(false);
                                    editAmount.setEnabled(false);


                                } else if (currentTimeToMili > timeInMilliseconds) {

                                    payabaleAmount.setText(amountAfterDue);
                                    editAmount.setText(amountAfterDue.split("\\.")[0]);
                                    editAmount.setFocusableInTouchMode(false);
                                    editAmount.setEnabled(false);

                                }

                            }
                        }
                        if (currentTimeToMili <= timeInMilliseconds) {


                            payabaleAmount.setText(amountBeforeDue);

                        } else if (currentTimeToMili > timeInMilliseconds) {

                            payabaleAmount.setText(amountAfterDue);

                        }

                    }

                    constants.destinationAccountEditor.putString("isSelect", "No");
                    constants.destinationAccountEditor.commit();
                    constants.destinationAccountEditor.apply();
                    isDestination = false;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Verification() {

        try {


            CryptLib _crypt = new CryptLib();
            Intent intent = new Intent(EatMubarkDetails.this, EatMubarakDetailsReview.class);
            intent.putExtra("restaurantName", restaurantName);
            intent.putExtra("orderId", orderId);
            intent.putExtra("discount", discount);
            intent.putExtra("deliveryAddress", deliveryAddress);
            intent.putExtra("deliveryEta", deliveryEta);
            intent.putExtra("promoCode", promoCode);
            intent.putExtra("amount", amount);
            intent.putExtra("userName", sourceuserName);
            intent.putExtra("balance", sourceBalance);
            intent.putExtra("accountNumber", sourceAccount);
            intent.putExtra("purpose", purposeSelected);
            intent.putExtra("currency", sourceCurrency);
            intent.putExtra("distributorAccountNumber", distributorAccountNumber);
            intent.putExtra("mobile", orderId);
            intent.putExtra("remarks", remarks);
            intent.putExtra("stan", stan);
            intent.putExtra("rrn", rrn);
            intent.putExtra("type", "EM");
            intent.putExtra("amount", amount);
            intent.putExtra("branchCode", branchCode);
            intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            intent.putExtra("distributorName", alias);
            intent.putExtra("distributorID", distributorID);
            intent.putExtra("categoryType", categoryType);
            intent.putExtra("transactionAmount", amount);
            intent.putExtra("companyName", restaurantName);
            intent.putExtra("ConversionRate",eatMubarakConversion);

            intent.putExtra("service_provider", "Eat Mubarak");
            overridePendingTransition(0, 0);
            pd.dismiss();
            startActivity(intent);
            validateBtn.setEnabled(true);


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void showDilogForError(String msg, String header) {

        try {
            dialogPopup = new Dialog(EatMubarkDetails.this);
            dialogPopup.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogPopup.setContentView(R.layout.error_dialog);
            textView = dialog.findViewById(R.id.validatingTxt);
            doneBtn = dialog.findViewById(R.id.doneBtn);
            heading = dialog.findViewById(R.id.heading);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    finish();

                }
            });
            textView.setText(msg);
            heading.setText(header);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showDialog(String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alert:");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

//                Intent intent = new Intent(EatMubarkDetails.this, EatMubarakMainScreen.class);
//                startActivity(intent);
//                finishAffinity();

                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

}