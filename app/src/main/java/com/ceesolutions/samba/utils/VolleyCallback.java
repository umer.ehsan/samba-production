package com.ceesolutions.samba.utils;

import org.json.JSONObject;

/**
 * Created by ceeayaz on 6/8/18.
 */

public interface VolleyCallback {
    void onSuccess(JSONObject result);
    void onError();
}