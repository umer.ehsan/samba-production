package com.ceesolutions.samba.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.transactions.AccountsScreen;
import com.ceesolutions.samba.transactions.ManageAccountsScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ayaz on 1/25/18.
 */

public class PopupDialogFingerPrint extends DialogFragment {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private TextView confirmBtn;
    private TextView cancelBtn, message, doneBtn, heading, userNameText, textView;
    private String token, target, userName;
    private AlertDialog OptionDialog;
    private String isFirstLogin;
    private Utils utils;
    private Dialog dialog, dialogLogout;
    private String error = "";
    private AppPreferences appPreferences;
    private String location, notiEnabled;

    /**
     * creates a new instance of PropDialogFragment
     */
    public PopupDialogFingerPrint() { /*empty*/ }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //getting proper access to LayoutInflater is the trick. getLayoutInflater is a                   //Function
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.pop_up_finger, null);
        OptionDialog = new AlertDialog.Builder(getActivity()).create();
        OptionDialog.setView(view);

//        popupDialog = new PopupDialog();
        requestQueue = Volley.newRequestQueue(getActivity());
        helper = Helper.getHelper(getActivity());
        constants = Constants.getConstants(getActivity());
        pd = new Dialog(getActivity());
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        utils = new Utils(getActivity());
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        try {
            final CryptLib _crypt = new CryptLib();
            Log.d("Token", "--" + token);
            Intent intent = getActivity().getIntent();
            appPreferences = new AppPreferences(getActivity());
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }


            Log.d("Name----", "-" + constants.sharedPreferences.getString("userName", "N/A"));
            userName = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            cancelBtn = (TextView) view.findViewById(R.id.cancelBtn);
            userNameText = (TextView) view.findViewById(R.id.userNameText);
            userNameText.setText(_crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()).toUpperCase());

            confirmBtn = (TextView) view.findViewById(R.id.doneBtn);


            confirmBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Close dialog
                    confirmBtn.setEnabled(false);
                    AddTrustedDevice("1");


                }
            });
            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Close dialog
                    cancelBtn.setEnabled(false);
//                    AddTrustedDevice("0");
                    if (!TextUtils.isEmpty(getActivity().getIntent().getStringExtra("isFirst")) && getActivity().getIntent().getStringExtra("isFirst").equals("Yes")) {
                        GetManageAccountsList("0");
                    } else {

                        try {


                            constants.fingerPrintEditor.clear();
                            constants.fingerPrintEditor.commit();
                            constants.fingerPrintEditor.apply();

                            constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams("False", constants.getDeviceMac(), constants.getDeviceIMEI()));
                            constants.editor.commit();
                            constants.editor.apply();
                            constants.isFingerCheckEditor.putString("isCheck", "False");
                            constants.isFingerCheckEditor.commit();
                            constants.isFingerCheckEditor.apply();
                            pd.dismiss();
//                                            finish();
                            confirmBtn.setEnabled(true);
                            cancelBtn.setEnabled(true);
                            OptionDialog.cancel();
                            getActivity().finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return OptionDialog;
    }

    public void AddTrustedDevice(final String flag) {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                final CryptLib _crypt = new CryptLib();
                final String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                final String userName = user;
                final String md5Password = helper.md5(userName.toUpperCase() + constants.address);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sFingerPrintToken", md5Password);
                params.put("sIsActive", flag);
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(getActivity());
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.setFingerprintURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_SetUserFingerPrintResult").getString("Status_Code").equals("00")) {


                                            if (flag.equals("1")) {

                                                if (!TextUtils.isEmpty(getActivity().getIntent().getStringExtra("isFirst")) && getActivity().getIntent().getStringExtra("isFirst").equals("Yes")) {
                                                    GetManageAccountsList("1");
                                                } else {
                                                    try {


                                                        constants.fingerPrintEditor.putString("userName", user);
                                                        constants.fingerPrintEditor.commit();
                                                        constants.fingerPrintEditor.apply();
                                                        constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams("True", constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.editor.commit();
                                                        constants.editor.apply();
                                                        pd.dismiss();
                                                        constants.isFingerCheckEditor.putString("isCheck", "False");
                                                        constants.isFingerCheckEditor.commit();
                                                        constants.isFingerCheckEditor.apply();
//                                            finish();
                                                        confirmBtn.setEnabled(true);
                                                        cancelBtn.setEnabled(true);
                                                        OptionDialog.cancel();
                                                        getActivity().finish();
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }


                                            } else {
                                                if (!TextUtils.isEmpty(getActivity().getIntent().getStringExtra("isFirst")) && getActivity().getIntent().getStringExtra("isFirst").equals("Yes")) {
                                                    GetManageAccountsList("0");
                                                } else {
                                                    try {


                                                        constants.fingerPrintEditor.clear();
                                                        constants.fingerPrintEditor.commit();
                                                        constants.fingerPrintEditor.apply();

                                                        constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams("False", constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.editor.commit();
                                                        constants.editor.apply();
                                                        constants.isFingerCheckEditor.putString("isCheck", "False");
                                                        constants.isFingerCheckEditor.commit();
                                                        constants.isFingerCheckEditor.apply();
                                                        pd.dismiss();
//                                            finish();
                                                        confirmBtn.setEnabled(true);
                                                        cancelBtn.setEnabled(true);
                                                        OptionDialog.cancel();
                                                        getActivity().finish();
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        } else if (jsonObject.getJSONObject("Signs_SetUserFingerPrintResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            showDilogForLogout(jsonObject.getJSONObject("Signs_SetUserFingerPrintResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());
                                            confirmBtn.setEnabled(true);
                                            cancelBtn.setEnabled(true);

                                        } else {
                                            pd.dismiss();
                                            showDilogForError(jsonObject.getJSONObject("Signs_SetUserFingerPrintResult").getString("Status_Description"), "ERROR");
                                            confirmBtn.setEnabled(true);
                                            cancelBtn.setEnabled(true);
                                        }
                                    } else {
                                        pd.dismiss();
                                        showDilogForError(constants.Invalid_Message, "ERROR");
                                        confirmBtn.setEnabled(true);
                                        cancelBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    confirmBtn.setEnabled(true);
                                    cancelBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    confirmBtn.setEnabled(true);
                                    cancelBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    confirmBtn.setEnabled(true);
                                    cancelBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", userName);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceMacAddress", constants.address);
//                        params.put("sFcm_Token", token);
//                        params.put("sDeviceType", target);
//                        params.put("iUserID", "0");
//                        params.put("sNotificationFlag", flag);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_LONG).show();
                cancelBtn.setEnabled(true);
                confirmBtn.setEnabled(true);
                pd.dismiss();
                showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_LONG).show();
            cancelBtn.setEnabled(true);
            pd.dismiss();
            cancelBtn.setEnabled(true);
            confirmBtn.setEnabled(true);
            showDilogForError("Device cannot be added. Please try again later.", "ERROR");

        }
    }

    public void CheckForFirstTimeLogin() {

        if (isFirstLogin.equals("Yes")) {

//            Toast.makeText(getActivity(), "Device cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
            pd.dismiss();
            showDilogForError("Device cannot be added. Please try again later.", "ERROR");
            cancelBtn.setEnabled(true);
            confirmBtn.setEnabled(true);

        } else {

            pd.dismiss();
            showDilogForError("Device cannot be added. Please try again later.", "ERROR");
            cancelBtn.setEnabled(true);
            confirmBtn.setEnabled(true);

        }
    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();
                pd.dismiss();

                cancelBtn.setEnabled(true);
                doneBtn.setEnabled(true);
                getActivity().finish();
                OptionDialog.cancel();
            }


        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public void showDilogForLogout(String msg, String header, Context className, final AppCompatActivity toActivity) {

        dialogLogout = new Dialog(getActivity());
        dialogLogout.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLogout.setContentView(R.layout.error_dialog);
        textView = (TextView) dialogLogout.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialogLogout.findViewById(R.id.doneBtn);
        heading = (TextView) dialogLogout.findViewById(R.id.heading);
        dialogLogout.setCancelable(false);
        dialogLogout.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogLogout.dismiss();
                Constants constants = Constants.getConstants(getActivity());
                constants.editor.clear();
                constants.editor.commit();
                constants.editor.apply();
                constants.nonSambaCredentialsEditor.clear();
                constants.nonSambaCredentialsEditor.commit();
                constants.nonSambaCredentialsEditor.apply();
                constants.destinationAccountEditor.clear().commit();
                constants.destinationAccountEditor.apply();
                constants.sourceAccountEditor.clear().commit();
                constants.sourceAccountEditor.apply();
                constants.accountListEditor.clear().commit();
                constants.accountListEditor.apply();
                constants.accountsEditor.clear().commit();
                constants.accountsEditor.apply();
                constants.forgotPasswordEditor.clear().commit();
                constants.forgotPasswordEditor.apply();
                constants.purposeBillList.clear().commit();
                constants.purposeBillList.apply();
                constants.purposePrepaidList.clear().commit();
                constants.purposePrepaidList.apply();
                constants.purposeListEditor.clear().commit();
                constants.purposeListEditor.apply();
                constants.reasonListEditor.clear().commit();
                constants.reasonListEditor.apply();
                constants.statusListEditor.clear().commit();
                constants.statusListEditor.apply();
                constants.channelListEditor.clear().commit();
                constants.channelListEditor.apply();
                Intent intent = new Intent(getActivity(), toActivity.getClass());
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                ((Activity) getActivity()).startActivity(intent);
                cancelBtn.setEnabled(true);
                doneBtn.setEnabled(true);
                OptionDialog.cancel();
                ((Activity) getActivity()).finish();
                ((Activity) getActivity()).finishAffinity();

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialogLogout.show();

    }

    public void GetManageAccountsList(final String flag) {

        try {
//            pd.show();
            if (helper.isNetworkAvailable()) {
                //135821
                //constants.sharedPreferences.getString("T24_CIF", "N/A")

                try {
                    final CryptLib _crypt = new CryptLib();
                    final String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sT24_CIF", _crypt.decrypt(constants.sharedPreferences.getString("T24_CIF", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sLegalIdentityType", _crypt.decrypt(constants.sharedPreferences.getString("LegalType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sSigns_UserType", _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sIsAllAccounts", "0");
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                    HttpsTrustManager.allowMySSL(getActivity());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAccountListURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("00")) {

                                                int count = 0;
                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("AccountList"));

                                                if (jsonArray.length() > 1) {


//                                                    for (int i = 0; i < jsonArray.length(); i++) {
//                                                        try {
//
//                                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
//                                                            Account account = new Account();
//                                                            account.setAccountNumber(jsonObj.getString("AccountID"));
//                                                            account.setAccountName(jsonObj.getString("AccountTitle"));
//                                                            account.setUserName(jsonObj.getString("AccountTitle"));
//                                                            account.setBranchName(jsonObj.getString("BranchName"));
//                                                            account.setAccountType(jsonObj.getString("Type"));
//                                                            account.setAvailBal(jsonObj.getString("AvailBal"));
//                                                            account.setBankCode(jsonObj.getString("BankCode"));
//                                                            account.setBankID(jsonObj.getString("BankCode"));
//                                                            account.setBranchCode(jsonObj.getString("BranchCode"));
//                                                            account.setCurrency(jsonObj.getString("Currency"));
//                                                            account.setCurrencyType(jsonObj.getString("Currency"));
//                                                            account.setProductType(jsonObj.getString("Product"));
//                                                            account.setBankColor(jsonObj.getString("BankColorCode"));
//                                                            account.setIsActive(jsonObj.getString("Active"));
//                                                            account.setId(jsonObj.getString("ID"));
//                                                            account.setBranchShortName(jsonObj.getString("BranchNameShort"));
//                                                            accounts.add(account);
//
//
//                                                        } catch (Exception ex) {
//                                                            ex.printStackTrace();
//                                                            pd.dismiss();
//                                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                                        }
//
//                                                    }
//                                                    manageAccountsCustomListAdapter = new ManageAccountsCustomListAdapter(accounts, ManageAccountsScreen.this);
//                                                    listView.setAdapter(manageAccountsCustomListAdapter);
//                                                    pd.dismiss();

                                                    if (flag.equals("1")) {
                                                        Intent intent = new Intent(getActivity(), ManageAccountsScreen.class);
                                                        intent.putExtra("intent", "home");
                                                        startActivity(intent);
                                                        constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams("True", constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.editor.commit();
                                                        constants.editor.apply();
                                                        constants.fingerPrintEditor.putString("userName", user);
                                                        constants.fingerPrintEditor.commit();
                                                        constants.fingerPrintEditor.apply();
                                                        pd.dismiss();
                                                        constants.isFingerCheckEditor.putString("isCheck", "False");
                                                        constants.isFingerCheckEditor.commit();
                                                        constants.isFingerCheckEditor.apply();
//                                            finish();
                                                        confirmBtn.setEnabled(true);
                                                        cancelBtn.setEnabled(true);
                                                        OptionDialog.cancel();
                                                        getActivity().finish();

                                                    } else {

                                                        Intent intent = new Intent(getActivity(), ManageAccountsScreen.class);
                                                        intent.putExtra("intent", "home");
                                                        startActivity(intent);
                                                        constants.fingerPrintEditor.clear();
                                                        constants.fingerPrintEditor.commit();
                                                        constants.fingerPrintEditor.apply();
                                                        constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams("False", constants.getDeviceMac(), constants.getDeviceIMEI()));

                                                        constants.editor.commit();
                                                        constants.editor.apply();
                                                        constants.isFingerCheckEditor.putString("isCheck", "False");
                                                        constants.isFingerCheckEditor.commit();
                                                        constants.isFingerCheckEditor.apply();
                                                        pd.dismiss();
//                                            finish();
                                                        confirmBtn.setEnabled(true);
                                                        cancelBtn.setEnabled(true);
                                                        OptionDialog.cancel();
                                                        getActivity().finish();
                                                    }
                                                } else {

                                                    if (flag.equals("1")) {
                                                        constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams("True", constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.editor.commit();
                                                        constants.editor.apply();
                                                        constants.fingerPrintEditor.putString("userName", user);
                                                        constants.fingerPrintEditor.commit();
                                                        constants.fingerPrintEditor.apply();
                                                        pd.dismiss();
                                                        constants.isFingerCheckEditor.putString("isCheck", "False");
                                                        constants.isFingerCheckEditor.commit();
                                                        constants.isFingerCheckEditor.apply();
//                                            finish();
                                                        confirmBtn.setEnabled(true);
                                                        cancelBtn.setEnabled(true);
                                                        OptionDialog.cancel();
                                                        getActivity().finish();

                                                    } else {

                                                        constants.fingerPrintEditor.clear();
                                                        constants.fingerPrintEditor.commit();
                                                        constants.fingerPrintEditor.apply();
                                                        constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams("False", constants.getDeviceMac(), constants.getDeviceIMEI()));

                                                        constants.editor.commit();
                                                        constants.editor.apply();
                                                        constants.isFingerCheckEditor.putString("isCheck", "False");
                                                        constants.isFingerCheckEditor.commit();
                                                        constants.isFingerCheckEditor.apply();
                                                        pd.dismiss();
//                                            finish();
                                                        confirmBtn.setEnabled(true);
                                                        cancelBtn.setEnabled(true);
                                                        OptionDialog.cancel();
                                                        getActivity().finish();
                                                    }


                                                }
                                            } else if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());


                                            } else if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("49")) {
                                                pd.dismiss();
                                                String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                                if (userType.equals("NON_SAMBA")) {
                                                    Intent intent = new Intent(getActivity(), AccountsScreen.class);
                                                    getActivity().overridePendingTransition(0, 0);
                                                    if (flag.equals("1")) {
                                                        constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams("True", constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.editor.commit();
                                                        constants.editor.apply();
                                                        constants.fingerPrintEditor.putString("userName", user);
                                                        constants.fingerPrintEditor.commit();
                                                        constants.fingerPrintEditor.apply();
                                                        pd.dismiss();
                                                        constants.isFingerCheckEditor.putString("isCheck", "False");
                                                        constants.isFingerCheckEditor.commit();
                                                        constants.isFingerCheckEditor.apply();
//                                            finish();
                                                        confirmBtn.setEnabled(true);
                                                        cancelBtn.setEnabled(true);
                                                        OptionDialog.cancel();
                                                        getActivity().finish();

                                                    } else {

                                                        constants.fingerPrintEditor.clear();
                                                        constants.fingerPrintEditor.commit();
                                                        constants.fingerPrintEditor.apply();
                                                        constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams("False", constants.getDeviceMac(), constants.getDeviceIMEI()));

                                                        constants.editor.commit();
                                                        constants.editor.apply();
                                                        constants.isFingerCheckEditor.putString("isCheck", "False");
                                                        constants.isFingerCheckEditor.commit();
                                                        constants.isFingerCheckEditor.apply();
                                                        pd.dismiss();
//                                            finish();
                                                        confirmBtn.setEnabled(true);
                                                        cancelBtn.setEnabled(true);
                                                        OptionDialog.cancel();
                                                        getActivity().finish();
                                                    }
                                                    startActivity(intent);

                                                } else {
                                                    Intent intent = new Intent(getActivity(), ManageAccountsScreen.class);
                                                    intent.putExtra("intent", "home");
                                                    getActivity().overridePendingTransition(0, 0);
                                                    if (flag.equals("1")) {
                                                        constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams("True", constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.editor.commit();
                                                        constants.editor.apply();
                                                        constants.fingerPrintEditor.putString("userName", user);
                                                        constants.fingerPrintEditor.commit();
                                                        constants.fingerPrintEditor.apply();
                                                        pd.dismiss();
                                                        constants.isFingerCheckEditor.putString("isCheck", "False");
                                                        constants.isFingerCheckEditor.commit();
                                                        constants.isFingerCheckEditor.apply();
//                                            finish();
                                                        confirmBtn.setEnabled(true);
                                                        cancelBtn.setEnabled(true);
                                                        OptionDialog.cancel();
                                                        getActivity().finish();

                                                    } else {

                                                        constants.fingerPrintEditor.clear();
                                                        constants.fingerPrintEditor.commit();
                                                        constants.fingerPrintEditor.apply();
                                                        constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams("False", constants.getDeviceMac(), constants.getDeviceIMEI()));

                                                        constants.editor.commit();
                                                        constants.editor.apply();
                                                        constants.isFingerCheckEditor.putString("isCheck", "False");
                                                        constants.isFingerCheckEditor.commit();
                                                        constants.isFingerCheckEditor.apply();
                                                        pd.dismiss();
//                                            finish();
                                                        confirmBtn.setEnabled(true);
                                                        cancelBtn.setEnabled(true);
                                                        OptionDialog.cancel();
                                                        getActivity().finish();
                                                    }
                                                    startActivity(intent);
                                                }
                                            } else {
                                                pd.dismiss();
                                                confirmBtn.setEnabled(true);
                                                cancelBtn.setEnabled(true);
                                                OptionDialog.cancel();
                                                getActivity().finish();
//                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "WARNING");
                                            }
                                        } else {
                                            pd.dismiss();
                                            confirmBtn.setEnabled(true);
                                            cancelBtn.setEnabled(true);
                                            OptionDialog.cancel();
                                            getActivity().finish();
//                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        confirmBtn.setEnabled(true);
                                        cancelBtn.setEnabled(true);
                                        OptionDialog.cancel();
                                        getActivity().finish();
//                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        confirmBtn.setEnabled(true);
                                        cancelBtn.setEnabled(true);
                                        OptionDialog.cancel();
                                        getActivity().finish();
//                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        confirmBtn.setEnabled(true);
                                        cancelBtn.setEnabled(true);
                                        OptionDialog.cancel();
                                        getActivity().finish();
//                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sT24_CIF", constants.sharedPreferences.getString("T24_CIF", "N/A"));
//                            params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                            params.put("sLegalIdentityType", constants.sharedPreferences.getString("LegalType", "N/A"));
//                            params.put("sSigns_UserType", type);
//                            params.put("sIsAllAccounts", "0");
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    confirmBtn.setEnabled(true);
                    cancelBtn.setEnabled(true);
                    OptionDialog.cancel();
                    getActivity().finish();
//                    utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                }

            } else {
                pd.dismiss();
                confirmBtn.setEnabled(true);
                cancelBtn.setEnabled(true);
                OptionDialog.cancel();
                getActivity().finish();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
            confirmBtn.setEnabled(true);
            cancelBtn.setEnabled(true);
            OptionDialog.cancel();
            getActivity().finish();
//            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }
}