package com.ceesolutions.samba.utils;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by Belal on 5/27/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    Constants constants;
    int count = 0;
    private NotificationChannel mChannel;

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional


        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());



        //Calling method to generate notification
        String msg = remoteMessage.getNotification().getBody();
        if (msg.equals("Remote Wipe"))
            sendNotification(remoteMessage.getNotification().getBody(), "Remote Wipe");
        else
            sendNotification(remoteMessage.getNotification().getBody(), "Normal");
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void sendNotification(String messageBody, String msgType) {
        Intent intent = new Intent(this, LoginScreen.class);
        constants = Constants.getConstants(this);
        this.handleIntent(intent);
//        if (msgType.equals("Remote Wipe")) {
//
//            intent.putExtra("MessageType", "RemoteWipe");
//        } else {
//
//            Log.d("--->", "---");
//        }

        if (messageBody.toLowerCase().contains("birthday")) {
            Date currentDate = Calendar.getInstance().getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String formattedDate = sdf.format(currentDate);
            constants.fcmEditor.putString("birthday", "yes");
            constants.fcmEditor.putString("date", formattedDate);
            constants.fcmEditor.commit();
            constants.fcmEditor.apply();

        }

        if (messageBody.toLowerCase().contains("eid-ul-adha")) {
            Date currentDate = Calendar.getInstance().getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String formattedDate = sdf.format(currentDate);
            constants.fcmEditor.putString("eidadha", "yes");
            constants.fcmEditor.putString("date", formattedDate);
            constants.fcmEditor.commit();
            constants.fcmEditor.apply();

        }

        if (messageBody.toLowerCase().contains("eid-ul-fitr")) {
            Date currentDate = Calendar.getInstance().getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String formattedDate = sdf.format(currentDate);
            constants.fcmEditor.putString("eidfitr", "yes");
            constants.fcmEditor.putString("date", formattedDate);
            constants.fcmEditor.commit();
            constants.fcmEditor.apply();

        }

        Log.d("Message----", messageBody);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(),
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.samba)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.samba))
                .setBadgeIconType(R.mipmap.samba)
                .setContentTitle("SIGNS")
                .setStyle(new NotificationCompat.BigTextStyle())
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent);
        Random random = new Random();
        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = 1;
        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            /* Create or update. */
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        } else
            notificationManager.notify(m, notificationBuilder.build());
//        try {
//            constants = Constants.getConstants(this);
//
//            count = constants.badgeCountCredentials.getInt("badge", 0);
//            Log.d("Count1---", "---" + count);
//            count = count + 1;
//            constants.badgeCountEditor.putInt("badge", count);
//            constants.badgeCountEditor.commit();
//            constants.badgeCountEditor.apply();
//            Log.d("Count---", "---" + count);
//            ShortcutBadger.applyCount(getApplicationContext(), count);
//        } catch (Exception e) {
//            Log.e("failedToParse", "Badge!?");
//        }

//        ShortcutBadger.applyCount(this, 1); //for 1.1.4+
//        ShortcutBadger.with(getApplicationContext()).count(badgeCount);
    }

//    @Override
//    public boolean zzE(Intent intent) {
////        if(intent.hasExtra("badge"))
////        {
//        try {
//            constants = Constants.getConstants(this);
//
//            count = constants.badgeCountCredentials.getInt("badge", 0);
//            Log.d("Count1---", "---" + count);
//            count = count + 1;
//            constants.badgeCountEditor.putInt("badge", count);
//            constants.badgeCountEditor.commit();
//            constants.badgeCountEditor.apply();
//            Log.d("Count---", "---" + count);
//            ShortcutBadger.applyCount(getApplicationContext(), count);
//        } catch (Exception e) {
//            Log.e("failedToParse", "Badge!?");
//        }
////        }
//        // pass the intent through to the non-overriden zzE
//        // to show the default notification.
//        return super.zzE(intent);
//
//        // You could also show a custom notification here
//        // and return true instead of this if you
//        // don't want the default notifications.
//    }

//    @Override
//    public void zzm(Intent intent) {
//        Log.i("uniqbadge", "zzm");
//        Set<String> keys = intent.getExtras().keySet();
//        for (String key : keys) {
//            try {
//                Log.i("uniq", " " + key + " " + intent.getExtras().get(key));
//                if (key.equals("badge")) {
//                    String cnt = intent.getExtras().get(key).toString();
//                    int badgeCount = Integer.valueOf(cnt);
//                    Log.i("uniq", " badge count " + badgeCount);
//                    ShortcutBadger.applyCount(this, badgeCount);
//                    Log.i("uniq", " " + "end");
//                }
//            } catch (Exception e) {
//                Log.i("uniqbadge", "zzm Custom_FirebaseMessagingService" + e.getMessage());
//            }
//        }
//
//        super.zzm(intent);
//    }

    @Override
    public void handleIntent(Intent intent) {
        try {
            constants = Constants.getConstants(this);


            //String token123 = FirebaseMessaging.getInstance().getToken().getResult();
            FirebaseMessaging.getInstance().getToken()
                    .addOnCompleteListener(new OnCompleteListener<String>() {
                        @Override
                        public void onComplete(@NonNull Task<String> task) {
                            if (!task.isSuccessful()) {
                                Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                                return;
                            }

                            // Get new FCM registration token
                            String token = task.getResult();

                            // Log and toast
                            System.out.println(token);

                            String refreshedToken = token;

                            //Displaying token on logcat
                            Log.d("Hello", "Refreshed token: " + refreshedToken);

                            ConfigAPI.fcmToken = refreshedToken;

                            Constants constants = Constants.getConstants(getApplicationContext());
                            constants.fcmEditor.putString("fcmToken", refreshedToken);
                            constants.fcmEditor.commit();
                            constants.fcmEditor.apply();

                            String str =  constants.fcmSharedPref.getString("fcmToken","fcmToken");
                        }
                    });




            String title = "";
            String body = "";
            if (intent.getExtras() != null) {
                for (String key : intent.getExtras().keySet()) {
                    Object value = intent.getExtras().get(key);
                    Log.d(TAG, "Key: " + key + " Value: " + value);
                    if (key.equals("gcm.notification.title")) {
                        title = String.valueOf(value);
                    }

                    if (key.equals("gcm.notification.body")) {

                        body = String.valueOf(value);
                    }
                }
            }
//            this.onMessageReceived(builder.build());

            Log.d("title---","-"+title);
            if (TextUtils.isEmpty(title)) {

                title = "Welcome to SambaSmart";
            }

            if (title.toLowerCase().contains("birthday")) {
                Date currentDate = Calendar.getInstance().getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String formattedDate = sdf.format(currentDate);
                constants.fcmEditor.putString("birthday", "yes");
                constants.fcmEditor.putString("date", formattedDate);
                constants.fcmEditor.commit();
                constants.fcmEditor.apply();

            }

            if (title.toLowerCase().contains("adha")) {
                Date currentDate = Calendar.getInstance().getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String formattedDate = sdf.format(currentDate);
                constants.fcmEditor.putString("eidadha", "yes");
                constants.fcmEditor.putString("date", formattedDate);
                constants.fcmEditor.commit();
                constants.fcmEditor.apply();

            }

            if (title.toLowerCase().contains("fitr")) {
                Date currentDate = Calendar.getInstance().getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String formattedDate = sdf.format(currentDate);
                constants.fcmEditor.putString("eidfitr", "yes");
                constants.fcmEditor.putString("date", formattedDate);
                constants.fcmEditor.commit();
                constants.fcmEditor.apply();

            }

            ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
            Log.d("Message----", body);
            Log.d("intent---", "" + cn.toString());
            Class<?> c = null;
            Intent classIntent = null;
            if (!TextUtils.isEmpty(cn.getClassName()) && !cn.getClassName().equals(null) && !cn.getClassName().equals("com.android.searchlauncher.SearchLauncher") ) {

                try {

                    if(cn.getClassName().contains("android.launcher")){

                        Log.d("Class1--",""+cn.getClassName());
                        classIntent = new Intent(this, LoginScreen.class);
                    }else{
                        Log.d("Class--",""+cn.getClassName());
                        c = Class.forName(cn.getClassName());
                        classIntent = new Intent();
                    }

                }catch (Exception e){

                    classIntent = new Intent(this, LoginScreen.class);
                    Log.d("Class2--",""+cn.getClassName());
                    e.printStackTrace();
                }

            } else {

                classIntent = new Intent(this, LoginScreen.class);
            }

            if(classIntent == null){

                classIntent = new Intent(this, LoginScreen.class);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, classIntent,
                    PendingIntent.FLAG_ONE_SHOT);


            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.samba)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.samba))
                    .setBadgeIconType(R.mipmap.samba)
                    .setContentTitle(title)
                    .setStyle(new NotificationCompat.BigTextStyle())
                    .setContentText(body)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            Random random = new Random();
            int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

//        Notification notification = notificationBuilder.build();
//        ShortcutBadger.applyNotification(this,notification,1);
//        ShortcutBadger.applyCount(this, 2); //for 1.1.4+
//        BadgeUtils.setBadge(this,(int)3);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int notificationId = 1;
                String channelId = "channel-01";
                String channelName = "Channel Name";
                mChannel = null;
                int importance = NotificationManager.IMPORTANCE_HIGH;
                if (mChannel == null) {
                    mChannel = new NotificationChannel
                            (String.valueOf(m), title, importance);
                    mChannel.setDescription(body);
                    mChannel.enableVibration(true);
                    notificationManager.createNotificationChannel(mChannel);
                }
                notificationBuilder.setChannelId(String.valueOf(m));
                notificationManager.notify(m, notificationBuilder.build());
                count = constants.badgeCountCredentials.getInt("badge", 0);
                Log.d("Count1---", "---" + count);
                count = count + 1;
                constants.badgeCountEditor.putInt("badge", count);
                constants.badgeCountEditor.commit();
                constants.badgeCountEditor.apply();
                Log.d("Count---", "---" + count);
                ShortcutBadger.applyCount(getApplicationContext(), count);
            } else {
                notificationManager.notify(m, notificationBuilder.build());
                count = constants.badgeCountCredentials.getInt("badge", 0);
                Log.d("Count1---", "---" + count);
                count = count + 1;
                constants.badgeCountEditor.putInt("badge", count);
                constants.badgeCountEditor.commit();
                constants.badgeCountEditor.apply();
                Log.d("Count---", "---" + count);
                ShortcutBadger.applyCount(getApplicationContext(), count);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("failedToParse", "--"+e.getLocalizedMessage());
        }
    }


//    private void displayCustomNotificationForOrders(String title, String description) {
//        if (notifManager == null) {
//            notifManager = (NotificationManager) getSystemService
//                    (Context.NOTIFICATION_SERVICE);
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationCompat.Builder builder;
//            Intent intent = new Intent(this, Dashboard.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            PendingIntent pendingIntent;
//            int importance = NotificationManager.IMPORTANCE_HIGH;
//            if (mChannel == null) {
//                mChannel = new NotificationChannel
//                        ("0", title, importance);
//                mChannel.setDescription(description);
//                mChannel.enableVibration(true);
//                notifManager.createNotificationChannel(mChannel);
//            }
//            builder = new NotificationCompat.Builder(this, "0");
//
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
//                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            pendingIntent = PendingIntent.getActivity(this, 1251, intent, PendingIntent.FLAG_ONE_SHOT);
//            builder.setContentTitle(title)
//                    .setSmallIcon(getNotificationIcon()) // required
//                    .setContentText(description)  // required
//                    .setDefaults(Notification.DEFAULT_ALL)
//                    .setAutoCancel(true)
//                    .setLargeIcon(BitmapFactory.decodeResource
//                            (getResources(), R.mipmap.logo))
//                    .setBadgeIconType(R.mipmap.logo)
//                    .setContentIntent(pendingIntent)
//                    .setSound(RingtoneManager.getDefaultUri
//                            (RingtoneManager.TYPE_NOTIFICATION));
//            Notification notification = builder.build();
//            notifManager.notify(0, notification);
//        } else {
//
//            Intent intent = new Intent(this, Dashboard.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            PendingIntent pendingIntent = null;
//
//            pendingIntent = PendingIntent.getActivity(this, 1251, intent, PendingIntent.FLAG_ONE_SHOT);
//
//            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                    .setContentTitle(title)
//                    .setContentText(description)
//                    .setAutoCancel(true)
//                    .setColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimary))
//                    .setSound(defaultSoundUri)
//                    .setSmallIcon(getNotificationIcon())
//                    .setContentIntent(pendingIntent)
//                    .setStyle(new NotificationCompat.BigTextStyle().setBigContentTitle(title).bigText(description));
//
//            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(1251, notificationBuilder.build());
//        }
//    }
//
//    private int getNotificationIcon() {
//        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
//        return useWhiteIcon ? R.mipmap.logo : R.mipmap.logo;
//    }
//}
}