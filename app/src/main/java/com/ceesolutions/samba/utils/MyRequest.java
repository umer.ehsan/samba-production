package com.ceesolutions.samba.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 6/8/18.
 */

public class MyRequest {
    private static Context context;
    private static SharedPreferences sharedPrefs;
    private static String serverUrl = "https://serverURL";

    public MyRequest(Context context) {
        this.context = context;
        this.sharedPrefs = context.getSharedPreferences("preference", Context.MODE_PRIVATE);
    }

    public static String getUrlApi(String service){
        return service;
    }

    public static JSONObject getJsonPayload(JSONObject params, String service){

        JSONObject jsonData = null;
        JSONObject request = null;
        try {
            jsonData = new JSONObject();
            request = new JSONObject();
            request.put("device",android.os.Build.BRAND + " " + android.os.Build.DEVICE + " " + android.os.Build.MODEL + " - v." + Build.VERSION.RELEASE);
            request.put("device_platform","android");
            request.put("device_type","android");
            request.put("params",params);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonData;
    }

    public void sendRequest(String urlAPI, Context cont, JSONObject params, RequestQueue requestQueue, final VolleyCallback callback){

        JSONObject jsonData = getJsonPayload(params,urlAPI);
        String url = getUrlApi(urlAPI);
        HttpsTrustManager.allowMySSL(cont);
        JsonObjectRequest postRequest = new JsonObjectRequest(url, jsonData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject resp){
                        callback.onSuccess(resp);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("ERRORE", "Error: " + error.getMessage());
                        callback.onError();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
                //Log.i("SETHEADERJSON",headers.toString());
                return headers;
            }
        };

        int socketTimeout = 10000;//10 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);

        requestQueue.add(postRequest);
    }
}