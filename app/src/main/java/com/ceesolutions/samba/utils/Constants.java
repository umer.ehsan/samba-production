package com.ceesolutions.samba.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.mastercard.mpqr.pushpayment.model.PushPaymentData;

import java.io.File;
import java.io.IOException;
import java.net.NetworkInterface;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ayaz on 1/10/18.
 */

public class Constants {

    //    public static final String EncryptKey = "F6A4EEPUMNHR4D1D89NIEDTF4A4RXA1N";
//    public static final String IV = "YN1EUF7F2E676SPX";
    public static Date dateObj;
    public static String companyID = "12";
    public static String companyType = "7";
    public static String checkForStan = "";
    public static String checkForRRN = "";
    public static String filterGatewayName = "";
    public static final String EncryptKey = Constant1.xK_aa + Constant1.xK_bb + Constant2.xK_aa + Constant2.xK_bb + Constant3.xK_aa + Constant3.xK_bb + Constant4.xK_aa + Constant4.xK_bb;
    public static final String IV = Constant1.xV_cc + Constant2.xV_cc + Constant3.xV_cc + Constant4.xV_cc;
    public static final String PREFS_NAME = "Samba";
    public static final String FINGERPRINT = "fingerPrint";
    public static final String SOURCE = "source";
    public static final String DESTINATION = "destination";
    public static final String FORGOT_PASSWORD = "FP";
    public static final String BADGE_COUNT = "badge_count";
    public static final String ACCOUNTS = "account";
    public static final String ACCOUNTSLIST = "accountList";
    public static final String PURPOSELIST = "purposeList";
    public static final String PURPOSEPREPAIDLIST = "purposePrepaidList";
    public static final String PURPOSEBILLLIST = "purposeBillList";
    public static final String STATUSLIST = "statusList";
    public static final String FCM = "fcm";
    public static final String CREDENTIALS_NAME = "credentials";
    public static final String LANGUAGE = "language";
    public static final String NOTI = "noti";
    public static final String COACHMARKS = "coach";
    public static final String ISFINGERCHECK = "isFinger";
    public static final String REASON = "reason";
    public static final String BILLER = "biller";
    public static final String CHANNEL = "channel";
    public static final String CONSTANST = "y1ndnIxd51gsJDkH";
    public static final String PASSWORDPOLICY = "- Password must contain atleast one numeric character." +
            "\n - Password must contain atleast one of the following special characters ( @#$!|?_ ).\n - Password must contain a capital letter.\n - Maximium length of password must be less then 16 characters";
    public static final String Invalid_Message = "Invalid Session";
    public static final String NON_SAMBA_CREDENTIALS_NAME = "non_credentials";
    public static final String EMAIL_REGEX = "\"^[_A-Za-z0-9-\\\\+]+(\\\\.[_A-Za-z0-9-]+)*@\"\n" +
            "                                + \"[A-Za-z0-9-]+(\\\\.[A-Za-z0-9]+)*(\\\\.[A-Za-z]{2,})$\"";
    public static final String OLD_EMAIL_REGEX = "\"[a-zA-Z0-9._-]+@[a-z\\\\-]+.[a-z]+[.[a-z]]+\"";
    public static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*?[0-9])(?=.*[A-Z])(?=.*?[^\\w]).{8,})";
    //   "((?=.*\\d)(?=.*?[0-9])(?=.*[A-Z])(?=.*?[^\\w\\s]).{10,})";
    public static final String EXCLUDE_PASSWORD_PATTERN =
            "([&+,~'<>^()%\"])";
    public static final String USERNAME_PATTERN =
            "(^[a-zA-Z0-9]*$)";
    public static final String allowedPassCharactersRegex = "([@#$!|?_])";
    public static final String bannedPassCharactersRegex = "([<>%()&+\\*-])";
    public static final String allowedPassNumericCharactersRegex = "([0-9])";
    public static final String allowedPassAlphaCharactersRegex = "(^[A-Z]{1,}[a-z])";
    public static DateFormat df;
    public static String dateTime;
    public static String accountNumber;
    public static String signsUserName = "signs";
    public static String signsPassword = "wP06zrS2hPB6JacQHYMCSqog715aC9zqJZT4NYohltyjURHBQeGYj1cnQQolkLU6knA2jnzeOmrbaJMo@PLUS@3M6qw==";
    public static ImageView backBtn;
    public static TextView headingTxt;
    public static String strSpaceReplaceVal = "@SP@";
    public static String strSpaceSoundEx = "SP@000";
    public static String Old_Shared_Key = "81a238ae7fd6df71c4a067112fcffde2";
    public static String Shared_KEY = "";//"81a238ae7fd6df71c4a067112fcffde2"
    static Constants constants;
    public Context context;
    public String version;
    public String model;
    public String appVersion;
    public WifiManager manager;
    public WifiInfo info;
    public String passCode;
    public static String QR_MERCHANT_NAME = "";
    public static String QR_MERCHANT_IDENTIFIER = "";
    public static String QR_MERCHANT_CATEGORYCODE = "";
    public static String QR_COMPLETE_DATA = "";
    public static String QR_MERCHANT_CURRENCYCODE = "586";
    public static String QR_MERCHANT_LOGOLINK = "";
    public static String QR_GATEWAY_IDENTIFIER = "";
    public static String QR_PAYMENT_TYPE =  "";
    public static String QR_COUNTRY_CODE = "";
    public static String QR_CITY_CODE = "";
    public static String QR_TRANSACTION_AMOUNT = "";
    public static String QR_FEE_INDICATOR = "";
    public static String QR_FEE_FIXED = "";
    public static String QR_FEE_PERCENTAGE = "";
    public static String Gateway = "";
    public static String logoLink = "";
    public static String MERCHANTIDENTIFIER = "";
    public static String OrderDetails  = "";
    public static  int requestTimeout = 180000;
    public static PushPaymentData  pushPaymentData;
    public static String CollectionAccount = "";
    public static String Currency = "";
    public static String GatewayName = "";
    public static String code = "";
    public static String BookingDetail = "";
    public static String ProcCode = "";
    public static String Android = "";
    public static String OrignalAcquiringID = "";
    public static String OriginalMT = "";
    public static HashMap<String, String> currencyHashMap;


    //    public static String signsPassword = "123456";
    public String address;
    public SharedPreferences sharedPreferences, credentialsSharedPreferences, nonSambaCredentials, forgotPasswordCredentials, badgeCountCredentials, languageCredentials, sourceAccount, destinationAccount, accounts, accountList, purposeList, puposePrepaid, purposeBill, status, fingerPrint, fcmSharedPref, notiPref, reasonList, channelList, coachMarks, isFingerCheck,billerList;
    public SharedPreferences.Editor editor, credentialsEditor, nonSambaCredentialsEditor, forgotPasswordEditor, badgeCountEditor, languageEditor, sourceAccountEditor, destinationAccountEditor, accountsEditor, accountListEditor, purposeListEditor, purposePrepaidList, purposeBillList, statusListEditor, fingerPrintEditor, fcmEditor, notiEditor, reasonListEditor, channelListEditor, coachMarksEditor, isFingerCheckEditor,billerListEditor;

    private Constants(Context mContext) {
        context = mContext;

        version = Build.VERSION.RELEASE;
        model = Build.MODEL;
        appVersion = "v2.10";
//        appVersion = "v1.2";
        manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        info = manager.getConnectionInfo();

        if (!TextUtils.isEmpty(getWifiMacAddress())) {
            address = getWifiMacAddress();
        } else {
            address = info.getMacAddress();
        }
//        try {
//            PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
//            appVerison = pInfo.versionName;
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }

        sharedPreferences = context.getSharedPreferences(PREFS_NAME, context.MODE_PRIVATE);

        editor = sharedPreferences.edit();
        credentialsSharedPreferences = context.getSharedPreferences(CREDENTIALS_NAME, context.MODE_PRIVATE);

        credentialsEditor = credentialsSharedPreferences.edit();

        nonSambaCredentials = context.getSharedPreferences(NON_SAMBA_CREDENTIALS_NAME, context.MODE_PRIVATE);

        nonSambaCredentialsEditor = nonSambaCredentials.edit();

        forgotPasswordCredentials = context.getSharedPreferences(FORGOT_PASSWORD, context.MODE_PRIVATE);

        forgotPasswordEditor = forgotPasswordCredentials.edit();

        badgeCountCredentials = context.getSharedPreferences(BADGE_COUNT, context.MODE_PRIVATE);

        badgeCountEditor = badgeCountCredentials.edit();

        languageCredentials = context.getSharedPreferences(LANGUAGE, context.MODE_PRIVATE);

        languageEditor = languageCredentials.edit();

        sourceAccount = context.getSharedPreferences(SOURCE, context.MODE_PRIVATE);

        sourceAccountEditor = sourceAccount.edit();

        destinationAccount = context.getSharedPreferences(DESTINATION, context.MODE_PRIVATE);

        destinationAccountEditor = destinationAccount.edit();

        accounts = context.getSharedPreferences(ACCOUNTS, context.MODE_PRIVATE);

        accountsEditor = accounts.edit();

        accountList = context.getSharedPreferences(ACCOUNTSLIST, context.MODE_PRIVATE);

        accountListEditor = accountList.edit();

        purposeList = context.getSharedPreferences(PURPOSELIST, context.MODE_PRIVATE);

        purposeListEditor = purposeList.edit();

        puposePrepaid = context.getSharedPreferences(PURPOSEPREPAIDLIST, context.MODE_PRIVATE);

        purposePrepaidList = puposePrepaid.edit();

        purposeBill = context.getSharedPreferences(PURPOSEBILLLIST, context.MODE_PRIVATE);

        purposeBillList = purposeBill.edit();


        status = context.getSharedPreferences(STATUSLIST, context.MODE_PRIVATE);

        statusListEditor = status.edit();

        fingerPrint = context.getSharedPreferences(FINGERPRINT, context.MODE_PRIVATE);

        fingerPrintEditor = fingerPrint.edit();

        fcmSharedPref = context.getSharedPreferences(FCM, context.MODE_PRIVATE);

        fcmEditor = fcmSharedPref.edit();

        notiPref = context.getSharedPreferences(NOTI, context.MODE_PRIVATE);

        notiEditor = notiPref.edit();

        reasonList = context.getSharedPreferences(REASON, context.MODE_PRIVATE);

        reasonListEditor = reasonList.edit();


        channelList = context.getSharedPreferences(CHANNEL, context.MODE_PRIVATE);

        channelListEditor = channelList.edit();

        coachMarks = context.getSharedPreferences(COACHMARKS, context.MODE_PRIVATE);

        coachMarksEditor = coachMarks.edit();

        isFingerCheck = context.getSharedPreferences(ISFINGERCHECK, context.MODE_PRIVATE);

        isFingerCheckEditor = isFingerCheck.edit();

        billerList = context.getSharedPreferences(BILLER, context.MODE_PRIVATE);

        billerListEditor = billerList.edit();

    }

    public static Constants getConstants(Context mContext) {

        if (constants == null) {
            constants = new Constants(mContext);

        }
        df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        dateTime = df.format(Calendar.getInstance().getTime()).split("\\+")[0];
        return constants;
    }

    public static String getDateAndTime(){

        df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        dateTime = df.format(Calendar.getInstance().getTime()).split("\\+")[0];
        return dateTime;
    }

    public static String getDateForQRPayment(){

        df = new SimpleDateFormat("MMdd");
        dateTime = df.format(Calendar.getInstance().getTime()).split("\\+")[0];
        return dateTime;
    }

    public static String getTimeForQRPayment(){

        df = new SimpleDateFormat("HHmmss");
        dateTime = df.format(Calendar.getInstance().getTime()).split("\\+")[0];
        return dateTime;
    }

    public static String getWifiMacAddress() {
        try {
            String interfaceName = "wlan0";
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (!intf.getName().equalsIgnoreCase(interfaceName)) {
                    continue;
                }

                byte[] mac = intf.getHardwareAddress();
                if (mac == null) {
                    return "";
                }

                StringBuilder buf = new StringBuilder();
                for (byte aMac : mac) {
                    buf.append(String.format("%02X:", aMac));
                }
                if (buf.length() > 0) {
                    buf.deleteCharAt(buf.length() - 1);
                }
                return buf.toString();
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }


    public static final String getDeviceMac() {

        final String mac = Constant1.xK_aa + Constant1.xK_bb + PREFS_NAME + Constant2.xK_bb + Constant3.xK_aa + Constant3.xK_bb + Constant4.xK_aa + Constant4.xK_bb;
        return mac;
    }


    public static final String getDeviceIMEI() {


        final String imei = Constant1.xV_cc + Constant2.xV_cc + PREFS_NAME + Constant4.xV_cc;
        return imei;
    }

    public static boolean findBinary(String binaryName) {
        boolean found = false;
        if (!found) {
            String[] places = {"/sbin/", "/system/bin/", "/system/xbin/",
                    "/data/local/xbin/", "/data/local/bin/",
                    "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};
            for (String where : places) {
                if (new File(where + binaryName).exists()) {
                    found = true;

                    break;
                }
            }
        }
        return found;
    }

    public static boolean isRooted() {
        return findBinary("su");
    }

    public static boolean isRootAvailable() {
        for (String pathDir : System.getenv("PATH").split(":")) {
            if (new File(pathDir, "su").exists()) {
                return true;
            }
        }
        return false;
    }


    public static boolean isRootAvailableForRoot() {
        Process p = null;
        try {
            p = Runtime.getRuntime().exec(new String[]{"su"});
            writeCommandToConsole(p, "exit 0", true);
            int result = p.waitFor();
            if (result != 0)
                throw new Exception("Root check result with exit command " + result);
            return true;
        } catch (IOException e) {
            Log.e("Hello", "Su executable is not available ", e);
        } catch (Exception e) {
            Log.e("Hello", "Root is unavailable ", e);
        } finally {
            if (p != null)
                p.destroy();
        }
        return true;
    }

    private static String writeCommandToConsole(Process proc, String command, boolean ignoreError) {

        byte[] tmpArray = new byte[1024];
        try {


            proc.getOutputStream().write((command + "\n").getBytes());
            proc.getOutputStream().flush();
            int bytesRead = 0;
            if (proc.getErrorStream().available() > 0) {
                if ((bytesRead = proc.getErrorStream().read(tmpArray)) > 1) {
                    Log.e("Hello", new String(tmpArray, 0, bytesRead));
                    if (!ignoreError)
                        throw new Exception(new String(tmpArray, 0, bytesRead));
                }
            }
            if (proc.getInputStream().available() > 0) {
                bytesRead = proc.getInputStream().read(tmpArray);
                Log.i("Hello", new String(tmpArray, 0, bytesRead));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String(tmpArray);
    }


}
