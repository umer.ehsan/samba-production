package com.ceesolutions.samba.utils;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Viram Purohit on 5/12/2017.
 */


public class AESHelper {

    /*This method use to encript the sting  */
    public static String encrypt(String inputString) throws Exception {
        byte[] result = encrypt(inputString.getBytes());
        String result1 = Base64.encodeToString(result,
                Base64.NO_WRAP);//Instead of Default
        return result1;
    }

    public static String decrypt(String encrypted) throws Exception {
        byte[] data = Base64.decode(encrypted, Base64.NO_WRAP);
        byte[] result = decrypt(data);
        String original = new String(result, "UTF-8");

        return new String(original);
    }

    /*This method return encrypted text with byte code*/
    private static byte[] encrypt( byte[] message) throws Exception {
        byte[] KEY = "OFRna73m*aze01xY".getBytes();//Key for encryption

        SecretKeySpec skeySpec = new SecretKeySpec(KEY, "AES");
        byte[] ivx =  new byte[] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};//make same as web team
        IvParameterSpec ivSpec = new IvParameterSpec(ivx);
        Cipher ecipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
        ecipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);

        byte[] encrypted = ecipher.doFinal(message);
        return encrypted;
    }

    private static byte[] decrypt(byte[] encrypted) throws Exception {
        byte[] KEY = "OFRna73m*aze01xY".getBytes();//Key for decrypt

        SecretKeySpec skeySpec = new SecretKeySpec(KEY, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
        byte[] ivx =  new byte[] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};//make same as web team
        IvParameterSpec ivSpec = new IvParameterSpec(ivx);

        cipher.init(Cipher.DECRYPT_MODE, skeySpec,ivSpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return decrypted;
    }


}


