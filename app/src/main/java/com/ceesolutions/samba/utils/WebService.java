package com.ceesolutions.samba.utils;

/**
 * Created by ayaz on 12/18/17.
 */

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.ceesolutions.samba.utils.Constants.constants;

/**
 * Created by aali on 4/15/2017.
 */

public class WebService {
    public JSONObject loginReqResponse;
    public JSONObject regReqResponse;
    public JSONObject valReqResponse;
    public JSONObject valReqNonSambaResponse;
    public JSONObject valCodeReqResponse;
    public JSONObject valCodeReNonSambaqResponse;
    public JSONObject valConfirmReqResponse;
    public JSONObject valConfirmReqNonSambaResponse;
    public JSONObject passAndPinReqResponse;
    public JSONObject passAndPinReqNonSambaResponse;
    public JSONObject regeneratePinReqResponse;
    public JSONObject accountNbeneReqResponse;
    public JSONObject trustedDeviceReqResponse;
    public JSONObject getQuestionsReqResponse;
    public JSONObject setQuestionAnswerReqResponse;
    public JSONObject setImageReqResponse;
    public JSONObject setPasscodeReqResponse;
    public JSONObject regeneratePinRegistrationReqResponse;
    public JSONObject forgotPassValidationReqResponse;
    public JSONObject updatePasswordReqResponse;
    public JSONObject getPurposeList;
    public JSONObject getAccountsList;
    public JSONObject titleFetch;
    public JSONObject latestBalance;
    public JSONObject validateOtp;
    public JSONObject getBankList;
    public JSONObject submitFt;
    public JSONObject addBene;
    public JSONObject addNonSamba;
    public JSONObject addBeneFav;
    public JSONObject addFriendFav;
    public JSONObject removeBeneFav;
    public JSONObject friendsList;
    public JSONObject addFriend;
    public JSONObject messagesList;
    public JSONObject actionFriend;
    public JSONObject deleteFriend;
    public JSONObject reqestMoney;
    public String exceptionReqResponse;
    public String conversionReqResponse;
    public String conversionReqResponse2;
    public String conversionReqResponseOutofStock;
    public String conversionReqResponseStockCount;
    public int responseIdOutofStock;
    public int responseIdStockCount;
    public int responseId;
    public int responseId2;
    public int responseExceptionId;
    public String logoutResponse;
    public String conversionSimpleReqResponse;
    public JSONObject jsonObject;
    public String checkoutResponse;

    public JSONObject Login(final String userName, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.loginURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code"));
                                    JsonObj.put("Is_Active", jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Is_Active"));
                                    JsonObj.put("Is_Locked", jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Is_Locked"));
                                    JsonObj.put("Is_ForgotPassword", jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Is_ForgotPassword"));
                                    loginReqResponse = JsonObj;

                                } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code").equals("100")) {

                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code"));
                                    JsonObj.put("Is_Active", jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Is_Active"));
                                    JsonObj.put("sIdentityType", jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("sIdentityType"));
                                    JsonObj.put("sIdentityValue", jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("sIdentityValue"));
                                    loginReqResponse = JsonObj;

                                } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code").equals("17")) {

                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code"));
                                    loginReqResponse = JsonObj;

                                } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code").equals("18")) {

                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code"));
                                    loginReqResponse = JsonObj;

                                } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code").equals("19")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code"));

                                    loginReqResponse = JsonObj;

                                } else {
                                    loginReqResponse = jsonObject.put("Error", "Error");

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                loginReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sSigns_Username", userName);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sLogin_Type", "USERNAME_PASSWORD");

                    return params;
                }
            };


            requestQueue.add(postRequest);


            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return loginReqResponse;
    }

    public JSONObject getLoginReqResponse() {


        return loginReqResponse;
    }

    public JSONObject LoginWithPassAndPin(final String userName, final String pass, final String pin, final String version, final String model, final String dateTime, final String address, final String fcmToken, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.validateURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code"));
                                    JsonObj.put("T24_CIF", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("T24_CIF"));
                                    JsonObj.put("T24_Target", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("T24_Target"));
                                    JsonObj.put("FullName", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("FullName"));
                                    JsonObj.put("LegalID", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("LegalID"));
                                    JsonObj.put("Is_DeviceExist", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Is_DeviceExist"));
                                    JsonObj.put("Is_FirstLogin", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Is_FirstLogin"));
                                    JsonObj.put("MobileNumber", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("MobileNumber"));
                                    JsonObj.put("EmailAddress", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("EmailAddress"));

                                    passAndPinReqResponse = JsonObj;
                                } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code").equals("02")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code"));
                                    passAndPinReqResponse = JsonObj;
                                } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code").equals("10")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code"));
                                    passAndPinReqResponse = JsonObj;
                                } else
                                    passAndPinReqResponse = jsonObject.put("Error", "Error");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                passAndPinReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sSigns_Username", userName);
                    params.put("sSigns_Password", pass);
                    params.put("sSigns_PinData", pin);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sLogin_Type", "0");
                    params.put("sFcm_Token", fcmToken);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 70000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return passAndPinReqResponse;
    }

    public JSONObject getPassAndPinReqResponse() {

        return passAndPinReqResponse;
    }

    public JSONObject LoginWithPassAndPinForNonSamba(final String userName, final String pass, final String pin, final String version, final String model, final String dateTime, final String address, final String fcmToken, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.validatenonsambaURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code"));
                                    passAndPinReqNonSambaResponse = JsonObj;
                                } else
                                    passAndPinReqNonSambaResponse = jsonObject.put("Error", "Error");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                passAndPinReqNonSambaResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sSigns_Username", userName);
                    params.put("sSigns_Password", pass);
                    params.put("sPinData", pin);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sLogin_Type", "0");
                    params.put("sFcm_Token", ConfigAPI.fcmToken);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 70000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return passAndPinReqNonSambaResponse;
    }

    public JSONObject getPassAndPinReqNonSambaResponse() {

        return passAndPinReqNonSambaResponse;
    }

    public JSONObject RegeneratePin(final String userName, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("00")) {
                                    regeneratePinReqResponse = jsonObject;
                                } else
                                    regeneratePinReqResponse = jsonObject.put("Error", "Error");

                                Log.d("Response", regeneratePinReqResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                regeneratePinReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sSigns_Username", userName);

                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + regeneratePinReqResponse);
        return regeneratePinReqResponse;
    }

    public JSONObject getRegeneratePinReqResponse() {

        return regeneratePinReqResponse;
    }

    public JSONObject GetAccountNBeneList(final String userName, final String cif, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAccountNBenelistURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Local_BenefeciaryList", jsonObject.getJSONObject("Signs_BeneficiaryListResult").getJSONArray("Local_BenefeciaryList"));
//                                    JsonObj.put("T24_OwnAccountList", jsonObject.getJSONObject("Signs_BeneficiaryListResult").getJSONArray("T24_OwnAccountList"));
                                    accountNbeneReqResponse = JsonObj;
                                } else
                                    accountNbeneReqResponse = jsonObject.put("Error", "Error");

                                Log.d("Response", accountNbeneReqResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                accountNbeneReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sSigns_Username", userName);
                    params.put("sT24_CIF", cif);

                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + accountNbeneReqResponse);
        return accountNbeneReqResponse;
    }

    public JSONObject getAccountNbeneReqResponse() {

        return accountNbeneReqResponse;
    }

    public JSONObject RegeneratePinForRegistration(final String email, final String mobile, final String identityType, final String identityValue, final String version, final String model, final String dateTime, final String address, final String fullName, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinRegistrationURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getJSONObject("Signs_GenerateRegister_OTACResult").getString("Status_Code").equals("00")) {
                                    regeneratePinRegistrationReqResponse = jsonObject;
                                } else
                                    regeneratePinRegistrationReqResponse = jsonObject.put("Error", "Error");

                                Log.d("Response", regeneratePinRegistrationReqResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                regeneratePinRegistrationReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sEmailAddress", email);
                    params.put("sMobileNumber", mobile);
                    params.put("sIdentityType", identityType);
                    params.put("sLegalIdentityValue", identityValue);
                    params.put("sFullName", fullName);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        return regeneratePinRegistrationReqResponse;
    }

    public JSONObject getRegeneratePinRegistrationReqResponse() {

        return regeneratePinRegistrationReqResponse;
    }


    public JSONObject ForgotPasswordValidation(final String username, final String email, final String motherName, final String identityType, final String identityValue, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.forgotPasswordvalidationURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code"));
                                    JsonObj.put("Mobile_Number", jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Mobile_Number"));
                                    JsonObj.put("FullName", jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("FullName"));

                                    forgotPassValidationReqResponse = JsonObj;
                                } else if (jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code").equals("17")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code"));

                                    forgotPassValidationReqResponse = JsonObj;

                                } else if (jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code").equals("18")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code"));

                                    forgotPassValidationReqResponse = JsonObj;

                                } else if (jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code").equals("19")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code"));

                                    forgotPassValidationReqResponse = JsonObj;

                                } else if (jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code").equals("20")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code"));

                                    forgotPassValidationReqResponse = JsonObj;

                                } else
                                    forgotPassValidationReqResponse = jsonObject.put("Error", "Error");

                                Log.d("Response", forgotPassValidationReqResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                forgotPassValidationReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sUserMotherMaidenName", motherName);
                    params.put("sEmailAddress", email);
                    params.put("sLegalIdentityType", identityType);
                    params.put("sLegalIdentityValue", identityValue);
                    params.put("sSigns_Username", username);
                    params.put("sDeviceLatitude", "0");
                    params.put("sDeviceLongitude", "0");
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        return forgotPassValidationReqResponse;
    }

    public JSONObject getForgotPassValidationReqResponse() {

        return forgotPassValidationReqResponse;
    }


    public JSONObject UpdateUserPassword(final String username, final String newPassword, final String identityType, final String identityValue, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.forgotPasswordupdateURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_UserUpdatePasswordResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_UserUpdatePasswordResult").getString("Status_Code"));
                                    updatePasswordReqResponse = JsonObj;
                                } else
                                    updatePasswordReqResponse = jsonObject.put("Error", "Error");

                                Log.d("Response", updatePasswordReqResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                updatePasswordReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sSigns_Username", username);
                    params.put("sNewPassword", newPassword);
                    params.put("sLegalIdentityType", identityType);
                    params.put("sLegalIdentityValue", identityValue);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        return updatePasswordReqResponse;
    }

    public JSONObject getUpdatePasswordReqResponse() {

        return updatePasswordReqResponse;
    }

    public JSONObject Registration(final String nic, final String version, final String model, final String dateTime, final String address, final String isType, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.uservalidationURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject(response);
                                if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code"));

                                    regReqResponse = JsonObj;

                                } else if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("13")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code"));

                                    regReqResponse = JsonObj;

                                } else if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("11")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code"));

                                    regReqResponse = JsonObj;

                                } else if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("17")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code"));

                                    regReqResponse = JsonObj;

                                } else if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("18")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code"));

                                    regReqResponse = JsonObj;

                                } else if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("19")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code"));

                                    regReqResponse = JsonObj;

                                } else if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("20")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code"));

                                    regReqResponse = JsonObj;

                                } else
                                    regReqResponse = jsonObject.put("Error", "Error");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener()

                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                regReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            )

            {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sIdentityValue", nic);
                    params.put("sIdentityType", isType);

                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + regReqResponse);
        return regReqResponse;
    }

    public JSONObject getRegReqResponse() {

        return regReqResponse;
    }

    public JSONObject UserValidation(final String accountID, final String dob, final String mob, final String email, final String motherName, final String cnic,final String type, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.userinformationvalidationURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("T24_FullName", jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("T24_FullName"));
                                    JsonObj.put("T24_CIF", jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("T24_CIF"));
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Code"));

                                    valReqResponse = JsonObj;
                                } else if (jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Code").equals("17")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Code"));
                                    valReqResponse = JsonObj;
                                } else if (jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Code").equals("18")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Code"));
                                    valReqResponse = JsonObj;
                                } else if (jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Code").equals("19")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Code"));
                                    valReqResponse = JsonObj;
                                } else
                                    valReqResponse = jsonObject.put("Error", "Error");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                valReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sAccountNumber", accountID);
                    params.put("sDateOfBirth", dob);
                    params.put("sMobileNumber", mob);
                    params.put("sMotherMaidenName", motherName);
                    params.put("sEmailAddress", email);
                    params.put("sIdentityValue", cnic);
                    params.put("sIdentityType", type);

                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return valReqResponse;
    }

    public JSONObject getValReqResponse() {

        return valReqResponse;
    }

    public JSONObject UserValidationForNonSamba(final String userName, final String dob, final String mob, final String email, final String nicExpiry, final String identityValue, final String identityType, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.userinformationnonsambavalidationURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getString("Status_Code").equals("00")) {
                                    valReqNonSambaResponse = jsonObject;
                                } else
                                    valReqNonSambaResponse = jsonObject.put("Error", "Error");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                valReqNonSambaResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sCustomerName", userName);
                    params.put("sDateOfBirth", dob);
                    params.put("sMobileNumber", mob);
                    params.put("sNicExpiryDate", nicExpiry);
                    params.put("sCustomerEmail", email);
                    params.put("sIdentityType", identityValue);
                    params.put("sIdentityValue", identityType);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return valReqNonSambaResponse;
    }

    public JSONObject getValReqNonSambaResponse() {

        return valReqNonSambaResponse;
    }

    public JSONObject UserCodeValidation(final String pinData, final String cnic, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.usercodeverificationURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getJSONObject("Signs_ValidateRegister_OTACResult").getString("Status_Code").equals("00")) {
                                    valCodeReqResponse = jsonObject;
                                } else
                                    valCodeReqResponse = jsonObject.put("Error", "Error");

                                Log.d("Response", valCodeReqResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                valCodeReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sLegalIdentityValue", cnic);
                    params.put("sPinData", pinData);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + valCodeReqResponse);
        return valCodeReqResponse;
    }

    public JSONObject getValCodeReqResponse() {

        return valCodeReqResponse;
    }

    public JSONObject UserCodeValidationForNonSamba(final String pinData, final String identityValue, final String identityType, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.usercodenonsambaverificationURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("Status_Code").equals("00")) {
                                    valCodeReNonSambaqResponse = jsonObject;
                                } else
                                    valCodeReNonSambaqResponse = jsonObject.put("Error", "Error");

                                Log.d("Response", valCodeReNonSambaqResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                valCodeReNonSambaqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sIdentityValue", identityValue);
                    params.put("sIdentityType", identityType);
                    params.put("sSmsCode", pinData);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + valCodeReNonSambaqResponse);
        return valCodeReNonSambaqResponse;
    }

    public JSONObject getValCodeReNonSambaqResponse() {

        return valCodeReNonSambaqResponse;
    }

    public JSONObject UserConfirmation(final String userName, final String password, final String cnic, final String fullName, final String T24_CIF, final String mobileNo, final String email, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.userlogincreateURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject object = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_RegisterUserResult").getString("Status_Code").equals("00")) {
                                    object.put("Status_Code", jsonObject.getJSONObject("Signs_RegisterUserResult").getString("Status_Code"));
                                    valConfirmReqResponse = object;
                                } else if (jsonObject.getJSONObject("Signs_RegisterUserResult").getString("Status_Code").equals("13")) {
                                    object.put("Status_Code", jsonObject.getJSONObject("Signs_RegisterUserResult").getString("Status_Code"));
                                    valConfirmReqResponse = object;
                                } else {
                                    valConfirmReqResponse = jsonObject.put("Error", "Error");
                                }
                                Log.d("Response", valConfirmReqResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                valConfirmReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sSigns_Password", password);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sT24_LegalIdentityValue", cnic);
                    params.put("sT24_LegalIdentityType", "CNIC");
                    params.put("sT24_FullName", fullName);
                    params.put("sT24_CIF", T24_CIF);
                    params.put("sT24_EmailAddress", email);
                    params.put("sT24_MobileNumber", mobileNo);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + valConfirmReqResponse);
        return valConfirmReqResponse;
    }

    public JSONObject getValConfirmReqResponse() {

        return valConfirmReqResponse;
    }

    public JSONObject UserConfirmationForNonSamba(final String userName, final String password, final String identityValue, final String identityType, final String fullName, final String nicExpiry, final String email, final String mobile, final String image, final String dob, final String motherName, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.userinformationnonsambavalidationURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject object = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_RegisterNonSambaUserResult").getString("Status_Code").equals("00")) {
                                    object.put("Status_Code", jsonObject.getJSONObject("Signs_RegisterNonSambaUserResult").getString("Status_Code"));
                                    valConfirmReqNonSambaResponse = object;
                                } else if (jsonObject.getJSONObject("Signs_RegisterNonSambaUserResult").getString("Status_Code").equals("13")) {
                                    object.put("Status_Code", jsonObject.getJSONObject("Signs_RegisterNonSambaUserResult").getString("Status_Code"));
                                    valConfirmReqNonSambaResponse = object;

                                } else {
                                    valConfirmReqNonSambaResponse = jsonObject.put("Error", "Error");

                                }

                                Log.d("Response", valConfirmReqNonSambaResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                valConfirmReqNonSambaResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sSigns_Password", password);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sT24_EmailAddress", email);
                    params.put("sT24_MobileNumber", mobile);
                    params.put("sLegalId_Capture", image);
                    params.put("sLegalIdExpiry", nicExpiry);
                    params.put("sFullName", fullName);
                    params.put("sT24_LegalIdentityValue", identityValue);
                    params.put("sT24_LegalIdentityType", identityType);
                    params.put("sDateOfBirth", dob);
                    params.put("sMotherMaidenName", motherName);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + valConfirmReqNonSambaResponse);
        return valConfirmReqNonSambaResponse;
    }

    public JSONObject getValConfirmReqNonSambaResponse() {

        return valConfirmReqNonSambaResponse;
    }

    public JSONObject AddTrustedDevices(final String userName, final String token, final String target, final String version, final String model, final String dateTime, final String address, final String flag, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.trustedDeviceURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObject = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_RegisterTrustedDeviceResult").getString("Status_Code").equals("00")) {
                                    JsonObject.put("Status_Code", jsonObject.getJSONObject("Signs_RegisterTrustedDeviceResult").getString("Status_Code"));
                                    trustedDeviceReqResponse = JsonObject;
                                } else
                                    trustedDeviceReqResponse = jsonObject.put("Error", "Error");

                                Log.d("Response", trustedDeviceReqResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                trustedDeviceReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sDeviceMacAddress", address);
                    params.put("sFcm_Token", token);
                    params.put("sDeviceType", target);
                    params.put("iUserID", "0");
                    params.put("sNotificationFlag", flag);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + trustedDeviceReqResponse);
        return trustedDeviceReqResponse;
    }

    public JSONObject getTrustedDeviceReqResponse() {

        return trustedDeviceReqResponse;
    }

    public JSONObject GetSecretQuestions(final String userName, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getQuestionsURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_GetSecretQuestionResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("SecretQuestionList", jsonObject.getJSONObject("Signs_GetSecretQuestionResult").getJSONArray("SecretQuestionList"));
                                    getQuestionsReqResponse = JsonObj;
                                } else
                                    getQuestionsReqResponse = jsonObject.put("Error", "Error");

                                Log.d("Response", getQuestionsReqResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                getQuestionsReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sDeviceLatitude", "0");
                    params.put("sDeviceLongitude", "0");
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + getQuestionsReqResponse);
        return getQuestionsReqResponse;
    }

    public JSONObject getGetQuestionsReqResponse() {

        return getQuestionsReqResponse;
    }


    public JSONObject AddSecretQuestionAnswer(final String userName, final String quesitonCode, final String questionAnswer, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.setQuestionAnswerURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_SetSecretQuestionResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_SetSecretQuestionResult").getString("Status_Code"));

                                    setQuestionAnswerReqResponse = JsonObj;
                                } else
                                    setQuestionAnswerReqResponse = jsonObject.put("Error", "Error");

                                Log.d("Response", setQuestionAnswerReqResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                setQuestionAnswerReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sQuestionCode", quesitonCode);
                    params.put("sAnswerText", questionAnswer);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + setQuestionAnswerReqResponse);
        return setQuestionAnswerReqResponse;
    }

    public JSONObject getSetQuestionAnswerReqResponse() {

        return setQuestionAnswerReqResponse;
    }


    public JSONObject SetProfileImage(final String userName, final String identityValue, final String image, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.setProfileImageURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_SetProfileAvatarResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_SetProfileAvatarResult").getString("Status_Code"));

                                    setImageReqResponse = JsonObj;
                                } else
                                    setImageReqResponse = jsonObject.put("Error", "Error");

                                Log.d("Response", setImageReqResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                setImageReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sDeviceLatitude", "0");
                    params.put("sDeviceLongitude", "0");
                    params.put("sLegalIdentityValue", identityValue);
                    params.put("sBase64Image", image);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + setImageReqResponse);
        return setImageReqResponse;
    }

    public JSONObject getSetImageReqResponse() {

        return setImageReqResponse;
    }

    public JSONObject SetDevicePasscode(final String userName, final String identityValue, final String passcode, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.setDevicePasscodeURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_SetDevicePasscodeResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_SetDevicePasscodeResult").getString("Status_Code"));

                                    setPasscodeReqResponse = JsonObj;
                                } else
                                    setPasscodeReqResponse = jsonObject.put("Error", "Error");

                                Log.d("Response", setPasscodeReqResponse.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                setPasscodeReqResponse = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sDeviceLatitude", "0");
                    params.put("sDeviceLongitude", "0");
                    params.put("sLegalIdentityValue", identityValue);
                    params.put("sPassCodeValue", passcode);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + setPasscodeReqResponse);
        return setPasscodeReqResponse;
    }

    public JSONObject getSetPasscodeReqResponse() {

        return setPasscodeReqResponse;
    }

    public JSONObject GetPurposeOfTransfer(final String userName, final String type, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getPuposesURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("POTList", jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getJSONArray("POTList"));
                                    getPurposeList = JsonObj;
                                } else
                                    getPurposeList = jsonObject.put("Error", "Error");

                                Log.d("Response", getPurposeList.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                getPurposeList = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sDeviceLatitude", "0");
                    params.put("sDeviceLongitude", "0");
                    params.put("sPurposeGroupCode", type);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + getPurposeList);
        return getPurposeList;
    }

    public JSONObject getGetPurposeList() {

        return getPurposeList;
    }

    public JSONObject GetAccountList(final String userName, final String cif, final String type, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAccountListURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("AccountList", jsonObject.getJSONObject("Signs_T24AccountListResult").getJSONArray("AccountList"));
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code"));
                                    getAccountsList = JsonObj;
                                } else if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("14")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code"));
                                    getAccountsList = JsonObj;
                                } else
                                    getAccountsList = jsonObject.put("Error", "Error");

                                Log.d("Response", getAccountsList.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                getAccountsList = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sDeviceLatitude", "0");
                    params.put("sDeviceLongitude", "0");
                    params.put("sT24_CIF", cif);
                    params.put("sSigns_UserType", type);
                    params.put("sIsAllAccounts", "1");

                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + getAccountsList);
        return getAccountsList;
    }

    public JSONObject getGetAccountsList() {

        return getAccountsList;
    }


    public JSONObject TitleFetch(final String userName, final String fromAccount, final String toAccountNumber, final String currency, final String bankId, final String identityValue, final String stan, final String rrn, final String beneType, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.titleFetchURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Code"));
                                    JsonObj.put("ChargeCode", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("ChargeCode"));
                                    JsonObj.put("ConversionRate", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("ConversionRate"));
                                    JsonObj.put("AccountTitle", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("AccountTitle"));
                                    JsonObj.put("Currency", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Currency"));

                                    titleFetch = JsonObj;
                                } else
                                    titleFetch = jsonObject.put("Error", "Error");

                                Log.d("Response", titleFetch.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                titleFetch = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sToAccountNumber", toAccountNumber);
                    params.put("sBankID", bankId);
                    params.put("sLegalIdentityValue", identityValue);
                    params.put("sSTAN", stan);
                    params.put("sRRN", rrn);
                    params.put("sBeneType", beneType);
                    params.put("sFromAccountNumber", fromAccount);
                    params.put("sDeviceLatitude", "0");
                    params.put("sDeviceLongitude", "0");
                    params.put("sBeneCurrencyCode", currency);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + titleFetch);
        return titleFetch;
    }

    public JSONObject getTitleFetch() {

        return titleFetch;
    }

    public JSONObject LatestBalance(final String userName, final String toAccountNumber, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getLatestBalURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code"));
                                    JsonObj.put("AvailBal", jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"));

                                    latestBalance = JsonObj;
                                } else
                                    latestBalance = jsonObject.put("Error", "Error");

                                Log.d("Response", latestBalance.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                latestBalance = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sT24_AccountNumber", toAccountNumber);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + latestBalance);
        return latestBalance;
    }

    public JSONObject getLatestBalance() {

        return latestBalance;
    }

    public JSONObject IPINVerification(final String userName, final String pindata, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.ipinVerificationURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_ValidateOTPResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ValidateOTPResult").getString("Status_Code"));
                                    validateOtp = JsonObj;
                                } else if (jsonObject.getJSONObject("Signs_ValidateOTPResult").getString("Status_Code").equals("10")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ValidateOTPResult").getString("Status_Code"));
                                    validateOtp = JsonObj;
                                } else
                                    validateOtp = jsonObject.put("Error", "Error");


                                Log.d("Response", validateOtp.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                validateOtp = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sSigns_Username", userName);
                    params.put("sPinData", pindata);

                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + validateOtp);
        return validateOtp;
    }

    public JSONObject getValidateOtp() {

        return validateOtp;
    }

    public JSONObject SubmitFT(final String userName, final String fromAccount, final String toAccount, final String amount, final String branchCode, final String currency, final String comments, final String bankID, final String identityValue, final String beneType, final String beneID, final String mobileNumber, final String email, final String purpose, final String stan, final String rrn, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.submitFTURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_FundsTransferResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_FundsTransferResult").getString("Status_Code"));
                                    JsonObj.put("Transaction_Reference", jsonObject.getJSONObject("Signs_FundsTransferResult").getString("Transaction_Reference"));
                                    submitFt = JsonObj;
                                } else
                                    submitFt = jsonObject.put("Error", "Error");

                                Log.d("Response", submitFt.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                submitFt = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sSigns_Username", userName);
                    params.put("sDeviceLatitude", "0");
                    params.put("sDeviceLongitude", "0");
                    params.put("sFromAccountNumber", fromAccount);
                    params.put("sToAccountNumber", toAccount);
                    params.put("sTransactionAmount", amount);
                    params.put("sBranchCode", branchCode);
                    params.put("sDebitCurrency", currency);
                    params.put("sNarration", comments);
                    params.put("sComments", comments);
                    params.put("sBankID", bankID);
                    params.put("sLegalIdentityValue", identityValue);
                    params.put("sBeneType", beneType);
                    params.put("sBeneID", beneID);
                    params.put("sSigns_MobileNumber", mobileNumber);
                    params.put("sSigns_EmailAddress", email);
                    params.put("sPurposeOfTransaction", purpose);
                    params.put("sSTAN", stan);
                    params.put("sRRN", rrn);


                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + submitFt);
        return submitFt;
    }

    public JSONObject getSubmitFt() {

        return submitFt;
    }

    public JSONObject GetBankList(final String userName, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getBankListURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_BankListResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("BankList", jsonObject.getJSONObject("Signs_BankListResult").getJSONArray("BankList"));
                                    getBankList = JsonObj;
                                } else
                                    getBankList = jsonObject.put("Error", "Error");

                                Log.d("Response", getBankList.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                getBankList = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + getBankList);
        return getBankList;
    }

    public JSONObject getGetBankList() {

        return getBankList;
    }

    public JSONObject AddBeneficiary(final String userName, final String alias, final String accountType, final String mobileNumber, final String email, final String bankID, final String userNumber, final String currency, final String title, final String identityValue, final String bankName, final String accountNumber, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.addBeneficiaryURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Status_Code"));
                                    JsonObj.put("BeneficiaryID", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("BeneficiaryID"));

                                    addBene = JsonObj;
                                } else
                                    addBene = jsonObject.put("Error", "Error");

                                Log.d("Response", addBene.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                addBene = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sSigns_Alias", alias);
                    params.put("sSigns_AccountType", accountType);
                    params.put("sSigns_AccountNumber", accountNumber);
                    params.put("sSigns_UserMobileNo", mobileNumber);
                    params.put("sSigns_UserEmailAddress", email);
                    params.put("sSigns_BankID", bankID);
                    params.put("sLegalIdentityValue", identityValue);
                    params.put("sSigns_CustomerMobileNo", userNumber);
                    params.put("sBeneAccountTitle", title);
                    params.put("sBeneCurrency", currency);
                    params.put("sSigns_BankName", bankName);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + addBene);
        return addBene;
    }

    public JSONObject getAddBene() {

        return addBene;
    }

    public JSONObject AddBeneficiaryToFav(final String userName, final String beneId, final String favFlag, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.addBeneFavURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_BeneficiaryFavorite_Mark_UnMarkResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_BeneficiaryFavorite_Mark_UnMarkResult").getString("Status_Code"));

                                    addBeneFav = JsonObj;
                                } else
                                    addBeneFav = jsonObject.put("Error", "Error");

                                Log.d("Response", addBeneFav.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                addBene = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sSigns_BeneID", beneId);
                    params.put("sSigns_BeneFavoriteFlag", favFlag);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + addBeneFav);
        return addBeneFav;
    }

    public JSONObject getAddBeneFav() {

        return addBeneFav;
    }

    public JSONObject DeletBene(final String userName, final String beneId, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.removeBeneURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_RemoveBeneficiaryResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_RemoveBeneficiaryResult").getString("Status_Code"));

                                    removeBeneFav = JsonObj;
                                } else
                                    removeBeneFav = jsonObject.put("Error", "Error");

                                Log.d("Response", removeBeneFav.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                removeBeneFav = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sSigns_BeneID", beneId);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + removeBeneFav);
        return removeBeneFav;
    }

    public JSONObject getRemoveBeneFav() {

        return removeBeneFav;
    }

    public JSONObject AddNonSamba(final String userName, final String alias, final String accountType, final String mobileNumber, final String email, final String bankID, final String userNumber, final String currency, final String title, final String identityValue, final String bankName, final String accountNumber, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.addNonSambaURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("Signs_AddNonSambaAccountResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_AddNonSambaAccountResult").getString("Status_Code"));
                                    JsonObj.put("ID", jsonObject.getJSONObject("Signs_AddNonSambaAccountResult").getString("ID"));

                                    addNonSamba = JsonObj;
                                } else
                                    addNonSamba = jsonObject.put("Error", "Error");

                                Log.d("Response", addNonSamba.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                addNonSamba = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sSigns_Username", userName);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sSigns_Alias", alias);
                    params.put("sSigns_AccountType", accountType);
                    params.put("sSigns_AccountNumber", accountNumber);
                    params.put("sSigns_UserMobileNo", "-");
                    params.put("sSigns_UserEmailAddress", "-");
                    params.put("sSigns_BankID", bankID);
                    params.put("sLegalIdentityValue", identityValue);
                    params.put("sSigns_CustomerMobileNo", userNumber);
                    params.put("sSigns_BankName", bankName);
                    params.put("sSigns_AccountTitle", title);
                    params.put("sComments", "-");
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + addNonSamba);
        return addNonSamba;
    }

    public JSONObject getAddNonSamba() {

        return addNonSamba;
    }

    public JSONObject GetFriendsList(final String userName, final String userNumber, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getFrindsURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("friends_listResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("friend_list", jsonObject.getJSONObject("friends_listResult").getJSONArray("friend_list"));
//                                    JsonObj.put("T24_OwnAccountList", jsonObject.getJSONObject("Signs_BeneficiaryListResult").getJSONArray("T24_OwnAccountList"));
                                    friendsList = JsonObj;
                                } else
                                    friendsList = jsonObject.put("Error", "Error");

                                Log.d("Response", friendsList.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                friendsList = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sUser_id", userName);
                    params.put("sUser_number", userNumber);

                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + friendsList);
        return friendsList;
    }

    public JSONObject getFriendsList() {

        return friendsList;
    }

    public JSONObject AddFriend(final String userID, final String userName, final String userNumber, final String friendNumber, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.addFriendURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("add_friendResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("add_friendResult").getString("Status_Code"));
//                                    JsonObj.put("T24_OwnAccountList", jsonObject.getJSONObject("Signs_BeneficiaryListResult").getJSONArray("T24_OwnAccountList"));
                                    addFriend = JsonObj;
                                } else
                                    addFriend = jsonObject.put("Error", "Error");

                                Log.d("Response", addFriend.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                addFriend = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sUser_id", userID);
                    params.put("sUser_number", userNumber);
                    params.put("sUser_name", userName);
                    params.put("sFriend_number", friendNumber);

                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + addFriend);
        return addFriend;
    }

    public JSONObject getAddFriend() {

        return addFriend;
    }

    public JSONObject GetMessages(final String userName, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getMessagesURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("message_center_for_userResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("messages", jsonObject.getJSONObject("message_center_for_userResult").getJSONArray("messages"));
//                                    JsonObj.put("T24_OwnAccountList", jsonObject.getJSONObject("Signs_BeneficiaryListResult").getJSONArray("T24_OwnAccountList"));
                                    messagesList = JsonObj;
                                } else
                                    messagesList = jsonObject.put("Error", "Error");

                                Log.d("Response", messagesList.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                messagesList = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sLegalIdentityValue", userName);

                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + messagesList);
        return messagesList;
    }

    public JSONObject getMessagesList() {

        return messagesList;
    }


    public JSONObject ActionFriendRequest(final String userID, final String userName, final String userNumber, final String friendNumber, final String messageId, final String action, final String requestId, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.actionFriendRequestURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("action_friend_requestResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("action_friend_requestResult").getString("Status_Code"));
//                                    JsonObj.put("T24_OwnAccountList", jsonObject.getJSONObject("Signs_BeneficiaryListResult").getJSONArray("T24_OwnAccountList"));
                                    actionFriend = JsonObj;
                                } else
                                    actionFriend = jsonObject.put("Error", "Error");

                                Log.d("Response", actionFriend.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                actionFriend = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sUser_id", userID);
                    params.put("sUser_number", userNumber);
                    params.put("sUser_name", userName);
                    params.put("sFriend_number", friendNumber);
                    params.put("sRequest_id", requestId);
                    params.put("sMessage_id", messageId);
                    params.put("sAction", action);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + actionFriend);
        return actionFriend;
    }

    public JSONObject getActionFriend() {

        return actionFriend;
    }

    public JSONObject AddFriendToFav(final String userId, final String userNumber, final String friendNumber, final String sAction, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.addFriendFavURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("update_favourite_friendResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("update_favourite_friendResult").getString("Status_Code"));

                                    addFriendFav = JsonObj;
                                } else
                                    addFriendFav = jsonObject.put("Error", "Error");

                                Log.d("Response", addFriendFav.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                addFriendFav = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sUser_id", userId);
                    params.put("sUser_number", userNumber);
                    params.put("sFriend_number", friendNumber);
                    params.put("sAction", sAction);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + addFriendFav);
        return addFriendFav;
    }

    public JSONObject getAddFriendFav() {

        return addFriendFav;
    }


    public JSONObject DeleteFriend(final String userId, final String userNumber, final String friendNumber, final String sAction, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.actionDeleteFriendURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("action_delete_favouriteResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("action_delete_favouriteResult").getString("Status_Code"));

                                    deleteFriend = JsonObj;
                                } else
                                    deleteFriend = jsonObject.put("Error", "Error");

                                Log.d("Response", deleteFriend.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                deleteFriend = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sUser_id", userId);
                    params.put("sUser_number", userNumber);
                    params.put("sFriend_number", friendNumber);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + deleteFriend);
        return deleteFriend;
    }

    public JSONObject getDeleteFriend() {

        return deleteFriend;
    }


    public JSONObject SendRequest(final String userId, final String userNumber, final String friendID, final String friendNumber, final String friendName, final String accountNumber, final String title, final String purpose, final String remarks, final String amount, final String flag, final String bankCode, final String currency, final String version, final String model, final String dateTime, final String address, RequestQueue requestQueue) {

        try {

            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.requestMoneyURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // response

                            Log.d("Response---", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject JsonObj = new JSONObject();
                                if (jsonObject.getJSONObject("request_moneyResult").getString("Status_Code").equals("00")) {
                                    JsonObj.put("Status_Code", jsonObject.getJSONObject("request_moneyResult").getString("Status_Code"));

                                    reqestMoney = JsonObj;
                                } else
                                    reqestMoney = jsonObject.put("Error", "Error");

                                Log.d("Response", reqestMoney.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // error
                            Log.d("Error.Response", "---" + error.getMessage());
                            try {
                                jsonObject = new JSONObject();
                                deleteFriend = jsonObject.put("Error", "Error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", address);
                    params.put("sOS_Version", version);
                    params.put("sDeviceName", model);
                  params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", dateTime);
                    params.put("sRequester_id", userId);
                    params.put("sRequester_number", userNumber);
                    params.put("sFriend_number", friendNumber);
                    params.put("sFriend_id", friendID);
                    params.put("sTo_account_number", accountNumber);
                    params.put("sTo_account_title", title);
                    params.put("sRequest_purpose", purpose);
                    params.put("sRequest_remarks", remarks);
                    params.put("sFriend_name", friendName);
                    params.put("sRequest_amount", amount);
                    params.put("sShow_account_number", flag);
                    params.put("sRequester_bankcode", bankCode);
                    params.put("sCurrency", currency);
                    return params;
                }
            };


            requestQueue.add(postRequest);
            postRequest.setRetryPolicy(new RetryPolicy() {
                @Override
                public int getCurrentTimeout() {
                    return 50000;
                }

                @Override
                public int getCurrentRetryCount() {
                    return 50000;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


        Log.d("Resp", "----" + reqestMoney);
        return reqestMoney;
    }

    public JSONObject getReqestMoney() {

        return reqestMoney;
    }
//    public JSONObject PushError(int id, final String baId, final String baName, final String errorMsg, RequestQueue requestQueue) {
//
//        try {
//            responseExceptionId = id;
//            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.exceptionURL,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            // response
//                            try {
//                                Log.d("Response1---", response);
//
//
//                                exceptionReqResponse = response.toString();
//
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            try {  // error
//                                Log.d("Error.Response", "---" + error.getMessage());
//
//                                exceptionReqResponse = "Error";
//
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//            ) {
//                @Override
//                protected Map<String, String> getParams() {
//                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("baId", baId);
//                    params.put("baName", baName);
//                    params.put("errorMessage", errorMsg);
//
//                    return params;
//                }
//            };
//
//
//            requestQueue.add(postRequest);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//        Log.d("Resp", "----" + loginReqResponse);
//        return loginReqResponse;
//    }
//
//    public String getExceptionReqResponse() {
//
//        Log.d("responseId2", "----" + responseExceptionId + "------" + exceptionReqResponse);
//
//        return responseExceptionId + "," + exceptionReqResponse;
//    }
//
//    public String CheckIn(final String uId, final String xCoordinate, final String yCoordinate, final String profileImg, RequestQueue requestQueue) {
//
//
//        try {
//
//
//            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.checkInURL,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            // response
//                            Log.d("Response", response.toString());
//                            checkInReqResponse = response.toString();
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            // error
//                            Log.d("Error.Response", "---" + error.getMessage());
//                            checkInReqResponse = "Error";
//                        }
//                    }
//            ) {
//                @Override
//                protected Map<String, String> getParams() {
//                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("userid", uId);
//                    params.put("xcoordinate", xCoordinate);
//                    params.put("ycoordinate", yCoordinate);
//                    params.put("image", profileImg);
//                    params.put("version", "2");
//                    return params;
//                }
//            };
//
//
//            requestQueue.add(postRequest);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return checkInReqResponse;
//    }
//
//    public String getCheckInReqResponse() {
//
//        return checkInReqResponse;
//    }
//
//
//    public String CheckOutUrl(final String uId, final String xCoordinate, final String yCoordinate, RequestQueue requestQueue) {
//
//        try {
//
//
//            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.checkoutURL,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            // response
//                            Log.d("Response", response.toString());
//                            checkoutResponse = response.toString();
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            // error
//                            Log.d("Error.Response", "---" + error.getMessage());
//                            checkoutResponse = "Error";
//                        }
//                    }
//            ) {
//                @Override
//                protected Map<String, String> getParams() {
//                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("userid", uId);
//                    params.put("xcoordinate", xCoordinate);
//                    params.put("ycoordinate", yCoordinate);
//
//                    return params;
//                }
//            };
//
//
//            requestQueue.add(postRequest);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return checkoutResponse;
//    }
//
//    public String getCheckoutResponse() {
//
//        return checkoutResponse;
//    }
//
//    public String Logout(final String uId, final String time, RequestQueue requestQueue) {
//
//
//        try {
//
//            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.logoutURL,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            // response
//                            Log.d("Response", response);
//                            JSONObject jsonObject = null;
//                            try {
//                                jsonObject = new JSONObject(response);
//                                if (jsonObject.has("status"))
//                                    logoutResponse = jsonObject.get("status").toString();
//                                else
//                                    logoutResponse = "Error";
//
//                                Log.d("Response", logoutResponse);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            // error
//                            Log.d("Error.Response", "---" + error.getMessage());
//                            logoutResponse = "Error";
//                        }
//                    }
//            ) {
//                @Override
//                protected Map<String, String> getParams() {
//                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("userid", uId);
//                    params.put("time", time);
//                    return params;
//                }
//            };
//
//
//            requestQueue.add(postRequest);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return logoutResponse;
//    }
//
//    public String getLogoutResponse() {
//
//        return logoutResponse;
//    }
//
//    public String Conversion(final String uId, final String city, final String store, final String name, final String phone, final String range, final String time, final String month, final String year, final String conversion, final String lattitude, final String longitude, RequestQueue requestQueue) {
//
//        try {
//
//            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.customerURL,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            // response
//                            Log.d("Response", response);
//                            conversionSimpleReqResponse = response;
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            // error
//                            error.printStackTrace();
//                            conversionSimpleReqResponse = "Error";
//                        }
//                    }
//            ) {
//                @Override
//                protected Map<String, String> getParams() {
//                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("userid", uId);
//                    params.put("customername", name);
//                    params.put("city", city);
//                    params.put("store", store);
//                    params.put("phone", phone);
//                    params.put("month", month);
//                    params.put("year", year);
//                    params.put("range", range);
//                    params.put("xcoordinate", lattitude);
//                    params.put("ycoordinate", longitude);
//                    params.put("time", time);
//                    params.put("conversion", conversion);
//                    return params;
//                }
//            };
//
//
//            requestQueue.add(postRequest);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return conversionSimpleReqResponse;
//    }
//
//    public String getConversionSimpleReqResponse() {
//
//        return conversionSimpleReqResponse;
//    }
//
//
//    public void Conversion(int id, final String uId, final String city, final String store, final String name, final String gender, final String age, final String phone, final String range, final String time, final String month, final String year, final String conversion, final String lattitude, final String longitude, RequestQueue requestQueue) {
//
//        try {
//
//            Log.d("Id", "----" + id);
//            responseId = id;
//            Log.d("responseId1", "----" + id);
//            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.customerURL,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            // response
//                            Log.d("Response----", response);
//                            conversionReqResponse = response;
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            // error
//                            Log.d("Error.Response", "---" + error.getMessage());
//                            conversionReqResponse = "Error";
//                        }
//                    }
//            ) {
//                @Override
//                protected Map<String, String> getParams() {
//                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("userid", uId);
//                    params.put("customername", name);
//                    params.put("city", city);
//                    params.put("store", store);
//                    params.put("phone", phone);
//                    params.put("month", month);
//                    params.put("year", year);
//                    params.put("range", range);
//                    params.put("xcoordinate", lattitude);
//                    params.put("ycoordinate", longitude);
//                    params.put("time", time);
//                    params.put("conversion", conversion);
//                    params.put("gender", gender);
//                    params.put("age", age);
//                    return params;
//                }
//            };
//
//
//            requestQueue.add(postRequest);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    public void Conversion(int id, final String uId, final String city, final String store, final String name, final String phone, final String range, final String time, final String month, final String year, final String conversion, final String lattitude, final String longitude, RequestQueue requestQueue) {
//
//        try {
//
//            Log.d("Id", "----" + id);
//            responseId = id;
//            Log.d("responseId1", "----" + id);
//            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.customerURL,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            // response
//                            Log.d("Response----", response);
//                            conversionReqResponse = response;
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            // error
//                            Log.d("Error.Response", "---" + error.getMessage());
//                            conversionReqResponse = "Error";
//                        }
//                    }
//            ) {
//                @Override
//                protected Map<String, String> getParams() {
//                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("userid", uId);
//                    params.put("customername", name);
//                    params.put("city", city);
//                    params.put("store", store);
//                    params.put("phone", phone);
//                    params.put("month", month);
//                    params.put("year", year);
//                    params.put("range", range);
//                    params.put("xcoordinate", lattitude);
//                    params.put("ycoordinate", longitude);
//                    params.put("time", time);
//                    params.put("conversion", conversion);
//                    params.put("gender", "NA");
//                    params.put("age", "NA");
//                    return params;
//                }
//            };
//
//
//            requestQueue.add(postRequest);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    public void ConversionStockCount(int id, final String uId, final String city, final String store, final String name, final String phone, final String range, final String time, final String month, final String year, final String conversion, final String lattitude, final String longitude, RequestQueue requestQueue) {
//
//        try {
//            Log.d("Id", "----" + id);
//            responseIdStockCount = id;
//            Log.d("responseId1", "----" + id);
//            StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.stockCountURL,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            // response
//                            Log.d("Response----", response);
//                            conversionReqResponseStockCount = response;
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            // error
//                            Log.d("Error.Response", "---" + error.getMessage());
//                            conversionReqResponseStockCount = "Error";
//                        }
//                    }
//            ) {
//                @Override
//                protected Map<String, String> getParams() {
//                    Map<String, String> params = new HashMap<String, String>();
//                    params.put("userid", uId);
//                    params.put("customername", name);
//                    params.put("city", city);
//                    params.put("store", store);
//                    params.put("phone", phone);
//                    params.put("month", month);
//                    params.put("year", year);
//                    params.put("range", range);
//                    params.put("xcoordinate", lattitude);
//                    params.put("ycoordinate", longitude);
//                    params.put("time", time);
//                    params.put("conversion", conversion);
//                    return params;
//                }
//            };
//
//
//            requestQueue.add(postRequest);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    public void ConversionOutOfStock(RequestQueue requestQueue) {
//
//        try {
//
//            StringRequest postRequest = new StringRequest(Request.Method.GET, "http://api.openweathermap.org/data/2.5/forecast?lat=24.941788&lon=67.050598&appid=c53385c48ef07a46a0cae7221382a497",
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            // response
//                            Log.d("Response----", response);
//                            conversionReqResponseOutofStock = response;
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            // error
//                            Log.d("Error.Response", "---" + error.getMessage());
//                            conversionReqResponseOutofStock = "Error";
//                        }
//                    }
//            );
//
//
//            requestQueue.add(postRequest);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//    public String getConversionReqResponse() {
//
//        Log.d("responseId2", "----" + responseId + "------" + conversionReqResponse);
//
//        return responseId + "," + conversionReqResponse;
//    }
//
//    public String getConversionReqResponse2() {
//
//        Log.d("responseId2", "----" + responseId2 + "------" + conversionReqResponse2);
//
//        return responseId2 + "," + conversionReqResponse2;
//    }
//
//    public String getConversionReqResponseOutofStock() {
//
//        Log.d("responseId2", "----" + responseIdOutofStock + "------" + conversionReqResponseOutofStock);
//
//        return responseIdOutofStock + "," + conversionReqResponseOutofStock;
//    }
//
//    public String getConversionReqResponseStockCount() {
//
//        Log.d("responseId2", "----" + responseIdStockCount + "------" + conversionReqResponseStockCount);
//
//        return responseIdStockCount + "," + conversionReqResponseStockCount;
//    }

}
