package com.ceesolutions.samba.utils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.RegistrationScreen;
import com.ceesolutions.samba.views.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by ayaz on 1/25/18.
 */

public class PopupDialog extends DialogFragment implements Camera.PictureCallback {
    Typeface typefaceReg, typefaceBold;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private PopupDialogSecretQuestion popupDialog;
    private ProgressDialog pd;
    private MirrorView mCamPreview;
    private FrameLayout mPreviewLayout;
    private int mCameraId = 1;
    private Camera mCam;
    Bitmap bmp;
    private final int PICTURE_TAKEN_FROM_GALLERY = 2;
    String imgString;
    CircleImageView circleImageView;
    boolean openCam = true;
    Button captureImage;
    private RelativeLayout relativeLayout;
    String userName,legalID;
    AlertDialog OptionDialog;

    private int findFirstFrontFacingCamera() {
        int foundId = 1;
        // find the first front facing camera
        int numCams = Camera.getNumberOfCameras();
        for (int camId = 0; camId < numCams; camId++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(camId, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                Log.d("Test", "Found front facing camera");
                foundId = camId;
                break;
            }
        }
        return foundId;
    }

    private void startCameraInLayout(FrameLayout layout, int cameraId) {

        // TODO pull this out of the UI thread.
        mCam = Camera.open(cameraId);
        if (mCam != null) {
            mCamPreview = new PopupDialog.MirrorView(getActivity(), mCam);
            layout.addView(mCamPreview);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCam == null && mPreviewLayout != null) {
            mPreviewLayout.removeAllViews();
//            startCameraInLayout(mPreviewLayout, mCameraId);
        }
    }

    @Override
    public void onPause() {
        if (mCam != null) {
            mCam.release();
            mCam = null;
        }
        super.onPause();
    }


    public void onPictureTaken(byte[] data, Camera camera) {
//        File pictureFileDir = new File(
//                Environment
//                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "SimpleSelfCam");
//
//        if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {
//
//            Log.d(DEBUG_TAG, "Can't create directory to save image");
//            Toast.makeText(this, "Can't make path to save pic.",
//                    Toast.LENGTH_LONG).show();
//            return;
//
//        }
//
//        String filename = pictureFileDir.getPath() + File.separator
//                + "latest_mug.jpg";
//        File pictureFile = new File(filename);

        try {
//            FileOutputStream fos = new FileOutputStream(pictureFile);
//            fos.write(data);
//            fos.close();
//            Toast.makeText(this, "Image saved as latest_mug.jpg",
//                    Toast.LENGTH_LONG).show();
            Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
            circleImageView.setImageBitmap(RotateBitmap(bitmap, 270));
            bmp = RotateBitmap(bitmap, 270);
            imgString = Base64.encodeToString(getBytesFromBitmap(bmp),
                    Base64.NO_WRAP);


        } catch (Exception error) {
            Log.d("Test", "File not saved: " + error.getMessage());
            Toast.makeText(getActivity(), "Can't save image.", Toast.LENGTH_LONG).show();
        }
    }

    public class MirrorView extends SurfaceView implements
            SurfaceHolder.Callback {
        private SurfaceHolder mHolder;
        private Camera mCamera;

        public MirrorView(Context context, Camera camera) {
            super(context);
            mCamera = camera;
            mHolder = getHolder();
            mHolder.addCallback(this);
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        public void surfaceCreated(SurfaceHolder holder) {
            try {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            } catch (Exception error) {
                Log.d("Test",
                        "Error starting mPreviewLayout: " + error.getMessage());
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int w,
                                   int h) {
            if (mHolder.getSurface() == null) {
                return;
            }

            // can't make changes while mPreviewLayout is active
            try {
                mCamera.stopPreview();
            } catch (Exception e) {

            }

            try {
                // set rotation to match device orientation
                setCameraDisplayOrientationAndSize();

                // start up the mPreviewLayout
                mCamera.setPreviewDisplay(mHolder);
                mCamera.startPreview();

            } catch (Exception error) {
                Log.d("Test",
                        "Error starting mPreviewLayout: " + error.getMessage());
            }
        }

        public void setCameraDisplayOrientationAndSize() {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(mCameraId, info);
//            Camera.Parameters params = mCamera.getParameters();
//            params.setJpegQuality(100);
//            params.setFocusMode(Camera.Parameters.FOCUS_MODE_FIXED);
//            mCamera.setParameters(params);
            int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
            int degrees = rotation;

            /*
             * // the above is just a shorter way of doing this, but could break
             * if the values change switch (rotation) { case Surface.ROTATION_0:
             * degrees = 0; break; case Surface.ROTATION_90: degrees = 90;
             * break; case Surface.ROTATION_180: degrees = 180; break; case
             * Surface.ROTATION_270: degrees = 270; break; }
             */

            int result;
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = (info.orientation + degrees) % 360;
                result = (360 - result) % 360;
            } else {
                result = (info.orientation - degrees + 360) % 360;
            }
            mCamera.setDisplayOrientation(result);

            Camera.Size previewSize = mCam.getParameters().getPreviewSize();
            if (result == 90 || result == 270) {
                // swap - the physical camera itself doesn't rotate in relation
                // to the screen ;)
                mHolder.setFixedSize(previewSize.height, previewSize.width);
            } else {
                mHolder.setFixedSize(previewSize.width, previewSize.height);

            }

        }

    }

    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
        return stream.toByteArray();
    }

    /**
     * creates a new instance of PropDialogFragment
     */

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //getting proper access to LayoutInflater is the trick. getLayoutInflater is a                   //Function
        LayoutInflater inflater = getActivity().getLayoutInflater();


        View view = inflater.inflate(R.layout.pop_up, null);
        OptionDialog = new AlertDialog.Builder(getActivity()).create();
        OptionDialog.setView(view);
        popupDialog = new PopupDialogSecretQuestion();
        requestQueue = Volley.newRequestQueue(getActivity());
        webService = new WebService();
        helper = Helper.getHelper(getActivity());
        constants = Constants.getConstants(getActivity());
        userName = constants.sharedPreferences.getString("userName","N/A");
        pd = new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.setTitle("Add Device to Device list");
        legalID = constants.sharedPreferences.getString("LegalID", "N/A");
        circleImageView = (CircleImageView) view.findViewById(R.id.profile_image);
        final TextView cancelBtn = (TextView) view.findViewById(R.id.cancelBtn);

        final TextView submitBtn = (TextView) view.findViewById(R.id.submitBtn);
//        cancelBtn.setTypeface(typefaceBold);
        mPreviewLayout = (FrameLayout) view.findViewById(R.id.camPreview);
        captureImage = (Button) view.findViewById(R.id.capture);
        final TextView confirmBtn = (TextView) view.findViewById(R.id.confirmBtn);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.relativeLayout);
        relativeLayout.setVisibility(View.GONE);
//        confirmBtn.setTypeface(typefaceBold);
        captureImage.setVisibility(View.GONE);
        captureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                takeFocusedPicture();
                mCam.takePicture(null, null, PopupDialog.this);
                circleImageView.setVisibility(View.VISIBLE);
                mPreviewLayout.setVisibility(View.GONE);
                relativeLayout.setVisibility(View.GONE);
                captureImage.setVisibility(View.GONE);
                cancelBtn.setVisibility(View.VISIBLE);
                confirmBtn.setVisibility(View.VISIBLE);
                cancelBtn.setEnabled(true);

            }
        });


//        dialog.setCancelable(false);// if decline button is clicked, close the custom dialog
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                if (mCam != null) {
                    mCam.release();
                    mCam = null;
                }
                if (!getActivity().getPackageManager()
                        .hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                    Toast.makeText(getActivity(), "No camera feature on this device",
                            Toast.LENGTH_LONG).show();
                } else {

                    mCameraId = findFirstFrontFacingCamera();

                    if (mCameraId >= 0) {

                        mPreviewLayout.setVisibility(View.INVISIBLE);
                        mPreviewLayout.bringToFront();
                        mPreviewLayout.removeAllViews();
                        startCameraInLayout(mPreviewLayout, mCameraId);
                    } else {
                        Toast.makeText(getActivity(), "No front facing camera found.",
                                Toast.LENGTH_LONG).show();
                    }
                }
//                    }

                cancelBtn.setEnabled(false);
                cancelBtn.setVisibility(View.INVISIBLE);
                captureImage.setVisibility(View.VISIBLE);
                relativeLayout.setVisibility(View.VISIBLE);
                relativeLayout.bringToFront();
                mPreviewLayout.setVisibility(View.VISIBLE);
                cancelBtn.setText("RETAKE");
                openCam = false;
            }
        });
        
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                SetProfileImage();
            }
        });

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
//                Toast.makeText(getActivity(), "Confirm..!", Toast.LENGTH_SHORT).show();
                getPictureFromGallery();
            }
        });

        return OptionDialog;
    }

    private void getPictureFromGallery() {
    /*
    //This allows to select the application to use when selecting an image.
    Intent i = new Intent(Intent.ACTION_GET_CONTENT);
    i.setType("image/*");
    startActivityForResult(Intent.createChooser(i, "Escoge una foto"), PICTURE_TAKEN_FROM_GALLERY);
    */

        //This takes images directly from gallery
        Intent gallerypickerIntent = new Intent(Intent.ACTION_PICK);
        gallerypickerIntent.setType("image/*");
        startActivityForResult(gallerypickerIntent, PICTURE_TAKEN_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap takenPictureData = null;

        switch (requestCode) {


            case PICTURE_TAKEN_FROM_GALLERY:
                if (resultCode == getActivity().RESULT_OK) {
                    takenPictureData = handleResultFromChooser(data);
                }
                break;
        }

        //And show the result in the image view.
//        if(takenPictureData!=null){
//            circleImageView.setImageBitmap(takenPictureData);
//        }
    }

    private Bitmap handleResultFromChooser(Intent data) {
        Bitmap takenPictureData = null;

        Uri photoUri = data.getData();
        if (photoUri != null) {
            try {
                //We get the file path from the media info returned by the content resolver
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(photoUri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String filePath = cursor.getString(columnIndex);
                useImage(photoUri);
//                byte[] decodedString = Base64.decode(filePath, Base64.URL_SAFE );
//                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                cursor.close();

//                takenPictureData = StringToBitMap(filePath);

            } catch (Exception e) {
//                ToolBox.dialog_showToastAlert(this, "Error getting selected image.", false);
            }
        }

        return takenPictureData;
    }

    void useImage(Uri uri) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
        //use the bitmap as you like
        circleImageView.setImageBitmap(bitmap);
    }

    public void SetProfileImage() {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                webService.SetProfileImage(userName,legalID, imgString, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                        JSONObject jsonObject = webService.getSetImageReqResponse();
                        Log.d("Response", "Object----" + jsonObject);

                        if (jsonObject != null) {

                            if (jsonObject.has("Error")) {

                                Toast.makeText(getActivity(), "Information is not valid!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(), RegistrationScreen.class);
                                pd.dismiss();
                                getActivity().overridePendingTransition(0, 0);
                                startActivity(intent);


                            } else {

                                try {


                                    if (jsonObject.get("Status_Code").equals("00")) {

                                        Toast.makeText(getActivity(), "Profile Picture Saved successfully !", Toast.LENGTH_SHORT).show();
//                                        Intent intent = new Intent(getActivity(), UserValidationScreen.class);
                                        pd.dismiss();
//                                        getActivity().overridePendingTransition(0, 0);
//                                        intent.putExtra("Type", "Samba");
//                                        startActivity(intent);
                                        popupDialog.setCancelable(false);
                                        popupDialog.show(getFragmentManager(), "dialog");
                                        OptionDialog.cancel();


                                    } else {
                                        Toast.makeText(getActivity(), "Profile Picture can not be set. Please try again later!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(), RegistrationScreen.class);
                                        pd.dismiss();
                                        getActivity().overridePendingTransition(0, 0);
                                        startActivity(intent);

                                    }


                                } catch (JSONException ex) {

                                    ex.printStackTrace();
                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), "Profile Picture can not be set. Please try again later!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), RegistrationScreen.class);
                            pd.dismiss();
                            getActivity().overridePendingTransition(0, 0);
                            startActivity(intent);

                        }
                    }
                }, 10000);
            } else {
                Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                pd.dismiss();


            }
        } catch (Exception e) {

            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
            pd.dismiss();

        }
    }
}