package com.ceesolutions.samba.utils;

/**
 * Created by ceeayaz on 6/17/18.
 */

public interface DrawableClickListener {

    public static enum DrawablePosition { TOP, BOTTOM, LEFT, RIGHT };
    public void onClick(DrawablePosition target);
}