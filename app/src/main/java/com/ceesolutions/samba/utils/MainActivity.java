package com.ceesolutions.samba.utils;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;


public class MainActivity extends AppCompatActivity {

    private TextView mTextView;
    JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = (TextView) findViewById(R.id.loginBtn);
        String url = "https://10.21.8.207:443/login";

        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(URL url) throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
                try {
                    httpsURLConnection.setSSLSocketFactory(getSSLSocketFactory());
                    httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        jsonObject = new JSONObject();

//        StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.validateURL,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        // response
//
//                        Log.d("Response---", response);
//
//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//                            JSONObject JsonObj = new JSONObject();
//                            if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code").equals("00")) {
//                                JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code"));
//                                JsonObj.put("T24_CIF", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("T24_CIF"));
//                                JsonObj.put("T24_Target", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("T24_Target"));
//                                JsonObj.put("FullName", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("FullName"));
//                                JsonObj.put("LegalID", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("LegalID"));
//                                JsonObj.put("Is_DeviceExist", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Is_DeviceExist"));
//                                JsonObj.put("Is_FirstLogin", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Is_FirstLogin"));
//                                JsonObj.put("MobileNumber", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("MobileNumber"));
//                                JsonObj.put("EmailAddress", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("EmailAddress"));
//
//
//                            } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code").equals("02")) {
//                                JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code"));
//
//                            } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code").equals("10")) {
//                                JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code"));
//
//                            } else
//                               Log.d("Erro","---");
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        // error
//                        Log.d("Error.Response", "---" + error.getMessage());
//                        try {
//                            jsonObject = new JSONObject();
////                            passAndPinReqResponse = jsonObject.put("Error", "Error");
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//        ) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("sService_Username", constants.signsUserName);
//                params.put("sService_Password", constants.signsPassword);
//                return params;
//            }
//        };
//
//
////        requestQueue.add(postRequest);
//        postRequest.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 70000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 50000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//
//            }
//        });
        jsonObject = new JSONObject();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    mTextView.setText(response.toString(5));
                } catch (JSONException e) {
                    mTextView.setText(e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mTextView.setText(error.toString());
            }
        });

        final RequestQueue requestQueue = Volley.newRequestQueue(this, hurlStack);

        requestQueue.add(jsonObjectRequest);

//        final RequestQueue requestQueue = Volley.newRequestQueue(this, hurlStack);
//
//        requestQueue.add(jsonObjectRequest);
    }

    // Let's assume your server app is hosting inside a server machine
    // which has a server certificate in which "Issued to" is "localhost",for example.
    // Then, inside verify method you can verify "localhost".
    // If not, you can temporarily return true
    private HostnameVerifier getHostnameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                //return true; // verify always returns true, which could cause insecure network traffic due to trusting TLS/SSL server certificates for wrong hostnames
                HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
//                return hv.verify("localhost", session);
                if (hostname.equals("127.0.0.1"))
                    return true;
                else return false;
            }
        };
    }

    private TrustManager[] getWrappedTrustManagers(TrustManager[] trustManagers) {
        final X509TrustManager originalTrustManager = (X509TrustManager) trustManagers[0];
        return new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return originalTrustManager.getAcceptedIssuers();
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0) {
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkClientTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            Log.w("checkClientTrusted", e.toString());
                        }
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0) {
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkServerTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            Log.w("checkServerTrusted", e.toString());
                        }
                    }
                }
        };
    }

    private SSLSocketFactory getSSLSocketFactory()
            throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException, KeyManagementException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream caInput = getResources().openRawResource(R.raw.cert); // this cert file stored in \app\src\main\res\raw folder path

        Certificate ca = cf.generateCertificate(caInput);
        caInput.close();

        KeyStore keyStore = KeyStore.getInstance("BKS");
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

        TrustManager[] wrappedTrustManagers = getWrappedTrustManagers(tmf.getTrustManagers());

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, wrappedTrustManagers, null);

        return sslContext.getSocketFactory();
    }
}