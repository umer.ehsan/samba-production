package com.ceesolutions.samba.utils;

/**
 * Created by ayaz on 1/30/18.
 */

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.RegistrationScreen;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ayaz on 1/25/18.
 */

public class PopupPassCode extends DialogFragment {

    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private ProgressDialog pd;
    Button confirmBtn;
    Button cancelBtn;
//    private PopupDialog popupDialog;
    String token, target, userName, passcode, legalID;
    private EditText editPasscode;
    private TextView errorMessage;
    AlertDialog OptionDialog;

    /**
     * creates a new instance of PropDialogFragment
     */
    public PopupPassCode() { /*empty*/ }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //getting proper access to LayoutInflater is the trick. getLayoutInflater is a                   //Function
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.pop_up_passcode, null);
        AlertDialog OptionDialog = new AlertDialog.Builder(getActivity()).create();
        OptionDialog.setView(view);

//        popupDialog = new PopupDialog();
        requestQueue = Volley.newRequestQueue(getActivity());
        webService = new WebService();
        helper = Helper.getHelper(getActivity());
        constants = Constants.getConstants(getActivity());
        legalID = constants.sharedPreferences.getString("LegalID", "N/A");
        pd = new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.setTitle("Add Device to Device list");
        token = constants.sharedPreferences.getString("fcmToken", "fcmToken");
        target = constants.sharedPreferences.getString("T24_Target", "N/A");
        userName = constants.sharedPreferences.getString("userName", "N/A");
        Intent intent = getActivity().getIntent();
        editPasscode = (EditText) view.findViewById(R.id.editPasscode);
        errorMessage = (TextView) view.findViewById(R.id.errorMessage);

        editPasscode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                passcode = editable.toString().trim();

            }
        });

        passcode = editPasscode.getText().toString().trim();

        cancelBtn = (Button) view.findViewById(R.id.skipBtn);


        confirmBtn = (Button) view.findViewById(R.id.confirmBtn);


//        dialog.setCancelable(false);// if decline button is clicked, close the custom dialog
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                SetPassCode();

            }
        });
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog


                if (TextUtils.isEmpty(passcode)) {
                    errorMessage.setVisibility(View.VISIBLE);
                    errorMessage.bringToFront();
                    errorMessage.setError("");
                    errorMessage.setText("Passcode can not be empty");

                } else {
                    confirmBtn.setEnabled(false);
                    SetPassCode();
                    pd.show();

                }


            }
        });

        return OptionDialog;
    }

    public void SetPassCode() {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                String md5 = Helper.md5(userName.toUpperCase() + passcode);
                webService.SetDevicePasscode(userName, legalID, md5, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                        JSONObject jsonObject = webService.getSetPasscodeReqResponse();
                        Log.d("Response", "Object----" + jsonObject);

                        if (jsonObject != null) {

                            if (jsonObject.has("Error")) {

                                Toast.makeText(getActivity(), "Information is not valid!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(), RegistrationScreen.class);
                                pd.dismiss();
                                getActivity().overridePendingTransition(0, 0);
                                startActivity(intent);
                                cancelBtn.setEnabled(true);


                            } else {

                                try {


                                    if (jsonObject.get("Status_Code").equals("00")) {

                                        Toast.makeText(getActivity(), "Passcode has been set Successfully !", Toast.LENGTH_SHORT).show();
                                        pd.dismiss();
                                        cancelBtn.setEnabled(true);
//                                        popupDialog.setCancelable(false);
//                                        popupDialog.show(getFragmentManager(), "dialog");
//                                        OptionDialog.cancel();

                                    } else {
                                        Toast.makeText(getActivity(), "Passcode can not be set. Please try again later!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(), RegistrationScreen.class);
                                        pd.dismiss();
                                        getActivity().overridePendingTransition(0, 0);
                                        startActivity(intent);
                                        cancelBtn.setEnabled(true);

                                    }


                                } catch (JSONException ex) {

                                    ex.printStackTrace();
                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), "Device cannot be added. Please try again later!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), RegistrationScreen.class);
                            pd.dismiss();
                            getActivity().overridePendingTransition(0, 0);
                            startActivity(intent);
                            cancelBtn.setEnabled(true);

                        }
                    }
                }, 5000);
            } else {
                Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                cancelBtn.setEnabled(true);
                pd.dismiss();


            }
        } catch (Exception e) {

            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
            cancelBtn.setEnabled(true);
            pd.dismiss();

        }
    }
}