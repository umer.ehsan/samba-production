package com.ceesolutions.samba.utils;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.accessControl.ProfileSetupScreen;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ayaz on 1/25/18.
 */

public class PopupDialogPermission extends DialogFragment {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private TextView confirmBtn;
    private TextView cancelBtn, message, doneBtn, heading, userNameText;
    private String token, target, userName;
    private AlertDialog OptionDialog;
    private String isFirstLogin;
    private Utils utils;
    private Dialog dialog;
    private String error = "";
    private AppPreferences appPreferences;
    private String location, notiEnabled;

    /**
     * creates a new instance of PropDialogFragment
     */
    public PopupDialogPermission() { /*empty*/ }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //getting proper access to LayoutInflater is the trick. getLayoutInflater is a                   //Function
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.pop_up_permission, null);
        OptionDialog = new AlertDialog.Builder(getActivity()).create();
        OptionDialog.setView(view);

//        popupDialog = new PopupDialog();
        requestQueue = Volley.newRequestQueue(getActivity());
        webService = new WebService();
        helper = Helper.getHelper(getActivity());
        constants = Constants.getConstants(getActivity());
        pd = new Dialog(getActivity());
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        utils = new Utils(getActivity());
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        try {
            CryptLib _crypt = new CryptLib();
            token = constants.fcmSharedPref.getString("fcmToken", "N/A");
            Log.d("Token", "--" + token);
            target = _crypt.decrypt(constants.sharedPreferences.getString("T24_Target", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            Log.d("Name----", "-" + target);
            notiEnabled = _crypt.decrypt(constants.sharedPreferences.getString("NotificationEnable", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            Intent intent = getActivity().getIntent();
            appPreferences = new AppPreferences(getActivity());
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }


            Log.d("Name----", "-" + constants.sharedPreferences.getString("userName", "N/A"));
            userName = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            isFirstLogin = intent.getStringExtra("isFirstLogin");
            cancelBtn = (TextView) view.findViewById(R.id.cancelBtn);
            userNameText = (TextView) view.findViewById(R.id.userNameText);
            userNameText.setText(_crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()).toUpperCase());

            confirmBtn = (TextView) view.findViewById(R.id.doneBtn);


            confirmBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Close dialog
                    confirmBtn.setEnabled(false);

                    constants.notiEditor.putString("EnableNoti", "True");
                    constants.notiEditor.commit();
                    constants.notiEditor.apply();
                    AddTrustedDevice("Y");


                }
            });
            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Close dialog
                    cancelBtn.setEnabled(false);
                    constants.notiEditor.putString("EnableNoti", "False");
                    constants.notiEditor.commit();
                    constants.notiEditor.apply();
                    AddTrustedDevice("N");

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return OptionDialog;
    }

    public void AddTrustedDevice(final String flag) {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                final CryptLib _crypt = new CryptLib();
                final String md5 = helper.md5(userName.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", userName);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sDeviceMacAddress", constants.address);
                if (!token.equals("N/A")) {
                    params.put("sFcm_Token", token);
                } else {
                    Log.d("Token", "--" + constants.fcmSharedPref.getString("fcmToken", "N/A"));
                    params.put("sFcm_Token", constants.fcmSharedPref.getString("fcmToken", "N/A"));
                }
//                params.put("sFcm_Token", token);
                params.put("sDeviceType", target);
                params.put("iUserID", "0");
                params.put("sNotificationFlag", flag);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(getActivity());
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.trustedDeviceURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObject = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_RegisterTrustedDeviceResult").getString("Status_Code").equals("00")) {
                                            if (isFirstLogin.equals("Yes")) {
                                                try {
//                                                    constants.editor.putString("userName", _crypt.encryptForParams(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                                                    constants.editor.commit();
//                                                    constants.editor.apply();
                                                    pd.dismiss();
                                                    Intent intent = new Intent(getActivity(), ProfileSetupScreen.class);
                                                    intent.putExtra("isFirst", isFirstLogin);
                                                    getActivity().overridePendingTransition(0, 0);
                                                    cancelBtn.setEnabled(true);
                                                    if (flag.equals("Y")) {
                                                        constants.editor.putString("NotificationEnable", _crypt.encryptForParams("True", constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.editor.commit();
                                                        constants.editor.apply();
                                                    }

                                                    confirmBtn.setEnabled(true);
                                                    startActivity(intent);
                                                    getActivity().finish();
                                                    OptionDialog.cancel();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    cancelBtn.setEnabled(true);
                                                    confirmBtn.setEnabled(true);
                                                }
                                            } else {

                                                pd.dismiss();
                                                Intent intent = new Intent(getActivity(), HomeScreen.class);
                                                try {


//                                                    constants.editor.putString("userName", _crypt.encryptForParams(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
//                                                    constants.editor.commit();
//                                                    constants.editor.apply();
                                                    Log.d("----", "---" + constants.sharedPreferences.getString("userName", "N/A"));
                                                    Log.d("---", "---" + constants.sharedPreferences.getString("T24_Target", "N/A"));
                                                    getActivity().overridePendingTransition(0, 0);
                                                    cancelBtn.setEnabled(true);
                                                    confirmBtn.setEnabled(true);
                                                    if (flag.equals("Y")) {
                                                        constants.editor.putString("NotificationEnable", _crypt.encryptForParams("True", constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                        constants.editor.commit();
                                                        constants.editor.apply();
                                                    }
                                                    startActivity(intent);
                                                    getActivity().finish();
                                                    OptionDialog.cancel();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    cancelBtn.setEnabled(true);
                                                    confirmBtn.setEnabled(true);
                                                }
                                            }
                                        } else if (jsonObject.getJSONObject("Signs_RegisterTrustedDeviceResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_RegisterTrustedDeviceResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());
                                            cancelBtn.setEnabled(true);
                                            confirmBtn.setEnabled(true);
                                            OptionDialog.cancel();

                                        } else {
                                            pd.dismiss();
                                            CheckForFirstTimeLogin();
                                        }

                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        cancelBtn.setEnabled(true);
                                        confirmBtn.setEnabled(true);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    CheckForFirstTimeLogin();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    CheckForFirstTimeLogin();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    CheckForFirstTimeLogin();
                                }
                            }

                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", userName);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceMacAddress", constants.address);
//                        params.put("sFcm_Token", token);
//                        params.put("sDeviceType", target);
//                        params.put("iUserID", "0");
//                        params.put("sNotificationFlag", flag);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_LONG).show();
                cancelBtn.setEnabled(true);
                confirmBtn.setEnabled(true);
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_LONG).show();
            cancelBtn.setEnabled(true);
            pd.dismiss();
            cancelBtn.setEnabled(true);
            confirmBtn.setEnabled(true);
            showDilogForError("Device cannot be added. Please try again later.", "ERROR");

        }
    }

    public void CheckForFirstTimeLogin() {

        if (isFirstLogin.equals("Yes")) {

//            Toast.makeText(getActivity(), "Device cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
            pd.dismiss();
            showDilogForError("Device cannot be added. Please try again later.", "ERROR");
            cancelBtn.setEnabled(true);
            confirmBtn.setEnabled(true);

        } else {

            pd.dismiss();
            showDilogForError("Device cannot be added. Please try again later.", "ERROR");
            cancelBtn.setEnabled(true);
            confirmBtn.setEnabled(true);

        }
    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (error.equals("error")) {
                    dialog.dismiss();
                    pd.dismiss();
                    Intent intent = new Intent(getActivity(), ProfileSetupScreen.class);
                    getActivity().overridePendingTransition(0, 0);
                    startActivity(intent);
                    getActivity().finish();
                    cancelBtn.setEnabled(true);
                    doneBtn.setEnabled(true);
                    OptionDialog.cancel();

                } else {
                    if (isFirstLogin.equals("Yes")) {

                        dialog.dismiss();
                        pd.dismiss();
                        Intent intent = new Intent(getActivity(), ProfileSetupScreen.class);
                        intent.putExtra("isFirst", isFirstLogin);
                        getActivity().overridePendingTransition(0, 0);
                        startActivity(intent);
                        getActivity().finish();
                        cancelBtn.setEnabled(true);
                        doneBtn.setEnabled(true);
                        OptionDialog.cancel();

                    } else {

                        dialog.dismiss();
                        pd.dismiss();
                        Intent intent = new Intent(getActivity(), HomeScreen.class);
                        getActivity().overridePendingTransition(0, 0);
                        cancelBtn.setEnabled(true);
                        doneBtn.setEnabled(true);
                        startActivity(intent);
                        getActivity().finish();
                        OptionDialog.cancel();
                    }
                }


            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();

    }
}