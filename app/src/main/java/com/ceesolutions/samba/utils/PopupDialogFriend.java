package com.ceesolutions.samba.utils;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.friends.ManageFriends;
import com.ceesolutions.samba.views.CircleImageView;
import com.ceesolutions.samba.views.RoundImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/4/18.
 */

public class PopupDialogFriend extends DialogFragment {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private ImageView closeBtn;
    private CircleImageView profile_image;
    private Dialog pd;
    private TextView confirmBtn, title, description, addFriend;
    private TextView cancelBtn, message, doneBtn, heading;
    private String token, target, userName;
    private AlertDialog OptionDialog;
    private String isFirstLogin;
    private Utils utils;
    private Dialog dialog;
    private String error = "";
    private Intent intent;
    private RoundImageView titleImage;
    private TextView titleTxt;
    private int color;
    private AppPreferences appPreferences;
    private String location;

    /**
     * creates a new instance of PropDialogFragment
     */
    public PopupDialogFriend() { /*empty*/ }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //getting proper access to LayoutInflater is the trick. getLayoutInflater is a                   //Function
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.popup_friend, null);
        OptionDialog = new AlertDialog.Builder(getActivity()).create();
        OptionDialog.setView(view);
        profile_image = view.findViewById(R.id.profile_image);
//        popupDialog = new PopupDialog();
        requestQueue = Volley.newRequestQueue(getActivity());
        webService = new WebService();
        helper = Helper.getHelper(getActivity());
        constants = Constants.getConstants(getActivity());
        pd = new Dialog(getActivity());
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        utils = new Utils(getActivity());
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        intent = getActivity().getIntent();
        title = (TextView) view.findViewById(R.id.title);
        titleTxt = (TextView) view.findViewById(R.id.titleTxt);
        titleImage = (RoundImageView) view.findViewById(R.id.titleImage);
        titleTxt.bringToFront();
        description = (TextView) view.findViewById(R.id.description);
        title.setText(intent.getStringExtra("friendName"));
        addFriend = (TextView) view.findViewById(R.id.addFriend);
        addFriend.setText(intent.getStringExtra("friendNumber"));
        String img = intent.getStringExtra("friend_image");
        String s = intent.getStringExtra("friendName").substring(0, 1);
        appPreferences = new AppPreferences(getActivity());
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        switch (s.toUpperCase()) {

            case "A":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "B":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "C":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "D":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "E":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "F":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "G":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "H":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "I":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "J":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "K":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "L":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "M":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "N":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "O":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "P":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "Q":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "R":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "S":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "T":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "U":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "V":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "W":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "X":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "Y":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "Z":
                color = getResources().getColor(R.color.friendColor4);
                break;

            default:
                color = getResources().getColor(R.color.defaultColor);
                break;
        }

        if (!TextUtils.isEmpty(img)) {
            byte[] imageByteArray = Base64.decode(img, Base64.DEFAULT);
            titleTxt.setVisibility(View.INVISIBLE);
            titleImage.setVisibility(View.INVISIBLE);
            RequestOptions options = new RequestOptions();
            options.centerCrop();
            Glide.with(getActivity())
                    .load(imageByteArray)
                    .apply(options)
                    .into(profile_image);

        } else {

            profile_image.setVisibility(View.INVISIBLE);
            titleImage.setBackgroundTintList(ColorStateList.valueOf((color)));
            titleTxt.setText(s);
        }


        cancelBtn = (TextView) view.findViewById(R.id.rejectBtn);
        closeBtn = (ImageView) view.findViewById(R.id.closeBtn);

        confirmBtn = (TextView) view.findViewById(R.id.doneBtn);

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().finish();
            }
        });

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                confirmBtn.setEnabled(false);


                FriendRequest("1");


            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                cancelBtn.setEnabled(false);
                FriendRequest("0");

            }
        });

        return OptionDialog;
    }

    public void FriendRequest(final String flag) {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.ActionFriendRequest(constants.sharedPreferences.getString("LegalID", "N/A"), intent.getStringExtra("userName"), constants.sharedPreferences.getString("MobileNumber", "N/A"), intent.getStringExtra("friendNumber"), intent.getStringExtra("messageId"), flag, intent.getStringExtra("request_id"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getActionFriend();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                cancelBtn.setEnabled(true);
//                                confirmBtn.setEnabled(true);
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
//
//                                        pd.dismiss();
//                                        Intent intent = new Intent(getActivity(), ManageFriends.class);
//                                        intent.putExtra("addFriend","addFriend");
//                                        getActivity().overridePendingTransition(0, 0);
//                                        startActivity(intent);
//                                        getActivity().finish();
//
//                                    } else {
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                        cancelBtn.setEnabled(true);
//                                        confirmBtn.setEnabled(true);
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    cancelBtn.setEnabled(true);
//                                    confirmBtn.setEnabled(true);
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                }
//                            }
//                        } else {
//
//
//                            pd.dismiss();
//                            cancelBtn.setEnabled(true);
//                            confirmBtn.setEnabled(true);
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                        }
//                    }
//                }, 5000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sUser_id", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sUser_number", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sUser_name", intent.getStringExtra("userName"));
                params.put("sFriend_number", intent.getStringExtra("friendNumber"));
                params.put("sRequest_id", intent.getStringExtra("request_id"));
                params.put("sMessage_id", intent.getStringExtra("messageId"));
                params.put("sFriend_username", intent.getStringExtra("friend_username"));
                params.put("sAction", flag);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                params.put("sSigns_Username", user);
                HttpsTrustManager.allowMySSL(getActivity());

                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.actionFriendRequestURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("action_friend_requestResult").getString("Status_Code").equals("00")) {
                                            pd.dismiss();
                                            Intent intent = new Intent(getActivity(), ManageFriends.class);
                                            intent.putExtra("addFriend", "addFriend");
                                            getActivity().overridePendingTransition(0, 0);
                                            startActivity(intent);
                                            getActivity().finish();
                                        } else if (jsonObject.getJSONObject("action_friend_requestResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("action_friend_requestResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());
                                            cancelBtn.setEnabled(true);
                                            confirmBtn.setEnabled(true);

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                            cancelBtn.setEnabled(true);
                                            confirmBtn.setEnabled(true);
                                        }
                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        cancelBtn.setEnabled(true);
                                        confirmBtn.setEnabled(true);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    cancelBtn.setEnabled(true);
                                    confirmBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    cancelBtn.setEnabled(true);
                                    confirmBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    cancelBtn.setEnabled(true);
                                    confirmBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sUser_id", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sUser_number", constants.sharedPreferences.getString("MobileNumber", "N/A"));
//                        params.put("sUser_name", intent.getStringExtra("userName"));
//                        params.put("sFriend_number", intent.getStringExtra("friendNumber"));
//                        params.put("sRequest_id", intent.getStringExtra("request_id"));
//                        params.put("sMessage_id", intent.getStringExtra("messageId"));
//                        params.put("sAction", flag);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_LONG).show();
                cancelBtn.setEnabled(true);
                confirmBtn.setEnabled(true);
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_LONG).show();
            cancelBtn.setEnabled(true);
            pd.dismiss();
            cancelBtn.setEnabled(true);
            confirmBtn.setEnabled(true);
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }

}