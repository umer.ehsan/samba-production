package com.ceesolutions.samba.utils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.RegistrationScreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ayaz on 1/29/18.
 */

public class PopupDialogSecretQuestion extends DialogFragment {

    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private ProgressDialog pd;
    Button confirmBtn;
    Button skipBtn;
    private EditText editQuestionAnswer;
    private PopupPassCode popupDialog;
    String token, target, userName, questionSelected;
    private Spinner questionSpinner;
    private String[] quesitons;
    private String legalID;
    ArrayList<String> questionList = new ArrayList<String>();
    private String questionAnswer;
    private TextView errorMessage;
    private String questionCode;
    AlertDialog OptionDialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //getting proper access to LayoutInflater is the trick. getLayoutInflater is a                   //Function
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.pop_up_secret_question, null);
        OptionDialog = new AlertDialog.Builder(getActivity()).create();
        OptionDialog.setView(view);

        popupDialog = new PopupPassCode();
        requestQueue = Volley.newRequestQueue(getActivity());
        webService = new WebService();
        helper = Helper.getHelper(getActivity());
        constants = Constants.getConstants(getActivity());
        pd = new ProgressDialog(getActivity());
        token = constants.sharedPreferences.getString("fcmToken", "fcmToken");
        target = constants.sharedPreferences.getString("T24_Target", "N/A");


        userName = constants.sharedPreferences.getString("userName", "N/A");
        pd = new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.setTitle("Get Secret Questions List");
        pd.show();
        legalID = constants.sharedPreferences.getString("LegalID", "N/A");
        errorMessage = (TextView) view.findViewById(R.id.errorMessage);
        skipBtn = (Button) view.findViewById(R.id.skipBtn);
        confirmBtn = (Button) view.findViewById(R.id.confirmBtn);
        editQuestionAnswer = (EditText) view.findViewById(R.id.editQuestionAnswer);

        editQuestionAnswer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                questionAnswer = editable.toString().trim();

            }
        });

        questionAnswer = editQuestionAnswer.getText().toString().trim();

        questionSpinner = (Spinner) view.findViewById(R.id.questionSpinner);
        GetSecretQuestions();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                pd.dismiss();
                spinnerAdapter questionAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview);
                questionAdapter.addAll(quesitons);
                questionAdapter.add("Select Question");
                questionSpinner.setAdapter(questionAdapter);
                questionSpinner.setSelection(questionAdapter.getCount());
                questionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {

//                            ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
//                            ((TextView) parent.getChildAt(0)).setTextSize(20);

                            // Get select item
                            if (questionSpinner.getSelectedItem() == "Select Question") {
                                questionSelected = questionSpinner.getSelectedItem().toString();
                                Log.d("range", "---" + questionSpinner);
                                //Do nothing.
                            } else {

                                questionSelected = questionSpinner.getSelectedItem().toString();
//                            checkForBrand(brandSpinner.getSelectedItem().toString(), CustomerScreen.this);
//                    CheckRange(brandSpinner.getSelectedItem().toString());
//                    rangeStr = brandSpinner.getSelectedItem().toString() + ",";

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });

            }

        }, 15000);


//        dialog.setCancelable(false);// if decline button is clicked, close the custom dialog
        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog

                popupDialog.setCancelable(false);
                popupDialog.show(getFragmentManager(), "dialog");
                OptionDialog.cancel();
            }
        });
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog

                if (questionSelected.equals("Select Question")) {

                    Toast.makeText(getActivity(), "Please select any one question", Toast.LENGTH_SHORT).show();


                } else {

                    if (TextUtils.isEmpty(questionAnswer)) {
                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("Question Answer can not be empty");

                    } else {
                        confirmBtn.setEnabled(false);

                        if (questionSelected.equals("What is your favourite car?")) {

                            questionCode = "SQ001";

                        } else if (questionSelected.equals("Who is your childhood best freind?")) {

                            questionCode = "SQ002";

                        } else if (questionSelected.equals("What was the name of your first pet?")) {

                            questionCode = "SQ003";
                        }

                        AddSecretQuestionAnswer();
                        pd.show();

                    }
                }

//                AddSecretQuestionAnswer();
//                OptionDialog.cancel();


            }
        });

        return OptionDialog;
    }

    public void GetSecretQuestions() {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                webService.GetSecretQuestions(userName, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                        JSONObject jsonObject = webService.getGetQuestionsReqResponse();
                        Log.d("Response", "Object----" + jsonObject);

                        if (jsonObject != null) {

                            if (jsonObject.has("Error")) {

                                Toast.makeText(getActivity(), "Information is not valid!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(), RegistrationScreen.class);
                                pd.dismiss();
                                getActivity().overridePendingTransition(0, 0);
                                startActivity(intent);
                                skipBtn.setEnabled(true);


                            } else {

                                try {


                                    JSONArray jsonArray = new JSONArray(jsonObject.getString("SecretQuestionList"));
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        try {


                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                            questionList.add(jsonObj.getString("Question_Description"));


//                                    if (jsonObject.get("Status_Code").equals("00")) {
//
////                                        Toast.makeText(getActivity(), "Device added successfully !", Toast.LENGTH_SHORT).show();
////                                        Intent intent = new Intent(getActivity(), UserValidationScreen.class);

////                                        getActivity().overridePendingTransition(0, 0);
////                                        intent.putExtra("Type", "Samba");
////                                        startActivity(intent);
////                                        skipBtn.setEnabled(true);
//                                        popupDialog.setCancelable(false);
//                                        popupDialog.show(getFragmentManager(), "dialog");
//}catch (Exception ex){
                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }

                                    }

                                    quesitons = new String[questionList.size()];
                                    quesitons = questionList.toArray(quesitons);

//                                    for(int i = 0; i < quesitons.length ; i++){
//                                        Log.d("string is",(String)quesitons[i]);
//                                    }

//                                    } else {
//                                        Toast.makeText(getActivity(), "Server is down. Please try again later!", Toast.LENGTH_SHORT).show();
//                                        Intent intent = new Intent(getActivity(), RegistrationScreen.class);
//                                        pd.dismiss();
//                                        getActivity().overridePendingTransition(0, 0);
//                                        startActivity(intent);
//                                        skipBtn.setEnabled(true);
//
//                                    }


                                } catch (JSONException ex) {

                                    ex.printStackTrace();
                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), "Device cannot be added. Please try again later!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), RegistrationScreen.class);
                            pd.dismiss();
                            getActivity().overridePendingTransition(0, 0);
                            startActivity(intent);
                            skipBtn.setEnabled(true);

                        }
                    }
                }, 8000);
            } else {
                Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                skipBtn.setEnabled(true);
                pd.dismiss();


            }
        } catch (Exception e) {

            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
            skipBtn.setEnabled(true);
            pd.dismiss();

        }
    }

    public void AddSecretQuestionAnswer() {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                webService.AddSecretQuestionAnswer(userName, questionCode, questionAnswer, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                        JSONObject jsonObject = webService.getSetQuestionAnswerReqResponse();
                        Log.d("Response", "Object----" + jsonObject);

                        if (jsonObject != null) {

                            if (jsonObject.has("Error")) {

                                Toast.makeText(getActivity(), "Information is not valid!", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(), RegistrationScreen.class);
                                pd.dismiss();
                                getActivity().overridePendingTransition(0, 0);
                                startActivity(intent);
                                skipBtn.setEnabled(true);


                            } else {

                                try {


                                    if (jsonObject.get("Status_Code").equals("00")) {
                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_SHORT).show();
                                        pd.dismiss();
                                        popupDialog.setCancelable(false);
                                        popupDialog.show(getFragmentManager(), "dialog");
                                        OptionDialog.cancel();

                                    } else {
                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getActivity(), RegistrationScreen.class);
                                        pd.dismiss();
                                        getActivity().overridePendingTransition(0, 0);
                                        startActivity(intent);
                                        skipBtn.setEnabled(true);

                                    }


                                } catch (JSONException ex) {

                                    ex.printStackTrace();
                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), "Device cannot be added. Please try again later!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), RegistrationScreen.class);
                            pd.dismiss();
                            getActivity().overridePendingTransition(0, 0);
                            startActivity(intent);
                            skipBtn.setEnabled(true);

                        }
                    }
                }, 5000);
            } else {
                Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                skipBtn.setEnabled(true);
                pd.dismiss();


            }
        } catch (Exception e) {

            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
            skipBtn.setEnabled(true);
            pd.dismiss();

        }
    }
}