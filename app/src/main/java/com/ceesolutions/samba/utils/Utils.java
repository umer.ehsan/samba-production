package com.ceesolutions.samba.utils;

/**
 * Created by ayaz on 12/19/17.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.transactions.FundTransferScreen;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.regex.Pattern;
//import org.apache.http.conn.util.InetAddressUtils;

public class Utils {

    public static Pattern passwordPattern;
    public final String EXCLUDE_PASSWORD_PATTERN =
            "([&+,~'<>^()%\"])";
    public final String EXCLUDE_PASSWORD_PATTERN_SPACE =
            "([&+,~'<>^()\\s%\"])";
    TextView textView, doneBtn, heading;
    Context context;
    /**
     * Convert byte array to hex string
     *
     * @param bytes
     * @return
     */
    private Dialog dialog, newDialog;

    public Utils() {

    }

    public Utils(Context mContext) {

        context = mContext;
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuilder sbuf = new StringBuilder();
        for (int idx = 0; idx < bytes.length; idx++) {
            int intVal = bytes[idx] & 0xff;
            if (intVal < 0x10) sbuf.append("0");
            sbuf.append(Integer.toHexString(intVal).toUpperCase());
        }
        return sbuf.toString();
    }

    /**
     * Get utf8 byte array.
     *
     * @param str
     * @return array of NULL if error was found
     */
    public static byte[] getUTF8Bytes(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Load UTF8withBOM or any ansi text file.
     *
     * @param filename
     * @return
     * @throws IOException
     */
    public static String loadFileAsString(String filename) throws IOException {
        final int BUFLEN = 1024;
        BufferedInputStream is = new BufferedInputStream(new FileInputStream(filename), BUFLEN);
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFLEN);
            byte[] bytes = new byte[BUFLEN];
            boolean isUTF8 = false;
            int read, count = 0;
            while ((read = is.read(bytes)) != -1) {
                if (count == 0 && bytes[0] == (byte) 0xEF && bytes[1] == (byte) 0xBB && bytes[2] == (byte) 0xBF) {
                    isUTF8 = true;
                    baos.write(bytes, 3, read - 3); // drop UTF8 bom marker
                } else {
                    baos.write(bytes, 0, read);
                }
                count += read;
            }
            return isUTF8 ? new String(baos.toByteArray(), "UTF-8") : new String(baos.toByteArray());
        } finally {
            try {
                is.close();
            } catch (Exception ex) {
            }
        }
    }

    /**
     * Returns MAC address of the given interface name.
     *
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return mac address or empty string
     */
    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
        /*try {
            // this is so Linux hack
            return loadFileAsString("/sys/class/net/" +interfaceName + "/address").toUpperCase().trim();
        } catch (IOException ex) {
            return null;
        }*/
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @param // true=return ipv4, false=return ipv6
     * @return address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    public void showDilogForAmount(String msg, String header) {

        try {

            // new log
            Bundle bundle = new Bundle();
            bundle.putString("Type", header);
            bundle.putString("Message", msg);
            FirebaseAnalytics.getInstance(context).logEvent("Error",bundle);

            newDialog = new Dialog(context);
            newDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            newDialog.setContentView(R.layout.error_dialog);
            textView = (TextView) newDialog.findViewById(R.id.validatingTxt);
            doneBtn = (TextView) newDialog.findViewById(R.id.doneBtn);
            heading = (TextView) newDialog.findViewById(R.id.heading);
            newDialog.setCancelable(false);
            newDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    newDialog.dismiss();

                }
            });
            textView.setText(msg);
            heading.setText(header);
            newDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showDilogForError(String msg, String header) {

        try {

            // new log
            Bundle bundle = new Bundle();
            bundle.putString("Type", header);
            bundle.putString("Message", msg);
            FirebaseAnalytics.getInstance(context).logEvent("Error",bundle);

            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.error_dialog);
            textView = (TextView) dialog.findViewById(R.id.validatingTxt);
            doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
            heading = (TextView) dialog.findViewById(R.id.heading);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();

                }
            });
            textView.setText(msg);
            heading.setText(header);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showDilogForQR(String msg, String header, final Context finalContext) {

        try {

            // new log
            Bundle bundle = new Bundle();
            bundle.putString("Type", header);
            bundle.putString("Message", msg);
            FirebaseAnalytics.getInstance(context).logEvent("Error",bundle);


            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.error_dialog);
            textView = (TextView) dialog.findViewById(R.id.validatingTxt);
            doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
            heading = (TextView) dialog.findViewById(R.id.heading);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    Intent intent1 = new Intent(finalContext, HomeScreen.class);
                    ((Activity) context).overridePendingTransition(0, 0);
                    ((Activity) context).startActivity(intent1);
                    ((Activity) context).finish();
                    ((Activity) context).finishAffinity();

                }
            });
            textView.setText(msg);
            heading.setText(header);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void showDilogForTransaction(String msg, String header, final Context finalContext) {

        try {

            // new log
            Bundle bundle = new Bundle();
            bundle.putString("Type", header);
            bundle.putString("Message", msg);
            FirebaseAnalytics.getInstance(context).logEvent("Error",bundle);


            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.error_dialog);
            textView = (TextView) dialog.findViewById(R.id.validatingTxt);
            doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
            heading = (TextView) dialog.findViewById(R.id.heading);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    Intent intent1 = new Intent(finalContext, FundTransferScreen.class);
                    intent1.putExtra("addBene", "addBene");
                    ((Activity) context).overridePendingTransition(0, 0);
                    ((Activity) context).startActivity(intent1);
                    ((Activity) context).finish();
                    ((Activity) context).finishAffinity();

                }
            });
            textView.setText(msg);
            heading.setText(header);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showDilogForBillPayment(String msg, String header, final Context finalContext) {

        try {

            // new log
            Bundle bundle = new Bundle();
            bundle.putString("Type", header);
            bundle.putString("Message", msg);
            FirebaseAnalytics.getInstance(context).logEvent("Error",bundle);


            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.error_dialog);
            textView = (TextView) dialog.findViewById(R.id.validatingTxt);
            doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
            heading = (TextView) dialog.findViewById(R.id.heading);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    Intent intent1 = new Intent(finalContext, HomeScreen.class);
                    ((Activity) context).overridePendingTransition(0, 0);
                    ((Activity) context).startActivity(intent1);
                    ((Activity) context).finish();
                    ((Activity) context).finishAffinity();

                }
            });
            textView.setText(msg);
            heading.setText(header);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void showDilogForInvalidSession(String msg, String header) {

        try {

            // new log
            Bundle bundle = new Bundle();
            bundle.putString("Type", header);
            bundle.putString("Message", msg);
            FirebaseAnalytics.getInstance(context).logEvent("Error",bundle);


            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.error_dialog);
            textView = (TextView) dialog.findViewById(R.id.validatingTxt);
            doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
            heading = (TextView) dialog.findViewById(R.id.heading);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();

                }
            });
            textView.setText(msg);
            heading.setText(header);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDilogForErrorNew(String msg, String header) {

        // new log
        Bundle bundle = new Bundle();
        bundle.putString("Type", header);
        bundle.putString("Message", msg);
        FirebaseAnalytics.getInstance(context).logEvent("Error",bundle);


        newDialog = new Dialog(context);
        newDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        newDialog.setContentView(R.layout.error_dialog);
        textView = (TextView) newDialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) newDialog.findViewById(R.id.doneBtn);
        heading = (TextView) newDialog.findViewById(R.id.heading);
        newDialog.setCancelable(false);
        newDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                newDialog.dismiss();

            }
        });
        textView.setText(msg);
        heading.setText(header);
        newDialog.show();

    }

    public void showDilogForLogout(String msg, String header, Context className, final AppCompatActivity toActivity) {



        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Constants constants = Constants.getConstants(context);
                constants.editor.clear();
                constants.editor.commit();
                constants.editor.apply();
                constants.nonSambaCredentialsEditor.clear();
                constants.nonSambaCredentialsEditor.commit();
                constants.nonSambaCredentialsEditor.apply();
                constants.destinationAccountEditor.clear().commit();
                constants.destinationAccountEditor.apply();
                constants.sourceAccountEditor.clear().commit();
                constants.sourceAccountEditor.apply();
                constants.accountListEditor.clear().commit();
                constants.accountListEditor.apply();
                constants.accountsEditor.clear().commit();
                constants.accountsEditor.apply();
                constants.forgotPasswordEditor.clear().commit();
                constants.forgotPasswordEditor.apply();
                constants.purposeBillList.clear().commit();
                constants.purposeBillList.apply();
                constants.purposePrepaidList.clear().commit();
                constants.purposePrepaidList.apply();
                constants.purposeListEditor.clear().commit();
                constants.purposeListEditor.apply();
                constants.reasonListEditor.clear().commit();
                constants.reasonListEditor.apply();
                constants.statusListEditor.clear().commit();
                constants.statusListEditor.apply();
                constants.channelListEditor.clear().commit();
                constants.channelListEditor.apply();
                Intent intent = new Intent(context, toActivity.getClass());
                ((Activity) context).overridePendingTransition(0, 0);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                ((Activity) context).startActivity(intent);
                ((Activity) context).finish();
                ((Activity) context).finishAffinity();

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


    public String getMaskedString(String accountNumber) {
        int length = accountNumber.length() - 6;
        String firstTwoChars = accountNumber.substring(0, 2);
        String lastFourChars = accountNumber.substring(accountNumber.length() - 4, accountNumber.length());
        StringBuilder sb = new StringBuilder(length);
        sb.append(firstTwoChars);
        for (int i = 0; i < length; i++) {
            sb.append("X");
        }
        sb.append(lastFourChars);
        return sb.toString();
    }


    public String getMaskedStringForOther(String accountNumber) {
        int length = accountNumber.length();
        String firstTwoChars = accountNumber.substring(0, 2);
        String lastFourChars = accountNumber.substring(accountNumber.length() - 4, accountNumber.length());
        StringBuilder sb = new StringBuilder(length);
        sb.append(firstTwoChars);
        for (int i = 0; i < length; i++) {
            sb.append("X");
        }
        sb.append(lastFourChars);
        return sb.toString();
    }

    public String getMaskedStringForIBAN(String accountNumber) {
        int length = accountNumber.length();
        String firstTwoChars = accountNumber.substring(0, 4);
        String lastFourChars = accountNumber.substring(accountNumber.length() - 4, accountNumber.length());
        StringBuilder sb = new StringBuilder(length);
        sb.append(firstTwoChars);
        for (int i = 0; i < length; i++) {
            sb.append("X");
        }
        sb.append(lastFourChars);
        return sb.toString();
    }

    public String createKey(String legalId, String sessionId) {

        String initalDigits = legalId.substring(0, 10);
        String middleDigits = sessionId.substring(0, 12);
        String endDigits = legalId.substring(3);

        String key = initalDigits + middleDigits + endDigits;

        return key.toUpperCase();
    }

    public String createIV(String legalId, String sessionId) {


        String intialKeyDigits = sessionId.substring(0, 4);
        String middleKeyDigits = legalId.substring(0, 10);
        String endKeyDigits = sessionId.substring(sessionId.length() - 2);

        String iv = intialKeyDigits + middleKeyDigits + endKeyDigits;

        return iv.toUpperCase();

    }

}