package com.ceesolutions.samba.utils;

/**
 * Created by ayaz on 12/18/17.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * Created by aali on 4/9/2017.
 */

public class Helper {

    public static boolean flag = false;
    public static Pattern passwordPattern;
    static Helper helper;
    public final String EXCLUDE_PASSWORD_PATTERN =
            "([&+,~'<>^()%\"])";
    public final String EXCLUDE_PASSWORD_PATTERN_SPACE =
            "([&+,~'<>^()\\s%\"])";
    Context context;
    WebService webService;
    RequestQueue requestQueue;
    TextView textView, doneBtn, heading;
    private Dialog dialog;

    private Helper(Context ctx) {

        this.context = ctx;

    }

    public static Helper getHelper(Context mContext) {

        if (helper == null) {

            helper = new Helper(mContext);

        }

        return helper;
    }

    public static final String md5(final String password) {
        try {

            MessageDigest digest = java.security.MessageDigest
                    .getInstance("MD5");
            digest.update(password.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static long generateRandom(int length) {
        Random random = new Random();
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < length; i++) {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        return Long.parseLong(new String(digits));
    }

    public static Bitmap getScaledBitmap(Bitmap bm, int bmOriginalWidth, int bmOriginalHeight, double originalWidthToHeightRatio, double originalHeightToWidthRatio, int maxHeight, int maxWidth) {
        if (bmOriginalWidth > maxWidth || bmOriginalHeight > maxHeight) {
//            Log.v(TAG, format("RESIZING bitmap FROM %sx%s ", bmOriginalWidth, bmOriginalHeight));

            if (bmOriginalWidth > bmOriginalHeight) {
                bm = scaleDeminsFromWidth(bm, maxWidth, bmOriginalHeight, originalHeightToWidthRatio);
            } else {
                bm = scaleDeminsFromHeight(bm, maxHeight, bmOriginalHeight, originalWidthToHeightRatio);
            }

//            Log.v("", format("RESIZED bitmap TO %sx%s ", bm.getWidth(), bm.getHeight()));
        }
        return bm;
    }

    private static Bitmap scaleDeminsFromHeight(Bitmap bm, int maxHeight, int bmOriginalHeight, double originalWidthToHeightRatio) {
        int newHeight = (int) Math.min(maxHeight, bmOriginalHeight * .55);
        int newWidth = (int) (newHeight * originalWidthToHeightRatio);
        bm = Bitmap.createScaledBitmap(bm, newWidth, newHeight, true);
        return bm;
    }

    private static Bitmap scaleDeminsFromWidth(Bitmap bm, int maxWidth, int bmOriginalWidth, double originalHeightToWidthRatio) {
        //scale the width
        int newWidth = (int) Math.min(maxWidth, bmOriginalWidth * .75);
        int newHeight = (int) (newWidth * originalHeightToWidthRatio);
        bm = Bitmap.createScaledBitmap(bm, newWidth, newHeight, true);
        return bm;
    }

    public static String convertMapToString(Map<String, String> map) {

        StringBuilder stringBuilder = new StringBuilder();
//        Iterator iter = map.keySet().iterator();
        int count = 0;
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey().toString();
            String value = map.get(key);
            if (count > 0) {
                stringBuilder.append("&" + key + "=" + value);
            } else {
                count++;
                stringBuilder.append(key + "=" + value);
            }

        }
        return stringBuilder.toString();
    }

    public void showDilog(String msg) {
        new AlertDialog.Builder(this.context)
                .setTitle("Alert")
                .setMessage(msg)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })

                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void writeToFile(String data, Context context) {
        try {
            flag = true;
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("test.txt", Context.MODE_APPEND));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public void readFromFile(Context context) {

        String ret = "";
        if (!flag)
            return;
        try {
            InputStream inputStream = context.openFileInput("test.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                String nextLine = "";
                String[] split;
                StringBuilder stringBuilder = new StringBuilder();
                webService = new WebService();
                requestQueue = Volley.newRequestQueue(context);
                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                    nextLine = receiveString;
                    split = receiveString.split(", ");
                    Log.d("NextLine", "--->" + nextLine);
//                    webService.Conversion(split[0],split[1],split[2],split[3],split[4],requestQueue);

                }

                inputStream.close();
                ret = stringBuilder.toString();
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("test.txt", Context.MODE_PRIVATE));
                outputStreamWriter.close();
                flag = false;
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }


    }

    public boolean validateInputForSC(String str) {
        boolean isValid = true;
        passwordPattern = Pattern.compile(EXCLUDE_PASSWORD_PATTERN);
        if (passwordPattern.matcher(str).find()) {

            //handle your action here toast message/ snackbar or something else
            return false;
        } else return isValid;
    }

    public boolean validateInputForSCWithSpace(String str) {
        boolean isValid = true;
        passwordPattern = Pattern.compile(EXCLUDE_PASSWORD_PATTERN);
        if (passwordPattern.matcher(str).find()) {

            //handle your action here toast message/ snackbar or something else
            return false;
        } else return isValid;
    }

    public int nDigitRandomNo(int digits) {
        int max = (int) Math.pow(10, (digits)) - 1; //for digits =7, max will be 9999999
        int min = (int) Math.pow(10, digits - 1); //for digits = 7, min will be Constants.requestTimeout0
        int range = max - min; //This is 8999999
        Random r = new Random();
        int x = r.nextInt(range);// This will generate random integers in range 0 - 8999999
        int nDigitRandomNo = x + min; //Our random rumber will be any random number x + min
        return nDigitRandomNo;
    }

    public void galleryAddPic(String filename) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(filename);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }


}
