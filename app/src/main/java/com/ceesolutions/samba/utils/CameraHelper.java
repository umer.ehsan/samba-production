package com.ceesolutions.samba.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by ayaz on 6/3/17.
 */

public class CameraHelper extends SurfaceView implements
        SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera mCamera;
    private Context mContext;
    private final static String DEBUG_TAG = "CheckInScreen";
    private int mCameraId = 0;
    private Camera mCam;
    private int rotate;

    public CameraHelper(Context context, Camera camera, int rotation) {
        super(context);
        mCamera = camera;
        mHolder = getHolder();
        mContext = context;
        rotate = rotation;
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (Exception error) {
            Log.d(DEBUG_TAG,
                    "Error starting mPreviewLayout: " + error.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w,
                               int h) {
        if (mHolder.getSurface() == null) {
            return;
        }

        // can't make changes while mPreviewLayout is active
        try {
            mCamera.stopPreview();
        } catch (Exception e) {

        }

        try {
            // set rotation to match device orientation
            setCameraDisplayOrientationAndSize();

            // start up the mPreviewLayout
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception error) {
            Log.d(DEBUG_TAG,
                    "Error starting mPreviewLayout: " + error.getMessage());
        }
    }

    public void setCameraDisplayOrientationAndSize() {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(mCameraId, info);
        int rotation = rotate;
        int degrees = rotation * 90;

            /*
             * // the above is just a shorter way of doing this, but could break
             * if the values change switch (rotation) { case Surface.ROTATION_0:
             * degrees = 0; break; case Surface.ROTATION_90: degrees = 90;
             * break; case Surface.ROTATION_180: degrees = 180; break; case
             * Surface.ROTATION_270: degrees = 270; break; }
             */

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;
        } else {
            result = (info.orientation - degrees + 360) % 360;
        }
        mCamera.setDisplayOrientation(result);

        Camera.Size previewSize = mCam.getParameters().getPreviewSize();
        if (result == 90 || result == 270) {
            // swap - the physical camera itself doesn't rotate in relation
            // to the screen ;)
            mHolder.setFixedSize(previewSize.height, previewSize.width);
        } else {
            mHolder.setFixedSize(previewSize.width, previewSize.height);

        }
    }


    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

}