package com.ceesolutions.samba.utils;

import android.content.Context;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessagingService;

import java.io.IOException;

/**
 * Created by Belal on 5/27/2016.
 */


//Class extending FirebaseInstanceIdService
public class MyFirebaseInstanceIDService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseIIDService";


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("newToken", s);

        String refreshedToken = s;

        //Displaying token on logcat
        Log.d("Hello", "Refreshed token: " + refreshedToken);

        ConfigAPI.fcmToken = refreshedToken;

        Constants constants = Constants.getConstants(getApplicationContext());
        constants.fcmEditor.putString("fcmToken", refreshedToken);
        constants.fcmEditor.commit();
        constants.fcmEditor.apply();

        String str =  constants.fcmSharedPref.getString("fcmToken","fcmToken");
    }

//    @Override
//    public void onTokenRefresh() {
//
//        //Getting registration token
//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//
//        //Displaying token on logcat
//        Log.d("Hello", "Refreshed token: " + refreshedToken);
//
//        ConfigAPI.fcmToken = refreshedToken;
//
//        Constants constants = Constants.getConstants(getApplicationContext());
//        constants.fcmEditor.putString("fcmToken", refreshedToken);
//        constants.fcmEditor.commit();
//        constants.fcmEditor.apply();
//
//       String str =  constants.fcmSharedPref.getString("fcmToken","fcmToken");
//    }

    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project
    }
}
