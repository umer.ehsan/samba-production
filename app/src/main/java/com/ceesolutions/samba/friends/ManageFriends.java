package com.ceesolutions.samba.friends;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import com.ceesolutions.samba.eatMubarak.EatMubarakMainScreen;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.Bookme.BookMeMainScreen;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.beneficiariesManagement.ManageBeneficiaresScreen;
import com.ceesolutions.samba.billPayments.PayBillsScreen;
import com.ceesolutions.samba.cardManagement.ManageCardsScreen;
import com.ceesolutions.samba.friends.Adapter.CustomHorizontalListAdapter;
import com.ceesolutions.samba.friends.Adapter.CustomListFriendsAdapter;
import com.ceesolutions.samba.friends.Model.Friend;
import com.ceesolutions.samba.masterCardIntegration.MainActivity;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.transactions.AccountsScreen;
import com.ceesolutions.samba.transactions.FundTransferScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.TapTarget;
import com.ceesolutions.samba.utils.TapTargetView;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.HorizontalListView;
import com.daimajia.swipe.util.Attributes;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.OnBoomListenerAdapter;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/1/18.
 */

public class ManageFriends extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private ListView listView;
    private HorizontalListView horizontalListView;
    private CustomListFriendsAdapter customListBeneAdapter;
    private CustomHorizontalListAdapter customHorizontalListAdapter;
    private ArrayList<Friend> friendModels;
    private ArrayList<Friend> favouritesModels;
    private FloatingActionButton floatingAction;
    private Dialog pd, dialog;
    private WebService webService;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Utils utils;
    private String type, backType;
    private ImageView backBtn, notificationBtn, sambaBtn, shareBtn, helpBtn;
    private String menu, userType;
    private BoomMenuButton bmb;
    private EditText chatBotText;
    private TextView heading, textView, btn;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_friends);
        initViews();
        getSupportActionBar().hide();
        try {

            CryptLib _crypt = new CryptLib();
            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }
            menu = constants.sharedPreferences.getString("Menus", "N/A");
            if (!TextUtils.isEmpty(menu) && !menu.equals("N/A") && !menu.equals("")) {


            } else {

                menu = "N/A";
            }
            floatingAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (userType.equals("NON_SAMBA")) {
                        FirebaseAnalytics.getInstance(ManageFriends.this).logEvent("Non_Samba_Add_Friend_Pressed", new Bundle());

                    } else {
                        FirebaseAnalytics.getInstance(ManageFriends.this).logEvent("Add_Friend_Pressed", new Bundle());

                    }

                    Intent intent = new Intent(ManageFriends.this, ImportFromAddressBook.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
//                finish();

                }
            });

            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!TextUtils.isEmpty(type) && !type.equals(null) && !type.equals("null")) {
                        finish();

                    } else if (!TextUtils.isEmpty(backType) && !backType.equals(null) && !backType.equals("null")) {
                        Intent intent = new Intent(ManageFriends.this, HomeScreen.class);
                        overridePendingTransition(0, 0);
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(ManageFriends.this, HomeScreen.class);
                        overridePendingTransition(0, 0);
                        startActivity(intent);
                        finish();
                    }

                }
            });

            notificationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(ManageFriends.this, MessageCenter.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
//                finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initViews() {

        listView = (ListView) findViewById(R.id.listView);
        horizontalListView = (HorizontalListView) findViewById(R.id.horizontalListView);
        floatingAction = (FloatingActionButton) findViewById(R.id.floatingAction);
        friendModels = new ArrayList<>();
        favouritesModels = new ArrayList<>();
        backBtn = (ImageView) findViewById(R.id.backBtn);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        helpBtn = (ImageView) findViewById(R.id.helpBtn);
        Intent intent = getIntent();
        type = intent.getStringExtra("source");
        backType = intent.getStringExtra("addFriend");
        sambaBtn = (ImageView) findViewById(R.id.sambaBtn);
        shareBtn = (ImageView) findViewById(R.id.shareBtn);
        chatBotText = (EditText) findViewById(R.id.chatBoxText);
        if (!TextUtils.isEmpty(type) && !type.equals(null) && !type.equals("null")) {


        } else if (!TextUtils.isEmpty(backType) && !backType.equals(null) && !backType.equals("null")) {

        } else
            type = "notDestination";


        bmb = (BoomMenuButton) findViewById(R.id.bmb);
        assert bmb != null;
        bmb.setButtonEnum(ButtonEnum.TextOutsideCircle);
        bmb.setBackgroundColor(Color.TRANSPARENT);
        bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_9_1);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_9_1);


        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {


                    Intent intent = new Intent("android.intent.action.VIEW");

                    /** creates an sms uri */
                    Uri data = Uri.parse("sms:");

                    /** Setting sms uri to the intent */
                    intent.setData(data);
                    intent.putExtra("sms_body", "Download Samba Mobile App and register yourself. Click https://bit.ly/2KB0nfG for details");

                    /** Initiates the SMS compose screen, because the activity contain ACTION_VIEW and sms uri */
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        chatBotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDilogForErrorForLogin(getResources().getString(R.string.message), "WARNING");
                Intent i = new Intent(ManageFriends.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(i);
                finish();
                finishAffinity();
            }
        });
        horizontalListView = (HorizontalListView) findViewById(R.id.horizontalListView);
        floatingAction = (FloatingActionButton) findViewById(R.id.floatingAction);
        friendModels = new ArrayList<>();
        favouritesModels = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(ManageFriends.this);
        webService = new WebService();
        helper = Helper.getHelper(ManageFriends.this);
        constants = Constants.getConstants(ManageFriends.this);
        pd = new Dialog(ManageFriends.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        utils = new Utils(ManageFriends.this);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                GetFriendsList();

            }
        }, 500);
        appPreferences = new AppPreferences(ManageFriends.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

        String coachMarks = constants.coachMarks.getString("manageFriends", "N/A");

//        if (!TextUtils.isEmpty(coachMarks) && coachMarks.equals("N/A")) {
//            constants.coachMarksEditor.putString("manageFriends", "1");
//            constants.coachMarksEditor.commit();
//            constants.coachMarksEditor.apply();
//            int mainTxt = getResources().getDimensionPixelSize(R.dimen._7sdp);
//            int des = getResources().getDimensionPixelSize(R.dimen._4sdp);
//            TapTargetView.showFor(ManageFriends.this,                 // `this` is an Activity
//                    TapTarget.forView(findViewById(R.id.helpBtn), "", "- Use plus (+) button to add a new friend. You can send friend request to any Pakistan’s mobile number through your device’s local contact list.\n" +
//                            "\n" +
//                            "- Also, you can accept or reject someone’s friend’s request.\n" +
//                            "\n" +
//                            "- Once you are friends with someone, you can request money from them request to pay bill\n" +
//                            "\n" +
//                            "- Swipe your registered friend in the list on left to explore more options")
//                            // All options below are optional
//                            .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
//                            .outerCircleAlpha(0.90f)
//                            // Specify the alpha amount for the outer circle
//                            // Specify a color for the target circle
//                            .titleTextSize(mainTxt)// Specify the size (in sp) of the title text
//                            .titleTextColor(R.color.white)      // Specify the color of the title text
//                            .descriptionTextSize(des)            // Specify the size (in sp) of the description text
//                            .descriptionTextColor(R.color.white)  // Specify the color of the description text
//                            .textColor(R.color.white)            // Specify a color for both the title and description text
//                            .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
//                            // If set, will dim behind the view with 30% opacity of the given color
//                            .drawShadow(true)                   // Whether to draw a drop shadow or not
//                            .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
//                            .tintTarget(true)                   // Whether to tint the target view's color
//                            .transparentTarget(false),           // Specify whether the target is transparent (displays the content underneath)
//                    // Specify a custom drawable to draw as the target
//                    // Specify the target radius (in dp)
//                    new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
//                        @Override
//                        public void onTargetClick(TapTargetView view) {
//                            super.onTargetClick(view);      // This call is optional
////                        doSomething();
//                        }
//                    });
//        }

        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int mainTxt = getResources().getDimensionPixelSize(R.dimen._7sdp);
                int des = getResources().getDimensionPixelSize(R.dimen._4sdp);

                TapTargetView.showFor(ManageFriends.this,                 // `this` is an Activity
                        TapTarget.forView(findViewById(R.id.helpBtn), "About this Screen!!!", "- Use plus (+) button to add a new friend. You can send friend request to any Pakistan’s mobile number through your device’s local contact list.\n" +
                                "\n" +
                                "- Also, you can accept or reject someone’s friend’s request.\n" +
                                "\n" +
                                "- Once you are friends with someone, you can request money from them request to pay bill\n" +
                                "\n" +
                                "- Swipe your registered friend in the list on left to explore more options")
                                // All options below are optional
                                .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                .outerCircleAlpha(0.90f)
                                // Specify the alpha amount for the outer circle
                                // Specify a color for the target circle
                                .titleTextSize(mainTxt)// Specify the size (in sp) of the title text
                                .titleTextColor(R.color.white)      // Specify the color of the title text
                                .descriptionTextSize(des)            // Specify the size (in sp) of the description text
                                .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                .textColor(R.color.white)            // Specify a color for both the title and description text
                                .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                                // If set, will dim behind the view with 30% opacity of the given color
                                .drawShadow(true)                   // Whether to draw a drop shadow or not
                                .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                .tintTarget(true)                   // Whether to tint the target view's color
                                .transparentTarget(false),           // Specify whether the target is transparent (displays the content underneath)
                        // Specify a custom drawable to draw as the target
                        // Specify the target radius (in dp)
                        new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                            @Override
                            public void onTargetClick(TapTargetView view) {
                                super.onTargetClick(view);      // This call is optional
//                        doSomething();

                            }
                        });
            }
        });


        sambaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                bmb.boom();
            }
        });

        bmb.setOnBoomListener(new OnBoomListenerAdapter() {
            @Override
            public void onBoomWillShow() {
                super.onBoomWillShow();
                // logic here
            }
        });

        bmb.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {
                // If you have implement listeners for boom-buttons in builders,
                // then you shouldn't add any listener here for duplicate callbacks.

                switch (index) {


                    case 0:

                        Intent home = new Intent(ManageFriends.this, HomeScreen.class);
                        overridePendingTransition(0, 0);
                        startActivity(home);
                        finish();
                        break;

                    case 1:

                        if (!menu.equals("N/A")) {

                            if (menu.toLowerCase().contains("accounts")) {
                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                            } else {

                                Intent intent = new Intent(ManageFriends.this, AccountsScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Intent intent = new Intent(ManageFriends.this, AccountsScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(intent);
                            finish();

                        }

                        break;

                    case 2:

                        try {

                            CryptLib _crypt = new CryptLib();
                            Intent transfers = new Intent(ManageFriends.this, FundTransferScreen.class);
                            String user = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            if (!TextUtils.isEmpty(user) && !user.equals("N/A")) {

                                if (user.equals("NON_SAMBA")) {

                                    transfers.putExtra("requestFunds", "nonSamba");
                                }
                            }
                            overridePendingTransition(0, 0);
                            startActivity(transfers);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                            finish();
                        break;


                    case 3:

                        try {

                            CryptLib _crypt;

                            if (!menu.equals("N/A")) {
                                _crypt = new CryptLib();
                                if (menu.toLowerCase().contains("biller management")) {
                                    showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                } else {
                                    String user1 = constants.sharedPreferences.getString("type", "N/A");
                                    Intent payments = new Intent(ManageFriends.this, PayBillsScreen.class);
                                    overridePendingTransition(0, 0);
                                    user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                    if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                        if (user1.equals("NON_SAMBA")) {

                                            payments.putExtra("requestbill", "nonSamba");
                                        }
                                    }
                                    startActivity(payments);
//                                    finish();
                                }
                            } else {
                                _crypt = new CryptLib();
                                Intent payments = new Intent(ManageFriends.this, PayBillsScreen.class);
                                overridePendingTransition(0, 0);
                                String user1 = constants.sharedPreferences.getString("type", "N/A");
                                user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                    if (user1.equals("NON_SAMBA")) {

                                        payments.putExtra("requestbill", "nonSamba");
                                    }
                                }
                                startActivity(payments);
//                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;


                    case 4:

                        if (!menu.equals("N/A")) {

                            if (menu.toLowerCase().contains("beneficiary management")) {
                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                            } else {

                                if (userType.equals("NON_SAMBA")) {
                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                } else {
                                    Intent bene = new Intent(ManageFriends.this, ManageBeneficiaresScreen.class);
                                    overridePendingTransition(0, 0);
                                    startActivity(bene);
//                                    finish();
                                }
                            }
                        } else {
                            if (userType.equals("NON_SAMBA")) {
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            } else {
                                Intent bene = new Intent(ManageFriends.this, ManageBeneficiaresScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(bene);
//                                    finish();
                            }
//                                finish();
                        }

                        break;


                    case 5:
                        if (userType.equals("NON_SAMBA"))
                            showDilogForError("This feature is only available for Samba Users.", "WARNING");
                        else {
                            Intent intent = new Intent(ManageFriends.this, MainActivity.class);
                            overridePendingTransition(0, 0);
                            startActivity(intent);
                        }



//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("friends management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//
//                                Intent friends = new Intent(ManageFriends.this, ManageFriends.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(friends);
////                                    finish();
//                            }
//                        } else {
//                            Intent friends = new Intent(ManageFriends.this, ManageFriends.class);
//                            overridePendingTransition(0, 0);
//                            startActivity(friends);
////                                finish();
//                        }

                        break;


                    case 6:

                        try {

                            CryptLib _crypt = new CryptLib();

                            String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                            if (userType.equals("NON_SAMBA"))
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            else {
                                Intent intent = new Intent(ManageFriends.this, EatMubarakMainScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        break;


//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("cards management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//                                if (userType.equals("NON_SAMBA")) {
//                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                                } else {
//                                    Intent cards = new Intent(ManageFriends.this, EatMubarakMainScreen.class);
//                                    overridePendingTransition(0, 0);
//                                    startActivity(cards);
////                                    finish();
//                                }
//
////                                    finish();
//                            }
//                        } else {
//                            if (userType.equals("NON_SAMBA")) {
//                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                            } else {
//                                Intent cards = new Intent(ManageFriends.this, EatMubarakMainScreen.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(cards);
////                                    finish();
//                            }
////                                finish();
//                        }


                    case 7:
                        if (userType.equals("NON_SAMBA")) {
                            showDilogForError("This feature is only available for Samba Users.", "WARNING");
                        } else {
                            Intent atm = new Intent(ManageFriends.this, BookMeMainScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(atm);
//                        finish();
                        }
                        break;

                    case 8:

                        pd.show();
                        showDialog("Are you sure you want to Logout?");
                        break;


                    default: {

                        break;
                    }
                }
            }

            @Override
            public void onBackgroundClick() {
//                textViewForAnimation.setText("Click background!!!");
            }

            @Override
            public void onBoomWillHide() {
                Log.d("BMB", "onBoomWillHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will RE-BOOM!!!");
            }

            @Override
            public void onBoomDidHide() {
                Log.d("BMB", "onBoomDidHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did RE-BOOM!!!");
            }

            @Override
            public void onBoomWillShow() {
                Log.d("BMB", "onBoomWillShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will BOOM!!!");
            }

            @Override
            public void onBoomDidShow() {
                Log.d("BMB", "onBoomDidShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did BOOM!!!");
            }
        });

//        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++)
//            bmb.addBuilder(BuilderManager.getSimpleCircleButtonBuilder());

        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder()
                    .listener(new OnBMClickListener() {
                        @Override
                        public void onBoomButtonClick(int index) {
                            // When the boom-button corresponding this builder is clicked.
//                            Toast.makeText(HomeScreen.this, "Clicked " + index, Toast.LENGTH_SHORT).show();
                        }
                    });

//            int margin = getResources().getDimensionPixelSize(R.dimen._4sdp);
            builder.textSize(12);

            switch (i) {

                case 0:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Home");
                    builder.unableText("Home");
                    builder.normalText("Home");
                    break;

                case 1:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Accounts");
                    builder.unableText("Accounts");
                    builder.normalText("Accounts");
                    break;

                case 2:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Transfers");
                    builder.unableText("Transfers");
                    builder.normalText("Transfers");
                    break;


                case 3:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Payments");
                    builder.unableText("Payments");
                    builder.normalText("Payments");
                    break;


                case 4:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Beneficiaries");
                    builder.unableText("Beneficiaries");
                    builder.normalText("Beneficiaries");
                    break;


                case 5:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("QR Payments");
                    builder.unableText("QR Payments");
                    builder.normalText("QR Payments");


//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Friends");
//                    builder.unableText("Friends");
//                    builder.normalText("Friends");
                    break;


                case 6:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Food");
                    builder.unableText("Food");
                    builder.normalText("Food");


//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Cards");
//                    builder.unableText("Cards");
//                    builder.normalText("Cards");
                    break;


                case 7:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("E-Tickets");
                    builder.unableText("E-Tickets");
                    builder.normalText("E-Tickets");
                    break;

                case 8:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Logout");
                    builder.unableText("Logout");
                    builder.normalText("Logout");
                    break;


                default: {

                    break;
                }

            }
//            builder.normalImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.unableImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.normalColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedColor(getResources().getColor(R.color.notiheader));
//            builder.unableColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedText("Cards");
//            builder.unableText("Cards");
//            builder.normalText("Cards");
//            builder.imagePadding(new Rect(40, 40, 40, 40));
            bmb.addBuilder(builder);
        }
    }

    public void GetFriendsList() {

        try {

            pd.show();
            if (helper.isNetworkAvailable()) {

//                webService.GetFriendsList(constants.sharedPreferences.getString("LegalID", "N/A"), constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getFriendsList();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                            } else {
//
//
//                                try {
//
//                                    JSONArray friendArray = new JSONArray(jsonObject.getString("friend_list"));
//
//                                    if (friendArray.length() > 0) {
//                                        for (int i = 0; i < friendArray.length(); i++) {
//                                            try {
//
//
//                                                JSONObject jsonObj = new JSONObject(friendArray.get(i).toString());
//
//                                                if (jsonObj.getString("is_favourite").equals("0")) {
//
//                                                    Friend friend = new Friend();
//                                                    friend.setFriendID(jsonObj.getString("friendId"));
//                                                    friend.setFriendName(jsonObj.getString("friend_name"));
//                                                    friend.setFriendNumber(jsonObj.getString("friend_number"));
//                                                    friend.setUserID(jsonObj.getString("user_id"));
//                                                    friend.setUserName(jsonObj.getString("user_name"));
//                                                    friend.setUserNumber(jsonObj.getString("user_number"));
//                                                    friend.setIsFav(jsonObj.getString("is_favourite"));
//                                                    friend.setFriendImage(jsonObj.getString("friend_image"));
//                                                    String firstName = jsonObj.getString("friend_name");
//                                                    String s = firstName.substring(0, 1);
//                                                    friend.setFirstLetter(s);
//                                                    switch (s.toUpperCase()) {
//
//                                                        case "A":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "B":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "C":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "D":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "E":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "F":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "G":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "H":
//                                                            friend.setFriendColor("#0099cb");
//                                                            break;
//
//                                                        case "I":
//                                                            friend.setFriendColor("#0099cb");
//                                                            break;
//
//                                                        case "J":
//                                                            friend.setFriendColor("#0099cb");
//                                                            break;
//
//                                                        case "K":
//                                                            friend.setFriendColor("#0099cb");
//                                                            break;
//
//                                                        case "L":
//                                                            friend.setFriendColor("#0099cb");
//                                                            break;
//
//                                                        case "M":
//                                                            friend.setFriendColor("#0099cb");
//                                                            break;
//
//                                                        case "N":
//                                                            friend.setFriendColor("#cc0001");
//                                                            break;
//
//                                                        case "O":
//                                                            friend.setFriendColor("#cc0001");
//                                                            break;
//
//                                                        case "P":
//                                                            friend.setFriendColor("#cc0001");
//                                                            break;
//
//                                                        case "Q":
//                                                            friend.setFriendColor("#cc0001");
//                                                            break;
//
//                                                        case "R":
//                                                            friend.setFriendColor("#cc0001");
//                                                            break;
//
//                                                        case "S":
//                                                            friend.setFriendColor("#cc0001");
//                                                            break;
//
//                                                        case "T":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        case "U":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        case "V":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        case "W":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        case "X":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        case "Y":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        case "Z":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        default:
//                                                            friend.setFriendColor("#9a34c6");
//                                                            break;
//                                                    }
//                                                    friendModels.add(friend);
//
//
//                                                } else {
//
//                                                    Friend friend = new Friend();
//                                                    friend.setFriendID(jsonObj.getString("friendId"));
//                                                    friend.setFriendName(jsonObj.getString("friend_name"));
//                                                    friend.setFriendNumber(jsonObj.getString("friend_number"));
//                                                    friend.setUserID(jsonObj.getString("user_id"));
//                                                    friend.setUserName(jsonObj.getString("user_name"));
//                                                    friend.setUserNumber(jsonObj.getString("user_number"));
//                                                    friend.setFriendImage(jsonObj.getString("friend_image"));
//                                                    friend.setIsFav(jsonObj.getString("is_favourite"));
//                                                    String firstName = jsonObj.getString("friend_name");
//                                                    String s = firstName.substring(0, 1);
//                                                    friend.setFirstLetter(s);
//                                                    switch (s.toUpperCase()) {
//
//                                                        case "A":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "B":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "C":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "D":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "E":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "F":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "G":
//                                                            friend.setFriendColor("#379022");
//                                                            break;
//
//                                                        case "H":
//                                                            friend.setFriendColor("#0099cb");
//                                                            break;
//
//                                                        case "I":
//                                                            friend.setFriendColor("#0099cb");
//                                                            break;
//
//                                                        case "J":
//                                                            friend.setFriendColor("#0099cb");
//                                                            break;
//
//                                                        case "K":
//                                                            friend.setFriendColor("#0099cb");
//                                                            break;
//
//                                                        case "L":
//                                                            friend.setFriendColor("#0099cb");
//                                                            break;
//
//                                                        case "M":
//                                                            friend.setFriendColor("#0099cb");
//                                                            break;
//
//                                                        case "N":
//                                                            friend.setFriendColor("#cc0001");
//                                                            break;
//
//                                                        case "O":
//                                                            friend.setFriendColor("#cc0001");
//                                                            break;
//
//                                                        case "P":
//                                                            friend.setFriendColor("#cc0001");
//                                                            break;
//
//                                                        case "Q":
//                                                            friend.setFriendColor("#cc0001");
//                                                            break;
//
//                                                        case "R":
//                                                            friend.setFriendColor("#cc0001");
//                                                            break;
//
//                                                        case "S":
//                                                            friend.setFriendColor("#cc0001");
//                                                            break;
//
//                                                        case "T":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        case "U":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        case "V":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        case "W":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        case "X":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        case "Y":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        case "Z":
//                                                            friend.setFriendColor("#ffbb34");
//                                                            break;
//
//                                                        default:
//                                                            friend.setFriendColor("#9a34c6");
//                                                            break;
//                                                    }
//                                                    friendModels.add(friend);
//                                                    favouritesModels.add(friend);
//                                                }
//
//                                                customListBeneAdapter = new CustomListFriendsAdapter(friendModels, ManageFriends.this, type);
//                                                listView.setAdapter(customListBeneAdapter);
//                                                customListBeneAdapter.setMode(Attributes.Mode.Multiple);
//                                                listView.setAdapter(customListBeneAdapter);
//                                                listView.setSelected(false);
//
//                                                customHorizontalListAdapter = new CustomHorizontalListAdapter(favouritesModels, ManageFriends.this, type);
//                                                horizontalListView.setAdapter(customHorizontalListAdapter);
//                                                pd.dismiss();
//
//
//                                            } catch (Exception ex) {
//                                                ex.printStackTrace();
//                                                pd.dismiss();
//                                            }
//
//
//                                        }
//                                    } else {
//                                        pd.dismiss();
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//                                }
//                            }
//                        } else {
//
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                        }
//                    }
//                }, 1500);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sUser_id", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sUser_number", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sSigns_Username", user);
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(ManageFriends.this);

                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getFrindsURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("friends_listResult").getString("Status_Code").equals("00")) {
                                            try {
//
                                                JSONArray friendArray = new JSONArray(jsonObject.getJSONObject("friends_listResult").getString("friend_list"));

                                                if (friendArray.length() > 0) {
                                                    for (int i = 0; i < friendArray.length(); i++) {
                                                        try {


                                                            JSONObject jsonObj = new JSONObject(friendArray.get(i).toString());

                                                            if (jsonObj.getString("is_favourite").equals("0")) {

                                                                Friend friend = new Friend();
                                                                friend.setFriendID(jsonObj.getString("friendId"));
                                                                friend.setFriendName(jsonObj.getString("friend_name"));
                                                                friend.setFriendNumber(jsonObj.getString("friend_number"));
                                                                friend.setUserID(jsonObj.getString("user_id"));
                                                                friend.setUserName(jsonObj.getString("user_name"));
                                                                friend.setUserNumber(jsonObj.getString("user_number"));
                                                                friend.setIsFav(jsonObj.getString("is_favourite"));
                                                                if (jsonObj.has("friend_image")) {
                                                                    friend.setFriendImage(jsonObj.getString("friend_image").replaceAll("@PLUS@", "\\+"));
                                                                }


                                                                String firstName = jsonObj.getString("friend_name");
                                                                String s = firstName.substring(0, 1).toUpperCase();
                                                                friend.setFirstLetter(s);
                                                                switch (s.toUpperCase()) {

                                                                    case "A":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "B":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "C":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "D":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "E":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "F":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "G":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "H":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor2));
                                                                        break;

                                                                    case "I":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor2));
                                                                        break;

                                                                    case "J":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor2));
                                                                        break;

                                                                    case "K":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor2));
                                                                        break;

                                                                    case "L":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor2));
                                                                        break;

                                                                    case "M":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor2));
                                                                        break;

                                                                    case "N":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor3));
                                                                        break;

                                                                    case "O":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor3));
                                                                        break;

                                                                    case "P":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor3));
                                                                        break;

                                                                    case "Q":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor3));
                                                                        break;

                                                                    case "R":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor3));
                                                                        break;

                                                                    case "S":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor3));
                                                                        break;

                                                                    case "T":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    case "U":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    case "V":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    case "W":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    case "X":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    case "Y":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    case "Z":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    default:
                                                                        friend.setFriendColor(getResources().getColor(R.color.defaultColor));
                                                                        break;
                                                                }
                                                                friendModels.add(friend);


                                                            } else {

                                                                Friend friend = new Friend();
                                                                friend.setFriendID(jsonObj.getString("friendId"));
                                                                friend.setFriendName(jsonObj.getString("friend_name"));
                                                                friend.setFriendNumber(jsonObj.getString("friend_number"));
                                                                friend.setUserID(jsonObj.getString("user_id"));
                                                                friend.setUserName(jsonObj.getString("user_name"));
                                                                friend.setUserNumber(jsonObj.getString("user_number"));
                                                                if (jsonObj.has("friend_image")) {
                                                                    friend.setFriendImage(jsonObj.getString("friend_image").replaceAll("@PLUS@", "\\+"));
                                                                }
                                                                friend.setIsFav(jsonObj.getString("is_favourite"));
                                                                String firstName = jsonObj.getString("friend_name");
                                                                String s = firstName.substring(0, 1).toUpperCase();
                                                                friend.setFirstLetter(s);
                                                                switch (s.toUpperCase()) {

                                                                    case "A":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "B":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "C":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "D":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "E":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "F":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "G":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor1));
                                                                        break;

                                                                    case "H":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor2));
                                                                        break;

                                                                    case "I":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor2));
                                                                        break;

                                                                    case "J":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor2));
                                                                        break;

                                                                    case "K":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor2));
                                                                        break;

                                                                    case "L":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor2));
                                                                        break;

                                                                    case "M":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor2));
                                                                        break;

                                                                    case "N":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor3));
                                                                        break;

                                                                    case "O":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor3));
                                                                        break;

                                                                    case "P":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor3));
                                                                        break;

                                                                    case "Q":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor3));
                                                                        break;

                                                                    case "R":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor3));
                                                                        break;

                                                                    case "S":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor3));
                                                                        break;

                                                                    case "T":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    case "U":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    case "V":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    case "W":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    case "X":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    case "Y":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    case "Z":
                                                                        friend.setFriendColor(getResources().getColor(R.color.friendColor4));
                                                                        break;

                                                                    default:
                                                                        friend.setFriendColor(getResources().getColor(R.color.defaultColor));
                                                                        break;
                                                                }
                                                                friendModels.add(friend);
                                                                favouritesModels.add(friend);
                                                            }

                                                            customListBeneAdapter = new CustomListFriendsAdapter(friendModels, ManageFriends.this, type);
                                                            listView.setAdapter(customListBeneAdapter);
                                                            customListBeneAdapter.setMode(Attributes.Mode.Multiple);
                                                            listView.setAdapter(customListBeneAdapter);
                                                            listView.setSelected(false);

                                                            customHorizontalListAdapter = new CustomHorizontalListAdapter(favouritesModels, ManageFriends.this, type);
                                                            horizontalListView.setAdapter(customHorizontalListAdapter);
                                                            pd.dismiss();


                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                            pd.dismiss();
//                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        }


                                                    }
                                                } else {
                                                    pd.dismiss();
                                                }


                                            } catch (JSONException ex) {

                                                ex.printStackTrace();
                                                pd.dismiss();
                                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                            }

                                        } else if (jsonObject.getJSONObject("friends_listResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("friends_listResult").getString("Status_Description"), "ERROR", ManageFriends.this, new LoginScreen());


                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("friends_listResult").getString("Status_Message"), "ERROR");
                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sUser_id", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sUser_number", constants.sharedPreferences.getString("MobileNumber", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ManageFriends.this, HomeScreen.class);
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(ManageFriends.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        btn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


    private void showDialog(String message) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Logout:");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Logout();
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                pd.dismiss();
            }
        });

        builder.show();
    }

    public void Logout() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSessionID", Constants.Shared_KEY);
                    HttpsTrustManager.allowMySSL(ManageFriends.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.logoutURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Code").equals("00")) {
                                                constants.editor.clear();
                                                constants.editor.commit();
                                                constants.editor.apply();
                                                constants.nonSambaCredentialsEditor.clear();
                                                constants.nonSambaCredentialsEditor.commit();
                                                constants.nonSambaCredentialsEditor.apply();
                                                constants.destinationAccountEditor.clear().commit();
                                                constants.destinationAccountEditor.apply();
                                                constants.sourceAccountEditor.clear().commit();
                                                constants.sourceAccountEditor.apply();
                                                constants.accountListEditor.clear().commit();
                                                constants.accountListEditor.apply();
                                                constants.accountsEditor.clear().commit();
                                                constants.accountsEditor.apply();
                                                constants.forgotPasswordEditor.clear().commit();
                                                constants.forgotPasswordEditor.apply();
                                                constants.purposeBillList.clear().commit();
                                                constants.purposeBillList.apply();
                                                constants.purposePrepaidList.clear().commit();
                                                constants.purposePrepaidList.apply();
                                                constants.purposeListEditor.clear().commit();
                                                constants.purposeListEditor.apply();


                                                pd.dismiss();
                                                Intent intent = new Intent(ManageFriends.this, LoginScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                finish();
                                                finishAffinity();


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Description"), "ERROR", ManageFriends.this, new LoginScreen());

                                            }

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sSessionID", Constants.Shared_KEY);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity", "WARNING");


            }
        } catch (Exception e) {

            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity", "WARNING");


        }
    }
}
