package com.ceesolutions.samba.friends.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.friends.FriendDetailsScreen;
import com.ceesolutions.samba.friends.ManageFriends;
import com.ceesolutions.samba.friends.Model.Friend;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.views.CircleImageView;
import com.ceesolutions.samba.views.RoundImageView;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 3/6/18.
 */

public class CustomListFriendsAdapter extends BaseSwipeAdapter {
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    ArrayList<Friend> userModels;
    LayoutInflater layoutInflater;
    View row;
    Context context;
    ImageView edit, trash, fav, arrow;
    TextView bankName, userName, accountNumber, titleTxt, heading, doneBtn, textView;
    RoundImageView title;
    ImageView click;
    int[] androidColors;
    private String isType;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Dialog pd, dialog;
    private Utils utils;
    private ImageView imageView;
    private CircleImageView profile_image;
    private AppPreferences appPreferences;
    private String location;

    public CustomListFriendsAdapter(ArrayList<Friend> userModelArrayList, Context mContext, String type) {
        userModels = userModelArrayList;
        context = mContext;
        constants = Constants.getConstants(context);
        isType = type;
        appPreferences = new AppPreferences(context);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return userModels.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View generateView(final int position, ViewGroup parent) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.friend_list_item, null);

//        if (convertView == null) {
//            convertView = LayoutInflater.from(context).
//                    inflate(R.layout.friend_list_item, parent, false);
//        }


        requestQueue = Volley.newRequestQueue(context);
        constants = Constants.getConstants(context);
        helper = Helper.getHelper(context);
        utils = new Utils(context);
        pd = new Dialog(context);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        return convertView;
    }

    @Override
    public void fillValues(final int position, View convertView) {

        click = (ImageView) convertView.findViewById(R.id.click);
        arrow = (ImageView) convertView.findViewById(R.id.image);
        profile_image = (CircleImageView) convertView.findViewById(R.id.profile_image);
        edit = (ImageView) convertView.findViewById(R.id.edit);
        trash = (ImageView) convertView.findViewById(R.id.trash);
        fav = (ImageView) convertView.findViewById(R.id.fav);
        userName = (TextView) convertView.findViewById(R.id.userName);
        titleTxt = (TextView) convertView.findViewById(R.id.titleTxt);
        title = (RoundImageView) convertView.findViewById(R.id.title);
        titleTxt.bringToFront();
        if (!TextUtils.isEmpty(userModels.get(position).getFriendImage()) && !userModels.get(position).getFriendImage().equals("") && !userModels.get(position).getFriendImage().equals(null)) {
            profile_image.setVisibility(View.VISIBLE);
            byte[] imageByteArray = Base64.decode(userModels.get(position).getFriendImage(), Base64.DEFAULT);
            RequestOptions options = new RequestOptions();
            options.centerCrop();
            Glide.with(context)
                    .load(imageByteArray)
                    .apply(options)
                    .into(profile_image);
            title.setVisibility(View.INVISIBLE);
            titleTxt.setVisibility(View.INVISIBLE);
        } else {
            title.setBackgroundTintList(ColorStateList.valueOf((userModels.get(position).getFriendColor())));
            titleTxt.setText(userModels.get(position).getFirstLetter());
            profile_image.setVisibility(View.INVISIBLE);
            title.setVisibility(View.VISIBLE);
            titleTxt.setVisibility(View.VISIBLE);
        }
        userName.setText(userModels.get(position).getFriendName());

        if (userModels.get(position).getIsFav().equals("1")) {

            fav.setImageResource(R.drawable.fav_);
        } else {

            fav.setImageResource(R.drawable.fav);
        }

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(((Activity) context), FriendDetailsScreen.class);
                intent.putExtra("userName", userModels.get(position).getUserName());
                intent.putExtra("userNumber", userModels.get(position).getUserNumber());
                intent.putExtra("userId", userModels.get(position).getUserID());
                intent.putExtra("friendName", userModels.get(position).getFriendName());
                intent.putExtra("friendNumber", userModels.get(position).getFriendNumber());
                intent.putExtra("friendImage", userModels.get(position).getFriendImage());
                intent.putExtra("friendID", userModels.get(position).getFriendID());
                intent.putExtra("isFav", userModels.get(position).getIsFav());
                ((Activity) context).overridePendingTransition(0, 0);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        });
        trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDilogForError("Are you sure you want to delete the friend?", "CONFIRMATION", "Delete", position, "");
            }
        });
        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userModels.get(position).getIsFav().equals("1")) {
                    showDilogForError("Are you sure you want to remove the friend from favourite?", "CONFIRMATION", "Fav", position, "0");
                } else {
                    showDilogForError("Are you sure you want to mark the friend as favourite?", "CONFIRMATION", "Fav", position, "1");
                }
            }
        });

        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    CryptLib _crypt = new CryptLib();
                    click.setEnabled(false);
                    if (!TextUtils.isEmpty(isType) && isType.equals("destination")) {
                        constants.sourceAccountEditor.putString("isSelect", "Yes");
                        constants.sourceAccountEditor.putString("userName", _crypt.encryptForParams(userModels.get(position).getUserName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendNumber", _crypt.encryptForParams(userModels.get(position).getFriendNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("userID", _crypt.encryptForParams(userModels.get(position).getUserID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendID", _crypt.encryptForParams(userModels.get(position).getFriendID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("mobileNumber", _crypt.encryptForParams(userModels.get(position).getUserNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendName", _crypt.encryptForParams(userModels.get(position).getFriendName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("isFav", _crypt.encryptForParams(userModels.get(position).getIsFav(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendImage", _crypt.encryptForParams(userModels.get(position).getFriendImage(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.commit();
                        constants.sourceAccountEditor.apply();
                        ((Activity) context).finish();
                        click.setEnabled(true);

                    } else {
                        Intent intent = new Intent(((Activity) context), FriendDetailsScreen.class);
                        intent.putExtra("userName", userModels.get(position).getUserName());
                        intent.putExtra("userNumber", userModels.get(position).getUserNumber());
                        intent.putExtra("userId", userModels.get(position).getUserID());
                        intent.putExtra("friendName", userModels.get(position).getFriendName());
                        intent.putExtra("friendNumber", userModels.get(position).getFriendNumber());
                        intent.putExtra("friendImage", userModels.get(position).getFriendImage());
                        intent.putExtra("friendID", userModels.get(position).getFriendID());
                        intent.putExtra("isFav", userModels.get(position).getIsFav());
                        ((Activity) context).overridePendingTransition(0, 0);
                        context.startActivity(intent);
                        click.setEnabled(true);
                        ((Activity) context).finish();
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        });

        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    CryptLib _crypt = new CryptLib();
                    arrow.setEnabled(false);
                    if (!TextUtils.isEmpty(isType) && isType.equals("destination")) {
                        constants.sourceAccountEditor.putString("isSelect", "Yes");
                        constants.sourceAccountEditor.putString("userName", _crypt.encryptForParams(userModels.get(position).getUserName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendNumber", _crypt.encryptForParams(userModels.get(position).getFriendNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("userID", _crypt.encryptForParams(userModels.get(position).getUserID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendID", _crypt.encryptForParams(userModels.get(position).getFriendID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("mobileNumber", _crypt.encryptForParams(userModels.get(position).getUserNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendName", _crypt.encryptForParams(userModels.get(position).getFriendName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("isFav", _crypt.encryptForParams(userModels.get(position).getIsFav(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendImage", _crypt.encryptForParams(userModels.get(position).getFriendImage(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.commit();
                        constants.sourceAccountEditor.apply();
                        ((Activity) context).finish();
                        arrow.setEnabled(true);

                    } else {
                        Intent intent = new Intent(((Activity) context), FriendDetailsScreen.class);
                        intent.putExtra("userName", userModels.get(position).getUserName());
                        intent.putExtra("userNumber", userModels.get(position).getUserNumber());
                        intent.putExtra("userId", userModels.get(position).getUserID());
                        intent.putExtra("friendName", userModels.get(position).getFriendName());
                        intent.putExtra("friendNumber", userModels.get(position).getFriendNumber());
                        intent.putExtra("friendImage", userModels.get(position).getFriendImage());
                        intent.putExtra("friendID", userModels.get(position).getFriendID());
                        intent.putExtra("isFav", userModels.get(position).getIsFav());
                        ((Activity) context).overridePendingTransition(0, 0);
                        context.startActivity(intent);
                        arrow.setEnabled(true);
                        ((Activity) context).finish();

                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        });
    }

    public void DeleteFriend(final int position) {


        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.DeleteFriend(constants.sharedPreferences.getString("LegalID", "N/A"), constants.sharedPreferences.getString("MobileNumber", "N/A"), intent1.getStringExtra("friendNumber"), "1", constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getDeleteFriend();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                               
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                      
//
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                       
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                   
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                           
//
//
//                        }
//                    }
//                }, 5000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sUser_id", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sUser_number", userModels.get(position).getUserNumber());
                params.put("sFriend_number", userModels.get(position).getFriendNumber());
                params.put("sSigns_Username", user);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(context);

                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.actionDeleteFriendURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("action_delete_favouriteResult").getString("Status_Code").equals("00")) {

                                            pd.dismiss();
                                            Intent intent = new Intent(context, ManageFriends.class);
                                            ((Activity) context).overridePendingTransition(0, 0);
                                            ((Activity) context).startActivity(intent);
                                            ((Activity) context).finish();

                                        } else if (jsonObject.getJSONObject("action_delete_favouriteResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("action_delete_favouriteResult").getString("Status_Description"), "ERROR", context, new LoginScreen());


                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("action_delete_favouriteResult").getString("Status_Description"), "ERROR");
                                        }

                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sUser_id", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sUser_number", userModels.get(position).getUserNumber());
//                        params.put("sFriend_number", userModels.get(position).getFriendNumber());
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    public void AddFriendToFav(final int poisition, final String sAction) {


        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.AddFriendToFav(constants.sharedPreferences.getString("LegalID", "N/A"), constants.sharedPreferences.getString("MobileNumber", "N/A"), intent1.getStringExtra("friendNumber"), "1", constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getAddFriendFav();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//
//                        }
//                    }
//                }, 5000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sUser_id", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sUser_number", userModels.get(poisition).getUserNumber());
                params.put("sFriend_number", userModels.get(poisition).getFriendNumber());
                params.put("sAction", sAction);
                params.put("sSigns_Username", user);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(context);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.addFriendFavURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("update_favourite_friendResult").getString("Status_Code").equals("00")) {

                                            pd.dismiss();
                                            Intent intent = new Intent(context, ManageFriends.class);
                                            ((Activity) context).overridePendingTransition(0, 0);
                                            ((Activity) context).startActivity(intent);
                                            ((Activity) context).finish();

                                        } else if (jsonObject.getJSONObject("update_favourite_friendResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("update_favourite_friendResult").getString("Status_Description"), "ERROR", context, new LoginScreen());


                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("update_favourite_friendResult").getString("Status_Description"), "ERROR");

                                        }

                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sUser_id", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sUser_number", userModels.get(poisition).getUserNumber());
//                        params.put("sFriend_number", userModels.get(poisition).getFriendNumber());
//                        params.put("sAction", sAction);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    public void showDilogForError(String msg, String header, final String type, final int position, final String sAction) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (type.equals("Delete")) {
                    dialog.dismiss();
                    DeleteFriend(position);
                } else {
                    dialog.dismiss();
                    AddFriendToFav(position, sAction);
                }

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }
}