package com.ceesolutions.samba.friends.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.beneficiariesManagement.Model.BankModel;
import com.ceesolutions.samba.views.CircularTextView;

import java.util.ArrayList;

/**
 * Created by ceeayaz on 3/6/18.
 */

public class CustomListAdapter extends BaseAdapter {
    ArrayList<BankModel> bankModels;
    LayoutInflater layoutInflater;
    View row;
    Context context;
    TextView bankName;
    CircularTextView title;

    public CustomListAdapter(ArrayList<BankModel> userModelArrayList, Context mContext) {
        bankModels = userModelArrayList;
        context = mContext;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return bankModels.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.bank_list_item, parent, false);
        }


        title = (CircularTextView) convertView.findViewById(R.id.title);
        bankName = (TextView) convertView.findViewById(R.id.bankName);

        title.setSolidColor(bankModels.get(position).getBankColor());
        title.setText(bankModels.get(position).getBankCode());

        bankName.setText(bankModels.get(position).getBankName());

        return convertView;
    }
}