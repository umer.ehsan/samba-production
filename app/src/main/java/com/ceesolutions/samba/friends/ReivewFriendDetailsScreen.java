package com.ceesolutions.samba.friends;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.CircleImageView;
import com.ceesolutions.samba.views.RoundImageView;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ceeayaz on 4/1/18.
 */

public class ReivewFriendDetailsScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private TextView message, doneBtn, heading, refTxt, mobileNumberTxt, userNameTxt, timeTxt, accMaskTxt, friendNameTxt, accFrindMaskTxt, totalAmountTxt, amountInWordsTxt, reviewBtn, dateTxt, emailAddressTxt;
    private ImageView cancelBtn;
    private Dialog pd, dialog;
    private WebService webService;
    private RequestQueue requestQueue;
    private Utils utils;
    private String userName, email, mobileNo;
    private byte[] byteArray;
    private Bitmap bitmap;
    private Helper helper;
    private Constants constants;
    private ImageView backBtn;
    private CircleImageView profile_image;
    private Intent intent1;
    private String finalDate, currentTime, imgString;
    private Date currentDate;
    private RoundImageView title;
    private TextView titleTxt;
    private int color;
    private AppPreferences appPreferences;
    private String location;
    private String userType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review_friend_details);
        initViews();
        getSupportActionBar().hide();

        reviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reviewBtn.setEnabled(false);
                if (userType.equals("NON_SAMBA")) {
                    FirebaseAnalytics.getInstance(ReivewFriendDetailsScreen.this).logEvent("Non_Samba_Send_Invitation_Pressed", new Bundle());
                }
                else
                {
                    FirebaseAnalytics.getInstance(ReivewFriendDetailsScreen.this).logEvent("Send_Invitation_Pressed", new Bundle());
                }

                AddFriend();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                showDilogForErrorForLogin("Are you sure you want to cancel the request?", "WARNING");
                Intent intent = new Intent(ReivewFriendDetailsScreen.this, ManageFriends.class);
                intent.putExtra("addFriend", "addFriend");
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
                finishAffinity();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }

    public void initViews() {

        titleTxt = (TextView) findViewById(R.id.titleTxt);
        title = (RoundImageView) findViewById(R.id.title);
        titleTxt.bringToFront();
        intent1 = getIntent();
        profile_image = (CircleImageView) findViewById(R.id.profile_image);
        userNameTxt = (TextView) findViewById(R.id.userNameText);
        reviewBtn = (TextView) findViewById(R.id.reviewBtn);
        dateTxt = (TextView) findViewById(R.id.date);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        mobileNumberTxt = (TextView) findViewById(R.id.mobileNumberTxt);
        emailAddressTxt = (TextView) findViewById(R.id.emailAddressTxt);
        cancelBtn = (ImageView) findViewById(R.id.cancelBtn);
        requestQueue = Volley.newRequestQueue(ReivewFriendDetailsScreen.this);
        webService = new WebService();
        helper = Helper.getHelper(ReivewFriendDetailsScreen.this);
        constants = Constants.getConstants(ReivewFriendDetailsScreen.this);
        pd = new Dialog(ReivewFriendDetailsScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        utils = new Utils(ReivewFriendDetailsScreen.this);
        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.US);
        currentDate = Calendar.getInstance().getTime();
        appPreferences = new AppPreferences(ReivewFriendDetailsScreen.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        try {
            CryptLib _crypt = new CryptLib();
            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        formatter = new SimpleDateFormat("dd-MMM-yyyy");
        dateTxt.setText(formatter.format(currentDate));
        finalDate = formatter.format(currentDate);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
        currentTime = sdf.format(currentDate);
        userNameTxt.setText(intent1.getStringExtra("userName"));
        mobileNumberTxt.setText(intent1.getStringExtra("mobileNumber"));
        emailAddressTxt.setText(intent1.getStringExtra("emailAddress"));
        byteArray = intent1.getByteArrayExtra("image");
        String s = intent1.getStringExtra("userName").substring(0, 1);

        switch (s.toUpperCase()) {

            case "A":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "B":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "C":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "D":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "E":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "F":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "G":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "H":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "I":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "J":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "K":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "L":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "M":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "N":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "O":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "P":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "Q":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "R":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "S":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "T":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "U":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "V":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "W":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "X":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "Y":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "Z":
                color = getResources().getColor(R.color.friendColor4);
                break;

            default:
                color = getResources().getColor(R.color.defaultColor);
                break;
        }
        if (byteArray != null)
            bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        else
            bitmap = null;


        if (bitmap != null) {
            titleTxt.setVisibility(View.INVISIBLE);
            title.setVisibility(View.INVISIBLE);
            RequestOptions options = new RequestOptions();
            options.centerCrop();
            Glide.with(ReivewFriendDetailsScreen.this)
                    .load((bitmap))
                    .apply(options)
                    .into(profile_image);
        } else {

            profile_image.setVisibility(View.INVISIBLE);
            title.setBackgroundTintList(ColorStateList.valueOf(color));
            titleTxt.setText(s);
        }

        mobileNo = mobileNumberTxt.getText().

                toString().

                startsWith("0") ? mobileNumberTxt.getText().

                toString().

                substring(1) : mobileNumberTxt.getText().

                toString();

        mobileNo = mobileNo.replaceAll("\\)", "").

                replaceAll("\\(", "").

                replaceAll(" ", "").

                trim();

    }

    public void AddFriend() {

        try {

            pd.show();
            if (helper.isNetworkAvailable()) {

//                webService.AddFriend(constants.sharedPreferences.getString("LegalID", "N/A"), constants.sharedPreferences.getString("userName", "N/A"), constants.sharedPreferences.getString("MobileNumber", "N/A"), mobileNo, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getAddFriend();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                reviewBtn.setEnabled(true);
//
//                            } else {
//
//
//                                try {
//
//                                    if (jsonObject.getString("Status_Code").equals("00")) {
//
//                                        pd.dismiss();
//                                        Intent intent = new Intent(ReivewFriendDetailsScreen.this, FriendAddConfirmation.class);
//                                        intent.putExtra("userName", intent1.getStringExtra("userName"));
//                                        intent.putExtra("finalDate", finalDate);
//                                        intent.putExtra("currentTime", currentTime);
//                                        overridePendingTransition(0, 0);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        startActivity(intent);
//                                        finish();
//                                        finishAffinity();
//
//                                    } else {
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                        reviewBtn.setEnabled(true);
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    reviewBtn.setEnabled(true);
//                                }
//                            }
//                        } else {
//
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            reviewBtn.setEnabled(true);
//
//                        }
//                    }
//                }, 3000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sUser_id", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sUser_number", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sUser_name", _crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sFriend_number", mobileNo);
                params.put("sFriend_email", intent1.getStringExtra("emailAddress"));
                params.put("sSigns_Username", user);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(ReivewFriendDetailsScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.addFriendURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("add_friendResult").getString("Status_Code").equals("00")) {

                                            pd.dismiss();
                                            Intent intent = new Intent(ReivewFriendDetailsScreen.this, FriendAddConfirmation.class);
                                            intent.putExtra("userName", intent1.getStringExtra("userName"));
                                            intent.putExtra("finalDate", finalDate);
                                            intent.putExtra("currentTime", currentTime);
                                            intent.putExtra("image", intent1.getByteArrayExtra("image"));
                                            overridePendingTransition(0, 0);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                            finish();
                                            finishAffinity();

                                        } else if (jsonObject.getJSONObject("add_friendResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("add_friendResult").getString("Status_Description"), "ERROR", ReivewFriendDetailsScreen.this, new LoginScreen());
                                            reviewBtn.setEnabled(true);

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("add_friendResult").getString("Status_Message"), "ERROR");
                                            reviewBtn.setEnabled(true);
                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        reviewBtn.setEnabled(true);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    reviewBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    reviewBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    reviewBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sUser_id", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sUser_number", constants.sharedPreferences.getString("MobileNumber", "N/A"));
//                        params.put("sUser_name", constants.sharedPreferences.getString("T24_FullName", "N/A"));
//                        params.put("sFriend_number", mobileNo);
//                        params.put("sFriend_email", intent1.getStringExtra("emailAddress"));
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                reviewBtn.setEnabled(true);

            }
        } catch (Exception e) {

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            reviewBtn.setEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        finish();
//        showDilogForErrorForLogin("Are you sure you want to cancel the request?", "WARNING");
        Intent intent = new Intent(ReivewFriendDetailsScreen.this, ManageFriends.class);
        intent.putExtra("addFriend", "addFriend");
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
        finishAffinity();
    }

    public void showDilogForErrorForLogin(String msg, String header) {

        dialog = new Dialog(ReivewFriendDetailsScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(ReivewFriendDetailsScreen.this, ManageFriends.class);
                intent.putExtra("addFriend", "addFriend");
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
                finishAffinity();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();


    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }
}
