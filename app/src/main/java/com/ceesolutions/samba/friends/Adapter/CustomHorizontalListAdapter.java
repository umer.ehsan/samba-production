package com.ceesolutions.samba.friends.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.beneficiariesManagement.Model.UserModel;
import com.ceesolutions.samba.friends.FriendDetailsScreen;
import com.ceesolutions.samba.friends.Model.Friend;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.views.CircleImageView;
import com.ceesolutions.samba.views.CircularTextView;
import com.ceesolutions.samba.views.RoundImageView;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ceeayaz on 4/1/18.
 */

public class CustomHorizontalListAdapter extends BaseAdapter {
    ArrayList<Friend> userModels;
    LayoutInflater layoutInflater;
    View row;
    Context context;
    TextView userName, titleTxt;
    RoundImageView title;
    int[] androidColors;
    private Constants constants;
    private String isType;
    private CircleImageView profile_image;

    public CustomHorizontalListAdapter(ArrayList<Friend> userModelArrayList, Context mContext, String type) {
        userModels = userModelArrayList;
        context = mContext;
        androidColors = context.getResources().getIntArray(R.array.androidcolors);
        constants = Constants.getConstants(context);
        isType = type;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return userModels.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.favourite_list_item, parent, false);
        }


//        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
//
//        String color = String.format("#%06X", (0xFFFFFF & randomAndroidColor));
        titleTxt = (TextView) convertView.findViewById(R.id.titleTxt);
        title = (RoundImageView) convertView.findViewById(R.id.title);
        profile_image = (CircleImageView) convertView.findViewById(R.id.profile_image);
        titleTxt.bringToFront();
        userName = (TextView) convertView.findViewById(R.id.userName);
        if (!TextUtils.isEmpty(userModels.get(position).getFriendImage())) {

            profile_image.setVisibility(View.VISIBLE);
            byte[] imageByteArray = Base64.decode(userModels.get(position).getFriendImage(), Base64.DEFAULT);
            RequestOptions options = new RequestOptions();
            options.centerCrop();
            Glide.with(context)
                    .load(imageByteArray)
                    .apply(options)
                    .into(profile_image);
            title.setVisibility(View.INVISIBLE);
            titleTxt.setVisibility(View.INVISIBLE);
        } else {
            title.setBackgroundTintList(ColorStateList.valueOf(userModels.get(position).getFriendColor()));
            titleTxt.setText(userModels.get(position).getFirstLetter());
        }
//        title.setSolidColor(userModels.get(position).getFriendColor());
//        title.setText(userModels.get(position).getFirstLetter());


        userName.setText(userModels.get(position).getFriendName());

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    CryptLib _crypt = new CryptLib();

                    title.setEnabled(false);

                    if (!TextUtils.isEmpty(isType) && isType.equals("destination")) {
                        constants.sourceAccountEditor.putString("isSelect", "Yes");
                        constants.sourceAccountEditor.putString("userName", _crypt.encryptForParams(userModels.get(position).getUserName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendNumber", _crypt.encryptForParams(userModels.get(position).getFriendNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("userID", _crypt.encryptForParams(userModels.get(position).getUserID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendID", _crypt.encryptForParams(userModels.get(position).getFriendID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("mobileNumber", _crypt.encryptForParams(userModels.get(position).getUserNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendName", _crypt.encryptForParams(userModels.get(position).getFriendName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("isFav", _crypt.encryptForParams(userModels.get(position).getIsFav(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendImage", _crypt.encryptForParams(userModels.get(position).getFriendImage(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.commit();
                        constants.sourceAccountEditor.apply();
                        ((Activity) context).finish();
                        title.setEnabled(true);

                    } else {
                        Intent intent = new Intent(((Activity) context), FriendDetailsScreen.class);
                        intent.putExtra("userName", userModels.get(position).getUserName());
                        intent.putExtra("userNumber", userModels.get(position).getUserNumber());
                        intent.putExtra("userId", userModels.get(position).getUserID());
                        intent.putExtra("friendName", userModels.get(position).getFriendName());
                        intent.putExtra("friendNumber", userModels.get(position).getFriendNumber());
                        intent.putExtra("friendImage", userModels.get(position).getFriendImage());
                        intent.putExtra("friendID", userModels.get(position).getFriendID());
                        intent.putExtra("isFav", userModels.get(position).getIsFav());
                        ((Activity) context).overridePendingTransition(0, 0);
                        context.startActivity(intent);
                        title.setEnabled(true);
                        ((Activity) context).finish();
//                    finalConvertView.setEnabled(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    CryptLib _crypt = new CryptLib();
                    profile_image.setEnabled(false);
                    if (!TextUtils.isEmpty(isType) && isType.equals("destination")) {
                        constants.sourceAccountEditor.putString("isSelect", "Yes");
                        constants.sourceAccountEditor.putString("userName", _crypt.encryptForParams(userModels.get(position).getUserName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendNumber", _crypt.encryptForParams(userModels.get(position).getFriendNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("userID", _crypt.encryptForParams(userModels.get(position).getUserID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendID", _crypt.encryptForParams(userModels.get(position).getFriendID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("mobileNumber", _crypt.encryptForParams(userModels.get(position).getUserNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendName", _crypt.encryptForParams(userModels.get(position).getFriendName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("isFav", _crypt.encryptForParams(userModels.get(position).getIsFav(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.putString("friendImage", _crypt.encryptForParams(userModels.get(position).getFriendImage(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.sourceAccountEditor.commit();
                        constants.sourceAccountEditor.apply();
                        ((Activity) context).finish();
                        profile_image.setEnabled(true);

                    } else {
                        Intent intent = new Intent(((Activity) context), FriendDetailsScreen.class);
                        intent.putExtra("userName", userModels.get(position).getUserName());
                        intent.putExtra("userNumber", userModels.get(position).getUserNumber());
                        intent.putExtra("userId", userModels.get(position).getUserID());
                        intent.putExtra("friendName", userModels.get(position).getFriendName());
                        intent.putExtra("friendNumber", userModels.get(position).getFriendNumber());
                        intent.putExtra("friendImage", userModels.get(position).getFriendImage());
                        intent.putExtra("friendID", userModels.get(position).getFriendID());
                        intent.putExtra("isFav", userModels.get(position).getIsFav());
                        ((Activity) context).overridePendingTransition(0, 0);
                        context.startActivity(intent);
                        profile_image.setEnabled(true);
                        ((Activity) context).finish();
//                    finalConvertView.setEnabled(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return convertView;
    }
}