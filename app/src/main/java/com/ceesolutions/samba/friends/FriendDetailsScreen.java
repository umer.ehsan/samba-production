package com.ceesolutions.samba.friends;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.transactions.FundTransferScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.CircleImageView;
import com.ceesolutions.samba.views.RoundImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/1/18.
 */

public class FriendDetailsScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private EditText editAlias, editAccountNumber, editMobileNumber, editEmailAddress, editAccountTitle, editBankName;
    private TextView transferFundsBtn, addToFavBtn, deleteBeneficiaryBtn, requestFundsBtn, textView, doneBtn, heading;
    private Dialog dialog;
    private WebService webService;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private Intent intent1;
    private ImageView backBtn, imageHeader;
    private String isFav;
    private CircleImageView profile_image;
    private RoundImageView title;
    private TextView titleTxt;
    private int color;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friend_detials);
        getSupportActionBar().hide();
        initViews();

        transferFundsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        addToFavBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isFav.equals("1")) {
                    showDilogForError("Are you sure you want to remove the friend from favourite?", "CONFIRMATION", "Fav", "0");

                } else {
                    showDilogForError("Are you sure you want to mark the friend as favourite?", "CONFIRMATION", "Fav", "1");


                }

            }
        });

        deleteBeneficiaryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDilogForError("Are you sure you want to delete the friend?", "CONFIRMATION", "Delete", "");
            }
        });


        requestFundsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(FriendDetailsScreen.this, FundTransferScreen.class);
                overridePendingTransition(0, 0);
                intent.putExtra("friendName", intent1.getStringExtra("friendName"));
                intent.putExtra("friendID", intent1.getStringExtra("friendID"));
                intent.putExtra("friendNumber", intent1.getStringExtra("friendNumber"));
                intent.putExtra("friendImage", intent1.getStringExtra("friendImage"));
                intent.putExtra("userId", intent1.getStringExtra("userId"));
                intent.putExtra("userNumber", intent1.getStringExtra("userNumber"));
                intent.putExtra("userName", intent1.getStringExtra("userName"));
                intent.putExtra("isFav", intent1.getStringExtra("isFav"));
                intent.putExtra("requestFunds", "friendRequest");
                startActivity(intent);


            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(FriendDetailsScreen.this, ManageFriends.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        });
    }

    private void initViews() {

        try {


            titleTxt = (TextView) findViewById(R.id.titleTxt);
            title = (RoundImageView) findViewById(R.id.title);
            titleTxt.bringToFront();
            editAlias = (EditText) findViewById(R.id.editAlias);
            profile_image = (CircleImageView) findViewById(R.id.profile_image);
            editMobileNumber = (EditText) findViewById(R.id.editMobileNumber);
            transferFundsBtn = (TextView) findViewById(R.id.transferFundsBtn);
            addToFavBtn = (TextView) findViewById(R.id.addToFavBtn);
            deleteBeneficiaryBtn = (TextView) findViewById(R.id.deleteBeneficiaryBtn);
            requestFundsBtn = (TextView) findViewById(R.id.requestFundsBtn);
            webService = new WebService();
            helper = Helper.getHelper(this);
            backBtn = (ImageView) findViewById(R.id.backBtn);
            requestQueue = Volley.newRequestQueue(this);
            constants = Constants.getConstants(FriendDetailsScreen.this);
            utils = new Utils(FriendDetailsScreen.this);
            pd = new Dialog(FriendDetailsScreen.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            intent1 = getIntent();
            appPreferences = new AppPreferences(FriendDetailsScreen.this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }
            editAlias.setText(intent1.getStringExtra("friendName"));
            String s = intent1.getStringExtra("friendName").substring(0, 1);

            switch (s.toUpperCase()) {

                case "A":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "B":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "C":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "D":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "E":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "F":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "G":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "H":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "I":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "J":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "K":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "L":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "M":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "N":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "O":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "P":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "Q":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "R":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "S":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "T":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "U":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "V":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "W":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "X":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "Y":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "Z":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                default:
                    color = getResources().getColor(R.color.defaultColor);
                    break;
            }
            isFav = intent1.getStringExtra("isFav");
            if (isFav.equals("1")) {

                addToFavBtn.setText("Remove Favourite");
                addToFavBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.favoritefill, 0, 0, 0);
                addToFavBtn.setBackgroundResource(R.drawable.border_blue);
                addToFavBtn.setTextColor(getResources().getColor(R.color.notiheader));

            } else {

            }
            editMobileNumber.setText(intent1.getStringExtra("friendNumber"));
            String img = intent1.getStringExtra("friendImage");
//        imageHeader = (ImageView) findViewById(R.id.imageHeader);
            if (!TextUtils.isEmpty(img)) {
                titleTxt.setVisibility(View.INVISIBLE);
                title.setVisibility(View.INVISIBLE);
                byte[] imageByteArray = Base64.decode(img, Base64.DEFAULT);
                RequestOptions options = new RequestOptions();
                options.centerCrop();
                Glide.with(FriendDetailsScreen.this)
                        .load(imageByteArray)
                        .apply(options)
                        .into(profile_image);

            } else {

                profile_image.setVisibility(View.INVISIBLE);
                title.setBackgroundTintList(ColorStateList.valueOf(color));
                titleTxt.setText(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showDilogForError(String msg, String header, final String type, final String sAction) {

        dialog = new Dialog(FriendDetailsScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doneBtn.setEnabled(false);
                if (type.equals("Delete")) {
                    dialog.dismiss();
                    DeleteFriend();
                } else {
                    dialog.dismiss();
                    AddFriendToFav(sAction);
                }

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


    public void AddFriendToFav(final String sAction) {


        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.AddFriendToFav(constants.sharedPreferences.getString("LegalID", "N/A"), constants.sharedPreferences.getString("MobileNumber", "N/A"), intent1.getStringExtra("friendNumber"), "1", constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getAddFriendFav();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                doneBtn.setEnabled(true);
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        Intent intent = new Intent(FriendDetailsScreen.this, ManageFriends.class);
//                                        intent.putExtra("addFriend","addFriend");
//                                        startActivity(intent);
//                                        overridePendingTransition(0, 0);
//                                        finish();
//                                        finishAffinity();
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                        doneBtn.setEnabled(true);
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    doneBtn.setEnabled(true);
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            doneBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 5000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sUser_id", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sUser_number", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sFriend_number", intent1.getStringExtra("friendNumber"));
                params.put("sAction", sAction);
                params.put("sServerSessionKey", md5);
                params.put("sSigns_Username", user);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(FriendDetailsScreen.this);

                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.addFriendFavURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("update_favourite_friendResult").getString("Status_Code").equals("00")) {
//                                        JsonObj.put("Status_Code", jsonObject.getJSONObject("update_favourite_friendResult").getString("Status_Code"));

                                            pd.dismiss();
                                            Intent intent = new Intent(FriendDetailsScreen.this, ManageFriends.class);
//                                        intent.putExtra("addFriend", "addFriend");
                                            doneBtn.setEnabled(true);
                                            startActivity(intent);
                                            overridePendingTransition(0, 0);
                                            finish();
                                        } else if (jsonObject.getJSONObject("update_favourite_friendResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("update_favourite_friendResult").getString("Status_Description"), "ERROR", FriendDetailsScreen.this, new LoginScreen());
                                            doneBtn.setEnabled(true);

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("update_favourite_friendResult").getString("Status_Message"), "ERROR");
                                            doneBtn.setEnabled(true);
                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        doneBtn.setEnabled(true);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sUser_id", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sUser_number", constants.sharedPreferences.getString("MobileNumber", "N/A"));
//                        params.put("sFriend_number", intent1.getStringExtra("friendNumber"));
//                        params.put("sAction", sAction);
//                        params.put("sServerSessionKey", md5);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                doneBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            doneBtn.setEnabled(true);

        }
    }

    public void DeleteFriend() {


        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.DeleteFriend(constants.sharedPreferences.getString("LegalID", "N/A"), constants.sharedPreferences.getString("MobileNumber", "N/A"), intent1.getStringExtra("friendNumber"), "1", constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getDeleteFriend();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                doneBtn.setEnabled(true);
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        Intent intent = new Intent(FriendDetailsScreen.this, ManageFriends.class);
//                                        startActivity(intent);
//                                        doneBtn.setEnabled(true);
//                                        overridePendingTransition(0, 0);
//                                        finish();
//
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                        doneBtn.setEnabled(true);
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    doneBtn.setEnabled(true);
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            doneBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 5000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sUser_id", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sUser_number", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sFriend_number", intent1.getStringExtra("friendNumber"));
                params.put("sSigns_Username", user);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(FriendDetailsScreen.this);

                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.actionDeleteFriendURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("action_delete_favouriteResult").getString("Status_Code").equals("00")) {
//                                        JsonObj.put("Status_Code", jsonObject.getJSONObject("action_delete_favouriteResult").getString("Status_Code"));
                                            pd.dismiss();
                                            Intent intent = new Intent(FriendDetailsScreen.this, ManageFriends.class);
                                            startActivity(intent);
                                            doneBtn.setEnabled(true);
                                            overridePendingTransition(0, 0);
                                            finish();

                                        } else if (jsonObject.getJSONObject("action_delete_favouriteResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("action_delete_favouriteResult").getString("Status_Description"), "ERROR", FriendDetailsScreen.this, new LoginScreen());
                                            doneBtn.setEnabled(true);

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("action_delete_favouriteResult").getString("Status_Message"), "ERROR");
                                            doneBtn.setEnabled(true);
                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        doneBtn.setEnabled(true);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sUser_id", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sUser_number", constants.sharedPreferences.getString("MobileNumber", "N/A"));
//                        params.put("sFriend_number", intent1.getStringExtra("friendNumber"));
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                doneBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            doneBtn.setEnabled(true);

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(FriendDetailsScreen.this, ManageFriends.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }
}
