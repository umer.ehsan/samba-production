package com.ceesolutions.samba.friends;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.ceesolutions.samba.views.CircleImageView;
import com.ceesolutions.samba.views.RoundImageView;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

/**
 * Created by ceeayaz on 4/1/18.
 */

public class AddFriendScreen extends AppCompatActivity {


    private AppCompatTextView message, messageForAccountNumber, errorMessageForEmail, errorMessageForMobNumber, errorMessageForAlias, errorMessageForAccount;
    private ImageView backbtn, notificationBtn, imageHeader;
    private TextInputEditText editAlias, editAccountNumber, editMobileNumber, editEmailAddress;
    private String alias, accountNumber, emailAddress, mobileNumber, bankName;
    private Spinner mobileNumbersSpinner, bankNameSpinner;
    private AppCompatTextView validateBtn;
    private String[] mobileNumbers;
    private boolean isValid = false;
    private Dialog pd;
    private WebService webService;
    private RequestQueue requestQueue;
    private Utils utils;
    private String userName, email;
    private byte[] byteArray;
    private Bitmap bitmap;
    private Helper helper;
    private Constants constants;
    private ArrayList<String> phoneNumbers;
    private Intent intent1;
    private CircleImageView profile_image;
    private RoundImageView title;
    private TextView titleTxt;
    private int color;
    private String userType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_friend);
        getSupportActionBar().hide();
        initViews();

        editAlias.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessageForAlias.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessageForAlias.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

                alias = s.toString();
            }
        });


        editEmailAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessageForEmail.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessageForEmail.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                emailAddress = s.toString();
            }
        });


        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (userType.equals("NON_SAMBA")) {
                    FirebaseAnalytics.getInstance(AddFriendScreen.this).logEvent("Non_Samba_Add_Friend_Verification_Press", new Bundle());
                } else {
                    FirebaseAnalytics.getInstance(AddFriendScreen.this).logEvent("Add_Friend_Verification_Pressed", new Bundle());
                }


                alias = editAlias.getText().toString().trim();
                emailAddress = editEmailAddress.getText().toString().trim();

                try {
                    CryptLib _crypt = new CryptLib();

                    if (TextUtils.isEmpty(emailAddress)) {

                        emailAddress = "N/A";
                        isValid = true;

                    } else {

                        if (!emailAddress.equals("N/A")) {


                            if (!emailAddress.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") || emailAddress.contains(" ")) {
                                errorMessageForEmail.setVisibility(View.VISIBLE);
                                errorMessageForEmail.bringToFront();
                                errorMessageForEmail.setError("");
                                isValid = false;
                                errorMessageForEmail.setText("Please enter valid email address");
                            } else {

                                isValid = true;
                                emailAddress = editEmailAddress.getText().toString();

                            }
                        } else {

                            isValid = true;
                        }
                    }


                    if (TextUtils.isEmpty(alias)) {
                        errorMessageForAlias.setVisibility(View.VISIBLE);
                        errorMessageForAlias.bringToFront();
                        errorMessageForAlias.setError("");
                        errorMessageForAlias.setText("Please enter valid alias");

                    } else if (!helper.validateInputForSC(alias)) {

                        errorMessageForAlias.setVisibility(View.VISIBLE);
                        errorMessageForAlias.bringToFront();
                        errorMessageForAlias.setError("");
                        errorMessageForAlias.setText("Alias name cannot contains <,>,\",',%,(,),&,+,\\,~");


                    }

                    if ((!TextUtils.isEmpty(alias) && helper.validateInputForSC(alias)) && ((!TextUtils.isEmpty(emailAddress) && isValid == true && helper.validateInputForSC(emailAddress) && !emailAddress.contains(" ")))) {
//                    Intent intent = new Intent(UserValidationScreen.this, UserCodeVerificationScreen.class);
//                    startActivity(intent);
                        String userNumber = _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                        if (!TextUtils.isEmpty(userNumber) && !userNumber.equals("N/A")) {

                            String friendNumber = "";

                            if (!TextUtils.isEmpty(mobileNumber) && !mobileNumber.equals(null) && !mobileNumber.equals("null")) {

                                friendNumber = mobileNumber.replaceAll("-", "").replaceAll(" ", "").replaceAll("\\)", "").replaceAll("\\(", "").replaceAll("\\+", "");

                            }
                            if (!TextUtils.isEmpty(friendNumber) && !friendNumber.equals("null") && !friendNumber.equals(null)) {


                                if (friendNumber.contains(userNumber)) {

                                    utils.showDilogForError("You cannot send a friend request to your own mobile number.", "ERROR");
                                } else {


                                    validateBtn.setEnabled(false);
                                    Intent intent = new Intent(AddFriendScreen.this, ReivewFriendDetailsScreen.class);
                                    overridePendingTransition(0, 0);
                                    intent.putExtra("userName", alias);
                                    intent.putExtra("mobileNumber", mobileNumber.replaceAll("-", "").replaceAll(" ", "").replaceAll("\\)", "").replaceAll("\\(", "").replaceAll("\\+", ""));
                                    intent.putExtra("emailAddress", emailAddress);
                                    intent.putExtra("image", intent1.getByteArrayExtra("image"));
                                    overridePendingTransition(0, 0);
                                    startActivity(intent);
                                    validateBtn.setEnabled(true);
                                }
                            } else {

                                utils.showDilogForError("No phone number found. Please select another friend.", "WARNING");
                            }
                        } else {

                            //Do Nothing

                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }


    private void initViews() {

        backbtn = (ImageView) findViewById(R.id.backBtn);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backbtn.setEnabled(false);
                finish();
            }
        });
        titleTxt = (TextView) findViewById(R.id.titleTxt);
        title = (RoundImageView) findViewById(R.id.title);
        titleTxt.bringToFront();
        profile_image = (CircleImageView) findViewById(R.id.profile_image);
        editAlias = (TextInputEditText) findViewById(R.id.editAlias);
        editEmailAddress = (TextInputEditText) findViewById(R.id.editEmailAddress);
        errorMessageForEmail = (AppCompatTextView) findViewById(R.id.errorMessageForEmail);
        errorMessageForAlias = (AppCompatTextView) findViewById(R.id.errorMessageForAlias);
//        imageHeader = (ImageView) findViewById(R.id.imageHeader);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        requestQueue = Volley.newRequestQueue(AddFriendScreen.this);
        webService = new WebService();
        helper = Helper.getHelper(AddFriendScreen.this);
        constants = Constants.getConstants(AddFriendScreen.this);
        pd = new Dialog(AddFriendScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        utils = new Utils(AddFriendScreen.this);
        try {
            CryptLib _crypt = new CryptLib();
            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AddFriendScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
            }
        });

        validateBtn = (AppCompatTextView) findViewById(R.id.reviewBtn);

        intent1 = getIntent();

        userName = intent1.getStringExtra("name");
        byteArray = intent1.getByteArrayExtra("image");
        if (byteArray != null)
            bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        else
            bitmap = null;
        phoneNumbers = new ArrayList<>();
        phoneNumbers = intent1.getStringArrayListExtra("phoneNumbers");
        if (phoneNumbers != null && phoneNumbers.size() > 0) {
            mobileNumbers = new String[phoneNumbers.size()];
            mobileNumbers = phoneNumbers.toArray(mobileNumbers);
        }
        email = intent1.getStringExtra("email");

        String s = userName.substring(0, 1);

        switch (s.toUpperCase()) {

            case "A":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "B":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "C":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "D":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "E":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "F":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "G":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "H":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "I":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "J":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "K":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "L":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "M":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "N":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "O":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "P":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "Q":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "R":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "S":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "T":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "U":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "V":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "W":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "X":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "Y":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "Z":
                color = getResources().getColor(R.color.friendColor4);
                break;

            default:
                color = getResources().getColor(R.color.defaultColor);
                break;
        }

        editAlias.setText(userName);
        if (!TextUtils.isEmpty(email) && !email.equals("null"))
            editEmailAddress.setText(email);
        else
            editEmailAddress.setText("N/A");

        if (bitmap != null) {
//            imageHeader.setScaleType(ImageView.ScaleType.CENTER_CROP);
            titleTxt.setVisibility(View.INVISIBLE);
            title.setVisibility(View.INVISIBLE);
            RequestOptions options = new RequestOptions();
            options.centerCrop();
            Glide.with(AddFriendScreen.this)
                    .load((bitmap))
                    .apply(options)
                    .into(profile_image);
        } else {

            profile_image.setVisibility(View.INVISIBLE);
            title.setBackgroundTintList(ColorStateList.valueOf(color));
            titleTxt.setText(s);
        }

        mobileNumbersSpinner = (Spinner) findViewById(R.id.accountTypeSpinner);
        spinnerAdapter mobileNumbersAdapter = new spinnerAdapter(AddFriendScreen.this, R.layout.custom_textview_fp);
        if (mobileNumbers != null && mobileNumbers.length > 0) {
            mobileNumbersAdapter.addAll(mobileNumbers);
            mobileNumbersAdapter.add(" ");
        } else {
            mobileNumbersAdapter.add("Select Mobile Number");
        }
        mobileNumbersSpinner.setAdapter(mobileNumbersAdapter);
        mobileNumbersSpinner.setSelection(0);
        mobileNumbersSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {

                    mobileNumber = mobileNumbersSpinner.getSelectedItem().toString();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    @Override
    public void onBackPressed() {
        backbtn.setEnabled(false);
        finish();
    }
}

