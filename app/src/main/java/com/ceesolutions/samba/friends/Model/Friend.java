package com.ceesolutions.samba.friends.Model;

import proguard.annotation.Keep;

/**
 * Created by ceeayaz on 4/4/18.
 */
@Keep
public class Friend {

    private  String friendID;
    private String userName;
    private String userID;
    private String friendName;
    private String isFav;
    private String userNumber;
    private String firstLetter;
    private String friendNumber;

    public int getFriendColor() {
        return friendColor;
    }

    public void setFriendColor(int friendColor) {
        this.friendColor = friendColor;
    }

    private int friendColor;
    private String friendImage;

    public String getFriendImage() {
        return friendImage;
    }

    public void setFriendImage(String friendImage) {
        this.friendImage = friendImage;
    }




    public String getFriendNumber() {
        return friendNumber;
    }

    public void setFriendNumber(String friendNumber) {
        this.friendNumber = friendNumber;
    }


    public String getFirstLetter() {
        return firstLetter;
    }

    public void setFirstLetter(String firstLetter) {
        this.firstLetter = firstLetter;
    }

    public String getFriendID() {
        return friendID;
    }

    public void setFriendID(String friendID) {
        this.friendID = friendID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getIsFav() {
        return isFav;
    }

    public void setIsFav(String isFav) {
        this.isFav = isFav;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }
}
