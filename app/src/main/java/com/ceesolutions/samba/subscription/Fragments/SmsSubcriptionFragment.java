package com.ceesolutions.samba.subscription.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.SecurityRecommmendationScreen;
import com.ceesolutions.samba.subscription.SmsReviewScreen;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.SwitchCompat;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.CircleImageView;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ceeayaz on 3/9/18.
 */

public class SmsSubcriptionFragment extends Fragment {

    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private CircleImageView profile_image;
    private Dialog pd;
    private String remarks, amount, isFP;
    private EditText editAmount, editRemarks, editShareAccount, editShareAccount1, payabaleAmount;
    private RelativeLayout upperPart1, upperPart2;
    private TextView agreementTxt, errorMessageForAmount, errorMessageForRemarks, errorMessageForSpinner, userNameTxt, accMaskTxt, amountTxt, friendNameTxt, accFrindMaskTxt, validateBtn, reviewBtn;
    private Spinner purposesSpinner;
    private String purposeSelected;
    private String stan, rrn;
    private String sourceBalance, sourceuserName, sourceAccount, friendNumber, destinationName;
    private ArrayList<String> purpStringArrayList;
    private String[] purpose = {
            "School Fees",
            "School Fees",
            "School Fees",
            "School Fees",
            "School Fees"

    };
    private Utils utils;
    private boolean isChecked = true;
    private SwitchCompat switchCompat, switchCompat1;
    private boolean isSource = false;
    private boolean isDestination = false;
    private boolean isFirst = false;
    private String isFav = "SAMBA";
    private String userID, branchCode, email, mobile;
    private TextView doneBtn, textView, heading;
    private Dialog dialog;
    private String friendID, sourceCurrency;
    private String bankCode, flag = "N", flag1 = "N";
    private String isSelectSource, isSelectDestination;
    private ImageView imageBtn;
    private HashMap<String, String> alertMap;
    private boolean isCredit = false, isDebit = false;
    private String credit = "Accounting Cr Txn";
    private String creditCode = "8230";
    private String debit = "Accounting Dr Txn";
    private String debitCode = "8240";
    private Intent intent1;
    private String status1, status2, state1, state2;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.smssubscription, container, false);

        initViews(convertView);


        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

//                Toast.makeText(getActivity(),"Test---"+isChecked,Toast.LENGTH_SHORT).show();
                isDebit = !isDebit;
                if (isChecked) {
                    editShareAccount.setText("Subscribed");
                    flag = "Y";
                } else {
                    editShareAccount.setText("UnSubscribed");
                    flag = "N";
                }
            }
        });

        switchCompat1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

//                Toast.makeText(getActivity(),"Test---"+isChecked,Toast.LENGTH_SHORT).show();
                isCredit = !isCredit;
                if (isChecked) {
                    editShareAccount1.setText("Subscribed");
                    flag1 = "Y";
                } else {
                    editShareAccount1.setText("UnSubscribed");
                    flag1 = "N";
                }
            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAnalytics.getInstance(getContext()).logEvent("Sms_Subscription_Review_Pressed", new Bundle());


                if (!TextUtils.isEmpty(state1) && !TextUtils.isEmpty(state2)) {


                    if (editShareAccount.getText().toString().equals(state2) && editShareAccount1.getText().toString().equals(state1)) {

                        utils.showDilogForError("SMS alert is already " + state1 + " on " + "Credit " + " & " + "Debit alert", "WARNING");
                    } else {
                        Intent intent = new Intent(getActivity(), SmsReviewScreen.class);
                        intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                        intent.putExtra("accountTitle", intent1.getStringExtra("accountTitle"));
                        intent.putExtra("debit", flag);
                        intent.putExtra("credit", flag1);
                        intent.putExtra("iscredit", isCredit);
                        intent.putExtra("isdebit", isDebit);
                        intent.putExtra("branchCode", intent1.getStringExtra("branchCode"));
                        getActivity().overridePendingTransition(0, 0);
                        startActivity(intent);

                    }
                } else {
                    Intent intent = new Intent(getActivity(), SmsReviewScreen.class);
                    intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                    intent.putExtra("accountTitle", intent1.getStringExtra("accountTitle"));
                    intent.putExtra("branchCode", intent1.getStringExtra("branchCode"));
                    intent.putExtra("debit", flag);
                    intent.putExtra("credit", flag1);
                    intent.putExtra("iscredit", isCredit);
                    intent.putExtra("isdebit", isDebit);
                    getActivity().overridePendingTransition(0, 0);
                    startActivity(intent);
                }
            }
        });

        return convertView;
    }

    public void initViews(View convertView) {

        try {

            CryptLib _crypt = new CryptLib();
            intent1 = getActivity().getIntent();
            alertMap = new HashMap<>();
            validateBtn = (TextView) convertView.findViewById(R.id.validateBtn);
            agreementTxt = (TextView) convertView.findViewById(R.id.agreementTxt);
            payabaleAmount = (EditText) convertView.findViewById(R.id.payabaleAmount);
            editShareAccount = (EditText) convertView.findViewById(R.id.editShareAccount);
            editShareAccount.setText("UnSubscribed");
            editShareAccount1 = (EditText) convertView.findViewById(R.id.editShareAccount1);
            editShareAccount1.setText("UnSubscribed");
            imageBtn = (ImageView) convertView.findViewById(R.id.imageBtn);
            switchCompat = (SwitchCompat) convertView.findViewById(R.id.switchCompat);
            switchCompat1 = (SwitchCompat) convertView.findViewById(R.id.switchCompat1);
            requestQueue = Volley.newRequestQueue(getActivity());
//        webService = new WebService();
            helper = Helper.getHelper(getActivity());
            constants = Constants.getConstants(getActivity());
            utils = new Utils(getActivity());
            pd = new Dialog(getActivity());
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            purpStringArrayList = new ArrayList<>();
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            String txt = "<font COLOR=\'#034ea2\'>" + "I agree to accept Samba Bank Limited \n" + "</font>"
                    + "<font COLOR=\'#034ea2\'><u>" + "Terms &amp; Conditions" + "</u></font>";
            agreementTxt.setText(Html.fromHtml(txt));
            imageBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!isChecked) {
                        imageBtn.setBackground(getResources().getDrawable(R.drawable.checked));
                        isChecked = true;
                    } else {
                        imageBtn.setBackground(getResources().getDrawable(R.drawable.normal));
                        isChecked = false;
                    }

                }
            });

            agreementTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    agreementTxt.setEnabled(false);
                    Intent intent = new Intent(getActivity(), SecurityRecommmendationScreen.class);
                    intent.putExtra("Login", "login");
                    startActivity(intent);
                    agreementTxt.setEnabled(true);
//                finish();
                }
            });

            payabaleAmount.setText(_crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));

            String type1 = intent1.getStringExtra("type1");
            String type2 = intent1.getStringExtra("type2");

            try {

                if (!TextUtils.isEmpty(type1) && !type1.equals(null) && !type1.equals("null")) {

                    status1 = type1.split(",")[3];
                    if (status1.toLowerCase().equals("subscribe")) {


                        switchCompat.setChecked(true, false);
                        flag = "Y";
                        editShareAccount.setText("Subscribed");
                        state2 = "Subscribed";

                    } else {

                        switchCompat.setChecked(false, false);
                        flag = "N";
                        editShareAccount.setText("UnSubscribed");
                        state2 = "UnSubscribed";
                    }

                }


                if (!TextUtils.isEmpty(type2) && !type2.equals(null) && !type2.equals("null")) {

                    status2 = type2.split(",")[3];
                    if (status2.toLowerCase().equals("subscribe")) {


                        switchCompat1.setChecked(true, false);
                        flag1 = "Y";
                        editShareAccount1.setText("Subscribed");
                        state1 = "Subscribed";

                    } else {

                        switchCompat1.setChecked(false, false);
                        flag1 = "N";
                        editShareAccount1.setText("UnSubscribed");
                        state1 = "UnSubscribed";
                    }

                }

//        Verification();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//
//            constants = Constants.getConstants(getActivity());
//            utils = new Utils(getActivity());
//            isVisible = true;
//            onResume();
//
//        } else {
//
//            //Do Nothing
//        }
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//        if(isVisible){
//
//            isVisible = false;
//
//        }
//    }
//
//    public void Verification() {
//
//
//        try {
//
//            if (helper.isNetworkAvailable()) {
//
//                pd.show();
//
//                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAlertURL,
//                        new Response.Listener<String>() {
//                            @Override
//                            public void onResponse(String response) {
//                                // response
//
//                                Log.d("Response---", response);
//
//                                try {
//                                    int count = 0;
//                                    JSONObject jsonObject = new JSONObject(response);
//                                    JSONObject JsonObj = new JSONObject();
//                                    if (jsonObject.getJSONObject("Signs_AlertGroupListResult").getString("Status_Code").equals("00")) {
//
//                                        JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_AlertGroupListResult").getString("AlertGroupList"));
//                                        for (int i = 0; i < jsonArray.length(); i++) {
//                                            try {
//
//                                                JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
//
//                                                purpStringArrayList.add(jsonObj.getString("Description"));
//                                                if (count == 0) {
//                                                    editShareAccount.setHint(jsonObj.getString("Description"));
//                                                    count++;
//                                                } else {
//
//                                                    editShareAccount1.setHint(jsonObj.getString("Description"));
//                                                    count = 0;
//                                                }
//                                                alertMap.put(jsonObj.getString("Description"), jsonObj.getString("ID"));
//
//                                            } catch (Exception ex) {
//                                                ex.printStackTrace();
//                                                pd.dismiss();
//                                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//                                            }
//
//                                        }
//                                        pd.dismiss();
//
//
//                                    } else {
//                                        pd.dismiss();
//                                        utils.showDilogForError(jsonObject.getJSONObject("Signs_AlertGroupListResult").getString("Status_Description"), "ERROR");
//
//                                    }
//
//
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//                                }
//
//                            }
//                        },
//                        new Response.ErrorListener() {
//                            @Override
//                            public void onErrorResponse(VolleyError error) {
//                                // error
//                                Log.d("Error.Response", "---" + error.getMessage());
//                                try {
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//                                }
//                            }
//                        }
//                ) {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceLatitude", "0");
//                        params.put("sDeviceLongitude", "0");
//                        return params;
//                    }
//                };
//
//
//                requestQueue.add(postRequest);
//                postRequest.setRetryPolicy(new RetryPolicy() {
//                    @Override
//                    public int getCurrentTimeout() {
//                        return 50000;
//                    }
//
//                    @Override
//                    public int getCurrentRetryCount() {
//                        return 50000;
//                    }
//
//                    @Override
//                    public void retry(VolleyError error) throws VolleyError {
//
//                    }
//                });
//            } else {
////                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
//                pd.dismiss();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
//
//
//            }
//        } catch (Exception e) {
//
////            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();
//
//            pd.dismiss();
//            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//        }
//    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public void showDilogForErrorForMenu(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(getActivity(), HomeScreen.class);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).startActivity(intent);
                ((Activity) getActivity()).finish();
                ((Activity) getActivity()).finishAffinity();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


}