package com.ceesolutions.samba.subscription;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.BuildConfig;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import co.infinum.goldfinger.Error;
import co.infinum.goldfinger.Goldfinger;
import co.infinum.goldfinger.Warning;

/**
 * Created by ceeayaz on 4/17/18.
 */

public class PinVerificationScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private TextView second_dialog_button, fingerStatus, hintTxt, message, heading, doneBtn, textView, submitBtn, regenerateBtn, accMaskTxt, friendNameTxt, accFrindMaskTxt, totalAmountTxt, amountInWordsTxt, reviewBtn, timeTxt, dateTxt;
    private EditText editiPin;
    private ImageView fingerBtn, fingerPrintIcon;
    private Dialog dialog, dialog1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private String iPIN;
    private Intent intent1;
    private ImageView backBtn;
    private String changePin;
    private String password = "password";
    private Goldfinger goldfinger;
    private String credit = "Accounting Cr Txn";
    private String creditCode = "8230";
    private String debit = "Accounting Dr Txn";
    private String debitCode = "8240";
    private AppPreferences appPreferences;
    private String location;
    private List<Address> addresses;
    private boolean isCredit, isDebit;
    private boolean isFinger = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_verification);
        initViews();
        getSupportActionBar().hide();
        String isFinger = constants.sharedPreferences.getString("Finger", "N/A");
        if (isFinger.equals("True")) {


        } else if (isFinger.equals("False")) {

            fingerBtn.setVisibility(View.INVISIBLE);
            hintTxt.setText("Authenticate with iPIN");
        }
        goldfinger = new Goldfinger.Builder(this).setLogEnabled(BuildConfig.DEBUG).build();
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getIntent().hasExtra("change")) {

                    if (getIntent().getStringExtra("change").equalsIgnoreCase("estatement")) {
                        FirebaseAnalytics.getInstance(PinVerificationScreen.this).logEvent("EStatement_Subscription_IPin_Submitted", new Bundle());
                    } else if (getIntent().getStringExtra("change").equalsIgnoreCase("sms")) {
                        FirebaseAnalytics.getInstance(PinVerificationScreen.this).logEvent("Sms_Subscription_IPin_Submitted", new Bundle());
                    }
                }


                iPIN = editiPin.getText().toString().trim();
                if (TextUtils.isEmpty(iPIN) || iPIN.length() < 5)
                    utils.showDilogForError("Please enter valid iPIN", "WARNING");
                else {
                    submitBtn.setEnabled(false);
                    if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                        longitude = Double.valueOf(longitude1);
                        latitude = Double.valueOf(latitude1);
                        location = getAddress("ipin");
//                        if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null)) {
//
//                            SubmitIPINNew();
//
//                        } else {
//                            location = "-";
//                            SubmitIPINNew();
////                utils.showDilogForError("Please turn on location services.", "WARNING");
////                validateBtn.setEnabled(true);
//                        }
                    } else {

                        longitude = 0;
                        latitude = 0;
                        location = "-";
                        SubmitIPINNew();
//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
                    }

                }
            }
        });

        regenerateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                regenerateBtn.setEnabled(false);
                if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                    longitude = Double.valueOf(longitude1);
                    latitude = Double.valueOf(latitude1);
                    location = getAddress("regenerate");
//                    if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null)) {
//
//                        RegenerateiPin();
//
//                    } else {
//                        location = "-";
//                        RegenerateiPin();
////                utils.showDilogForError("Please turn on location services.", "WARNING");
////                validateBtn.setEnabled(true);
//                    }
                } else {

                    longitude = 0;
                    latitude = 0;
                    location = "-";
                    RegenerateiPin();
//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
                }
            }
        });

        fingerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showDilogForFinger();
            }
        });

        editiPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                iPIN = s.toString();
            }
        });
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

//        fingerPrintIcon.setEnabled(goldfinger.hasEnrolledFingerprint());

        if (goldfinger.hasFingerprintHardware()
                && goldfinger.hasEnrolledFingerprint()) {
//            finger.setVisibility(View.GONE);
        } else {
            fingerBtn.setVisibility(View.INVISIBLE);
            hintTxt.setText("Authenticate with iPIN");
//            statusView.setText(getString(R.string.fingerprint_not_available));
//            statusView.setTextColor(ContextCompat.getColor(this, R.color.error));
        }
    }

    public void initViews() {

        intent1 = getIntent();
        changePin = intent1.getStringExtra("change");

        if (!TextUtils.isEmpty(changePin) && changePin.equals("sms")) {


        } else
            changePin = "estatement";

        String userName = intent1.getStringExtra("userName");
        isCredit = intent1.getBooleanExtra("iscredit", false);
        isDebit = intent1.getBooleanExtra("isdebit", false);
        editiPin = (EditText) findViewById(R.id.editiPin);
        submitBtn = (TextView) findViewById(R.id.submitBtn);
        regenerateBtn = (TextView) findViewById(R.id.regenerateBtn);
        fingerBtn = (ImageView) findViewById(R.id.fingerBtn);
        hintTxt = (TextView) findViewById(R.id.hintTxt);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(PinVerificationScreen.this);
        webService = new WebService();
        helper = Helper.getHelper(this);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        utils = new Utils(PinVerificationScreen.this);
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
    }


    public void SubmitIPINNew() {

        try {

            if (helper.isNetworkAvailable()) {
                dialog.show();
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String userName = user;
                final String md5PasswordAndPin = helper.md5(iPIN + userName.toUpperCase());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);

                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Username", userName);
                params.put("sPinData", md5PasswordAndPin);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sLocationName", location);
                params.put("sServerSessionKey", md5);


                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());


                HttpsTrustManager.allowMySSL(PinVerificationScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.ipinVerificationURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_ValidateOTPResult").getString("Status_Code").equals("00")) {
                                            dialog.dismiss();
                                            appPreferences.putString("location", location);
                                            if (!TextUtils.isEmpty(changePin) && changePin.equals("sms")) {



                                                if (isCredit) {
                                                    ChangeCreditStatus();
                                                } else if (isDebit) {
                                                    ChangeDebitStatus();
                                                }


                                            } else {


                                                StatusChange();
                                            }

                                        } else if (jsonObject.getJSONObject("Signs_ValidateOTPResult").getString("Status_Code").equals("54")) {
                                            dialog.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ValidateOTPResult").getString("Status_Description"), "ERROR", PinVerificationScreen.this, new LoginScreen());
                                            submitBtn.setEnabled(true);
                                            clearViews();

                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_ValidateOTPResult").getString("Status_Description"), "ERROR");
                                            submitBtn.setEnabled(true);
                                            clearViews();

                                        }
                                    } else {
                                        dialog.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        submitBtn.setEnabled(true);
                                        clearViews();

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    FirebaseAnalytics.getInstance(PinVerificationScreen.this).logEvent("Error", new Bundle());
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    submitBtn.setEnabled(true);
                                    clearViews();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    submitBtn.setEnabled(true);
                                    clearViews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    submitBtn.setEnabled(true);
                                    clearViews();
                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", userName);
//                        params.put("sPinData", md5PasswordAndPin);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLocationName", location);
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                dialog.dismiss();
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");

                submitBtn.setEnabled(true);

                clearViews();

            }
        } catch (Exception e) {

            submitBtn.setEnabled(true);
            dialog.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            clearViews();

        }
    }

    public void clearViews() {

        editiPin.getText().clear();
    }

    public void RegenerateiPin() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                dialog.show();
                //                webService.RegeneratePin(constants.sharedPreferences.getString("userName", "N/A"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getRegeneratePinReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//                                pd.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "Invalid Username/iPIN!", Toast.LENGTH_LONG).show();
//                                utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
//                                regenerateBtn.setEnabled(true);
//
//
//                            } else {
//                                pd.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "iPIN has been sent on your registered mobile and email address.", Toast.LENGTH_LONG).show();
//
//                                utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
//                                regenerateBtn.setEnabled(true);
//
//                            }
//                        } else {
//                            pd.dismiss();
//                            utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
//                            regenerateBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 5000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);

                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sLocationName", location);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Username", user);
                params.put("sServerSessionKey", md5);
                params.put("sOptionalIpinGenerater", "Y");


                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());


                HttpsTrustManager.allowMySSL(PinVerificationScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("00")) {
                                            {
                                                dialog.dismiss();
                                                utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                                regenerateBtn.setEnabled(true);
                                                clearViews();
                                            }
                                        } else if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("54")) {
                                            dialog.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR", PinVerificationScreen.this, new LoginScreen());
                                            regenerateBtn.setEnabled(true);
                                            clearViews();

                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                            regenerateBtn.setEnabled(true);
                                            clearViews();
                                        }
                                    } else {
                                        dialog.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        regenerateBtn.setEnabled(true);
                                        clearViews();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                    regenerateBtn.setEnabled(true);
                                    clearViews();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    dialog.dismiss();
                                    utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                    regenerateBtn.setEnabled(true);
                                    clearViews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                    regenerateBtn.setEnabled(true);
                                    clearViews();
                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLocationName", location);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sServerSessionKey", md5);
//                        params.put("sOptionalIpinGenerater", "Y");
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                regenerateBtn.setEnabled(true);
                clearViews();


            }
        } catch (Exception e) {
            dialog.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");
            regenerateBtn.setEnabled(true);
            clearViews();


        }
    }

    public void StatusChange() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    dialog.show();
                    submitBtn.setEnabled(false);
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sLocationName", location);
                    params.put("sAccountNumber", intent1.getStringExtra("accountNumber"));
                    params.put("sCIF", _crypt.decrypt(constants.sharedPreferences.getString("T24_CIF", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));//intent1.getStringExtra("code")
                    params.put("IsSubscribe", intent1.getStringExtra("status"));
                    params.put("sT24AlertID", intent1.getStringExtra("value"));
                    params.put("sT24AlertDescription", intent1.getStringExtra("purposeSelected"));
                    params.put("sBranchCode", intent1.getStringExtra("branchCode"));
                    params.put("sServerSessionKey", md5);


                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());


                    HttpsTrustManager.allowMySSL(PinVerificationScreen.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.subsEmailURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_SubscribeEmailAlertResult").getString("Status_Code").equals("00")) {


                                                dialog.dismiss();
                                                Intent intent = new Intent(PinVerificationScreen.this, EstatementConfirmationScreen.class);
                                                intent.putExtra("userName", intent1.getStringExtra("userName"));
                                                intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                                                intent.putExtra("status", intent1.getStringExtra("status"));
                                                intent.putExtra("purposeSelected", intent1.getStringExtra("purposeSelected"));
                                                intent.putExtra("currentDate", intent1.getStringExtra("currentDate"));
                                                intent.putExtra("currentTime", intent1.getStringExtra("currentTime"));
                                                intent.putExtra("branchCode", intent1.getStringExtra("branchCode"));
                                                overridePendingTransition(0, 0);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                                finish();
                                                finishAffinity();
                                                submitBtn.setEnabled(true);
                                                submitBtn.setEnabled(true);

                                            } else if (jsonObject.getJSONObject("Signs_SubscribeEmailAlertResult").getString("Status_Code").equals("54")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_SubscribeEmailAlertResult").getString("Status_Description"), "ERROR", PinVerificationScreen.this, new LoginScreen());
                                                submitBtn.setEnabled(true);

                                            } else {

                                                dialog.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_SubscribeEmailAlertResult").getString("Status_Description"), "ERROR");
                                                submitBtn.setEnabled(true);
                                            }
                                        } else {

                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            submitBtn.setEnabled(true);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);
                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sLocationName", location);
//                            params.put("sAccountNumber", intent1.getStringExtra("accountNumber"));
//                            params.put("sCIF", constants.sharedPreferences.getString("T24_CIF", "N/A"));//intent1.getStringExtra("code")
//                            params.put("IsSubscribe", intent1.getStringExtra("status"));
//                            params.put("sT24AlertID", intent1.getStringExtra("value"));
//                            params.put("sT24AlertDescription", intent1.getStringExtra("purposeSelected"));
//                            params.put("sBranchCode", intent1.getStringExtra("branchCode"));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    submitBtn.setEnabled(true);
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "ERROR");
                submitBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            dialog.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            submitBtn.setEnabled(true);

        }
    }


    public void ChangeCreditStatus() {

        try {


            if (helper.isNetworkAvailable()) {
                try {

                    dialog.show();
                    submitBtn.setEnabled(false);
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sLocationName", location);
                    params.put("sAccountNumber", intent1.getStringExtra("accountNumber"));
                    params.put("sCIF", _crypt.decrypt(constants.sharedPreferences.getString("T24_CIF", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));//intent1.getStringExtra("code")
                    params.put("IsSubscribe", intent1.getStringExtra("credit"));
                    params.put("sT24AlertID", creditCode);
                    params.put("sT24AlertDescription", credit);
                    params.put("sBranchCode", intent1.getStringExtra("branchCode"));
                    params.put("sServerSessionKey", md5);


                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                    HttpsTrustManager.allowMySSL(PinVerificationScreen.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.subsSmsURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_SubscribeSmsAlertResult").getString("Status_Code").equals("00")) {


                                                dialog.dismiss();
                                                if (isDebit)
                                                    ChangeDebitStatus();
                                                else {
                                                    Intent intent = new Intent(PinVerificationScreen.this, SmsConfirmationScreen.class);
                                                    intent.putExtra("userName", intent1.getStringExtra("userName"));
                                                    intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                                                    intent.putExtra("debit", intent1.getStringExtra("debit"));
                                                    intent.putExtra("credit", intent1.getStringExtra("credit"));
                                                    intent.putExtra("currentDate", intent1.getStringExtra("currentDate"));
                                                    intent.putExtra("currentTime", intent1.getStringExtra("currentTime"));
                                                    overridePendingTransition(0, 0);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    startActivity(intent);
                                                    finish();
                                                    finishAffinity();
                                                    submitBtn.setEnabled(true);
                                                }

                                            } else if (jsonObject.getJSONObject("Signs_SubscribeSmsAlertResult").getString("Status_Code").equals("54")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_SubscribeSmsAlertResult").getString("Status_Description"), "ERROR", PinVerificationScreen.this, new LoginScreen());
                                                submitBtn.setEnabled(true);

                                            } else {

                                                dialog.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_SubscribeSmsAlertResult").getString("Status_Description"), "ERROR");
                                                submitBtn.setEnabled(true);
                                            }
                                        } else {

                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            submitBtn.setEnabled(true);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);
                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sLocationName", location);
//                            params.put("sAccountNumber", intent1.getStringExtra("accountNumber"));
//                            params.put("sCIF", constants.sharedPreferences.getString("T24_CIF", "N/A"));//intent1.getStringExtra("code")
//                            params.put("IsSubscribe", intent1.getStringExtra("credit"));
//                            params.put("sT24AlertID", creditCode);
//                            params.put("sT24AlertDescription", credit);
//                            params.put("sBranchCode", intent1.getStringExtra("branchCode"));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    submitBtn.setEnabled(true);
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "ERROR");
                submitBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            dialog.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            submitBtn.setEnabled(true);

        }
    }

    public void ChangeDebitStatus() {

        try {


            if (helper.isNetworkAvailable()) {
                try {

                    dialog.show();
                    submitBtn.setEnabled(false);
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sLocationName", location);
                    params.put("sAccountNumber", intent1.getStringExtra("accountNumber"));
                    params.put("sCIF", _crypt.decrypt(constants.sharedPreferences.getString("T24_CIF", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));//intent1.getStringExtra("code")
                    params.put("IsSubscribe", intent1.getStringExtra("debit"));
                    params.put("sT24AlertID", debitCode);
                    params.put("sT24AlertDescription", debit);
                    params.put("sBranchCode", intent1.getStringExtra("branchCode"));
                    params.put("sServerSessionKey", md5);


                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());


                    HttpsTrustManager.allowMySSL(PinVerificationScreen.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.subsSmsURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_SubscribeSmsAlertResult").getString("Status_Code").equals("00")) {


                                                dialog.dismiss();
                                                Intent intent = new Intent(PinVerificationScreen.this, SmsConfirmationScreen.class);
                                                intent.putExtra("userName", intent1.getStringExtra("userName"));
                                                intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                                                intent.putExtra("debit", intent1.getStringExtra("debit"));
                                                intent.putExtra("credit", intent1.getStringExtra("credit"));
                                                intent.putExtra("currentDate", intent1.getStringExtra("currentDate"));
                                                intent.putExtra("currentTime", intent1.getStringExtra("currentTime"));
                                                overridePendingTransition(0, 0);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                                finish();
                                                finishAffinity();
                                                submitBtn.setEnabled(true);

                                            } else if (jsonObject.getJSONObject("Signs_SubscribeSmsAlertResult").getString("Status_Code").equals("54")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_SubscribeSmsAlertResult").getString("Status_Description"), "ERROR", PinVerificationScreen.this, new LoginScreen());
                                                submitBtn.setEnabled(true);

                                            } else {

                                                dialog.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_SubscribeSmsAlertResult").getString("Status_Description"), "ERROR");
                                                submitBtn.setEnabled(true);
                                            }
                                        } else {

                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            submitBtn.setEnabled(true);
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);
                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sLocationName", location);
//                            params.put("sAccountNumber", intent1.getStringExtra("accountNumber"));
//                            params.put("sCIF", constants.sharedPreferences.getString("T24_CIF", "N/A"));//intent1.getStringExtra("code")
//                            params.put("IsSubscribe", intent1.getStringExtra("debit"));
//                            params.put("sT24AlertID", debitCode);
//                            params.put("sT24AlertDescription", debit);
//                            params.put("sBranchCode", intent1.getStringExtra("branchCode"));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };
                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    submitBtn.setEnabled(true);
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "ERROR");
                submitBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            dialog.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            submitBtn.setEnabled(true);

        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        finish();

//        showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");
        finish();
    }

    public void showDilogForErrorForLogin(String msg, String header) {

        dialog = new Dialog(PinVerificationScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
//                Intent intent = new Intent(PinVerificationScreen.this, FundTransferScreen.class);
//                overridePendingTransition(0, 0);
//                startActivity(intent);
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();


    }

    public void AuthenticateFingerprint() {

        try {

            if (!isFinger) {
                if (helper.isNetworkAvailable()) {
                    dialog.show();
                    isFinger = true;
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String userName = user;
                    final String md5PasswordAndPin = helper.md5(iPIN + userName.toUpperCase());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sSigns_Username", userName);
                    params.put("sSecureFlag", "F");
                    params.put("sSecureVerificationData", md5PasswordAndPin);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sLocationName", location);
                    params.put("sServerSessionKey", md5);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                    HttpsTrustManager.allowMySSL(PinVerificationScreen.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.ipinFingerVerification,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);
                                    isFinger = false;
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ValidateOTP_FingerPrintResult").getString("Status_Code").equals("00")) {
                                                dialog.dismiss();
                                                if (!TextUtils.isEmpty(changePin) && changePin.equals("sms")) {


                                                    ChangeCreditStatus();

                                                } else
                                                    StatusChange();

                                            } else if (jsonObject.getJSONObject("Signs_ValidateOTP_FingerPrintResult").getString("Status_Code").equals("54")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ValidateOTP_FingerPrintResult").getString("Status_Description"), "ERROR", PinVerificationScreen.this, new LoginScreen());
                                                submitBtn.setEnabled(true);
                                                clearViews();


                                            } else {
                                                dialog.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_ValidateOTP_FingerPrintResult").getString("Status_Description"), "ERROR");
                                                submitBtn.setEnabled(true);
                                                clearViews();

                                            }
                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            submitBtn.setEnabled(true);
                                            clearViews();

                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        isFinger = false;
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);
                                        clearViews();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    isFinger = false;
                                    try {
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);
                                        clearViews();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);
                                        clearViews();
                                    }
                                }
                            }
                    )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", userName);
//                        params.put("sSecureFlag", "F");
//                        params.put("sSecureVerificationData", md5PasswordAndPin);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLocationName", location);
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };


                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } else {
                    dialog.dismiss();
                    isFinger = false;
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                    utils.showDilogForError("Please check your internet connectivity.", "WARNING");

                    submitBtn.setEnabled(true);

                    clearViews();

                }
            } else {
                //Think and write
            }
        } catch (Exception e) {
            isFinger = false;
            submitBtn.setEnabled(true);
            dialog.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            clearViews();

        }
    }

    public void showDilogForFinger() {

        dialog1 = new Dialog(PinVerificationScreen.this);

        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog1.setContentView(R.layout.finger_dialog);
//        View view = dialog.findViewById(R.id.content);
        fingerPrintIcon = (ImageView) dialog1.findViewById(R.id.fingerprint_icon);
        fingerStatus = (TextView) dialog1.findViewById(R.id.validatingTxt);
        second_dialog_button = (TextView) dialog1.findViewById(R.id.doneBtn);

        second_dialog_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
            }
        });
//        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
//        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
//        heading = (TextView) dialog.findViewById(R.id.heading);
//        dialog.setCancelable(false);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        doneBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                dialog.dismiss();
//                ForgotPassword();
//            }
//        });
//        textView.setText(msg);
//        heading.setText(header);
        dialog1.show();

        resetStatusText();
        authenticateUserFingerprint();
    }

    private void resetStatusText() {
//        fingerStatus.setTextColor(ContextCompat.getColor(this, R.color.notiheader));
//        fingerStatus.setText(getString(R.string.authenticate_user));
    }

    private void authenticateUserFingerprint() {
        goldfinger.authenticate(new Goldfinger.Callback() {
            @Override
            public void onSuccess(String value) {
                onSuccessResult(value);
            }

            @Override
            public void onWarning(Warning warning) {
                onWarningResult(warning);
            }

            @Override
            public void onError(Error error) {
                onErrorResult(error);
            }
        });
    }

    private void onSuccessResult(String value) {
        onResult("Authenticated Successfully", value);
        fingerPrintIcon.setBackgroundResource(R.drawable.fingerprint_circle_blue);

        dialog1.dismiss();
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            location = getAddress("fingerprint");
//            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null)) {
//
//                AuthenticateFingerprint();
//
//            } else {
//                location = "-";
//                AuthenticateFingerprint();
////                utils.showDilogForError("Please turn on location services.", "WARNING");
////                validateBtn.setEnabled(true);
//            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";
            AuthenticateFingerprint();
//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }


//        fingerStatus.setTextColor(ContextCompat.getColor(this, R.color.notiheader));
    }

    private void onErrorResult(Error error) {
        onResult("Authentication Failure", error.toString());
        fingerPrintIcon.setBackgroundResource(R.drawable.fingerprint_circle_red);
//        fingerStatus.setTextColor(ContextCompat.getColor(this, R.color.tabunderline));
    }

    private void onWarningResult(Warning warning) {
        onResult("Authentication Failure", warning.toString());
        fingerPrintIcon.setBackgroundResource(R.drawable.fingerprint_circle_red);
//        fingerStatus.setTextColor(ContextCompat.getColor(this, R.color.tabunderline));
    }

    private void onResult(String methodName, String value) {
//        fingerStatus.setText(String.format(Locale.US, "%s", methodName, ""));
    }


    public Address getAddress(final double latitude, final double longitude, final String value) {
        final Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {


                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    if (addresses != null) {
                        if (!TextUtils.isEmpty(addresses.get(0).toString()) && !addresses.get(0).equals("null") && !addresses.get(0).equals(null)) {

                            Address locationAddress = addresses.get(0);
                            location = locationAddress.getAddressLine(0);

                            if (value.equals("ipin")) {
                                SubmitIPINNew();
                            } else if (value.equals("regenerate")) {
                                RegenerateiPin();
                            } else if (value.equals("fingerprint")) {
                                AuthenticateFingerprint();
                            }
                        } else {
                            if (value.equals("ipin")) {
                                location = "-";
                                SubmitIPINNew();
                            } else if (value.equals("regenerate")) {

                                location = "-";
                                RegenerateiPin();
                            } else if (value.equals("fingerprint")) {

                                location = "-";
                                AuthenticateFingerprint();
                            }

                        }
                    } else {

                        if (value.equals("ipin")) {
                            location = "-";
                            SubmitIPINNew();
                        } else if (value.equals("regenerate")) {

                            location = "-";
                            RegenerateiPin();
                        } else if (value.equals("fingerprint")) {

                            location = "-";
                            AuthenticateFingerprint();
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    if (value.equals("ipin")) {
                        location = "-";
                        SubmitIPINNew();
                    } else if (value.equals("regenerate")) {

                        location = "-";
                        RegenerateiPin();
                    } else if (value.equals("fingerprint")) {

                        location = "-";
                        AuthenticateFingerprint();
                    }
                }


            }
        });

        if (addresses != null) {
            return addresses.get(0);
        } else
            return null;
    }

    public String getAddress(String login) {

        Address locationAddress = getAddress(latitude, longitude, login);
        String address = "";
        if (locationAddress != null) {
            address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();

            String currentLocation;

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address;

                if (!TextUtils.isEmpty(address1))
                    currentLocation += "\n" + address1;

                if (!TextUtils.isEmpty(city)) {
                    currentLocation += "\n" + city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += " - " + postalCode;
                } else {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += "\n" + postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation += "\n" + state;

                if (!TextUtils.isEmpty(country))
                    currentLocation += "\n" + country;


            }

        }

        return address;

    }
}
