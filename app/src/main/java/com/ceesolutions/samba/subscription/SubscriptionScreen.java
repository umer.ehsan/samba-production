package com.ceesolutions.samba.subscription;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ceesolutions.samba.R;

import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.subscription.Adapter.SubscriptionAdapter;

/**
 * Created by ceeayaz on 5/19/18.
 */

public class SubscriptionScreen extends AppCompatActivity {

    private ImageView backBtn, notificationBtn;
    public static ViewPager viewPager;
    private TextView headingTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subs);
        getSupportActionBar().hide();
//        TextView textView = (TextView) findViewById(R.id.text);
//        textView.bringToFront();

        backBtn = (ImageView) findViewById(R.id.backBtn);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        headingTxt = (TextView) findViewById(R.id.headingTxt);
        headingTxt.setText("Account Subscription");
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent(PayBillsScreen.this, HomeScreen.class);
//                overridePendingTransition(0, 0);
//                startActivity(intent);
                finish();
            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubscriptionScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
            }
        });
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("E-Statement Subscription"));
        tabLayout.addTab(tabLayout.newTab().setText("SMS Subscription"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        Intent intent = getIntent();
        viewPager = (ViewPager) findViewById(R.id.pager);
        final SubscriptionAdapter adapter = new SubscriptionAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        String request = intent.getStringExtra("requestbill");
//        if (!TextUtils.isEmpty(request)) {
//            if (intent.getStringExtra("requestbill").equals("requestbill")) {
//                viewPager.setCurrentItem(0);
//            } else if (intent.getStringExtra("requestbill").equals("requestvoucher")) {
//                viewPager.setCurrentItem(1);
//            } else if (intent.getStringExtra("requestbill").equals("bill")) {
//                viewPager.setCurrentItem(0);
//            }
//            else if (intent.getStringExtra("requestbill").equals("nonSamba")) {
//                viewPager.setCurrentItem(2);
//            }
//        }


    }

    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(PayBillsScreen.this, Hom.class);
//        overridePendingTransition(0, 0);
//        startActivity(intent);
        finish();
    }

}