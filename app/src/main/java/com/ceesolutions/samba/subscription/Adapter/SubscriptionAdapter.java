package com.ceesolutions.samba.subscription.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ceesolutions.samba.subscription.Fragments.EStatmentSubscriptionFragment;
import com.ceesolutions.samba.subscription.Fragments.SmsSubcriptionFragment;

/**
 * Created by ceeayaz on 3/9/18.
 */

public class SubscriptionAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public SubscriptionAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                EStatmentSubscriptionFragment eStatmentSubscriptionFragment = new EStatmentSubscriptionFragment();
                return eStatmentSubscriptionFragment;
            case 1:
                SmsSubcriptionFragment smsSubcriptionFragment = new SmsSubcriptionFragment();
                return smsSubcriptionFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}