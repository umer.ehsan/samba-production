package com.ceesolutions.samba.subscription.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.accessControl.SecurityRecommmendationScreen;
import com.ceesolutions.samba.subscription.EstatementReviewScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.SwitchCompat;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.ceesolutions.samba.views.CircleImageView;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 3/9/18.
 */

public class EStatmentSubscriptionFragment extends Fragment {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    public String freqs;
    boolean flag1 = false;
    int count = 0;
    int switchCount = 0;
    String state;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private CircleImageView profile_image;
    private Dialog pd;
    private String remarks, amount, isFP;
    private EditText editAmount, editRemarks, editShareAccount, payabaleAmount;
    private RelativeLayout upperPart1, upperPart2;
    private TextView agreementTxt, errorMessageForAmount, errorMessageForRemarks, errorMessageForSpinner, userNameTxt, accMaskTxt, amountTxt, friendNameTxt, accFrindMaskTxt, validateBtn, reviewBtn;
    private Spinner purposesSpinner;
    private String purposeSelected;
    private String stan, rrn;
    private String sourceBalance, sourceuserName, sourceAccount, friendNumber, destinationName;
    private ArrayList<String> purpStringArrayList;
    private String[] purpose = {
            "Monthly",
            "Weekly",
            "Yearly"


    };
    private boolean isChecked = true;
    private Utils utils;
    private SwitchCompat switchCompat;
    private boolean isSource = false;
    private boolean isDestination = false;
    private boolean isFirst = false;
    private String isFav = "SAMBA";
    private String userID, branchCode, email, mobile;
    private TextView doneBtn, textView, heading, errorMessage;
    private Dialog dialog;
    private String friendID, sourceCurrency;
    private String bankCode, flag = "N";
    private String isSelectSource, isSelectDestination;
    private ImageView imageBtn;
    private Intent intent1;
    private HashMap<String, String> freqHashMap;
    private String frequency, frequencySec;
    private AppPreferences appPreferences;
    private String location;
    private boolean changeCompact = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.estatement, container, false);

        initViews(convertView);


        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

//                Toast.makeText(getActivity(),"Test---"+isChecked,Toast.LENGTH_SHORT).show();
                changeCompact = !changeCompact;
                if (isChecked) {
                    editShareAccount.setText("Subscribed");
                    flag = "Y";
                    if (purposeSelected.equals(frequency)) {
                        if (count == 2) {

                            switchCount = 1;
                        }
                    }

                } else {
                    editShareAccount.setText("UnSubscribed");
                    flag = "N";
                    if (purposeSelected.equals(frequency)) {
                        if (count == 1) {

                            switchCount = 2;

                        }
                    }
                }
            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateBtn.setEnabled(false);

                FirebaseAnalytics.getInstance(getContext()).logEvent("EStatement_Subscription_Review_Pressed", new Bundle());

                if (isChecked) {

                    if (changeCompact) {

                        if (!TextUtils.isEmpty(frequency)) {


                            if (purposeSelected.equals(frequency)) {

                                if (editShareAccount.getText().toString().equals(state)) {

                                    utils.showDilogForError("You have already " + state + " " + frequency + " frequency.", "WARNING");
                                    validateBtn.setEnabled(true);

                                } else {

                                    String value = freqHashMap.get(purposeSelected);
                                    Intent intent = new Intent(getActivity(), EstatementReviewScreen.class);
                                    intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                                    intent.putExtra("accountTitle", intent1.getStringExtra("accountTitle"));
                                    intent.putExtra("branchCode", intent1.getStringExtra("branchCode"));
                                    intent.putExtra("status", flag);
                                    intent.putExtra("value", value);
                                    intent.putExtra("freqs", freqs);
                                    intent.putExtra("purposeSelected", purposeSelected);
                                    getActivity().overridePendingTransition(0, 0);
                                    startActivity(intent);
                                    validateBtn.setEnabled(true);
                                }
//                        if (count == 1 && flag1 == true && switchCount == 2 ) {
//
//
//                        }
//
//                        if (count == 2 && flag1 == true && switchCount == 1) {
//
//
//                        }

                            } else {
                                String value = freqHashMap.get(purposeSelected);
                                Intent intent = new Intent(getActivity(), EstatementReviewScreen.class);
                                intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                                intent.putExtra("accountTitle", intent1.getStringExtra("accountTitle"));
                                intent.putExtra("branchCode", intent1.getStringExtra("branchCode"));
                                intent.putExtra("status", flag);
                                intent.putExtra("value", value);
                                intent.putExtra("freqs", freqs);
                                intent.putExtra("purposeSelected", purposeSelected);
                                getActivity().overridePendingTransition(0, 0);
                                startActivity(intent);
                                validateBtn.setEnabled(true);
                            }

                        } else {
                            String value = freqHashMap.get(purposeSelected);
                            Intent intent = new Intent(getActivity(), EstatementReviewScreen.class);
                            intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                            intent.putExtra("accountTitle", intent1.getStringExtra("accountTitle"));
                            intent.putExtra("branchCode", intent1.getStringExtra("branchCode"));
                            intent.putExtra("status", flag);
                            intent.putExtra("value", value);
                            intent.putExtra("freqs", freqs);
                            intent.putExtra("purposeSelected", purposeSelected);
                            getActivity().overridePendingTransition(0, 0);
                            startActivity(intent);
                            validateBtn.setEnabled(true);
                        }

                    } else {

                        if (!TextUtils.isEmpty(flag) && !flag.equals("N/A")) {


                            if (flag.equals("N")) {

                                utils.showDilogForError("E-Statement is already " + "UnSubscribed on" + " " + purposeSelected + " frequency.", "WARNING");
                                validateBtn.setEnabled(true);

                            } else {

                                utils.showDilogForError("E-Statement is already " + "Subscribed on" + " " + purposeSelected + " frequency.", "WARNING");
                                validateBtn.setEnabled(true);
                            }
                        }
                    }
                } else {

                    utils.showDilogForError("Please accept Terms & Conditions", "WARNING");
                    validateBtn.setEnabled(true);
                }

            }
        });

        return convertView;
    }

    public void initViews(View convertView) {

        try {

            CryptLib _crypt = new CryptLib();
            intent1 = getActivity().getIntent();
            freqHashMap = new HashMap<>();
//        profile_image = (CircleImageView) convertView.findViewById(R.id.profile_image);
            purposesSpinner = (Spinner) convertView.findViewById(R.id.purposeSpinner);
            validateBtn = (TextView) convertView.findViewById(R.id.validateBtn);
            payabaleAmount = (EditText) convertView.findViewById(R.id.payabaleAmount);
            agreementTxt = (TextView) convertView.findViewById(R.id.agreementTxt);
            errorMessage = (TextView) convertView.findViewById(R.id.errorMessage);
            imageBtn = (ImageView) convertView.findViewById(R.id.imageBtn);
//        upperPart1 = (RelativeLayout) convertView.findViewById(R.id.upperPart1);
//        upperPart2 = (RelativeLayout) convertView.findViewById(R.id.upperPart2);
//        friendNameTxt = (TextView) convertView.findViewById(R.id.friendNameTxt);
//        friendNameTxt.setText("Select Destination Account ");
//        userNameTxt = (TextView) convertView.findViewById(R.id.userNameTxt);
//        accMaskTxt = (TextView) convertView.findViewById(R.id.accMaskTxt);
//        amountTxt = (TextView) convertView.findViewById(R.id.amountTxt);
//        accFrindMaskTxt = (TextView) convertView.findViewById(R.id.accFrindMaskTxt);
//        accFrindMaskTxt.setVisibility(View.INVISIBLE);
//        userNameTxt.setText("Select Friend");
//        errorMessageForAmount = (TextView) convertView.findViewById(R.id.errorMessageForAmount);
//        errorMessageForRemarks = (TextView) convertView.findViewById(R.id.errorMessageForRemarks);
//        errorMessageForSpinner = (TextView) convertView.findViewById(R.id.errorMessageForSpinner);
            editShareAccount = (EditText) convertView.findViewById(R.id.editShareAccount);
            editShareAccount.setText("UnSubscribed");
            purpStringArrayList = new ArrayList<>();
//        editRemarks = (EditText) convertView.findViewById(R.id.editRemarks);
//        editAmount = (EditText) convertView.findViewById(R.id.editAmount);
            switchCompat = (SwitchCompat) convertView.findViewById(R.id.switchCompat);
            requestQueue = Volley.newRequestQueue(getActivity());
//        webService = new WebService();
            helper = Helper.getHelper(getActivity());
            constants = Constants.getConstants(getActivity());
            utils = new Utils(getActivity());
            pd = new Dialog(getActivity());
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            String txt = "<font COLOR=\'#034ea2\'>" + "I agree to accept Samba Bank Limited \n" + "</font>"
                    + "<font COLOR=\'#034ea2\'><u>" + "Terms &amp; Conditions" + "</u></font>";
            agreementTxt.setText(Html.fromHtml(txt));

            String freq = intent1.getStringExtra("freq");

            try {

                if (!TextUtils.isEmpty(freq) && !freq.equals(null) && !freq.equals("null")) {


//                frequencySec = frequencySec.substring(0, 1).toUpperCase() + frequencySec.substring(1).replaceAll("\\.", " ");
                    frequency = freq.split(", ")[1].toLowerCase();
                    if (frequency.equals("half.yearly")) {
                        frequency = "Half Yearly";

                    } else {
                        frequency = frequency.substring(0, 1).toUpperCase() + frequency.substring(1).replaceAll("\\.", " ");
                    }

                    freq = freq.substring(1).replaceAll("\\.", " ");
                    if (frequency.toLowerCase().equals("half yearly")) {
                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.setText("Subscribed Frequency: " + frequency);
                    } else {
                        frequencySec = freq.split(" ")[2].toLowerCase();
                        Log.d("--", "---" + frequencySec);
                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.setText("Subscribed Frequency: " + freq);
                    }


                } else {
                    errorMessage.setVisibility(View.VISIBLE);
                    errorMessage.setText("Subscribed Frequency: " + "N/A");
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            String status = intent1.getStringExtra("status");
            if (!TextUtils.isEmpty(status) && !status.equals(null) && !status.equals("null")) {

                if (status.toLowerCase().equals("subscribe")) {


                    switchCompat.setChecked(true, false);
                    flag = "Y";
                    editShareAccount.setText("Subscribe");
                    count = 1;
                    flag1 = true;
                    state = "Subscribed";

                } else {

                    switchCompat.setChecked(false, false);
                    flag = "N";
                    editShareAccount.setText("UnSubscribe");
                    count = 2;
                    flag1 = true;
                    state = "UnSubscribed";
                }


            }
            imageBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!isChecked) {
                        imageBtn.setBackground(getResources().getDrawable(R.drawable.checked));
                        isChecked = true;
                    } else {
                        imageBtn.setBackground(getResources().getDrawable(R.drawable.normal));
                        isChecked = false;
                    }

                }
            });

            agreementTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    agreementTxt.setEnabled(false);
                    Intent intent = new Intent(getActivity(), SecurityRecommmendationScreen.class);
                    intent.putExtra("Login", "es");
                    startActivity(intent);
                    agreementTxt.setEnabled(true);
//                finish();
                }
            });

            payabaleAmount.setText(_crypt.decrypt(constants.sharedPreferences.getString("EmailAddress", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            appPreferences = new AppPreferences(getActivity());
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");

            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }
            GetPurposeList();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void GetPurposeList() {

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(getActivity());
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getFreqURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getJSONObject("Signs_EstatementFrequencyResult").getString("Status_Code").equals("00")) {

                                        try {
//
//
                                            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_EstatementFrequencyResult").getString("FrequencyList"));
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                try {

                                                    JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                    purpStringArrayList.add(jsonObj.getString("FrequencyDescription"));
                                                    freqHashMap.put(jsonObj.getString("FrequencyDescription"), jsonObj.getString("FrequencyValue"));

                                                } catch (Exception ex) {
                                                    ex.printStackTrace();
                                                    pd.dismiss();
                                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                                }

                                            }

                                            purpose = new String[purpStringArrayList.size()];
                                            purpose = purpStringArrayList.toArray(purpose);
                                            showSpinner();

                                        } catch (JSONException ex) {

                                            ex.printStackTrace();
                                            pd.dismiss();
                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                        }
                                    } else if (jsonObject.getJSONObject("Signs_EstatementFrequencyResult").getString("Status_Code").equals("54")) {
                                        dialog.dismiss();
                                        utils.showDilogForLogout(jsonObject.getJSONObject("Signs_EstatementFrequencyResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());


                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForError(jsonObject.getJSONObject("Signs_EstatementFrequencyResult").getString("Status_Description"), "ERROR");

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", user);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }

    //    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//
//            constants = Constants.getConstants(getActivity());
//            constants.destinationAccountEditor.putString("isSelect","No");
//            constants.destinationAccountEditor.commit();
//            constants.destinationAccountEditor.apply();
//            constants.sourceAccountEditor.putString("isSelect","No");
//            constants.sourceAccountEditor.commit();
//            constants.sourceAccountEditor.apply();
//        } else {
//
//            //Do Nothing
//        }
//    }
    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                pd.dismiss();
                try {

                    spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Transfer");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(0);
//                    if (!TextUtils.isEmpty(frequency)) {

                    if (!TextUtils.isEmpty(frequency) && !frequency.equals("null") && !frequency.equals(null)) {
                        purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(frequency.replaceAll("\\.", " ")));

                    }


                    //                    } else {


                    purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            try {


                                // Get select item
                                if (purposesSpinner.getSelectedItem() == "Purpose of Transfer") {
                                    purposeSelected = purposesSpinner.getSelectedItem().toString();
                                    Log.d("identity", "---" + purposeSelected);

                                } else {

                                    purposeSelected = purposesSpinner.getSelectedItem().toString();

                                    if (purposeSelected.equals(frequency)) {


                                        switchCompat.setChecked(true, false);
                                        flag = "Y";
                                        editShareAccount.setText("Subscribed");

                                    } else {

                                        if (!TextUtils.isEmpty(frequencySec) && !frequencySec.equals("null") && !frequencySec.equals(null) && frequencySec.equals("half")) {

                                            Log.d("---", "aya");
                                            if (purposeSelected.equals("Half Yearly")) {

                                                switchCompat.setChecked(true, false);
                                                flag = "Y";
                                                editShareAccount.setText("Subscribed");

                                            } else {

                                                switchCompat.setChecked(false, false);
                                                flag = "N";
                                                editShareAccount.setText("UnSubscribed");
                                            }

                                        } else {

                                            switchCompat.setChecked(false, false);
                                            flag = "N";
                                            editShareAccount.setText("UnSubscribed");
                                        }

                                    }

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });

//                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }, 4000);
    }


//    @Override
//    public void onResume() {
//        super.onResume();
//        if (isFirst) {
//
//            upperPart1.setEnabled(true);
//            upperPart2.setEnabled(true);
//            if (isDestination) {
//
//                isSelectSource = constants.sourceAccount.getString("isSelect", "N/A");
//                if (!isSelectSource.equals("N/A") && isSelectSource.equals("Yes")) {
//                    friendNumber = constants.sourceAccount.getString("friendNumber", "N/A");
//                    if (friendNumber.equals("N/A")) {
//
//                    } else {
//                        destinationName = constants.sourceAccount.getString("friendName", "N/A");
//                        userID = constants.sourceAccount.getString("userID", "N/A");
//                        friendID = constants.sourceAccount.getString("friendID", "N/A");
//                        email = constants.sourceAccount.getString("email", "N/A");
//                        mobile = constants.sourceAccount.getString("mobileNumber", "N/A");
//                        isFav = constants.sourceAccount.getString("isFav", "N/A");
//                        userNameTxt.setText(destinationName);
//                    }
//                }
//
//                constants.sourceAccountEditor.putString("isSelect", "No");
//                constants.sourceAccountEditor.commit();
//                constants.sourceAccountEditor.apply();
//                isDestination = false;
//            }
//
//            if (isSource) {
//
//                isSelectDestination = constants.destinationAccount.getString("isSelect", "N/A");
//                if (!isSelectDestination.equals("N/A") && isSelectDestination.equals("Yes")) {
//                    sourceuserName = constants.destinationAccount.getString("userName", "N/A");
//                    if (sourceuserName.equals("N/A")) {
//
//                    } else {
//                        sourceBalance = constants.destinationAccount.getString("balance", "N/A");
//                        sourceAccount = constants.destinationAccount.getString("accountNumber", "N/A");
//                        branchCode = constants.destinationAccount.getString("branchCode", "N/A");
//                        bankCode = constants.destinationAccount.getString("bankCode", "N/A");
//                        sourceCurrency = constants.destinationAccount.getString("currency", "N/A");
//                        friendNameTxt.setText(sourceuserName);
//                        accFrindMaskTxt.setVisibility(View.VISIBLE);
//                        accFrindMaskTxt.setText(utils.getMaskedString(sourceAccount));
//
//                    }
//                }
//
//                constants.destinationAccountEditor.putString("isSelect", "No");
//                constants.destinationAccountEditor.commit();
//                constants.destinationAccountEditor.apply();
//                isSource = false;
//            }
//
//        }
//    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public void showDilogForErrorForMenu(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(getActivity(), HomeScreen.class);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).startActivity(intent);
                ((Activity) getActivity()).finish();
                ((Activity) getActivity()).finishAffinity();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


}