package com.ceesolutions.samba.transactions;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.BuildConfig;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import co.infinum.goldfinger.Error;
import co.infinum.goldfinger.Goldfinger;
import co.infinum.goldfinger.Warning;

/**
 * Created by ceeayaz on 3/27/18.
 */

public class PinVerificationScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1, stan;
    public String latitude1;
    private TextView hintTxt, second_dialog_button, fingerStatus, message, heading, doneBtn, textView, submitBtn, regenerateBtn, accMaskTxt, friendNameTxt, accFrindMaskTxt, totalAmountTxt, amountInWordsTxt, reviewBtn, timeTxt, dateTxt;
    private EditText editiPin;
    private ImageView fingerBtn, fingerPrintIcon;
    private Dialog dialog, dialog1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private String iPIN;
    private Intent intent1;
    private ImageView backBtn;
    private String ftType;
    private String reference;
    private Goldfinger goldfinger;
    private AppPreferences appPreferences;
    private String location;
    private boolean isFinger = false;
    private List<Address> addresses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_verification);
        initViews();
        getSupportActionBar().hide();
        int stanNumber = 6;
        stan = intent1.getStringExtra("stan");
        String isFinger = constants.sharedPreferences.getString("Finger", "N/A");
        if (isFinger.equals("True")) {


        } else if (isFinger.equals("False")) {

            fingerBtn.setVisibility(View.INVISIBLE);
            hintTxt.setText("Authenticate with iPIN");
        }
        goldfinger = new Goldfinger.Builder(this).setLogEnabled(BuildConfig.DEBUG).build();
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAnalytics.getInstance(PinVerificationScreen.this).logEvent("IPin_Pressed", new Bundle());

                iPIN = editiPin.getText().toString().trim();
                if (TextUtils.isEmpty(iPIN) || iPIN.length() < 5)
                    utils.showDilogForError("Please enter valid iPIN", "WARNING");
                else {
                    submitBtn.setEnabled(false);
                    if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                        longitude = Double.valueOf(longitude1);
                        latitude = Double.valueOf(latitude1);
                        location = getAddress("ipin");
//                        if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null)) {
//
//                            SubmitIPINNew();
//
//                        } else {
//                            location = "-";
//                            SubmitIPINNew();
////                utils.showDilogForError("Please turn on location services.", "WARNING");
////                validateBtn.setEnabled(true);
//                        }
                    } else {

                        longitude = 0;
                        latitude = 0;
                        location = "-";

                        SubmitIPINNew();
//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
                    }

                }
            }
        });

        regenerateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                regenerateBtn.setEnabled(false);
                if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                    longitude = Double.valueOf(longitude1);
                    latitude = Double.valueOf(latitude1);
                    location = getAddress("regenerate");
//                    if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null)) {
//
//                        RegenerateiPin();
//
//                    } else {
//                        location = "-";
//                        RegenerateiPin();
////                utils.showDilogForError("Please turn on location services.", "WARNING");
////                validateBtn.setEnabled(true);
//                    }
                } else {

                    longitude = 0;
                    latitude = 0;
                    location = "-";
                    RegenerateiPin();
//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
                }
            }
        });

        fingerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDilogForFinger();

            }
        });

        editiPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                iPIN = s.toString();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");
                finish();
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();

//        fingerPrintIcon.setEnabled(goldfinger.hasEnrolledFingerprint());

        if (goldfinger.hasFingerprintHardware()
                && goldfinger.hasEnrolledFingerprint()) {
//            finger.setVisibility(View.GONE);
        } else {
            fingerBtn.setVisibility(View.INVISIBLE);
            hintTxt.setText("Authenticate with iPIN");
//            statusView.setText(getString(R.string.fingerprint_not_available));
//            statusView.setTextColor(ContextCompat.getColor(this, R.color.error));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        goldfinger.cancel();
    }

    public void initViews() {

        intent1 = getIntent();
        String userName = intent1.getStringExtra("userName");
        editiPin = findViewById(R.id.editiPin);
        submitBtn = findViewById(R.id.submitBtn);
        regenerateBtn = findViewById(R.id.regenerateBtn);
        fingerBtn = findViewById(R.id.fingerBtn);
        hintTxt = findViewById(R.id.hintTxt);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(PinVerificationScreen.this);
        webService = new WebService();
        helper = Helper.getHelper(this);
        backBtn = findViewById(R.id.backBtn);
        utils = new Utils(PinVerificationScreen.this);
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        String name = intent1.getStringExtra("destionationName");
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
    }


//    public void SubmitiPIN() {
//
//        try {
//
//            if (helper.isNetworkAvailable()) {
//
//                dialog.show();
//                String userName = constants.sharedPreferences.getString("userName", "N/A");
//                String md5PasswordAndPin = helper.md5(iPIN + userName.toUpperCase());
//
//                webService.IPINVerification(userName, md5PasswordAndPin, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getValidateOtp();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        try {
//
//                            if (jsonObject != null) {
//
//                                if (jsonObject.has("Error")) {
//                                    dialog.dismiss();
////                                    Toast.makeText(LoginWithPassAndPinScreen.this, "Invalid Username/Password/iPIN!", Toast.LENGTH_LONG).show();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    submitBtn.setEnabled(true);
//                                    clearViews();
//
//
//                                } else {
//
//                                    if (jsonObject.getString("Status_Code").equals("00")) {
//
//                                        dialog.dismiss();
//                                        SubmitFTNew();
//
//                                    } else if (jsonObject.getString("Status_Code").equals("10")) {
//                                        dialog.dismiss();
//                                        utils.showDilogForError("Invalid iPIN entered. Please enter correct iPIN received on your mobile/email address.", "ERROR");
//                                        submitBtn.setEnabled(true);
//                                        clearViews();
//
//                                    } else {
//
//                                        dialog.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                        submitBtn.setEnabled(true);
//                                        clearViews();
//                                    }
//
//                                }
//                            } else {
//                                dialog.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "Invalid Username/Password/iPIN!", Toast.LENGTH_LONG).show();
//
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                submitBtn.setEnabled(true);
//
//                                clearViews();
//
//                            }
//
//                        } catch (JSONException e) {
//                            dialog.dismiss();
////                            Toast.makeText(LoginWithPassAndPinScreen.this, "Invalid Username/Password/iPIN!!", Toast.LENGTH_LONG).show();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            submitBtn.setEnabled(true);
//
//                            clearViews();
//                        }
//                    }
//                }, 3000);
//            } else {
//                dialog.dismiss();
////                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
//
//                submitBtn.setEnabled(true);
//
//                clearViews();
//
//            }
//        } catch (Exception e) {
//
////            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
//            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
//            submitBtn.setEnabled(true);
//            dialog.dismiss();
//            clearViews();
//
//        }
//    }

    public void SubmitIPINNew() {

        try {

            if (helper.isNetworkAvailable()) {
                dialog.show();
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());

                String time = Constants.getDateAndTime();
                final String userName = user;
                final String md5PasswordAndPin = Helper.md5(iPIN + userName.toUpperCase());
                final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + time);

                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", Constants.signsUserName);
                params.put("sService_Password", Constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", time);
                params.put("sSigns_Username", userName);
                params.put("sDeviceLatitude", latitude + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sLocationName", location);
                params.put("sPinData", md5PasswordAndPin);
                params.put("sServerSessionKey", md5);

                final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());


                HttpsTrustManager.allowMySSL(PinVerificationScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.ipinVerificationURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_ValidateOTPResult").getString("Status_Code").equals("00")) {
                                            dialog.dismiss();
                                            appPreferences.putString("location", location);
                                            SubmitFTNew();
                                        } else if (jsonObject.getJSONObject("Signs_ValidateOTPResult").getString("Status_Code").equals("54")) {
                                            dialog.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ValidateOTPResult").getString("Status_Description"), "ERROR", PinVerificationScreen.this, new LoginScreen());
                                            submitBtn.setEnabled(true);
                                            clearViews();

                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_ValidateOTPResult").getString("Status_Description"), "ERROR");
                                            submitBtn.setEnabled(true);
                                            clearViews();

                                        }
                                    } else {
                                        dialog.dismiss();
                                        utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                        submitBtn.setEnabled(true);
                                        clearViews();

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    submitBtn.setEnabled(true);
                                    clearViews();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    submitBtn.setEnabled(true);
                                    clearViews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    submitBtn.setEnabled(true);
                                    clearViews();
                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                       params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", userName);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLocationName", location);
//                        params.put("sPinData", md5PasswordAndPin);
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                dialog.dismiss();
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");

                submitBtn.setEnabled(true);

                clearViews();

            }
        } catch (Exception e) {

            submitBtn.setEnabled(true);
            dialog.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            clearViews();

        }
    }

    public void SubmitFTNew() {

        try {

            if (helper.isNetworkAvailable()) {

                int stanNumber = 6;
                String stan1 = String.valueOf(helper.nDigitRandomNo(stanNumber));
                if (TextUtils.isEmpty(Constants.checkForStan) && TextUtils.isEmpty(Constants.checkForRRN)) {

                    Constants.checkForRRN = intent1.getStringExtra("rrn");
                    Constants.checkForStan = stan1;
                } else {

                    if ((Constants.checkForStan.equals(stan1)) || (Constants.checkForRRN.equals(intent1.getStringExtra("rrn")))) {


                        utils.showDilogForTransaction("Service is currently unavailable. Please try again later.", "ERROR", PinVerificationScreen.this);
                        return;
                    }
                }
                Constants.checkForStan = stan1;
                Constants.checkForRRN = intent1.getStringExtra("rrn");
                dialog.show();
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());

                String time = Constants.getDateAndTime();

                String userName1 = user;
                final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + time);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", Constants.signsUserName);
                params.put("sService_Password", Constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", time);
                ftType = intent1.getStringExtra("ftType");
                if (!TextUtils.isEmpty(ftType) && ftType.equals("requestMoney")) {
                    params.put("sFriendTransferFlag", "F");
                } else {
                    params.put("sFriendTransferFlag", "B");
                }
                params.put("sSigns_Username", user);
                params.put("sDeviceLatitude", latitude + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sLocationName", location);
                params.put("sFromAccountNumber", intent1.getStringExtra("accountNumber"));
                params.put("sToAccountNumber", intent1.getStringExtra("destinationAccount"));
                params.put("sTransactionAmount", intent1.getStringExtra("amount"));
                params.put("sBranchCode", intent1.getStringExtra("branchCode"));
                params.put("sDebitCurrency", intent1.getStringExtra("currency"));
                params.put("sNarration", intent1.getStringExtra("remarks"));
                params.put("sComments", intent1.getStringExtra("remarks"));
                params.put("sBankID", intent1.getStringExtra("bankCode"));
                params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                params.put("sBeneType", intent1.getStringExtra("beneType"));
                params.put("sBeneID", intent1.getStringExtra("beneID"));
                params.put("sSigns_MobileNumber", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                params.put("sSigns_EmailAddress", _crypt.decrypt(constants.sharedPreferences.getString("EmailAddress", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                params.put("sPurposeOfTransaction", intent1.getStringExtra("purpose"));
                params.put("sSTAN", stan1);
                params.put("sRRN", intent1.getStringExtra("rrn"));
                params.put("sServerSessionKey", md5);
                params.put("sSourceAccountTiltle", intent1.getStringExtra("userName"));
                ftType = intent1.getStringExtra("ftType");
                if (!TextUtils.isEmpty(ftType) && ftType.equals("requestMoney")) {
                    params.put("sDestinationAccountTiltle", intent1.getStringExtra("titleFetch"));
                } else {
                    params.put("sDestinationAccountTiltle", intent1.getStringExtra("titleFetch"));
                }


                final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());


                HttpsTrustManager.allowMySSL(PinVerificationScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.submitFTURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_FundsTransferResult").getString("Status_Code").equals("00")) {

                                            if (intent1.getStringExtra("type").equals("moveMoney")) {


                                                Intent intent = new Intent(PinVerificationScreen.this, MoveMoneyConfirmationScreen.class);
                                                intent.putExtra("userName", intent1.getStringExtra("userName"));
                                                intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                                                intent.putExtra("purpose", intent1.getStringExtra("purpose"));
                                                intent.putExtra("destionationName", intent1.getStringExtra("destionationName"));
                                                intent.putExtra("destinationAccount", intent1.getStringExtra("destinationAccount"));
                                                intent.putExtra("remarks", intent1.getStringExtra("remarks"));
                                                intent.putExtra("amount", intent1.getStringExtra("amount"));
                                                intent.putExtra("amountFinal", intent1.getStringExtra("amountFinal"));
                                                intent.putExtra("amountWords", intent1.getStringExtra("amountWords"));
                                                intent.putExtra("branchCode", intent1.getStringExtra("branchCode"));
                                                intent.putExtra("bankCode", intent1.getStringExtra("bankCode"));
                                                intent.putExtra("currency", intent1.getStringExtra("currency"));
                                                intent.putExtra("currentDate", intent1.getStringExtra("currentDate"));
                                                intent.putExtra("currentTime", intent1.getStringExtra("currentTime"));
                                                intent.putExtra("Transaction_Reference", jsonObject.getJSONObject("Signs_FundsTransferResult").getString("Transaction_Reference"));
                                                overridePendingTransition(0, 0);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                                constants.accountsEditor.clear();
                                                constants.accountsEditor.commit();
                                                constants.accountsEditor.apply();
                                                constants.destinationAccountEditor.clear();
                                                constants.destinationAccountEditor.commit();
                                                constants.destinationAccountEditor.apply();
                                                dialog.dismiss();
                                                finish();
                                                finishAffinity();
                                                submitBtn.setEnabled(true);
                                                clearViews();
                                            } else {

                                                ftType = intent1.getStringExtra("ftType");

                                                if (!TextUtils.isEmpty(ftType) && ftType.equals("requestMoney")) {

                                                    reference = jsonObject.getJSONObject("Signs_FundsTransferResult").getString("Transaction_Reference");
                                                    dialog.dismiss();
                                                    RejectRequest();
                                                    submitBtn.setEnabled(true);
                                                    clearViews();
                                                } else {
                                                    Intent intent = new Intent(PinVerificationScreen.this, SendMoneyConfirmationScreen.class);
                                                    intent.putExtra("userName", intent1.getStringExtra("userName"));
                                                    intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                                                    intent.putExtra("purpose", intent1.getStringExtra("purpose"));
                                                    intent.putExtra("destionationName", intent1.getStringExtra("destionationName"));
                                                    intent.putExtra("destinationAccount", intent1.getStringExtra("destinationAccount"));
                                                    intent.putExtra("remarks", intent1.getStringExtra("remarks"));
                                                    intent.putExtra("amount", intent1.getStringExtra("amount"));
                                                    intent.putExtra("amountFinal", intent1.getStringExtra("amountFinal"));
                                                    intent.putExtra("amountWords", intent1.getStringExtra("amountWords"));
                                                    intent.putExtra("branchCode", intent1.getStringExtra("branchCode"));
                                                    intent.putExtra("bankCode", intent1.getStringExtra("bankCode"));
                                                    intent.putExtra("currency", intent1.getStringExtra("currency"));
                                                    intent.putExtra("currentDate", intent1.getStringExtra("currentDate"));
                                                    intent.putExtra("currentTime", intent1.getStringExtra("currentTime"));
                                                    intent.putExtra("isShow", intent1.getStringExtra("isShow"));
                                                    intent.putExtra("Transaction_Reference", jsonObject.getJSONObject("Signs_FundsTransferResult").getString("Transaction_Reference"));
                                                    overridePendingTransition(0, 0);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    startActivity(intent);
                                                    constants.accountsEditor.clear();
                                                    constants.accountsEditor.commit();
                                                    constants.accountsEditor.apply();
                                                    constants.destinationAccountEditor.clear();
                                                    constants.destinationAccountEditor.commit();
                                                    constants.destinationAccountEditor.apply();
                                                    dialog.dismiss();
                                                    finish();
                                                    finishAffinity();
                                                    submitBtn.setEnabled(true);
                                                    clearViews();
                                                }
                                            }


                                        } else if (jsonObject.getJSONObject("Signs_FundsTransferResult").getString("Status_Code").equals("54")) {
                                            dialog.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_FundsTransferResult").getString("Status_Description"), "ERROR", PinVerificationScreen.this, new LoginScreen());
                                            submitBtn.setEnabled(true);
                                            clearViews();

                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForTransaction(jsonObject.getJSONObject("Signs_FundsTransferResult").getString("Status_Description"), "ERROR", PinVerificationScreen.this);
                                            submitBtn.setEnabled(true);
                                            clearViews();
                                        }
                                    } else {
                                        dialog.dismiss();
                                        utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                        submitBtn.setEnabled(true);
                                        clearViews();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    FirebaseAnalytics.getInstance(PinVerificationScreen.this).logEvent("Error", new Bundle());
                                    dialog.dismiss();
                                    utils.showDilogForTransaction("Service is currently unavailable. Please try again later.", "ERROR", PinVerificationScreen.this);
                                    submitBtn.setEnabled(true);
                                    clearViews();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    dialog.dismiss();
                                    utils.showDilogForTransaction("Service is currently unavailable. Please try again later.", "ERROR", PinVerificationScreen.this);
                                    submitBtn.setEnabled(true);
                                    clearViews();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForTransaction("Service is currently unavailable. Please try again later.", "ERROR", PinVerificationScreen.this);
                                    submitBtn.setEnabled(true);
                                    clearViews();
                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLocationName", location);
//                        params.put("sFromAccountNumber", intent1.getStringExtra("accountNumber"));
//                        params.put("sToAccountNumber", intent1.getStringExtra("destinationAccount"));
//                        params.put("sTransactionAmount", intent1.getStringExtra("amount"));
//                        params.put("sBranchCode", intent1.getStringExtra("branchCode"));
//                        params.put("sDebitCurrency", intent1.getStringExtra("currency"));
//                        params.put("sNarration", intent1.getStringExtra("remarks"));
//                        params.put("sComments", intent1.getStringExtra("remarks"));
//                        params.put("sBankID", intent1.getStringExtra("bankCode"));
//                        params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sBeneType", intent1.getStringExtra("beneType"));
//                        params.put("sBeneID", intent1.getStringExtra("beneID"));
//                        params.put("sSigns_MobileNumber", constants.sharedPreferences.getString("MobileNumber", "N/A"));
//                        params.put("sSigns_EmailAddress", constants.sharedPreferences.getString("EmailAddress", "N/A"));
//                        params.put("sPurposeOfTransaction", intent1.getStringExtra("purpose"));
//                        params.put("sSTAN", stan);
//                        params.put("sRRN", intent1.getStringExtra("rrn"));
//                        params.put("sServerSessionKey", md5);
//
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                    }
                };
                postRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                        -1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
//                postRequest.setRetryPolicy(new RetryPolicy() {
//                    @Override
//                    public int getCurrentTimeout() {
//                        return 50000;
//                    }
//
//                    @Override
//                    public int getCurrentRetryCount() {
//                        return 50000;
//                    }
//
//                    @Override
//                    public void retry(VolleyError error) throws VolleyError {
//
//                    }
//                });
            } else {

                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                submitBtn.setEnabled(true);
                dialog.dismiss();
                clearViews();
            }
        } catch (Exception e) {
            e.printStackTrace();
            utils.showDilogForTransaction("Please check your internet connectivity.", "WARNING", PinVerificationScreen.this);
            submitBtn.setEnabled(true);
            dialog.dismiss();
            clearViews();
        }
    }


    public void clearViews() {

        editiPin.getText().clear();
    }

    public void RegenerateiPin() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                dialog.show();
                //                webService.RegeneratePin(constants.sharedPreferences.getString("userName", "N/A"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getRegeneratePinReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//                                pd.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "Invalid Username/iPIN!", Toast.LENGTH_LONG).show();
//                                utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
//                                regenerateBtn.setEnabled(true);
//
//
//                            } else {
//                                pd.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "iPIN has been sent on your registered mobile and email address.", Toast.LENGTH_LONG).show();
//
//                                utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
//                                regenerateBtn.setEnabled(true);
//
//                            }
//                        } else {
//                            pd.dismiss();
//                            utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
//                            regenerateBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 5000);

                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                String time = Constants.getDateAndTime();
                final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + time);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", Constants.signsUserName);
                params.put("sService_Password", Constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sDeviceLatitude", latitude + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sLocationName", location);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", time);
                params.put("sSigns_Username", user);
                params.put("sServerSessionKey", md5);
                params.put("sOptionalIpinGenerater", "Y");

                final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());

                HttpsTrustManager.allowMySSL(PinVerificationScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("00")) {
                                            {
                                                dialog.dismiss();
                                                utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                                regenerateBtn.setEnabled(true);
                                            }
                                        } else if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("54")) {
                                            dialog.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR", PinVerificationScreen.this, new LoginScreen());
                                            regenerateBtn.setEnabled(true);

                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                            regenerateBtn.setEnabled(true);
                                        }
                                    } else {
                                        dialog.dismiss();
                                        utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                        regenerateBtn.setEnabled(true);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                    regenerateBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    dialog.dismiss();
                                    utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                    regenerateBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                    regenerateBtn.setEnabled(true);
                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLocationName", location);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sServerSessionKey", md5);
//                        params.put("sOptionalIpinGenerater","Y");
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                regenerateBtn.setEnabled(true);


            }
        } catch (Exception e) {
            dialog.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            regenerateBtn.setEnabled(true);


        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();

//        showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");
    }

//    public void showDilogForErrorForLogin(String msg, String header) {
//
//        dialog = new Dialog(PinVerificationScreen.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.error_dialog);
//        message = (TextView) dialog.findViewById(R.id.validatingTxt);
//        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
//        heading = (TextView) dialog.findViewById(R.id.heading);
//        dialog.setCancelable(true);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        doneBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                dialog.dismiss();
////                Intent intent = new Intent(PinVerificationScreen.this, FundTransferScreen.class);
////                overridePendingTransition(0, 0);
////                startActivity(intent);
//                finish();
//
//            }
//        });
//        message.setText(msg);
//        heading.setText(header);
//        dialog.show();
//
//
//    }

    public void RejectRequest() {


        try {
            dialog.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    String time = Constants.getDateAndTime();
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + time);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", time);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sLocationName", location);
                    params.put("sUser_name", _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                    params.put("sUser_id", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                    params.put("sUser_number", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                    params.put("sAction", "1");
                    params.put("sFriend_number", intent1.getStringExtra("friendNumber"));
                    params.put("sMessage_id", intent1.getStringExtra("messageId"));
                    params.put("sTarget_form_id", intent1.getStringExtra("targetId"));
                    params.put("sAmount", intent1.getStringExtra("amount"));
                    params.put("sServerSessionKey", md5);

                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());

                    HttpsTrustManager.allowMySSL(PinVerificationScreen.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.rejectReqURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {

                                            if (jsonObject.getJSONObject("action_message_requestResult").getString("Status_Code").equals("00")) {

                                                Intent intent = new Intent(PinVerificationScreen.this, SendMoneyConfirmationScreen.class);
                                                intent.putExtra("userName", intent1.getStringExtra("userName"));
                                                intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                                                intent.putExtra("purpose", intent1.getStringExtra("purpose"));
                                                intent.putExtra("destionationName", intent1.getStringExtra("destionationName"));
                                                intent.putExtra("destinationAccount", intent1.getStringExtra("destinationAccount"));
                                                intent.putExtra("remarks", intent1.getStringExtra("remarks"));
                                                intent.putExtra("amount", intent1.getStringExtra("amount"));
                                                intent.putExtra("amountFinal", intent1.getStringExtra("amountFinal"));
                                                intent.putExtra("amountWords", intent1.getStringExtra("amountWords"));
                                                intent.putExtra("branchCode", intent1.getStringExtra("branchCode"));
                                                intent.putExtra("bankCode", intent1.getStringExtra("bankCode"));
                                                intent.putExtra("currency", intent1.getStringExtra("currency"));
                                                intent.putExtra("currentDate", intent1.getStringExtra("currentDate"));
                                                intent.putExtra("currentTime", intent1.getStringExtra("currentTime"));
                                                intent.putExtra("isShow", intent1.getStringExtra("isShow"));
                                                intent.putExtra("Transaction_Reference", reference);
                                                overridePendingTransition(0, 0);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                                constants.accountsEditor.clear();
                                                constants.accountsEditor.commit();
                                                constants.accountsEditor.apply();
                                                constants.destinationAccountEditor.clear();
                                                constants.destinationAccountEditor.commit();
                                                constants.destinationAccountEditor.apply();
                                                dialog.dismiss();
                                                finish();
                                                finishAffinity();
                                                clearViews();

                                            } else if (jsonObject.getJSONObject("action_message_requestResult").getString("Status_Code").equals("54")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("action_message_requestResult").getString("Status_Description"), "ERROR", PinVerificationScreen.this, new LoginScreen());


                                            } else {

                                                dialog.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("action_message_requestResult").getString("Status_Description"), "ERROR");

                                            }
                                        } else {

                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");

                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sLocationName", location);
//                            params.put("sUser_name", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sUser_id", constants.sharedPreferences.getString("LegalID", "N/A"));
//                            params.put("sUser_number", constants.sharedPreferences.getString("MobileNumber", "N/A"));
//                            params.put("sAction", "1");
//                            params.put("sFriend_number", intent1.getStringExtra("friendNumber"));
//                            params.put("sMessage_id", intent1.getStringExtra("messageId"));
//                            params.put("sTarget_form_id", intent1.getStringExtra("targetId"));
//                            params.put("sAmount", intent1.getStringExtra("amount"));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                }
            } else {
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {


            dialog.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    public void AuthenticateFingerprint() {

        try {

            if (!isFinger) {

                if (helper.isNetworkAvailable()) {
                    dialog.show();
                    isFinger = true;
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    String time = Constants.getDateAndTime();
                    final String userName = user;
                    final String md5PasswordAndPin = Helper.md5(iPIN + userName.toUpperCase());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + time);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", time);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sLocationName", location);
                    params.put("sSigns_Username", userName);
                    params.put("sSecureFlag", "F");
                    ftType = intent1.getStringExtra("ftType");
                    if (!TextUtils.isEmpty(ftType) && ftType.equals("requestMoney")) {
                        params.put("sFriendTransferFlag", "F");
                    } else {
                        params.put("sFriendTransferFlag", "B");
                    }
                    params.put("sSecureVerificationData", md5PasswordAndPin);
                    params.put("sServerSessionKey", md5);

                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());

                    HttpsTrustManager.allowMySSL(PinVerificationScreen.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.ipinFingerVerification,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);
                                    isFinger = false;

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ValidateOTP_FingerPrintResult").getString("Status_Code").equals("00")) {
                                                dialog.dismiss();
                                                SubmitFTNew();
                                            } else if (jsonObject.getJSONObject("Signs_ValidateOTP_FingerPrintResult").getString("Status_Code").equals("54")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ValidateOTP_FingerPrintResult").getString("Status_Description"), "ERROR", PinVerificationScreen.this, new LoginScreen());
                                                submitBtn.setEnabled(true);
                                                clearViews();

                                            } else {
                                                dialog.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_ValidateOTP_FingerPrintResult").getString("Status_Description"), "ERROR");
                                                submitBtn.setEnabled(true);
                                                clearViews();

                                            }
                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                            submitBtn.setEnabled(true);
                                            clearViews();

                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        isFinger = false;
                                        dialog.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);
                                        clearViews();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        dialog.dismiss();
                                        isFinger = false;
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);
                                        clearViews();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();
                                        isFinger = false;
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        submitBtn.setEnabled(true);
                                        clearViews();
                                    }
                                }
                            }
                    )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLocationName", location);
//                        params.put("sSigns_Username", userName);
//                        params.put("sSecureFlag", "F");
//                        params.put("sSecureVerificationData", md5PasswordAndPin);
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } else {
                    dialog.dismiss();
                    isFinger = false;
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                    utils.showDilogForError("Please check your internet connectivity.", "WARNING");

                    submitBtn.setEnabled(true);

                    clearViews();

                }
            } else {
                // Think and write
            }
        } catch (Exception e) {

            submitBtn.setEnabled(true);
            dialog.dismiss();
            isFinger = false;

            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            clearViews();

        }
    }

    public void showDilogForFinger() {

        dialog1 = new Dialog(PinVerificationScreen.this);

        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog1.setContentView(R.layout.finger_dialog);
//        View view = dialog.findViewById(R.id.content);
        fingerPrintIcon = dialog1.findViewById(R.id.fingerprint_icon);
        fingerStatus = dialog1.findViewById(R.id.validatingTxt);
        second_dialog_button = dialog1.findViewById(R.id.doneBtn);

        second_dialog_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
            }
        });
//        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
//        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
//        heading = (TextView) dialog.findViewById(R.id.heading);
//        dialog.setCancelable(false);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        doneBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                dialog.dismiss();
//                ForgotPassword();
//            }
//        });
//        textView.setText(msg);
//        heading.setText(header);
        dialog1.show();

        resetStatusText();
        authenticateUserFingerprint();
    }

    private void resetStatusText() {
//        fingerStatus.setTextColor(ContextCompat.getColor(this, R.color.notiheader));
//        fingerStatus.setText(getString(R.string.authenticate_user));
    }

    private void authenticateUserFingerprint() {
        goldfinger.authenticate(new Goldfinger.Callback() {
            @Override
            public void onSuccess(String value) {
                onSuccessResult(value);
            }

            @Override
            public void onWarning(Warning warning) {
                onWarningResult(warning);
            }

            @Override
            public void onError(Error error) {
                onErrorResult(error);
            }
        });
    }

    private void onSuccessResult(String value) {
        onResult("Authenticated Successfully", value);
        fingerPrintIcon.setBackgroundResource(R.drawable.fingerprint_circle_blue);

        dialog1.dismiss();
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            location = getAddress("fingerprint");
//            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null)) {
//
//                AuthenticateFingerprint();
//
//            } else {
//                location = "-";
//                AuthenticateFingerprint();
////                utils.showDilogForError("Please turn on location services.", "WARNING");
////                validateBtn.setEnabled(true);
//            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";
            AuthenticateFingerprint();
//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }


//        fingerStatus.setTextColor(ContextCompat.getColor(this, R.color.notiheader));
    }

    private void onErrorResult(Error error) {
        onResult("Authentication Failure", error.toString());
        fingerPrintIcon.setBackgroundResource(R.drawable.fingerprint_circle_red);
//        fingerStatus.setTextColor(ContextCompat.getColor(this, R.color.tabunderline));
    }

    private void onWarningResult(Warning warning) {
        onResult("Authentication Failure", warning.toString());
        fingerPrintIcon.setBackgroundResource(R.drawable.fingerprint_circle_red);
//        fingerStatus.setTextColor(ContextCompat.getColor(this, R.color.tabunderline));
    }

    private void onResult(String methodName, String value) {
//        fingerStatus.setText(String.format(Locale.US, "%s", methodName, ""));
    }


    public Address getAddress(final double latitude, final double longitude, final String value) {
        final Geocoder geocoder;

        geocoder = new Geocoder(this, Locale.getDefault());

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {


                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    if (addresses != null) {
                        if (!TextUtils.isEmpty(addresses.get(0).toString()) && !addresses.get(0).equals("null") && !addresses.get(0).equals(null)) {

                            Address locationAddress = addresses.get(0);
                            location = locationAddress.getAddressLine(0);

                            if (value.equals("ipin")) {
                                SubmitIPINNew();
                            } else if (value.equals("regenerate")) {

                                RegenerateiPin();
                            } else if (value.equals("fingerprint")) {

                                AuthenticateFingerprint();
                            }
                        } else {

                            if (value.equals("ipin")) {
                                location = "-";
                                SubmitIPINNew();
                            } else if (value.equals("regenerate")) {

                                location = "-";
                                RegenerateiPin();
                            } else if (value.equals("fingerprint")) {

                                location = "-";
                                AuthenticateFingerprint();
                            }
                        }
                    } else {

                        if (value.equals("ipin")) {
                            location = "-";
                            SubmitIPINNew();
                        } else if (value.equals("regenerate")) {

                            location = "-";
                            RegenerateiPin();
                        } else if (value.equals("fingerprint")) {

                            location = "-";
                            AuthenticateFingerprint();
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    if (value.equals("ipin")) {
                        location = "-";
                        SubmitIPINNew();
                    } else if (value.equals("regenerate")) {

                        location = "-";
                        RegenerateiPin();
                    } else if (value.equals("fingerprint")) {

                        location = "-";
                        AuthenticateFingerprint();
                    }
                }


            }
        });

        if (addresses != null) {
            return addresses.get(0);
        } else
            return null;
    }

    public String getAddress(String login) {

        Address locationAddress = getAddress(latitude, longitude, login);
        String address = "";
        if (locationAddress != null) {
            address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();

            String currentLocation;

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address;

                if (!TextUtils.isEmpty(address1))
                    currentLocation += "\n" + address1;

                if (!TextUtils.isEmpty(city)) {
                    currentLocation += "\n" + city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += " - " + postalCode;
                } else {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += "\n" + postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation += "\n" + state;

                if (!TextUtils.isEmpty(country))
                    currentLocation += "\n" + country;


            }

        }

        return address;

    }
}
