package com.ceesolutions.samba.transactions.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.viewpager.widget.PagerAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.beneficiariesManagement.SelectBankScreen;
import com.ceesolutions.samba.billPayments.PayBillsScreen;
import com.ceesolutions.samba.subscription.SubscriptionScreen;
import com.ceesolutions.samba.transactions.AccountDetailsScreen;
import com.ceesolutions.samba.transactions.FundTransferScreen;
import com.ceesolutions.samba.transactions.Model.Account;
import com.ceesolutions.samba.transactions.TransactionDetailsScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/4/18.
 */

public class SlidingAdapter extends PagerAdapter {


    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    ArrayList<Account> accounts;
    String type;
    String frequencies = "";
    String freqStatus = "";
    String type1 = "";
    String type2 = "";
    private ArrayList<Integer> IMAGES;
    private LayoutInflater inflater;
    private Context mContext;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private String isType;
    private String userType;
    private AppPreferences appPreferences;
    private String location;

    public SlidingAdapter(Context context, ArrayList<Integer> IMAGES, ArrayList<Account> accountArrayList, String type, String user1) {

        this.IMAGES = IMAGES;
        mContext = context;
        accounts = accountArrayList;
        inflater = LayoutInflater.from(context);
        constants = Constants.getConstants(context);
        requestQueue = Volley.newRequestQueue(context);
        utils = new Utils(context);
//        webService = new WebService();
        helper = Helper.getHelper(context);
        pd = new Dialog(context);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        isType = type;
        userType = user1;
        appPreferences = new AppPreferences(context);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return accounts.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        final View convertView = inflater.inflate(R.layout.account_item, view, false);
        try {

            assert convertView != null;
            final RelativeLayout imageView = (RelativeLayout) convertView
                    .findViewById(R.id.main);


            RelativeLayout relativeLayout = (RelativeLayout) convertView.findViewById(R.id.headerImage);
            relativeLayout.bringToFront();
//            RelativeLayout relativeLayout = (RelativeLayout) convertView.findViewById(R.id.progress_view);
            final TextView textView1 = (TextView) convertView
                    .findViewById(R.id.accountName);
            final TextView textView2 = (TextView) convertView
                    .findViewById(R.id.accountNumber);
            final TextView textView3 = (TextView) convertView
                    .findViewById(R.id.accountBranch);
            final TextView textView4 = (TextView) convertView
                    .findViewById(R.id.accountType);

            final View view2 = (View) convertView.findViewById(R.id.view2);
            final TextView more1 = (TextView) convertView
                    .findViewById(R.id.more1);

            final TextView aliasHint = (TextView) convertView
                    .findViewById(R.id.aliasHint);

            final TextView aliasTxt = (TextView) convertView
                    .findViewById(R.id.aliasTxt);

            final TextView accountHint = (TextView) convertView
                    .findViewById(R.id.accountHint);

            final View view6 = (View) convertView.findViewById(R.id.view6);

            final TextView accountNameTxt = (TextView) convertView
                    .findViewById(R.id.accountNameTxt);

            final TextView accountNumberNonSamba = (TextView) convertView
                    .findViewById(R.id.accountNumberNonSamba);

            ImageView right = (ImageView) convertView.findViewById(R.id.right);

            RelativeLayout newAccount = (RelativeLayout) convertView.findViewById(R.id.newAccount);
            TextView addAccountBtn = (TextView) convertView.findViewById(R.id.addAccountBtn);
            RelativeLayout main = (RelativeLayout) convertView.findViewById(R.id.main);
            ScrollView scrollView = (ScrollView) convertView.findViewById(R.id.scrollView);

            final TextView reveal = (TextView) convertView.findViewById(R.id.reveal);
            View view5 = (View) convertView.findViewById(R.id.view5);
            View view4 = (View) convertView.findViewById(R.id.view4);
            TextView otherLogo = (TextView) convertView.findViewById(R.id.otherLogo);
            final TextView currentBal = (TextView) convertView.findViewById(R.id.currentBal);
            ImageView sambaLogo = (ImageView) convertView.findViewById(R.id.sambaLogo);
            View view1 = (View) convertView.findViewById(R.id.view);
            View view3 = (View) convertView.findViewById(R.id.view1);
            if (userType.equals("SAMBA")) {

//                textView2.setEnabled(false);

            } else {
                textView2.setEnabled(false);
                right.setVisibility(View.GONE);
            }


            right.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(mContext, AccountDetailsScreen.class);
//                    intent.putExtra("accountNumber", accounts.get(position).getAccountNumber());
//                    ((Activity) mContext).overridePendingTransition(0, 0);
//                    ((Activity) mContext).startActivity(intent);

                    LatestBalance(position, convertView, textView1, "AccountDetailsScreen");
                }
            });

            textView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    Intent intent = new Intent(mContext, AccountDetailsScreen.class);
//                    intent.putExtra("accountNumber", accounts.get(position).getAccountNumber());
//                    ((Activity) mContext).overridePendingTransition(0, 0);
//                    ((Activity) mContext).startActivity(intent);

                    LatestBalance(position, convertView, textView1, "AccountDetailsScreen");
                }
            });

            TextView reviewBtn = (TextView) convertView.findViewById(R.id.reviewBtn);

            reviewBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FirebaseAnalytics.getInstance(mContext).logEvent("View_Transactions_Pressed", new Bundle());
                    LatestBalance(position, convertView, textView1, "transactions");

                }
            });


            addAccountBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(((Activity) mContext), SelectBankScreen.class);
                    ((Activity) mContext).overridePendingTransition(0, 0);
                    intent.putExtra("type", "addAccount");
                    ((Activity) mContext).startActivity(intent);
//                    ((Activity) mContext).finish();
                }
            });


            if (userType.equals("SAMBA")) {


            } else {


                currentBal.setVisibility(View.GONE);
            }

            RelativeLayout headerImage = (RelativeLayout) convertView.findViewById(R.id.headerImage);
            reveal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    LatestBalance(position, convertView, reveal, "reveal");
                }
            });
            TextView ft = (TextView) convertView.findViewById(R.id.fundTransfer);
            TextView bp = (TextView) convertView.findViewById(R.id.billPayments);
            TextView more = (TextView) convertView.findViewById(R.id.more);

            if (userType.equals("SAMBA")) {

                more.setVisibility(View.VISIBLE);
                more1.setVisibility(View.GONE);
//                more.setEnabled(false);
            } else {


                more.setVisibility(View.GONE);
                more1.setVisibility(View.VISIBLE);
                more1.setText("Request\nVoucher");
                Drawable top = mContext.getResources().getDrawable(R.drawable.icon_request_voucher);
                more1.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);


            }

            more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (userType.equals("SAMBA")) {

                        Subscription(position, convertView, currentBal, type);
                    } else {

                        Intent intent = new Intent(((Activity) mContext), PayBillsScreen.class);
                        intent.putExtra("requestbill", "voucher");
//                        intent.putExtra("ft", "ft");
//                        intent.putExtra("accountNumber", accounts.get(position).getAccountNumber());
//                        intent.putExtra("accountName", accounts.get(position).getAccountName());
//                        intent.putExtra("accountType", accounts.get(position).getAccountType());
//                        intent.putExtra("bankCode", accounts.get(position).getBankCode());
//                        intent.putExtra("branchCode", accounts.get(position).getBranchCode());
//                        intent.putExtra("currency", accounts.get(position).getCurrency());
                        ((Activity) mContext).overridePendingTransition(0, 0);
                        ((Activity) mContext).startActivity(intent);
                    }
                }
            });

            more1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (userType.equals("SAMBA")) {
                        Intent intent = new Intent(((Activity) mContext), SubscriptionScreen.class);
                        intent.putExtra("accountNumber", accounts.get(position).getAccountNumber());
                        intent.putExtra("accountTitle", accounts.get(position).getAccountName());
                        ((Activity) mContext).overridePendingTransition(0, 0);
                        ((Activity) mContext).startActivity(intent);
                    } else {

                        Intent intent = new Intent(((Activity) mContext), PayBillsScreen.class);
                        intent.putExtra("requestbill", "voucher");
//                        intent.putExtra("ft", "ft");
//                        intent.putExtra("accountNumber", accounts.get(position).getAccountNumber());
//                        intent.putExtra("accountName", accounts.get(position).getAccountName());
//                        intent.putExtra("accountType", accounts.get(position).getAccountType());
//                        intent.putExtra("bankCode", accounts.get(position).getBankCode());
//                        intent.putExtra("branchCode", accounts.get(position).getBranchCode());
//                        intent.putExtra("currency", accounts.get(position).getCurrency());
                        ((Activity) mContext).overridePendingTransition(0, 0);
                        ((Activity) mContext).startActivity(intent);
                    }
                }
            });
            bp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (userType.equals("SAMBA"))
                        LatestBalance(position, convertView, textView1, "bp");

                    else {

                        Intent intent = new Intent(((Activity) mContext), PayBillsScreen.class);
                        intent.putExtra("requestbill", "nonSamba");
//                        intent.putExtra("ft", "ft");
//                        intent.putExtra("accountNumber", accounts.get(position).getAccountNumber());
//                        intent.putExtra("accountName", accounts.get(position).getAccountName());
//                        intent.putExtra("accountType", accounts.get(position).getAccountType());
//                        intent.putExtra("bankCode", accounts.get(position).getBankCode());
//                        intent.putExtra("branchCode", accounts.get(position).getBranchCode());
//                        intent.putExtra("currency", accounts.get(position).getCurrency());
                        ((Activity) mContext).overridePendingTransition(0, 0);
                        ((Activity) mContext).startActivity(intent);
//                        ((Activity) mContext).finish();
                    }
                }
            });
            ft.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (userType.equals("SAMBA"))
                        LatestBalance(position, convertView, textView1, "ft");

                    else {

                        Intent intent = new Intent(((Activity) mContext), FundTransferScreen.class);
                        intent.putExtra("requestFunds", "nonsambarequest");
                        intent.putExtra("accountNumber", accounts.get(position).getAccountNumber());
                        intent.putExtra("accountName", accounts.get(position).getAccountName());
                        intent.putExtra("accountType", accounts.get(position).getAccountType());
                        intent.putExtra("bankCode", accounts.get(position).getBankCode());
                        intent.putExtra("branchCode", accounts.get(position).getBranchCode());
                        intent.putExtra("currency", accounts.get(position).getCurrency());
                        intent.putExtra("balance", accounts.get(position).getAvailBal());
                        ((Activity) mContext).overridePendingTransition(0, 0);
                        ((Activity) mContext).startActivity(intent);
//                        ((Activity) mContext).finish();
                    }
                }
            });


            if (isType.equals("addAccount")) {

                newAccount.setVisibility(View.VISIBLE);
                newAccount.bringToFront();
                headerImage.setVisibility(View.GONE);
                main.setVisibility(View.GONE);
                scrollView.setVisibility(View.GONE);
                view3.setVisibility(View.GONE);


            } else {
                textView1.setText(accounts.get(position).getAccountName());
                textView2.setText(utils.getMaskedString(accounts.get(position).getAccountNumber()));
                textView3.setText(accounts.get(position).getBranchName());
                textView4.setText(accounts.get(position).getProductType());

                if (accounts.get(position).getBankCode().equals("69")) {

                    headerImage.setBackground(mContext.getDrawable(R.drawable.group_10));
                    view5.setVisibility(View.GONE);
                    otherLogo.setVisibility(View.GONE);
//                    sambaLogo.setVisibility(View.VISIBLE);
//                    sambaLogo.bringToFront();


                } else {

                    otherLogo.setVisibility(View.VISIBLE);
                    otherLogo.bringToFront();
                    headerImage.setBackground(null);
//                    sambaLogo.setVisibility(View.GONE);
                    otherLogo.setText(accounts.get(position).getBranchShortName());
                    otherLogo.setBackgroundColor(Color.parseColor(accounts.get(position).getBankColor()));
                    view5.setVisibility(View.VISIBLE);
                    textView4.setVisibility(View.GONE);
                    reveal.setVisibility(View.GONE);
                    reviewBtn.setVisibility(View.INVISIBLE);
                    textView1.setVisibility(View.GONE);
//                    view1.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                    ft.setText("Request\nMoney");
                    view4.setVisibility(View.VISIBLE);
                    bp.setText("Request Bill\nPayments");
                    more.setText("Request\nTop-up");
                    aliasHint.setVisibility(View.VISIBLE);
                    aliasTxt.setVisibility(View.VISIBLE);
                    if (!TextUtils.isEmpty(accounts.get(position).getAlias())) {
                        aliasTxt.setText(accounts.get(position).getAlias());
                    } else {
                        aliasTxt.setText(accounts.get(position).getAccountName());
                    }
                    textView3.setText("Account Title");
                    accountHint.setVisibility(View.VISIBLE);
                    accountNameTxt.setVisibility(View.VISIBLE);
                    accountNameTxt.setText(accounts.get(position).getAccountName());
                    textView2.setVisibility(View.INVISIBLE);
                    accountNumberNonSamba.setVisibility(View.VISIBLE);
                    accountNumberNonSamba.setText(utils.getMaskedString(accounts.get(position).getAccountNumber()));
                    view6.setVisibility(View.VISIBLE);
                }

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                    }
                });
            }
            view.addView(convertView, 0);

        } catch (Exception e) {

            e.printStackTrace();
        }

        return convertView;

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


    public void LatestBalance(final int position, final View convertView, final TextView currenBal, final String type) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sT24_AccountNumber", accounts.get(position).getAccountNumber());
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(mContext);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getLatestBalURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code").equals("00")) {

                                                if (type.equals("ft")) {

                                                    pd.dismiss();
                                                    Intent intent = new Intent(((Activity) mContext), FundTransferScreen.class);
                                                    intent.putExtra("ft", "ft");
                                                    intent.putExtra("accountNumber", accounts.get(position).getAccountNumber());
                                                    intent.putExtra("accountName", accounts.get(position).getAccountName());
                                                    intent.putExtra("accountType", accounts.get(position).getAccountType());
                                                    intent.putExtra("bankCode", accounts.get(position).getBankCode());
                                                    intent.putExtra("branchCode", accounts.get(position).getBranchCode());
                                                    intent.putExtra("currency", accounts.get(position).getCurrency());
                                                    intent.putExtra("balance", jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"));
                                                    ((Activity) mContext).overridePendingTransition(0, 0);
                                                    ((Activity) mContext).startActivity(intent);
//                                                ((Activity) mContext).finish();

                                                } else if (type.equals("transactions")) {

                                                    pd.dismiss();
                                                    Intent intent = new Intent(mContext, TransactionDetailsScreen.class);
                                                    intent.putExtra("accountNumber", accounts.get(position).getAccountNumber());
                                                    intent.putExtra("branchCode", accounts.get(position).getBranchCode());
                                                    intent.putExtra("type", accounts.get(position).getAccountType());
                                                    intent.putExtra("bal", jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"));
                                                    intent.putExtra("cur", accounts.get(position).getCurrency());
                                                    ((Activity) mContext).overridePendingTransition(0, 0);
                                                    ((Activity) mContext).startActivity(intent);

                                                } else if (type.equals("AccountDetailsScreen")) {

                                                    FirebaseAnalytics.getInstance(mContext).logEvent("Additional_Info_Pressed", new Bundle());
                                                    pd.dismiss();
                                                    Intent intent = new Intent(mContext, AccountDetailsScreen.class);
                                                    intent.putExtra("accountNumber", accounts.get(position).getAccountNumber());
                                                    intent.putExtra("branchCode", accounts.get(position).getBranchCode());
                                                    intent.putExtra("type", accounts.get(position).getAccountType());
                                                    intent.putExtra("bal", jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"));
                                                    intent.putExtra("cur", accounts.get(position).getCurrency());
                                                    ((Activity) mContext).overridePendingTransition(0, 0);
                                                    ((Activity) mContext).startActivity(intent);




                                                } else if (type.equals("bp")) {

                                                    pd.dismiss();
                                                    Intent intent = new Intent(mContext, PayBillsScreen.class);
                                                    intent.putExtra("accountNumber", accounts.get(position).getAccountNumber());
                                                    intent.putExtra("accountName", accounts.get(position).getAccountName());
                                                    intent.putExtra("accountType", accounts.get(position).getAccountType());
                                                    intent.putExtra("bankCode", accounts.get(position).getBankCode());
                                                    intent.putExtra("branchCode", accounts.get(position).getBranchCode());
                                                    intent.putExtra("currency", accounts.get(position).getCurrency());
                                                    intent.putExtra("balance", jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"));
                                                    intent.putExtra("paybill", "paybill");
                                                    ((Activity) mContext).overridePendingTransition(0, 0);
                                                    ((Activity) mContext).startActivity(intent);
                                                } else {
                                                    pd.dismiss();
                                                    currenBal.setText(accounts.get(position).getCurrency() + " " + jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"));
                                                    currenBal.setEnabled(false);
                                                }

                                            } else if (jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Description"), "ERROR", mContext, new LoginScreen());


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Description"), "ERROR");

                                            }
                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sT24_AccountNumber", accounts.get(position).getAccountNumber());
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }

            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    public void Subscription(final int position, final View convertView, final TextView currenBal, final String type) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();
                try {
                    frequencies = "";
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sAccountNumber", accounts.get(position).getAccountNumber());
                    params.put("sBranchCode", accounts.get(position).getBranchCode());
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(mContext);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAccountEstatementURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("SignsAccountSubscribedEstatementResult").getString("Status_Code").equals("00")) {


                                                int count = 0;
                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("SignsAccountSubscribedEstatementResult").getString("Frequency"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

//                                                    if (count == 0)
//                                                        frequencies = jsonObj.getString("FrequencyValue");
//                                                    else

                                                        frequencies = frequencies + ", " + jsonObj.getString("FrequencyValue");

                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        pd.dismiss();
                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                                    }

                                                }

                                                SubscriptionSms(position, convertView, currenBal, type);

                                            } else if (jsonObject.getJSONObject("SignsAccountSubscribedEstatementResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("SignsAccountSubscribedEstatementResult").getString("Status_Description"), "ERROR", mContext, new LoginScreen());

                                            } else if (jsonObject.getJSONObject("SignsAccountSubscribedEstatementResult").getString("Status_Code").equals("14")) {

                                                SubscriptionSms(position, convertView, currenBal, type);

                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("SignsAccountSubscribedEstatementResult").getString("Status_Description"), "ERROR");

                                            }
                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sAccountNumber", accounts.get(position).getAccountNumber());
//                            params.put("sBranchCode", accounts.get(position).getBranchCode());
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }

            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    public void SubscriptionSms(final int position, final View convertView, final TextView currenBal, final String type) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();
                try {
                    type1 = "";
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sAccountNumber", accounts.get(position).getAccountNumber());
                    params.put("sBranchCode", accounts.get(position).getBranchCode());
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(mContext);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getSmsStatementURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("SignsAccountSubscribedSmsResult").getString("Status_Code").equals("00")) {

                                                String one = "", two = "";
                                                int count = 0;
                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("SignsAccountSubscribedSmsResult").getString("AccountSubscribedSMS"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

//                                                    if (count == 0)
//                                                        frequencies = jsonObj.getString("FrequencyValue");
//                                                    else


                                                        if (count == 0) {
                                                            type1 = type1 + "," + jsonObj.getString("AccountID") + "," + jsonObj.getString("Message_Group") + "," + jsonObj.getString("Message_Subscription");
                                                            count++;
                                                        } else {
                                                            type2 = type2 + "," + jsonObj.getString("AccountID") + "," + jsonObj.getString("Message_Group") + "," + jsonObj.getString("Message_Subscription");

                                                        }
                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        pd.dismiss();
                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                                    }

                                                }

                                                pd.dismiss();
                                                Intent intent = new Intent(((Activity) mContext), SubscriptionScreen.class);
                                                intent.putExtra("accountNumber", accounts.get(position).getAccountNumber());
                                                intent.putExtra("accountTitle", accounts.get(position).getAccountName());
                                                intent.putExtra("branchCode", accounts.get(position).getBranchCode());
                                                intent.putExtra("freq", frequencies);
                                                intent.putExtra("status", freqStatus);
                                                intent.putExtra("type1", type1);
                                                intent.putExtra("type2", type2);
                                                ((Activity) mContext).overridePendingTransition(0, 0);
                                                ((Activity) mContext).startActivity(intent);

                                            } else if (jsonObject.getJSONObject("SignsAccountSubscribedSmsResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("SignsAccountSubscribedSmsResult").getString("Status_Description"), "ERROR", mContext, new LoginScreen());


                                            } else if (jsonObject.getJSONObject("SignsAccountSubscribedSmsResult").getString("Status_Code").equals("14")) {

                                                pd.dismiss();
                                                Intent intent = new Intent(((Activity) mContext), SubscriptionScreen.class);
                                                intent.putExtra("accountNumber", accounts.get(position).getAccountNumber());
                                                intent.putExtra("accountTitle", accounts.get(position).getAccountName());
                                                intent.putExtra("branchCode", accounts.get(position).getBranchCode());
                                                intent.putExtra("freq", frequencies);
                                                intent.putExtra("status", freqStatus);
                                                intent.putExtra("type1", type1);
                                                intent.putExtra("type2", type2);
                                                ((Activity) mContext).overridePendingTransition(0, 0);
                                                ((Activity) mContext).startActivity(intent);

                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("SignsAccountSubscribedSmsResult").getString("Status_Description"), "ERROR");

                                            }
                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sAccountNumber", accounts.get(position).getAccountNumber());
//                            params.put("sBranchCode", accounts.get(position).getBranchCode());
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }

            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}