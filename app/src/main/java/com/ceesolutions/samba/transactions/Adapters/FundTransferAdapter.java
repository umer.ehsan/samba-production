package com.ceesolutions.samba.transactions.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ceesolutions.samba.transactions.Fragments.MoveMoneyFragment;
import com.ceesolutions.samba.transactions.Fragments.RequestMoneyFragment;
import com.ceesolutions.samba.transactions.Fragments.SendMoneyFragment;

/**
 * Created by ceeayaz on 3/9/18.
 */

public class FundTransferAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public FundTransferAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MoveMoneyFragment moveMoneyFragment = new MoveMoneyFragment();
                return moveMoneyFragment;
            case 1:
                SendMoneyFragment sendMoneyFragment = new SendMoneyFragment();
                return sendMoneyFragment;
            case 2:
                RequestMoneyFragment requestMoneyFragment = new RequestMoneyFragment();
                return requestMoneyFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}