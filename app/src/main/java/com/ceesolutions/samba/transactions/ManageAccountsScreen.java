package com.ceesolutions.samba.transactions;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import com.ceesolutions.samba.eatMubarak.EatMubarakMainScreen;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.Bookme.BookMeMainScreen;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.beneficiariesManagement.ManageBeneficiaresScreen;
import com.ceesolutions.samba.beneficiariesManagement.SelectBankScreen;
import com.ceesolutions.samba.billPayments.PayBillsScreen;
import com.ceesolutions.samba.cardManagement.ManageCardsScreen;
import com.ceesolutions.samba.masterCardIntegration.MainActivity;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.transactions.Adapters.ManageAccountsCustomListAdapter;
import com.ceesolutions.samba.transactions.Model.Account;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.OnBoomListenerAdapter;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/12/18.
 */

public class ManageAccountsScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public ProgressBar progress;
    public String longitude1;
    public String latitude1;
    private FloatingActionButton floatingAction;
    private ImageView backBtn;
    private ListView listView;
    private ManageAccountsCustomListAdapter manageAccountsCustomListAdapter;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Utils utils;
    private ArrayList<Account> accounts;
    private ImageView notificationBtn, sambaBtn, shareBtn;
    private Dialog pd, dialog;
    private String type;
    private String menu, userType;
    private EditText chatBotText;
    private TextView progressTxt, message, doneBtn, textView, heading, btn;
    private BoomMenuButton bmb;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_accounts);
        getSupportActionBar().hide();
        initViews();
        try {
            CryptLib _crypt = new CryptLib();
            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }
            menu = constants.sharedPreferences.getString("Menus", "N/A");
            if (!TextUtils.isEmpty(menu) && !menu.equals("N/A") && !menu.equals("")) {


            } else {

                menu = "N/A";
            }

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        Menu navMenu = navigationView.getMenu();
//        navigationView.setItemIconTintList(null);
////        navMenu.add(0,R.id.home,0,)
//        navigationView.setNavigationItemSelectedListener(this);
//
//        if (!menu.equals("N/A")) {
//
//            if (menu.toLowerCase().contains("home")) {
//
//
//            } else {
//                MenuItem home = navMenu.add(0, R.id.home, 0, "Home");
//                home.setIcon(R.drawable.home);
//
//            }
//
//            if (menu.toLowerCase().contains("accounts")) {
//
//            } else {
//
//                MenuItem accounts = navMenu.add(0, R.id.accounts, 0, "Accounts");
//                accounts.setIcon(R.drawable.accounts);
//            }
//
//            if (menu.toLowerCase().contains("beneficiary management")) {
//
//            } else {
//
//                MenuItem beneManagement = navMenu.add(0, R.id.manageBene, 0, "Beneficiary Management");
//                beneManagement.setIcon(R.drawable.beneficiary);
//            }
//
//            if (menu.toLowerCase().contains("friends management")) {
//
//            } else {
//
//                MenuItem friends = navMenu.add(0, R.id.manageFriends, 0, "Friends Management");
//                friends.setIcon(R.drawable.friends);
//            }
//
//            if (menu.toLowerCase().contains("biller management")) {
//
//            } else {
//
//                MenuItem billPayments = navMenu.add(0, R.id.billPayments, 0, "Biller Management");
//                billPayments.setIcon(R.drawable.receipt);
//
//            }
//
//            if (menu.toLowerCase().contains("cards management")) {
//
//            } else {
//
//                MenuItem cardManagement = navMenu.add(0, R.id.cardManagement, 0, "Cards Management");
//                cardManagement.setIcon(R.drawable.credit_card);
//
//            }
//
//            if (menu.toLowerCase().contains("feedback")) {
//
//            } else {
//
//                MenuItem feedback = navMenu.add(0, R.id.feedback, 0, "Customer Feedback");
//                feedback.setIcon(R.drawable.chat_smiley);
//
//            }
//
//        } else {
//
//            MenuItem home = navMenu.add(0, R.id.home, 0, "Home");
//            home.setIcon(R.drawable.home);
//
//            MenuItem accounts = navMenu.add(0, R.id.accounts, 0, "Accounts");
//            accounts.setIcon(R.drawable.accounts);
//
//            MenuItem beneManagement = navMenu.add(0, R.id.manageBene, 0, "Beneficiary Management");
//            beneManagement.setIcon(R.drawable.beneficiary);
//
//            MenuItem friends = navMenu.add(0, R.id.manageFriends, 0, "Friends Management");
//            friends.setIcon(R.drawable.friends);
//
//            MenuItem billPayments = navMenu.add(0, R.id.billPayments, 0, "Biller Management");
//            billPayments.setIcon(R.drawable.receipt);
//
//
//            MenuItem cardManagement = navMenu.add(0, R.id.cardManagement, 0, "Cards Management");
//            cardManagement.setIcon(R.drawable.credit_card);
//
//            MenuItem feedback = navMenu.add(0, R.id.feedback, 0, "Customer Feedback");
//            feedback.setIcon(R.drawable.chat_smiley);
//
//        }
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
//        ImageView notificationBtn = (ImageView) toolbar.findViewById(R.id.notificationBtn);
//
//        notificationBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(ManageAccountsScreen.this, MessageCenter.class);
//                overridePendingTransition(0,0);
//                startActivity(intent);
//            }
//        });
//
//        navigationView.setNavigationItemSelectedListener(this);
//        View headerLayout = navigationView.getHeaderView(0);
//        TextView textView = (TextView) headerLayout.findViewById(R.id.profileName);
//        ImageView profile = (ImageView) headerLayout.findViewById(R.id.profile_image);
//        profile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(ManageAccountsScreen.this, EditProfileScreen.class);
//                overridePendingTransition(0, 0);
//                startActivity(intent);
//
//            }
//        });
//        progress = (ProgressBar) headerLayout.findViewById(R.id.progress);
//        progressTxt = (TextView) headerLayout.findViewById(R.id.progressTxt);
//
//        String str = constants.sharedPreferences.getString("UserProgress","N/A");
//
//        progressTxt.setText(str+"% Profile Completed");
//
//        int pro = Integer.valueOf(str);
//        progress.setProgress(pro);
//
//        String image = constants.sharedPreferences.getString("ProfilePic", "N/A");
//
//
//        try {
//
//            if (!image.equals("N/A")) {
//                byte[] imageByteArray = Base64.decode(image, Base64.DEFAULT);
//                RequestOptions options = new RequestOptions();
//                options.centerCrop();
//                Glide.with(ManageAccountsScreen.this)
//                        .load(imageByteArray)
//                        .apply(options)
//                        .into(profile);
//            } else {
//
//
//            }
//        } catch (Exception e) {
//
//
//        }
//
//
//        ImageView imageView = (ImageView) headerLayout.findViewById(R.id.settings);
//        textView.setText(constants.sharedPreferences.getString("T24_FullName", "N/A"));
//
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                Intent intent = new Intent(ManageAccountsScreen.this, SettingsScreen.class);
//                overridePendingTransition(0, 0);
//                startActivity(intent);
//                finish();
//
//            }
//        });

            floatingAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (userType.equals("NON_SAMBA")) {
                        FirebaseAnalytics.getInstance(ManageAccountsScreen.this).logEvent("Non_Samba_User_Add_Account_Clicked", new Bundle());
                    }




                    Intent intent = new Intent(ManageAccountsScreen.this, SelectBankScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("type", "addAccount");
                    startActivity(intent);
                }
            });

            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (!TextUtils.isEmpty(getIntent().getStringExtra("intent")) && getIntent().getStringExtra("intent").equals("home")) {

                        finish();

                    } else {
                        Intent intent = new Intent(ManageAccountsScreen.this, AccountsScreen.class);
                        overridePendingTransition(0, 0);
                        startActivity(intent);
                        finish();
                    }
                }
            });

            notificationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(ManageAccountsScreen.this, MessageCenter.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
//                finish();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initViews() {


        floatingAction = (FloatingActionButton) findViewById(R.id.floatingAction);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        listView = (ListView) findViewById(R.id.listView);
        helper = Helper.getHelper(this);
        chatBotText = (EditText) findViewById(R.id.chatBoxText);
        sambaBtn = (ImageView) findViewById(R.id.sambaBtn);
        shareBtn = (ImageView) findViewById(R.id.shareBtn);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(ManageAccountsScreen.this);
        utils = new Utils(ManageAccountsScreen.this);
        pd = new Dialog(ManageAccountsScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        try {
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }
            bmb = (BoomMenuButton) findViewById(R.id.bmb);
            assert bmb != null;
            bmb.setButtonEnum(ButtonEnum.TextOutsideCircle);
            bmb.setBackgroundColor(Color.TRANSPARENT);
            bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_9_1);
            bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_9_1);
            CryptLib _crypt = new CryptLib();
            type = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

            if (type.equals("SAMBA")) {

                floatingAction.setVisibility(View.GONE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        GetAccountsList();

                    }
                }, 200);
            } else {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        GetAccountsList();

                    }
                }, 200);
            }
            accounts = new ArrayList<>();

            chatBotText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                showDilogForErrorForLogin(getResources().getString(R.string.message), "WARNING");
                    Intent i = new Intent(ManageAccountsScreen.this, HomeScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(i);
                    finish();
                    finishAffinity();
                }
            });
//        if (list.equals("N/A")) {
//
//        }


            sambaBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    bmb.boom();
                }
            });

            shareBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {


                        Intent intent = new Intent("android.intent.action.VIEW");

                        /** creates an sms uri */
                        Uri data = Uri.parse("sms:");

                        /** Setting sms uri to the intent */
                        intent.setData(data);
                        intent.putExtra("sms_body", "Download Samba Mobile App and register yourself. Click https://bit.ly/2KB0nfG for details");

                        /** Initiates the SMS compose screen, because the activity contain ACTION_VIEW and sms uri */
                        startActivity(intent);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            bmb.setOnBoomListener(new OnBoomListenerAdapter() {
                @Override
                public void onBoomWillShow() {
                    super.onBoomWillShow();
                    // logic here
                }
            });

            bmb.setOnBoomListener(new OnBoomListener() {
                @Override
                public void onClicked(int index, BoomButton boomButton) {
                    // If you have implement listeners for boom-buttons in builders,
                    // then you shouldn't add any listener here for duplicate callbacks.

                    switch (index) {


                        case 0:

                            Intent home = new Intent(ManageAccountsScreen.this, HomeScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(home);
                            finish();
                            break;

                        case 1:

                            if (!menu.equals("N/A")) {

                                if (menu.toLowerCase().contains("accounts")) {
                                    showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                } else {

                                    Intent intent = new Intent(ManageAccountsScreen.this, AccountsScreen.class);
                                    overridePendingTransition(0, 0);
                                    startActivity(intent);
//                                finish();
                                }
                            } else {
                                Intent intent = new Intent(ManageAccountsScreen.this, AccountsScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
//                            finish();

                            }

                            break;

                        case 2:
                            try {
                                CryptLib _crypt = new CryptLib();
                                Intent transfers = new Intent(ManageAccountsScreen.this, FundTransferScreen.class);
                                String user = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                if (!TextUtils.isEmpty(user) && !user.equals("N/A")) {

                                    if (user.equals("NON_SAMBA")) {

                                        transfers.putExtra("requestFunds", "nonSamba");
                                    }
                                }
                                overridePendingTransition(0, 0);
                                startActivity(transfers);
//                            finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;


                        case 3:

                            try {
                                CryptLib _crypt = new CryptLib();
                                if (!menu.equals("N/A")) {

                                    if (menu.toLowerCase().contains("biller management")) {
                                        showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                    } else {
                                        String user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                        Intent payments = new Intent(ManageAccountsScreen.this, PayBillsScreen.class);
                                        overridePendingTransition(0, 0);
                                        user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                        if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                            if (user1.equals("NON_SAMBA")) {

                                                payments.putExtra("requestbill", "nonSamba");
                                            }
                                        }
                                        startActivity(payments);
//                                    finish();
                                    }
                                } else {
                                    Intent payments = new Intent(ManageAccountsScreen.this, PayBillsScreen.class);
                                    overridePendingTransition(0, 0);
                                    String user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                    user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                    if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                        if (user1.equals("NON_SAMBA")) {

                                            payments.putExtra("requestbill", "nonSamba");
                                        }
                                    }
                                    startActivity(payments);
//                                finish();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;


                        case 4:

                            if (!menu.equals("N/A")) {

                                if (menu.toLowerCase().contains("beneficiary management")) {
                                    showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                } else {

                                    if (userType.equals("NON_SAMBA")) {
                                        showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                    } else {
                                        Intent bene = new Intent(ManageAccountsScreen.this, ManageBeneficiaresScreen.class);
                                        overridePendingTransition(0, 0);
                                        startActivity(bene);
//                                    finish();
                                    }
                                }
                            } else {
                                if (userType.equals("NON_SAMBA")) {
                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                } else {
                                    Intent bene = new Intent(ManageAccountsScreen.this, ManageBeneficiaresScreen.class);
                                    overridePendingTransition(0, 0);
                                    startActivity(bene);
//                                    finish();
                                }
//                                finish();
                            }

                            break;


                        case 5:

                            if (userType.equals("NON_SAMBA"))
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            else {
                                Intent intent = new Intent(ManageAccountsScreen.this, MainActivity.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                            }

//                            if (!menu.equals("N/A")) {
//
//                                if (menu.toLowerCase().contains("friends management")) {
//                                    showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                                } else {
//
//                                    Intent friends = new Intent(ManageAccountsScreen.this, ManageFriends.class);
//                                    overridePendingTransition(0, 0);
//                                    startActivity(friends);
////                                    finish();
//                                }
//                            } else {
//                                Intent friends = new Intent(ManageAccountsScreen.this, ManageFriends.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(friends);
////                                finish();
//                            }

                            break;


                        case 6:

                            try {

                                CryptLib _crypt = new CryptLib();

                                String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                                if (userType.equals("NON_SAMBA"))
                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                else {
                                    Intent intent = new Intent(ManageAccountsScreen.this, EatMubarakMainScreen.class);
                                    overridePendingTransition(0, 0);
                                    startActivity(intent);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }



//                            if (!menu.equals("N/A")) {
//
//                                if (menu.toLowerCase().contains("cards management")) {
//                                    showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                                } else {
//                                    if (userType.equals("NON_SAMBA")) {
//                                        showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                                    } else {
//                                        Intent cards = new Intent(ManageAccountsScreen.this, ManageCardsScreen.class);
//                                        overridePendingTransition(0, 0);
//                                        startActivity(cards);
////                                    finish();
//                                    }
//
////                                    finish();
//                                }
//                            } else {
//                                if (userType.equals("NON_SAMBA")) {
//                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                                } else {
//                                    Intent cards = new Intent(ManageAccountsScreen.this, ManageCardsScreen.class);
//                                    overridePendingTransition(0, 0);
//                                    startActivity(cards);
////                                    finish();
//                                }
////                                finish();
//                            }
                            break;


                        case 7:
                            if (userType.equals("NON_SAMBA")) {
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            } else {
                            Intent atm = new Intent(ManageAccountsScreen.this, BookMeMainScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(atm);
                            }
//                        finish();
                            break;

                        case 8:

                            pd.show();
                            showDialog("Are you sure you want to Logout?");
                            break;


                        default: {

                            break;
                        }
                    }
                }

                @Override
                public void onBackgroundClick() {
//                textViewForAnimation.setText("Click background!!!");
                }

                @Override
                public void onBoomWillHide() {
                    Log.d("BMB", "onBoomWillHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will RE-BOOM!!!");
                }

                @Override
                public void onBoomDidHide() {
                    Log.d("BMB", "onBoomDidHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did RE-BOOM!!!");
                }

                @Override
                public void onBoomWillShow() {
                    Log.d("BMB", "onBoomWillShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will BOOM!!!");
                }

                @Override
                public void onBoomDidShow() {
                    Log.d("BMB", "onBoomDidShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did BOOM!!!");
                }
            });

//        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++)
//            bmb.addBuilder(BuilderManager.getSimpleCircleButtonBuilder());

            for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
                TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder()
                        .listener(new OnBMClickListener() {
                            @Override
                            public void onBoomButtonClick(int index) {
                                // When the boom-button corresponding this builder is clicked.
//                            Toast.makeText(HomeScreen.this, "Clicked " + index, Toast.LENGTH_SHORT).show();
                            }
                        });

//            int margin = getResources().getDimensionPixelSize(R.dimen._4sdp);
                builder.textSize(12);

                switch (i) {

                    case 0:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Home");
                        builder.unableText("Home");
                        builder.normalText("Home");
                        break;

                    case 1:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Accounts");
                        builder.unableText("Accounts");
                        builder.normalText("Accounts");
                        break;

                    case 2:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Transfers");
                        builder.unableText("Transfers");
                        builder.normalText("Transfers");
                        break;


                    case 3:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Payments");
                        builder.unableText("Payments");
                        builder.normalText("Payments");
                        break;


                    case 4:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Beneficiaries");
                        builder.unableText("Beneficiaries");
                        builder.normalText("Beneficiaries");
                        break;


                    case 5:

                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("QR Payments");
                        builder.unableText("QR Payments");
                        builder.normalText("QR Payments");


//                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                        builder.normalColor(getResources().getColor(R.color.notiheader));
//                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                        builder.unableColor(getResources().getColor(R.color.notiheader));
//                        builder.highlightedText("Friends");
//                        builder.unableText("Friends");
//                        builder.normalText("Friends");
                        break;


                    case 6:

                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Food");
                        builder.unableText("Food");
                        builder.normalText("Food");

//                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                        builder.normalColor(getResources().getColor(R.color.notiheader));
//                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                        builder.unableColor(getResources().getColor(R.color.notiheader));
//                        builder.highlightedText("Cards");
//                        builder.unableText("Cards");
//                        builder.normalText("Cards");
                        break;


                    case 7:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("E-Tickets");
                        builder.unableText("E-Tickets");
                        builder.normalText("E-Tickets");
                        break;

                    case 8:
                        builder.normalImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                        builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                        builder.unableImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                        builder.normalColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedColor(getResources().getColor(R.color.notiheader));
                        builder.unableColor(getResources().getColor(R.color.notiheader));
                        builder.highlightedText("Logout");
                        builder.unableText("Logout");
                        builder.normalText("Logout");
                        break;


                    default: {

                        break;
                    }

                }
//            builder.normalImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.unableImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.normalColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedColor(getResources().getColor(R.color.notiheader));
//            builder.unableColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedText("Cards");
//            builder.unableText("Cards");
//            builder.normalText("Cards");
//            builder.imagePadding(new Rect(40, 40, 40, 40));
                bmb.addBuilder(builder);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void GetAccountsList() {

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {
                //135821
                //constants.sharedPreferences.getString("T24_CIF", "N/A")

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sT24_CIF", _crypt.decrypt(constants.sharedPreferences.getString("T24_CIF", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sLegalIdentityType", _crypt.decrypt(constants.sharedPreferences.getString("LegalType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sSigns_UserType", type);
                    params.put("sIsAllAccounts", "0");
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                    HttpsTrustManager.allowMySSL(ManageAccountsScreen.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAccountListURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("00")) {

                                                int count = 0;
                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("AccountList"));

                                                if (jsonArray.length() > 0) {


                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                        try {

                                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
                                                            Account account = new Account();
                                                            account.setAccountNumber(jsonObj.getString("AccountID"));
                                                            account.setAccountName(jsonObj.getString("AccountTitle"));
                                                            account.setUserName(jsonObj.getString("AccountTitle"));
                                                            account.setBranchName(jsonObj.getString("BranchName"));
                                                            account.setAccountType(jsonObj.getString("Type"));
                                                            account.setAvailBal(jsonObj.getString("AvailBal"));
                                                            account.setBankCode(jsonObj.getString("BankCode"));
                                                            account.setBankID(jsonObj.getString("BankCode"));
                                                            account.setBranchCode(jsonObj.getString("BranchCode"));
                                                            account.setCurrency(jsonObj.getString("Currency"));
                                                            account.setCurrencyType(jsonObj.getString("Currency"));
                                                            account.setProductType(jsonObj.getString("Product"));
                                                            account.setBankColor(jsonObj.getString("BankColorCode"));
                                                            account.setIsActive(jsonObj.getString("Active"));
                                                            account.setId(jsonObj.getString("ID"));
                                                            account.setBranchShortName(jsonObj.getString("BranchNameShort"));
                                                            accounts.add(account);


                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                            pd.dismiss();
                                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        }

                                                    }
                                                    manageAccountsCustomListAdapter = new ManageAccountsCustomListAdapter(accounts, ManageAccountsScreen.this);
                                                    listView.setAdapter(manageAccountsCustomListAdapter);
                                                    pd.dismiss();
                                                } else {
                                                    pd.dismiss();

                                                }
                                            } else if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "ERROR", ManageAccountsScreen.this, new LoginScreen());


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "WARNING");
                                            }
                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sT24_CIF", constants.sharedPreferences.getString("T24_CIF", "N/A"));
//                            params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                            params.put("sLegalIdentityType", constants.sharedPreferences.getString("LegalType", "N/A"));
//                            params.put("sSigns_UserType", type);
//                            params.put("sIsAllAccounts", "0");
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                }

            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }

    @Override
    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
////            super.onBackPressed();
//        }
        if (!TextUtils.isEmpty(getIntent().getStringExtra("intent")) && getIntent().getStringExtra("intent").equals("home")) {

            finish();

        } else {
            Intent intent = new Intent(ManageAccountsScreen.this, AccountsScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);
            finish();
        }
    }

//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.home) {
//            Intent intent = new Intent(ManageAccountsScreen.this, HomeScreen.class);
//            overridePendingTransition(0, 0);
//            startActivity(intent);
//            finish();
//        } else if (id == R.id.accounts) {
//
//            Intent intent = new Intent(ManageAccountsScreen.this, AccountsScreen.class);
//            overridePendingTransition(0, 0);
//            startActivity(intent);
//            finish();
//
//        } else if (id == R.id.manageBene) {
//
//            Intent intent1 = new Intent(ManageAccountsScreen.this, ManageBeneficiaresScreen.class);
//            overridePendingTransition(0, 0);
//            startActivity(intent1);
//            finish();
//
//        } else if (id == R.id.manageFriends) {
//
//            Intent intent2 = new Intent(ManageAccountsScreen.this, ManageFriends.class);
//            overridePendingTransition(0, 0);
//            startActivity(intent2);
//            finish();
//
//        } else if (id == R.id.billPayments) {
//
//            Intent intent2 = new Intent(ManageAccountsScreen.this, ManageBillerScreen.class);
//            overridePendingTransition(0, 0);
//            startActivity(intent2);
//            finish();
//
//        } else if (id == R.id.cardManagement) {
//
//            Intent intent2 = new Intent(ManageAccountsScreen.this, ManageCardsScreen.class);
//            overridePendingTransition(0, 0);
//            startActivity(intent2);
//            finish();
//
//        } else if (id == R.id.feedback) {
//
//            Intent intent2 = new Intent(ManageAccountsScreen.this, FeedbackScreen.class);
//            overridePendingTransition(0, 0);
//            startActivity(intent2);
//            finish();
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//        return true;
//    }

    public void showDilogForErrorForLogin(String msg, String header) {

        dialog = new Dialog(ManageAccountsScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent i = new Intent(ManageAccountsScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(i);
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();


    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(ManageAccountsScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        btn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


    private void showDialog(String message) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Logout:");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

//                constants.editor.clear();
//                constants.editor.commit();
//                constants.editor.apply();
//                constants.nonSambaCredentialsEditor.clear();
//                constants.nonSambaCredentialsEditor.commit();
//                constants.nonSambaCredentialsEditor.apply();
//                constants.destinationAccountEditor.clear().commit();
//                constants.destinationAccountEditor.apply();
//                constants.sourceAccountEditor.clear().commit();
//                constants.sourceAccountEditor.apply();
//                constants.accountListEditor.clear().commit();
//                constants.accountListEditor.apply();
//                constants.accountsEditor.clear().commit();
//                constants.accountsEditor.apply();
//                constants.forgotPasswordEditor.clear().commit();
//                constants.forgotPasswordEditor.apply();
//                Handler handler1 = new Handler();
//                handler1.postDelayed(new Runnable() {
//                    public void run() {
//
//                        pd.dismiss();
//                        Intent intent = new Intent(ManageAccountsScreen.this, LoginScreen.class);
//                        startActivity(intent);
//                        overridePendingTransition(0, 0);
//                        finish();
//                        finishAffinity();
//
//                    }
//                }, 1000);
                Logout();
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                pd.dismiss();
            }
        });

        builder.show();
    }

    public void Logout() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSessionID", Constants.Shared_KEY);
                    HttpsTrustManager.allowMySSL(ManageAccountsScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.logoutURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Code").equals("00")) {
                                                constants.editor.clear();
                                                constants.editor.commit();
                                                constants.editor.apply();
                                                constants.nonSambaCredentialsEditor.clear();
                                                constants.nonSambaCredentialsEditor.commit();
                                                constants.nonSambaCredentialsEditor.apply();
                                                constants.destinationAccountEditor.clear().commit();
                                                constants.destinationAccountEditor.apply();
                                                constants.sourceAccountEditor.clear().commit();
                                                constants.sourceAccountEditor.apply();
                                                constants.accountListEditor.clear().commit();
                                                constants.accountListEditor.apply();
                                                constants.accountsEditor.clear().commit();
                                                constants.accountsEditor.apply();
                                                constants.forgotPasswordEditor.clear().commit();
                                                constants.forgotPasswordEditor.apply();
                                                constants.purposeBillList.clear().commit();
                                                constants.purposeBillList.apply();
                                                constants.purposePrepaidList.clear().commit();
                                                constants.purposePrepaidList.apply();
                                                constants.purposeListEditor.clear().commit();
                                                constants.purposeListEditor.apply();


                                                pd.dismiss();
                                                Intent intent = new Intent(ManageAccountsScreen.this, LoginScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                finish();
                                                finishAffinity();


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Description"), "ERROR", ManageAccountsScreen.this, new LoginScreen());

                                            }

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sSessionID", Constants.Shared_KEY);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity", "WARNING");


            }
        } catch (Exception e) {

            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity", "WARNING");


        }
    }
}
