package com.ceesolutions.samba.transactions.Model;

import proguard.annotation.Keep;

/**
 * Created by ceeayaz on 4/13/18.
 */
@Keep
public class AccountDetails {
    private String title;
    private String dailyLimit;
    private String usedLimit;
    private String remainingLimit;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDailyLimit() {
        return dailyLimit;
    }

    public void setDailyLimit(String dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    public String getUsedLimit() {
        return usedLimit;
    }

    public void setUsedLimit(String usedLimit) {
        this.usedLimit = usedLimit;
    }

    public String getRemainingLimit() {
        return remainingLimit;
    }

    public void setRemainingLimit(String remainingLimit) {
        this.remainingLimit = remainingLimit;
    }
}
