package com.ceesolutions.samba.transactions;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.transactions.Adapters.CustomListAdapter;
import com.ceesolutions.samba.transactions.Model.Account;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 3/14/18.
 */

public class SelectAccountScreen extends AppCompatActivity {


    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private ListView listView;
    private CustomListAdapter customListAdapter;
    private ArrayList<Account> userModelArrayList;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private String text, type;
    private TextView mainTxt;
    private ImageView backBtn;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_account);
        initViews();
        getSupportActionBar().hide();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }


    public void initViews() {

        mainTxt = (TextView) findViewById(R.id.mainTxt);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        Intent intent = getIntent();
        text = intent.getStringExtra("source");


        if (text.equals("source")) {

            mainTxt.setText("Source Accounts");
        } else {

            mainTxt.setText("Destination Accounts");
        }
        try {
            CryptLib _crypt = new CryptLib();
            listView = (ListView) findViewById(R.id.listView);
            userModelArrayList = new ArrayList<>();
            utils = new Utils(SelectAccountScreen.this);
            requestQueue = Volley.newRequestQueue(SelectAccountScreen.this);
            helper = Helper.getHelper(SelectAccountScreen.this);
            constants = Constants.getConstants(SelectAccountScreen.this);
            pd = new Dialog(SelectAccountScreen.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            String list = _crypt.decrypt(constants.accountList.getString("accounts", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            type = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            if (list.equals("N/A"))
                GetAccountsList();
            else {
                Gson gson = new Gson();
                Type type1 = new TypeToken<ArrayList<Account>>() {
                }.getType();
                userModelArrayList = gson.fromJson(list, type1);
                customListAdapter = new CustomListAdapter(userModelArrayList, SelectAccountScreen.this, text);
                listView.setAdapter(customListAdapter);
            }

            appPreferences = new AppPreferences(this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");

            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void GetAccountsList() {

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {
                //135821
                //constants.sharedPreferences.getString("T24_CIF","N/A")
//                webService.GetAccountList(constants.sharedPreferences.getString("userName", "N/A"), constants.sharedPreferences.getString("T24_CIF", "N/A"), constants.sharedPreferences.getString("type", "N/A"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getGetAccountsList();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
//
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                            } else {
//
//                                try {
//
//
//                                    JSONArray jsonArray = new JSONArray(jsonObject.getString("AccountList"));
//                                    for (int i = 0; i < jsonArray.length(); i++) {
//                                        try {
//
//                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
//
//                                            UserModel userModel = new UserModel();
//                                            userModel.setUserName(jsonObj.getString("AccountTitle"));
//                                            userModel.setAccountNumber(jsonObj.getString("AccountID"));
//                                            userModel.setBranchName(jsonObj.getString("BranchName"));
//                                            userModel.setAccountType(jsonObj.getString("Type"));
//                                            userModel.setBalance(jsonObj.getString("AvailBal"));
//                                            userModel.setBankID(jsonObj.getString("BankCode"));
//                                            userModel.setBranchCode(jsonObj.getString("BranchCode"));
//                                            userModel.setCurrencyType(jsonObj.getString("Currency"));
//                                            userModelArrayList.add(userModel);
//
//                                        } catch (Exception ex) {
//                                            ex.printStackTrace();
//                                            pd.dismiss();
//                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                        }
//
//                                    }
//                                    customListAdapter = new CustomListAdapter(userModelArrayList, SelectAccountScreen.this, text);
//                                    listView.setAdapter(customListAdapter);
//                                    pd.dismiss();
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                }
//                            }
//                        } else {
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//                        }
//                    }
//                }, 5000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sT24_CIF", _crypt.decrypt(constants.sharedPreferences.getString("T24_CIF", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sSigns_UserType", type);
                params.put("sIsAllAccounts", "1");
                params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sLegalIdentityType", _crypt.decrypt(constants.sharedPreferences.getString("LegalType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sServerSessionKey", md5);

                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                HttpsTrustManager.allowMySSL(SelectAccountScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAccountListURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("00")) {

                                            try {
//
//
                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("AccountList"));
                                                if (jsonArray.length() > 0) {
                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                        try {


                                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                            Account account = new Account();
                                                            account.setAccountNumber(jsonObj.getString("AccountID"));
                                                            account.setAccountName(jsonObj.getString("AccountTitle"));
                                                            account.setUserName(jsonObj.getString("AccountTitle"));
                                                            account.setBranchName(jsonObj.getString("BranchName"));
                                                            account.setAccountType(jsonObj.getString("Type"));
                                                            account.setAvailBal(jsonObj.getString("AvailBal"));
                                                            account.setBankCode(jsonObj.getString("BankCode"));
                                                            account.setBankID(jsonObj.getString("BankCode"));
                                                            account.setBranchCode(jsonObj.getString("BranchCode"));
                                                            account.setCurrency(jsonObj.getString("Currency"));
                                                            account.setCurrencyType(jsonObj.getString("Currency"));
                                                            account.setProductType(jsonObj.getString("Product"));
                                                            account.setBankColor(jsonObj.getString("BankColorCode"));
                                                            account.setIsActive(jsonObj.getString("Active"));
                                                            account.setId(jsonObj.getString("ID"));
                                                            account.setBranchShortName(jsonObj.getString("BranchNameShort"));
                                                            if (jsonObj.has("ACCOUNT_ALIAS")) {
                                                                account.setAlias(jsonObj.getString("ACCOUNT_ALIAS"));
                                                            }
                                                            userModelArrayList.add(account);

                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                            pd.dismiss();
                                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        }

                                                    }
                                                    customListAdapter = new CustomListAdapter(userModelArrayList, SelectAccountScreen.this, text);
                                                    listView.setAdapter(customListAdapter);
                                                    pd.dismiss();
                                                } else {

                                                    pd.dismiss();
                                                }
                                            } catch (JSONException ex) {

                                                ex.printStackTrace();
                                                pd.dismiss();
                                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                            }

                                        } else if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "ERROR", SelectAccountScreen.this, new LoginScreen());


                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "ERROR");
                                        }
                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sT24_CIF", constants.sharedPreferences.getString("T24_CIF", "N/A"));
//                        params.put("sSigns_UserType", type);
//                        params.put("sIsAllAccounts", "1");
//                        params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sLegalIdentityType", constants.sharedPreferences.getString("LegalType", "N/A"));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}