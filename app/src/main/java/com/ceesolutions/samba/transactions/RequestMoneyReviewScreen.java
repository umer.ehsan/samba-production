package com.ceesolutions.samba.transactions;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.CircleImageView;
import com.ceesolutions.samba.views.EnglishNumberToWords;
import com.ceesolutions.samba.views.RoundImageView;
import com.google.firebase.analytics.FirebaseAnalytics;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ceeayaz on 3/27/18.
 */

public class RequestMoneyReviewScreen extends AppCompatActivity {


    private ExpandableLayout expandableLayout0;
    private RelativeLayout firstRow;
    private TextView message, mainTxt, textView, doneBtn, heading, date, userNameTxt, accMaskTxt, friendNameTxt, accFrindMaskTxt, accFrindEmailTxt, accFrindMobileTxt, totalAmountTxt, amountInWordsTxt, purposeHeadingTxt, remarksHeadingTxt, conversionHeadingTxt, chargesHeadingTxt, reviewBtn;
    private ImageView backBtn, cancelBtn;
    boolean flag = false;
    private Dialog dialog, dialog1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private String amountFinal, amountWords, finalDate, currentTime;
    private Intent intent1;
    private Date currentDate;
    private CircleImageView profile_image;
    private RoundImageView titleImage;
    private TextView titleTxt;
    private int color;
    private AppPreferences appPreferences;
    private String location;
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    String userType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_money_review);
        initViews();
        getSupportActionBar().hide();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDilogForErrorForLogin("Are you sure you want to cancel the transaction?","WARNING");
                Intent intent = new Intent(RequestMoneyReviewScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
                finishAffinity();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                finish();
            }
        });

        reviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reviewBtn.setEnabled(false);


                if (userType.equals("NON_SAMBA")) {

                    FirebaseAnalytics.getInstance(RequestMoneyReviewScreen.this).logEvent("Non_Samba_Request_Money_Request_Pressed", new Bundle());

                } else {

                    FirebaseAnalytics.getInstance(RequestMoneyReviewScreen.this).logEvent("Request_Money_Request_Pressed", new Bundle());
                }



                SendRequest();
            }
        });
    }

    public void initViews() {

        date = (TextView) findViewById(R.id.date);
        profile_image = (CircleImageView) findViewById(R.id.profile_image);
        userNameTxt = (TextView) findViewById(R.id.userNameTxt);
        friendNameTxt = (TextView) findViewById(R.id.friendNameTxt);
        accFrindMaskTxt = (TextView) findViewById(R.id.accFrindMaskTxt);
        totalAmountTxt = (TextView) findViewById(R.id.totalAmountTxt);
        accFrindMobileTxt = (TextView) findViewById(R.id.accFrindMobileTxt);
        amountInWordsTxt = (TextView) findViewById(R.id.amountInWordsTxt);
        purposeHeadingTxt = (TextView) findViewById(R.id.purposeHeadingTxt);
        remarksHeadingTxt = (TextView) findViewById(R.id.remarksHeadingTxt);
        conversionHeadingTxt = (TextView) findViewById(R.id.conversionHeadingTxt);
        chargesHeadingTxt = (TextView) findViewById(R.id.chargesHeadingTxt);
        reviewBtn = (TextView) findViewById(R.id.reviewBtn);
        cancelBtn = (ImageView) findViewById(R.id.cancelBtn);
        titleTxt = (TextView) findViewById(R.id.titleTxt);
        titleImage = (RoundImageView) findViewById(R.id.titleImage);
        titleTxt.bringToFront();

        requestQueue = Volley.newRequestQueue(RequestMoneyReviewScreen.this);
        webService = new WebService();
        helper = Helper.getHelper(RequestMoneyReviewScreen.this);
        constants = Constants.getConstants(RequestMoneyReviewScreen.this);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        utils = new Utils(RequestMoneyReviewScreen.this);
        mainTxt = (TextView) findViewById(R.id.mainTxt);
        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        firstRow = (RelativeLayout) findViewById(R.id.firstRow);
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        currentDate = Calendar.getInstance().getTime();
        formatter = new SimpleDateFormat("dd-MMM-yyyy");
        date.setText(formatter.format(currentDate));
        finalDate = formatter.format(currentDate);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
        currentTime = sdf.format(currentDate);
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        try {
            CryptLib _crypt = new CryptLib();
            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        firstRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!flag) {
                    expandableLayout0.expand();
                    mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_less, 0);
                    flag = true;
                } else {
                    expandableLayout0.collapse();
                    mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_more, 0);
                    flag = false;
                }

            }
        });


        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Intent intent = getIntent();
        intent1 = intent;
        userNameTxt.setText(intent.getStringExtra("destionationName"));
        purposeHeadingTxt.setText(intent.getStringExtra("purpose"));
        friendNameTxt.setText(intent.getStringExtra("userName"));
        accFrindMaskTxt.setText(intent.getStringExtra("accountNumber"));
        remarksHeadingTxt.setText(intent.getStringExtra("remarks"));
        String friendImage = intent.getStringExtra("friendImg");
        String s = intent.getStringExtra("destionationName").substring(0, 1);

        switch (s.toUpperCase()) {

            case "A":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "B":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "C":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "D":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "E":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "F":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "G":
                color = getResources().getColor(R.color.friendColor1);
                break;

            case "H":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "I":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "J":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "K":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "L":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "M":
                color = getResources().getColor(R.color.friendColor2);
                break;

            case "N":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "O":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "P":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "Q":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "R":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "S":
                color = getResources().getColor(R.color.friendColor3);
                break;

            case "T":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "U":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "V":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "W":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "X":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "Y":
                color = getResources().getColor(R.color.friendColor4);
                break;

            case "Z":
                color = getResources().getColor(R.color.friendColor4);
                break;

            default:
                color = getResources().getColor(R.color.defaultColor);
                break;
        }
        if (!TextUtils.isEmpty(friendImage)) {
            byte[] imageByteArray = Base64.decode(friendImage, Base64.DEFAULT);
            RequestOptions options = new RequestOptions();
            options.centerCrop();
            titleTxt.setVisibility(View.INVISIBLE);
            titleImage.setVisibility(View.INVISIBLE);
            Glide.with(RequestMoneyReviewScreen.this)
                    .load(imageByteArray)
                    .apply(options)
                    .into(profile_image);

        } else {

            profile_image.setVisibility(View.INVISIBLE);
            titleImage.setBackgroundTintList(ColorStateList.valueOf((color)));
            titleTxt.setText(s);
        }
        int amount = Integer.valueOf(intent.getStringExtra("amount"));
//        String amountCode = intent.getStringExtra("currency");
//        if (!amountCode.equals("N/A")) {
//            totalAmountTxt.setText(amountCode + " " + getFormatedAmount(amount));
//            amountFinal = amountCode + " " + getFormatedAmount(amount);
//        } else {
        totalAmountTxt.setText("PKR" + " " + getFormatedAmount(amount) + ".00");
        amountFinal = "PKR" + " " + getFormatedAmount(amount) + ".00";
//        }
        String amountInWords = EnglishNumberToWords.convert(amount);
        amountInWordsTxt.setText("PKR " + amountInWords + " only");
        amountWords = "PKR " + amountInWords + " only";


    }

    public void showDilogForError(String msg, String header) {

        dialog1 = new Dialog(RequestMoneyReviewScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog1.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog1.findViewById(R.id.doneBtn);
        heading = (TextView) dialog1.findViewById(R.id.heading);
//        dialog.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog1.show();

    }


    private String getFormatedAmount(int amount) {
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }


    public void SendRequest() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                dialog.show();
//                webService.SendRequest(intent1.getStringExtra("userID"), intent1.getStringExtra("mobile"), intent1.getStringExtra("friendID"), intent1.getStringExtra("friendNumber"), intent1.getStringExtra("destionationName"), intent1.getStringExtra("accountNumber"), intent1.getStringExtra("userName"), intent1.getStringExtra("purpose"), intent1.getStringExtra("remarks"), intent1.getStringExtra("amount"), intent1.getStringExtra("flag"), intent1.getStringExtra("bankCode"), intent1.getStringExtra("currency"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getReqestMoney();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//                                dialog.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "Invalid Username/iPIN!", Toast.LENGTH_LONG).show();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                reviewBtn.setEnabled(true);
//
//
//                            } else {
//
//                                Intent intent = new Intent(RequestMoneyReviewScreen.this, RequestMoneyConfirmationScreen.class);
//                                intent.putExtra("userName", intent1.getStringExtra("userName"));
//                                intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
//                                intent.putExtra("purpose", intent1.getStringExtra("purpose"));
//                                intent.putExtra("destionationName", intent1.getStringExtra("destionationName"));
//                                intent.putExtra("friendNumber", intent1.getStringExtra("friendNumber"));
//                                intent.putExtra("remarks", intent1.getStringExtra("remarks"));
//                                intent.putExtra("amount", intent1.getStringExtra("amount"));
//                                intent.putExtra("amountFinal", amountFinal);
//                                intent.putExtra("amountWords", amountWords);
//                                intent.putExtra("currency", intent1.getStringExtra("currency"));
//                                intent.putExtra("stan", intent1.getStringExtra("stan"));
//                                intent.putExtra("rrn", intent1.getStringExtra("rrn"));
//                                intent.putExtra("beneType", intent1.getStringExtra("beneType"));
//                                intent.putExtra("friendImg", intent1.getStringExtra("friendImg"));
//                                intent.putExtra("currentDate", finalDate);
//                                intent.putExtra("currentTime", currentTime);
//                                overridePendingTransition(0, 0);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(intent);
//                                constants.accountsEditor.clear();
//                                constants.accountsEditor.commit();
//                                constants.accountsEditor.apply();
//                                constants.destinationAccountEditor.clear();
//                                constants.destinationAccountEditor.commit();
//                                constants.destinationAccountEditor.apply();
//                                dialog.dismiss();
//                                finish();
//                                finishAffinity();
//                                reviewBtn.setEnabled(true);
//
//                            }
//                        } else {
//                            dialog.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            reviewBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 5000);

                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);

                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sRequester_id", intent1.getStringExtra("userID"));
                params.put("sRequester_number", intent1.getStringExtra("mobile"));
                params.put("sFriend_number", intent1.getStringExtra("friendNumber"));
                params.put("sFriend_id", intent1.getStringExtra("friendID"));
                params.put("sTo_account_number", intent1.getStringExtra("accountNumber"));
                params.put("sTo_account_title", intent1.getStringExtra("userName"));
                params.put("sRequest_purpose", intent1.getStringExtra("purpose"));
                params.put("sRequest_remarks", intent1.getStringExtra("remarks"));
                params.put("sFriend_name", intent1.getStringExtra("destionationName"));
                params.put("sRequest_amount", intent1.getStringExtra("amount"));
                params.put("sShow_account_number", intent1.getStringExtra("flag"));
                params.put("sRequester_bankcode", intent1.getStringExtra("bankCode"));
                params.put("sCurrency", intent1.getStringExtra("currency"));
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sSigns_Username", user);
                params.put("sServerSessionKey", md5);

                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());


                HttpsTrustManager.allowMySSL(RequestMoneyReviewScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.requestMoneyURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("request_moneyResult").getString("Status_Code").equals("00")) {


                                            Intent intent = new Intent(RequestMoneyReviewScreen.this, RequestMoneyConfirmationScreen.class);
                                            intent.putExtra("userName", intent1.getStringExtra("userName"));
                                            intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                                            intent.putExtra("purpose", intent1.getStringExtra("purpose"));
                                            intent.putExtra("destionationName", intent1.getStringExtra("destionationName"));
                                            intent.putExtra("friendNumber", intent1.getStringExtra("friendNumber"));
                                            intent.putExtra("remarks", intent1.getStringExtra("remarks"));
                                            intent.putExtra("amount", intent1.getStringExtra("amount"));
                                            intent.putExtra("amountFinal", amountFinal);
                                            intent.putExtra("amountWords", amountWords);
                                            intent.putExtra("currency", intent1.getStringExtra("currency"));
                                            intent.putExtra("stan", intent1.getStringExtra("stan"));
                                            intent.putExtra("rrn", intent1.getStringExtra("rrn"));
                                            intent.putExtra("beneType", intent1.getStringExtra("beneType"));
                                            intent.putExtra("friendImg", intent1.getStringExtra("friendImg"));
                                            intent.putExtra("currentDate", finalDate);
                                            intent.putExtra("currentTime", currentTime);
                                            overridePendingTransition(0, 0);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                            constants.accountsEditor.clear();
                                            constants.accountsEditor.commit();
                                            constants.accountsEditor.apply();
                                            constants.destinationAccountEditor.clear();
                                            constants.destinationAccountEditor.commit();
                                            constants.destinationAccountEditor.apply();
                                            dialog.dismiss();
                                            finish();
                                            finishAffinity();
                                            reviewBtn.setEnabled(true);
                                        } else if (jsonObject.getJSONObject("request_moneyResult").getString("Status_Code").equals("54")) {
                                            dialog.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("request_moneyResult").getString("Status_Description"), "ERROR", RequestMoneyReviewScreen.this, new LoginScreen());
                                            reviewBtn.setEnabled(true);

                                        } else {

                                            dialog.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("request_moneyResult").getString("Status_Description"), "ERROR");
                                            reviewBtn.setEnabled(true);
                                        }
                                    } else {

                                        dialog.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        reviewBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    FirebaseAnalytics.getInstance(RequestMoneyReviewScreen.this).logEvent("Error", new Bundle());
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    reviewBtn.setEnabled(true);
                                }


                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sRequester_id", intent1.getStringExtra("userID"));
//                        params.put("sRequester_number", intent1.getStringExtra("mobile"));
//                        params.put("sFriend_number", intent1.getStringExtra("friendNumber"));
//                        params.put("sFriend_id", intent1.getStringExtra("friendID"));
//                        params.put("sTo_account_number", intent1.getStringExtra("accountNumber"));
//                        params.put("sTo_account_title", intent1.getStringExtra("userName"));
//                        params.put("sRequest_purpose", intent1.getStringExtra("purpose"));
//                        params.put("sRequest_remarks", intent1.getStringExtra("remarks"));
//                        params.put("sFriend_name", intent1.getStringExtra("destionationName"));
//                        params.put("sRequest_amount", intent1.getStringExtra("amount"));
//                        params.put("sShow_account_number", intent1.getStringExtra("flag"));
//                        params.put("sRequester_bankcode", intent1.getStringExtra("bankCode"));
//                        params.put("sCurrency", intent1.getStringExtra("currency"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                reviewBtn.setEnabled(true);


            }
        } catch (Exception e) {
            dialog.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            reviewBtn.setEnabled(true);


        }
    }


//    public void showDilogForErrorForLogin(String msg, String header) {
//
//        dialog = new Dialog(RequestMoneyReviewScreen.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.error_dialog);
//        message = (TextView) dialog.findViewById(R.id.validatingTxt);
//        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
//        heading = (TextView) dialog.findViewById(R.id.heading);
//        dialog.setCancelable(true);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        doneBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                dialog.dismiss();
//                finish();
//
//            }
//        });
//        message.setText(msg);
//        heading.setText(header);
//        dialog.show();
//
//
//    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");
        finish();
    }
}