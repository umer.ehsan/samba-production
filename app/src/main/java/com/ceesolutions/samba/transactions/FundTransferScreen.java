package com.ceesolutions.samba.transactions;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.transactions.Adapters.FundTransferAdapter;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.TapTarget;
import com.ceesolutions.samba.utils.TapTargetView;

/**
 * Created by ceeayaz on 3/9/18.
 */

public class FundTransferScreen extends AppCompatActivity {

    private ImageView backBtn, notificationBtn;

    public static ViewPager viewPager;
    public Intent intent;
    private String type;
    private Constants constants;
    private ImageView helpBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fund_transfer_screen);
        getSupportActionBar().hide();
//        TextView textView = (TextView) findViewById(R.id.text);
//        textView.bringToFront();
        intent = getIntent();
        try{
            CryptLib _crypt = new CryptLib();
        backBtn = (ImageView) findViewById(R.id.backBtn);
        helpBtn = (ImageView) findViewById(R.id.helpBtn);
        constants = Constants.getConstants(FundTransferScreen.this);
        type = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String bene = intent.getStringExtra("addBene");
                if (!TextUtils.isEmpty(bene) && bene.equals("addBene")) {

                    Intent intent = new Intent(FundTransferScreen.this, HomeScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                    finish();
                } else {
//                Intent intent = new Intent(FundTransferScreen.this, HomeScreen.class);
//                overridePendingTransition(0, 0);
//                startActivity(intent);
                    finish();
                }
            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FundTransferScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
            }
        });

//        constants = Constants.getConstants(FundTransferScreen.this);
//        constants.sourceAccountEditor.putString("isSelect", "No");
//        constants.sourceAccountEditor.commit();
//        constants.sourceAccountEditor.apply();
//        constants.destinationAccountEditor.putString("isSelect", "No");
//        constants.destinationAccountEditor.commit();
//        constants.destinationAccountEditor.apply();
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Move Money"));
        tabLayout.addTab(tabLayout.newTab().setText("Send Money"));
        tabLayout.addTab(tabLayout.newTab().setText("Request Money"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) findViewById(R.id.pager);
        final FundTransferAdapter adapter = new FundTransferAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (!TextUtils.isEmpty(type) && type.equals("SAMBA")) {
                    viewPager.setCurrentItem(tab.getPosition());
                } else {


                    viewPager.setCurrentItem(tab.getPosition());
                }
//                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        String request = intent.getStringExtra("requestFunds");
        if (!TextUtils.isEmpty(request)) {
            if (intent.getStringExtra("requestFunds").equals("requestFunds")) {

                viewPager.setCurrentItem(2);
            } else if (intent.getStringExtra("requestFunds").equals("requestMoney")) {
                viewPager.setCurrentItem(1);
            } else if (intent.getStringExtra("requestFunds").equals("sendMoney")) {
                viewPager.setCurrentItem(1);
            } else if (intent.getStringExtra("requestFunds").equals("sendMoneyFromHome")) {

                viewPager.setCurrentItem(1);

            } else if (intent.getStringExtra("requestFunds").equals("moveMoneyFromHome")) {

                viewPager.setCurrentItem(0);
            } else if (intent.getStringExtra("requestFunds").equals("nonsambarequest")) {

                viewPager.setCurrentItem(2);
            } else if (intent.getStringExtra("requestFunds").equals("friendRequest")) {

                viewPager.setCurrentItem(2);
            } else if (intent.getStringExtra("requestFunds").equals("nonSamba")) {

                viewPager.setCurrentItem(2);
            }
            else if (intent.getStringExtra("requestFunds").equals("transfer")) {

                viewPager.setCurrentItem(1);
            } else if (intent.getStringExtra("requestFunds").equals("chatbot")) {

                viewPager.setCurrentItem(2);
            }
        }


        String coachMarks = constants.coachMarks.getString("fundTransfer", "N/A");

//        if (!TextUtils.isEmpty(coachMarks) && coachMarks.equals("N/A")) {
//            constants.coachMarksEditor.putString("fundTransfer","1");
//            constants.coachMarksEditor.commit();
//            constants.coachMarksEditor.apply();
//            int mainTxt = getResources().getDimensionPixelSize(R.dimen._7sdp);
//            int des = getResources().getDimensionPixelSize(R.dimen._4sdp);
//            TapTargetView.showFor(FundTransferScreen.this,                 // `this` is an Activity
//                    TapTarget.forView(findViewById(R.id.helpBtn), "", "- Move money:  Move funds between your own linked Samba accounts – your own personal / joint accounts\n\n" +
//                            "- Send Money:  Transfer funds to any Samba’s account or any other bank account(IBFT) you have added as Beneficiaries  \n\n" +
//                            "- Request Money:  A unique feature that gives you a facility to request money from your registered friend. While Requesting money, Friend, has to be a Samba Customer / Account holder")
//                            // All options below are optional
//                            .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
//                            .outerCircleAlpha(0.90f)
//                            // Specify the alpha amount for the outer circle
//                            // Specify a color for the target circle
//                            .titleTextSize(mainTxt)// Specify the size (in sp) of the title text
//                            .titleTextColor(R.color.white)      // Specify the color of the title text
//                            .descriptionTextSize(des)            // Specify the size (in sp) of the description text
//                            .descriptionTextColor(R.color.white)  // Specify the color of the description text
//                            .textColor(R.color.white)            // Specify a color for both the title and description text
//                            .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
//                            // If set, will dim behind the view with 30% opacity of the given color
//                            .drawShadow(true)                   // Whether to draw a drop shadow or not
//                            .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
//                            .tintTarget(true)                   // Whether to tint the target view's color
//                            .transparentTarget(false),           // Specify whether the target is transparent (displays the content underneath)
//                    // Specify a custom drawable to draw as the target
//                    // Specify the target radius (in dp)
//                    new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
//                        @Override
//                        public void onTargetClick(TapTargetView view) {
//                            super.onTargetClick(view);      // This call is optional
////                        doSomething();
//                        }
//                    });
//        }

        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int mainTxt = getResources().getDimensionPixelSize(R.dimen._7sdp);
                int des = getResources().getDimensionPixelSize(R.dimen._4sdp);

                TapTargetView.showFor(FundTransferScreen.this,                 // `this` is an Activity
                        TapTarget.forView(findViewById(R.id.helpBtn), "About this Screen!!!", "- Move money:  Move funds between your own linked Samba accounts – your own personal / joint accounts\n\n" +
                                "- Send Money:  transfer funds between other Samba Bank or any other bank customer (IBFT) you have added as Beneficiaries \n\n" +
                                "- Request Money:  a unique feature that gives you a facility to request money from your registered friend. While Requesting money, Friend, has to be a Samba Customer / Account holder")
                                // All options below are optional
                                .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                .outerCircleAlpha(0.90f)
                                // Specify the alpha amount for the outer circle
                                // Specify a color for the target circle
                                .titleTextSize(mainTxt)// Specify the size (in sp) of the title text
                                .titleTextColor(R.color.white)      // Specify the color of the title text
                                .descriptionTextSize(des)            // Specify the size (in sp) of the description text
                                .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                .textColor(R.color.white)            // Specify a color for both the title and description text
                                .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                                // If set, will dim behind the view with 30% opacity of the given color
                                .drawShadow(true)                   // Whether to draw a drop shadow or not
                                .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                .tintTarget(true)                   // Whether to tint the target view's color
                                .transparentTarget(false),           // Specify whether the target is transparent (displays the content underneath)
                        // Specify a custom drawable to draw as the target
                        // Specify the target radius (in dp)
                        new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                            @Override
                            public void onTargetClick(TapTargetView view) {
                                super.onTargetClick(view);      // This call is optional
//                        doSomething();

                            }
                        });
            }
        });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        String bene = intent.getStringExtra("addBene");
        if (!TextUtils.isEmpty(bene) && bene.equals("addBene")) {

            Intent intent = new Intent(FundTransferScreen.this, HomeScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);
            finish();
        } else {
//                Intent intent = new Intent(FundTransferScreen.this, HomeScreen.class);
//                overridePendingTransition(0, 0);
//                startActivity(intent);
            finish();
        }
    }
}