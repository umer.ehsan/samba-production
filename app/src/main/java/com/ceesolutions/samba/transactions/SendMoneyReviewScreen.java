package com.ceesolutions.samba.transactions;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.EnglishNumberToWords;
import com.google.firebase.analytics.FirebaseAnalytics;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ceeayaz on 3/13/18.
 */

public class SendMoneyReviewScreen extends AppCompatActivity {


    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    boolean flag = false;
    private ExpandableLayout expandableLayout0;
    private RelativeLayout firstRow;
    private TextView agreementTxt, message, mainTxt, textView, doneBtn, heading, date, userNameTxt, accMaskTxt, friendNameTxt, accFrindMaskTxt, accFrindEmailTxt, accFrindMobileTxt, totalAmountTxt, amountInWordsTxt, purposeHeadingTxt, remarksHeadingTxt, conversionHeadingTxt, chargesHeadingTxt, reviewBtn;
    private ImageView imageBtn, backBtn, cancelBtn, notificationBtn;
    private Dialog dialog, dialog1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private String amountFinal, amountWords, finalDate, currentTime;
    private Intent intent1;
    private Date currentDate;
    private AppPreferences appPreferences;
    private String location, stan;
    private RelativeLayout middle;
    private boolean isChecked = false, isRequest = false, isEmail = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_money_review);
        initViews();
        getSupportActionBar().hide();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");
                Intent intent = new Intent(SendMoneyReviewScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
                finishAffinity();

            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        reviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewBtn.setEnabled(false);

                FirebaseAnalytics.getInstance(SendMoneyReviewScreen.this).logEvent("Send_Money_Transfer_Pressed", new Bundle());

                if (!isRequest)
                    GenerateiPIN();
                else {
                    if (isChecked) {
                        GenerateiPIN();
                    } else {
                        utils.showDilogForError("Please \"Accept\" to proceed further.", "WARNING");
                        reviewBtn.setEnabled(true);
                    }
                }
            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SendMoneyReviewScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
            }
        });
    }

    public void initViews() {

        date = (TextView) findViewById(R.id.date);
        userNameTxt = (TextView) findViewById(R.id.userNameTxt);
        accMaskTxt = (TextView) findViewById(R.id.accMaskTxt);
        friendNameTxt = (TextView) findViewById(R.id.friendNameTxt);
        accFrindMaskTxt = (TextView) findViewById(R.id.accFrindMaskTxt);
        accFrindEmailTxt = (TextView) findViewById(R.id.accFrindEmailTxt);
        accFrindMobileTxt = (TextView) findViewById(R.id.accFrindMobileTxt);
        totalAmountTxt = (TextView) findViewById(R.id.totalAmountTxt);
        accFrindMobileTxt = (TextView) findViewById(R.id.accFrindMobileTxt);
        amountInWordsTxt = (TextView) findViewById(R.id.amountInWordsTxt);
        purposeHeadingTxt = (TextView) findViewById(R.id.purposeHeadingTxt);
        remarksHeadingTxt = (TextView) findViewById(R.id.remarksHeadingTxt);
        conversionHeadingTxt = (TextView) findViewById(R.id.conversionHeadingTxt);
        chargesHeadingTxt = (TextView) findViewById(R.id.chargesHeadingTxt);
        agreementTxt = (TextView) findViewById(R.id.agreementTxt);
        reviewBtn = (TextView) findViewById(R.id.reviewBtn);
        cancelBtn = (ImageView) findViewById(R.id.cancelBtn);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        imageBtn = (ImageView) findViewById(R.id.imageBtn);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        mainTxt = (TextView) findViewById(R.id.mainTxt);
        middle = (RelativeLayout) findViewById(R.id.middle);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(SendMoneyReviewScreen.this);
        webService = new WebService();
        helper = Helper.getHelper(this);
        int stanNumber = 6;
        stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
        utils = new Utils(SendMoneyReviewScreen.this);
        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.US);
        currentDate = Calendar.getInstance().getTime();
        formatter = new SimpleDateFormat("dd-MMM-yyyy");
        date.setText(formatter.format(currentDate));
        finalDate = formatter.format(currentDate);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
        currentTime = sdf.format(currentDate);
        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        firstRow = (RelativeLayout) findViewById(R.id.firstRow);
        imageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isChecked) {
                    imageBtn.setBackground(getResources().getDrawable(R.drawable.checked));
                    isChecked = true;
                } else {
                    imageBtn.setBackground(getResources().getDrawable(R.drawable.normal));
                    isChecked = false;
                }

            }
        });
        firstRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!flag) {
                    expandableLayout0.expand();
                    mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_less, 0);
                    flag = true;
                } else {
                    expandableLayout0.collapse();
                    mainTxt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.details, 0, R.drawable.expand_more, 0);
                    flag = false;
                }

            }
        });

        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Intent intent = getIntent();
        intent1 = intent;
        userNameTxt.setText(intent.getStringExtra("userName"));


        String isShow = intent.getStringExtra("isShow");
        if (!TextUtils.isEmpty(isShow) && !isShow.equals("null") && !isShow.equals(null)) {

            middle.setVisibility(View.VISIBLE);
            isRequest = true;
            if (isShow.equals("1")) {
                friendNameTxt.setText(intent.getStringExtra("friendName"));
                accFrindMaskTxt.setText(intent.getStringExtra("destinationAccount"));
                accFrindEmailTxt.setText(intent.getStringExtra("titleFetch"));
                isEmail = true;

            } else if (isShow.equals("3")) {
                middle.setVisibility(View.GONE);
                isRequest = false;
                isEmail = true;
                if (!TextUtils.isEmpty(intent.getStringExtra("accTitle")))
                    friendNameTxt.setText(intent.getStringExtra("accTitle"));
                else
                    friendNameTxt.setText(intent.getStringExtra("destionationName"));

                accFrindEmailTxt.setText(intent.getStringExtra("titleFetch"));

                accFrindMaskTxt.setText(intent.getStringExtra("destinationAccount"));

            } else {
                friendNameTxt.setText(intent.getStringExtra("destionationName"));
                accFrindMaskTxt.setText(intent.getStringExtra("titleFetch"));
            }
        } else {
            if (!TextUtils.isEmpty(intent.getStringExtra("accTitle")))
                friendNameTxt.setText(intent.getStringExtra("accTitle"));
            else
                friendNameTxt.setText(intent.getStringExtra("destionationName"));
            accFrindMaskTxt.setText(intent.getStringExtra("destinationAccount"));
        }

        accMaskTxt.setText(intent.getStringExtra("accountNumber"));
        purposeHeadingTxt.setText(intent.getStringExtra("purpose"));


        remarksHeadingTxt.setText(intent.getStringExtra("remarks"));
        int amount = Integer.valueOf(intent.getStringExtra("amount"));
        String amountCode = intent.getStringExtra("currency");
        if (!amountCode.equals("N/A")) {
            totalAmountTxt.setText(amountCode + " " + getFormatedAmount(amount) + ".00");
            amountFinal = amountCode + " " + getFormatedAmount(amount) + ".00";
        } else {
            totalAmountTxt.setText("PKR" + " " + getFormatedAmount(amount) + ".00");
            amountFinal = "PKR" + " " + getFormatedAmount(amount) + ".00";
        }
        String amountInWords = EnglishNumberToWords.convert(amount);
        amountInWordsTxt.setText(amountCode + " " + amountInWords + " only");
        amountWords = amountCode + " " + amountInWords + " only";
        conversionHeadingTxt.setText(intent.getStringExtra("conversionRate"));
        chargesHeadingTxt.setText(intent.getStringExtra("chargeCode"));
        String email = intent.getStringExtra("email");
        if (!TextUtils.isEmpty(email)) {

//            accFrindEmailTxt.setText(intent.getStringExtra("email"));
            String title = intent.getStringExtra("acTitle");
            if (!TextUtils.isEmpty(title))
                accFrindEmailTxt.setText(intent.getStringExtra("acTitle").toUpperCase());
            else
                accFrindEmailTxt.setText(intent.getStringExtra("destionationName"));

        } else {

            if (!isRequest) {


                if (!isEmail) {
                    accFrindEmailTxt.setVisibility(View.INVISIBLE);
                }
            } else {

                if (!isEmail) {
                    accFrindEmailTxt.setVisibility(View.INVISIBLE);
                } else {


                }
            }
        }

        String mobile = intent.getStringExtra("mobile");
        if (!TextUtils.isEmpty(mobile)) {

            accFrindMobileTxt.setText(intent.getStringExtra("mobile"));
            accFrindMobileTxt.setVisibility(View.INVISIBLE);
        } else {

            accFrindMobileTxt.setVisibility(View.INVISIBLE);
        }

        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

    }

    public void showDilogForError(String msg, String header) {

        dialog1 = new Dialog(SendMoneyReviewScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog1.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog1.findViewById(R.id.doneBtn);
        heading = (TextView) dialog1.findViewById(R.id.heading);
//        dialog.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog1.show();

    }


    private String getFormatedAmount(int amount) {
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }


    public void GenerateiPIN() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                dialog.show();
//                webService.RegeneratePin(constants.sharedPreferences.getString("userName", "N/A"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getRegeneratePinReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//                                dialog.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "Invalid Username/iPIN!", Toast.LENGTH_LONG).show();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                reviewBtn.setEnabled(true);
//
//
//                            } else {
//
//                                Intent intent = new Intent(SendMoneyReviewScreen.this, PinVerificationScreen.class);
//                                intent.putExtra("userName", intent1.getStringExtra("userName"));
//                                intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
//                                intent.putExtra("purpose", intent1.getStringExtra("purpose"));
//                                intent.putExtra("destionationName", intent1.getStringExtra("destionationName"));
//                                intent.putExtra("destinationAccount", intent1.getStringExtra("destinationAccount"));
//                                intent.putExtra("remarks", intent1.getStringExtra("remarks"));
//                                intent.putExtra("amount", intent1.getStringExtra("amount"));
//                                intent.putExtra("branchCode", intent1.getStringExtra("branchCode"));
//                                intent.putExtra("bankCode", intent1.getStringExtra("bankCode"));
//                                intent.putExtra("amountFinal", amountFinal);
//                                intent.putExtra("amountWords", amountWords);
//                                intent.putExtra("currency", intent1.getStringExtra("currency"));
//                                intent.putExtra("type", "sendMoney");
//                                intent.putExtra("ftType", intent1.getStringExtra("ftType"));
//                                intent.putExtra("userNumber", intent1.getStringExtra("userNumber"));
//                                intent.putExtra("friendNumber", intent1.getStringExtra("friendNumber"));
//                                intent.putExtra("messageId", intent1.getStringExtra("messageId"));
//                                intent.putExtra("name", intent1.getStringExtra("name"));
//                                intent.putExtra("targetId", intent1.getStringExtra("targetId"));
//                                intent.putExtra("stan", intent1.getStringExtra("stan"));
//                                intent.putExtra("rrn", intent1.getStringExtra("rrn"));
//                                intent.putExtra("beneType", intent1.getStringExtra("beneType"));
//                                intent.putExtra("beneID", intent1.getStringExtra("beneID"));
//                                intent.putExtra("currentDate", finalDate);
//                                intent.putExtra("currentTime", currentTime);
//                                overridePendingTransition(0, 0);
//                                dialog.dismiss();
////                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(intent);
////                                constants.accountsEditor.clear();
////                                constants.accountsEditor.commit();
////                                constants.accountsEditor.apply();
////                                constants.destinationAccountEditor.clear();
////                                constants.destinationAccountEditor.commit();
////                                constants.destinationAccountEditor.apply();
////                                dialog.dismiss();
////                                finish();
////                                finishAffinity();
//                                reviewBtn.setEnabled(true);
//
//                            }
//                        } else {
//                            dialog.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            reviewBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 10000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Username", user);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);

                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());


                HttpsTrustManager.allowMySSL(SendMoneyReviewScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("00")) {
                                            Intent intent = new Intent(SendMoneyReviewScreen.this, PinVerificationScreen.class);
                                            intent.putExtra("userName", intent1.getStringExtra("userName"));
                                            intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                                            intent.putExtra("titleFetch", intent1.getStringExtra("titleFetch"));
                                            intent.putExtra("purpose", intent1.getStringExtra("purpose"));
                                            intent.putExtra("destionationName", intent1.getStringExtra("destionationName"));
                                            intent.putExtra("destinationAccount", intent1.getStringExtra("destinationAccount"));
                                            intent.putExtra("remarks", intent1.getStringExtra("remarks"));
                                            intent.putExtra("amount", intent1.getStringExtra("amount"));
                                            intent.putExtra("branchCode", intent1.getStringExtra("branchCode"));
                                            intent.putExtra("bankCode", intent1.getStringExtra("bankCode"));
                                            intent.putExtra("amountFinal", amountFinal);
                                            intent.putExtra("amountWords", amountWords);
                                            intent.putExtra("currency", intent1.getStringExtra("currency"));
                                            intent.putExtra("type", "sendMoney");
                                            intent.putExtra("ftType", intent1.getStringExtra("ftType"));
                                            intent.putExtra("userNumber", intent1.getStringExtra("userNumber"));
                                            intent.putExtra("friendNumber", intent1.getStringExtra("friendNumber"));
                                            intent.putExtra("messageId", intent1.getStringExtra("messageId"));
                                            intent.putExtra("name", intent1.getStringExtra("name"));
                                            intent.putExtra("targetId", intent1.getStringExtra("targetId"));
                                            intent.putExtra("stan", stan);
                                            intent.putExtra("rrn", intent1.getStringExtra("rrn"));
                                            intent.putExtra("beneType", intent1.getStringExtra("beneType"));
                                            intent.putExtra("beneID", intent1.getStringExtra("beneID"));
                                            intent.putExtra("currentDate", finalDate);
                                            intent.putExtra("currentTime", currentTime);
                                            intent.putExtra("isShow", intent1.getStringExtra("isShow"));
                                            overridePendingTransition(0, 0);
                                            dialog.dismiss();
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
//                                constants.accountsEditor.clear();
//                                constants.accountsEditor.commit();
//                                constants.accountsEditor.apply();
//                                constants.destinationAccountEditor.clear();
//                                constants.destinationAccountEditor.commit();
//                                constants.destinationAccountEditor.apply();
//                                dialog.dismiss();
//                                finish();
//                                finishAffinity();
                                            reviewBtn.setEnabled(true);
                                        } else if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("54")) {
                                            dialog.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR", SendMoneyReviewScreen.this, new LoginScreen());
                                            reviewBtn.setEnabled(true);

                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR");
                                            reviewBtn.setEnabled(true);
                                        }
                                    } else {
                                        dialog.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        reviewBtn.setEnabled(true);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    reviewBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    reviewBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    reviewBtn.setEnabled(true);
                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                reviewBtn.setEnabled(true);


            }
        } catch (Exception e) {
            dialog.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            reviewBtn.setEnabled(true);


        }
    }


    public void showDilogForErrorForLogin(String msg, String header) {

        dialog = new Dialog(SendMoneyReviewScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();


    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
//        showDilogForErrorForLogin("Are you sure you want to cancel the transaction?", "WARNING");
    }
}
