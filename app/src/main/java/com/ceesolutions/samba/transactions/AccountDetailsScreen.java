package com.ceesolutions.samba.transactions;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.Bookme.BookMeMainScreen;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.beneficiariesManagement.ManageBeneficiaresScreen;
import com.ceesolutions.samba.billPayments.PayBillsScreen;
import com.ceesolutions.samba.cardManagement.ManageCardsScreen;
import com.ceesolutions.samba.eatMubarak.EatMubarakMainScreen;
import com.ceesolutions.samba.masterCardIntegration.MainActivity;
import com.ceesolutions.samba.transactions.Adapters.AccountDetailsCustomListAdapter;
import com.ceesolutions.samba.transactions.Model.AccountDetails;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.NonScrollListView;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.OnBoomListenerAdapter;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/12/18.
 */

public class AccountDetailsScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private NonScrollListView listView;
    private AccountDetailsCustomListAdapter accountDetailsCustomListAdapter;
    private ArrayList<AccountDetails> accountDetailsArrayList;
    private Dialog pd, dialog1;
    private RequestQueue requestQueue;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private ImageView backBtn;
    private String accountNumber;
    private TextView message, doneBtn, btn, heading, textView, statusTxt, oldAccountNumberTxt, ibanTxt, modeTxt, postTxt, accountInfoTxt, accountNumberTxt, accountTxt, amountTxt, currencyTxt;
    private EditText chatBotText;
    private ImageView notificationBtn, sambaBtn, shareBtn;
    private BoomMenuButton bmb;
    private String menu, userType, bal;
    private AppPreferences appPreferences;
    private String location;
    private Spinner channelSpinner;
    private String channelSelected;
    private HashMap<String, String> channelsArrayList;
    private ArrayList<String> channelStringArrayList;
    private String[] channels = {
            "ATM",
            "ATM",
            "ATM",
            "ATM",
            "ATM"

    };
    private boolean sepratedLength = false;
    private String oldAccount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_details);
        initViews();
        try {
            CryptLib _crypt = new CryptLib();

            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }
            menu = constants.sharedPreferences.getString("Menus", "N/A");
            if (!TextUtils.isEmpty(menu) && !menu.equals("N/A") && !menu.equals("")) {


            } else {

                menu = "N/A";
            }
            getSupportActionBar().hide();

            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void initViews() {

        Intent intent = getIntent();
        accountNumber = intent.getStringExtra("accountNumber");
        bal = intent.getStringExtra("bal");
        accountDetailsArrayList = new ArrayList<>();
        channelsArrayList = new HashMap<>();
        channelStringArrayList = new ArrayList<>();
        listView = findViewById(R.id.listView);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(AccountDetailsScreen.this);
        helper = Helper.getHelper(this);
        sambaBtn = findViewById(R.id.sambaBtn);
        chatBotText = findViewById(R.id.chatBoxText);
        shareBtn = findViewById(R.id.shareBtn);
        statusTxt = findViewById(R.id.statusTxt);
        oldAccountNumberTxt = findViewById(R.id.oldAccountNumberTxt);
        ibanTxt = findViewById(R.id.ibanTxt);
        modeTxt = findViewById(R.id.modeTxt);
        postTxt = findViewById(R.id.postTxt);
        currencyTxt = findViewById(R.id.currencyTxt);
        accountNumberTxt = findViewById(R.id.accountNumberTxt);
        amountTxt = findViewById(R.id.amountTxt);
        accountTxt = findViewById(R.id.accountTxt);
        accountInfoTxt = findViewById(R.id.accountInfoTxt);
        backBtn = findViewById(R.id.backBtn);
        utils = new Utils(AccountDetailsScreen.this);
        channelSpinner = findViewById(R.id.channelSpinner);
        pd = new Dialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        bmb = findViewById(R.id.bmb);
        assert bmb != null;
        bmb.setButtonEnum(ButtonEnum.TextOutsideCircle);
        bmb.setBackgroundColor(Color.TRANSPARENT);
        bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_9_1);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_9_1);
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

        String list = constants.channelList.getString("limits", "N/A");

        if (list.equals("N/A")) {

        } else {

            String channelslist = constants.channelList.getString("channels", "N/A");
            Gson gson = new Gson();
            Type type = new TypeToken<HashMap<String, String>>() {
            }.getType();
            Type type1 = new TypeToken<ArrayList<String>>() {
            }.getType();
            channelsArrayList = gson.fromJson(list, type);


            channelStringArrayList = gson.fromJson(channelslist, type1);
            channels = new String[channelStringArrayList.size()];
            channels = channelStringArrayList.toArray(channels);
            channelSpinner.bringToFront();
            showSpinner();
        }
        chatBotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDilogForErrorForLogin(getResources().getString(R.string.message), "WARNING");
                Intent i = new Intent(AccountDetailsScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(i);
                finish();
                finishAffinity();
            }
        });


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                AccountDetails(accountNumber, bal);

            }
        }, 500);
        sambaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                bmb.boom();
            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {


                    Intent intent = new Intent("android.intent.action.VIEW");

                    /** creates an sms uri */
                    Uri data = Uri.parse("sms:");

                    /** Setting sms uri to the intent */
                    intent.setData(data);
                    intent.putExtra("sms_body", "Download Samba Mobile App and register yourself. Click https://bit.ly/2KB0nfG for details");

                    /** Initiates the SMS compose screen, because the activity contain ACTION_VIEW and sms uri */
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        bmb.setOnBoomListener(new OnBoomListenerAdapter() {
            @Override
            public void onBoomWillShow() {
                super.onBoomWillShow();
                // logic here
            }
        });

        bmb.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {
                // If you have implement listeners for boom-buttons in builders,
                // then you shouldn't add any listener here for duplicate callbacks.

                switch (index) {


                    case 0:

                        Intent home = new Intent(AccountDetailsScreen.this, HomeScreen.class);
                        overridePendingTransition(0, 0);
                        startActivity(home);
                        finish();
                        break;

                    case 1:

                        if (!menu.equals("N/A")) {

                            if (menu.toLowerCase().contains("accounts")) {
                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                            } else {

                                Intent intent = new Intent(AccountDetailsScreen.this, AccountsScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
//                                finish();
                            }
                        } else {
                            Intent intent = new Intent(AccountDetailsScreen.this, AccountsScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(intent);
//                            finish();

                        }

                        break;

                    case 2:
                        try {

                            CryptLib _crypt = new CryptLib();
                            Intent transfers = new Intent(AccountDetailsScreen.this, FundTransferScreen.class);
                            String user = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                            if (!TextUtils.isEmpty(user) && !user.equals("N/A")) {

                                if (user.equals("NON_SAMBA")) {

                                    transfers.putExtra("requestFunds", "nonSamba");
                                }
                            }
                            overridePendingTransition(0, 0);
                            startActivity(transfers);
//                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;


                    case 3:
                        try {
                            CryptLib _crypt = new CryptLib();
                            if (!menu.equals("N/A")) {

                                if (menu.toLowerCase().contains("biller management")) {
                                    showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                } else {

                                    String user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                                    Intent payments = new Intent(AccountDetailsScreen.this, PayBillsScreen.class);
                                    overridePendingTransition(0, 0);
                                    user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                                    if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                        if (user1.equals("NON_SAMBA")) {

                                            payments.putExtra("requestbill", "nonSamba");
                                        }
                                    }
                                    startActivity(payments);
//                                    finish();

                                }
                            } else {
                                Intent payments = new Intent(AccountDetailsScreen.this, PayBillsScreen.class);
                                overridePendingTransition(0, 0);
                                String user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                                user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                                if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                    if (user1.equals("NON_SAMBA")) {

                                        payments.putExtra("requestbill", "nonSamba");
                                    }
                                }
                                startActivity(payments);
//                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;


                    case 4:

                        if (!menu.equals("N/A")) {

                            if (menu.toLowerCase().contains("beneficiary management")) {
                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                            } else {

                                if (userType.equals("NON_SAMBA")) {
                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                } else {
                                    Intent bene = new Intent(AccountDetailsScreen.this, ManageBeneficiaresScreen.class);
                                    overridePendingTransition(0, 0);
                                    startActivity(bene);
//                                    finish();
                                }
                            }
                        } else {
                            if (userType.equals("NON_SAMBA")) {
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            } else {
                                Intent bene = new Intent(AccountDetailsScreen.this, ManageBeneficiaresScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(bene);
//                                    finish();
                            }
//                                finish();
                        }

                        break;


                    case 5:

                        if (userType.equals("NON_SAMBA"))
                            showDilogForError("This feature is only available for Samba Users.", "WARNING");
                        else {
                            Intent intent = new Intent(AccountDetailsScreen.this, MainActivity.class);
                            overridePendingTransition(0, 0);
                            startActivity(intent);
                        }

//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("friends management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//
//                                Intent friends = new Intent(AccountDetailsScreen.this, ManageFriends.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(friends);
////                                    finish();
//                            }
//                        } else {
//                            Intent friends = new Intent(AccountDetailsScreen.this, ManageFriends.class);
//                            overridePendingTransition(0, 0);
//                            startActivity(friends);
////                                finish();
//                        }

                        break;


                    case 6:


                        try {

                            CryptLib _crypt = new CryptLib();

                            String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                            if (userType.equals("NON_SAMBA"))
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            else {
                                Intent intent = new Intent(AccountDetailsScreen.this, EatMubarakMainScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        break;



//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("cards management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//                                if (userType.equals("NON_SAMBA")) {
//                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                                } else {
//                                    Intent cards = new Intent(AccountDetailsScreen.this, EatMubarakMainScreen.class);
//                                    overridePendingTransition(0, 0);
//                                    startActivity(cards);
////                                    finish();
//                                }
//
////                                    finish();
//                            }
//                        } else {
//                            if (userType.equals("NON_SAMBA")) {
//                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                            } else {
//                                Intent cards = new Intent(AccountDetailsScreen.this, EatMubarakMainScreen.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(cards);
////                                    finish();
//                            }
////                                finish();
//                        }


                    case 7:
                        if (userType.equals("NON_SAMBA")) {
                            showDilogForError("This feature is only available for Samba Users.", "WARNING");
                        } else {
                            Intent atm = new Intent(AccountDetailsScreen.this, BookMeMainScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(atm);
                        }
//                        finish();
                        break;

                    case 8:

                        pd.show();
                        showDialog("Are you sure you want to Logout?");
                        break;


                    default: {

                        break;
                    }
                }
            }

            @Override
            public void onBackgroundClick() {
//                textViewForAnimation.setText("Click background!!!");
            }

            @Override
            public void onBoomWillHide() {
                Log.d("BMB", "onBoomWillHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will RE-BOOM!!!");
            }

            @Override
            public void onBoomDidHide() {
                Log.d("BMB", "onBoomDidHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did RE-BOOM!!!");
            }

            @Override
            public void onBoomWillShow() {
                Log.d("BMB", "onBoomWillShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will BOOM!!!");
            }

            @Override
            public void onBoomDidShow() {
                Log.d("BMB", "onBoomDidShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did BOOM!!!");
            }
        });

//        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++)
//            bmb.addBuilder(BuilderManager.getSimpleCircleButtonBuilder());

        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder()
                    .listener(new OnBMClickListener() {
                        @Override
                        public void onBoomButtonClick(int index) {
                            // When the boom-button corresponding this builder is clicked.
//                            Toast.makeText(HomeScreen.this, "Clicked " + index, Toast.LENGTH_SHORT).show();
                        }
                    });

//            int margin = getResources().getDimensionPixelSize(R.dimen._4sdp);
            builder.textSize(12);

            switch (i) {

                case 0:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Home");
                    builder.unableText("Home");
                    builder.normalText("Home");
                    break;

                case 1:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Accounts");
                    builder.unableText("Accounts");
                    builder.normalText("Accounts");
                    break;

                case 2:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Transfers");
                    builder.unableText("Transfers");
                    builder.normalText("Transfers");
                    break;


                case 3:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Payments");
                    builder.unableText("Payments");
                    builder.normalText("Payments");
                    break;


                case 4:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Beneficiaries");
                    builder.unableText("Beneficiaries");
                    builder.normalText("Beneficiaries");
                    break;


                case 5:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("QR Payments");
                    builder.unableText("QR Payments");
                    builder.normalText("QR Payments");


//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Friends");
//                    builder.unableText("Friends");
//                    builder.normalText("Friends");

                    break;


                case 6:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Food");
                    builder.unableText("Food");
                    builder.normalText("Food");

//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Cards");
//                    builder.unableText("Cards");
//                    builder.normalText("Cards");
                    break;


                case 7:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("E-Tickets");
                    builder.unableText("E-Tickets");
                    builder.normalText("E-Tickets");
                    break;

                case 8:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Logout");
                    builder.unableText("Logout");
                    builder.normalText("Logout");
                    break;


                default: {

                    break;
                }

            }
//            builder.normalImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.unableImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.normalColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedColor(getResources().getColor(R.color.notiheader));
//            builder.unableColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedText("Cards");
//            builder.unableText("Cards");
//            builder.normalText("Cards");
//            builder.imagePadding(new Rect(40, 40, 40, 40));
            bmb.addBuilder(builder);
        }


    }


    public void AccountDetails(final String accountNumber, final String bal) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);

                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", Constants.signsUserName);
                params.put("sService_Password", Constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sDeviceLatitude", latitude + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", Constants.dateTime);
                params.put("sSigns_Username", user);
                params.put("sT24_AccountNumber", accountNumber);
                params.put("sServerSessionKey", md5);

                final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());

                HttpsTrustManager.allowMySSL(AccountDetailsScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.acountDetailURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("Status_Code").equals("00")) {

                                            statusTxt.setText(jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("Status"));
                                            String str = jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("AlternateAccount");
                                            String[] separated = str.split("\\|");
                                            modeTxt.setText(jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("ModeofOperation"));
                                            if (!TextUtils.isEmpty(jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("PostingRestrictionDescription"))) {

                                                postTxt.setText(jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("PostingRestrictionDescription"));
                                            } else {
                                                postTxt.setText("None");
                                            }

                                            accountInfoTxt.setText(jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("Product"));
//                                        currencyTxt.setText(jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("Currency"));
//                                        accountNumberTxt.setText(jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("AccountID"));
                                            currencyTxt.setText(bal);
                                            if (jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("Type").toLowerCase().equals("current")) {

                                                accountNumberTxt.setText("Current Account");
                                            } else {
                                                accountNumberTxt.setText("Saving Account");
                                            }
                                            accountTxt.setText(jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("AccountID"));
//                                        amountTxt.setText(jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("AvailBal"));
                                            amountTxt.setText("Avaliable Balance (" + jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("Currency") + ")");
                                            if (separated.length > 1) {
                                                oldAccountNumberTxt.setText(separated[1]);
                                                ibanTxt.setText(separated[0]);
                                                sepratedLength = true;
                                                oldAccount = separated[1];
//                                            AccountDetailsLimits(accountNumber, separated[1]);
                                            } else {
                                                ibanTxt.setText(separated[0]);
                                                sepratedLength = false;
                                                oldAccount = "";
//                                            AccountDetailsLimits(accountNumber, "");
                                            }
                                            pd.dismiss();

                                        } else if (jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("Status_Description"), "ERROR", AccountDetailsScreen.this, new LoginScreen());


                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountDetailResult").getString("Status_Description"), "ERROR");


                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sT24_AccountNumber", accountNumber);
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                pd.dismiss();
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");

            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");

        }
    }


    public void AccountDetailsLimits(final String accountNumber, final String Account, final String channelID) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", Constants.signsUserName);
                params.put("sService_Password", Constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", Constants.dateTime);
                params.put("sSigns_Username", user);
                params.put("sAccountNumberNew", accountNumber);
                params.put("sChannelID", channelID);
                params.put("sAccountTarget", "1");
                params.put("sDeviceLatitude", latitude + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sAccountNumberOld", Account);
                params.put("sServerSessionKey", md5);

                final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());


                HttpsTrustManager.allowMySSL(AccountDetailsScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.acountDetailLimitURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_LimitsAccountResult").getString("Status_Code").equals("00")) {

                                            accountDetailsArrayList.clear();
                                            accountDetailsArrayList = new ArrayList<>();
                                            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_LimitsAccountResult").getString("AccountLimits"));

                                            if (jsonArray.length() > 0) {

                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {
                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
                                                        AccountDetails accountDetails = new AccountDetails();
                                                        accountDetails.setTitle(jsonObj.getString("TransactionDescription"));
                                                        accountDetails.setDailyLimit(jsonObj.getString("AvailableDailyLimit_Max"));
                                                        accountDetails.setRemainingLimit(jsonObj.getString("RemainingDailyLimit"));
                                                        String max = jsonObj.getString("AvailableDailyLimit_Max").replaceAll(",", "");
                                                        String remaining = jsonObj.getString("RemainingDailyLimit").replaceAll(",", "");
                                                        Float remains = Float.valueOf(max) - Float.valueOf(remaining);
                                                        accountDetails.setUsedLimit(String.valueOf(remains));
                                                        accountDetailsArrayList.add(accountDetails);

                                                    } catch (JSONException ex) {
                                                        ex.printStackTrace();
                                                    }


                                                }

                                                accountDetailsCustomListAdapter = new AccountDetailsCustomListAdapter(accountDetailsArrayList, AccountDetailsScreen.this);
                                                listView.setAdapter(accountDetailsCustomListAdapter);
                                                pd.dismiss();

                                            } else {
                                                pd.dismiss();
                                            }


                                        } else if (jsonObject.getJSONObject("Signs_LimitsAccountResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_LimitsAccountResult").getString("Status_Description"), "ERROR", AccountDetailsScreen.this, new LoginScreen());


                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_LimitsAccountResult").getString("Status_Description"), "ERROR");

                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sAccountNumberNew", accountNumber);
//                        params.put("sChannelID", channelID);
//                        params.put("sAccountTarget", "1");
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sAccountNumberOld", Account);
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                pd.dismiss();
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");

            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");

        }
    }

    public void showDilogForErrorForLogin(String msg, String header) {

        dialog1 = new Dialog(AccountDetailsScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        message = dialog1.findViewById(R.id.validatingTxt);
        doneBtn = dialog1.findViewById(R.id.doneBtn);
        heading = dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(true);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
                Intent i = new Intent(AccountDetailsScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(i);
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog1.show();


    }

    public void showDilogForError(String msg, String header) {

        dialog1 = new Dialog(AccountDetailsScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        textView = dialog1.findViewById(R.id.validatingTxt);
        btn = dialog1.findViewById(R.id.doneBtn);
        heading = dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog1.show();

    }

    private void showDialog(String message) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Logout:");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

//                constants.editor.clear();
//                constants.editor.commit();
//                constants.editor.apply();
//                constants.nonSambaCredentialsEditor.clear();
//                constants.nonSambaCredentialsEditor.commit();
//                constants.nonSambaCredentialsEditor.apply();
//                constants.destinationAccountEditor.clear().commit();
//                constants.destinationAccountEditor.apply();
//                constants.sourceAccountEditor.clear().commit();
//                constants.sourceAccountEditor.apply();
//                constants.accountListEditor.clear().commit();
//                constants.accountListEditor.apply();
//                constants.accountsEditor.clear().commit();
//                constants.accountsEditor.apply();
//                constants.forgotPasswordEditor.clear().commit();
//                constants.forgotPasswordEditor.apply();
//                Handler handler1 = new Handler();
//                handler1.postDelayed(new Runnable() {
//                    public void run() {
//
//                        pd.dismiss();
//                        Intent intent = new Intent(AccountDetailsScreen.this, LoginScreen.class);
//                        startActivity(intent);
//                        overridePendingTransition(0, 0);
//                        finish();
//                        finishAffinity();
//
//                    }
//                }, 1000);
                Logout();
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                pd.dismiss();
            }
        });

        builder.show();
    }

    public void Logout() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSessionID", Constants.Shared_KEY);
                    HttpsTrustManager.allowMySSL(AccountDetailsScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.logoutURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Code").equals("00")) {
                                                constants.editor.clear();
                                                constants.editor.commit();
                                                constants.editor.apply();
                                                constants.nonSambaCredentialsEditor.clear();
                                                constants.nonSambaCredentialsEditor.commit();
                                                constants.nonSambaCredentialsEditor.apply();
                                                constants.destinationAccountEditor.clear().commit();
                                                constants.destinationAccountEditor.apply();
                                                constants.sourceAccountEditor.clear().commit();
                                                constants.sourceAccountEditor.apply();
                                                constants.accountListEditor.clear().commit();
                                                constants.accountListEditor.apply();
                                                constants.accountsEditor.clear().commit();
                                                constants.accountsEditor.apply();
                                                constants.forgotPasswordEditor.clear().commit();
                                                constants.forgotPasswordEditor.apply();
                                                constants.purposeBillList.clear().commit();
                                                constants.purposeBillList.apply();
                                                constants.purposePrepaidList.clear().commit();
                                                constants.purposePrepaidList.apply();
                                                constants.purposeListEditor.clear().commit();
                                                constants.purposeListEditor.apply();
                                                pd.dismiss();
                                                Intent intent = new Intent(AccountDetailsScreen.this, LoginScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                finish();
                                                finishAffinity();


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Description"), "ERROR", AccountDetailsScreen.this, new LoginScreen());

                                            }

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sSessionID", Constants.Shared_KEY);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity", "WARNING");


            }
        } catch (Exception e) {

            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity", "WARNING");


        }
    }

    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                spinnerAdapter channelsAdapter = new spinnerAdapter(AccountDetailsScreen.this, R.layout.custom_textview_fp);
                channelsAdapter.addAll(channels);
                channelsAdapter.add("Select Channel to view limits");
                channelSpinner.setAdapter(channelsAdapter);
                channelSpinner.setSelection(channelsAdapter.getCount());
                channelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


                            // Get select item
                            if (channelSpinner.getSelectedItem().equals("Select Channel to view limits")) {
                                channelSelected = channelSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + channelSelected);

                            } else {

                                channelSelected = channelSpinner.getSelectedItem().toString();

                                if (sepratedLength) {

                                    String channels = channelsArrayList.get(channelSelected);
                                    AccountDetailsLimits(accountNumber, oldAccount, channels);

                                } else {
                                    String channels = channelsArrayList.get(channelSelected);
                                    AccountDetailsLimits(accountNumber, oldAccount, channels);

                                }

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });


            }

        }, 1000);
    }

}