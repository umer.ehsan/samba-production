package com.ceesolutions.samba.transactions;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.Bookme.BookMeMainScreen;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.beneficiariesManagement.ManageBeneficiaresScreen;
import com.ceesolutions.samba.billPayments.PayBillsScreen;
import com.ceesolutions.samba.cardManagement.ManageCardsScreen;
import com.ceesolutions.samba.eatMubarak.EatMubarakMainScreen;
import com.ceesolutions.samba.masterCardIntegration.MainActivity;
import com.ceesolutions.samba.transactions.Adapters.TransactionDetailsCustomListAdapter;
import com.ceesolutions.samba.transactions.Model.TransactionDetails;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.OnBoomListenerAdapter;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by ceeayaz on 4/13/18.
 */

public class TransactionDetailsScreen extends AppCompatActivity {

    private Dialog dialog1, dialog2;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private ImageView backBtn;
    private ListView listView;
    private TransactionDetailsCustomListAdapter transactionDetailsCustomListAdapter;
    private ArrayList<TransactionDetails> transactionDetailsList;
    private TextView accountNumberTxt, accountTxt, amountTxt, currencyTxt, statusHeading;
    private LinearLayout linearLayout;
    private LayoutInflater inflater1;
    private TextView title, cashTxt, beforeTxt, afterTxt, reviewBtn, doneBtn, heading, btn, message, textView;
    private ImageView icon;
    private View view;
    ArrayList<String> arrayList1;
    private int year, day, month;
    private String monthName, yearWithTwoDigits;
    private Dialog pd;
    final int DATE_PICKER_ID = 1;
    private int count = 0;
    private String FromDate, toDate, sFromDate, sToDate;
    private String accountNumber, branchCode, type, bal, cur;
    private ImageView notificationBtn, sambaBtn, shareBtn;
    private BoomMenuButton bmb;
    private EditText chatBotText;
    private String menu, userType;
    private AppPreferences appPreferences;
    private String location;
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transactions_details);
        initViews();
        try {
            getSupportActionBar().hide();
            CryptLib _crypt = new CryptLib();
            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }
            menu = constants.sharedPreferences.getString("Menus", "N/A");
            if (!TextUtils.isEmpty(menu) && !menu.equals("N/A") && !menu.equals("")) {


            } else {

                menu = "N/A";
            }

            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    finish();
                }
            });

            reviewBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    SelectDate();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initViews() {

        Intent intent = getIntent();

        accountNumber = intent.getStringExtra("accountNumber");
        branchCode = intent.getStringExtra("branchCode");
        type = intent.getStringExtra("type");
        bal = intent.getStringExtra("bal");
        cur = intent.getStringExtra("cur");
        arrayList1 = new ArrayList<>();
        transactionDetailsList = new ArrayList<>();
        reviewBtn = (TextView) findViewById(R.id.reviewBtn);
        sambaBtn = (ImageView) findViewById(R.id.sambaBtn);
        chatBotText = (EditText) findViewById(R.id.chatBoxText);
        shareBtn = (ImageView) findViewById(R.id.shareBtn);
        listView = (ListView) findViewById(R.id.listView);
        currencyTxt = (TextView) findViewById(R.id.currencyTxt);
        accountNumberTxt = (TextView) findViewById(R.id.accountNumberTxt);
        amountTxt = (TextView) findViewById(R.id.amountTxt);
        accountTxt = (TextView) findViewById(R.id.accountTxt);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(TransactionDetailsScreen.this);
        helper = Helper.getHelper(this);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        statusHeading = (TextView) findViewById(R.id.statusHeading);
        utils = new Utils(TransactionDetailsScreen.this);
        pd = new Dialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        bmb = (BoomMenuButton) findViewById(R.id.bmb);
        assert bmb != null;
        bmb.setButtonEnum(ButtonEnum.TextOutsideCircle);
        bmb.setBackgroundColor(Color.TRANSPARENT);
        bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_9_1);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_9_1);

        chatBotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDilogForErrorForLogin(getResources().getString(R.string.message), "WARNING");
                Intent i = new Intent(TransactionDetailsScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(i);
                finish();
                finishAffinity();
            }
        });


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                AccountDetails(accountNumber, branchCode, type, bal, cur);

            }
        }, 500);
        sambaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                bmb.boom();
            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {


                    Intent intent = new Intent("android.intent.action.VIEW");

                    /** creates an sms uri */
                    Uri data = Uri.parse("sms:");

                    /** Setting sms uri to the intent */
                    intent.setData(data);
                    intent.putExtra("sms_body", "Download Samba Mobile App and register yourself. Click https://bit.ly/2KB0nfG for details");

                    /** Initiates the SMS compose screen, because the activity contain ACTION_VIEW and sms uri */
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        bmb.setOnBoomListener(new OnBoomListenerAdapter() {
            @Override
            public void onBoomWillShow() {
                super.onBoomWillShow();
                // logic here
            }
        });

        bmb.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {
                // If you have implement listeners for boom-buttons in builders,
                // then you shouldn't add any listener here for duplicate callbacks.

                switch (index) {


                    case 0:

                        Intent home = new Intent(TransactionDetailsScreen.this, HomeScreen.class);
                        overridePendingTransition(0, 0);
                        startActivity(home);
                        finish();
                        break;

                    case 1:

                        if (!menu.equals("N/A")) {

                            if (menu.toLowerCase().contains("accounts")) {
                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                            } else {

                                Intent intent = new Intent(TransactionDetailsScreen.this, AccountsScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
//                                finish();
                            }
                        } else {
                            Intent intent = new Intent(TransactionDetailsScreen.this, AccountsScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(intent);
//                            finish();

                        }

                        break;

                    case 2:
                        try {
                            CryptLib _crypt = new CryptLib();
                            Intent transfers = new Intent(TransactionDetailsScreen.this, FundTransferScreen.class);
                            String user = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            if (!TextUtils.isEmpty(user) && !user.equals("N/A")) {

                                if (user.equals("NON_SAMBA")) {

                                    transfers.putExtra("requestFunds", "nonSamba");
                                }
                            }
                            overridePendingTransition(0, 0);
                            startActivity(transfers);
//                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;


                    case 3:

                        try {
                            CryptLib _crypt = new CryptLib();
                            if (!menu.equals("N/A")) {

                                if (menu.toLowerCase().contains("biller management")) {
                                    showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                } else {
                                    String user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                    Intent payments = new Intent(TransactionDetailsScreen.this, PayBillsScreen.class);
                                    overridePendingTransition(0, 0);
                                    user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                    if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                        if (user1.equals("NON_SAMBA")) {

                                            payments.putExtra("requestbill", "nonSamba");
                                        }
                                    }
                                    startActivity(payments);
//                                    finish();
                                }
                            } else {
                                Intent payments = new Intent(TransactionDetailsScreen.this, PayBillsScreen.class);
                                overridePendingTransition(0, 0);
                                String user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                    if (user1.equals("NON_SAMBA")) {

                                        payments.putExtra("requestbill", "nonSamba");
                                    }
                                }
                                startActivity(payments);
//                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;


                    case 4:

                        if (!menu.equals("N/A")) {

                            if (menu.toLowerCase().contains("beneficiary management")) {
                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                            } else {

                                if (userType.equals("NON_SAMBA")) {
                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                } else {
                                    Intent bene = new Intent(TransactionDetailsScreen.this, ManageBeneficiaresScreen.class);
                                    overridePendingTransition(0, 0);
                                    startActivity(bene);
//                                    finish();
                                }
                            }
                        } else {
                            if (userType.equals("NON_SAMBA")) {
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            } else {
                                Intent bene = new Intent(TransactionDetailsScreen.this, ManageBeneficiaresScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(bene);
//                                    finish();
                            }
//                                finish();
                        }

                        break;


                    case 5:

                        if (userType.equals("NON_SAMBA"))
                            showDilogForError("This feature is only available for Samba Users.", "WARNING");
                        else {
                            Intent intent = new Intent(TransactionDetailsScreen.this, MainActivity.class);
                            overridePendingTransition(0, 0);
                            startActivity(intent);
                        }

//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("friends management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//
//                                Intent friends = new Intent(TransactionDetailsScreen.this, ManageFriends.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(friends);
////                                    finish();
//                            }
//                        } else {
//                            Intent friends = new Intent(TransactionDetailsScreen.this, ManageFriends.class);
//                            overridePendingTransition(0, 0);
//                            startActivity(friends);
////                                finish();
//                        }

                        break;


                    case 6:


                        try {

                            CryptLib _crypt = new CryptLib();

                            String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                            if (userType.equals("NON_SAMBA"))
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            else {
                                Intent intent = new Intent(TransactionDetailsScreen.this, EatMubarakMainScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;


//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("cards management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//                                if (userType.equals("NON_SAMBA")) {
//                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                                } else {
//                                    Intent cards = new Intent(TransactionDetailsScreen.this, ManageCardsScreen.class);
//                                    overridePendingTransition(0, 0);
//                                    startActivity(cards);
////                                    finish();
//                                }
//
////                                    finish();
//                            }
//                        } else {
//                            if (userType.equals("NON_SAMBA")) {
//                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                            } else {
//                                Intent cards = new Intent(TransactionDetailsScreen.this, ManageCardsScreen.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(cards);
////                                    finish();
//                            }
////                                finish();
//                        }


                    case 7:
                        if (userType.equals("NON_SAMBA")) {
                            showDilogForError("This feature is only available for Samba Users.", "WARNING");
                        } else {
                            Intent atm = new Intent(TransactionDetailsScreen.this, BookMeMainScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(atm);
//                        finish();
                        }
                        break;

                    case 8:

                        pd.show();
                        showDialog("Are you sure you want to Logout?");
                        break;


                    default: {

                        break;
                    }
                }
            }

            @Override
            public void onBackgroundClick() {
//                textViewForAnimation.setText("Click background!!!");
            }

            @Override
            public void onBoomWillHide() {
                Log.d("BMB", "onBoomWillHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will RE-BOOM!!!");
            }

            @Override
            public void onBoomDidHide() {
                Log.d("BMB", "onBoomDidHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did RE-BOOM!!!");
            }

            @Override
            public void onBoomWillShow() {
                Log.d("BMB", "onBoomWillShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will BOOM!!!");
            }

            @Override
            public void onBoomDidShow() {
                Log.d("BMB", "onBoomDidShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did BOOM!!!");
            }
        });

//        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++)
//            bmb.addBuilder(BuilderManager.getSimpleCircleButtonBuilder());

        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder()
                    .listener(new OnBMClickListener() {
                        @Override
                        public void onBoomButtonClick(int index) {
                            // When the boom-button corresponding this builder is clicked.
//                            Toast.makeText(HomeScreen.this, "Clicked " + index, Toast.LENGTH_SHORT).show();
                        }
                    });

//            int margin = getResources().getDimensionPixelSize(R.dimen._4sdp);
            builder.textSize(12);

            switch (i) {

                case 0:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Home");
                    builder.unableText("Home");
                    builder.normalText("Home");
                    break;

                case 1:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Accounts");
                    builder.unableText("Accounts");
                    builder.normalText("Accounts");
                    break;

                case 2:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Transfers");
                    builder.unableText("Transfers");
                    builder.normalText("Transfers");
                    break;


                case 3:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Payments");
                    builder.unableText("Payments");
                    builder.normalText("Payments");
                    break;


                case 4:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Beneficiaries");
                    builder.unableText("Beneficiaries");
                    builder.normalText("Beneficiaries");
                    break;


                case 5:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("QR Payments");
                    builder.unableText("QR Payments");
                    builder.normalText("QR Payments");

//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Friends");
//                    builder.unableText("Friends");
//                    builder.normalText("Friends");
                    break;


                case 6:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Food");
                    builder.unableText("Food");
                    builder.normalText("Food");

//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Cards");
//                    builder.unableText("Cards");
//                    builder.normalText("Cards");
                    break;


                case 7:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("E-Tickets");
                    builder.unableText("E-Tickets");
                    builder.normalText("E-Tickets");
                    break;

                case 8:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Logout");
                    builder.unableText("Logout");
                    builder.normalText("Logout");
                    break;


                default: {

                    break;
                }

            }
//            builder.normalImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.unableImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.normalColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedColor(getResources().getColor(R.color.notiheader));
//            builder.unableColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedText("Cards");
//            builder.unableText("Cards");
//            builder.normalText("Cards");
//            builder.imagePadding(new Rect(40, 40, 40, 40));
            bmb.addBuilder(builder);
        }


    }

    public void AccountDetails(final String accountNumber, final String branchCode, final String type, final String bal, final String cur) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();

                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Username", user);
                params.put("sT24_AccountNumber", accountNumber);
                params.put("sBranchCode", branchCode);
                params.put("sServerSessionKey", md5);

                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                HttpsTrustManager.allowMySSL(TransactionDetailsScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.miniStatementURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("Status_Code").equals("00")) {

                                            accountTxt.setText(jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("AccountID"));
                                            currencyTxt.setText(bal);
                                            amountTxt.setText("Avaliable Balance (" + cur + ")");
                                            if (type.equals("SAVINGS.ACCOUNTS"))
                                                accountNumberTxt.setText("Saving Account");
                                            else
                                                accountNumberTxt.setText("Current Account");
                                            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("MiniStatement"));

                                            if (jsonArray.length() > 0) {

                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {
                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        TransactionDetails transactionDetails = new TransactionDetails();
                                                        transactionDetails.setTitle(jsonObj.getString("Description"));
                                                        Long date = Long.valueOf(jsonObj.getString("DateTime"));
                                                        String value = jsonObj.getString("ValueDate");
                                                        String year = value.substring(0, 4);
                                                        String month = value.substring(4, 6);
                                                        String datetype = value.substring(6, 8);
                                                        String formatedDate = year + "-" + month + "-" + datetype;
                                                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                                                        Date mDate;
                                                        long timeInMilliseconds = 0;
                                                        try {

                                                            mDate = sdf1.parse(formatedDate);
                                                            timeInMilliseconds = mDate.getTime();

                                                        } catch (Exception e) {

                                                            e.printStackTrace();
                                                        }

                                                        Long valueDate = Long.valueOf(jsonObj.getString("ValueDate"));
                                                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                                                        String date1 = getDate(date, "dd-MMM-yyyy");
                                                        transactionDetails.setDate(date1);
                                                        transactionDetails.setAccNumber(jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("AccountID"));
                                                        transactionDetails.setCurrency(cur);
                                                        transactionDetails.setValueDate(getDate(timeInMilliseconds, "dd-MMM-yyyy"));
                                                        transactionDetails.setAfterAmount(jsonObj.getString("Balance"));
                                                        transactionDetails.setDeposit(jsonObj.getString("Deposits"));
                                                        transactionDetails.setWithDraw(jsonObj.getString("Withdrawals"));
                                                        transactionDetails.setRequestNumber(jsonObj.getString("Reference"));
                                                        transactionDetailsList.add(transactionDetails);


                                                    } catch (JSONException ex) {

                                                        ex.printStackTrace();
                                                    }

                                                }
//                                            render(transactionDetailsList);
                                                transactionDetailsCustomListAdapter = new TransactionDetailsCustomListAdapter(transactionDetailsList, TransactionDetailsScreen.this);
                                                listView.setAdapter(transactionDetailsCustomListAdapter);
                                                pd.dismiss();

                                            } else {

                                                pd.dismiss();
                                            }

                                        } else if (jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("Status_Description"), "ERROR", TransactionDetailsScreen.this, new LoginScreen());


                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("Status_Description"), "ERROR");


                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sT24_AccountNumber", accountNumber);
//                        params.put("sBranchCode", branchCode);
//                        params.put("sServerSessionKey", md5);
//
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                pd.dismiss();
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");

            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");

        }
    }


    public void FullStatement(final String accountNumber, final String branchCode, final String type, final String bal, final String cur, final String fromDate, final String toDate) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();

                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);

                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Username", user);
                params.put("sT24_AccountNumber", accountNumber);
                params.put("sBranchCode", branchCode);
                params.put("sFromDate", fromDate);
                params.put("sToDate", toDate);
                params.put("sServerSessionKey", md5);

                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                HttpsTrustManager.allowMySSL(TransactionDetailsScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.accountStatementURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_T24AccountStatementResult").getString("Status_Code").equals("00")) {

                                            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_T24AccountStatementResult").getString("AccountStatement"));

                                            if (jsonArray.length() > 0) {

                                                transactionDetailsList = new ArrayList<>();

                                                for (int i = 0; i < jsonArray.length(); i++) {

                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        TransactionDetails transactionDetails = new TransactionDetails();
                                                        transactionDetails.setTitle(jsonObj.getString("Description"));
//                                                    Long date = Long.valueOf(jsonObj.getString("DateTime"));
                                                        String value = jsonObj.getString("ValueDate");
                                                        String year = value.substring(0, 4);
                                                        String month = value.substring(4, 6);
                                                        String datetype = value.substring(6, 8);
                                                        String formatedDate = year + "-" + month + "-" + datetype;
                                                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                                                        Date mDate;
                                                        long timeInMilliseconds = 0;
                                                        try {

                                                            mDate = sdf1.parse(formatedDate);
                                                            timeInMilliseconds = mDate.getTime();

                                                        } catch (Exception e) {

                                                            e.printStackTrace();
                                                        }

                                                        String booking = jsonObj.getString("BookingDate");
                                                        String bookingYear = booking.substring(0, 4);
                                                        String bookingMonth = booking.substring(4, 6);
                                                        String bookingDatetype = booking.substring(6, 8);
                                                        String bookingFormatedDate = bookingYear + "-" + bookingMonth + "-" + bookingDatetype;
                                                        SimpleDateFormat bookingSdf1 = new SimpleDateFormat("yyyy-MM-dd");
                                                        Date bookingmDate;
                                                        long bookingTimeInMilliseconds = 0;
                                                        try {

                                                            bookingmDate = bookingSdf1.parse(bookingFormatedDate);
                                                            bookingTimeInMilliseconds = bookingmDate.getTime();

                                                        } catch (Exception e) {

                                                            e.printStackTrace();
                                                        }


                                                        transactionDetails.setDate(getDate(bookingTimeInMilliseconds, "dd-MMM-yyyy"));
                                                        transactionDetails.setAccNumber(accountNumber);
                                                        transactionDetails.setCurrency(cur);
                                                        transactionDetails.setValueDate(getDate(timeInMilliseconds, "dd-MMM-yyyy"));
                                                        transactionDetails.setAfterAmount(jsonObj.getString("OpeningBalance"));
                                                        transactionDetails.setDeposit(jsonObj.getString("Credit"));
                                                        transactionDetails.setWithDraw(jsonObj.getString("Debit"));
                                                        transactionDetails.setRequestNumber(jsonObj.getString("Reference"));
                                                        transactionDetailsList.add(transactionDetails);


                                                    } catch (JSONException ex) {

                                                        ex.printStackTrace();
                                                    }

                                                }
//                                            render(transactionDetailsList);
                                                listView.setAdapter(null);
                                                transactionDetailsCustomListAdapter = new TransactionDetailsCustomListAdapter(transactionDetailsList, TransactionDetailsScreen.this);
                                                listView.setAdapter(transactionDetailsCustomListAdapter);
                                                pd.dismiss();

                                            } else {

                                                pd.dismiss();
                                            }

                                        } else if (jsonObject.getJSONObject("Signs_T24AccountStatementResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24AccountStatementResult").getString("Status_Description"), "ERROR", TransactionDetailsScreen.this, new LoginScreen());
                                            transactionDetailsList.clear();
                                            listView.setAdapter(null);

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountStatementResult").getString("Status_Description"), "ERROR");
                                            transactionDetailsList.clear();
                                            listView.setAdapter(null);

                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        transactionDetailsList.clear();
                                        listView.setAdapter(null);

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sT24_AccountNumber", accountNumber);
//                        params.put("sBranchCode", branchCode);
//                        params.put("sFromDate", fromDate);
//                        params.put("sToDate", toDate);
//                        params.put("sServerSessionKey", md5);
//
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                pd.dismiss();
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");

            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");

        }
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    private void SelectDate() {

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


        showDialog(DATE_PICKER_ID);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                DatePickerDialog datePickerDialog = new DatePickerDialog(this, pickerListener, year, month, day);
//                Calendar today = Calendar.getInstance();
//                Date currentDate = today.getTime();
//                datePickerDialog.getDatePicker().setMaxDate(currentDate.getTime());
                return datePickerDialog;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {


            yearWithTwoDigits = String.valueOf(selectedYear);
            monthName = getMonth(selectedMonth + 1);
            day = selectedDay;

            int mon = selectedMonth + 1;
            String monthSelect = String.valueOf(mon);
            if (day < 10) {

                if (count == 0) {
//                    statusHeading.setText(new StringBuilder().append("0").append(day)
//                            .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)));

                    FromDate = new StringBuilder().append("0").append(day).append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)).toString();
                    count++;
                    Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

                    if (mon < 10) {

                        sFromDate = yearWithTwoDigits + "0" + monthSelect + "0" + selectedDay;
                    } else {

                        sFromDate = yearWithTwoDigits + monthSelect + "0" + selectedDay;
                    }
                    DatePickerDialog dialog = new DatePickerDialog(TransactionDetailsScreen.this, this,
                            calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH));
//                    Calendar today = Calendar.getInstance();
//                    Date currentDate = today.getTime();
//                    dialog.getDatePicker().setMaxDate(currentDate.getTime());
                    dialog.show();
                } else {

                    toDate = new StringBuilder().append("0").append(day).append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)).toString();
                    statusHeading.setText(FromDate + " - " + toDate);
                    count = 0;
                    if (mon < 10) {

                        sToDate = yearWithTwoDigits + "0" + monthSelect + "0" + selectedDay;
                    } else {

                        sToDate = yearWithTwoDigits + monthSelect + "0" + selectedDay;
                    }

                    Calendar today = Calendar.getInstance();
                    today.set(Calendar.MILLISECOND, 0);
                    today.set(Calendar.SECOND, 0);
                    today.set(Calendar.MINUTE, 0);
                    today.set(Calendar.HOUR_OF_DAY, 0);
                    Date currentDate1;
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                    Date mDate;
                    long timeInMilliseconds = 0, currentTimeToMili = 0;
                    try {
                        mDate = sdf.parse(sToDate);
                        timeInMilliseconds = mDate.getTime();
                        currentDate1 = sdf.parse(sFromDate);
                        currentTimeToMili = currentDate1.getTime();
//                        System.out.println("Date in milli :: " + timeInMilliseconds);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (currentTimeToMili <= timeInMilliseconds)
                        FullStatement(accountNumber, branchCode, type, bal, cur, sFromDate, sToDate);
                    else
                        utils.showDilogForError("From date cannont be less than To date", "WARNING");

                }

            } else {

                if (count == 0) {

                    FromDate = new StringBuilder().append(day)
                            .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)).toString();
                    count++;

                    if (mon < 10) {

                        sFromDate = yearWithTwoDigits + "0" + monthSelect + selectedDay;
                    } else {

                        sFromDate = yearWithTwoDigits + monthSelect + selectedDay;
                    }
                    Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

                    DatePickerDialog dialog = new DatePickerDialog(TransactionDetailsScreen.this, this,
                            calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH));
//                    Calendar today = Calendar.getInstance();
//                    Date currentDate = today.getTime();
//                    dialog.getDatePicker().setMaxDate(currentDate.getTime());
                    dialog.show();

                } else {

                    toDate = new StringBuilder().append(day)
                            .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)).toString();
                    statusHeading.setText(FromDate + " - " + toDate);
                    count = 0;
                    if (mon < 10) {

                        sToDate = yearWithTwoDigits + "0" + monthSelect + selectedDay;
                    } else {

                        sToDate = yearWithTwoDigits + monthSelect + selectedDay;
                    }

                    Calendar today = Calendar.getInstance();
                    today.set(Calendar.MILLISECOND, 0);
                    today.set(Calendar.SECOND, 0);
                    today.set(Calendar.MINUTE, 0);
                    today.set(Calendar.HOUR_OF_DAY, 0);
                    Date currentDate1;
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                    Date mDate;
                    long timeInMilliseconds = 0, currentTimeToMili = 0;
                    try {
                        mDate = sdf.parse(sToDate);
                        timeInMilliseconds = mDate.getTime();
                        currentDate1 = sdf.parse(sFromDate);
                        currentTimeToMili = currentDate1.getTime();
//                        System.out.println("Date in milli :: " + timeInMilliseconds);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (currentTimeToMili <= timeInMilliseconds)
                        FullStatement(accountNumber, branchCode, type, bal, cur, sFromDate, sToDate);
                    else
                        utils.showDilogForError("From date cannont be less than To date", "WARNING");

                }
//                statusHeading.setText(new StringBuilder().append(day)
//                        .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)));

            }
            // Show selected date

//            dob = editDOB.getText().toString();
//            editMobNumber.requestFocus();
        }
    };


    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }


    public void showDilogForErrorForLogin(String msg, String header) {

        dialog1 = new Dialog(TransactionDetailsScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        message = (TextView) dialog1.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog1.findViewById(R.id.doneBtn);
        heading = (TextView) dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(true);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
                Intent i = new Intent(TransactionDetailsScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(i);
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog1.show();


    }

    public void showDilogForError(String msg, String header) {

        dialog1 = new Dialog(TransactionDetailsScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog1.findViewById(R.id.validatingTxt);
        btn = (TextView) dialog1.findViewById(R.id.doneBtn);
        heading = (TextView) dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog1.show();

    }

    private void showDialog(String message) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Logout:");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

//                constants.editor.clear();
//                constants.editor.commit();
//                constants.editor.apply();
//                constants.nonSambaCredentialsEditor.clear();
//                constants.nonSambaCredentialsEditor.commit();
//                constants.nonSambaCredentialsEditor.apply();
//                constants.destinationAccountEditor.clear().commit();
//                constants.destinationAccountEditor.apply();
//                constants.sourceAccountEditor.clear().commit();
//                constants.sourceAccountEditor.apply();
//                constants.accountListEditor.clear().commit();
//                constants.accountListEditor.apply();
//                constants.accountsEditor.clear().commit();
//                constants.accountsEditor.apply();
//                constants.forgotPasswordEditor.clear().commit();
//                constants.forgotPasswordEditor.apply();
//                Handler handler1 = new Handler();
//                handler1.postDelayed(new Runnable() {
//                    public void run() {
//
//                        pd.dismiss();
//                        Intent intent = new Intent(TransactionDetailsScreen.this, LoginScreen.class);
//                        startActivity(intent);
//                        overridePendingTransition(0, 0);
//                        finish();
//                        finishAffinity();
//
//                    }
//                }, 1000);
                Logout();
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                pd.dismiss();
            }
        });

        builder.show();
    }

    public void Logout() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSessionID", Constants.Shared_KEY);
                    HttpsTrustManager.allowMySSL(TransactionDetailsScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.logoutURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Code").equals("00")) {
                                                constants.editor.clear();
                                                constants.editor.commit();
                                                constants.editor.apply();
                                                constants.nonSambaCredentialsEditor.clear();
                                                constants.nonSambaCredentialsEditor.commit();
                                                constants.nonSambaCredentialsEditor.apply();
                                                constants.destinationAccountEditor.clear().commit();
                                                constants.destinationAccountEditor.apply();
                                                constants.sourceAccountEditor.clear().commit();
                                                constants.sourceAccountEditor.apply();
                                                constants.accountListEditor.clear().commit();
                                                constants.accountListEditor.apply();
                                                constants.accountsEditor.clear().commit();
                                                constants.accountsEditor.apply();
                                                constants.forgotPasswordEditor.clear().commit();
                                                constants.forgotPasswordEditor.apply();
                                                constants.purposeBillList.clear().commit();
                                                constants.purposeBillList.apply();
                                                constants.purposePrepaidList.clear().commit();
                                                constants.purposePrepaidList.apply();
                                                constants.purposeListEditor.clear().commit();
                                                constants.purposeListEditor.apply();
                                                pd.dismiss();
                                                Intent intent = new Intent(TransactionDetailsScreen.this, LoginScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                finish();
                                                finishAffinity();


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Description"), "ERROR", TransactionDetailsScreen.this, new LoginScreen());

                                            }

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sSessionID", Constants.Shared_KEY);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity", "WARNING");


            }
        } catch (Exception e) {

            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity", "WARNING");


        }
    }
}
