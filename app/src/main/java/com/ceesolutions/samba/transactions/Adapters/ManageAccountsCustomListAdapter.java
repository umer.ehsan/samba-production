package com.ceesolutions.samba.transactions.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.transactions.AccountsScreen;
import com.ceesolutions.samba.transactions.Model.Account;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.SwitchCompat;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.RoundImageView;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/12/18.
 */

public class ManageAccountsCustomListAdapter extends BaseAdapter {
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    ArrayList<Account> accountsModels;
    LayoutInflater layoutInflater;
    View row;
    Context context;
    TextView userName, accountNumber, titleTxt1, nonSambaBankName, productType, bankName;
    ImageView click, titleTxt;
    RoundImageView title;
    int[] androidColors;
    String type;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private SwitchCompat switchCompat;
    private AppPreferences appPreferences;
    private String location;

    public ManageAccountsCustomListAdapter(ArrayList<Account> accountsModelArrayList, Context mContext) {

        accountsModels = accountsModelArrayList;
        context = mContext;
        constants = Constants.getConstants(context);
        requestQueue = Volley.newRequestQueue(context);
        utils = new Utils(context);
        helper = Helper.getHelper(context);
        pd = new Dialog(context);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        appPreferences = new AppPreferences(mContext);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return accountsModels.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        try {


            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.manage_account_item, parent, false);
            }
            titleTxt1 = (TextView) convertView.findViewById(R.id.titleTxt1);
            titleTxt1.setVisibility(View.INVISIBLE);
            titleTxt = (ImageView) convertView.findViewById(R.id.titleTxt);
            title = (RoundImageView) convertView.findViewById(R.id.title);
            titleTxt.bringToFront();
            titleTxt1.bringToFront();
            accountNumber = (TextView) convertView.findViewById(R.id.accountNumber);
            userName = (TextView) convertView.findViewById(R.id.userName);
            switchCompat = (SwitchCompat) convertView.findViewById(R.id.switchCompat);
            nonSambaBankName = (TextView) convertView.findViewById(R.id.nonSambaBankName);
            productType = (TextView) convertView.findViewById(R.id.productType);
            bankName = (TextView) convertView.findViewById(R.id.bankName);
            CryptLib _crypt = new CryptLib();
            String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());


            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("") && userType.equals("NON_SAMBA")) {
                nonSambaBankName.setVisibility(View.VISIBLE);
                titleTxt.setVisibility(View.GONE);
                title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(accountsModels.get(position).getBankColor())));
                nonSambaBankName.setText(accountsModels.get(position).getBranchShortName());
                productType.setVisibility(View.GONE);

            } else if (accountsModels.get(position).getAccountType().toUpperCase().contains("CURRENT")) {

                if (!TextUtils.isEmpty(accountsModels.get(position).getBankColor())) {
                    title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(accountsModels.get(position).getBankColor())));
                } else {
                    title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#0064b2")));
                }

                if (!TextUtils.isEmpty(accountsModels.get(position).getCurrencyType()) && !accountsModels.get(position).getCurrencyType().equals("null") && !accountsModels.get(position).getCurrencyType().equals(null)) {
                    nonSambaBankName.setVisibility(View.VISIBLE);
                    nonSambaBankName.setText(accountsModels.get(position).getCurrencyType());
                    titleTxt.setVisibility(View.GONE);
                    productType.setText(accountsModels.get(position).getProductType());
                } else {
                    nonSambaBankName.setVisibility(View.VISIBLE);
                    nonSambaBankName.setText(accountsModels.get(position).getCurrency());
                    titleTxt.setVisibility(View.GONE);
                    productType.setText(accountsModels.get(position).getProductType());
                }
//            titleTxt.setImageResource(R.drawable.current);
//            titleTxt1.setVisibility(View.INVISIBLE);
//            titleTxt.setVisibility(View.VISIBLE);
            } else if (accountsModels.get(position).getAccountType().toUpperCase().contains("SAVING")) {

                if (!TextUtils.isEmpty(accountsModels.get(position).getBankColor())) {
                    title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(accountsModels.get(position).getBankColor())));

                } else {
                    title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#0064b2")));
                }

                if (!TextUtils.isEmpty(accountsModels.get(position).getCurrencyType()) && !accountsModels.get(position).getCurrencyType().equals("null") && !accountsModels.get(position).getCurrencyType().equals(null)) {
                    nonSambaBankName.setVisibility(View.VISIBLE);
                    nonSambaBankName.setText(accountsModels.get(position).getCurrencyType());
                    titleTxt.setVisibility(View.GONE);
                    productType.setText(accountsModels.get(position).getProductType());
                } else {
                    nonSambaBankName.setVisibility(View.VISIBLE);
                    nonSambaBankName.setText(accountsModels.get(position).getCurrency());
                    titleTxt.setVisibility(View.GONE);
                    productType.setText(accountsModels.get(position).getProductType());
                }
//            titleTxt.setImageResource(R.drawable.savings);
//            titleTxt1.setVisibility(View.INVISIBLE);
//            titleTxt.setVisibility(View.VISIBLE);
            } else {

                if (!TextUtils.isEmpty(accountsModels.get(position).getBankColor())) {
                    title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(accountsModels.get(position).getBankColor())));

                } else {
                    title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#0064b2")));

                }

                if (!TextUtils.isEmpty(accountsModels.get(position).getCurrencyType()) && !accountsModels.get(position).getCurrencyType().equals("null") && !accountsModels.get(position).getCurrencyType().equals(null)) {
                    nonSambaBankName.setVisibility(View.VISIBLE);
                    nonSambaBankName.setText(accountsModels.get(position).getCurrencyType());
                    titleTxt.setVisibility(View.GONE);
                    productType.setText(accountsModels.get(position).getProductType());
                } else {
                    nonSambaBankName.setVisibility(View.VISIBLE);
                    nonSambaBankName.setText(accountsModels.get(position).getCurrency());
                    titleTxt.setVisibility(View.GONE);
                    productType.setText(accountsModels.get(position).getProductType());
                }

//            titleTxt1.setVisibility(View.VISIBLE);
//            titleTxt1.setText(accountsModels.get(position).getBranchShortName());
//            titleTxt.setVisibility(View.INVISIBLE);
            }

            switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    FirebaseAnalytics.getInstance(context).logEvent("Account_Status_Toggle_Pressed", new Bundle());

                    if (isChecked) {
                        AccountVisibility(position, "1", switchCompat, isChecked);
                    } else {
                        AccountVisibility(position, "0", switchCompat, isChecked);
                    }
                }
            });


            userName.setText(accountsModels.get(position).getUserName());
            accountNumber.setText(accountsModels.get(position).getAccountNumber());
            bankName.setText(accountsModels.get(position).getBranchName());

            if (accountsModels.get(position).getIsActive().equals("0")) {

                switchCompat.setChecked(false, false);

            } else {

                switchCompat.setChecked(true, false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public void AccountVisibility(final int position, final String flag1, final SwitchCompat switchCompat1, final boolean check) {

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sAccountList_ID", accountsModels.get(position).getId());
                    params.put("sSigns_AccountEnableDisable", flag1);
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(context);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.accountVisibleURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_AccountEnableDisableResult").getString("Status_Code").equals("00")) {

                                                pd.dismiss();
                                                Intent intent = new Intent(context, AccountsScreen.class);
                                                intent.putExtra("addAccount", "add");
                                                ((Activity) context).overridePendingTransition(0, 0);
                                                ((Activity) context).startActivity(intent);
                                                ((Activity) context).finish();

                                            } else if (jsonObject.getJSONObject("Signs_AccountEnableDisableResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_AccountEnableDisableResult").getString("Status_Description"), "ERROR", context, new LoginScreen());
                                                switchCompat1.setChecked(check, false);

                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_AccountEnableDisableResult").getString("Status_Description"), "ERROR");
                                                switchCompat1.setChecked(check, false);
                                            }
                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            switchCompat1.setChecked(check, false);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        switchCompat1.setChecked(check, false);
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        switchCompat1.setChecked(check, false);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        switchCompat1.setChecked(check, false);
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sAccountList_ID", accountsModels.get(position).getId());
//                            params.put("sSigns_AccountEnableDisable", flag1);
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    switchCompat1.setChecked(check, false);
                }

            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                switchCompat1.setChecked(check, false);


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            e.printStackTrace();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            switchCompat1.setChecked(check, false);


        }
    }


}