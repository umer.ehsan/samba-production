package com.ceesolutions.samba.transactions.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.transactions.Model.AccountDetails;

import java.util.ArrayList;

/**
 * Created by ceeayaz on 4/12/18.
 */

public class AccountDetailsCustomListAdapter extends BaseAdapter {

    ArrayList<AccountDetails> accoutDetailsModel;
    Context context;
    TextView dailyTxt, todayTxt, remainTxt;
    TextView title;

    public AccountDetailsCustomListAdapter(ArrayList<AccountDetails> accountDetailsArrayList, Context mContext) {

        accoutDetailsModel = accountDetailsArrayList;
        context = mContext;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return accoutDetailsModel.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.details_list_item, parent, false);
        }

        title = (TextView) convertView.findViewById(R.id.title);

        dailyTxt = (TextView) convertView.findViewById(R.id.dailyTxt);
        todayTxt = (TextView) convertView.findViewById(R.id.todayTxt);
        remainTxt = (TextView) convertView.findViewById(R.id.remainTxt);

        dailyTxt.setText(accoutDetailsModel.get(position).getDailyLimit());
        todayTxt.setText(accoutDetailsModel.get(position).getUsedLimit());
        remainTxt.setText(accoutDetailsModel.get(position).getRemainingLimit());
        title.setText(accoutDetailsModel.get(position).getTitle());


        return convertView;
    }

}