package com.ceesolutions.samba.transactions.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.friends.ManageFriends;
import com.ceesolutions.samba.transactions.RequestMoneyReviewScreen;
import com.ceesolutions.samba.transactions.SelectAccountScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.ceesolutions.samba.views.CircleImageView;
import com.ceesolutions.samba.views.RoundImageView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 3/9/18.
 */

public class RequestMoneyFragment extends Fragment {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private CircleImageView profile_image;
    private Dialog pd;
    private String remarks, amount, isFP;
    private EditText editAmount, editRemarks, editShareAccount;
    private RelativeLayout upperPart1, upperPart2;
    private TextView errorMessageForAmount, errorMessageForRemarks, errorMessageForSpinner, userNameTxt, accMaskTxt, amountTxt, friendNameTxt, accFrindMaskTxt, validateBtn, reviewBtn;
    private Spinner purposesSpinner;
    private String purposeSelected;
    private String stan, rrn;
    private String sourceBalance, sourceuserName, sourceAccount, friendNumber, destinationName;
    private ArrayList<String> purpStringArrayList;
    private String friendImg;
    private String[] purpose = {
            "School Fees",
            "School Fees",
            "School Fees",
            "School Fees",
            "School Fees"

    };
    private Utils utils;
    private SwitchCompat switchCompat;
    private boolean isSource = false;
    private boolean isDestination = false;
    private boolean isFirst = false;
    private String isFav = "SAMBA";
    private String userID, branchCode, email, mobile;
    private TextView doneBtn, textView, heading;
    private Dialog dialog;
    private String friendID, sourceCurrency;
    private String bankCode, flag = "0";
    private String isSelectSource, isSelectDestination;
    private RoundImageView title;
    private TextView titleTxt;
    private int color;
    private TextView friendNameText;
    private AppPreferences appPreferences;
    private String location;
    private boolean isValidCurrency = false;
    String userType;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.request_money_fragment, container, false);

        initViews(convertView);
        // new log
        FirebaseAnalytics.getInstance(getContext()).logEvent("Request_Money_Pressed", new Bundle());



        editRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                remarks = editable.toString();
            }
        });


        editAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForAmount.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForAmount.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                amount = editable.toString();
            }
        });


        upperPart1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                upperPart1.setEnabled(false);
                Intent intent = new Intent(getActivity(), ManageFriends.class);
                intent.putExtra("source", "destination");
                startActivity(intent);
                isDestination = true;
                isSource = false;
                isFirst = true;

            }
        });

        upperPart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                upperPart2.setEnabled(false);
                isSource = true;
                isDestination = false;
                isFirst = true;
                Intent intent = new Intent(getActivity(), SelectAccountScreen.class);
                intent.putExtra("source", "destination");
                startActivity(intent);


            }
        });
        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (userType.equals("NON_SAMBA")) {
                    FirebaseAnalytics.getInstance(getContext()).logEvent("Non_Samba_Request_Money_Review_Pressed", new Bundle());
                } else {
                    FirebaseAnalytics.getInstance(getContext()).logEvent("Request_Money_Review_Pressed", new Bundle());
                }

                remarks = editRemarks.getText().toString().trim();
                amount = editAmount.getText().toString().trim();

                try {
                    CryptLib _crypt = new CryptLib();
                    remarks = editRemarks.getText().toString().trim();
                    amount = editAmount.getText().toString().trim();

                    if (TextUtils.isEmpty(amount)) {

                        errorMessageForAmount.setVisibility(View.VISIBLE);
                        errorMessageForAmount.bringToFront();
                        errorMessageForAmount.setError("");
                        errorMessageForAmount.setText("Please enter valid amount");

                    } else if (!helper.validateInputForSC(amount) || amount.contains(" ")) {

                        errorMessageForAmount.setVisibility(View.VISIBLE);
                        errorMessageForAmount.bringToFront();
                        errorMessageForAmount.setError("");
                        errorMessageForAmount.setText("Amount cannot contains <,>,\",',%,(,),&,+,\\,~,space");


                    } else if (amount.startsWith("0")) {

                        amount = amount.replaceFirst("^0+(?!$)", "");
                        if (!TextUtils.isEmpty(amount) && !amount.equals("")) {

                            if (amount.equals("0")) {
                                errorMessageForAmount.setVisibility(View.VISIBLE);
                                errorMessageForAmount.bringToFront();
                                errorMessageForAmount.setError("");
                                errorMessageForAmount.setText("Please enter valid amount");
                            }

                        } else {

                            errorMessageForAmount.setVisibility(View.VISIBLE);
                            errorMessageForAmount.bringToFront();
                            errorMessageForAmount.setError("");
                            errorMessageForAmount.setText("Please enter valid amount");
                        }


                    }

                    if (TextUtils.isEmpty(remarks)) {

                        remarks = "-";

                    } else if (!helper.validateInputForSC(remarks)) {

                        errorMessageForRemarks.setVisibility(View.VISIBLE);
                        errorMessageForRemarks.bringToFront();
                        errorMessageForRemarks.setError("");
                        errorMessageForRemarks.setText("Amount cannot contains <,>,\",',%,(,),&,+,\\,~");


                    }

                    if (purposeSelected.equals("Purpose of Transfer")) {

                        errorMessageForSpinner.setVisibility(View.VISIBLE);
                        errorMessageForSpinner.bringToFront();
                        errorMessageForSpinner.setError("");
                        errorMessageForSpinner.setText("Please select purpose of transfer");
                    }

                    if ((TextUtils.isEmpty(sourceAccount) || sourceAccount.equals("N/A")) || (TextUtils.isEmpty(sourceuserName) || sourceuserName.equals("N/A"))) {

                        utils.showDilogForError("Please select Receiving account.", "WARNING");

                    }

                    if ((TextUtils.isEmpty(destinationName) || destinationName.equals("N/A"))) {

                        showDilogForError("Please select a Friend.", "WARNING");

                    }

                    if (sourceCurrency.toUpperCase().contains("PKR")) {

                        isValidCurrency = true;
                    } else {

                        showDilogForError("You cannot request money in FCY account.", "WARNING");
                        isValidCurrency = false;
                    }

                    if (!TextUtils.isEmpty(amount) && !amount.startsWith("0") && !TextUtils.isEmpty(remarks) && !purposeSelected.equals("Purpose of Transfer") && (!TextUtils.isEmpty(sourceAccount) && !sourceAccount.equals("N/A")) && (!TextUtils.isEmpty(sourceuserName) && !sourceuserName.equals("N/A")) && (!TextUtils.isEmpty(destinationName) && !destinationName.equals("N/A")) && isValidCurrency == true) {


                        //Send to confirm
                        Intent intent = new Intent(getActivity(), RequestMoneyReviewScreen.class);
                        intent.putExtra("userName", sourceuserName);
                        intent.putExtra("balance", sourceBalance);
                        intent.putExtra("accountNumber", sourceAccount);
                        intent.putExtra("purpose", purposeSelected);
                        intent.putExtra("destionationName", destinationName);
                        intent.putExtra("friendNumber", friendNumber);
                        intent.putExtra("mobile", mobile);
                        intent.putExtra("remarks", remarks);
                        intent.putExtra("isFav", isFav);
                        intent.putExtra("stan", stan);
                        intent.putExtra("rrn", rrn);
                        intent.putExtra("amount", amount);
                        intent.putExtra("branchCode", branchCode);
                        intent.putExtra("userID", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        intent.putExtra("friendID", friendID);
                        intent.putExtra("flag", flag);
                        intent.putExtra("bankCode", bankCode);
                        intent.putExtra("currency", sourceCurrency);
                        intent.putExtra("friendImg", friendImg);
                        getActivity().overridePendingTransition(0, 0);
                        startActivity(intent);
                        validateBtn.setEnabled(true);
//                        constants.accountsEditor.clear();
//                        constants.accountsEditor.commit();
//                        constants.accountsEditor.apply();
//                        constants.destinationAccountEditor.clear();
//                        constants.destinationAccountEditor.commit();
//                        constants.destinationAccountEditor.apply();

//                        getActivity().finish();
                    }


                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        });

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

//                Toast.makeText(getActivity(),"Test---"+isChecked,Toast.LENGTH_SHORT).show();
                if (isChecked) {
                    editShareAccount.setText("Yes");
                    flag = "1";
                } else {
                    editShareAccount.setText("No");
                    flag = "0";
                }
            }
        });

        return convertView;
    }

    public void initViews(View convertView) {

        titleTxt = (TextView) convertView.findViewById(R.id.titleTxt);
        title = (RoundImageView) convertView.findViewById(R.id.title);
        titleTxt.bringToFront();
        titleTxt.setVisibility(View.INVISIBLE);
        title.setVisibility(View.INVISIBLE);
        profile_image = (CircleImageView) convertView.findViewById(R.id.profile_image);
        purposesSpinner = (Spinner) convertView.findViewById(R.id.purposeSpinner);
        validateBtn = (TextView) convertView.findViewById(R.id.reviewBtn);
        upperPart1 = (RelativeLayout) convertView.findViewById(R.id.upperPart1);
        upperPart2 = (RelativeLayout) convertView.findViewById(R.id.upperPart2);
        friendNameTxt = (TextView) convertView.findViewById(R.id.friendNameTxt);
        friendNameTxt.setText("Select Destination Account ");
        userNameTxt = (TextView) convertView.findViewById(R.id.userNameTxt);
        friendNameText = (TextView) convertView.findViewById(R.id.userNameTxt1);
        accMaskTxt = (TextView) convertView.findViewById(R.id.accMaskTxt);
        amountTxt = (TextView) convertView.findViewById(R.id.amountTxt);
        accFrindMaskTxt = (TextView) convertView.findViewById(R.id.accFrindMaskTxt);
        accFrindMaskTxt.setVisibility(View.INVISIBLE);
        userNameTxt.setText("Select Friend");
        errorMessageForAmount = (TextView) convertView.findViewById(R.id.errorMessageForAmount);
        errorMessageForRemarks = (TextView) convertView.findViewById(R.id.errorMessageForRemarks);
        errorMessageForSpinner = (TextView) convertView.findViewById(R.id.errorMessageForSpinner);
        editShareAccount = (EditText) convertView.findViewById(R.id.editShareAccount);
        editShareAccount.setText("No");
        purpStringArrayList = new ArrayList<>();
        editRemarks = (EditText) convertView.findViewById(R.id.editRemarks);
        editAmount = (EditText) convertView.findViewById(R.id.editAmount);
        switchCompat = (SwitchCompat) convertView.findViewById(R.id.switchCompat);
        requestQueue = Volley.newRequestQueue(getActivity());
        webService = new WebService();
        helper = Helper.getHelper(getActivity());
        constants = Constants.getConstants(getActivity());
        utils = new Utils(getActivity());
        pd = new Dialog(getActivity());
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        appPreferences = new AppPreferences(getActivity());
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        try {
            CryptLib _crypt = new CryptLib();
            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }




        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        String menus = constants.sharedPreferences.getString("Menus", "N/A");

        if (!TextUtils.isEmpty(menus) && !menus.equals("N/A") && !menus.equals("")) {

            if (menus.toLowerCase().contains("request money")) {
                showDilogForErrorForMenu("This service is currently disabled. Please try again later.", "WARNING");
            }
        }

        String list = constants.purposeList.getString("purposeList", "N/A");

        if (list.equals("N/A")) {
            GetPurposeList();
            showSpinner();
        } else {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();

            purpStringArrayList = gson.fromJson(list, type);
            purpose = new String[purpStringArrayList.size()];
            purpose = purpStringArrayList.toArray(purpose);
            spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
            purposeAdapter.addAll(purpose);
            purposeAdapter.add("Purpose of Transfer");
            purposesSpinner.setAdapter(purposeAdapter);
            purposesSpinner.setSelection(0);
            purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    try {


                        // Get select item
                        if (purposesSpinner.getSelectedItem() == "Purpose of Transfer") {
                            purposeSelected = purposesSpinner.getSelectedItem().toString();
                            Log.d("identity", "---" + purposeSelected);

                        } else {

                            purposeSelected = purposesSpinner.getSelectedItem().toString();

                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub
                }
            });
        }

        int stanNumber = 6;
        stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
        int rrnNumber = 12;
        rrn = String.valueOf(helper.generateRandom(rrnNumber));
        Intent intent = getActivity().getIntent();

        String requestType = intent.getStringExtra("requestFunds");

        if (!TextUtils.isEmpty(requestType) && requestType.equals("nonsambarequest")) {

            sourceuserName = intent.getStringExtra("accountName");
            sourceBalance = intent.getStringExtra("balance");
            sourceAccount = intent.getStringExtra("accountNumber");
            branchCode = intent.getStringExtra("branchCode");
            bankCode = intent.getStringExtra("bankCode");
            sourceCurrency = intent.getStringExtra("currency");
            friendNameTxt.setText(sourceuserName);
            accFrindMaskTxt.setVisibility(View.VISIBLE);
            accFrindMaskTxt.setText(utils.getMaskedString(sourceAccount));

        }


        String type4 = intent.getStringExtra("ft");

        if (!TextUtils.isEmpty(type4) && type4.equals("ft")) {

            sourceuserName = intent.getStringExtra("accountName");
            sourceCurrency = intent.getStringExtra("currency");
            sourceBalance = intent.getStringExtra("balance");
            sourceAccount = intent.getStringExtra("accountNumber");
            branchCode = intent.getStringExtra("branchCode");
            bankCode = intent.getStringExtra("bankCode");
            friendNameTxt.setText(sourceuserName);
            accFrindMaskTxt.setVisibility(View.VISIBLE);
            accFrindMaskTxt.setText(utils.getMaskedString(sourceAccount));
//            amountTxt.setVisibility(View.VISIBLE);
//            amountTxt.setText(sourceCurrency + " " + sourceBalance);

        }

        String friendRequest = intent.getStringExtra("requestFunds");
        if (!TextUtils.isEmpty(friendRequest) && friendRequest.equals("friendRequest")) {

            friendNumber = intent.getStringExtra("friendNumber");
            if (friendNumber.equals("N/A")) {

            } else {
                destinationName = intent.getStringExtra("friendName");
                String s = destinationName.substring(0, 1);

                switch (s.toUpperCase()) {

                    case "A":
                        color = getResources().getColor(R.color.friendColor1);
                        break;

                    case "B":
                        color = getResources().getColor(R.color.friendColor1);
                        break;

                    case "C":
                        color = getResources().getColor(R.color.friendColor1);
                        break;

                    case "D":
                        color = getResources().getColor(R.color.friendColor1);
                        break;

                    case "E":
                        color = getResources().getColor(R.color.friendColor1);
                        break;

                    case "F":
                        color = getResources().getColor(R.color.friendColor1);
                        break;

                    case "G":
                        color = getResources().getColor(R.color.friendColor1);
                        break;

                    case "H":
                        color = getResources().getColor(R.color.friendColor2);
                        break;

                    case "I":
                        color = getResources().getColor(R.color.friendColor2);
                        break;

                    case "J":
                        color = getResources().getColor(R.color.friendColor2);
                        break;

                    case "K":
                        color = getResources().getColor(R.color.friendColor2);
                        break;

                    case "L":
                        color = getResources().getColor(R.color.friendColor2);
                        break;

                    case "M":
                        color = getResources().getColor(R.color.friendColor2);
                        break;

                    case "N":
                        color = getResources().getColor(R.color.friendColor3);
                        break;

                    case "O":
                        color = getResources().getColor(R.color.friendColor3);
                        break;

                    case "P":
                        color = getResources().getColor(R.color.friendColor3);
                        break;

                    case "Q":
                        color = getResources().getColor(R.color.friendColor3);
                        break;

                    case "R":
                        color = getResources().getColor(R.color.friendColor3);
                        break;

                    case "S":
                        color = getResources().getColor(R.color.friendColor3);
                        break;

                    case "T":
                        color = getResources().getColor(R.color.friendColor4);
                        break;

                    case "U":
                        color = getResources().getColor(R.color.friendColor4);
                        break;

                    case "V":
                        color = getResources().getColor(R.color.friendColor4);
                        break;

                    case "W":
                        color = getResources().getColor(R.color.friendColor4);
                        break;

                    case "X":
                        color = getResources().getColor(R.color.friendColor4);
                        break;

                    case "Y":
                        color = getResources().getColor(R.color.friendColor4);
                        break;

                    case "Z":
                        color = getResources().getColor(R.color.friendColor4);
                        break;

                    default:
                        color = getResources().getColor(R.color.defaultColor);
                        break;
                }
                userID = intent.getStringExtra("userId");
                friendID = intent.getStringExtra("friendID");
//                email = intent.getStringExtra("friendName");
                mobile = intent.getStringExtra("userNumber");
                isFav = intent.getStringExtra("isFav");
                userNameTxt.setVisibility(View.INVISIBLE);
                friendNameText.setVisibility(View.VISIBLE);
                friendNameText.setText(destinationName);
                String friendImage = intent.getStringExtra("friendImage");
                if (!TextUtils.isEmpty(friendImage)) {
                    friendImg = friendImage;
                    titleTxt.setVisibility(View.INVISIBLE);
                    title.setVisibility(View.INVISIBLE);
                    profile_image.setVisibility(View.VISIBLE);
                    byte[] imageByteArray = Base64.decode(friendImage, Base64.DEFAULT);
                    RequestOptions options = new RequestOptions();
                    options.centerCrop();
                    Glide.with(getActivity())
                            .load(imageByteArray)
                            .apply(options)
                            .into(profile_image);

                } else {
                    titleTxt.setVisibility(View.VISIBLE);
                    title.setVisibility(View.VISIBLE);
                    profile_image.setVisibility(View.INVISIBLE);
                    title.setBackgroundTintList(ColorStateList.valueOf((color)));
                    titleTxt.setText(s.toUpperCase());
                }

            }
        }

    }


    public void GetPurposeList() {

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {
//                webService.GetPurposeOfTransfer(constants.sharedPreferences.getString("userName", "N/A"), "FT", constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getGetPurposeList();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Information is not valid!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
////                                ProfileSetupScreen.pager.setCurrentItem(4);
////                                ProfileSetupScreen.currentItem++;
//
//
//                            } else {
//
//                                try {
//
//
//                                    JSONArray jsonArray = new JSONArray(jsonObject.getString("POTList"));
//                                    for (int i = 0; i < jsonArray.length(); i++) {
//                                        try {
//
//                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
//
//                                            purpStringArrayList.add(jsonObj.getString("Name"));
//
//                                        } catch (Exception ex) {
//                                            ex.printStackTrace();
//                                            pd.dismiss();
//                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                            validateBtn.setEnabled(true);
//                                        }
//
//                                    }
//
//                                    purpose = new String[purpStringArrayList.size()];
//                                    purpose = purpStringArrayList.toArray(purpose);
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    validateBtn.setEnabled(true);
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//                        }
//                    }
//                }, 3000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sPurposeGroupCode", "FT");
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(getActivity());

                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getPuposesURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("00")) {
                                            try {

                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("POTList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        purpStringArrayList.add(jsonObj.getString("Name"));

                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        pd.dismiss();
                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        validateBtn.setEnabled(true);
                                                    }

                                                }

                                                purpose = new String[purpStringArrayList.size()];
                                                purpose = purpStringArrayList.toArray(purpose);


                                            } catch (JSONException ex) {

                                                ex.printStackTrace();
                                                pd.dismiss();
                                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } else if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());
                                            validateBtn.setEnabled(true);

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                        }
                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sPurposeGroupCode", "FT");
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
        }
    }

    //    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//
//            constants = Constants.getConstants(getActivity());
//            constants.destinationAccountEditor.putString("isSelect","No");
//            constants.destinationAccountEditor.commit();
//            constants.destinationAccountEditor.apply();
//            constants.sourceAccountEditor.putString("isSelect","No");
//            constants.sourceAccountEditor.commit();
//            constants.sourceAccountEditor.apply();
//        } else {
//
//            //Do Nothing
//        }
//    }
    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                pd.dismiss();
                spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                purposeAdapter.addAll(purpose);
                purposeAdapter.add("Purpose of Transfer");
                purposesSpinner.setAdapter(purposeAdapter);
                purposesSpinner.setSelection(0);
                purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


                            // Get select item
                            if (purposesSpinner.getSelectedItem() == "Purpose of Transfer") {
                                purposeSelected = purposesSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + purposeSelected);

                            } else {

                                purposeSelected = purposesSpinner.getSelectedItem().toString();

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });

            }

        }, 5000);
    }


    @Override
    public void onResume() {
        super.onResume();

        try {

            CryptLib _crypt = new CryptLib();
            if (isFirst) {

                upperPart1.setEnabled(true);
                upperPart2.setEnabled(true);
                if (isDestination) {

                    isSelectSource = constants.sourceAccount.getString("isSelect", "N/A");
                    if (!isSelectSource.equals("N/A") && isSelectSource.equals("Yes")) {
                        friendNumber = _crypt.decrypt(constants.sourceAccount.getString("friendNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        if (friendNumber.equals("N/A")) {

                        } else {

                            destinationName = _crypt.decrypt(constants.sourceAccount.getString("friendName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            String s = destinationName.substring(0, 1);

                            switch (s.toUpperCase()) {

                                case "A":
                                    color = getResources().getColor(R.color.friendColor1);
                                    break;

                                case "B":
                                    color = getResources().getColor(R.color.friendColor1);
                                    break;

                                case "C":
                                    color = getResources().getColor(R.color.friendColor1);
                                    break;

                                case "D":
                                    color = getResources().getColor(R.color.friendColor1);
                                    break;

                                case "E":
                                    color = getResources().getColor(R.color.friendColor1);
                                    break;

                                case "F":
                                    color = getResources().getColor(R.color.friendColor1);
                                    break;

                                case "G":
                                    color = getResources().getColor(R.color.friendColor1);
                                    break;

                                case "H":
                                    color = getResources().getColor(R.color.friendColor2);
                                    break;

                                case "I":
                                    color = getResources().getColor(R.color.friendColor2);
                                    break;

                                case "J":
                                    color = getResources().getColor(R.color.friendColor2);
                                    break;

                                case "K":
                                    color = getResources().getColor(R.color.friendColor2);
                                    break;

                                case "L":
                                    color = getResources().getColor(R.color.friendColor2);
                                    break;

                                case "M":
                                    color = getResources().getColor(R.color.friendColor2);
                                    break;

                                case "N":
                                    color = getResources().getColor(R.color.friendColor3);
                                    break;

                                case "O":
                                    color = getResources().getColor(R.color.friendColor3);
                                    break;

                                case "P":
                                    color = getResources().getColor(R.color.friendColor3);
                                    break;

                                case "Q":
                                    color = getResources().getColor(R.color.friendColor3);
                                    break;

                                case "R":
                                    color = getResources().getColor(R.color.friendColor3);
                                    break;

                                case "S":
                                    color = getResources().getColor(R.color.friendColor3);
                                    break;

                                case "T":
                                    color = getResources().getColor(R.color.friendColor4);
                                    break;

                                case "U":
                                    color = getResources().getColor(R.color.friendColor4);
                                    break;

                                case "V":
                                    color = getResources().getColor(R.color.friendColor4);
                                    break;

                                case "W":
                                    color = getResources().getColor(R.color.friendColor4);
                                    break;

                                case "X":
                                    color = getResources().getColor(R.color.friendColor4);
                                    break;

                                case "Y":
                                    color = getResources().getColor(R.color.friendColor4);
                                    break;

                                case "Z":
                                    color = getResources().getColor(R.color.friendColor4);
                                    break;

                                default:
                                    color = getResources().getColor(R.color.defaultColor);
                                    break;
                            }
                            userID = _crypt.decrypt(constants.sourceAccount.getString("userID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            friendID = _crypt.decrypt(constants.sourceAccount.getString("friendID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            String em = constants.sourceAccount.getString("email", "N/A");
                            if (!TextUtils.isEmpty(em) && !em.equals("N/A")) {
                                email = _crypt.decrypt(constants.sourceAccount.getString("email", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            } else {
                                email = "N/A";
                            }
                            String mobb = constants.sourceAccount.getString("mobileNumber", "N/A");
                            if (!TextUtils.isEmpty(mobb) && !mobb.equals("N/A")) {
                                mobile = _crypt.decrypt(constants.sourceAccount.getString("mobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            } else {
                                mobile = "N/A";
                            }

                            isFav = _crypt.decrypt(constants.sourceAccount.getString("isFav", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            userNameTxt.setVisibility(View.INVISIBLE);
                            friendNameText.setVisibility(View.VISIBLE);
                            friendNameText.setText(destinationName);
                            friendImg = _crypt.decrypt(constants.sourceAccount.getString("friendImage", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                            if (!TextUtils.isEmpty(friendImg) && !friendImg.equals("N/A")) {
                                titleTxt.setVisibility(View.INVISIBLE);
                                title.setVisibility(View.INVISIBLE);
                                profile_image.setVisibility(View.VISIBLE);
                                byte[] imageByteArray = Base64.decode(friendImg, Base64.DEFAULT);
                                RequestOptions options = new RequestOptions();
                                options.centerCrop();
                                Glide.with(getActivity())
                                        .load(imageByteArray)
                                        .apply(options)
                                        .into(profile_image);

                            } else {
                                titleTxt.setVisibility(View.VISIBLE);
                                title.setVisibility(View.VISIBLE);
                                profile_image.setVisibility(View.INVISIBLE);
                                title.setBackgroundTintList(ColorStateList.valueOf((color)));
                                titleTxt.setText(s.toUpperCase());
                            }
                        }
                    }

                    constants.sourceAccountEditor.putString("isSelect", "No");
                    constants.sourceAccountEditor.commit();
                    constants.sourceAccountEditor.apply();
                    isDestination = false;
                }

                if (isSource) {

                    isSelectDestination = (constants.destinationAccount.getString("isSelect", "N/A"));
                    if (!isSelectDestination.equals("N/A") && isSelectDestination.equals("Yes")) {
                        sourceuserName = _crypt.decrypt(constants.destinationAccount.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        if (sourceuserName.equals("N/A")) {

                        } else {
                            sourceBalance = _crypt.decrypt(constants.destinationAccount.getString("balance", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            sourceAccount = _crypt.decrypt(constants.destinationAccount.getString("accountNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            branchCode = _crypt.decrypt(constants.destinationAccount.getString("branchCode", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            bankCode = _crypt.decrypt(constants.destinationAccount.getString("bankCode", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            sourceCurrency = _crypt.decrypt(constants.destinationAccount.getString("currency", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            friendNameTxt.setText(sourceuserName);
                            accFrindMaskTxt.setVisibility(View.VISIBLE);
                            accFrindMaskTxt.setText(utils.getMaskedString(sourceAccount));

                        }
                    }

                    constants.destinationAccountEditor.putString("isSelect", "No");
                    constants.destinationAccountEditor.commit();
                    constants.destinationAccountEditor.apply();
                    isSource = false;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public void showDilogForErrorForMenu(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(getActivity(), HomeScreen.class);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).startActivity(intent);
                ((Activity) getActivity()).finish();
                ((Activity) getActivity()).finishAffinity();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


}