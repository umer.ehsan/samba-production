package com.ceesolutions.samba.transactions.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.transactions.FundTransferScreen;
import com.ceesolutions.samba.transactions.MoveMoneyReviewScreen;
import com.ceesolutions.samba.transactions.SelectAccountScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 3/9/18.
 */

public class MoveMoneyFragment extends Fragment {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private String remarks, amount, isFP;
    private EditText editAmount, editRemarks;
    private RelativeLayout upperPart1, upperPart2;
    private TextView errorMessageForAmount, errorMessageForRemarks, errorMessageForSpinner, userNameTxt, accMaskTxt, amountTxt, friendNameTxt, accFrindMaskTxt, validateBtn;
    private Spinner purposesSpinner;
    private String purposeSelected;
    private String[] purpose;
    private ArrayList<String> purpStringArrayList;
    private String sourceBalance, sourceuserName, sourceAccount, destinationAccount, destinationName, sourceProductType = "", destinationProductType = "";
    private Utils utils;
    private boolean isSourceMoveMoney = false;
    private boolean isDestinationMoveMoney = false;
    private boolean isFirst = false;
    private String identityValue, stan, rrn, type1;
    private String beneType = "SAMBA";
    private TextView doneBtn, textView, heading;
    private Dialog dialog;
    private String currency, sourceCurrency;
    private String bankCode, branchCode, mobile, email;
    private int amountToInt;
    private int bal;
    private Intent intent;
    private String isSelectSource, isSelectDestination;
    private boolean isVisible = false;
    private AppPreferences appPreferences;
    private String location;
    public String product;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.move_money_fragment, container, false);

        initViews(convertView);
        // new log
        FirebaseAnalytics.getInstance(getContext()).logEvent("Move_Money_Pressed", new Bundle());


        editRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForRemarks.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                remarks = editable.toString();
            }
        });


        editAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForAmount.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForAmount.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                amount = editable.toString();

            }
        });


        upperPart1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                upperPart1.setEnabled(false);
                isSourceMoveMoney = true;
                isDestinationMoveMoney = false;
                isFirst = true;
                Intent intent = new Intent(getActivity(), SelectAccountScreen.class);
                intent.putExtra("source", "source");
                startActivity(intent);


            }
        });

        upperPart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                upperPart2.setEnabled(false);
                Intent intent = new Intent(getActivity(), SelectAccountScreen.class);
                intent.putExtra("source", "destination");
                startActivity(intent);
                isDestinationMoveMoney = true;
                isSourceMoveMoney = false;
                isFirst = true;


            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAnalytics.getInstance(getContext()).logEvent("Move_Money_Review_Pressed", new Bundle());
                if (checkForRoshanPakistan()) {
                    ValidationCheckForFCVA();
                } else {
                    ValidationCheck();
                }

            }
        });

        return convertView;
    }

    private boolean checkForRoshanPakistan() {

        if (destinationProductType != null && destinationProductType.contains("FOREIGN CURRENCY VALUE AC") || destinationProductType.equals("NONRESIDENT RUPEEVALUE SAVING(NRVA)") || destinationProductType.equals("NON RESIDENT RUPEE VALUE CUR (NRVA)")) {
            return true;
        } else if (sourceProductType != null && sourceProductType.contains("FOREIGN CURRENCY VALUE AC") || sourceProductType.equals("NONRESIDENT RUPEEVALUE SAVING(NRVA)") || sourceProductType.equals("NON RESIDENT RUPEE VALUE CUR (NRVA)")) {
            return true;
        }
        return false;
    }

    public void initViews(View convertView) {

        purposesSpinner = (Spinner) convertView.findViewById(R.id.purposeSpinner);
        validateBtn = (TextView) convertView.findViewById(R.id.validateBtn);
        upperPart1 = (RelativeLayout) convertView.findViewById(R.id.upperPart1);
        upperPart2 = (RelativeLayout) convertView.findViewById(R.id.upperPart2);
        friendNameTxt = (TextView) convertView.findViewById(R.id.friendNameTxt);
        friendNameTxt.setText("Select Destination Account ");
        userNameTxt = (TextView) convertView.findViewById(R.id.userNameTxt);
        userNameTxt.setText("Select Source Account");
        accMaskTxt = (TextView) convertView.findViewById(R.id.accMaskTxt);
        accMaskTxt.setVisibility(View.INVISIBLE);
        amountTxt = (TextView) convertView.findViewById(R.id.amountTxt);
        amountTxt.setVisibility(View.INVISIBLE);
        accFrindMaskTxt = (TextView) convertView.findViewById(R.id.accFrindMaskTxt);
        accFrindMaskTxt.setVisibility(View.INVISIBLE);
        errorMessageForAmount = (TextView) convertView.findViewById(R.id.errorMessageForAmount);
        errorMessageForRemarks = (TextView) convertView.findViewById(R.id.errorMessageForRemarks);
        errorMessageForSpinner = (TextView) convertView.findViewById(R.id.errorMessageForSpinner);
        editRemarks = (EditText) convertView.findViewById(R.id.editRemarks);
        editAmount = (EditText) convertView.findViewById(R.id.editAmount);
        requestQueue = Volley.newRequestQueue(getActivity());
        webService = new WebService();
        helper = Helper.getHelper(getActivity());
        constants = Constants.getConstants(getActivity());
        pd = new Dialog(getActivity());
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        purpStringArrayList = new ArrayList<>();
        utils = new Utils(getActivity());
        appPreferences = new AppPreferences(getActivity());
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

//        String userType = constants.sharedPreferences.getString("type", "N/A");
//
//        if (!TextUtils.isEmpty(userType) && userType.equals("NON_SAMBA")) {
//
//
//            showDilogForErrorForNonSamba("This feature is only available for Samba Users.", "WARNING");
//
//
//        }

        String menus = constants.sharedPreferences.getString("Menus", "N/A");

        if (!TextUtils.isEmpty(menus) && !menus.equals("N/A") && !menus.equals("")) {

            if (menus.toLowerCase().contains("move money")) {
                showDilogForErrorForMenu("This service is currently disabled. Please try again later.", "WARNING");
            }
        }

        intent = getActivity().getIntent();
        type1 = intent.getStringExtra("requestFunds");
        String list = constants.purposeList.getString("purposeList", "N/A");

        if (list.equals("N/A")) {
            GetPurposeList();
            showSpinner();
        } else {
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            purpStringArrayList = gson.fromJson(list, type);
            purpose = new String[purpStringArrayList.size()];
            purpose = purpStringArrayList.toArray(purpose);

            if (!TextUtils.isEmpty(type1) && type1.equals("moveMoneyFromHome")) {

                destinationAccount = intent.getStringExtra("destinationAccount");
                destinationName = intent.getStringExtra("destinationTitle");
                currency = intent.getStringExtra("destinationCurrency");
                email = intent.getStringExtra("email");
                mobile = intent.getStringExtra("mobileNumber");
//                beneType = intent.getStringExtra("beneType");
                bankCode = intent.getStringExtra("destinationBankCode");
                friendNameTxt.setText(destinationName);
                accFrindMaskTxt.setVisibility(View.VISIBLE);
                accFrindMaskTxt.setText(utils.getMaskedString(destinationAccount));


                editAmount.setText(intent.getStringExtra("amount"));

//                    purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(intent.getStringExtra("purpose")));
                friendNameTxt.setText(destinationName);
                accFrindMaskTxt.setVisibility(View.VISIBLE);
                accFrindMaskTxt.setText(utils.getMaskedString(destinationAccount));
                sourceuserName = intent.getStringExtra("sourceTile");
                amount = intent.getStringExtra("amount");
                purposeSelected = intent.getStringExtra("remarks");
                spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                purposeAdapter.addAll(purpose);
                purposeAdapter.add("Purpose of Transfer");
                purposesSpinner.setAdapter(purposeAdapter);
                purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(intent.getStringExtra("purpose")));
                sourceBalance = intent.getStringExtra("sourceAmount");
                sourceAccount = intent.getStringExtra("sourceNumber");
                branchCode = intent.getStringExtra("sourceBranchCode");
                sourceCurrency = intent.getStringExtra("sourceCurrency");
                sourceProductType = intent.getStringExtra("sourceproduct");
                destinationProductType = intent.getStringExtra("destproduct");
                userNameTxt.setText(sourceuserName);
                accMaskTxt.setVisibility(View.VISIBLE);
                accMaskTxt.setText(utils.getMaskedString(sourceAccount));
                amountTxt.setVisibility(View.VISIBLE);
                amountTxt.setText(sourceCurrency + " " + sourceBalance);


                ValidationCheck();
            } else {
                purpStringArrayList = gson.fromJson(list, type);
                purpose = new String[purpStringArrayList.size()];
                purpose = purpStringArrayList.toArray(purpose);
                spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                purposeAdapter.addAll(purpose);
                purposeAdapter.add("Purpose of Transfer");
                purposesSpinner.setAdapter(purposeAdapter);
                purposesSpinner.setSelection(0);
                purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


                            // Get select item
                            if (purposesSpinner.getSelectedItem() == "Purpose of Transfer") {
                                purposeSelected = purposesSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + purposeSelected);

                            } else {

                                purposeSelected = purposesSpinner.getSelectedItem().toString();

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });
            }
        }

        int stanNumber = 6;
        stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
        int rrnNumber = 12;
        rrn = String.valueOf(helper.generateRandom(rrnNumber));

        Intent intent = getActivity().getIntent();
        String type4 = intent.getStringExtra("ft");

        if (!TextUtils.isEmpty(type4) && type4.equals("ft")) {

            sourceuserName = intent.getStringExtra("accountName");
            sourceCurrency = intent.getStringExtra("currency");
            sourceBalance = intent.getStringExtra("balance");
            sourceAccount = intent.getStringExtra("accountNumber");
            branchCode = intent.getStringExtra("branchCode");
            product = intent.getStringExtra("product");
            userNameTxt.setText(sourceuserName);
            accMaskTxt.setVisibility(View.VISIBLE);
            accMaskTxt.setText(utils.getMaskedString(sourceAccount));
            amountTxt.setVisibility(View.VISIBLE);
            amountTxt.setText(sourceCurrency + " " + sourceBalance);

        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            try {

                CryptLib _crypt = new CryptLib();
                constants = Constants.getConstants(getActivity());
                String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                if (!TextUtils.isEmpty(userType) && userType.equals("NON_SAMBA")) {

                    isVisible = true;
                    onAttach(getActivity());


                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            //Do Nothing
        }
    }

    public void GetPurposeList() {

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {
//                webService.GetPurposeOfTransfer(constants.sharedPreferences.getString("userName", "N/A"), "FT", constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getGetPurposeList();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Information is not valid!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
////                                ProfileSetupScreen.pager.setCurrentItem(4);
////                                ProfileSetupScreen.currentItem++;
//
//
//                            } else {
//
//                                try {
//
//
//                                    JSONArray jsonArray = new JSONArray(jsonObject.getString("POTList"));
//                                    for (int i = 0; i < jsonArray.length(); i++) {
//                                        try {
//
//                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
//
//                                            purpStringArrayList.add(jsonObj.getString("Name"));
//
//                                        } catch (Exception ex) {
//                                            ex.printStackTrace();
//                                            pd.dismiss();
//                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                            validateBtn.setEnabled(true);
//                                        }
//
//                                    }
//
//                                    purpose = new String[purpStringArrayList.size()];
//                                    purpose = purpStringArrayList.toArray(purpose);
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    validateBtn.setEnabled(true);
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//                        }
//                    }
//                }, 3000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sPurposeGroupCode", "FT");
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(getActivity());
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getPuposesURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("00")) {
                                            try {

                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("POTList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        purpStringArrayList.add(jsonObj.getString("Name"));

                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        pd.dismiss();
                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        validateBtn.setEnabled(true);
                                                    }

                                                }

                                                purpose = new String[purpStringArrayList.size()];
                                                purpose = purpStringArrayList.toArray(purpose);


                                            } catch (JSONException ex) {

                                                ex.printStackTrace();
                                                pd.dismiss();
                                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } else if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());
                                            validateBtn.setEnabled(true);

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                        }
                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sPurposeGroupCode", "FT");
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
        }
    }


//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//
//            GetPurposeList();
//            showSpinner();
//        } else {
//
//            //Do Nothing
//        }
//    }


    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                pd.dismiss();
                if (!TextUtils.isEmpty(type1) && type1.equals("moveMoneyFromHome")) {

                    destinationAccount = intent.getStringExtra("destinationAccount");
                    destinationName = intent.getStringExtra("destinationTitle");
                    currency = intent.getStringExtra("destinationCurrency");
                    email = intent.getStringExtra("email");
                    mobile = intent.getStringExtra("mobileNumber");
                    beneType = intent.getStringExtra("beneType");
                    bankCode = intent.getStringExtra("destinationBankCode");
                    friendNameTxt.setText(destinationName);
                    accFrindMaskTxt.setVisibility(View.VISIBLE);
                    accFrindMaskTxt.setText(utils.getMaskedString(destinationAccount));


                    editAmount.setText(intent.getStringExtra("amount"));

//                    purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(intent.getStringExtra("purpose")));
                    friendNameTxt.setText(destinationName);
                    accFrindMaskTxt.setVisibility(View.VISIBLE);
                    accFrindMaskTxt.setText(utils.getMaskedString(destinationAccount));
                    sourceuserName = intent.getStringExtra("sourceTile");
                    amount = intent.getStringExtra("amount");
                    purposeSelected = intent.getStringExtra("remarks");
                    spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Transfer");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(intent.getStringExtra("purpose")));
                    sourceBalance = intent.getStringExtra("sourceAmount");
                    sourceAccount = intent.getStringExtra("sourceNumber");
                    branchCode = intent.getStringExtra("sourceBranchCode");
                    sourceCurrency = intent.getStringExtra("sourceCurrency");
                    userNameTxt.setText(sourceuserName);
                    accMaskTxt.setVisibility(View.VISIBLE);
                    accMaskTxt.setText(utils.getMaskedString(sourceAccount));
                    amountTxt.setVisibility(View.VISIBLE);
                    amountTxt.setText(sourceCurrency + " " + sourceBalance);
                    ValidationCheck();

                } else {
                    spinnerAdapter purposeAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_fp);
                    purposeAdapter.addAll(purpose);
                    purposeAdapter.add("Purpose of Transfer");
                    purposesSpinner.setAdapter(purposeAdapter);
                    purposesSpinner.setSelection(0);
                    purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            try {


                                // Get select item
                                if (purposesSpinner.getSelectedItem() == "Purpose of Transfer") {
                                    purposeSelected = purposesSpinner.getSelectedItem().toString();
                                    Log.d("identity", "---" + purposeSelected);

                                } else {

                                    purposeSelected = purposesSpinner.getSelectedItem().toString();

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // TODO Auto-generated method stub
                        }
                    });

                }
            }

        }, 5000);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {

            CryptLib _crypt = new CryptLib();
            if (isFirst) {

                upperPart1.setEnabled(true);
                upperPart2.setEnabled(true);
                if (isSourceMoveMoney) {

                    isSelectSource = constants.sourceAccount.getString("isSelect", "N/A");
                    if (!isSelectSource.equals("N/A") && isSelectSource.equals("Yes")) {
                        sourceuserName = _crypt.decrypt(constants.sourceAccount.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        if (sourceuserName.equals("N/A")) {
                        } else {
                            sourceBalance = _crypt.decrypt(constants.sourceAccount.getString("balance", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            sourceAccount = _crypt.decrypt(constants.sourceAccount.getString("accountNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            branchCode = _crypt.decrypt(constants.sourceAccount.getString("branchCode", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            sourceCurrency = _crypt.decrypt(constants.sourceAccount.getString("currency", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            sourceProductType = _crypt.decrypt(constants.sourceAccount.getString("SourceProductType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                            userNameTxt.setText(sourceuserName);

                            accMaskTxt.setVisibility(View.VISIBLE);

                            if (!sourceAccount.equals("N/A")) {
                                accMaskTxt.setText(utils.getMaskedString(sourceAccount));
                                amountTxt.setVisibility(View.VISIBLE);
                            } else {

                                accMaskTxt.setText(sourceAccount);
                                amountTxt.setVisibility(View.VISIBLE);

                            }
                            amountTxt.setText(sourceCurrency + " " + sourceBalance);
                        }
                    }

                    constants.sourceAccountEditor.putString("isSelect", "No");
                    constants.sourceAccountEditor.commit();
                    constants.sourceAccountEditor.apply();
                    isSourceMoveMoney = false;
                }


                if (isDestinationMoveMoney) {

                    isSelectDestination = constants.destinationAccount.getString("isSelect", "N/A");
                    if (!isSelectDestination.equals("N/A") && isSelectDestination.equals("Yes")) {
                        destinationAccount = _crypt.decrypt(constants.destinationAccount.getString("accountNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                        if (destinationAccount.equals("N/A")) {
                        } else {
                            destinationName = _crypt.decrypt(constants.destinationAccount.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            bankCode = _crypt.decrypt(constants.destinationAccount.getString("bankCode", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            currency = _crypt.decrypt(constants.destinationAccount.getString("currency", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            destinationProductType = _crypt.decrypt(constants.destinationAccount.getString("DestProductType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            friendNameTxt.setText(destinationName);
                            accFrindMaskTxt.setVisibility(View.VISIBLE);
                            accFrindMaskTxt.setText(utils.getMaskedString(destinationAccount));
                        }
                    }

                    constants.destinationAccountEditor.putString("isSelect", "No");
                    constants.destinationAccountEditor.commit();
                    constants.destinationAccountEditor.apply();
                    isDestinationMoveMoney = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void Verification() {


        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                int stanNumber = 6;
                stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
                int rrnNumber = 12;
                rrn = String.valueOf(helper.generateRandom(rrnNumber));
//                webService.TitleFetch(constants.sharedPreferences.getString("userName", "N/A"), sourceAccount, destinationAccount, sourceCurrency, bankCode, constants.sharedPreferences.getString("LegalID", "N/A"), stan, rrn, beneType, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getTitleFetch();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        Intent intent = new Intent(getActivity(), MoveMoneyReviewScreen.class);
//                                        intent.putExtra("userName", sourceuserName);
//                                        intent.putExtra("balance", sourceBalance);
//                                        intent.putExtra("accountNumber", sourceAccount);
//                                        intent.putExtra("purpose", purposeSelected);
//                                        intent.putExtra("destionationName", destinationName);
//                                        intent.putExtra("destinationAccount", destinationAccount);
//                                        intent.putExtra("remarks", remarks);
//                                        intent.putExtra("beneType", beneType);
//                                        intent.putExtra("beneID", "1");
//                                        intent.putExtra("stan", stan);
//                                        intent.putExtra("rrn", rrn);
//                                        intent.putExtra("amount", amount);
//                                        intent.putExtra("branchCode", branchCode);
//                                        intent.putExtra("bankCode", bankCode);
//                                        intent.putExtra("currency", sourceCurrency);
//                                        intent.putExtra("chargeCode", jsonObject.getString("ChargeCode"));
//                                        intent.putExtra("conversionRate", jsonObject.getString("ConversionRate"));
//                                        getActivity().overridePendingTransition(0, 0);
//                                        startActivity(intent);
//                                        validateBtn.setEnabled(true);
////                                        getActivity().overridePendingTransition(0, 0);
////                                        getActivity().finish();
//
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                        validateBtn.setEnabled(true);
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    validateBtn.setEnabled(true);
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 15000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sToAccountNumber", destinationAccount);
                params.put("sBankID", bankCode);
                params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sSTAN", stan);
                params.put("sRRN", rrn);
                params.put("sBeneType", beneType);
                params.put("sFromAccountNumber", sourceAccount);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sBeneCurrencyCode", sourceCurrency);
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(getActivity());
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.titleFetchURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Code").equals("00")) {
//                                        JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Code"));
//                                        JsonObj.put("ChargeCode", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("ChargeCode"));
//                                        JsonObj.put("ConversionRate", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("ConversionRate"));
//                                        JsonObj.put("AccountTitle", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("AccountTitle"));
//                                        JsonObj.put("Currency", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Currency"));

                                            pd.dismiss();
                                            Intent intent = new Intent(getActivity(), MoveMoneyReviewScreen.class);
                                            intent.putExtra("userName", sourceuserName);
                                            intent.putExtra("balance", sourceBalance);
                                            intent.putExtra("accountNumber", sourceAccount);
                                            intent.putExtra("purpose", purposeSelected);
                                            intent.putExtra("destionationName", destinationName);
                                            intent.putExtra("destinationAccount", destinationAccount);
                                            intent.putExtra("remarks", remarks);
                                            intent.putExtra("beneType", beneType);
                                            intent.putExtra("beneID", "1");
                                            intent.putExtra("stan", stan);
                                            intent.putExtra("rrn", rrn);
                                            intent.putExtra("amount", amount);
                                            intent.putExtra("branchCode", branchCode);
                                            intent.putExtra("bankCode", bankCode);
                                            intent.putExtra("currency", sourceCurrency);
                                            intent.putExtra("chargeCode", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("ChargeCode"));
                                            intent.putExtra("conversionRate", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("ConversionRate"));
                                            intent.putExtra("titleFetch", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("AccountTitle"));
                                            getActivity().overridePendingTransition(0, 0);
                                            startActivity(intent);
                                            validateBtn.setEnabled(true);
//                                        getActivity().overridePendingTransition(0, 0);
//                                        getActivity().finish();
//
                                        } else if (jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());
                                            validateBtn.setEnabled(true);

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                        }

                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sToAccountNumber", destinationAccount);
//                        params.put("sBankID", bankCode);
//                        params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sSTAN", stan);
//                        params.put("sRRN", rrn);
//                        params.put("sBeneType", beneType);
//                        params.put("sFromAccountNumber", sourceAccount);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sBeneCurrencyCode", sourceCurrency);
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);

        }
    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        constants = Constants.getConstants(context);
        if (isVisible) {
//            showDilogForErrorForNonSamba("This feature is only available for Samba Users.", "WARNING");
            dialog = new Dialog((Activity) context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.error_dialog);
            textView = (TextView) dialog.findViewById(R.id.validatingTxt);
            doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
            heading = (TextView) dialog.findViewById(R.id.heading);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    FundTransferScreen.viewPager.setCurrentItem(2);
                }
            });
            textView.setText("This feature is only available for Samba Users.");
            heading.setText("WARNING");
            dialog.show();
//            isVisible = false;

        } else {
            try {

                CryptLib _crypt = new CryptLib();
                String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                if (!TextUtils.isEmpty(userType) && userType.equals("NON_SAMBA")) {
                    dialog = new Dialog((Activity) context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.error_dialog);
                    textView = (TextView) dialog.findViewById(R.id.validatingTxt);
                    doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
                    heading = (TextView) dialog.findViewById(R.id.heading);
                    dialog.setCancelable(false);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    doneBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            dialog.dismiss();
                            FundTransferScreen.viewPager.setCurrentItem(2);
                        }
                    });
                    textView.setText("This feature is only available for Samba Users.");
                    heading.setText("WARNING");
                    dialog.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void showDilogForErrorForMenu(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(getActivity(), HomeScreen.class);
                ((Activity) getActivity()).overridePendingTransition(0, 0);
                ((Activity) getActivity()).startActivity(intent);
                ((Activity) getActivity()).finish();
                ((Activity) getActivity()).finishAffinity();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


    public void showDilogForErrorForNonSamba(String msg, String header) {

        dialog = new Dialog((Activity) getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                FundTransferScreen.viewPager.setCurrentItem(2);
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public void ValidationCheck() {

        try {


            remarks = editRemarks.getText().toString().trim();
            amount = editAmount.getText().toString().trim();

            if (TextUtils.isEmpty(amount)) {

                errorMessageForAmount.setVisibility(View.VISIBLE);
                errorMessageForAmount.bringToFront();
                errorMessageForAmount.setError("");
                errorMessageForAmount.setText("Please enter valid amount");

            } else if (!helper.validateInputForSC(amount) || amount.contains(" ")) {

                errorMessageForAmount.setVisibility(View.VISIBLE);
                errorMessageForAmount.bringToFront();
                errorMessageForAmount.setError("");
                errorMessageForAmount.setText("Amount cannot contains <,>,\",',%,(,),&,+,\\,~,space");


            } else if (amount.startsWith("0")) {

                amount = amount.replaceFirst("^0+(?!$)", "");
                if (!TextUtils.isEmpty(amount) && !amount.equals("")) {

                    if (amount.equals("0")) {
                        errorMessageForAmount.setVisibility(View.VISIBLE);
                        errorMessageForAmount.bringToFront();
                        errorMessageForAmount.setError("");
                        errorMessageForAmount.setText("Please enter valid amount");
                    }

                } else {

                    errorMessageForAmount.setVisibility(View.VISIBLE);
                    errorMessageForAmount.bringToFront();
                    errorMessageForAmount.setError("");
                    errorMessageForAmount.setText("Please enter valid amount");
                }


            }


            if (TextUtils.isEmpty(remarks)) {

                remarks = "-";

            } else if (!helper.validateInputForSC(remarks)) {

                errorMessageForRemarks.setVisibility(View.VISIBLE);
                errorMessageForRemarks.bringToFront();
                errorMessageForRemarks.setError("");
                errorMessageForRemarks.setText("Amount cannot contains <,>,\",',%,(,),&,+,\\,~");


            }

            if (purposeSelected.equals("Purpose of Transfer")) {

                errorMessageForSpinner.setVisibility(View.VISIBLE);
                errorMessageForSpinner.bringToFront();
                errorMessageForSpinner.setError("");
                errorMessageForSpinner.setText("Please select purpose of transfer");
            }

            if ((TextUtils.isEmpty(sourceAccount) || sourceAccount.equals("N/A")) || (TextUtils.isEmpty(sourceBalance) || sourceBalance.equals("N/A")) || (TextUtils.isEmpty(sourceuserName) || sourceuserName.equals("N/A"))) {

                utils.showDilogForError("Please select Source account", "WARNING");

            }

            if ((TextUtils.isEmpty(destinationAccount) || destinationAccount.equals("N/A")) || (TextUtils.isEmpty(destinationName) || destinationName.equals("N/A"))) {

                showDilogForError("Please select Destination account", "WARNING");

            }

            if (sourceAccount.equals(destinationAccount)) {

                showDilogForError("Source account and Destination account cannot be same.", "WARNING");
            }

            if (!TextUtils.isEmpty(amount) && !amount.startsWith("0")) {

                amountToInt = Integer.valueOf(amount);

                if (!TextUtils.isEmpty(sourceBalance) && !sourceBalance.equals("N/A")) {

                    sourceBalance = sourceBalance.replaceAll(",", "");
                    bal = new BigDecimal(sourceBalance).intValue();
                    ;

                    if (amountToInt > bal) {

                        utils.showDilogForError("Amount cannot be greater than avaliable balance.", "WARNING");

                    }
                }
            }


            if (!sourceCurrency.equals("PKR") && !currency.equals("PKR") && !sourceCurrency.equals(currency)) {

                utils.showDilogForError("Funds transfer is not allowed between different FCY accounts", "ERROR");

            } else if (!sourceCurrency.equals(currency) && sourceCurrency.equals("PKR")) {


                utils.showDilogForError("Funds transfer is not allowed from PKR to FCY accounts", "ERROR");

            }

            if (!TextUtils.isEmpty(amount) && !amount.startsWith("0") && amountToInt <= bal && !TextUtils.isEmpty(remarks) && !purposeSelected.equals("Purpose of Transfer") && (!TextUtils.isEmpty(sourceAccount) && !sourceAccount.equals("N/A")) && (!TextUtils.isEmpty(sourceBalance) && !sourceBalance.equals("N/A")) && (!TextUtils.isEmpty(sourceuserName) && !sourceuserName.equals("N/A")) && (!TextUtils.isEmpty(destinationAccount) && !destinationAccount.equals("N/A")) && (!TextUtils.isEmpty(destinationName) && !destinationName.equals("N/A")) && !sourceAccount.equals(destinationAccount) && (sourceCurrency.equals(currency) || currency.equals("PKR"))) {
                validateBtn.setEnabled(false);
                Verification();

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void ValidationCheckForFCVA() {

        try {


            remarks = editRemarks.getText().toString().trim();
            amount = editAmount.getText().toString().trim();

            if (TextUtils.isEmpty(amount)) {

                errorMessageForAmount.setVisibility(View.VISIBLE);
                errorMessageForAmount.bringToFront();
                errorMessageForAmount.setError("");
                errorMessageForAmount.setText("Please enter valid amount");

            } else if (!helper.validateInputForSC(amount) || amount.contains(" ")) {

                errorMessageForAmount.setVisibility(View.VISIBLE);
                errorMessageForAmount.bringToFront();
                errorMessageForAmount.setError("");
                errorMessageForAmount.setText("Amount cannot contains <,>,\",',%,(,),&,+,\\,~,space");


            } else if (amount.startsWith("0")) {

                amount = amount.replaceFirst("^0+(?!$)", "");
                if (!TextUtils.isEmpty(amount) && !amount.equals("")) {

                    if (amount.equals("0")) {
                        errorMessageForAmount.setVisibility(View.VISIBLE);
                        errorMessageForAmount.bringToFront();
                        errorMessageForAmount.setError("");
                        errorMessageForAmount.setText("Please enter valid amount");
                    }

                } else {

                    errorMessageForAmount.setVisibility(View.VISIBLE);
                    errorMessageForAmount.bringToFront();
                    errorMessageForAmount.setError("");
                    errorMessageForAmount.setText("Please enter valid amount");
                }


            }


            if (TextUtils.isEmpty(remarks)) {

                remarks = "-";

            } else if (!helper.validateInputForSC(remarks)) {

                errorMessageForRemarks.setVisibility(View.VISIBLE);
                errorMessageForRemarks.bringToFront();
                errorMessageForRemarks.setError("");
                errorMessageForRemarks.setText("Amount cannot contains <,>,\",',%,(,),&,+,\\,~");


            }

            if (purposeSelected.equals("Purpose of Transfer")) {

                errorMessageForSpinner.setVisibility(View.VISIBLE);
                errorMessageForSpinner.bringToFront();
                errorMessageForSpinner.setError("");
                errorMessageForSpinner.setText("Please select purpose of transfer");
            }

            if ((TextUtils.isEmpty(sourceAccount) || sourceAccount.equals("N/A")) || (TextUtils.isEmpty(sourceBalance) || sourceBalance.equals("N/A")) || (TextUtils.isEmpty(sourceuserName) || sourceuserName.equals("N/A"))) {

                utils.showDilogForError("Please select Source account", "WARNING");

            }

            if ((TextUtils.isEmpty(destinationAccount) || destinationAccount.equals("N/A")) || (TextUtils.isEmpty(destinationName) || destinationName.equals("N/A"))) {

                showDilogForError("Please select Destination account", "WARNING");

            }
//            if ((TextUtils.isEmpty(DestinationProductType) || DestinationProductType.equals("N/A"))||SourceProductType.equals(null))
//            {
//                showDilogForError("Destination product type not found", "WARNING");
//            }
//            if ((TextUtils.isEmpty(SourceProductType) || SourceProductType.equals("N/A"))||SourceProductType.equals(null))
//            {
//                showDilogForError("Source product type not found", "WARNING");
//            }
//            if (SourceProductType.equals(destinationAccount)) {
//
//                showDilogForError("Source account and Destination account cannot be same.", "WARNING");
//            }

            if (sourceAccount.equals(destinationAccount)) {

                showDilogForError("Source account and Destination account cannot be same.", "WARNING");
            }

            if (!TextUtils.isEmpty(amount) && !amount.startsWith("0")) {

                amountToInt = Integer.valueOf(amount);

                if (!TextUtils.isEmpty(sourceBalance) && !sourceBalance.equals("N/A")) {

                    sourceBalance = sourceBalance.replaceAll(",", "");
                    bal = new BigDecimal(sourceBalance).intValue();
                    ;

                    if (amountToInt > bal) {

                        utils.showDilogForError("Amount cannot be greater than avaliable balance.", "WARNING");

                    }
                }
            }


            boolean sourceCurrencyCheck = false, sourceProductType = false, destinationProductType = false;
            if (!TextUtils.isEmpty(amount) && !amount.startsWith("0") && amountToInt <= bal && !TextUtils.isEmpty(remarks) && !purposeSelected.equals("Purpose of Transfer") && (!TextUtils.isEmpty(sourceAccount) && !sourceAccount.equals("N/A")) && (!TextUtils.isEmpty(sourceBalance) && !sourceBalance.equals("N/A")) && (!TextUtils.isEmpty(sourceuserName) && !sourceuserName.equals("N/A")) && (!TextUtils.isEmpty(destinationAccount) && !destinationAccount.equals("N/A")) && (!TextUtils.isEmpty(destinationName) && !destinationName.equals("N/A")) && !sourceAccount.equals(destinationAccount) && (!TextUtils.isEmpty(this.sourceProductType) || !this.sourceProductType.equals("N/A")) && (!TextUtils.isEmpty(this.destinationProductType) || !this.destinationProductType.equals("N/A"))) {
                if (this.destinationProductType.equals("FOREIGN CURRENCY VALUE AC-SAVING") || this.destinationProductType.equals("FOREIGN CURRENCY VALUE AC-CURRENT")) {
                    destinationProductType = true;
                    if (this.sourceProductType.equals("NONRESIDENT RUPEEVALUE SAVING(NRVA)") || this.sourceProductType.equals("NON RESIDENT RUPEE VALUE CUR (NRVA)")) {
                        sourceProductType = true;
                        if (sourceCurrency.equals("PKR")) {
                            sourceCurrencyCheck = true;
                            validateBtn.setEnabled(false);
                            Verification();
                        }

                    } else if (this.sourceProductType.equals("FOREIGN CURRENCY VALUE AC-SAVING") || this.sourceProductType.equals("FOREIGN CURRENCY VALUE AC-CURRENT")) {
                        sourceProductType = true;
                        sourceCurrencyCheck = true;
                        validateBtn.setEnabled(false);
                        Verification();
                    }
                }
                if (destinationProductType == false) {
                    utils.showDilogForError("Destination product type incorrect", "WARNING");
                    //destination tyoe error
                }
                else if (sourceCurrencyCheck == false && sourceProductType == false) {
                    utils.showDilogForError("Source product type and Source currency incorrect", "WARNING");

                    //currecny error
                } else if (sourceCurrencyCheck == false && sourceProductType == true) {
                    utils.showDilogForError("Source currency incorrect", "WARNING");

                    //currecny error
                } else if (sourceCurrencyCheck == true && sourceProductType == false) {
                    utils.showDilogForError("Source type incorrect", "WARNING");

                }



            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}