package com.ceesolutions.samba.transactions.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.transactions.Model.AccountDetails;
import com.ceesolutions.samba.transactions.Model.TransactionDetails;
import com.ceesolutions.samba.transactions.TransactionDetailsViewScreen;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by ceeayaz on 4/13/18.
 */

public class TransactionDetailsCustomListAdapter extends BaseAdapter {

    ArrayList<TransactionDetails> transactionDetails;
    Context context;
    TextView dailyTxt, todayTxt, remainTxt;
    TextView title, cashTxt, beforeTxt, afterTxt;
    ImageView icon;
    View view;
    ArrayList<String> arrayList;

    public TransactionDetailsCustomListAdapter(ArrayList<TransactionDetails> transactionDetailsArrayList, Context mContext) {

        transactionDetails = transactionDetailsArrayList;
        context = mContext;
        arrayList = new ArrayList<>();
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return transactionDetails.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.transaction_details_item, parent, false);
        }

        title = (TextView) convertView.findViewById(R.id.title);
        cashTxt = (TextView) convertView.findViewById(R.id.cashTxt);
        icon = (ImageView) convertView.findViewById(R.id.icon);
        view = (View) convertView.findViewById(R.id.view1);
        beforeTxt = (TextView) convertView.findViewById(R.id.beforeTxt);
        afterTxt = (TextView) convertView.findViewById(R.id.afterTxt);
        if (!TextUtils.isEmpty(transactionDetails.get(position).getDeposit())) {
            icon.setBackgroundResource(R.drawable.debit_icon);
            beforeTxt.setText(transactionDetails.get(position).getDeposit());
            beforeTxt.setTextColor(Color.parseColor("#0064b2"));

        } else {

            icon.setBackgroundResource(R.drawable.credit_icon);
            beforeTxt.setText("-" + transactionDetails.get(position).getWithDraw());
            beforeTxt.setTextColor(Color.parseColor("#d0021b"));
        }

        afterTxt.setText(transactionDetails.get(position).getAfterAmount());

//        if (arrayList.size() > 0) {
//
//            Set<String> set = new HashSet<String>(arrayList);
//
//            if (set.contains(transactionDetails.get(position).getDate())) {
//
//                title.setVisibility(View.VISIBLE);
//                title.setTextColor(Color.parseColor("#00ff001C"));
//                String[] separated = transactionDetails.get(position).getDate().split("\\-");
//                String txt = "<font COLOR=\'#FFFFFFFF\'><Big><b>" + separated[0] + "</Big></b><br></font>"
//                        + "<font COLOR=\'#FFFFFFFF\'><Medium>" + separated[1] + "</Medium></font>";
//                title.setText(Html.fromHtml(txt));
//
//                cashTxt.setText(transactionDetails.get(position).getTitle());
//
//            } else {
//
//                arrayList.add(transactionDetails.get(position).getDate());
//                String[] separated = transactionDetails.get(position).getDate().split("\\-");
//                String txt = "<font COLOR=\'#034ea2\'><Big><b>" + separated[0] + "</Big></b><br></font>"
//                        + "<font COLOR=\'#034ea2\'><Medium>" + separated[1] + "</Medium><br></font>";
//
//                title.setText(Html.fromHtml(txt));
//
//                cashTxt.setText(transactionDetails.get(position).getTitle());
//            }
//
//        } else {

        arrayList.add(transactionDetails.get(position).getValueDate());
        String[] separated = transactionDetails.get(position).getValueDate().split("\\-");
        String txt = "<font COLOR=\'#034ea2\'><Big><b>" + separated[0] + "</Big></b><br></font>"
                + "<font COLOR=\'#034ea2\'><Medium>" + separated[1] + "</Medium><br></font>"
                + "<font COLOR=\'#034ea2\'><Medium>" + separated[2] + "</Medium><br></font>";
        title.setText(Html.fromHtml(txt));

        if (transactionDetails.get(position).getTitle().contains("~")) {

            String[] des = transactionDetails.get(position).getTitle().split("~");
            cashTxt.setText(des[0]);

        } else
            cashTxt.setText(transactionDetails.get(position).getTitle());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(context, TransactionDetailsViewScreen.class);
                intent.putExtra("accountNumber", transactionDetails.get(position).getAccNumber());
                intent.putExtra("currency", transactionDetails.get(position).getCurrency());
                intent.putExtra("date", transactionDetails.get(position).getValueDate());
                intent.putExtra("Transaction_Reference", transactionDetails.get(position).getRequestNumber());
                intent.putExtra("deposit", transactionDetails.get(position).getDeposit());
                intent.putExtra("withdraw", transactionDetails.get(position).getWithDraw());
                intent.putExtra("description", transactionDetails.get(position).getTitle());
                ((Activity) context).overridePendingTransition(0, 0);
                ((Activity) context).startActivity(intent);

            }
        });

        return convertView;
    }

}