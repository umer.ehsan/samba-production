package com.ceesolutions.samba.transactions.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.transactions.Model.Account;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.RoundImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 3/6/18.
 */

public class CustomListAdapter extends BaseAdapter {
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    ArrayList<Account> userModels;
    LayoutInflater layoutInflater;
    View row;
    Context context;
    TextView bankName, userName, accountNumber, nonSambaBankName, productType;
    ImageView click, titleTxt;
    RoundImageView title;
    int[] androidColors;
    String type;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private AppPreferences appPreferences;
    private String location;

    public CustomListAdapter(ArrayList<Account> userModelArrayList, Context mContext, String source) {

        userModels = userModelArrayList;
        context = mContext;
        constants = Constants.getConstants(context);
        requestQueue = Volley.newRequestQueue(context);
        utils = new Utils(context);
        webService = new WebService();
        helper = Helper.getHelper(context);

        type = source;
        appPreferences = new AppPreferences(mContext);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return userModels.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.select_account_item_new, parent, false);
        }

        try {


            click = (ImageView) convertView.findViewById(R.id.click);
            titleTxt = (ImageView) convertView.findViewById(R.id.titleTxt);
            title = (RoundImageView) convertView.findViewById(R.id.title);
            titleTxt.bringToFront();
            bankName = (TextView) convertView.findViewById(R.id.bankName);
            accountNumber = (TextView) convertView.findViewById(R.id.accountNumber);
            userName = (TextView) convertView.findViewById(R.id.userName);
            nonSambaBankName = (TextView) convertView.findViewById(R.id.nonSambaBankName);
            productType = (TextView) convertView.findViewById(R.id.productType);

            click.setVisibility(View.GONE);
            CryptLib _crypt = new CryptLib();
            pd = new Dialog(context);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());


            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("") && userType.equals("NON_SAMBA")) {
                nonSambaBankName.setVisibility(View.VISIBLE);
                titleTxt.setVisibility(View.GONE);
                title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(userModels.get(position).getBankColor())));
                nonSambaBankName.setText(userModels.get(position).getBranchShortName());
                productType.setVisibility(View.GONE);

            } else {
                if (userModels.get(position).getAccountType().toUpperCase().contains("CURRENT")) {

                    title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#0064b2")));
//                    titleTxt.setImageResource(R.drawable.current);
                } else if (userModels.get(position).getAccountType().toUpperCase().contains("SAVING")) {
                    title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#0064b2")));
//                    titleTxt.setImageResource(R.drawable.savings);
                } else {

                    title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#0064b2")));
//                    titleTxt.setImageResource(R.drawable.current);
                }

                if (!TextUtils.isEmpty(userModels.get(position).getCurrencyType()) && !userModels.get(position).getCurrencyType().equals("null") && !userModels.get(position).getCurrencyType().equals(null)) {
                    nonSambaBankName.setVisibility(View.VISIBLE);
                    nonSambaBankName.setText(userModels.get(position).getCurrencyType());
                    titleTxt.setVisibility(View.GONE);
                    productType.setText(userModels.get(position).getProductType());
                } else {
                    nonSambaBankName.setVisibility(View.VISIBLE);
                    nonSambaBankName.setText(userModels.get(position).getCurrency());
                    titleTxt.setVisibility(View.GONE);
                    productType.setText(userModels.get(position).getProductType());
                }

            }


            bankName.setText(userModels.get(position).getBranchName());
            userName.setText(userModels.get(position).getUserName());
            accountNumber.setText(userModels.get(position).getAccountNumber());

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        CryptLib _crypt = new CryptLib();
                        if (type.equals("source")) {
                            LatestBalance(position);
//                        LatestBalance(userModels.get(position).getAccountNumber(),userModels.get(position).getUserName(),userModels.get(position).getCurrencyType(),userModels.get(position).getBankID(),userModels.get(position).getBranchCode());
                        } else {
                            constants.destinationAccountEditor.putString("isSelect", "Yes");
                            constants.destinationAccountEditor.putString("userName", _crypt.encryptForParams(userModels.get(position).getUserName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                            constants.destinationAccountEditor.putString("accountNumber", _crypt.encryptForParams(userModels.get(position).getAccountNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                            constants.destinationAccountEditor.putString("bankCode", _crypt.encryptForParams(userModels.get(position).getBankID(), constants.getDeviceMac(), constants.getDeviceIMEI()));

                            if (!TextUtils.isEmpty(userModels.get(position).getCurrencyType()) && !userModels.get(position).getCurrencyType().equals("null") && !userModels.get(position).getCurrencyType().equals(null))
                                constants.destinationAccountEditor.putString("currency", _crypt.encryptForParams(userModels.get(position).getCurrencyType(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                            else
                                constants.destinationAccountEditor.putString("currency", _crypt.encryptForParams("N/A", constants.getDeviceMac(), constants.getDeviceIMEI()));

                            if (!TextUtils.isEmpty(userModels.get(position).getAvailBal()) && !userModels.get(position).getAvailBal().equals("null") && !userModels.get(position).getAvailBal().equals(null))
                                constants.destinationAccountEditor.putString("balance", _crypt.encryptForParams(userModels.get(position).getAvailBal(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                            else
                                constants.destinationAccountEditor.putString("balance", _crypt.encryptForParams("N/A", constants.getDeviceMac(), constants.getDeviceIMEI()));

                            if (!TextUtils.isEmpty(userModels.get(position).getBranchCode()) && !userModels.get(position).getBranchCode().equals("null") && !userModels.get(position).getBranchCode().equals(null))
                                constants.destinationAccountEditor.putString("branchCode", _crypt.encryptForParams(userModels.get(position).getBranchCode(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                            else
                                constants.destinationAccountEditor.putString("branchCode", _crypt.encryptForParams("N/A", constants.getDeviceMac(), constants.getDeviceIMEI()));

                            constants.destinationAccountEditor.putString("DestProductType", _crypt.encryptForParams(userModels.get(position).getProductType(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                            constants.destinationAccountEditor.commit();
                            constants.destinationAccountEditor.apply();
                            ((Activity) context).finish();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public void LatestBalance(final int position1) {

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.LatestBalance(constants.sharedPreferences.getString("userName","N/A"), userModels.get(position).getAccountNumber(), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getLatestBalance();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//
//                                        String avaliableBal = jsonObject.getString("AvailBal");
//                                        constants.sourceAccountEditor.putString("userName", userModels.get(position).getUserName());
//                                        constants.sourceAccountEditor.putString("accountNumber", userModels.get(position).getAccountNumber());
//                                        constants.sourceAccountEditor.putString("balance", avaliableBal);
//                                        constants.sourceAccountEditor.putString("branchCode", userModels.get(position).getBranchCode());
//                                        constants.sourceAccountEditor.putString("bankCode", userModels.get(position).getBankID());
//                                        constants.sourceAccountEditor.putString("currency", userModels.get(position).getCurrencyType());
//                                        constants.sourceAccountEditor.commit();
//                                        constants.sourceAccountEditor.apply();
//                                        ((Activity) context).finish();
//
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                        }
//                    }
//                }, 7000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sT24_AccountNumber", userModels.get(position1).getAccountNumber());
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(context);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getLatestBalURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();

                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        try {


                                            if (jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code").equals("00")) {
//                                        JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code"));
//                                        JsonObj.put("AvailBal", jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"));

                                                CryptLib _crypt2 = new CryptLib();
                                                String avaliableBal = jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal");
                                                constants.sourceAccountEditor.putString("isSelect", "Yes");
                                                constants.sourceAccountEditor.putString("userName", _crypt2.encryptForParams(userModels.get(position1).getUserName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.sourceAccountEditor.putString("accountNumber", _crypt2.encryptForParams(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AccountID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.sourceAccountEditor.putString("balance", _crypt2.encryptForParams(avaliableBal, constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.sourceAccountEditor.putString("branchCode", _crypt2.encryptForParams(userModels.get(position1).getBranchCode(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.sourceAccountEditor.putString("bankCode", _crypt2.encryptForParams(userModels.get(position1).getBankID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.sourceAccountEditor.putString("currency", _crypt2.encryptForParams(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Currency"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.sourceAccountEditor.putString("SourceProductType", _crypt2.encryptForParams(userModels.get(position1).getProductType(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.sourceAccountEditor.commit();
                                                constants.sourceAccountEditor.apply();
                                                pd.dismiss();
                                                ((Activity) context).finish();
//
                                            } else if (jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Description"), "ERROR", context, new LoginScreen());


                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Description"), "ERROR");

                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sT24_AccountNumber", userModels.get(position).getAccountNumber());
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    public void LatestBalance(String accountNumber, final String userName, final String currency, final String bankId, final String branchCode) {

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.LatestBalance(constants.sharedPreferences.getString("userName","N/A"), userModels.get(position).getAccountNumber(), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getLatestBalance();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//
//                                        String avaliableBal = jsonObject.getString("AvailBal");
//                                        constants.sourceAccountEditor.putString("userName", userModels.get(position).getUserName());
//                                        constants.sourceAccountEditor.putString("accountNumber", userModels.get(position).getAccountNumber());
//                                        constants.sourceAccountEditor.putString("balance", avaliableBal);
//                                        constants.sourceAccountEditor.putString("branchCode", userModels.get(position).getBranchCode());
//                                        constants.sourceAccountEditor.putString("bankCode", userModels.get(position).getBankID());
//                                        constants.sourceAccountEditor.putString("currency", userModels.get(position).getCurrencyType());
//                                        constants.sourceAccountEditor.commit();
//                                        constants.sourceAccountEditor.apply();
//                                        ((Activity) context).finish();
//
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                        }
//                    }
//                }, 7000);
                CryptLib _crypt = new CryptLib();
                Log.d("account---", "---" + accountNumber);
                Log.d("name---", "---" + userName);
                Log.d("branch---", "---" + bankId);
                Log.d("bank---", "---" + currency);
                Log.d("cur--", "--" + branchCode);


                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sT24_AccountNumber", accountNumber);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(context);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getLatestBalURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();

                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        try {


                                            if (jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code").equals("00")) {
//                                        JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code"));
//                                        JsonObj.put("AvailBal", jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"));

                                                CryptLib _crypt2 = new CryptLib();
                                                String avaliableBal = jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal");
                                                constants.sourceAccountEditor.putString("isSelect", "Yes");
                                                constants.sourceAccountEditor.putString("userName", _crypt2.encryptForParams(userName, constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.sourceAccountEditor.putString("accountNumber", _crypt2.encryptForParams(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AccountID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.sourceAccountEditor.putString("balance", _crypt2.encryptForParams(avaliableBal, constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.sourceAccountEditor.putString("branchCode", _crypt2.encryptForParams(branchCode, constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.sourceAccountEditor.putString("bankCode", _crypt2.encryptForParams(bankId, constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.sourceAccountEditor.putString("currency", _crypt2.encryptForParams(currency, constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.sourceAccountEditor.commit();
                                                constants.sourceAccountEditor.apply();
                                                pd.dismiss();
                                                ((Activity) context).finish();
//
                                            } else if (jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Description"), "ERROR", context, new LoginScreen());


                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Description"), "ERROR");

                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sT24_AccountNumber", userModels.get(position).getAccountNumber());
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }


}