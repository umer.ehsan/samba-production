package com.ceesolutions.samba.accessControl;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ceesolutions.samba.R;

/**
 * Created by ceeayaz on 3/17/18.
 */

public class ForgotPasswordConfirmationScreen extends AppCompatActivity {

    private TextView doneBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_confirmation);
        initViews();
        getSupportActionBar().hide();
    }

    public void initViews() {

        doneBtn = (TextView) findViewById(R.id.doneBtn);
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ForgotPasswordConfirmationScreen.this, LoginScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Log.d("Do Nothing", "----");
    }
}
