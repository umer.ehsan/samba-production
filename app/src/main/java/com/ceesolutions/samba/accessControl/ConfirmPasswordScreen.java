package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ceeayaz on 4/23/18.
 */

public class ConfirmPasswordScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd, dialog;
    private TextView validateBtn, heading, doneBtn, textView, message;
    private TextView errorMessage;
    private String password, confirmPassword, oldPassword;
    private ImageView backBtn, notificationBtn;
    private TextView headingTxt;
    private Utils utils;
    private EditText editPassword, editConfrimPassword;
    private TextView errorMessageForPassword, errorMessageForConfirmPassword;
    private Pattern passwordPattern, passwordMatchpattern, userNameMatcherPattern;
    private Matcher passwordMatcher, userMatcher;
    private ImageView info, info1;
    private AppPreferences appPreferences;
    private String location;
    private List<Address> addresses;

    private static boolean checkForCapitalLetter(String str) {
        char ch;
        boolean capitalFlag = false;
        boolean lowerCaseFlag = false;
        boolean numberFlag = false;
        for (int i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isDigit(ch)) {
                numberFlag = true;
            } else if (Character.isUpperCase(ch)) {
                capitalFlag = true;
            } else if (Character.isLowerCase(ch)) {
                lowerCaseFlag = true;
            }
            if (numberFlag && capitalFlag && lowerCaseFlag)
                return true;
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_password);
        initViews();
        final Intent intent = getIntent();
        oldPassword = intent.getStringExtra("password");
        getSupportActionBar().hide();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ConfirmPasswordScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
//                finishAffinity();
            }
        });


        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FirebaseAnalytics.getInstance(ConfirmPasswordScreen.this).logEvent("New_Password_Update_Pressed",new Bundle());
                password = editPassword.getText().toString().trim();

                if (TextUtils.isEmpty(password)) {
                    errorMessageForPassword.setVisibility(View.VISIBLE);
                    errorMessageForPassword.bringToFront();
                    errorMessageForPassword.setError("");
                    errorMessageForPassword.setText("Please enter valid password");
                    clearPasswordViews();

                } else if (!helper.validateInputForSC(password) || password.contains(" ")) {

                    errorMessageForPassword.setVisibility(View.VISIBLE);
                    errorMessageForPassword.bringToFront();
                    errorMessageForPassword.setError("");
                    errorMessageForPassword.setText("Password cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                    clearPasswordViews();

                } else if (!checkForCapitalLetter(password)) {

                    errorMessageForPassword.setVisibility(View.VISIBLE);
                    errorMessageForPassword.bringToFront();
                    errorMessageForPassword.setError("");
                    errorMessageForPassword.setText("Please must contains a capital letter");
                    clearPasswordViews();

                }

                if (TextUtils.isEmpty(confirmPassword)) {
                    errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                    errorMessageForConfirmPassword.bringToFront();
                    errorMessageForConfirmPassword.setError("");
                    errorMessageForConfirmPassword.setText("Please enter valid password");
                    clearPasswordViews();

                } else if (!helper.validateInputForSC(confirmPassword) || confirmPassword.contains(" ")) {


                    errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                    errorMessageForConfirmPassword.bringToFront();
                    errorMessageForConfirmPassword.setError("");
                    errorMessageForConfirmPassword.setText("Confirm Password cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                    clearPasswordViews();
                }

                if (confirmPassword.equals(password)) {

                    if ((!TextUtils.isEmpty(password) && password.length() >= 10 && helper.validateInputForSC(password) && !password.contains(" ") && checkForCapitalLetter(password)) && (!TextUtils.isEmpty(confirmPassword) && confirmPassword.length() >= 10 && helper.validateInputForSC(confirmPassword) && !confirmPassword.contains(" "))) {

                        if (passwordMatchpattern.matcher(editPassword.getText().toString()).find()) {
                            clearPasswordViews();
                            errorMessageForPassword.setVisibility(View.VISIBLE);
                            errorMessageForPassword.bringToFront();
                            errorMessageForPassword.setError("");
                            errorMessageForPassword.setText("Password <,>,\",',%,(,),&,+,\\,~");
                            errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                            errorMessageForConfirmPassword.bringToFront();
                            errorMessageForConfirmPassword.setError("");
                            errorMessageForConfirmPassword.setText("Confirm Password <,>,\",',%,(,),&,+,\\,~");


                        } else {

                            passwordMatcher = passwordPattern.matcher(password);

                            if (passwordMatcher.matches()) {

                                editPassword.setText(editPassword.getText().toString());


                                if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                                    longitude = Double.valueOf(longitude1);
                                    latitude = Double.valueOf(latitude1);
                                    location = getAddress("ipin");

                                } else {

                                    longitude = 0;
                                    latitude = 0;
                                    location = "-";
                                    ChangePassword();
//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
                                }
                                validateBtn.setEnabled(false);


                                //Funcation Call
                            } else {
                                clearPasswordViews();
                                errorMessageForPassword.setVisibility(View.VISIBLE);
                                errorMessageForPassword.bringToFront();
                                errorMessageForPassword.setError("");
                                errorMessageForPassword.setText("Password policy does not match");
                                errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                                errorMessageForConfirmPassword.bringToFront();
                                errorMessageForConfirmPassword.setError("");
                                errorMessageForConfirmPassword.setText("Password policy does not match");
//                                clearPasswordViews();
                            }

                        }

                    } else {
                        clearPasswordViews();
                        errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                        errorMessageForConfirmPassword.bringToFront();
                        errorMessageForConfirmPassword.setError("");
                        errorMessageForConfirmPassword.setText("Please enter valid password");
                        errorMessageForPassword.setVisibility(View.VISIBLE);
                        errorMessageForPassword.bringToFront();
                        errorMessageForPassword.setError("");
                        errorMessageForPassword.setText("Please enter valid password");
//                        clearPasswordViews();
                    }


                } else {


                    if (TextUtils.isEmpty(password) || password.length() < 8) {
                        clearPasswordViews();
                        errorMessageForPassword.setVisibility(View.VISIBLE);
                        errorMessageForPassword.bringToFront();
                        errorMessageForPassword.setError("");
                        errorMessageForPassword.setText("Please enter valid password");
//                        clearPasswordViews();
                    }

                    if (TextUtils.isEmpty(confirmPassword) || confirmPassword.length() < 8) {
                        clearPasswordViews();
                        errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                        errorMessageForConfirmPassword.bringToFront();
                        errorMessageForConfirmPassword.setError("");
                        errorMessageForConfirmPassword.setText("Please enter valid password");
//                        clearPasswordViews();
                    }

                    if (!TextUtils.isEmpty(password) && password.length() >= 8 && !TextUtils.isEmpty(confirmPassword) && confirmPassword.length() >= 8) {
                        clearPasswordViews();
                        errorMessageForPassword.setVisibility(View.VISIBLE);
                        errorMessageForPassword.bringToFront();
                        errorMessageForPassword.setError("");
                        errorMessageForPassword.setText("Password/Confirm Password does not match");
                        errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                        errorMessageForConfirmPassword.bringToFront();
                        errorMessageForConfirmPassword.setError("");
                        errorMessageForConfirmPassword.setText("Password/Confirm Password does not match");
//                        clearPasswordViews();
                    }


                }

            }
        });

        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessageForPassword.setVisibility(View.GONE);
                errorMessageForPassword.setError(null);

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessageForPassword.setVisibility(View.GONE);
                errorMessageForPassword.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                password = editable.toString().trim();
                if (password.length() == 0) {


                    //Do Nothing
                } else {

                    if (passwordMatchpattern.matcher(password).find()) {

                        errorMessageForPassword.setVisibility(View.VISIBLE);
                        errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                        errorMessageForPassword.setText("Password Strength: Weak");


                    } else {

                        if (!checkForCapitalLetter(password)) {

                            errorMessageForPassword.setVisibility(View.VISIBLE);
                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                            errorMessageForPassword.setText("Password Strength: Weak");


                        } else {

                            if (password.length() < 16) {
                                passwordMatcher = passwordPattern.matcher(password);


                                if (password.length() > 7) {

                                    if (passwordMatcher.matches()) {


                                        if (password.length() > 10) {

                                            errorMessageForPassword.setVisibility(View.VISIBLE);
                                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                            errorMessageForPassword.setText("Password Strength: Strong");

                                        } else {

                                            errorMessageForPassword.setVisibility(View.VISIBLE);
                                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                            errorMessageForPassword.setText("Password Strength: Good");

                                        }


                                    } else {

                                        if (password.length() > 7 && password.length() < 10) {

                                            errorMessageForPassword.setVisibility(View.VISIBLE);
                                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                            errorMessageForPassword.setText("Password Strength: Good");

                                        } else {

                                            errorMessageForPassword.setVisibility(View.VISIBLE);
                                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                            errorMessageForPassword.setText("Password Strength: Bad");
                                        }

                                    }
                                } else {

                                    if (password.length() > 7 && password.length() < 10) {

                                        errorMessageForPassword.setVisibility(View.VISIBLE);
                                        errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                        errorMessageForPassword.setText("Password Strength: Good");

                                    } else {

                                        errorMessageForPassword.setVisibility(View.VISIBLE);
                                        errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                        errorMessageForPassword.setText("Password Strength: Bad");
                                    }
                                }

                            }
                        }

                    }
                }
            }
        });

        editPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {

                    errorMessageForConfirmPassword.setVisibility(View.GONE);
                    editConfrimPassword.getText().clear();

//                    Toast.makeText(UserConfirmationScreen.this, "Focus Lose", Toast.LENGTH_SHORT).show();
                } else {


//                    Toast.makeText(UserConfirmationScreen.this, "Get Focus", Toast.LENGTH_SHORT).show();
                }

            }
        });
        editConfrimPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForConfirmPassword.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForConfirmPassword.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                confirmPassword = editable.toString();
            }
        });

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showInfo();
            }
        });

        info1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showInfo();
            }
        });
    }

    public void initViews() {


        validateBtn = (TextView) findViewById(R.id.validateBtn);
        requestQueue = Volley.newRequestQueue(ConfirmPasswordScreen.this);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        editPassword = (EditText) findViewById(R.id.editPassword);
        editConfrimPassword = (EditText) findViewById(R.id.editConfirmPassword);
        utils = new Utils(ConfirmPasswordScreen.this);
        helper = Helper.getHelper(ConfirmPasswordScreen.this);
        constants = Constants.getConstants(ConfirmPasswordScreen.this);
        pd = new Dialog(ConfirmPasswordScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        errorMessageForPassword = (TextView) findViewById(R.id.errorMessageForPassword);
        errorMessageForConfirmPassword = (TextView) findViewById(R.id.errorMessageForConfirmPassword);
        info = (ImageView) findViewById(R.id.info);
        info1 = (ImageView) findViewById(R.id.info1);
        passwordPattern = Pattern.compile(constants.PASSWORD_PATTERN);
        passwordMatchpattern = Pattern.compile(constants.EXCLUDE_PASSWORD_PATTERN);
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");

    }

    public void clearPasswordViews() {


        editPassword.getText().clear();

        editPassword.requestFocus();
    }

    public void showInfo() {

        dialog = new Dialog(ConfirmPasswordScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.password_policy);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        heading.setText("PASSWORD POLICY");
        message.setText(Constants.PASSWORDPOLICY);
        heading.setBackgroundColor(getResources().getColor(R.color.grey));
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }


    public void ChangePassword() {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5Password = helper.md5(user.toUpperCase() + password);
                final String md5OldPassword = helper.md5(user.toUpperCase() + oldPassword);
//                final String md5Answer = helper.md5(userName.toUpperCase() + questionCode + questionAnswer.toUpperCase());
//                webService.AddSecretQuestionAnswer(userName, questionCode, md5Answer, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getSetQuestionAnswerReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(SecurityQuestionScreen.this, "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                showDilogForError("Question cannot be added. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
//                                editQuestionAnswer.setText("");
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(SecurityQuestionScreen.this, "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        ProfileSetupScreen.pager.setCurrentItem(3);
//                                        ProfileSetupScreen.currentItem++;
//                                        headingTxt.setText("DONE");
//                                        validateBtn.setEnabled(true);
//                                        editQuestionAnswer.setText("");
//                                    } else {
////                                        Toast.makeText(SecurityQuestionScreen.this, "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        showDilogForError("Question cannot be added. Please try again later.", "ERROR");
//                                        validateBtn.setEnabled(true);
//                                        editQuestionAnswer.setText("");
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    showDilogForError("Question cannot be added. Please try again later.", "ERROR");
//                                    validateBtn.setEnabled(true);
//                                    editQuestionAnswer.setText("");
//                                }
//                            }
//                        } else {
////                            Toast.makeText(SecurityQuestionScreen.this, "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            showDilogForError("Question cannot be added. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//                            editQuestionAnswer.setText("");
//
//                        }
//                    }
//                }, 3000);

                final String md51 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sNewPassword", md5Password);
                params.put("sOldPassword", md5OldPassword);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sLegalIdentityType", _crypt.decrypt(constants.sharedPreferences.getString("LegalType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sServerSessionKey", md51);
                HttpsTrustManager.allowMySSL(ConfirmPasswordScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.changePasswordURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md51)) {
                                        if (jsonObject.getJSONObject("Signs_UserChangePasswordResult").getString("Status_Code").equals("00")) {

                                            pd.dismiss();
                                            appPreferences.putString("location", location);
                                            showDilogForError("Your password has been changed successfully.", "SUCCESS");
//                                        Intent intent = new Intent(ConfirmPasswordScreen.this, HomeScreen.class);
//                                        overridePendingTransition(0, 0);
//                                        startActivity(intent);
//                                        finish();
//                                        finishAffinity();
//                                        validateBtn.setEnabled(true);
//                                        clearPasswordViews();

                                        } else if (jsonObject.getJSONObject("Signs_UserChangePasswordResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_UserChangePasswordResult").getString("Status_Description"), "ERROR", ConfirmPasswordScreen.this, new LoginScreen());
                                            validateBtn.setEnabled(true);
                                            clearPasswordViews();

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_UserChangePasswordResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                            clearPasswordViews();
                                        }
                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                        clearPasswordViews();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    clearPasswordViews();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {

                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    clearPasswordViews();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    clearPasswordViews();
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sNewPassword", md5Password);
//                        params.put("sOldPassword", md5OldPassword);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLegalIdentityType", constants.sharedPreferences.getString("LegalType", "N/A"));
//                        params.put("sServerSessionKey", md51);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(SecurityQuestionScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
                clearPasswordViews();

            }
        } catch (Exception e) {

//            Toast.makeText(SecurityQuestionScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
            clearPasswordViews();
        }
    }

    public void GenerateiPIN() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.RegeneratePin(constants.sharedPreferences.getString("userName", "N/A"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getRegeneratePinReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//                                pd.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "Invalid Username/iPIN!", Toast.LENGTH_LONG).show();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
//
//
//                            } else {
//                                
//                                pd.dismiss();
//                                Intent intent = new Intent(ConfirmPasscodeScreen.this, PinVerificationScreen.class);
//                                intent.putExtra("passcode", passcode);
//                                intent.putExtra("change", "passcode");
//                                constants.passCode = "";
//                                   clearPasswordViews();
//                                validateBtn.setEnabled(true);
//                                overridePendingTransition(0, 0);
//                                startActivity(intent);
//
//
//                            }
//                        } else {
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 10000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md51 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                HttpsTrustManager.allowMySSL(ConfirmPasswordScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("00")) {
                                        pd.dismiss();
                                        Intent intent1 = new Intent(ConfirmPasswordScreen.this, PinVerificationScreen.class);
                                        intent1.putExtra("change", "password");
                                        intent1.putExtra("currentPassword", password);
                                        intent1.putExtra("oldPassword", oldPassword);
                                        overridePendingTransition(0, 0);
                                        validateBtn.setEnabled(true);
                                        clearPasswordViews();
                                        overridePendingTransition(0, 0);
                                        startActivity(intent1);
                                    } else if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("54")) {
                                        pd.dismiss();
                                        utils.showDilogForLogout(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR", ConfirmPasswordScreen.this, new LoginScreen());
                                        validateBtn.setEnabled(true);
                                        clearPasswordViews();

                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForError(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR");
                                        validateBtn.setEnabled(true);
                                        clearPasswordViews();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    clearPasswordViews();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    clearPasswordViews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    clearPasswordViews();
                                }
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("sService_Username", constants.signsUserName);
                        params.put("sService_Password", constants.signsPassword);
                        params.put("sIPAddress", Utils.getIPAddress(true));
                        params.put("sMacAddress", constants.address);
                        params.put("sOS_Version", constants.version);
                        params.put("sDeviceName", constants.model);
                        params.put("sAppVersion", constants.appVersion);
                        params.put("sService_SharedKey", Constants.Shared_KEY);
                        params.put("sTimeStamp", constants.dateTime);
                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
                        params.put("sServerSessionKey", md51);

                        return params;
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
                clearPasswordViews();

            }
        } catch (Exception e) {
            pd.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            validateBtn.setEnabled(true);
            clearPasswordViews();

        }
    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(ConfirmPasswordScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
//        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                Intent intent = new Intent(ConfirmPasswordScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
                validateBtn.setEnabled(true);
                clearPasswordViews();
                finish();
                finishAffinity();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public Address getAddress(final double latitude, final double longitude, final String value) {
        final Geocoder geocoder;
        geocoder = new Geocoder(this, Locale.getDefault());

//        try {
//            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//            return addresses.get(0);
//
//        } catch (IOException e) {
//            e.printStackTrace();
//            if (value.equals("ipin")) {
//                location = "-";
//                ChangePassword();
//            }
//        }
//
//        return null;
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {


                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    if (addresses != null) {
                        if (!TextUtils.isEmpty(addresses.get(0).toString()) && !addresses.get(0).equals("null") && !addresses.get(0).equals(null)) {


                            Address locationAddress = addresses.get(0);
                            location = locationAddress.getAddressLine(0);

                            if (value.equals("ipin"))
                                ChangePassword();


                        } else {
//                                utils.showDilogForError("Please turn on location services.", "WARNING");
//                                loginBtn.setEnabled(true);
                            location = "-";
                            if (value.equals("ipin"))
                                ChangePassword();
                        }
                    } else {

                        location = "-";
                        if (value.equals("ipin"))
                            ChangePassword();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    location = "-";
                    if (value.equals("ipin"))
                        ChangePassword();
                }


            }
        });

        if (addresses != null) {
            return addresses.get(0);
        } else
            return null;

    }

    public String getAddress(String login) {

        Address locationAddress = getAddress(latitude, longitude, login);
        String address = "";
        if (locationAddress != null) {
            address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();

            String currentLocation;

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address;

                if (!TextUtils.isEmpty(address1))
                    currentLocation += "\n" + address1;

                if (!TextUtils.isEmpty(city)) {
                    currentLocation += "\n" + city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += " - " + postalCode;
                } else {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += "\n" + postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation += "\n" + state;

                if (!TextUtils.isEmpty(country))
                    currentLocation += "\n" + country;


            }

        }

        return address;

    }
}
