package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.PinView;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by ceeayaz on 4/18/18.
 */

public class PasscodeScreen extends AppCompatActivity {

    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private TextView validateBtn;
    private TextView errorMessage;
    private String passcode = "";
    private ImageView backBtn,notificationBtn;
    private TextView headingTxt;
    private Utils utils;
    private PinView pinView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passcode);
        initViews();
        getSupportActionBar().hide();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(PasscodeScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
//                finishAffinity();
            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FirebaseAnalytics.getInstance(PasscodeScreen.this).logEvent("New_Passcode_Entered", new Bundle());
                passcode = pinView.getText().toString();

                if (!passcode.isEmpty()) {

                    if (!helper.validateInputForSC(passcode) || passcode.contains(" ")) {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.setText("Passcode cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                        errorMessage.setError("");

                    } else {
                        if (passcode.length() < 1 || passcode.length() < 6) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.setText("Please Enter 6 digits passcode");
                            errorMessage.setError("");
                        } else {

                            constants.passCode = passcode;
                            Intent intent = new Intent(PasscodeScreen.this,ConfirmPasscodeScreen.class);
                            overridePendingTransition(0,0);
                            startActivity(intent);
                            pinView.setText("");
                            finish();
                        }
                    }
                } else {

                    errorMessage.setVisibility(View.VISIBLE);
                    errorMessage.setText("Please Enter 6 digits passcode");
                    errorMessage.setError("");
                }
            }
        });
    }


    public void initViews() {


        validateBtn = (TextView) findViewById(R.id.validateBtn);
        errorMessage = (TextView) findViewById(R.id.errorMessage);
        pinView = (PinView) findViewById(R.id.pinView);
        requestQueue = Volley.newRequestQueue(PasscodeScreen.this);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        utils = new Utils(PasscodeScreen.this);
        helper = Helper.getHelper(PasscodeScreen.this);
        constants = Constants.getConstants(PasscodeScreen.this);
        pd = new Dialog(PasscodeScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        pinView.setTextColor(
                ResourcesCompat.getColor(getResources(), R.color.colorAccent, PasscodeScreen.this.getTheme()));
        pinView.setTextColor(
                ResourcesCompat.getColorStateList(getResources(), R.color.colorPrimary, PasscodeScreen.this.getTheme()));
        pinView.setLineColor(
                ResourcesCompat.getColor(getResources(), R.color.colorPrimary, PasscodeScreen.this.getTheme()));
        pinView.setLineColor(
                ResourcesCompat.getColorStateList(getResources(), R.color.line_colors, PasscodeScreen.this.getTheme()));
        pinView.setItemCount(6);
        pinView.setAnimationEnable(true);// start animation when adding text
        pinView.setCursorVisible(false);
        pinView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        pinView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() == 6) {

                    passcode = s.toString();
                    InputMethodManager imm = (InputMethodManager) PasscodeScreen.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(pinView.getWindowToken(), 0);

                } else {

                    passcode = s.toString();
                    // Do Nothing
                }
            }
        });

    }

}

