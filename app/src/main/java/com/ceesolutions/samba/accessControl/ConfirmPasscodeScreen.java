package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.PinView;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ceeayaz on 4/18/18.
 */

public class ConfirmPasscodeScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd, dialog1;
    private TextView validateBtn, validateBtn1;
    private TextView errorMessage, text2, heading, doneBtn, textView, text;
    private String token, target, userName, passcode = "", legalID;
    private ImageView backBtn, notificationBtn;
    private TextView headingTxt;
    private Utils utils;
    private PinView pinView;
    private AppPreferences appPreferences;
    private String location;
    private List<Address> addresses;
    private String securityCheck;
    private Intent intent1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passcode);
        initViews();
        getSupportActionBar().hide();

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (!TextUtils.isEmpty(securityCheck) && securityCheck.equals("passcode")) {

        } else {
            Intent intent = new Intent(ConfirmPasscodeScreen.this, PasscodeScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);
            finish();
        }
    }

    public void initViews() {

        try {

            intent1 = getIntent();
            securityCheck = intent1.getStringExtra("securityCheck");
            CryptLib _crypt = new CryptLib();
            text2 = (TextView) findViewById(R.id.text2);
            headingTxt = (TextView) findViewById(R.id.headingTxt);
            text = (TextView) findViewById(R.id.text);
            text2.setText("Confirm Passcode");
            validateBtn = (TextView) findViewById(R.id.validateBtn1);
            validateBtn1 = (TextView) findViewById(R.id.validateBtn);
            validateBtn1.setVisibility(View.GONE);
            validateBtn.setVisibility(View.VISIBLE);
            errorMessage = (TextView) findViewById(R.id.errorMessage);
            pinView = (PinView) findViewById(R.id.pinView);
            requestQueue = Volley.newRequestQueue(ConfirmPasscodeScreen.this);
            utils = new Utils(ConfirmPasscodeScreen.this);
            helper = Helper.getHelper(ConfirmPasscodeScreen.this);
            backBtn = (ImageView) findViewById(R.id.backBtn);
            notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
            constants = Constants.getConstants(ConfirmPasscodeScreen.this);
            token = constants.sharedPreferences.getString("fcmToken", "fcmToken");
            target = _crypt.decrypt(constants.sharedPreferences.getString("T24_Target", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            userName = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            legalID = _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            pd = new Dialog(ConfirmPasscodeScreen.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            appPreferences = new AppPreferences(ConfirmPasscodeScreen.this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }
            pinView.setEnabled(true);
            pinView.setTextColor(
                    ResourcesCompat.getColor(getResources(), R.color.colorAccent, ConfirmPasscodeScreen.this.getTheme()));
            pinView.setTextColor(
                    ResourcesCompat.getColorStateList(getResources(), R.color.colorPrimary, ConfirmPasscodeScreen.this.getTheme()));
            pinView.setLineColor(
                    ResourcesCompat.getColor(getResources(), R.color.colorPrimary, ConfirmPasscodeScreen.this.getTheme()));
            pinView.setLineColor(
                    ResourcesCompat.getColorStateList(getResources(), R.color.line_colors, ConfirmPasscodeScreen.this.getTheme()));
            pinView.setItemCount(6);
            pinView.setAnimationEnable(true);// start animation when adding text
            pinView.setCursorVisible(false);

            if (!TextUtils.isEmpty(securityCheck) && securityCheck.equals("passcode")) {
                backBtn.setVisibility(View.GONE);
                notificationBtn.setVisibility(View.INVISIBLE);
                headingTxt.setText("Validate Identity");
                validateBtn.setText("Validate");
                text.setText("Passcode");
                text2.setText("For security reasons, additional authentication is required to access your account.\n\nPlease enter your 6 digits passcode ");
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                        WindowManager.LayoutParams.FLAG_SECURE);
            }
            pinView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    errorMessage.setVisibility(View.GONE);
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    errorMessage.setVisibility(View.GONE);
                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (s.length() == 6) {

                        passcode = s.toString();
                        InputMethodManager imm = (InputMethodManager) ConfirmPasscodeScreen.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(pinView.getWindowToken(), 0);

                    } else {

                        passcode = s.toString();
                        // Do Nothing
                    }
                }
            });

            pinView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pinView.getText().clear();
//                pinView.setEnabled(false);
                }
            });

            validateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    FirebaseAnalytics.getInstance(ConfirmPasscodeScreen.this).logEvent("Confirm_New_Passcode_Pressed",new Bundle());
                    passcode = pinView.getText().toString();

                    if (!TextUtils.isEmpty(securityCheck) && securityCheck.equals("passcode")) {

                        if (!passcode.isEmpty()) {

                            if (!helper.validateInputForSC(passcode) || passcode.contains(" ")) {

                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.setText("Passcode cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                                errorMessage.setError("");
//                        pinView.setEnabled(true);

                            } else {
                                if (passcode.length() < 1 || passcode.length() < 6) {

                                    errorMessage.setVisibility(View.VISIBLE);
                                    errorMessage.setText("Please Enter 6 digits passcode");
                                    errorMessage.setError("");
//                            pinView.setEnabled(true);


                                } else {

                                    SubmitPasscode();
                                }

                            }
                        } else {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.setText("Please Enter 6 digits passcode");
                            errorMessage.setError("");

                        }
                    } else {

                        if (!passcode.isEmpty()) {

                            if (!helper.validateInputForSC(passcode) || passcode.contains(" ")) {

                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.setText("Passcode cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                                errorMessage.setError("");
//                        pinView.setEnabled(true);

                            } else {
                                if (passcode.length() < 1 || passcode.length() < 6) {

                                    errorMessage.setVisibility(View.VISIBLE);
                                    errorMessage.setText("Please Enter 6 digits passcode");
                                    errorMessage.setError("");
//                            pinView.setEnabled(true);


                                } else if (passcode.equals(constants.passCode)) {

                                    GenerateiPIN();
                                    validateBtn.setEnabled(false);

                                } else {

                                    errorMessage.setVisibility(View.VISIBLE);
                                    errorMessage.setText("Passcode and Confirm passcode does not match");
                                    errorMessage.setError("");
//                            pinView.setEnabled(true);

                                }
                            }
                        } else {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.setText("Please Enter 6 digits passcode");
                            errorMessage.setError("");

                        }
                    }
                }
            });


            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(ConfirmPasscodeScreen.this, PasscodeScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                    finish();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetPassCode() {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                final String md5 = Helper.md5(userName.toUpperCase() + passcode);
//                webService.SetDevicePasscode(userName, legalID, md5, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getSetPasscodeReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(ConfirmPasscodeScreen.this, "Information is not valid!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                pinView.setText("");
//                                validateBtn.setEnabled(true);
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
//
////                                        Toast.makeText(ConfirmPasscodeScreen.this, "Passcode has been set Successfully !", Toast.LENGTH_LONG).show();
//
//                                        pd.dismiss();
//                                        ProfileSetupScreen.pager.setCurrentItem(2);
//                                        ProfileSetupScreen.currentItem++;
//                                        headingTxt.setVisibility(View.VISIBLE);
//                                        backBtn.setVisibility(View.INVISIBLE);
//                                        pinView.setText("");
//                                        validateBtn.setEnabled(true);
//
//                                    } else {
////                                        Toast.makeText(ConfirmPasscodeScreen.this, "Passcode can not be set. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                        pinView.setText("");
//                                        validateBtn.setEnabled(true);
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                    pinView.setText("");
//                                    validateBtn.setEnabled(true);
//                                }
//                            }
//                        } else {
////                            Toast.makeText(ConfirmPasscodeScreen.this, "Device cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                            pinView.setText("");
//                            validateBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 3000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md51 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                HttpsTrustManager.allowMySSL(ConfirmPasscodeScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.setDevicePasscodeURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    try {
                                        CryptLib _crypt = new CryptLib();
                                        if (jsonObject.getJSONObject("Signs_SetDevicePasscodeResult").getString("Status_Code").equals("00")) {

                                            pd.dismiss();
                                            showDilogForError("Your have successfully set your Passcode.", "SUCCESS");
                                            constants.editor.putString("UserProgress", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_SetDevicePasscodeResult").getString("UserProgress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                            constants.editor.commit();
                                            constants.editor.apply();
                                            constants.passCode = "";
                                            pinView.setText("");

                                        } else if (jsonObject.getJSONObject("Signs_SetDevicePasscodeResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_SetDevicePasscodeResult").getString("Status_Description"), "ERROR", ConfirmPasscodeScreen.this, new LoginScreen());
                                            pinView.setText("");

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_SetDevicePasscodeResult").getString("Status_Description"), "ERROR");
                                            pinView.setText("");
                                            validateBtn.setEnabled(true);
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    pinView.setText("");
                                    validateBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    pinView.setText("");
                                    validateBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    pinView.setText("");
                                    validateBtn.setEnabled(true);
                                }
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("sService_Username", constants.signsUserName);
                        params.put("sService_Password", constants.signsPassword);
                        params.put("sIPAddress", Utils.getIPAddress(true));
                        params.put("sMacAddress", constants.address);
                        params.put("sSigns_Username", userName);
                        params.put("sOS_Version", constants.version);
                        params.put("sDeviceName", constants.model);
                        params.put("sAppVersion", constants.appVersion);
                        params.put("sDeviceLatitude", "0");
                        params.put("sDeviceLongitude", "0");
                        params.put("sService_SharedKey", Constants.Shared_KEY);
                        params.put("sTimeStamp", constants.dateTime);
                        params.put("sLegalIdentityValue", legalID);
                        params.put("sPassCodeValue", md5);
                        params.put("sServerSessionKey", md51);
                        return params;
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(ConfirmPasscodeScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");

                pinView.setText("");
                validateBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(ConfirmPasscodeScreen.this, "Please check your internet connection", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
            pinView.setText("");
            validateBtn.setEnabled(true);

        }
    }


    public void showDilogForError(String msg, String header) {

        dialog1 = new Dialog(ConfirmPasscodeScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog1.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog1.findViewById(R.id.doneBtn);
        heading = (TextView) dialog1.findViewById(R.id.heading);
//        dialog.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
                finish();
                validateBtn.setEnabled(true);
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog1.show();

    }

    public void GenerateiPIN() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.RegeneratePin(constants.sharedPreferences.getString("userName", "N/A"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getRegeneratePinReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//                                pd.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "Invalid Username/iPIN!", Toast.LENGTH_LONG).show();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
//
//
//                            } else {
//
//                                pd.dismiss();
//                                Intent intent = new Intent(ConfirmPasscodeScreen.this, PinVerificationScreen.class);
//                                intent.putExtra("passcode", passcode);
//                                intent.putExtra("change", "passcode");
//                                constants.passCode = "";
//                                pinView.setText("");
//                                validateBtn.setEnabled(true);
//                                overridePendingTransition(0, 0);
//                                startActivity(intent);
//
//
//                            }
//                        } else {
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 10000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md51 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Username", userName);
                params.put("sServerSessionKey", md51);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                HttpsTrustManager.allowMySSL(ConfirmPasscodeScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md51)) {
                                        if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("00")) {
                                            pd.dismiss();
                                            Intent intent = new Intent(ConfirmPasscodeScreen.this, PinVerificationScreen.class);
                                            intent.putExtra("passcode", passcode);
                                            intent.putExtra("change", "passcode");
                                            constants.passCode = "";
                                            pinView.setText("");
                                            validateBtn.setEnabled(true);
                                            overridePendingTransition(0, 0);
                                            startActivity(intent);
                                            finish();
                                        } else if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR", ConfirmPasscodeScreen.this, new LoginScreen());
                                            pinView.setText("");
                                            validateBtn.setEnabled(true);

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                            pinView.setText("");
                                        }

                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                        pinView.setText("");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    pinView.setText("");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    pinView.setText("");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    pinView.setText("");
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", userName);
//                        params.put("sServerSessionKey", md51);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
                pinView.setText("");

            }
        } catch (Exception e) {
            pd.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            validateBtn.setEnabled(true);
            pinView.setText("");

        }
    }

    public void SubmitPasscode() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                final String md5 = Helper.md5(userName.toUpperCase() + passcode);
                pd.show();
//                webService.RegeneratePin(constants.sharedPreferences.getString("userName", "N/A"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getRegeneratePinReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//                                pd.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "Invalid Username/iPIN!", Toast.LENGTH_LONG).show();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
//
//
//                            } else {
//
//                                pd.dismiss();
//                                Intent intent = new Intent(ConfirmPasscodeScreen.this, PinVerificationScreen.class);
//                                intent.putExtra("passcode", passcode);
//                                intent.putExtra("change", "passcode");
//                                constants.passCode = "";
//                                pinView.setText("");
//                                validateBtn.setEnabled(true);
//                                overridePendingTransition(0, 0);
//                                startActivity(intent);
//
//
//                            }
//                        } else {
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 10000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md51 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Username", userName);
                params.put("sServerSessionKey", md51);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sPassCodeValue", md5);
                params.put("sLegalIdentityValue", legalID);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                HttpsTrustManager.allowMySSL(ConfirmPasscodeScreen.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.passCodevalidateURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md51)) {
                                        if (jsonObject.getJSONObject("Signs_CompareDevicePasscodeResult").getString("Status_Code").equals("00")) {
                                            pd.dismiss();

                                            Intent intent;
                                            String isRooted = intent1.getStringExtra("isRooted");
                                            String isNotification = intent1.getStringExtra("isNotification");
                                            String isFirst = intent1.getStringExtra("isFirstLogin");
                                            String isFirstLogin = intent1.getStringExtra("firstLogin");

                                            if(!TextUtils.isEmpty(isFirstLogin) && isFirstLogin.equals("First")){

                                                if (!TextUtils.isEmpty(isRooted) && isRooted.toUpperCase().equals("N"))
                                                    intent = new Intent(ConfirmPasscodeScreen.this, TermsNConditionsScreen.class);
                                                else if (!TextUtils.isEmpty(isNotification) && isNotification.toUpperCase().equals("TRUE")) {

                                                    intent = new Intent(ConfirmPasscodeScreen.this, ProfileSetupScreen.class);
                                                    intent.putExtra("isFirst", "Yes");

                                                } else {
                                                    intent = new Intent(ConfirmPasscodeScreen.this, NotificationScreen.class);

                                                }

                                                intent.putExtra("username", intent1.getStringExtra("username"));
                                                intent.putExtra("login", "login");
                                                intent.putExtra("isFirstLogin", intent1.getStringExtra("isFirstLogin"));
                                                overridePendingTransition(0, 0);
                                                startActivity(intent);
                                                finish();
                                                finishAffinity();

                                            }else if(!TextUtils.isEmpty(isFirstLogin) && isFirstLogin.equals("No")){

                                                if (!TextUtils.isEmpty(isRooted) && isRooted.toUpperCase().equals("N"))
                                                    intent = new Intent(ConfirmPasscodeScreen.this, TermsNConditionsScreen.class);
                                                else
                                                    intent = new Intent(ConfirmPasscodeScreen.this, NotificationScreen.class);

                                                intent.putExtra("username",  intent1.getStringExtra("username"));
                                                intent.putExtra("login", "login");
                                                intent.putExtra("isFirstLogin", intent1.getStringExtra("isFirstLogin"));
                                                overridePendingTransition(0, 0);
                                                startActivity(intent);
                                                finish();
                                                finishAffinity();

                                            }else if(!TextUtils.isEmpty(isFirstLogin) && isFirstLogin.equals("Never")){

                                                intent = new Intent(ConfirmPasscodeScreen.this, HomeScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                startActivity(intent);
                                                finish();
                                                finishAffinity();

                                            }

                                            constants.passCode = "";
                                            pinView.setText("");
                                            validateBtn.setEnabled(true);

                                        } else if (jsonObject.getJSONObject("Signs_CompareDevicePasscodeResult").getString("Status_Code").equals("16")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_CompareDevicePasscodeResult").getString("Status_Description"), "ERROR", ConfirmPasscodeScreen.this, new LoginScreen());
                                            pinView.setText("");
                                            validateBtn.setEnabled(true);

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_CompareDevicePasscodeResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                            pinView.setText("");
                                        }

                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                        pinView.setText("");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    pinView.setText("");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    pinView.setText("");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    pinView.setText("");
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", userName);
//                        params.put("sServerSessionKey", md51);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
                pinView.setText("");

            }
        } catch (Exception e) {
            pd.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            validateBtn.setEnabled(true);
            pinView.setText("");

        }
    }
}

