package com.ceesolutions.samba.accessControl;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.Utils;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.scottyab.rootbeer.RootBeer;
import com.techworks.eatmubaraklibrary.EatMubarakWidget;
import com.techworks.eatmubaraklibrary.UserInfo;

import java.util.ArrayList;

/**
 * Created by ceeayaz on 3/19/18.
 */

public class SplashScreen extends AppCompatActivity implements OnMapReadyCallback {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private Utils utils;
    private Dialog dialog;
    private TextView textView, doneBtn, heading;
    private ImageView logo, sambaLogo;
    private RequestQueue requestQueue;
    private Helper helper;
    private String requestBody;
    private AppPreferences appPreferences;
    private Constants constants;
    private Dialog pd;
    private ArrayList<String> permissions1;
    private GoogleMap mMap;
    private int intentCode = 101;

    BroadcastReceiver analyticsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBundleExtra("EVENT_LOGIN") != null) {
                String userName = intent.getBundleExtra("EVENT_LOGIN")
                        .getString("UserName");
                String userPhone = intent.getBundleExtra("EVENT_LOGIN")
                        .getString("UserPhone");
                String timeStamp = intent.getBundleExtra("EVENT_LOGIN")
                        .getString("TimeStamp");
                //Add your code...
            } else if (intent.getBundleExtra("EVENT_BEGINCHECKOUT") != null) {
                String restName = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("RestName");
                String branchAddress = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("BranchAddress");
                String userName = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("UserName");
                String userPhone = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("UserPhone");
                String subtotal = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("Subtotal");
                String timeStamp = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("TimeStamp");
                //Add your code...
            }
            else if (intent.getBundleExtra("EVENT_RESTAURANTVIEWED") != null) {
                String restName = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("RestName");
                String branchAddress = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("BranchAddress");
                String userName = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("UserName");
                String userPhone = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("UserPhone");
                String timeStamp = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("TimeStamp");
                //Add your code...
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        getSupportActionBar().hide();
        utils = new Utils();
        permissions1 = new ArrayList<>();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);if (mapFragment != null) {
            mapFragment.getMapAsync(this);

        }
//        if (checkAndRequestPermissions()) {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                RootBeer rootBeer = new RootBeer(SplashScreen.this);
                if (rootBeer.isRooted()) {
//
////                    if (rootBeer.checkForBusyBoxBinary()) {
////                        Intent intent = new Intent(SplashScreen.this, LoginScreen.class);
////                        overridePendingTransition(0, 0);
////                        intent.putExtra("splash", "splash");
////                        startActivity(intent);
////                        finish();
////                    } else  {
                    showDilogForError("This application is not allowed on rooted device.", "WARNING");
////                    }
////
//                } else {
////
//                    CheckVersion();
//                    if (RootUtils.isDeviceRooted()) {
//
//                        showDilogForError("This application is not allowed on rooted device.", "WARNING");
//
//                    } else {
//
//                        if (Constants.isRootAvailable()) {
//
//                            showDilogForError("This application is not allowed on rooted device.", "WARNING");
//
//                        } else {
//
//                            if (Constants.isRooted()) {
//
//                                showDilogForError("This application is not allowed on rooted device.", "WARNING");
//
//                            } else {
//
//                                if (Constants.isRootAvailableForRoot()) {
//
//                                    showDilogForError("This application is not allowed on rooted device.", "WARNING");
                } else {

//                    registerEatMubarakReceiver();
//
//                    UserInfo userInfo = new UserInfo();
//                    userInfo.setUserContact("+923330000000"); // User's Phone Number
//                    userInfo.setUserEmail("devs@gmail.com"); // User's Email Address
//                    userInfo.setUserName("Devs Mikaelson"); // User's Name
//                    Intent intent = EatMubarakWidget.Builder.getBuilder()
//                            .setUserInfo(userInfo) // User Information Object
//                            .setLocation(67.0642024, 24.8786511) //SetLocation(Longitude,Latitude)
//                            .setApiKey("h510SdWaSS")
//                            .setStaging(true)
//                            .setTimeOut(30) // Set Time in Seconds
//                            .build(SplashScreen.this); // Your Current Context
//                    startActivityForResult(intent, intentCode);
//
                    Intent intent = new Intent(SplashScreen.this, LoginScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("splash", "splash");
                    startActivity(intent);
                    finish();
                }
//                            }
//                        }
//                    }

//                }

            }
        }, 2000);

//        }
        appPreferences = new AppPreferences(this);
        logo = (ImageView) findViewById(R.id.signsLogo);
        sambaLogo = (ImageView) findViewById(R.id.sambaIcon);
        requestQueue = Volley.newRequestQueue(this);
        pd = new Dialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        webService = new WebService();
        helper = Helper.getHelper(this);
        constants = Constants.getConstants(SplashScreen.this);
        logo.bringToFront();
        sambaLogo.bringToFront();






    }

    private void registerEatMubarakReceiver() {

        LocalBroadcastManager.getInstance(this).registerReceiver(analyticsReceiver,
                new IntentFilter("EM_ANALYTICS"));

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
//                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
//                == PackageManager.PERMISSION_GRANTED) {
//            mMap.setMyLocationEnabled(true);
//        } else {
//// Show rationale and request permission.
    }


    @Override
    public void onBackPressed() {
        //Do Nothing
    }

//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        if(new DeviceUtils().isDeviceRooted(getApplicationContext())){
//            showAlertDialogAndExitApp("This device is rooted. You can't use this app.");
//        }else {
//
//            Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                public void run() {
//                    Intent intent = new Intent(SplashScreen.this, LoginScreen.class);
//                    overridePendingTransition(0, 0);
//                    startActivity(intent);
//                    finish();
//                }
//            }, 2000);
//
//        }
//    }
//
//
//    public void showAlertDialogAndExitApp(String message) {
//
//        AlertDialog alertDialog = new AlertDialog.Builder(SplashScreen.this).create();
//        alertDialog.setTitle("Alert");
//        alertDialog.setMessage(message);
//        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                        Intent intent = new Intent(Intent.ACTION_MAIN);
//                        intent.addCategory(Intent.CATEGORY_HOME);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                        finish();
//                    }
//                });
//
//        alertDialog.show();
//    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(SplashScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                finish();

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == intentCode){

                String orderId = data.getStringExtra("orderId");
                String amount = data.getStringExtra("orderAmount");
                String restaurantName = data.getStringExtra("restaurantName");
                String deliveryAddress = data.getStringExtra("deliveryAddress");
                String deliveryEta = data.getStringExtra("deliveryEta");
                String isCod = data.getStringExtra("isCod");
                String discount = data.getStringExtra("discount");
                String promoCode = data.getStringExtra("promoCode");
                //Don’t forget to add this line if receiver is registered to catch
                //EM_ANALYTICS.

                LocalBroadcastManager.getInstance(this)
                        .unregisterReceiver(analyticsReceiver);
                //Add your code
            }
        }
    }



}
