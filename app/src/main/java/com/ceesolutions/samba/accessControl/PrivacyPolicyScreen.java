package com.ceesolutions.samba.accessControl;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ceesolutions.samba.R;

/**
 * Created by ayaz on 2/1/18.
 */

public class PrivacyPolicyScreen extends AppCompatActivity {

    private TextView doneBtn, headingTxt, securityHeading;
    private ImageView backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.disclaimer);
        initViews();
        getSupportActionBar().hide();

    }

    public void initViews() {

        doneBtn = (TextView) findViewById(R.id.nextBtn);
        headingTxt = (TextView) findViewById(R.id.headingTxt);
        securityHeading = (TextView) findViewById(R.id.subHeading);
        backBtn = (ImageView) findViewById(R.id.backBtn);

        headingTxt.setText("Privacy Policy");
        securityHeading.setText("Privacy Policy");


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                doneBtn.setEnabled(false);
                backBtn.setEnabled(false);
                Intent intent = new Intent(PrivacyPolicyScreen.this, LoginScreen.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        doneBtn.setEnabled(false);
        backBtn.setEnabled(false);
        Intent intent = new Intent(PrivacyPolicyScreen.this, LoginScreen.class);
        startActivity(intent);
        finish();
    }

}