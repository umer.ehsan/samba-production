package com.ceesolutions.samba.accessControl;

import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.RequiresApi;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityCompat.OnRequestPermissionsResultCallback;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.BuildConfig;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.CustomEditText;
import com.ceesolutions.samba.utils.DrawableClickListener;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.LocationHelper;
import com.ceesolutions.samba.utils.LocationService;
import com.ceesolutions.samba.utils.OnTextChangedListener;
import com.ceesolutions.samba.utils.PermissionUtils;
import com.ceesolutions.samba.utils.PopupDialog;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.github.kayvannj.permission_utils.PermissionUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import co.infinum.goldfinger.Error;
import co.infinum.goldfinger.Goldfinger;
import co.infinum.goldfinger.Warning;
import me.leolin.shortcutbadger.ShortcutBadger;

import static com.ceesolutions.samba.utils.ILocationConstants.LOACTION_ACTION;
import static com.ceesolutions.samba.utils.ILocationConstants.LOCATION_MESSAGE;

/**
 * Created by ayaz on 12/18/17.
 */

public class LoginScreen extends AppCompatActivity implements GestureDetector.OnGestureListener, ConnectionCallbacks,
        OnConnectionFailedListener, OnRequestPermissionsResultCallback,
        PermissionUtils.PermissionResultCallback {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static final String KEY_NAME = "Example";
    private final static int PLAY_SERVICES_REQUEST = 1000;
    private final static int REQUEST_CHECK_SETTINGS = 2000;
    public static double latitude;
    public static double longitude;
    public static boolean isPermissionGranted;
    boolean doubleBackToExitPressedOnce = false;
    Typeface tf;
    ArrayList<String> permissions1;
    PermissionUtils permissionUtils;
    List<Address> addresses;
    //@RequiresApi(api = Build.VERSION_CODES.M)
    Geocoder geocoder;
    private EditText editUserID;
    private CustomEditText editPassword;
    private ImageView sambaIcon, fingerPrintIcon, signsLogo, fingerIcon, profile_image, showPassword, background;
    private ImageButton closeBtn;
    private TextView forgotPassTxt, signUpTxt, agreementTxt, errorMessage, errorMessageForPassword, loginBtn, textView, doneBtn, heading, fingerStatus, message, imageTxt;
    private ProgressDialog pd;
    private RelativeLayout demoView, layoutMain, layoutButtons, passwordLayout;
    private String userName, pass;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private GestureDetector gestureDetector;
    private boolean isOpen = false;
    private boolean backPressed = false;
    private PopupDialog popupDialog;
    private LinearLayout linearLayout;
    private AVLoadingIndicatorView avi;
    private Dialog dialog, dialog1, dialogForFinger;
    private BottomSheetBehavior mBottomSheetBehavior;
    private CoordinatorLayout layout;
    private ImageView locationIcon, securityIcon, callIcon, FAQ, termsAndCondition;
    private boolean isChecked = true;
    private ImageView imageBtn;
    private RelativeLayout arrowBtn;
    private TextInputLayout userTextInput, passwordTextInput;
    private Utils utils;
    private TextView cancel_button, second_dialog_button;
    private View encryptButton;
    private View decryptButton;
    private View authenticateButton;
    private TextView statusView;
    private EditText secretInputView;
    private Goldfinger goldfinger;
    private String menus, fingerStr;
    private GoogleMap mMap;
    private String encryptedValue, fcmToken;
    private boolean isFinger = false;
    private String fingerError = "Fingerprint authentication is not enabled for current user. Please login using your username/password.";
    private LocationReceiver locationReceiver;
    private PermissionUtil.PermissionRequestObject mBothPermissionRequest;
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationHelper locationHelper;
    private AppPreferences appPreferences;
    private String location;
    private boolean isLogin = false;
    private boolean isLoginWithFinger = false;
    private boolean isVisible = false;
    private boolean isFingerLogin = false;
    private String requestBody;
    private OnTextChangedListener onTextChangedListener = new OnTextChangedListener() {
        @Override
        public void onTextChanged(String text) {
            encryptButton.setEnabled(!text.isEmpty() && goldfinger.hasEnrolledFingerprint());
        }
    };

//    public static void allowMySSL() {
//
//        try {
//            CertificateFactory cf = CertificateFactory.getInstance("X.509");
//
//            InputStream caInput = AppController.getInstance().getResources().openRawResource(R.raw.uatnew);
//            Certificate ca;
//            try {
//                ca = cf.generateCertificate(caInput);
//            } finally {
//                caInput.close();
//            }
//
//            // Create a KeyStore containing our trusted CAs
//            String keyStoreType = KeyStore.getDefaultType();
//            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//            keyStore.load(null, null);
//            keyStore.setCertificateEntry("ca", ca);
//
//            // Create a TrustManager that trusts the CAs in our KeyStore
//            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//            tmf.init(keyStore);
//
//            // Create an SSLContext that uses our TrustManager
//            SSLContext context = SSLContext.getInstance("TLS");
//            context.init(null, tmf.getTrustManagers(), null);
//
//            HttpsURLConnection.setDefaultSSLSocketFactory(context
//                    .getSocketFactory());
//
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (KeyStoreException e) {
//            e.printStackTrace();
//        } catch (KeyManagementException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (CertificateException e) {
//            e.printStackTrace();
//        }
//    }

//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
////        if (ContextCompat.checkSelfPermission(Login), Manifest.permission.ACCESS_FINE_LOCATION)
////                == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
////                == PackageManager.PERMISSION_GRANTED) {
////            mMap.setMyLocationEnabled(true);
////        } else {
////// Show rationale and request permission.
//        }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initViews();

//        String inputPattern = "MMMM dd, yyyy";
//        String outputPattern = "ddMMyyyy";
//        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.US);
//        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern,Locale.US);
//
//        Date date = null;
//        String str = null;
//
//        try {
//            String strCurrentDate = "November 11, 2018";
//            date = inputFormat.parse(strCurrentDate);// it's format should be same as inputPattern
//            str = outputFormat.format(date);
//            Log.e("Log ","str "+str);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        appPreferences = new AppPreferences(this);
        permissions1 = new ArrayList<>();
        locationReceiver = new LocationReceiver();
        ShortcutBadger.removeCount(this);
        constants.badgeCountEditor.clear();
        constants.badgeCountEditor.commit();
        constants.badgeCountEditor.apply();
        constants.editor.clear();
        constants.editor.commit();
        constants.editor.apply();
        constants.nonSambaCredentialsEditor.clear();
        constants.nonSambaCredentialsEditor.commit();
        constants.nonSambaCredentialsEditor.apply();
        constants.destinationAccountEditor.clear().commit();
        constants.destinationAccountEditor.apply();
        constants.sourceAccountEditor.clear().commit();
        constants.sourceAccountEditor.apply();
        constants.accountListEditor.clear().commit();
        constants.accountListEditor.apply();
        constants.accountsEditor.clear().commit();
        constants.accountsEditor.apply();
        constants.forgotPasswordEditor.clear().commit();
        constants.forgotPasswordEditor.apply();
        constants.purposeBillList.clear().commit();
        constants.purposeBillList.apply();
        constants.purposePrepaidList.clear().commit();
        constants.purposePrepaidList.apply();
        constants.purposeListEditor.clear().commit();
        constants.purposeListEditor.apply();
        constants.reasonListEditor.clear().commit();
        constants.reasonListEditor.apply();
        constants.statusListEditor.clear().commit();
        constants.statusListEditor.apply();
        constants.channelListEditor.clear().commit();
        constants.channelListEditor.apply();
        constants.billerListEditor.clear().commit();
        constants.billerListEditor.apply();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        getSupportActionBar().hide();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                fcmToken = constants.fcmSharedPref.getString("fcmToken", "N/A");
                Log.d("Fcm", "---->" + fcmToken);
            }

        }, 1500);


        fingerStr = constants.fingerPrint.getString("userName", "N/A");

        if (!TextUtils.isEmpty(fingerStr) && !fingerStr.equals("N/A") && !fingerStr.equals("")) {


            isFingerLogin = true;
            showDilogForFinger();
        }
//        FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(Context.FINGERPRINT_SERVICE);
//        if (!fingerprintManager.isHardwareDetected()) {
//            // Device doesn't support fingerprint authentication
//        } else if (!fingerprintManager.hasEnrolledFingerprints()) {
//            // User hasn't enrolled any fingerprints to authenticate with
//        } else {
//            // Everything is ready for fingerprint authentication
//        }

        if (Build.VERSION_CODES.LOLLIPOP > 22) {

//            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
//
//            if (mapFragment != null) {
//                mapFragment.getMapAsync(this);
//
//            }
        } else {

//            permissionUtils = new PermissionUtils(LoginScreen.this);
//
//            permissions1.add(Manifest.permission.ACCESS_FINE_LOCATION);
//            permissions1.add(Manifest.permission.ACCESS_COARSE_LOCATION);
//
//            permissionUtils.check_permission(permissions1, "Need GPS permission for getting your location", 1);
//
//            locationHelper = new LocationHelper(LoginScreen.this);
//            locationHelper.checkpermission();

            if (checkPlayServices()) {

                // Building the GoogleApi client
                buildGoogleApiClient();
                startLocationService();
//                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
//
//                if (mapFragment != null) {
//                    mapFragment.getMapAsync(this);
//
//                }
            }


        }
        if (checkAndRequestPermissions()) {

//            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
//
//            if (mapFragment != null) {
//                mapFragment.getMapAsync(this);
//
//            }
            // carry on the normal flow, as the case of  permissions  granted.
        }

//        MyRequest req = new MyRequest(LoginScreen.this);
//Params
//        JSONObject params = new JSONObject();
////Call
//        req.sendRequest("https://signsdev.samba.com.pk/login", LoginScreen.this, params, requestQueue, new VolleyCallback() {
//            @Override
//            public void onSuccess(JSONObject resp) {
//                try {
//                    JSONObject response = resp.getJSONObject("response");
//                    String status = response.getString("status");
//                    if (status.equals("OK")) {
//                        Object dataObject = resp.get("data");
//                        //Do something with dataObject
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//
//            @Override
//            public void onError() {
//
//                Log.d("--", "---");
//            }
//        });

        try {


//        if (RootUtils.isDeviceRooted()) {
//
//            showDilogForErrorForRoot("This application is not allowed on rooted device.", "WARNING");
//
//        } else {
//
//            if (Constants.isRootAvailable()) {
//
//                showDilogForErrorForRoot("This application is not allowed on rooted device.", "WARNING");
//
//            } else {
//
//                if (Constants.isRooted()) {
//
//                    showDilogForErrorForRoot("This application is not allowed on rooted device.", "WARNING");
//
//                } else {
//                    if (Constants.isRootAvailableForRoot()) {
//
//                        showDilogForErrorForRoot("This application is not allowed on rooted device.", "WARNING");
//                    }
//                }
//            }
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getLocation() {

        if (isPermissionGranted) {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {

                    try {
                        mLastLocation = LocationServices.FusedLocationApi
                                .getLastLocation(mGoogleApiClient);
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }
                    if (mLastLocation != null) {
                        latitude = mLastLocation.getLatitude();
                        longitude = mLastLocation.getLongitude();
                        getAddress("test");

                    } else {

                        showToast("Couldn't get the location. Make sure location is enabled on the device");
                    }

                }

            }, 3000);


        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i("", "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    public void showToast(String message) {
        Toast.makeText(LoginScreen.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            public void run() {
//                getLocation();
//
//                if (mLastLocation != null) {
//                    latitude = mLastLocation.getLatitude();
//                    longitude = mLastLocation.getLongitude();
//                    getAddress();
//
//                } else {
//
//                    showToast("Couldn't get the location. Make sure location is enabled on the device");
//                }
//
//            }
//
//        }, 3000);
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    public Address getAddress(final double latitude, final double longitude, final String value) {


        geocoder = new Geocoder(this, Locale.getDefault());

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {


                try {

                    new AsyncCaller().execute(String.valueOf(latitude), String.valueOf(longitude), value);
//                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//                    if (addresses != null) {
//                        if (!TextUtils.isEmpty(addresses.get(0).toString()) && !addresses.get(0).equals("null") && !addresses.get(0).equals(null)) {
//
//
//                            Address locationAddress = addresses.get(0);
//                            location = locationAddress.getAddressLine(0);
//
//                            if (value.equals("login"))
//                                Login();
//                            else if (value.equals("fingerprint"))
//                                LoginFingerPrint();
//                            else if (value.equals("forgot"))
//                                ForgotPassword();
//                            else if (value.equals("signup"))
//                                SignUp();
//
//                        } else {
////                                utils.showDilogForError("Please turn on location services.", "WARNING");
////                                loginBtn.setEnabled(true);
//                            location = "-";
//                            if (value.equals("login"))
//                                Login();
//                            else if (value.equals("fingerprint"))
//                                LoginFingerPrint();
//                            else if (value.equals("forgot"))
//                                ForgotPassword();
//                            else if (value.equals("signup"))
//                                SignUp();
//                        }
//                    } else {
//
//                        location = "-";
//                        if (value.equals("login"))
//                            Login();
//                        else if (value.equals("fingerprint"))
//                            LoginFingerPrint();
//                        else if (value.equals("forgot"))
//                            ForgotPassword();
//                        else if (value.equals("signup"))
//                            SignUp();
//                    }

                } catch (Exception e) {
                    e.printStackTrace();

                    if (value.equals("login")) {

                        location = "-";
                        Login();
                    } else if (value.equals("fingerprint")) {
                        location = "-";
                        LoginFingerPrint();
                    } else if (value.equals("forgot")) {
                        location = "-";
                        ForgotPassword();
                    } else if (value.equals("signup")) {
                        location = "-";
                        SignUp();
                    }
                }


            }
        });

        if (addresses != null) {
            return addresses.get(0);
        } else
            return null;

    }

    public String getAddress(String login) {

        Address locationAddress = getAddress(latitude, longitude, login);
        String address = "";
        if (locationAddress != null) {
            address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();

            String currentLocation;

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address;

                if (!TextUtils.isEmpty(address1))
                    currentLocation += "\n" + address1;

                if (!TextUtils.isEmpty(city)) {
                    currentLocation += "\n" + city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += " - " + postalCode;
                } else {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += "\n" + postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation += "\n" + state;

                if (!TextUtils.isEmpty(country))
                    currentLocation += "\n" + country;


            }

        }

        return address;

    }

    /**
     * Creating google api client object
     */

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        mGoogleApiClient.connect();

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {

                final Status status = locationSettingsResult.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location requests here
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(LoginScreen.this, REQUEST_CHECK_SETTINGS);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });


    }

    /**
     * Method to verify google play services on the device
     */

    private boolean checkPlayServices() {

        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();

        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(LoginScreen.this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(resultCode)) {
                googleApiAvailability.getErrorDialog(LoginScreen.this, resultCode,
                        PLAY_SERVICES_REQUEST).show();
            } else {
                Toast.makeText(LoginScreen.this,
                        "This device is not LoginScreen.", Toast.LENGTH_LONG)
                        .show();
                LoginScreen.this.finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {


            final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
            switch (requestCode) {
                case REQUEST_CHECK_SETTINGS:
                    switch (resultCode) {
                        case Activity.RESULT_OK:
                            // All required changes were successfully made
                            getLocation();
//                        Handler handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                            public void run() {
//                                getLocation();
//
//                                if (mLastLocation != null) {
//                                    latitude = mLastLocation.getLatitude();
//                                    longitude = mLastLocation.getLongitude();
//                                    getAddress();
//
//                                } else {
//
//                                    showToast("Couldn't get the location. Make sure location is enabled on the device");
//                                }
//
//                            }
//
//                        }, 3000);
                            break;
                        case Activity.RESULT_CANCELED:
                            // The user was asked to change settings, but chose not to
                            break;
                        default:
                            break;
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initViews() {

        tf = Typeface.createFromAsset(getAssets(), "fonts/Jameel Noori Nastaleeq.ttf");
        editUserID = findViewById(R.id.editUserID);
        userTextInput = findViewById(R.id.userTextInput);

//        userTextInput.setHint(getString(R.string.urdu));
//        editUserID.setHint(R.string.urdu);
//        editUserID.setTypeface(tf);
        goldfinger = new Goldfinger.Builder(this).setLogEnabled(BuildConfig.DEBUG).build();

//        secretInputView.addTextChangedListener(onTextChangedListener);
        errorMessageForPassword = findViewById(R.id.errorMessageForPassword);
        passwordTextInput = findViewById(R.id.passwordTextInput);
        editPassword = findViewById(R.id.editPassword);
        demoView = findViewById(R.id.demoView);
        layoutMain = findViewById(R.id.layoutMain);
        layoutButtons = findViewById(R.id.layoutButtons);
        linearLayout = findViewById(R.id.indicator);
        forgotPassTxt = findViewById(R.id.forgotPassTxt);
        agreementTxt = findViewById(R.id.agreementTxt);
        errorMessage = findViewById(R.id.errorMessage);
        signsLogo = findViewById(R.id.signsLogo);
        locationIcon = findViewById(R.id.locationIcon);
        imageBtn = findViewById(R.id.imageBtn);
        securityIcon = findViewById(R.id.editSecurity);
        callIcon = findViewById(R.id.contactIcon);
        FAQ = findViewById(R.id.faqsIcon);
//        privacyPolicy = (ImageView) findViewById(R.id.privacyPolicyIcon);
//        Disclaimer = (ImageView) findViewById(R.id.disclaimerIcon);
        termsAndCondition = findViewById(R.id.termNcondIcon);
        signUpTxt = findViewById(R.id.signUpTxt);
        arrowBtn = findViewById(R.id.arrowBtn);
        loginBtn = findViewById(R.id.loginBtn);
        closeBtn = findViewById(R.id.closeBtn);
        sambaIcon = findViewById(R.id.sambaIcon);
        background = findViewById(R.id.background);
        profile_image = findViewById(R.id.profile_image);
        imageTxt = findViewById(R.id.imageTxt);
        showPassword = findViewById(R.id.showPassword);
        fingerPrintIcon = findViewById(R.id.fingerPrintIcon);
        passwordLayout = findViewById(R.id.passwordLayout);
        geocoder = new Geocoder(this, Locale.getDefault());
        editPassword.setDrawableClickListener(new DrawableClickListener() {


            public void onClick(DrawablePosition target) {
                switch (target) {
                    case LEFT:
                        //Do something here
//                        Toast.makeText(LoginScreen.this, "Here", Toast.LENGTH_SHORT).show();
                        break;

                    case RIGHT:
                        if (!isVisible) {
                            editPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            isVisible = true;
                        } else {
                            isVisible = false;
                            editPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        }
                        editPassword.setSelection(editPassword.length());
                        break;

                    default:
                        break;
                }
            }

        });
//        avi = (AVLoadingIndicatorView) findViewById(R.id.avi);
//        avi.setIndicator("BallPulseIndicator");
//        View bottomSheet = findViewById(R.id.bottom_sheet);
//        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
//        mBottomSheetBehavior.setPeekHeight(0);
//        layout = (CoordinatorLayout) findViewById(R.id.layout);
//        layout.setVisibility(View.GONE);
        requestQueue = Volley.newRequestQueue(this);
//        webService = new WebService();
        helper = Helper.getHelper(this);
        constants = Constants.getConstants(LoginScreen.this);
        utils = new Utils(LoginScreen.this);
        pd = new ProgressDialog(LoginScreen.this);
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        String birthday = constants.fcmSharedPref.getString("birthday", "N/A");
        Log.d("Birth--", birthday);
        if (!TextUtils.isEmpty(birthday) && birthday.equals("yes")) {

            Date currentDate = Calendar.getInstance().getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String formattedDate = sdf.format(currentDate);
            String date = constants.fcmSharedPref.getString("date", "N/A");

            if (formattedDate.equals(date)) {


                profile_image.setVisibility(View.VISIBLE);
                imageTxt.setVisibility(View.VISIBLE);


            } else {

            }
        }

        String eidadha = constants.fcmSharedPref.getString("eidadha", "N/A");
        Log.d("eidadha--", eidadha);
        if (!TextUtils.isEmpty(eidadha) && eidadha.equals("yes")) {

            Date currentDate = Calendar.getInstance().getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String formattedDate = sdf.format(currentDate);
            String date = constants.fcmSharedPref.getString("date", "N/A");

            if (formattedDate.equals(date)) {


                profile_image.setVisibility(View.VISIBLE);
                profile_image.setImageResource(R.drawable.eiduladha);
                imageTxt.setVisibility(View.VISIBLE);
                imageTxt.setText("Eid Mubarak!");


            } else {

            }
        }

        String eidfitr = constants.fcmSharedPref.getString("eidfitr", "N/A");
        Log.d("eidfitr--", eidfitr);
        if (!TextUtils.isEmpty(eidfitr) && eidfitr.equals("yes")) {

            Date currentDate = Calendar.getInstance().getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String formattedDate = sdf.format(currentDate);
            String date = constants.fcmSharedPref.getString("date", "N/A");

            if (formattedDate.equals(date)) {


                profile_image.setVisibility(View.VISIBLE);
                profile_image.setImageResource(R.drawable.eid);
                imageTxt.setVisibility(View.VISIBLE);
                imageTxt.setText("Eid Mubarak!");


            } else {

            }
        }

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = df.format(Calendar.getInstance().getTime());

        if (date.length() > 5) {
            date = date.substring(date.length() - 5);
        } else {
            // whatever is appropriate in this case
//            throw new IllegalArgumentException("word has less than 3 characters!");
        }


        String day = date.substring(date.length() - 2);
        String mon = date.substring(0, 2);
        if (day.equals("14")) {
            if (mon.equals("08")) {


                profile_image.setVisibility(View.VISIBLE);
                profile_image.setImageResource(R.drawable.aug);
                imageTxt.setVisibility(View.VISIBLE);
                imageTxt.setText("Happy Independence Day");

            }
        }

        if (day.equals("23")) {
            if (mon.equals("03")) {


                profile_image.setVisibility(View.VISIBLE);
                profile_image.setImageResource(R.drawable.pakistan_day);
                imageTxt.setVisibility(View.VISIBLE);
                imageTxt.setText("Resolution Day");

            }
        }


        if (day.equals("09")) {
            if (mon.equals("11")) {


                profile_image.setVisibility(View.VISIBLE);
                profile_image.setImageResource(R.drawable.iqbal);
                imageTxt.setVisibility(View.VISIBLE);
                imageTxt.setText("Allama Iqbal Day");

            }
        }

        if (day.equals("25")) {
            if (mon.equals("12")) {


                profile_image.setVisibility(View.VISIBLE);
                profile_image.setImageResource(R.drawable.quaid);
                imageTxt.setVisibility(View.VISIBLE);
                imageTxt.setText("Quaid-e-Azam Day");

            }
        }


        CheckVersion();

        String txt = "<font COLOR=\'#73808a\'>" + "I accept " + "</font>"
                + "<font COLOR=\'#0064b2\'><u>" + "Security Recommendations" + "</u></font>";
        agreementTxt.setText(Html.fromHtml(txt));
        gestureDetector = new GestureDetector(LoginScreen.this, LoginScreen.this);
        locationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                locationIcon.setEnabled(false);
//                HttpsTrustManager.allowMySSL(LoginScreen.this);
                Intent intent = new Intent(LoginScreen.this, ATMLocatorScreen.class);
                startActivity(intent);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        locationIcon.setEnabled(true);
                    }
                }, 500);
//                finish();
            }
        });

        agreementTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                agreementTxt.setEnabled(false);
                Intent intent = new Intent(LoginScreen.this, SecurityRecommmendationScreen.class);
                intent.putExtra("Login", "login");
                startActivity(intent);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        agreementTxt.setEnabled(true);
                    }
                }, 500);
//                finish();
            }
        });

        securityIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                securityIcon.setEnabled(false);
                Intent intent = new Intent(LoginScreen.this, SecurityRecommmendationScreen.class);
                startActivity(intent);
                finish();
                securityIcon.setEnabled(true);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        securityIcon.setEnabled(true);
                    }
                }, 500);
//                finish();

            }
        });
        callIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                callIcon.setEnabled(false);
                Intent intent = new Intent(LoginScreen.this, ContactUsScreen.class);
                startActivity(intent);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        callIcon.setEnabled(true);
                    }
                }, 500);
//                finish();

            }
        });

        FAQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FAQ.setEnabled(false);
                Intent intent = new Intent(LoginScreen.this, FAQsScreen.class);
                startActivity(intent);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        FAQ.setEnabled(true);
                    }
                }, 500);
//                finish();

            }
        });
//        privacyPolicy.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                privacyPolicy.setEnabled(false);
//                Intent intent = new Intent(LoginScreen.this, PrivacyPolicyScreen.class);
//                startActivity(intent);
//                privacyPolicy.setEnabled(true);
//                finish();
//
////                Handler handler = new Handler();
////                handler.postDelayed(new Runnable() {
////                    public void run() {
////                        viewMenu(true);
////                        isOpen = false;
////                        loginBtn.setVisibility(View.VISIBLE);
////                        arrowBtn.bringToFront();
////                        signUpTxt.bringToFront();
////                        forgotPassTxt.bringToFront();
////                    }
////                }, 150);
////                finish();
//
//            }
//        });


        showPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isVisible) {
                    editPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    showPassword.setImageResource(R.drawable.eyecross);
                    isVisible = true;
                } else {
                    isVisible = false;
                    showPassword.setImageResource(R.drawable.eye);
                    editPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                editPassword.setSelection(editPassword.length());

            }
        });
//        Disclaimer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Disclaimer.setEnabled(false);
//                Intent intent = new Intent(LoginScreen.this, DisclaimerScreen.class);
//                startActivity(intent);
//                Disclaimer.setEnabled(true);
//                finish();
//
//            }
//        });
        termsAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                termsAndCondition.setEnabled(false);
                Intent intent = new Intent(LoginScreen.this, TermsNConditionsScreen.class);
                startActivity(intent);
                finish();
            }
        });


        signUpTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FirebaseAnalytics.getInstance(LoginScreen.this).logEvent("Registration_Pressed",new Bundle());
                signUpTxt.setEnabled(false);
                final String longitude1 = appPreferences.getString("longitude", "N/A");
                final String latitude1 = appPreferences.getString("latitude", "N/A");


                if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                    longitude = Double.valueOf(longitude1);
                    latitude = Double.valueOf(latitude1);
                    location = getAddress("signup");

                } else {

//                            utils.showDilogForError("Please turn on location services.", "WARNING");
//                            loginBtn.setEnabled(true);
                    longitude = 0;
                    latitude = 0;
                    location = "-";
                    SignUp();
                }


            }
        });

        forgotPassTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FirebaseAnalytics.getInstance(LoginScreen.this).logEvent("Reset_Password_Pressed", new Bundle());

                forgotPassTxt.setEnabled(false);
                final String longitude1 = appPreferences.getString("longitude", "N/A");
                final String latitude1 = appPreferences.getString("latitude", "N/A");


                if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                    longitude = Double.valueOf(longitude1);
                    latitude = Double.valueOf(latitude1);
                    location = getAddress("forgot");

                } else {

//                            utils.showDilogForError("Please turn on location services.", "WARNING");
//                            loginBtn.setEnabled(true);
                    longitude = 0;
                    latitude = 0;
                    location = "-";
                    ForgotPassword();
                }

            }
        });

        imageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isChecked) {
                    imageBtn.setBackground(getResources().getDrawable(R.drawable.normal));
                    isChecked = false;
                } else {
                    imageBtn.setBackground(getResources().getDrawable(R.drawable.checked));
                    isChecked = true;
                }

            }
        });
        userName = editUserID.getText().toString().trim();

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {


                viewMenu(true);
                isOpen = false;
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                        loginBtn.setVisibility(View.VISIBLE);
                        arrowBtn.bringToFront();
                        signUpTxt.bringToFront();
                        forgotPassTxt.bringToFront();
                    }
                }, 300);

            }
        });

        fingerPrintIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user = constants.fingerPrint.getString("userName", "N/A");
                if (!TextUtils.isEmpty(user) && !user.equals("N/A") && !user.equals("")) {
                    isFingerLogin = true;
                    showDilogForFinger();
                } else {
                    showDilogForErrorForFinger("Please login with user name and password to enable fingerprint authentication for your account.", "INFORMATION");
                }

//                resetStatusText();
//                authenticateUserFingerprint();
            }
        });

//        arrowBtn.setOnClickListener(new View.OnClickListener()
//
//        {
//            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//            @Override
//            public void onClick(View view) {
//
//                isOpen = true;
//
//                demoView.bringToFront();
//                demoView.setVisibility(View.VISIBLE);
//
//                viewMenu(false);
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        loginBtn.setVisibility(View.INVISIBLE);
//                    }
//                }, 200);
//            }
//        });


        editUserID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);

            }

            @Override
            public void afterTextChanged(Editable editable) {

                userName = editable.toString().trim();

            }
        });


        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                errorMessageForPassword.setVisibility(View.GONE);


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                errorMessageForPassword.setVisibility(View.GONE);


            }

            @Override
            public void afterTextChanged(Editable editable) {

                pass = editable.toString();


            }
        });

        editPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {

                    passwordLayout.setBackground(ContextCompat.getDrawable(LoginScreen.this, R.drawable.add_item_grey_login));
                    editUserID.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
                } else {

                    passwordLayout.setBackground(ContextCompat.getDrawable(LoginScreen.this, R.drawable.add_item_blue));
                    editUserID.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey)));
                }
            }
        });
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Intent intent = new Intent(LoginScreen.this, LoginWithPassAndPinScreen.class);
//                intent.putExtra("userName", userName);
//                startActivity(intent);
//                finish();

//                disableButton();
//                showIndication();
//                dialog.show();


                userName = editUserID.getText().toString().trim();
                pass = editPassword.getText().toString().trim();


                if (!helper.validateInputForSC(userName) || userName.contains(" ")) {

                    errorMessage.setVisibility(View.VISIBLE);
                    errorMessage.bringToFront();
                    errorMessage.setText("Username cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                    errorMessage.setError("");
                }

                if (!helper.validateInputForSC(pass) || pass.contains(" ")) {

                    errorMessageForPassword.setVisibility(View.VISIBLE);
                    errorMessageForPassword.bringToFront();
                    errorMessageForPassword.setError("");
                    errorMessageForPassword.setText("Password cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                }

                if (TextUtils.isEmpty(userName)) {
                    errorMessage.setVisibility(View.VISIBLE);
                    errorMessage.bringToFront();
                    errorMessage.setText("Username cannot be empty");
                    errorMessage.setError("");
                }
                if (TextUtils.isEmpty(pass)) {
                    errorMessageForPassword.setVisibility(View.VISIBLE);
                    errorMessageForPassword.bringToFront();
                    errorMessageForPassword.setError("");
                    errorMessageForPassword.setText("Password cannot be empty");

                }

                if ((!TextUtils.isEmpty(pass)) && (!TextUtils.isEmpty(userName)) && (helper.validateInputForSC(pass) && !pass.contains(" ")) && (helper.validateInputForSC(userName) && !userName.contains(" "))) {

                    if (isChecked) {
                        loginBtn.setEnabled(false);
                        dialog.show();

                        final String longitude1 = appPreferences.getString("longitude", "N/A");
                        final String latitude1 = appPreferences.getString("latitude", "N/A");


                        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                            longitude = Double.valueOf(longitude1);
                            latitude = Double.valueOf(latitude1);
                            location = getAddress("login");

                        } else {

//                            utils.showDilogForError("Please turn on location services.", "WARNING");
//                            loginBtn.setEnabled(true);
                            longitude = 0;
                            latitude = 0;
                            location = "-";
                            Login();
                        }

                    } else {

                        utils.showDilogForError("Please accept Security Recommendations", "WARNING");
                        loginBtn.setEnabled(true);
                    }
                }

            }
        });


    }

    private void resetStatusText() {
//        fingerStatus.setTextColor(ContextCompat.getColor(this, R.color.notiheader));
//        fingerStatus.setText(getString(R.string.authenticate_user));
    }

    private void authenticateUserFingerprint() {
        goldfinger.authenticate(new Goldfinger.Callback() {
            @Override
            public void onSuccess(String value) {
                onSuccessResult(value);
            }

            @Override
            public void onWarning(Warning warning) {
                onWarningResult(warning);
            }

            @Override
            public void onError(Error error) {
                onErrorResult(error);
            }
        });
    }

    private void onSuccessResult(String value) {
        onResult("Authenticated Successfully", value);
        fingerPrintIcon.setBackgroundResource(R.drawable.fingerprint_circle_blue);
//        imageBtn.setBackground(getResources().getDrawable(R.drawable.checked));
//        isChecked = true;
        if (isChecked) {

            if (isFingerLogin) {
                dialog1.dismiss();
                final String longitude1 = appPreferences.getString("longitude", "N/A");
                final String latitude1 = appPreferences.getString("latitude", "N/A");


                if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                    longitude = Double.valueOf(longitude1);
                    latitude = Double.valueOf(latitude1);
                    location = getAddress("fingerprint");

                } else {


//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                loginBtn.setEnabled(true);
                    longitude = 0;
                    latitude = 0;
                    location = "-";
                    LoginFingerPrint();
                }


            }
        } else {
            dialog1.dismiss();
            utils.showDilogForError("Please accept Security Recommendations", "WARNING");
        }
//        fingerStatus.setTextColor(ContextCompat.getColor(this, R.color.notiheader));
    }

    private void onErrorResult(Error error) {
        onResult("Authentication Failure", error.toString());
        fingerPrintIcon.setBackgroundResource(R.drawable.fingerprint_circle_red);
//        fingerStatus.setTextColor(ContextCompat.getColor(this, R.color.tabunderline));
    }

    private void onWarningResult(Warning warning) {
        onResult("Authentication Failure", warning.toString());
        fingerPrintIcon.setBackgroundResource(R.drawable.fingerprint_circle_red);
//        fingerStatus.setTextColor(ContextCompat.getColor(this, R.color.tabunderline));
    }

    private void onResult(String methodName, String value) {
//        fingerStatus.setText(String.format(Locale.US, "%s", methodName, ""));
    }

    @Override
    protected void onStart() {
        super.onStart();

        LocalBroadcastManager.getInstance(this).registerReceiver(locationReceiver, new IntentFilter(LOACTION_ACTION));
        fingerPrintIcon.setEnabled(goldfinger.hasEnrolledFingerprint());

        if (goldfinger.hasFingerprintHardware()
                && goldfinger.hasEnrolledFingerprint()) {
            fingerPrintIcon.setEnabled(true);
        } else {
            fingerPrintIcon.setVisibility(View.INVISIBLE);
//            statusView.setText(getString(R.string.fingerprint_not_available));
//            statusView.setTextColor(ContextCompat.getColor(this, R.color.error));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        goldfinger.cancel();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(locationReceiver);
    }

    public void LoginFingerPrint() {

        try {

            if (!isLoginWithFinger) {
                isLoginWithFinger = true;

                if (helper.isNetworkAvailable()) {

                    dialog.show();
                    final String md5 = Helper.md5(fingerStr.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    HttpsTrustManager.allowMySSL(LoginScreen.this);
                    final String time = Constants.dateTime;
                    final String token = Helper.md5(time + "signs" + constants.address);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sSigns_Username", constants.fingerPrint.getString("userName", "N/A"));
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", token);
                    params.put("sTimeStamp", time);
                    params.put("sLogin_Type", "0");
                    params.put("sDeviceLatitude", String.valueOf(latitude));
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sLocationName", location);
                    if (!fcmToken.equals("N/A"))
                        params.put("sFcm_Token", fcmToken);
                    else {
                        Log.d("token", "--" + constants.fcmSharedPref.getString("fcmToken", "N/A"));
                        params.put("sFcm_Token", constants.fcmSharedPref.getString("fcmToken", "N/A"));

                    }
                    params.put("sFingerPrintToken", "");
                    params.put("sServerSessionKey", md5);
                    CryptLib _crypt = new CryptLib();
                    requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.fingerprintAuthURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ApplicationLogin_FingerPrintResult").getString("Status_Code").equals("00")) {
                                                try {
                                                    appPreferences.putString("location", location);
                                                    isFinger = true;
                                                    menus = jsonObject.getString("menus");


                                                } catch (JSONException e) {
                                                    e.printStackTrace();
//                                                pd.dismiss();
//                                                isLoginWithFinger = false;
                                                }


                                                if (jsonObject.getJSONObject("Signs_ApplicationLogin_FingerPrintResult").getString("Is_Locked").equals("Y")) {

                                                    dialog.dismiss();
                                                    utils.showDilogForError("Your Samba Digital account is locked. Please contact Samba Phone Banking at at +92-11-11-72622 (SAMBA) for further information and assistance", "ERROR");

                                                } else {
                                                    if ((jsonObject.getJSONObject("Signs_ApplicationLogin_FingerPrintResult").getString("Is_FirstLogin").equals("Y"))) {
//
                                                        FirebaseAnalytics.getInstance(LoginScreen.this).logEvent("User_Login_Success",new Bundle());
                                                        isFirstLogin(jsonObject.getJSONObject("Signs_ApplicationLogin_FingerPrintResult"));
                                                        isLoginWithFinger = false;
                                                    } else {

                                                        if ((jsonObject.getJSONObject("Signs_ApplicationLogin_FingerPrintResult").getString("Is_DeviceExist").equals("False"))) {
                                                            FirebaseAnalytics.getInstance(LoginScreen.this).logEvent("User_Login_Success",new Bundle());
                                                            isDeviceExist(jsonObject.getJSONObject("Signs_ApplicationLogin_FingerPrintResult"));
                                                            isLoginWithFinger = false;
                                                        } else {

//                                            isFirstLogin(jsonObject);
                                                            FirebaseAnalytics.getInstance(LoginScreen.this).logEvent("User_Login_Success",new Bundle());
                                                            DeviceAlreadyExist(jsonObject.getJSONObject("Signs_ApplicationLogin_FingerPrintResult"));
                                                            isLoginWithFinger = false;

                                                        }
                                                    }
                                                }
                                            } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_FingerPrintResult").getString("Status_Code").equals("02")) {
                                                dialog.dismiss();
                                                showDilogForErrorForLogin(fingerError, "ERROR");
                                                isLoginWithFinger = false;
//
                                            } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_FingerPrintResult").getString("Status_Code").equals("10")) {
                                                dialog.dismiss();
                                                utils.showDilogForError("Invalid iPIN entered. Please enter correct iPIN received on your mobile/email address.", "ERROR");
                                                isLoginWithFinger = false;
                                            } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_FingerPrintResult").getString("Status_Code").equals("54")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogin_FingerPrintResult").getString("Status_Description"), "ERROR", LoginScreen.this, new LoginScreen());
                                                isLoginWithFinger = false;

                                            } else {
                                                dialog.dismiss();

                                                showDilogForErrorForLogin(fingerError, "ERROR");
                                                isLoginWithFinger = false;

                                            }
                                        } else {
                                            dialog.dismiss();

                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                            isLoginWithFinger = false;
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        dialog.dismiss();

                                        showDilogForErrorForLogin(fingerError, "ERROR");
                                        isLoginWithFinger = false;
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        dialog.dismiss();

                                        showDilogForErrorForLogin(fingerError, "ERROR");
                                        isLoginWithFinger = false;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        dialog.dismiss();

                                        showDilogForErrorForLogin(fingerError, "ERROR");
                                        isLoginWithFinger = false;
                                    }
                                }
                            })
//                    ) {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sSigns_Username", constants.fingerPrint.getString("userName", "N/A"));
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", token);
//                            params.put("sTimeStamp", time);
//                            params.put("sLogin_Type", "0");
//                            params.put("sDeviceLatitude", String.valueOf(latitude));
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sLocationName", location);
//                            params.put("sFcm_Token", fcmToken);
//                            params.put("sFingerPrintToken", "");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };
                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } else {
                    dialog.dismiss();
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                    utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                    isLoginWithFinger = false;

                }
            }

        } catch (Exception e) {

//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            dialog.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            isLoginWithFinger = false;

        }
    }

    public void Login() {

        try {
            if (!isLogin) {
                isLogin = true;

                if (helper.isNetworkAvailable()) {
                    final String time = Constants.dateTime;
                    final String token = Helper.md5(time.split("\\+")[0] + "signs" + constants.address);
                    dialog.show();

//                webService.Login(userName, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getLoginReqResponse();
//                        Log.d("Response", "Object---->" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
//                                Error();
//
//
//                            } else {
//
//                                try {
//
//                                    if (jsonObject.get("Status_Code").equals("100")) {
//
//                                        Status100();
//
//                                    } else if (jsonObject.get("Status_Code").equals("00")) {
//
//                                        if (jsonObject.getString("Is_ForgotPassword").equals("Y")) {
//
//                                            dialog.dismiss();
//                                            showDilogForError("Dear Samba Online Customer, your current password against your SOL ID is no longer valid on the new Samba Digital Banking. Please press ‘OK’ for issuence of new password.\n" +
//                                                    "For further clarity, kindly call SambaPhone at +92-21-11-11 72622 (SAMBA).\n" +
//                                                    "Note: For issuance of new passwrod, you must have registered email address and mobile number for authenticatioon purpose.", "ERROR");
//                                        } else {
//                                            if (jsonObject.get("Is_Locked").equals("Y")) {
//
//                                                Status00();
//
//                                            } else {
//
//                                                Status();
//
//                                            }
//                                        }
//
//                                    } else if (jsonObject.get("Status_Code").equals("17")) {
//
//                                        dialog.dismiss();
//                                        utils.showDilogForError("You cannot perform login on Dormant account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                        editUserID.setText("");
//                                        editPassword.setText("");
//                                        editUserID.requestFocus();
//                                        loginBtn.setEnabled(true);
//
//                                    } else if (jsonObject.get("Status_Code").equals("18")) {
//                                        dialog.dismiss();
//                                        utils.showDilogForError("You cannot perform login on Joint account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                        editUserID.setText("");
//                                        editPassword.setText("");
//                                        editUserID.requestFocus();
//                                        loginBtn.setEnabled(true);
//
//
//                                    } else if (jsonObject.get("Status_Code").equals("19")) {
//                                        dialog.dismiss();
//                                        utils.showDilogForError("You cannot perform login on Deceased customer. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                        editUserID.setText("");
//                                        editPassword.setText("");
//                                        editUserID.requestFocus();
//                                        loginBtn.setEnabled(true);
//
//                                    } else {
//
//                                        Error();
//                                    }
//
//
//                                } catch (Exception e) {
//
//                                    e.printStackTrace();
//                                    loginBtn.setEnabled(true);
//                                    dialog.dismiss();
//                                }
//                            }
//                        } else {
//
//                            Error();
//
//
//                        }
//                    }
//                }, 10000);
                    final String md5 = Helper.md5(userName.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sSigns_Username", userName);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", token);
                    params.put("sTimeStamp", time);
                    params.put("sDeviceLatitude", String.valueOf(latitude));
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sLocationName", location);
                    params.put("sLogin_Type", "USERNAME_PASSWORD");
                    params.put("sServerSessionKey", md5);
                    CryptLib _crypt = new CryptLib();
                    requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
//                    requestBody = requestBody.replaceAll("\\+", "@PLUS@");
//                    final String requestBody = stringBuilder.toString();
                    HttpsTrustManager.allowMySSL(LoginScreen.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.loginURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code").equals("00")) {


                                                if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Is_ForgotPassword").equals("Y")) {
//
                                                    dialog.dismiss();
                                                    showDilogForError("Dear Samba Online Customer, your current password against your SOL ID is no longer valid on the new Samba Digital Banking. Please press ‘OK’ for issuence of new password.\n" +
                                                            "For further clarity, kindly call SambaPhone at +92-21-11-11 72622 (SAMBA).\n" +
                                                            "Note: For issuance of new passwrod, you must have registered email address and mobile number for authenticatioon purpose.", "ERROR");
                                                    isLogin = false;
                                                } else {
                                                    if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Is_Locked").equals("Y")) {

                                                        Status00();

                                                    } else {

                                                        Status();

                                                    }
                                                }


                                            } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code").equals("100")) {

                                                Status100();
                                            } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code").equals("17")) {

                                                dialog.dismiss();
                                                utils.showDilogForError("You cannot perform login on Dormant account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                                editUserID.setText("");
                                                editPassword.setText("");
                                                editUserID.requestFocus();
                                                loginBtn.setEnabled(true);
                                                isLogin = false;

                                            } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code").equals("18")) {

                                                dialog.dismiss();
                                                utils.showDilogForError("You cannot perform login on Joint account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                                editUserID.setText("");
                                                editPassword.setText("");
                                                editUserID.requestFocus();
                                                loginBtn.setEnabled(true);
                                                isLogin = false;

                                            } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code").equals("19")) {
                                                dialog.dismiss();
                                                utils.showDilogForError("You cannot perform login on Deceased customer. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                                editUserID.setText("");
                                                editPassword.setText("");
                                                editUserID.requestFocus();
                                                loginBtn.setEnabled(true);
                                                isLogin = false;

                                            } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code").equals("54")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Description"), "ERROR", LoginScreen.this, new LoginScreen());
                                                editUserID.setText("");
                                                editPassword.setText("");
                                                editUserID.requestFocus();
                                                loginBtn.setEnabled(true);
                                                isLogin = false;

                                            } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Code").equals("69")) {
                                                dialog.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Description"), "ERROR", LoginScreen.this, new LoginScreen());
                                                editUserID.setText("");
                                                editPassword.setText("");
                                                editUserID.requestFocus();
                                                loginBtn.setEnabled(true);
                                                isLogin = false;

                                            } else {
                                                dialog.dismiss();
//                                        utils.showDilogForError(jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Description"), "ERROR");
//                                        editUserID.setText("");
//                                        editPassword.setText("");
//                                        editUserID.requestFocus();
//                                        loginBtn.setEnabled(true);
                                                Error();

                                            }
                                        } else {

                                            dialog.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
//                                        utils.showDilogForError(jsonObject.getJSONObject("Signs_ApplicationLogin_Step1Result").getString("Status_Description"), "ERROR");
                                            editUserID.setText("");
                                            editPassword.setText("");
                                            editUserID.requestFocus();
                                            loginBtn.setEnabled(true);
//                                            Error();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Error();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        Error();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Error();
                                    }
                                }
                            })
//                    ) {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sSigns_Username", userName);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", token);
//                            params.put("sTimeStamp", time);
//                            params.put("sDeviceLatitude", String.valueOf(latitude));
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sLocationName", location);
//                            params.put("sLogin_Type", "USERNAME_PASSWORD");
//                            params.put("sServerSessionKey", md5);
//
//                            return params;
//                        }
//                    };{
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } else {
                    dialog.dismiss();
                    utils.showDilogForError("Please check your internet connectivity.", "WARNING");
//                Toast.makeText(LoginScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                    loginBtn.setEnabled(true);
                    isLogin = false;

                }
            }
        } catch (Exception e) {
            dialog.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
//            Toast.makeText(LoginScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            loginBtn.setEnabled(true);
            isLogin = false;
//            Login();

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void viewMenu(boolean isOpen) {

        try {


            if (!isOpen) {

                int x = arrowBtn.getRight() - arrowBtn.getMeasuredWidth() / 2;
                int y = arrowBtn.getBottom() - arrowBtn.getMeasuredWidth() / 2;

                int startRadius = 0;
                int endRadius = (int) Math.hypot(layoutMain.getWidth(), layoutMain.getHeight());

                Animator anim = ViewAnimationUtils.createCircularReveal(layoutButtons, x, y, startRadius, endRadius);

                layoutButtons.setVisibility(View.VISIBLE);
                anim.setDuration(500);
                anim.start();

            } else {

                int x = arrowBtn.getRight() - arrowBtn.getMeasuredWidth() / 2;
                int y = arrowBtn.getBottom() - arrowBtn.getMeasuredWidth() / 2;

                int startRadius = Math.max(demoView.getWidth(), demoView.getHeight());
                int endRadius = 0;

                Animator anim = ViewAnimationUtils.createCircularReveal(layoutButtons, x, y, startRadius, endRadius);
                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        layoutButtons.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
                anim.setDuration(500);
                anim.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onFling(MotionEvent event, MotionEvent motionEvent, float X, float Y) {

        try {


            if (event.getY() - motionEvent.getY() > 50) {

                isOpen = true;
                demoView.bringToFront();
                demoView.setVisibility(View.VISIBLE);

                viewMenu(false);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                        loginBtn.setVisibility(View.INVISIBLE);
                    }
                }, 200);
//            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                return true;
            }

            if (motionEvent.getY() - event.getY() > 50) {
                isOpen = false;
                viewMenu(true);
//
                Handler handler1 = new Handler();
                handler1.postDelayed(new Runnable() {
                    public void run() {

                        loginBtn.setVisibility(View.VISIBLE);
                        arrowBtn.bringToFront();
                        signUpTxt.bringToFront();
                        forgotPassTxt.bringToFront();
                    }
                }, 300);

                return true;
            }

            if (event.getX() - motionEvent.getX() > 50) {

//            Toast.makeText(LoginScreen.this , " Swipe Left " , Toast.LENGTH_LONG).show();

                return true;
            }

            if (motionEvent.getX() - event.getX() > 50) {

//            Toast.makeText(LoginScreen.this, " Swipe Right ", Toast.LENGTH_LONG).show();

                return true;
            } else {

                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }

    }

    @Override
    public void onLongPress(MotionEvent arg0) {

        // TODO Auto-generated method stub

    }

    @Override
    public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {

        // TODO Auto-generated method stub

        return false;
    }

    @Override
    public void onShowPress(MotionEvent arg0) {

        // TODO Auto-generated method stub

    }

    @Override
    public boolean onSingleTapUp(MotionEvent arg0) {

        // TODO Auto-generated method stub

        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {

        // TODO Auto-generated method stub

        return gestureDetector.onTouchEvent(motionEvent);
    }

    @Override
    public boolean onDown(MotionEvent arg0) {

        // TODO Auto-generated method stub

        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBackPressed() {

        if (isOpen) {

            backPressed = true;

            viewMenu(true);
//
            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {

                    loginBtn.setVisibility(View.VISIBLE);
                    arrowBtn.bringToFront();
                    signUpTxt.bringToFront();
                    forgotPassTxt.bringToFront();
                    isOpen = false;
                    backPressed = false;
                }
            }, 300);

        }

        if (!backPressed) {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            } else {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_LONG).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        }
    }

//    public void showIndication() {
//
//        linearLayout.setVisibility(View.VISIBLE);
//        linearLayout.bringToFront();
//        avi.bringToFront();
//    }

    public void disableButton() {

        loginBtn.setEnabled(false);
        forgotPassTxt.setEnabled(false);
        signUpTxt.setEnabled(false);
        arrowBtn.setEnabled(false);
        editUserID.setFocusable(false);

    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.CAMERA);
        int readStoragePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStoragePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int accessfineLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int accessCoarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int readContacts = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
        int writeContacts = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }
        if (writeStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (accessfineLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (accessCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (readContacts != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }
        if (writeContacts != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_CONTACTS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        Log.d("Login", "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_CONTACTS, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Login", "sms & location services permission granted");
//                        permissionUtils = new PermissionUtils(LoginScreen.this);
//
//                        permissions1.add(Manifest.permission.ACCESS_FINE_LOCATION);
//                        permissions1.add(Manifest.permission.ACCESS_COARSE_LOCATION);
//
//                        permissionUtils.check_permission(permissions1, "Need GPS permission for getting your location", 1);

//                        locationHelper = new LocationHelper(LoginScreen.this);
//                        locationHelper.checkpermission();

                        if (checkPlayServices()) {

                            // Building the GoogleApi client
                            buildGoogleApiClient();
                            startLocationService();
                        }

//                        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
//
//                        if (mapFragment != null) {
//                            mapFragment.getMapAsync(this);
//
//                        }
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("Login", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_CONTACTS) || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {
                            showDialogOK("Camera and Read/Write Storage Services Permission required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(this, "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    public void Status100() {

        Intent intent = new Intent(LoginScreen.this, LoginWithPassAndPinScreen.class);
        intent.putExtra("userName", userName);
        intent.putExtra("Type", "NonSamba");
        intent.putExtra("password", pass);
        dialog.dismiss();
        editUserID.setText("");
        editPassword.setText("");
        overridePendingTransition(0, 0);
        startActivity(intent);
        loginBtn.setEnabled(true);
        editUserID.getText().clear();
        isLogin = true;
        finish();
    }

    public void Status00() {

        Intent intent = new Intent(LoginScreen.this, LoginWithPassAndPinScreen.class);
        intent.putExtra("userName", userName);
        intent.putExtra("Type", "Samba");
        intent.putExtra("Is_Locked", "Y");
        intent.putExtra("password", pass);
        editUserID.setText("");
        editPassword.setText("");
        dialog.dismiss();
        overridePendingTransition(0, 0);
        startActivity(intent);
        loginBtn.setEnabled(true);
        editUserID.getText().clear();
        isLogin = false;
        finish();
    }

    public void Status() {

        Intent intent = new Intent(LoginScreen.this, LoginWithPassAndPinScreen.class);
        intent.putExtra("userName", userName);
        intent.putExtra("Type", "Samba");
        intent.putExtra("Is_Locked", "N");
        intent.putExtra("password", pass);
        dialog.dismiss();
        editUserID.setText("");
        editPassword.setText("");
        overridePendingTransition(0, 0);
        startActivity(intent);
        loginBtn.setEnabled(true);
        editUserID.getText().clear();
        isLogin = false;
        finish();
    }

    public void Error() {

        Intent intent = new Intent(LoginScreen.this, LoginWithPassAndPinScreen.class);
        dialog.dismiss();
        intent.putExtra("userName", userName);
        intent.putExtra("Type", "Error");
        intent.putExtra("password", pass);
        loginBtn.setEnabled(true);
        editUserID.setText("");
        editPassword.setText("");
        overridePendingTransition(0, 0);
        startActivity(intent);
        editUserID.getText().clear();
        isLogin = false;
        finish();
    }

    public void SignUp() {

        Intent intent = new Intent(LoginScreen.this, TermsNConditionsScreen.class);
        intent.putExtra("signup", "signup");
        overridePendingTransition(0, 0);
        startActivity(intent);
        editUserID.setText("");
        editPassword.setText("");
        finish();
        editUserID.getText().clear();
        errorMessage.setVisibility(View.GONE);

    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (checkAndRequestPermissions()) {
//
//        }
//    }

    public void ForgotPassword() {

        Intent intent = new Intent(LoginScreen.this, TermsNConditionsScreen.class);
        intent.putExtra("FP", "FP");
        overridePendingTransition(0, 0);
        startActivity(intent);
        editUserID.setText("");
        editPassword.setText("");
        finish();
        editUserID.getText().clear();
        errorMessage.setVisibility(View.GONE);
    }

    @Override
    public void onSupportActionModeStarted(ActionMode mode) {
        super.onSupportActionModeStarted(mode);
        mode.finish();
    }

    public void showDilogForError(String msg, String header) {

        dialog1 = new Dialog(LoginScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        textView = dialog1.findViewById(R.id.validatingTxt);
        doneBtn = dialog1.findViewById(R.id.doneBtn);
        heading = dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
                ForgotPassword();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog1.show();

    }

    public void showDilogForFinger() {

        dialog1 = new Dialog(LoginScreen.this);

        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog1.setContentView(R.layout.finger_dialog);
//        View view = dialog.findViewById(R.id.content);
        fingerPrintIcon = dialog1.findViewById(R.id.fingerprint_icon);
        fingerStatus = dialog1.findViewById(R.id.validatingTxt);
        second_dialog_button = dialog1.findViewById(R.id.doneBtn);

        second_dialog_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
                dialog.dismiss();
                isFingerLogin = false;
            }
        });
//        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
//        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
//        heading = (TextView) dialog.findViewById(R.id.heading);
//        dialog.setCancelable(false);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        doneBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                dialog.dismiss();
//                ForgotPassword();
//            }
//        });
//        textView.setText(msg);
//        heading.setText(header);
        dialog1.show();

        resetStatusText();
        authenticateUserFingerprint();
    }

    public void showDilogForErrorForFinger(String msg, String header) {

        try {
            dialogForFinger = new Dialog(LoginScreen.this);
            dialogForFinger.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogForFinger.setContentView(R.layout.error_dialog);
            textView = dialogForFinger.findViewById(R.id.validatingTxt);
            doneBtn = dialogForFinger.findViewById(R.id.doneBtn);
            heading = dialogForFinger.findViewById(R.id.heading);
            dialogForFinger.setCancelable(false);
            dialogForFinger.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    constants.isFingerCheckEditor.putString("isCheck", "True");
                    constants.isFingerCheckEditor.commit();
                    constants.isFingerCheckEditor.apply();
                    dialogForFinger.dismiss();

                }
            });
            textView.setText(msg);
            heading.setText(header);
            dialogForFinger.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void showDilogForErrorForLogin(String msg, String header) {

        dialog1 = new Dialog(LoginScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        message = dialog1.findViewById(R.id.validatingTxt);
        doneBtn = dialog1.findViewById(R.id.doneBtn);
        heading = dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog1.dismiss();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog1.show();


    }

    public void isFirstLogin(JSONObject jsonObject) {

        try {
            Intent intent;
            if (jsonObject.getString("SecurityAlert").toUpperCase().equals("P")) {

                intent = new Intent(LoginScreen.this, ConfirmPasscodeScreen.class);
                intent.putExtra("securityCheck", "passcode");
                intent.putExtra("firstLogin", "First");
                if (jsonObject.has("isRooted"))
                    intent.putExtra("isRooted", jsonObject.getString("RootedDeviceTerms"));
                if (jsonObject.has("NotificationEnable"))
                    intent.putExtra("isNotification", jsonObject.getString("NotificationEnable"));

            } else if (jsonObject.getString("SecurityAlert").toUpperCase().equals("Q")) {

                intent = new Intent(LoginScreen.this, SecurityQuestionScreen.class);
                intent.putExtra("securityCheck", "securityQuestion");
                intent.putExtra("firstLogin", "First");
                if (jsonObject.has("isRooted"))
                    intent.putExtra("isRooted", jsonObject.getString("RootedDeviceTerms"));
                if (jsonObject.has("NotificationEnable"))
                    intent.putExtra("isNotification", jsonObject.getString("NotificationEnable"));

            } else {

//            Toast.makeText(LoginWithPassAndPinScreen.this, "Validate Successfully.", Toast.LENGTH_LONG).show();
//            Intent intent = new Intent(LoginScreen.this, NotificationScreen.class);

//            Toast.makeText(LoginWithPassAndPinScreen.this, "Validate Successfully.", Toast.LENGTH_LONG).show();
//            Intent intent = new Intent(LoginScreen.this, NotificationScreen.class);
                if (jsonObject.getString("RootedDeviceTerms").equals("N"))
                    intent = new Intent(LoginScreen.this, TermsNConditionsScreen.class);
                else if (jsonObject.getString("NotificationEnable").toUpperCase().equals("TRUE")) {

                    intent = new Intent(LoginScreen.this, ProfileSetupScreen.class);
                    intent.putExtra("isFirst", "Yes");

                } else {
                    intent = new Intent(LoginScreen.this, NotificationScreen.class);

                }

            }

            dialog.dismiss();
            CryptLib _crypt = new CryptLib();
            constants.editor.putString("T24_FullName", _crypt.encryptForParams(jsonObject.getString("FullName"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("T24_CIF", _crypt.encryptForParams(jsonObject.getString("T24_CIF"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("LegalID", _crypt.encryptForParams(jsonObject.getString("LegalID"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("LegalType", _crypt.encryptForParams(jsonObject.getString("LegalType"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("T24_Target", _crypt.encryptForParams(jsonObject.getString("T24_Target"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("EmailAddress", _crypt.encryptForParams(jsonObject.getString("EmailAddress"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("MobileNumber", _crypt.encryptForParams(jsonObject.getString("MobileNumber"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("ProfilePic", _crypt.encryptForParams(jsonObject.getString("ProfileAvatar").replaceAll("@PLUS@", "+"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("UserProgress", _crypt.encryptForParams(jsonObject.getString("UserProgress"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("UserAddress", _crypt.encryptForParams(jsonObject.getString("UserAddress"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("SessionID", _crypt.encryptForParams(jsonObject.getString("SessionID"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("NotificationEnable", _crypt.encryptForParams(jsonObject.getString("NotificationEnable"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams(jsonObject.getString("FingerPrintEnabled"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("SecurityAlert", _crypt.encryptForParams(jsonObject.getString("SecurityAlert"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            if (jsonObject.has("LastLoginLocation"))
                constants.editor.putString("LastLoginLocation", _crypt.encryptForParams(jsonObject.getString("LastLoginLocation"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));

            if (!isFinger) {
                constants.editor.putString("userName", _crypt.encryptForParams(userName, Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                constants.editor.putString("Finger", "False");
            } else {

                constants.editor.putString("Finger", "True");
                constants.editor.putString("userName", _crypt.encryptForParams(constants.fingerPrint.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            }
            constants.editor.putString("Menus", menus);
            if (!TextUtils.isEmpty(jsonObject.getString("T24_CIF"))) {

                constants.editor.putString("type", _crypt.encryptForParams("SAMBA", Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            } else {

                constants.editor.putString("type", _crypt.encryptForParams("NON_SAMBA", Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            }
            constants.editor.commit();
            constants.editor.apply();
//            dialog.dismiss();
            intent.putExtra("username", userName);
            intent.putExtra("login", "login");
            intent.putExtra("isFirstLogin", "Yes");
            Constants.Shared_KEY = jsonObject.getString("SessionID");
            startActivity(intent);
            finish();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void isDeviceExist(JSONObject jsonObject) {

        try {
            Intent intent;
            if (jsonObject.getString("SecurityAlert").toUpperCase().equals("P")) {

                intent = new Intent(LoginScreen.this, ConfirmPasscodeScreen.class);
                intent.putExtra("securityCheck", "passcode");
                intent.putExtra("firstLogin", "No");
                if (jsonObject.has("isRooted"))
                    intent.putExtra("isRooted", jsonObject.getString("RootedDeviceTerms"));
                if (jsonObject.has("NotificationEnable"))
                    intent.putExtra("isNotification", jsonObject.getString("NotificationEnable"));

            } else if (jsonObject.getString("SecurityAlert").toUpperCase().equals("Q")) {

                intent = new Intent(LoginScreen.this, SecurityQuestionScreen.class);
                intent.putExtra("securityCheck", "securityQuestion");
                intent.putExtra("firstLogin", "No");
                if (jsonObject.has("isRooted"))
                    intent.putExtra("isRooted", jsonObject.getString("RootedDeviceTerms"));
                if (jsonObject.has("NotificationEnable"))
                    intent.putExtra("isNotification", jsonObject.getString("NotificationEnable"));

            } else {

//            Toast.makeText(LoginWithPassAndPinScreen.this, "Validate Successfully.", Toast.LENGTH_LONG).show();
//            Intent intent = new Intent(LoginScreen.this, NotificationScreen.class);
                if (jsonObject.getString("RootedDeviceTerms").equals("N"))
                    intent = new Intent(LoginScreen.this, TermsNConditionsScreen.class);
                else
                    intent = new Intent(LoginScreen.this, NotificationScreen.class);

            }

            dialog.dismiss();
            CryptLib _crypt = new CryptLib();
            constants.editor.putString("T24_FullName", _crypt.encryptForParams(jsonObject.getString("FullName"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("T24_CIF", _crypt.encryptForParams(jsonObject.getString("T24_CIF"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("LegalID", _crypt.encryptForParams(jsonObject.getString("LegalID"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("LegalType", _crypt.encryptForParams(jsonObject.getString("LegalType"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("T24_Target", _crypt.encryptForParams(jsonObject.getString("T24_Target"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("EmailAddress", _crypt.encryptForParams(jsonObject.getString("EmailAddress"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("MobileNumber", _crypt.encryptForParams(jsonObject.getString("MobileNumber"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("ProfilePic", _crypt.encryptForParams(jsonObject.getString("ProfileAvatar").replaceAll("@PLUS@", "+"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("UserProgress", _crypt.encryptForParams(jsonObject.getString("UserProgress"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("UserAddress", _crypt.encryptForParams(jsonObject.getString("UserAddress"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("SessionID", _crypt.encryptForParams(jsonObject.getString("SessionID"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("NotificationEnable", _crypt.encryptForParams(jsonObject.getString("NotificationEnable"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams(jsonObject.getString("FingerPrintEnabled"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("SecurityAlert", _crypt.encryptForParams(jsonObject.getString("SecurityAlert"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            if (jsonObject.has("LastLoginLocation"))
                constants.editor.putString("LastLoginLocation", _crypt.encryptForParams(jsonObject.getString("LastLoginLocation"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));

            if (!isFinger) {
                constants.editor.putString("userName", _crypt.encryptForParams(userName, Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                constants.editor.putString("Finger", "False");
            } else {

                constants.editor.putString("Finger", "True");
                constants.editor.putString("userName", _crypt.encryptForParams(constants.fingerPrint.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            }
            constants.editor.putString("Menus", menus);
            if (!TextUtils.isEmpty(jsonObject.getString("T24_CIF"))) {

                constants.editor.putString("type", _crypt.encryptForParams("SAMBA", Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            } else {

                constants.editor.putString("type", _crypt.encryptForParams("NON_SAMBA", Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            }
            constants.editor.commit();
            constants.editor.apply();
            intent.putExtra("username", userName);
            intent.putExtra("login", "login");
            intent.putExtra("isFirstLogin", "No");
            Constants.Shared_KEY = jsonObject.getString("SessionID");
            startActivity(intent);
            finish();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void DeviceAlreadyExist(JSONObject jsonObject) {

        try {

            Intent intent;
            if (jsonObject.getString("SecurityAlert").toUpperCase().equals("P")) {

                intent = new Intent(LoginScreen.this, ConfirmPasscodeScreen.class);
                intent.putExtra("securityCheck", "passcode");
                intent.putExtra("firstLogin", "Never");
                if (jsonObject.has("isRooted"))
                    intent.putExtra("isRooted", jsonObject.getString("RootedDeviceTerms"));
                if (jsonObject.has("NotificationEnable"))
                    intent.putExtra("isNotification", jsonObject.getString("NotificationEnable"));

            } else if (jsonObject.getString("SecurityAlert").toUpperCase().equals("Q")) {

                intent = new Intent(LoginScreen.this, SecurityQuestionScreen.class);
                intent.putExtra("securityCheck", "securityQuestion");
                intent.putExtra("firstLogin", "Never");
                if (jsonObject.has("isRooted"))
                    intent.putExtra("isRooted", jsonObject.getString("RootedDeviceTerms"));
                if (jsonObject.has("NotificationEnable"))
                    intent.putExtra("isNotification", jsonObject.getString("NotificationEnable"));

            } else {
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Validate Successfully.", Toast.LENGTH_LONG).show();

                intent = new Intent(LoginScreen.this, HomeScreen.class);


            }

            dialog.dismiss();
            CryptLib _crypt = new CryptLib();
            constants.editor.putString("T24_FullName", _crypt.encryptForParams(jsonObject.getString("FullName"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("T24_CIF", _crypt.encryptForParams(jsonObject.getString("T24_CIF"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("LegalID", _crypt.encryptForParams(jsonObject.getString("LegalID"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("LegalType", _crypt.encryptForParams(jsonObject.getString("LegalType"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("T24_Target", _crypt.encryptForParams(jsonObject.getString("T24_Target"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("EmailAddress", _crypt.encryptForParams(jsonObject.getString("EmailAddress"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("MobileNumber", _crypt.encryptForParams(jsonObject.getString("MobileNumber"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("ProfilePic", _crypt.encryptForParams(jsonObject.getString("ProfileAvatar").replaceAll("@PLUS@", "+"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("UserProgress", _crypt.encryptForParams(jsonObject.getString("UserProgress"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("UserAddress", _crypt.encryptForParams(jsonObject.getString("UserAddress"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("SessionID", _crypt.encryptForParams(jsonObject.getString("SessionID"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("NotificationEnable", _crypt.encryptForParams(jsonObject.getString("NotificationEnable"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams(jsonObject.getString("FingerPrintEnabled"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            constants.editor.putString("SecurityAlert", _crypt.encryptForParams(jsonObject.getString("SecurityAlert"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            if (jsonObject.has("LastLoginLocation"))
                constants.editor.putString("LastLoginLocation", _crypt.encryptForParams(jsonObject.getString("LastLoginLocation"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));

            if (!isFinger) {
                constants.editor.putString("userName", _crypt.encryptForParams(userName, Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                constants.editor.putString("Finger", "False");
            } else {

                constants.editor.putString("Finger", "True");
                constants.editor.putString("userName", _crypt.encryptForParams(constants.fingerPrint.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            }
            constants.editor.putString("Menus", menus);
            if (!TextUtils.isEmpty(jsonObject.getString("T24_CIF"))) {

                constants.editor.putString("type", _crypt.encryptForParams("SAMBA", Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            } else {

                constants.editor.putString("type", _crypt.encryptForParams("NON_SAMBA", Constants.getDeviceMac(), Constants.getDeviceIMEI()));
            }
            constants.editor.commit();
            constants.editor.apply();
            Constants.Shared_KEY = jsonObject.getString("SessionID");
            startActivity(intent);
            finish();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void startLocationService() {

        try {


            Intent serviceIntent = new Intent(this, LocationService.class);
            startService(serviceIntent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void PermissionGranted(int request_code) {
        Log.i("PERMISSION", "GRANTED");
        isPermissionGranted = true;
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        Log.i("PERMISSION PARTIALLY", "GRANTED");
    }

    @Override
    public void PermissionDenied(int request_code) {
        Log.i("PERMISSION", "DENIED");
    }

    @Override
    public void NeverAskAgain(int request_code) {
        Log.i("PERMISSION", "NEVER ASK AGAIN");
    }

    public void CheckVersion() {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    final String md5 = Helper.md5("-" + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", "-");
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Old_Shared_Key);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sDevice", "ANDROID");
                    CryptLib _crypt = new CryptLib();
                    String data = Helper.convertMapToString(params);
                    requestBody = "sData=" + _crypt.encrypt(data, Constants.getDeviceMac(), Constants.getDeviceIMEI());
//                    requestBody = requestBody.replaceAll("\\+","@PLUS@");
                    //                    final String requestBody = stringBuilder.toString();

                    HttpsTrustManager.allowMySSL(LoginScreen.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.verisonURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();

                                        if (jsonObject.getString("ServerKey").equals(md5)) {

                                            if (jsonObject.getJSONObject("Signs_ValidateAppVersionResult").getString("Status_Code").equals("00")) {


                                            } else if (jsonObject.getJSONObject("Signs_ValidateAppVersionResult").getString("Status_Code").equals("54")) {

//                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_ValidateAppVersionResult").getString("Status_Description"), "ERROR");

                                                showDialogForUpdate(jsonObject.getJSONObject("Signs_ValidateAppVersionResult").getString("Status_Description"));

                                            } else {

//                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_ValidateAppVersionResult").getString("Status_Description"), "ERROR");
                                                showDialogForUpdate(jsonObject.getJSONObject("Signs_ValidateAppVersionResult").getString("Status_Description"));

                                            }
                                        } else {

                                            try {


                                                utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }) {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };
//                    ) {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", "-");
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", constants.Old_Shared_Key);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sDevice", "ANDROID");
//                            return params;
//                        }
//                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();

        }
    }

    private void showDialogForUpdate(String message) {
        try {


            final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setTitle("Alert:");
            builder.setMessage(message);
            builder.setCancelable(false);
            builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        anfe.printStackTrace();
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }

                    finish();
//                finishAffinity();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    pd.dismiss();
                    finish();
                }
            });

            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDilogForErrorForRoot(String msg, String header) {

        dialog = new Dialog(LoginScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = dialog.findViewById(R.id.validatingTxt);
        doneBtn = dialog.findViewById(R.id.doneBtn);
        heading = dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                finish();
                finishAffinity();

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    private class LocationReceiver extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {


            if (null != intent && intent.getAction().equals(LOACTION_ACTION)) {

                String locationData = intent.getStringExtra(LOCATION_MESSAGE);

//                tvLocationData.setText(locationData);
            }

        }
    }

    private class AsyncCaller extends AsyncTask<String, Void, String> {

        String value;
        double lati, longi;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

//            pd.show();

        }

        @Override
        protected String doInBackground(String... params) {

            //this method will be running on a background thread so don't update UI from here
            //do your long-running http tasks here, you don't want to pass argument and u can access the parent class' variable url over here
            try {


                lati = Double.valueOf(params[0]);
                longi = Double.valueOf(params[1]);
                value = params[2];
                addresses = geocoder.getFromLocation(lati, longi, 1);
            } catch (Exception e) {

                e.printStackTrace();
//                location = "-";
//                if (value.equals("login"))
//                    Login();
//                else if (value.equals("fingerprint"))
//                    LoginFingerPrint();
//                else if (value.equals("forgot"))
//                    ForgotPassword();
//                else if (value.equals("signup"))
//                    SignUp();
            }

            if (addresses != null)
                return addresses.get(0).toString();
            else
                return "null";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result != null) {
                if (!TextUtils.isEmpty(result) && !result.equals("null") && !result.equals(null)) {


                    Address locationAddress = addresses.get(0);
                    location = locationAddress.getAddressLine(0);

                    if (value.equals("login"))
                        Login();
                    else if (value.equals("fingerprint"))
                        LoginFingerPrint();
                    else if (value.equals("forgot"))
                        ForgotPassword();
                    else if (value.equals("signup"))
                        SignUp();

                } else {
//                                utils.showDilogForError("Please turn on location services.", "WARNING");
//                                loginBtn.setEnabled(true);
                    location = "-";
                    if (value.equals("login"))
                        Login();
                    else if (value.equals("fingerprint"))
                        LoginFingerPrint();
                    else if (value.equals("forgot"))
                        ForgotPassword();
                    else if (value.equals("signup"))
                        SignUp();
                }
            } else {

                location = "-";
                if (value.equals("login"))
                    Login();
                else if (value.equals("fingerprint"))
                    LoginFingerPrint();
                else if (value.equals("forgot"))
                    ForgotPassword();
                else if (value.equals("signup"))
                    SignUp();
            }
            //this method will be running on UI thread


        }

    }
}

