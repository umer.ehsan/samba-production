package com.ceesolutions.samba.accessControl;

/**
 * Created by ayaz on 12/18/17.
 */

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.utils.PopupDialogPermission;

public class NotificationScreen extends AppCompatActivity {

    private PopupDialogPermission popupDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        getSupportActionBar().hide();

        popupDialog = new PopupDialogPermission();
        popupDialog.setCancelable(false);
        popupDialog.show(getSupportFragmentManager(),"dialog");

    }

    @Override
    public void onBackPressed() {
        //Do Nothing
    }
}
