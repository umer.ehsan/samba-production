package com.ceesolutions.samba.accessControl.Fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.accessControl.ProfileSetupScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ceeayaz on 2/21/18.
 */

public class ProfilePictureFragment extends Fragment {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public static double latitude;
    public static double longitude;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public String longitude1;
    public String latitude1;
    String imgString;
    CircleImageView circleImageView;
    String userName, legalID;
    int maxHeight = 160;
    //choose a max width
    int maxWidth = 160;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private TextView validateBtn, heading, textView, doneBtn;
    private Bitmap bitmap;
    private ImageView imageBtn;
    private Dialog dialog;
    private ImageView backBtn;
    private TextView headingTxt;
    private Utils utils;
    private AppPreferences appPreferences;
    private String location;

    public static Bitmap decodeUri(Context c, Uri uri, final int requiredSize)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.set_profile_picture, container, false);

        initViews(convertView);

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                    longitude = Double.valueOf(longitude1);
                    latitude = Double.valueOf(latitude1);
                    if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                    } else {
                        location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                    }
                } else {

                    longitude = 0;
                    latitude = 0;
                    location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
                }

                if (circleImageView.getDrawable() == null) {

                    showDilogForError("You haven't selected a profile picture. Are you sure you want to skip?", "WARNING");
                } else
                    SetProfileImage();
            }
        });


        imageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectImage();

            }
        });

        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
            }
        });
//
//        headingTxt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                ProfileSetupScreen.currentItem = 0;
//                Intent intent = new Intent(getActivity(), HomeScreen.class);
//                startActivity(intent);
//                getActivity().finish();
//                validateBtn.setEnabled(true);
//
//            }
//        });
//


        return convertView;
    }

    public void initViews(View convertView) {

        try {

            CryptLib _crypt = new CryptLib();
            ProfileSetupScreen activity = (ProfileSetupScreen) getActivity();
            backBtn = (ImageView) activity.findViewById(R.id.backBtn);
            headingTxt = (TextView) activity.findViewById(R.id.headingTxt);
            utils = new Utils(getActivity());
            validateBtn = (TextView) convertView.findViewById(R.id.validateBtn);
            requestQueue = Volley.newRequestQueue(getActivity());
            webService = new WebService();
            helper = Helper.getHelper(getActivity());
            constants = Constants.getConstants(getActivity());
            pd = new Dialog(getActivity());
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            userName = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            legalID = _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            circleImageView = (CircleImageView) convertView.findViewById(R.id.profile_image);
            imageBtn = (ImageView) convertView.findViewById(R.id.imageBtn);
            appPreferences = new AppPreferences(getActivity());
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void selectImage() {
        try {
            PackageManager pm = getActivity().getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getActivity().getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_LONG).show();
            checkAndRequestPermissions();
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_LONG).show();
            checkAndRequestPermissions();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
                int width = circleImageView.getWidth();
                int height = circleImageView.getHeight();
//                circleImageView.setImageBitmap(getCroppedBitmap(bitmap));
                RequestOptions options = new RequestOptions();
                options.centerInside();
                options.override(width, height);
                options.placeholder(R.drawable.ic_launcher_background);
                Glide.with(getActivity())
                        .load((bitmap))
                        .apply(options)
                        .into(circleImageView);
                final int bmOriginalWidth = bitmap.getWidth();
                final int bmOriginalHeight = bitmap.getHeight();
                final double originalWidthToHeightRatio = 1.0 * bmOriginalWidth / bmOriginalHeight;
                final double originalHeightToWidthRatio = 1.0 * bmOriginalHeight / bmOriginalWidth;
                Log.e("Activity", "Pick from Gallery::>>> ");
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
//                        bitmap = getResizedBitmap(helper.getScaledBitmap(bitmap, bmOriginalWidth, bmOriginalHeight,
//                                originalWidthToHeightRatio, originalHeightToWidthRatio,
//                                200, 200), 400);
                        bitmap = getCroppedBitmap(bitmap);
                    }
                }, 100);



                Log.e("Activity", "Pick from Camera::>>> ");
                imageBtn.setVisibility(View.INVISIBLE);
                circleImageView.setVisibility(View.VISIBLE);

                imgString = Base64.encodeToString(getBytesFromBitmap((bitmap)),
                        Base64.NO_WRAP);
                deleteLastPhotoFromGallery();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {

            try {
                Uri selectedImage = data.getData();
                Log.d("File", "---" + selectedImage.getPath());
                File file = new File(getRealPathFromURI(selectedImage));
                Log.d("File", "---" + file.length());
                long fileSizeInKB = file.length() / 1024;
                long fileSizeInMB = fileSizeInKB / 1024;

                if (fileSizeInMB > 4) {

                    utils.showDilogForError("You cannot upload file greater than 4MB.", "ERROR");

                } else {

                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
                    imageBtn.setVisibility(View.INVISIBLE);
                    circleImageView.setVisibility(View.VISIBLE);
                    int width = circleImageView.getWidth();
                    int height = circleImageView.getHeight();
//                circleImageView.setImageBitmap(getCroppedBitmap(bitmap));
                    RequestOptions options = new RequestOptions();
                    options.centerInside();
                    options.override(width, height);
                    options.placeholder(R.drawable.ic_launcher_background);
                    Glide.with(getActivity())
                            .load((bitmap))
                            .apply(options)
                            .into(circleImageView);
                    final int bmOriginalWidth = bitmap.getWidth();
                    final int bmOriginalHeight = bitmap.getHeight();
                    final double originalWidthToHeightRatio = 1.0 * bmOriginalWidth / bmOriginalHeight;
                    final double originalHeightToWidthRatio = 1.0 * bmOriginalHeight / bmOriginalWidth;
                    Log.e("Activity", "Pick from Gallery::>>> ");
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
//                            bitmap = getResizedBitmap(helper.getScaledBitmap(bitmap, bmOriginalWidth, bmOriginalHeight,
//                                    originalWidthToHeightRatio, originalHeightToWidthRatio,
//                                    200, 200), 400);
                            bitmap = getCroppedBitmap(getResizedBitmap(bitmap, 400));
                            imgString = Base64.encodeToString(getBytesFromBitmap((bitmap)),
                                    Base64.NO_WRAP);
                        }
                    }, 100);

                    Log.e("Activity", "Pick from Gallery::>>> ");

//                    new AsyncCaller().execute(bitmap);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    public void SetProfileImage() {

//        Toast.makeText(RegistrationScreen.getActivity(), "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.SetProfileImage(userName, legalID, imgString, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getSetImageReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Information is not valid!", Toast.LENGTH_LONG).show();
//
//                                pd.dismiss();
//                                utils.showDilogForError("Profile Picture can not be set. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
//
////                                        Toast.makeText(getActivity(), "Profile Picture Saved successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        ProfileSetupScreen.currentItem = 0;
//                                        Intent intent = new Intent(getActivity(), HomeScreen.class);
//                                        startActivity(intent);
//                                        getActivity().finish();
//                                        validateBtn.setEnabled(true);
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Profile Picture can not be set. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Profile Picture can not be set. Please try again later.", "ERROR");
//                                        validateBtn.setEnabled(true);
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Profile Picture can not be set. Please try again later.", "ERROR");
//                                    validateBtn.setEnabled(true);
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Profile Picture can not be set. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Profile Picture can not be set. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//                        }
//                    }
//                }, 5000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", userName);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sLegalIdentityValue", legalID);
                params.put("sBase64Image", imgString.replaceAll("\\+", "@PLUS@"));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(getActivity());
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.setProfileImageURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        try {

                                            CryptLib _crypt = new CryptLib();
                                            if (jsonObject.getJSONObject("Signs_SetProfileAvatarResult").getString("Status_Code").equals("00")) {
                                                pd.dismiss();
                                                ProfileSetupScreen.currentItem = 0;
                                                Intent intent = new Intent(getActivity(), HomeScreen.class);
                                                Intent intent1 = getActivity().getIntent();
                                                intent.putExtra("isFirst",intent1.getStringExtra("isFirst"));
                                                startActivity(intent);
                                                getActivity().finish();
                                                validateBtn.setEnabled(true);
                                                constants.editor.putString("ProfilePic", _crypt.encryptForParams(imgString, constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.editor.putString("UserProgress", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_SetProfileAvatarResult").getString("UserProgress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.editor.commit();
                                                constants.editor.apply();

                                            } else if (jsonObject.getJSONObject("Signs_SetProfileAvatarResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_SetProfileAvatarResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());
                                                validateBtn.setEnabled(true);

                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_SetProfileAvatarResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Profile Picture can not be set. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Profile Picture can not be set. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Profile Picture can not be set. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", userName);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLegalIdentityValue", legalID);
//                        params.put("sBase64Image", imgString);
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Profile Picture can not be set. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
        }
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.CAMERA);
        int readStoragePermission = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStoragePermission = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }
        if (writeStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("Login", "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Login", "sms & location services permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("Login", "Some permissions are not granted ask again ");
                        //permission is denied (getActivity() is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            showDialogOK("Camera and Read/Write Storage Services Permission required for getActivity() app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(getActivity(), "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    private void deleteLastPhotoFromGallery() {

        String[] projection = new String[]{
                MediaStore.Images.ImageColumns._ID,
                MediaStore.Images.ImageColumns.DATA,
                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATE_TAKEN,
                MediaStore.Images.ImageColumns.MIME_TYPE};

        final Cursor cursor = getActivity().getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
                null, null, MediaStore.Images.ImageColumns.DATE_TAKEN + "DESC");

        if (cursor != null) {
            cursor.moveToFirst();

            int column_index_data =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            String image_path = cursor.getString(column_index_data);

            File file = new File(image_path);
            if (file.exists()) {
                file.delete();
            }
        }
    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
//        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                ProfileSetupScreen.currentItem = 0;
                Intent intent = new Intent(getActivity(), HomeScreen.class);
                Intent intent1 = getActivity().getIntent();
                intent.putExtra("isFirst",intent1.getStringExtra("isFirst"));
                startActivity(intent);
                getActivity().finish();
                validateBtn.setEnabled(true);
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getActivity().getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    private class AsyncCaller extends AsyncTask<Bitmap, Void, String> {

        public Bitmap bitmap1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

//            pd.show();

        }

        @Override
        protected String doInBackground(Bitmap... params) {

            //this method will be running on a background thread so don't update UI from here
            //do your long-running http tasks here, you don't want to pass argument and u can access the parent class' variable url over here
            try {

                bitmap1 = params[0];
                imgString = Base64.encodeToString(getBytesFromBitmap(bitmap1),
                        Base64.NO_WRAP);
            } catch (Exception e) {

                e.printStackTrace();

            }

            return imgString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


        }

    }
}