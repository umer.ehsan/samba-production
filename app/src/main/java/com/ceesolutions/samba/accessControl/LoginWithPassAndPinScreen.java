package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.view.ActionMode;

import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.PinView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ayaz on 12/26/17.
 */

public class LoginWithPassAndPinScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private ImageView sambaIcon, signsLogo;
    private TextView errorMessageiPIN, validateBtn, regenerateBtn, text2, message, doneBtn, heading;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private ProgressDialog pd;
    private String iPIN, pass, userName, T24_FullName, T24_CIF, T24_MobileNumber, T24_Target, isType, is_Locked;
    private Constants constants;
    private String fcmToken;
    private Dialog dialog;
    private ImageView backBtn;
    private Utils utils;
    private PinView pinView;
    private ArrayList<String> menu;
    private String[] separated;
    private String menus;
    private AppPreferences appPreferences;
    private String location;
    private List<Address> addresses;
    private Geocoder geocoder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_password_pin);
        initViews();
        getSupportActionBar().hide();
        InitPinView();
        pinView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessageiPIN.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessageiPIN.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() == 6) {

                    iPIN = s.toString();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(pinView.getWindowToken(), 0);

                } else {

                    iPIN = s.toString();
                    // Do Nothing
                }
            }
        });

    }


    public void initViews() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                fcmToken = constants.fcmSharedPref.getString("fcmToken", "N/A");
                Log.d("Fcm", "---->" + fcmToken);
            }

        }, 1500);
        menu = new ArrayList<>();
        Intent intent = getIntent();
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        userName = intent.getStringExtra("userName");
        isType = intent.getStringExtra("Type");
        is_Locked = intent.getStringExtra("Is_Locked");
        pass = intent.getStringExtra("password");
        text2 = (TextView) findViewById(R.id.text2);
        validateBtn = (TextView) findViewById(R.id.validateBtn);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        regenerateBtn = (TextView) findViewById(R.id.regenerateBtn);
        sambaIcon = (ImageView) findViewById(R.id.sambaIcon);
        signsLogo = (ImageView) findViewById(R.id.signsLogo);
        errorMessageiPIN = (TextView) findViewById(R.id.errorMessagePinCode);
        requestQueue = Volley.newRequestQueue(this);
//        webService = new WebService();
        helper = Helper.getHelper(this);
        pd = new ProgressDialog(LoginWithPassAndPinScreen.this);
        constants = Constants.getConstants(LoginWithPassAndPinScreen.this);
        utils = new Utils(LoginWithPassAndPinScreen.this);
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        String txt = "<font COLOR=\'#353535\'>" + "Your iPIN against " + "</font>"
                + "<font COLOR=\'#034ea2\'><b>" + userName + "</b></font>" + "<font COLOR=\'#353535\'>" + " has been sent on your registered mobile number & email. This iPIN is valid for an entire session." + "</font>";
        text2.setText(Html.fromHtml(txt));
        geocoder = new Geocoder(this, Locale.getDefault());
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginWithPassAndPinScreen.this, LoginScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
            }
        });


        pinView = (PinView) findViewById(R.id.pinView);

        pinView.setTextColor(
                ResourcesCompat.getColor(getResources(), R.color.colorAccent, getTheme()));
        pinView.setTextColor(
                ResourcesCompat.getColorStateList(getResources(), R.color.colorPrimary, getTheme()));
        pinView.setLineColor(
                ResourcesCompat.getColor(getResources(), R.color.colorPrimary, getTheme()));
        pinView.setLineColor(
                ResourcesCompat.getColorStateList(getResources(), R.color.line_colors, getTheme()));
        pinView.setItemCount(6);
        pinView.setAnimationEnable(true);// start animation when adding text
        pinView.setCursorVisible(false);

        pinView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessageiPIN.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessageiPIN.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() == 6) {

                    iPIN = s.toString();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(pinView.getWindowToken(), 0);

                } else {

                    iPIN = s.toString();
                    // Do Nothing
                }
            }
        });


        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Validate();


            }
        });

        regenerateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Regenerate();
            }
        });

    }

    public void LoginWithPassAndPin() {

        try {

            if (helper.isNetworkAvailable()) {

                dialog.show();
                final String time = constants.dateTime;
                final String md5 = helper.md5(userName.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                HttpsTrustManager.allowMySSL(LoginWithPassAndPinScreen.this);
                final String md5Password = helper.md5(userName.toUpperCase() + pass);
                final String md5PasswordAndPin = helper.md5(iPIN + userName.toUpperCase());
                final String token = helper.md5(time + "signs" + constants.address);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sSigns_Username", userName);
                params.put("sSigns_Password", md5Password);
                params.put("sSigns_PinData", md5PasswordAndPin);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", token);
                params.put("sTimeStamp", time);
                params.put("sDeviceLatitude", String.valueOf(latitude));
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sLocationName", location);
                params.put("sLogin_Type", "0");
                if (!fcmToken.equals("N/A"))
                    params.put("sFcm_Token", fcmToken);
                else {
                    Log.d("---", "---" + constants.fcmSharedPref.getString("fcmToken", "N/A"));
                    params.put("sFcm_Token", constants.fcmSharedPref.getString("fcmToken", "N/A"));
                }
                params.put("sServerSessionKey", md5);
                CryptLib _crypt = new CryptLib();
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.validateURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code").equals("00")) {
                                            try {

                                                appPreferences.putString("location", location);
                                                menus = jsonObject.getString("menus");
//

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }


                                            if ((jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Is_FirstLogin").equals("Y"))) {
//
                                                isFirstLogin(jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result"));
                                            } else {

                                                if ((jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Is_DeviceExist").equals("False"))) {

                                                    isDeviceExist(jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result"));

                                                } else {

//                                            isFirstLogin(jsonObject);
                                                    DeviceAlreadyExist(jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result"));


                                                }
                                            }
                                        } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code").equals("02")) {
                                            dialog.dismiss();
                                            showDilogForErrorForLogin("User ID or Password is Invalid. Kindly contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for any assistance", "ERROR");
                                            clearViews();
                                            validateBtn.setEnabled(true);
//
                                        } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code").equals("10")) {
                                            dialog.dismiss();
                                            utils.showDilogForError("Invalid iPIN entered. Please enter correct iPIN received on your mobile/email address.", "ERROR");
                                            clearViews();
                                            validateBtn.setEnabled(true);
                                        } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Code").equals("54")) {
                                            dialog.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogin_Step2Result").getString("Status_Description"), "ERROR", LoginWithPassAndPinScreen.this, new LoginScreen());
                                            clearViews();
                                            validateBtn.setEnabled(true);

                                        } else {
                                            dialog.dismiss();

                                            showDilogForErrorForLogin("User ID or Password is Invalid. Kindly contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for any assistance", "ERROR");
                                            validateBtn.setEnabled(true);

                                            clearViews();
                                        }
                                    } else {
                                        dialog.dismiss();

                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);

                                        clearViews();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    dialog.dismiss();

                                    showDilogForErrorForLogin("User ID or Password is Invalid. Kindly contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for any assistance", "ERROR");
                                    validateBtn.setEnabled(true);

                                    clearViews();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    dialog.dismiss();

                                    showDilogForErrorForLogin("User ID or Password is Invalid. Kindly contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for any assistance", "ERROR");
                                    validateBtn.setEnabled(true);

                                    clearViews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    dialog.dismiss();

                                    showDilogForErrorForLogin("User ID or Password is Invalid. Kindly contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for any assistance", "ERROR");
                                    validateBtn.setEnabled(true);

                                    clearViews();
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sSigns_Username", userName);
//                        params.put("sSigns_Password", md5Password);
//                        params.put("sSigns_PinData", md5PasswordAndPin);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", token);
//                        params.put("sTimeStamp", time);
//                        params.put("sDeviceLatitude", String.valueOf(latitude));
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLocationName", location);
//                        params.put("sLogin_Type", "0");
//                        params.put("sFcm_Token", fcmToken);
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };
                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                dialog.dismiss();
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");

                validateBtn.setEnabled(true);

                clearViews();

            }
        } catch (Exception e) {

//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            validateBtn.setEnabled(true);
            pd.dismiss();
            clearViews();

        }
    }


    public void RegeneratePin() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                dialog.show();
//                webService.RegeneratePin(userName, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getRegeneratePinReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//                                dialog.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "Invalid Username/iPIN!", Toast.LENGTH_LONG).show();
//                                utils.showDilogForError("iPIN has been sent Successfully.", "SUCCESS");
//                                regenerateBtn.setEnabled(true);
//                                validateBtn.setEnabled(true);
//                                clearViews();
//
//
//                            } else {
//                                dialog.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "iPIN has been sent Successfully.", Toast.LENGTH_LONG).show();
//                                utils.showDilogForError("iPIN has been sent Successfully.", "SUCCESS");
//                                regenerateBtn.setEnabled(true);
//                                validateBtn.setEnabled(true);
//                                clearViews();
//                            }
//                        } else {
//                            dialog.dismiss();
//                            utils.showDilogForError("iPIN has been sent Successfully.", "SUCCESS");
//                            regenerateBtn.setEnabled(true);
//                            validateBtn.setEnabled(true);
//                            clearViews();
//
//                        }
//                    }
//                }, 5000);
                final String md5 = helper.md5(userName.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sDeviceLatitude", String.valueOf(latitude));
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sLocationName", location);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Username", userName);
                params.put("sServerSessionKey", md5);
                params.put("sOptionalIpinGenerater", "Y");
                HttpsTrustManager.allowMySSL(LoginWithPassAndPinScreen.this);
                CryptLib _crypt = new CryptLib();
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("00")) {
                                            dialog.dismiss();
                                            utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                            regenerateBtn.setEnabled(true);
                                            validateBtn.setEnabled(true);
                                            clearViews();
                                        } else if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("54")) {
                                            dialog.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR", LoginWithPassAndPinScreen.this, new LoginScreen());
                                            regenerateBtn.setEnabled(true);
                                            validateBtn.setEnabled(true);

                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                            regenerateBtn.setEnabled(true);
                                            validateBtn.setEnabled(true);
                                            clearViews();
                                        }

                                    } else {

                                        dialog.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        regenerateBtn.setEnabled(true);
                                        validateBtn.setEnabled(true);
                                        clearViews();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                    regenerateBtn.setEnabled(true);
                                    validateBtn.setEnabled(true);
                                    clearViews();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    dialog.dismiss();
                                    utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                    regenerateBtn.setEnabled(true);
                                    validateBtn.setEnabled(true);
                                    clearViews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");
                                    regenerateBtn.setEnabled(true);
                                    validateBtn.setEnabled(true);
                                    clearViews();
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sDeviceLatitude", String.valueOf(latitude));
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLocationName", location);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", userName);
//                        params.put("sServerSessionKey", md5);
//                        params.put("sOptionalIpinGenerater", "Y");
//
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };
                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
                regenerateBtn.setEnabled(true);

                clearViews();

            }
        } catch (Exception e) {
            dialog.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            validateBtn.setEnabled(true);
            regenerateBtn.setEnabled(true);

            clearViews();

        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(LoginWithPassAndPinScreen.this, LoginScreen.class);
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
    }

    public void clearViews() {

//        pinView.setText("");
        pinView.getText().clear();
        InitPinView();
    }


    public void NonSamba() {

        validateBtn.setEnabled(false);

//                        LoginWithPassAndPinForNonSamba();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                dialog.dismiss();
                utils.showDilogForError("User ID or Password is Invalid. Kindly contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for any assistance.", "ERROR");
                validateBtn.setEnabled(true);

                clearViews();

            }

        }, 5000);
    }


    public void SambaWithLock() {

        validateBtn.setEnabled(false);

//                        LoginWithPassAndPinForNonSamba();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                dialog.dismiss();
                showDilogForErrorForLogin("Your Samba Digital account is locked. Please contact Samba Phone Banking at at +92-11-11-72622 (SAMBA) for further information and assistance", "ERROR");
                validateBtn.setEnabled(true);

                clearViews();

            }

        }, 5000);


    }

    public void SambaWithUnLock() {

        validateBtn.setEnabled(false);
        dialog.show();

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            location = getAddress("login");
//            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null)) {
//
//                LoginWithPassAndPin();
//
//            } else {
//                location = "-";
//                LoginWithPassAndPin();
////                utils.showDilogForError("Please turn on location services.", "WARNING");
////                validateBtn.setEnabled(true);
//            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";
            LoginWithPassAndPin();
//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

    }

    public void Error() {

        validateBtn.setEnabled(false);
//        dialog.show();
//                        LoginWithPassAndPinForNonSamba();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                dialog.dismiss();
                showDilogForErrorForLogin("User ID or Password is Invalid. Kindly contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for any assistance.", "ERROR");
                validateBtn.setEnabled(true);
                clearViews();

            }

        }, 5000);
    }


    public void Validate() {


        iPIN = pinView.getText().toString();

        if (TextUtils.isEmpty(iPIN) || iPIN.length() < 6 || iPIN.contains(" ") || !helper.validateInputForSC(iPIN)) {
            errorMessageiPIN.setVisibility(View.VISIBLE);
            errorMessageiPIN.bringToFront();
            errorMessageiPIN.setError("");
            errorMessageiPIN.setText("Please enter valid iPIN");
        }

        if ((!TextUtils.isEmpty(pass)) && (!TextUtils.isEmpty(iPIN) && iPIN.length() == 6)) {

            dialog.show();
            if (isType.equals("NonSamba")) {

                NonSamba();

            } else if (isType.equals("Samba")) {

                if (is_Locked.equals("Y")) {

                    SambaWithLock();

                } else {

                    SambaWithUnLock();
                }


            } else {

                Error();
            }

        }
    }


    public void NonSambaPin() {

        validateBtn.setEnabled(false);
//        dialog.show();
//                        LoginWithPassAndPinForNonSamba();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                dialog.dismiss();
                utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");

                validateBtn.setEnabled(true);
//                dialog.dismiss();
                clearViews();

            }

        }, 5000);
    }

    public void SambaWithLockNPin() {

        validateBtn.setEnabled(false);
        dialog.show();
//                        LoginWithPassAndPinForNonSamba();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                dialog.dismiss();
                utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");

                validateBtn.setEnabled(true);

                clearViews();

            }

        }, 5000);
    }

    public void SambaWithUnLockNPin() {

        regenerateBtn.setEnabled(false);
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            location = getAddress("regenerate");
//            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null)) {
//
//                RegeneratePin();
//
//            } else {
//                location = "-";
//                RegeneratePin();
////                utils.showDilogForError("Please turn on location services.", "WARNING");
////                validateBtn.setEnabled(true);
//            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";
            RegeneratePin();
//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

    }


    public void ErrorForPin() {

        validateBtn.setEnabled(false);
        dialog.show();
//                  LoginWithPassAndPinForNonSamba();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                dialog.dismiss();
                utils.showDilogForError("iPIN has been sent on your registered mobile and email address.", "SUCCESS");

                validateBtn.setEnabled(true);

                clearViews();

            }

        }, 5000);
    }


    public void isFirstLogin(JSONObject jsonObject) {

        try {

            Intent intent;
            if (jsonObject.getString("SecurityAlert").toUpperCase().equals("P")) {

                intent = new Intent(LoginWithPassAndPinScreen.this, ConfirmPasscodeScreen.class);
                intent.putExtra("securityCheck", "passcode");
                intent.putExtra("firstLogin", "First");
                if (jsonObject.has("isRooted"))
                    intent.putExtra("isRooted", jsonObject.getString("RootedDeviceTerms"));
                if (jsonObject.has("NotificationEnable"))
                    intent.putExtra("isNotification", jsonObject.getString("NotificationEnable"));

            } else if (jsonObject.getString("SecurityAlert").toUpperCase().equals("Q")) {

                intent = new Intent(LoginWithPassAndPinScreen.this, SecurityQuestionScreen.class);
                intent.putExtra("firstLogin", "First");
                intent.putExtra("securityCheck", "securityQuestion");
                if (jsonObject.has("isRooted"))
                    intent.putExtra("isRooted", jsonObject.getString("RootedDeviceTerms"));
                if (jsonObject.has("NotificationEnable"))
                    intent.putExtra("isNotification", jsonObject.getString("NotificationEnable"));
            } else {

                if (jsonObject.getString("RootedDeviceTerms").equals("N"))
                    intent = new Intent(LoginWithPassAndPinScreen.this, TermsNConditionsScreen.class);
                else if (jsonObject.getString("NotificationEnable").toUpperCase().equals("TRUE")) {

                    intent = new Intent(LoginWithPassAndPinScreen.this, ProfileSetupScreen.class);
                    intent.putExtra("isFirst", "Yes");

                } else {
                    intent = new Intent(LoginWithPassAndPinScreen.this, NotificationScreen.class);

                }


            }

            validateBtn.setEnabled(true);
            dialog.dismiss();
            CryptLib _crypt = new CryptLib();
            constants.editor.putString("T24_FullName", _crypt.encryptForParams(jsonObject.getString("FullName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("T24_CIF", _crypt.encryptForParams(jsonObject.getString("T24_CIF"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("LegalID", _crypt.encryptForParams(jsonObject.getString("LegalID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("LegalType", _crypt.encryptForParams(jsonObject.getString("LegalType"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("T24_Target", _crypt.encryptForParams(jsonObject.getString("T24_Target"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("EmailAddress", _crypt.encryptForParams(jsonObject.getString("EmailAddress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("MobileNumber", _crypt.encryptForParams(jsonObject.getString("MobileNumber"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("ProfilePic", _crypt.encryptForParams(jsonObject.getString("ProfileAvatar").replaceAll("@PLUS@", "+"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("UserProgress", _crypt.encryptForParams(jsonObject.getString("UserProgress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("UserAddress", _crypt.encryptForParams(jsonObject.getString("UserAddress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("SessionID", _crypt.encryptForParams(jsonObject.getString("SessionID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("Is_FirstLogin", _crypt.encryptForParams(jsonObject.getString("Is_FirstLogin"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("NotificationEnable", _crypt.encryptForParams(jsonObject.getString("NotificationEnable"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams(jsonObject.getString("FingerPrintEnabled"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            if (jsonObject.has("LastLoginLocation"))
                constants.editor.putString("LastLoginLocation", _crypt.encryptForParams(jsonObject.getString("LastLoginLocation"), constants.getDeviceMac(), constants.getDeviceIMEI()));

            constants.editor.putString("userName", _crypt.encryptForParams(userName, constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("Finger", "False");
            constants.editor.putString("Menus", menus);
            if (!TextUtils.isEmpty(jsonObject.getString("T24_CIF"))) {

                constants.editor.putString("type", _crypt.encryptForParams("SAMBA", constants.getDeviceMac(), constants.getDeviceIMEI()));
            } else {

                constants.editor.putString("type", _crypt.encryptForParams("NON_SAMBA", constants.getDeviceMac(), constants.getDeviceIMEI()));
            }
            constants.editor.commit();
            constants.editor.apply();
            clearViews();
//            dialog.dismiss();

            intent.putExtra("username", userName);
            intent.putExtra("login", "login");
            intent.putExtra("isFirstLogin", "Yes");
            Constants.Shared_KEY = jsonObject.getString("SessionID");
            startActivity(intent);
            finish();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void Regenerate() {

        dialog.show();
        if (isType.equals("NonSamba")) {

            NonSambaPin();

        } else if (isType.equals("Samba")) {


            if (is_Locked.equals("Y")) {

                SambaWithLockNPin();

            } else {

                SambaWithUnLockNPin();
            }


        } else {

            ErrorForPin();
        }
    }

    public void isDeviceExist(JSONObject jsonObject) {

        try {

            Intent intent;
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Validate Successfully.", Toast.LENGTH_LONG).show();
//            Intent intent = new Intent(LoginWithPassAndPinScreen.this, NotificationScreen.class);
            if (jsonObject.getString("SecurityAlert").toUpperCase().equals("P")) {

                intent = new Intent(LoginWithPassAndPinScreen.this, ConfirmPasscodeScreen.class);
                intent.putExtra("firstLogin", "No");
                intent.putExtra("securityCheck", "passcode");
                if (jsonObject.has("isRooted"))
                    intent.putExtra("isRooted", jsonObject.getString("RootedDeviceTerms"));
                if (jsonObject.has("NotificationEnable"))
                    intent.putExtra("isNotification", jsonObject.getString("NotificationEnable"));

            } else if (jsonObject.getString("SecurityAlert").toUpperCase().equals("Q")) {

                intent = new Intent(LoginWithPassAndPinScreen.this, SecurityQuestionScreen.class);
                intent.putExtra("securityCheck", "securityQuestion");
                intent.putExtra("firstLogin", "No");
                if (jsonObject.has("isRooted"))
                    intent.putExtra("isRooted", jsonObject.getString("RootedDeviceTerms"));
                if (jsonObject.has("NotificationEnable"))
                    intent.putExtra("isNotification", jsonObject.getString("NotificationEnable"));

            } else {

                if (jsonObject.getString("RootedDeviceTerms").equals("N"))
                    intent = new Intent(LoginWithPassAndPinScreen.this, TermsNConditionsScreen.class);
                else
                    intent = new Intent(LoginWithPassAndPinScreen.this, NotificationScreen.class);

            }

            validateBtn.setEnabled(true);
            dialog.dismiss();
            CryptLib _crypt = new CryptLib();
            constants.editor.putString("T24_FullName", _crypt.encryptForParams(jsonObject.getString("FullName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("T24_CIF", _crypt.encryptForParams(jsonObject.getString("T24_CIF"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("LegalID", _crypt.encryptForParams(jsonObject.getString("LegalID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("LegalType", _crypt.encryptForParams(jsonObject.getString("LegalType"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("T24_Target", _crypt.encryptForParams(jsonObject.getString("T24_Target"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("EmailAddress", _crypt.encryptForParams(jsonObject.getString("EmailAddress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("MobileNumber", _crypt.encryptForParams(jsonObject.getString("MobileNumber"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("ProfilePic", _crypt.encryptForParams(jsonObject.getString("ProfileAvatar").replaceAll("@PLUS@", "+"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("UserProgress", _crypt.encryptForParams(jsonObject.getString("UserProgress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("UserAddress", _crypt.encryptForParams(jsonObject.getString("UserAddress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("SessionID", _crypt.encryptForParams(jsonObject.getString("SessionID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("NotificationEnable", _crypt.encryptForParams(jsonObject.getString("NotificationEnable"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams(jsonObject.getString("FingerPrintEnabled"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            if (jsonObject.has("LastLoginLocation"))
                constants.editor.putString("LastLoginLocation", _crypt.encryptForParams(jsonObject.getString("LastLoginLocation"), constants.getDeviceMac(), constants.getDeviceIMEI()));

            constants.editor.putString("Menus", menus);
            constants.editor.putString("Finger", "False");
            constants.editor.putString("userName", _crypt.encryptForParams(userName, constants.getDeviceMac(), constants.getDeviceIMEI()));
            if (!TextUtils.isEmpty(jsonObject.getString("T24_CIF"))) {

                constants.editor.putString("type", _crypt.encryptForParams("SAMBA", constants.getDeviceMac(), constants.getDeviceIMEI()));
            } else {

                constants.editor.putString("type", _crypt.encryptForParams("NON_SAMBA", constants.getDeviceMac(), constants.getDeviceIMEI()));
            }
            constants.editor.commit();
            constants.editor.apply();
            clearViews();
            intent.putExtra("username", userName);
            intent.putExtra("login", "login");
            intent.putExtra("isFirstLogin", "No");
            Constants.Shared_KEY = jsonObject.getString("SessionID");
            startActivity(intent);
            finish();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void DeviceAlreadyExist(JSONObject jsonObject) {

        try {
            Intent intent;
            if (jsonObject.getString("SecurityAlert").toUpperCase().equals("P")) {

                intent = new Intent(LoginWithPassAndPinScreen.this, ConfirmPasscodeScreen.class);
                intent.putExtra("securityCheck", "passcode");
                intent.putExtra("firstLogin", "Never");
                if (jsonObject.has("isRooted"))
                    intent.putExtra("isRooted", jsonObject.getString("RootedDeviceTerms"));
                if (jsonObject.has("NotificationEnable"))
                    intent.putExtra("isNotification", jsonObject.getString("NotificationEnable"));

            } else if (jsonObject.getString("SecurityAlert").toUpperCase().equals("Q")) {

                intent = new Intent(LoginWithPassAndPinScreen.this, SecurityQuestionScreen.class);
                intent.putExtra("securityCheck", "securityQuestion");
                intent.putExtra("firstLogin", "Never");
                if (jsonObject.has("isRooted"))
                    intent.putExtra("isRooted", jsonObject.getString("RootedDeviceTerms"));
                if (jsonObject.has("NotificationEnable"))
                    intent.putExtra("isNotification", jsonObject.getString("NotificationEnable"));

            } else {
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Validate Successfully.", Toast.LENGTH_LONG).show();

                intent = new Intent(LoginWithPassAndPinScreen.this, HomeScreen.class);

            }

            validateBtn.setEnabled(true);
            dialog.dismiss();
            CryptLib _crypt = new CryptLib();
            constants.editor.putString("T24_FullName", _crypt.encryptForParams(jsonObject.getString("FullName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("T24_CIF", _crypt.encryptForParams(jsonObject.getString("T24_CIF"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("LegalID", _crypt.encryptForParams(jsonObject.getString("LegalID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("LegalType", _crypt.encryptForParams(jsonObject.getString("LegalType"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("T24_Target", _crypt.encryptForParams(jsonObject.getString("T24_Target"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("EmailAddress", _crypt.encryptForParams(jsonObject.getString("EmailAddress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("MobileNumber", _crypt.encryptForParams(jsonObject.getString("MobileNumber"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("ProfilePic", _crypt.encryptForParams(jsonObject.getString("ProfileAvatar").replaceAll("@PLUS@", "+"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("UserProgress", _crypt.encryptForParams(jsonObject.getString("UserProgress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("UserAddress", _crypt.encryptForParams(jsonObject.getString("UserAddress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("SessionID", _crypt.encryptForParams(jsonObject.getString("SessionID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("NotificationEnable", _crypt.encryptForParams(jsonObject.getString("NotificationEnable"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("FingerPrintEnabled", _crypt.encryptForParams(jsonObject.getString("FingerPrintEnabled"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            if (jsonObject.has("LastLoginLocation"))
                constants.editor.putString("LastLoginLocation", _crypt.encryptForParams(jsonObject.getString("LastLoginLocation"), constants.getDeviceMac(), constants.getDeviceIMEI()));

            constants.editor.putString("userName", _crypt.encryptForParams(userName, constants.getDeviceMac(), constants.getDeviceIMEI()));
            constants.editor.putString("Finger", "False");
            constants.editor.putString("Menus", menus);
            if (!TextUtils.isEmpty(jsonObject.getString("T24_CIF"))) {

                constants.editor.putString("type", _crypt.encryptForParams("SAMBA", constants.getDeviceMac(), constants.getDeviceIMEI()));
            } else {

                constants.editor.putString("type", _crypt.encryptForParams("NON_SAMBA", constants.getDeviceMac(), constants.getDeviceIMEI()));
            }
            constants.editor.commit();
            constants.editor.apply();
            Constants.Shared_KEY = jsonObject.getString("SessionID");
            clearViews();
            startActivity(intent);
            finish();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void showDilogForErrorForLogin(String msg, String header) {




        dialog = new Dialog(LoginWithPassAndPinScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(LoginWithPassAndPinScreen.this, LoginScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
                dialog.dismiss();
                finish();
            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();


    }

    @Override
    public void onSupportActionModeStarted(ActionMode mode) {
        super.onSupportActionModeStarted(mode);
        mode.finish();
    }

    public void InitPinView() {

        pinView = (PinView) findViewById(R.id.pinView);
        pinView.setTextColor(
                ResourcesCompat.getColor(getResources(), R.color.colorAccent, getTheme()));
        pinView.setTextColor(
                ResourcesCompat.getColorStateList(getResources(), R.color.colorPrimary, getTheme()));
        pinView.setLineColor(
                ResourcesCompat.getColor(getResources(), R.color.colorPrimary, getTheme()));
        pinView.setLineColor(
                ResourcesCompat.getColorStateList(getResources(), R.color.line_colors, getTheme()));
        pinView.setItemCount(6);
        pinView.setAnimationEnable(true);// start animation when adding text
        pinView.setCursorVisible(false);

        pinView.setEnabled(true);
        pinView.bringToFront();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(pinView, InputMethodManager.SHOW_IMPLICIT);

    }

    public Address getAddress(final double latitude, final double longitude, final String value) {


        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {


                try {
//                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
//
//                    if (addresses != null) {
//                        if (!TextUtils.isEmpty(addresses.get(0).toString()) && !addresses.get(0).equals("null") && !addresses.get(0).equals(null)) {
//
//                            Address locationAddress = addresses.get(0);
//                            location = locationAddress.getAddressLine(0);
//
//                            if (value.equals("login"))
//                                LoginWithPassAndPin();
//                            else if (value.equals("regenerate"))
//                                RegeneratePin();
//
//                        } else {
////                                utils.showDilogForError("Please turn on location services.", "WARNING");
////                                loginBtn.setEnabled(true);
//                            location = "-";
//                            if (value.equals("login"))
//                                LoginWithPassAndPin();
//                            else if (value.equals("regenerate"))
//                                RegeneratePin();
//                        }
//                    } else {
//
//                        location = "-";
//                        if (value.equals("login"))
//                            LoginWithPassAndPin();
//                        else if (value.equals("regenerate"))
//                            RegeneratePin();
//                    }
                    new AsyncCaller().execute(String.valueOf(latitude), String.valueOf(longitude), value);

                } catch (Exception e) {
                    e.printStackTrace();
                    if (value.equals("login")) {
                        location = "-";
                        LoginWithPassAndPin();
                    } else if (value.equals("regenerate")) {

                        location = "-";
                        RegeneratePin();
                    }
                }


            }
        });

        if (addresses != null) {
            return addresses.get(0);
        } else
            return null;
    }


    public String getAddress(String login) {

        Address locationAddress = getAddress(latitude, longitude, login);
        String address = "";
        if (locationAddress != null) {
            address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();

            String currentLocation;

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address;

                if (!TextUtils.isEmpty(address1))
                    currentLocation += "\n" + address1;

                if (!TextUtils.isEmpty(city)) {
                    currentLocation += "\n" + city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += " - " + postalCode;
                } else {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += "\n" + postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation += "\n" + state;

                if (!TextUtils.isEmpty(country))
                    currentLocation += "\n" + country;


            }

        }

        return address;

    }

    private class AsyncCaller extends AsyncTask<String, Void, String> {

        String value;
        double lati, longi;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

//            pd.show();

        }

        @Override
        protected String doInBackground(String... params) {

            //this method will be running on a background thread so don't update UI from here
            //do your long-running http tasks here, you don't want to pass argument and u can access the parent class' variable url over here
            try {


                lati = Double.valueOf(params[0]);
                longi = Double.valueOf(params[1]);
                value = params[2];
                addresses = geocoder.getFromLocation(lati, longi, 1);
            } catch (Exception e) {

                e.printStackTrace();
                location = "-";
                if (value.equals("login"))
                    LoginWithPassAndPin();
                else if (value.equals("regenerate"))
                    RegeneratePin();

            }

            if (addresses != null)
                return addresses.get(0).toString();
            else
                return "null";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result != null) {
                if (!TextUtils.isEmpty(result) && !result.equals("null") && !result.equals(null)) {


                    Address locationAddress = addresses.get(0);
                    location = locationAddress.getAddressLine(0);

                    if (value.equals("login"))
                        LoginWithPassAndPin();
                    else if (value.equals("regenerate"))
                        RegeneratePin();

                } else {
//                                utils.showDilogForError("Please turn on location services.", "WARNING");
//                                loginBtn.setEnabled(true);
                    location = "-";
                    if (value.equals("login"))
                        LoginWithPassAndPin();
                    else if (value.equals("regenerate"))
                        RegeneratePin();
                }
            } else {

                location = "-";
                if (value.equals("login"))
                    LoginWithPassAndPin();
                else if (value.equals("regenerate"))
                    RegeneratePin();

            }
            //this method will be running on UI thread


        }

    }
}