package com.ceesolutions.samba.accessControl.Fragments;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.appcompat.view.ActionMode;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.accessControl.ProfileSetupScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 2/21/18.
 */

public class SecurityQuestionFragment extends Fragment {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    ArrayList<String> questionList = new ArrayList<String>();
    String token, target, userName, questionSelected;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private TextView validateBtn, message, doneBtn, heading;
    private EditText editQuestionAnswer;
    private Spinner questionSpinner;
    private String[] quesitons;
    private String questionAnswer;
    private TextView errorMessage;
    private String questionCode;
    private ImageView backBtn;
    private TextView headingTxt;
    private ImageView info;
    private Dialog dialog;
    private Utils utils;
    private AppPreferences appPreferences;
    private String location;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.set_secret_question, container, false);

        initViews(convertView);

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                questionAnswer = editQuestionAnswer.getText().toString().trim();


                if (questionSelected.equals("Select Question")) {

//                    Toast.makeText(getActivity(), "Please select any one question", Toast.LENGTH_SHORT).show();

                    utils.showDilogForError("Please select any one question.", "WARNING");
                } else {

                    if (TextUtils.isEmpty(questionAnswer)) {
                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("Please enter valid answer");

                    } else if (!helper.validateInputForSC(questionAnswer)) {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("Answer cannot contains <,>,\",',%,(,),&,+,\\,~");

                    } else {
                        validateBtn.setEnabled(false);

                        if (questionSelected.equals("What is your favourite car?")) {

                            questionCode = "SQ001";

                        } else if (questionSelected.equals("Who is your childhood best friend?")) {

                            questionCode = "SQ002";

                        } else if (questionSelected.equals("What was the name of your first pet?")) {

                            questionCode = "SQ003";
                        }

                        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                            longitude = Double.valueOf(longitude1);
                            latitude = Double.valueOf(latitude1);
                            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                            } else {
                                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                            }
                        } else {

                            longitude = 0;
                            latitude = 0;
                            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
                        }
                        AddSecretQuestionAnswer();
                        pd.show();

                    }
                }

            }
        });

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showInfo();
            }
        });

        editQuestionAnswer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                questionAnswer = editable.toString().trim();

            }
        });

        questionAnswer = editQuestionAnswer.getText().toString().trim();

//        headingTxt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                ProfileSetupScreen.pager.setCurrentItem(3);
//                ProfileSetupScreen.currentItem++;
//                headingTxt.setText("DONE");
//                validateBtn.setEnabled(true);
//                editQuestionAnswer.setText("");
//
//            }
//        });


        return convertView;
    }

    public void initViews(View convertView) {

        try {

            CryptLib _crypt = new CryptLib();
            ProfileSetupScreen activity = (ProfileSetupScreen) getActivity();
            backBtn = (ImageView) activity.findViewById(R.id.backBtn);
            headingTxt = (TextView) activity.findViewById(R.id.headingTxt);
            info = (ImageView) convertView.findViewById(R.id.info);
//        headingTxt.setVisibility(View.VISIBLE);
            validateBtn = (TextView) convertView.findViewById(R.id.validateBtn);
            editQuestionAnswer = (EditText) convertView.findViewById(R.id.editQuestionAnswer);
            errorMessage = (TextView) convertView.findViewById(R.id.errorMessage);
            utils = new Utils(getActivity());
            questionSpinner = (Spinner) convertView.findViewById(R.id.questionSpinner);
            requestQueue = Volley.newRequestQueue(getActivity());
            webService = new WebService();
            helper = Helper.getHelper(getActivity());
            constants = Constants.getConstants(getActivity());
            token = constants.sharedPreferences.getString("fcmToken", "fcmToken");
            target = _crypt.decrypt(constants.sharedPreferences.getString("T24_Target", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            userName = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            pd = new Dialog(getActivity());
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            appPreferences = new AppPreferences(getActivity());
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            appPreferences = new AppPreferences(getActivity());
            utils = new Utils(getActivity());
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {

            GetSecretQuestions();

        } else {

            //Do Nothing
        }
    }

    public void GetSecretQuestions() {

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {
//                webService.GetSecretQuestions(userName, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getGetQuestionsReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Information is not valid!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
////                                ProfileSetupScreen.pager.setCurrentItem(4);
////                                ProfileSetupScreen.currentItem++;
//
//
//                            } else {
//
//                                try {
//
//
//                                    JSONArray jsonArray = new JSONArray(jsonObject.getString("SecretQuestionList"));
//                                    for (int i = 0; i < jsonArray.length(); i++) {
//                                        try {
//
//                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
//
//                                            questionList.add(jsonObj.getString("Question_Description"));
//
//                                        } catch (Exception ex) {
//                                            ex.printStackTrace();
//                                            pd.dismiss();
//                                            showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                            validateBtn.setEnabled(true);
//                                        }
//
//                                    }
//
//                                    quesitons = new String[questionList.size()];
//                                    quesitons = questionList.toArray(quesitons);
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    validateBtn.setEnabled(true);
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//                        }
//                    }
//                }, 3000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md51 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sServerSessionKey", md51);
                HttpsTrustManager.allowMySSL(getActivity());
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getQuestionsURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md51)) {
                                        if (jsonObject.getJSONObject("Signs_GetSecretQuestionResult").getString("Status_Code").equals("00")) {

                                            try {

                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_GetSecretQuestionResult").getString("SecretQuestionList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        questionList.add(jsonObj.getString("Question_Description"));

                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        pd.dismiss();
                                                        showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        validateBtn.setEnabled(true);
                                                    }

                                                }

                                                quesitons = new String[questionList.size()];
                                                quesitons = questionList.toArray(quesitons);
                                                showSpinner();


                                            } catch (JSONException ex) {

                                                ex.printStackTrace();
                                                pd.dismiss();
                                                showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } else if (jsonObject.getJSONObject("Signs_GetSecretQuestionResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_GetSecretQuestionResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());
                                            validateBtn.setEnabled(true);

                                        } else {
                                            pd.dismiss();
                                            showDilogForError(jsonObject.getJSONObject("Signs_GetSecretQuestionResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                        }

                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sServerSessionKey", md51);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
        }
    }

    public void AddSecretQuestionAnswer() {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                final String md5Answer = helper.md5(userName.toUpperCase() + questionCode + questionAnswer.toUpperCase());
//                webService.AddSecretQuestionAnswer(userName, questionCode, md5Answer, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getSetQuestionAnswerReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                showDilogForError("Question cannot be added. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
//                                editQuestionAnswer.setText("");
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        ProfileSetupScreen.pager.setCurrentItem(3);
//                                        ProfileSetupScreen.currentItem++;
//                                        headingTxt.setText("DONE");
//                                        validateBtn.setEnabled(true);
//                                        editQuestionAnswer.setText("");
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        showDilogForError("Question cannot be added. Please try again later.", "ERROR");
//                                        validateBtn.setEnabled(true);
//                                        editQuestionAnswer.setText("");
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    showDilogForError("Question cannot be added. Please try again later.", "ERROR");
//                                    validateBtn.setEnabled(true);
//                                    editQuestionAnswer.setText("");
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            showDilogForError("Question cannot be added. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//                            editQuestionAnswer.setText("");
//
//                        }
//                    }
//                }, 3000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", userName);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sQuestionCode", questionCode);
                params.put("sAnswerText", md5Answer);
                params.put("sServerSessionKey", md5);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                HttpsTrustManager.allowMySSL(getActivity());
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.setQuestionAnswerURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        try {
                                            CryptLib _crypt = new CryptLib();

                                            if (jsonObject.getJSONObject("Signs_SetSecretQuestionResult").getString("Status_Code").equals("00")) {
                                                JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_SetSecretQuestionResult").getString("Status_Code"));
                                                pd.dismiss();
                                                ProfileSetupScreen.pager.setCurrentItem(3);
                                                ProfileSetupScreen.currentItem++;
                                                headingTxt.setText("DONE");
                                                validateBtn.setEnabled(true);
                                                editQuestionAnswer.setText("");
                                                constants.editor.putString("UserProgress", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_SetSecretQuestionResult").getString("UserProgress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.editor.commit();
                                                constants.editor.apply();

                                            } else if (jsonObject.getJSONObject("Signs_SetSecretQuestionResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_SetSecretQuestionResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());
                                                validateBtn.setEnabled(true);
                                                editQuestionAnswer.setText("");

                                            } else {
                                                pd.dismiss();
                                                showDilogForError(jsonObject.getJSONObject("Signs_SetSecretQuestionResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                                editQuestionAnswer.setText("");
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                        editQuestionAnswer.setText("");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    showDilogForError("Question cannot be added. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    editQuestionAnswer.setText("");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    showDilogForError("Question cannot be added. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    editQuestionAnswer.setText("");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    showDilogForError("Question cannot be added. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    editQuestionAnswer.setText("");
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", userName);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sQuestionCode", questionCode);
//                        params.put("sAnswerText", md5Answer);
//                        params.put("sServerSessionKey", md5);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
                postRequest.setRetryPolicy(new RetryPolicy() {
                    @Override
                    public int getCurrentTimeout() {
                        return 50000;
                    }

                    @Override
                    public int getCurrentRetryCount() {
                        return 50000;
                    }

                    @Override
                    public void retry(VolleyError error) throws VolleyError {

                    }
                });
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
                editQuestionAnswer.setText("");

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            showDilogForError("Question cannot be added. Please try again later.", "ERROR");
            doneBtn.setEnabled(true);
            editQuestionAnswer.setText("");

        }
    }

    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                pd.dismiss();
                spinnerAdapter questionAdapter = new spinnerAdapter(getActivity(), R.layout.custom_textview_grey);
                questionAdapter.addAll(quesitons);
                questionAdapter.add("Select Question");
                questionSpinner.setAdapter(questionAdapter);
                questionSpinner.setSelection(questionAdapter.getCount());
                questionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


                            // Get select item
                            if (questionSpinner.getSelectedItem() == "Select Question") {
                                questionSelected = questionSpinner.getSelectedItem().toString();
                                Log.d("range", "---" + questionSpinner);

                            } else {

                                questionSelected = questionSpinner.getSelectedItem().toString();

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });

            }

        }, 2000);
    }

    public void showInfo() {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        heading.setText("Information");
        message.setText("Select a security question from question drop-down menu. Type an answer into the corresponding answer field. The answers to question is not case sensitive.");
        heading.setBackgroundColor(getResources().getColor(R.color.grey));
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                ProfileSetupScreen.pager.setCurrentItem(3);
                ProfileSetupScreen.currentItem++;
                headingTxt.setText("SKIP");
            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();

    }


}