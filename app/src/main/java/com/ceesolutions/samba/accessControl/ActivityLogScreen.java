package com.ceesolutions.samba.accessControl;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.Bookme.BookMeMainScreen;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.Adapters.ActivityLogCustomListAdapter;
import com.ceesolutions.samba.accessControl.Model.Logs;
import com.ceesolutions.samba.beneficiariesManagement.ManageBeneficiaresScreen;
import com.ceesolutions.samba.billPayments.PayBillsScreen;
import com.ceesolutions.samba.cardManagement.ManageCardsScreen;
import com.ceesolutions.samba.eatMubarak.EatMubarakMainScreen;
import com.ceesolutions.samba.masterCardIntegration.MainActivity;
import com.ceesolutions.samba.transactions.AccountsScreen;
import com.ceesolutions.samba.transactions.Adapters.TransactionDetailsCustomListAdapter;
import com.ceesolutions.samba.transactions.FundTransferScreen;
import com.ceesolutions.samba.transactions.Model.TransactionDetails;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.OnBoomListenerAdapter;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by ceeayaz on 4/23/18.
 */

public class ActivityLogScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    final int DATE_PICKER_ID = 1;
    public String longitude1;
    public String latitude1;
    ArrayList<String> arrayList1;
    private Dialog dialog1, dialog2;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private ImageView backBtn;
    private ListView listView;
    private TransactionDetailsCustomListAdapter transactionDetailsCustomListAdapter;
    private ArrayList<TransactionDetails> transactionDetailsList;
    private TextView statusHeading;
    private LinearLayout linearLayout;
    private LayoutInflater inflater1;
    private TextView title, cashTxt, beforeTxt, afterTxt, reviewBtn, doneBtn, heading, btn, message, textView;
    private ImageView icon;
    private View view;
    private int year, day, month;
    private String monthName, yearWithTwoDigits;
    private Dialog pd;
    private int count = 0;
    private String FromDate, toDate, sFromDate, sToDate;
    private String accountNumber, branchCode, type, bal, cur;
    private ImageView notificationBtn, sambaBtn, shareBtn;
    private BoomMenuButton bmb;
    private EditText chatBotText;
    private String menu;
    private Spinner purposesSpinner;
    private String purposeSelected;
    private ArrayList<String> purpStringArrayList;
    private ArrayList<Logs> logsArrayList;
    private String[] purpose = {
            "School Fees",
            "School Fees",
            "School Fees",
            "School Fees",
            "School Fees"

    };
    private HashMap<String, String> hashMap;
    private AppPreferences appPreferences;
    private String location;
    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {


            yearWithTwoDigits = String.valueOf(selectedYear);
            monthName = getMonth(selectedMonth + 1);
            day = selectedDay;

            int mon = selectedMonth + 1;
            String monthSelect = String.valueOf(mon);
            if (day < 10) {

                if (count == 0) {
//                    statusHeading.setText(new StringBuilder().append("0").append(day)
//                            .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)));

                    FromDate = new StringBuilder().append("0").append(day).append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)).toString();
                    count++;
                    Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

                    if (mon < 10) {

                        sFromDate = yearWithTwoDigits + "-" + "0" + monthSelect + "-" + "0" + selectedDay;
                    } else {

                        sFromDate = yearWithTwoDigits + "-" + monthSelect + "-" + "0" + selectedDay;
                    }
                    DatePickerDialog dialog = new DatePickerDialog(ActivityLogScreen.this, this,
                            calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH));
                    Calendar today = Calendar.getInstance();
                    Date currentDate = today.getTime();
                    dialog.getDatePicker().setMaxDate(currentDate.getTime());
                    dialog.show();
                } else {

                    toDate = new StringBuilder().append("0").append(day).append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)).toString();
                    statusHeading.setText(FromDate + " - " + toDate);
                    count = 0;
                    if (mon < 10) {

                        sToDate = yearWithTwoDigits + "-" + "0" + monthSelect + "-" + "0" + selectedDay;
                    } else {

                        sToDate = yearWithTwoDigits + "-" + monthSelect + "-" + "0" + selectedDay;
                    }

                    Calendar today = Calendar.getInstance();
                    today.set(Calendar.MILLISECOND, 0);
                    today.set(Calendar.SECOND, 0);
                    today.set(Calendar.MINUTE, 0);
                    today.set(Calendar.HOUR_OF_DAY, 0);
                    Date currentDate1;
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date mDate;
                    long timeInMilliseconds = 0, currentTimeToMili = 0;
                    try {
                        mDate = sdf.parse(sToDate);
                        timeInMilliseconds = mDate.getTime();
                        currentDate1 = sdf.parse(sFromDate);
                        currentTimeToMili = currentDate1.getTime();
//                        System.out.println("Date in milli :: " + timeInMilliseconds);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (currentTimeToMili <= timeInMilliseconds)
                        GetLogs(sFromDate, sToDate);
                    else
                        utils.showDilogForError("From date cannont be less than To date", "WARNING");
                }

            } else {

                if (count == 0) {

                    FromDate = new StringBuilder().append(day)
                            .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)).toString();
                    count++;

                    if (mon < 10) {

                        sFromDate = yearWithTwoDigits + "-" + "0" + monthSelect + "-" + selectedDay;
                    } else {

                        sFromDate = yearWithTwoDigits + "-" + monthSelect + "-" + selectedDay;
                    }
                    Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

                    DatePickerDialog dialog = new DatePickerDialog(ActivityLogScreen.this, this,
                            calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH));
                    Calendar today = Calendar.getInstance();
                    Date currentDate = today.getTime();
                    dialog.getDatePicker().setMaxDate(currentDate.getTime());
                    dialog.show();

                } else {

                    toDate = new StringBuilder().append(day)
                            .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)).toString();
                    statusHeading.setText(FromDate + " - " + toDate);
                    count = 0;
                    if (mon < 10) {

                        sToDate = yearWithTwoDigits + "-" + "0" + monthSelect + "-" + selectedDay;
                    } else {

                        sToDate = yearWithTwoDigits + "-" + monthSelect + "-" + selectedDay;
                    }

                    Calendar today = Calendar.getInstance();
                    today.set(Calendar.MILLISECOND, 0);
                    today.set(Calendar.SECOND, 0);
                    today.set(Calendar.MINUTE, 0);
                    today.set(Calendar.HOUR_OF_DAY, 0);
                    Date currentDate1;
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date mDate;
                    long timeInMilliseconds = 0, currentTimeToMili = 0;
                    try {
                        mDate = sdf.parse(sToDate);
                        timeInMilliseconds = mDate.getTime();
                        currentDate1 = sdf.parse(sFromDate);
                        currentTimeToMili = currentDate1.getTime();
//                        System.out.println("Date in milli :: " + timeInMilliseconds);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (currentTimeToMili <= timeInMilliseconds)
                        GetLogs(sFromDate, sToDate);
                    else
                        utils.showDilogForError("From date cannont be less than To date", "WARNING");


                }
//                statusHeading.setText(new StringBuilder().append(day)
//                        .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)));

            }
            // Show selected date

//            dob = editDOB.getText().toString();
//            editMobNumber.requestFocus();
        }
    };

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        initViews();
        getSupportActionBar().hide();

        menu = constants.sharedPreferences.getString("Menus", "N/A");
        if (!TextUtils.isEmpty(menu) && !menu.equals("N/A") && !menu.equals("")) {


        } else {

            menu = "N/A";
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        reviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SelectDate();
            }
        });
    }

//    public void AccountDetails(final String accountNumber, final String branchCode, final String type, final String bal, final String cur) {
//
//        try {
//
//            if (helper.isNetworkAvailable()) {
//                pd.show();
//
//
//                final String md5 = helper.md5(constants.sharedPreferences.getString("userName", "N/A").toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
//                HttpsTrustManager.allowMySSL(ActivityLogScreen.this);
//                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.miniStatementURL,
//                        new Response.Listener<String>() {
//                            @Override
//                            public void onResponse(String response) {
//                                // response
//
//                                Log.d("Response---", response);
//
//                                try {
//                                    JSONObject jsonObject = new JSONObject(response);
//                                    JSONObject JsonObj = new JSONObject();
//                                    if (jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("Status_Code").equals("00")) {
//
//
//                                        JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("MiniStatement"));
//
//                                        transactionDetailsList = new ArrayList<>();
//                                        transactionDetailsList.clear();
//                                        listView.setAdapter(null);
//                                        if (jsonArray.length() > 0) {
//
//                                            for (int i = 0; i < jsonArray.length(); i++) {
//                                                try {
//                                                    JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
//
//                                                    TransactionDetails transactionDetails = new TransactionDetails();
//                                                    transactionDetails.setTitle(jsonObj.getString("Description"));
//                                                    Long date = Long.valueOf(jsonObj.getString("DateTime"));
//                                                    String value = jsonObj.getString("ValueDate");
//                                                    String year = value.substring(0, 4);
//                                                    String month = value.substring(4, 6);
//                                                    String datetype = value.substring(6, 8);
//                                                    String formatedDate = year + "-" + month + "-" + datetype;
//                                                    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
//                                                    Date mDate;
//                                                    long timeInMilliseconds = 0;
//                                                    try {
//
//                                                        mDate = sdf1.parse(formatedDate);
//                                                        timeInMilliseconds = mDate.getTime();
//
//                                                    } catch (Exception e) {
//
//                                                        e.printStackTrace();
//                                                    }
//
//                                                    Long valueDate = Long.valueOf(jsonObj.getString("ValueDate"));
//                                                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
//                                                    String date1 = getDate(date, "dd-MMM-yyyy");
//                                                    transactionDetails.setDate(date1);
//                                                    transactionDetails.setAccNumber(jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("AccountID"));
//                                                    transactionDetails.setCurrency(cur);
//                                                    transactionDetails.setValueDate(getDate(timeInMilliseconds, "dd-MMM-yyyy"));
//                                                    transactionDetails.setAfterAmount(jsonObj.getString("Balance"));
//                                                    transactionDetails.setDeposit(jsonObj.getString("Deposits"));
//                                                    transactionDetails.setWithDraw(jsonObj.getString("Withdrawals"));
//                                                    transactionDetails.setRequestNumber(jsonObj.getString("Reference"));
//                                                    transactionDetailsList.add(transactionDetails);
//
//
//                                                } catch (JSONException ex) {
//
//                                                    ex.printStackTrace();
//                                                    pd.dismiss();
//                                                }
//
//                                            }
////                                            render(transactionDetailsList);
//                                            transactionDetailsCustomListAdapter = new TransactionDetailsCustomListAdapter(transactionDetailsList, ActivityLogScreen.this);
//                                            listView.setAdapter(transactionDetailsCustomListAdapter);
//                                            pd.dismiss();
//
//                                        } else {
//
//                                            pd.dismiss();
//                                        }
//
//                                    } else if (jsonObject.getJSONObject("Signs_ApplicationLogin_FingerPrintResult").getString("Status_Code").equals("56")) {
//                                        pd.dismiss();
//                                        utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogin_FingerPrintResult").getString("Status_Description"), "ERROR", ActivityLogScreen.this, new LoginScreen());
//
//
//                                    } else {
//                                        pd.dismiss();
//                                        utils.showDilogForError(jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("Status_Description"), "ERROR");
//
//                                    }
//
//
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//                                }
//
//                            }
//                        },
//                        new Response.ErrorListener() {
//                            @Override
//                            public void onErrorResponse(VolleyError error) {
//                                // error
//                                Log.d("Error.Response", "---" + error.getMessage());
//                                try {
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//                                }
//                            }
//                        }
//                ) {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sDeviceLatitude", "0");
//                        params.put("sDeviceLongitude", "0");
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sT24_AccountNumber", accountNumber);
//                        params.put("sBranchCode", branchCode);
//                        params.put("sServerSessionKey", md5);
//
//
//                        return params;
//                    }
//                };
//
//
//                requestQueue.add(postRequest);
//                postRequest.setRetryPolicy(new RetryPolicy() {
//                    @Override
//                    public int getCurrentTimeout() {
//                        return 50000;
//                    }
//
//                    @Override
//                    public int getCurrentRetryCount() {
//                        return 50000;
//                    }
//
//                    @Override
//                    public void retry(VolleyError error) throws VolleyError {
//
//                    }
//                });
//            } else {
//                pd.dismiss();
////                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
//
//            }
//        } catch (Exception e) {
//
//
//            pd.dismiss();
//            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
//
//        }
//    }

    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                pd.dismiss();

                spinnerAdapter purposeAdapter = new spinnerAdapter(ActivityLogScreen.this, R.layout.custom_textview_fp);
                purposeAdapter.addAll(purpose);
                purposeAdapter.add("Purpose of Transfer");
                purposesSpinner.setAdapter(purposeAdapter);
                purposesSpinner.setSelection(0);
                purposesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {

                            ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
//                            ((TextView) parent.getChildAt(0)).setTextSize(16);
                            // Get select item
                            if (purposesSpinner.getSelectedItem() == "Purpose of Transfer") {
                                purposeSelected = purposesSpinner.getSelectedItem().toString();
                                Log.d("identity", "---" + purposeSelected);

                            } else {

                                purposeSelected = purposesSpinner.getSelectedItem().toString();
//                                GetLogs();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });


            }

        }, 4000);
    }

    public void initViews() {

        Intent intent = getIntent();
        hashMap = new HashMap<>();
        arrayList1 = new ArrayList<>();
        logsArrayList = new ArrayList<>();
        purpStringArrayList = new ArrayList<>();
        purposesSpinner = (Spinner) findViewById(R.id.purposeSpinner);
        transactionDetailsList = new ArrayList<>();
        reviewBtn = (TextView) findViewById(R.id.reviewBtn);
        sambaBtn = (ImageView) findViewById(R.id.sambaBtn);
        chatBotText = (EditText) findViewById(R.id.chatBoxText);
        shareBtn = (ImageView) findViewById(R.id.shareBtn);
        listView = (ListView) findViewById(R.id.listView);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(ActivityLogScreen.this);
        helper = Helper.getHelper(this);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        statusHeading = (TextView) findViewById(R.id.statusHeading);
        utils = new Utils(ActivityLogScreen.this);
        pd = new Dialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        appPreferences = new AppPreferences(ActivityLogScreen.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        bmb = (BoomMenuButton) findViewById(R.id.bmb);
        assert bmb != null;
        bmb.setButtonEnum(ButtonEnum.TextOutsideCircle);
        bmb.setBackgroundColor(Color.TRANSPARENT);
        bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_9_1);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_9_1);
        GetPurposeList();

        chatBotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDilogForErrorForLogin(getResources().getString(R.string.message), "WARNING");
                Intent i = new Intent(ActivityLogScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(i);
                finish();
                finishAffinity();
            }
        });


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
//                AccountDetails(accountNumber, branchCode, type, bal, cur);

            }
        }, 500);
        sambaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                bmb.boom();
            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        bmb.setOnBoomListener(new OnBoomListenerAdapter() {
            @Override
            public void onBoomWillShow() {
                super.onBoomWillShow();
                // logic here
            }
        });

        bmb.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {
                // If you have implement listeners for boom-buttons in builders,
                // then you shouldn't add any listener here for duplicate callbacks.

                switch (index) {


                    case 0:

                        Intent home = new Intent(ActivityLogScreen.this, HomeScreen.class);
                        overridePendingTransition(0, 0);
                        startActivity(home);
                        finish();
                        break;

                    case 1:

                        if (!menu.equals("N/A")) {

                            if (menu.toLowerCase().contains("accounts")) {
                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                            } else {

                                Intent intent = new Intent(ActivityLogScreen.this, AccountsScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
//                                finish();
                            }
                        } else {
                            Intent intent = new Intent(ActivityLogScreen.this, AccountsScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(intent);
//                            finish();

                        }

                        break;

                    case 2:

                        try {

                            CryptLib _crypt = new CryptLib();
                            Intent transfers = new Intent(ActivityLogScreen.this, FundTransferScreen.class);

                            String user = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                            if (!TextUtils.isEmpty(user) && !user.equals("N/A")) {

                                if (user.equals("NON_SAMBA")) {

                                    transfers.putExtra("requestFunds", "nonSamba");
                                }
                            }
                            overridePendingTransition(0, 0);
                            startActivity(transfers);
//                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;


                    case 3:

                        try {
                            CryptLib _crypt = new CryptLib();
                            if (!menu.equals("N/A")) {

                                if (menu.toLowerCase().contains("biller management")) {
                                    showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                } else {
                                    String user1 = constants.sharedPreferences.getString("type", "N/A");
                                    Intent payments = new Intent(ActivityLogScreen.this, PayBillsScreen.class);
                                    overridePendingTransition(0, 0);
                                    user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                    if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                        if (user1.equals("NON_SAMBA")) {

                                            payments.putExtra("requestbill", "nonSamba");
                                        }
                                    }
                                    startActivity(payments);
//                                    finish();
                                }
                            } else {
                                Intent payments = new Intent(ActivityLogScreen.this, PayBillsScreen.class);
                                overridePendingTransition(0, 0);
                                String user1 = constants.sharedPreferences.getString("type", "N/A");
                                user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                    if (user1.equals("NON_SAMBA")) {

                                        payments.putExtra("requestbill", "nonSamba");
                                    }
                                }
                                startActivity(payments);
//                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;


                    case 4:

                        if (!menu.equals("N/A")) {

                            if (menu.toLowerCase().contains("beneficiary management")) {
                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                            } else {

                                Intent bene = new Intent(ActivityLogScreen.this, ManageBeneficiaresScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(bene);
//                                finish();
                            }
                        } else {
                            Intent bene = new Intent(ActivityLogScreen.this, ManageBeneficiaresScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(bene);
//                            finish();
                        }

                        break;


                    case 5:

                        try {

                            CryptLib _crypt = new CryptLib();

                            String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                            if (userType.equals("NON_SAMBA"))
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            else {
                                Intent intent = new Intent(ActivityLogScreen.this, MainActivity.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("friends management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//
//                                Intent friends = new Intent(ActivityLogScreen.this, ManageFriends.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(friends);
////                                finish();
//                            }
//                        } else {
//                            Intent friends = new Intent(ActivityLogScreen.this, ManageFriends.class);
//                            overridePendingTransition(0, 0);
//                            startActivity(friends);
////                            finish();
//                        }

                        break;


                    case 6:

                        try {

                            CryptLib _crypt = new CryptLib();

                            String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                            if (userType.equals("NON_SAMBA"))
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            else {
                                Intent intent = new Intent(ActivityLogScreen.this, EatMubarakMainScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        break;



//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("cards management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//
//                                Intent cards = new Intent(ActivityLogScreen.this, ManageCardsScreen.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(cards);
////                                finish();
//                            }
//                        } else {
//                            Intent cards = new Intent(ActivityLogScreen.this, ManageCardsScreen.class);
//                            overridePendingTransition(0, 0);
//                            startActivity(cards);
////                            finish();
//                        }


                    case 7:


                        try {

                            CryptLib _crypt = new CryptLib();

                            String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                            if (userType.equals("NON_SAMBA"))
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            else {
                                Intent atm = new Intent(ActivityLogScreen.this, BookMeMainScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(atm);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        finish();
                        break;

                    case 8:

                        pd.show();
                        showDialog("Are you sure you want to Logout?");
                        break;


                    default: {

                        break;
                    }
                }
            }

            @Override
            public void onBackgroundClick() {
//                textViewForAnimation.setText("Click background!!!");
            }

            @Override
            public void onBoomWillHide() {
                Log.d("BMB", "onBoomWillHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will RE-BOOM!!!");
            }

            @Override
            public void onBoomDidHide() {
                Log.d("BMB", "onBoomDidHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did RE-BOOM!!!");
            }

            @Override
            public void onBoomWillShow() {
                Log.d("BMB", "onBoomWillShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will BOOM!!!");
            }

            @Override
            public void onBoomDidShow() {
                Log.d("BMB", "onBoomDidShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did BOOM!!!");
            }
        });

//        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++)
//            bmb.addBuilder(BuilderManager.getSimpleCircleButtonBuilder());

        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder()
                    .listener(new OnBMClickListener() {
                        @Override
                        public void onBoomButtonClick(int index) {
                            // When the boom-button corresponding this builder is clicked.
//                            Toast.makeText(HomeScreen.this, "Clicked " + index, Toast.LENGTH_SHORT).show();
                        }
                    });

//            int margin = getResources().getDimensionPixelSize(R.dimen._4sdp);
            builder.textSize(12);

            switch (i) {

                case 0:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Home");
                    builder.unableText("Home");
                    builder.normalText("Home");
                    break;

                case 1:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Accounts");
                    builder.unableText("Accounts");
                    builder.normalText("Accounts");
                    break;

                case 2:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Transfers");
                    builder.unableText("Transfers");
                    builder.normalText("Transfers");
                    break;


                case 3:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Payments");
                    builder.unableText("Payments");
                    builder.normalText("Payments");
                    break;


                case 4:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Beneficiaries");
                    builder.unableText("Beneficiaries");
                    builder.normalText("Beneficiaries");
                    break;


                case 5:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("QR Payments");
                    builder.unableText("QR Payments");
                    builder.normalText("QR Payments");

//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Friends");
//                    builder.unableText("Friends");
//                    builder.normalText("Friends");
                    break;


                case 6:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Food");
                    builder.unableText("Food");
                    builder.normalText("Food");

//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Cards");
//                    builder.unableText("Cards");
//                    builder.normalText("Cards");
                    break;


                case 7:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("E-Tickets");
                    builder.unableText("E-Tickets");
                    builder.normalText("E-Tickets");

//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.atm, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.atm, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.atm, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Locate Us");
//                    builder.unableText("Locate Us");
//                    builder.normalText("Locate Us");
                    break;

                case 8:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Logout");
                    builder.unableText("Logout");
                    builder.normalText("Logout");
                    break;


                default: {

                    break;
                }

            }
//            builder.normalImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.unableImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.normalColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedColor(getResources().getColor(R.color.notiheader));
//            builder.unableColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedText("Cards");
//            builder.unableText("Cards");
//            builder.normalText("Cards");
//            builder.imagePadding(new Rect(40, 40, 40, 40));
            bmb.addBuilder(builder);
        }


    }

    private void SelectDate() {

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


        showDialog(DATE_PICKER_ID);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:

                DatePickerDialog datePickerDialog = new DatePickerDialog(this, pickerListener, year, month, day);
                Calendar today = Calendar.getInstance();
                Date currentDate = today.getTime();
                datePickerDialog.getDatePicker().setMaxDate(currentDate.getTime());
                return datePickerDialog;
        }
        return null;
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }


    public void showDilogForErrorForLogin(String msg, String header) {

        dialog1 = new Dialog(ActivityLogScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        message = (TextView) dialog1.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog1.findViewById(R.id.doneBtn);
        heading = (TextView) dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(true);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
                Intent i = new Intent(ActivityLogScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(i);
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog1.show();


    }

    public void showDilogForError(String msg, String header) {

        dialog1 = new Dialog(ActivityLogScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog1.findViewById(R.id.validatingTxt);
        btn = (TextView) dialog1.findViewById(R.id.doneBtn);
        heading = (TextView) dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog1.show();

    }

    private void showDialog(String message) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Alert:");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

//                constants.editor.clear();
//                constants.editor.commit();
//                constants.editor.apply();
//                constants.nonSambaCredentialsEditor.clear();
//                constants.nonSambaCredentialsEditor.commit();
//                constants.nonSambaCredentialsEditor.apply();
//                constants.destinationAccountEditor.clear().commit();
//                constants.destinationAccountEditor.apply();
//                constants.sourceAccountEditor.clear().commit();
//                constants.sourceAccountEditor.apply();
//                constants.accountListEditor.clear().commit();
//                constants.accountListEditor.apply();
//                constants.accountsEditor.clear().commit();
//                constants.accountsEditor.apply();
//                constants.forgotPasswordEditor.clear().commit();
//                constants.forgotPasswordEditor.apply();
//                Handler handler1 = new Handler();
//                handler1.postDelayed(new Runnable() {
//                    public void run() {
//
//                        pd.dismiss();
//                        Intent intent = new Intent(ActivityLogScreen.this, LoginScreen.class);
//                        startActivity(intent);
//                        overridePendingTransition(0, 0);
//                        finish();
//                        finishAffinity();
//
//                    }
//                }, 1000);
                Logout();
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                pd.dismiss();
            }
        });

        builder.show();
    }

    public void Logout() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSessionID", Constants.Shared_KEY);
                    HttpsTrustManager.allowMySSL(ActivityLogScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.logoutURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Code").equals("00")) {
                                                constants.editor.clear();
                                                constants.editor.commit();
                                                constants.editor.apply();
                                                constants.nonSambaCredentialsEditor.clear();
                                                constants.nonSambaCredentialsEditor.commit();
                                                constants.nonSambaCredentialsEditor.apply();
                                                constants.destinationAccountEditor.clear().commit();
                                                constants.destinationAccountEditor.apply();
                                                constants.sourceAccountEditor.clear().commit();
                                                constants.sourceAccountEditor.apply();
                                                constants.accountListEditor.clear().commit();
                                                constants.accountListEditor.apply();
                                                constants.accountsEditor.clear().commit();
                                                constants.accountsEditor.apply();
                                                constants.forgotPasswordEditor.clear().commit();
                                                constants.forgotPasswordEditor.apply();
                                                constants.purposeBillList.clear().commit();
                                                constants.purposeBillList.apply();
                                                constants.purposePrepaidList.clear().commit();
                                                constants.purposePrepaidList.apply();
                                                constants.purposeListEditor.clear().commit();
                                                constants.purposeListEditor.apply();
                                                pd.dismiss();
                                                Intent intent = new Intent(ActivityLogScreen.this, LoginScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                finish();
                                                finishAffinity();


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Description"), "ERROR", ActivityLogScreen.this, new LoginScreen());

                                            }

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sSessionID", Constants.Shared_KEY);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity", "WARNING");


            }
        } catch (Exception e) {

            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity", "WARNING");


        }
    }

    public void GetPurposeList() {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    pd.show();
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sSigns_UserType", _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(ActivityLogScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getActivityLogTypeURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_UserActivityLogTypeListResult").getString("Status_Code").equals("00")) {

                                                int count = 0;

                                                purpStringArrayList = new ArrayList<>();
                                                purpStringArrayList.clear();
                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_UserActivityLogTypeListResult").getString("ActivityList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        if (count == 0) {

                                                            purpStringArrayList.add("All");
                                                            hashMap.put("All", "");
                                                            count++;
                                                        } else {
                                                            purpStringArrayList.add(jsonObj.getString("ActivityText"));

                                                            hashMap.put(jsonObj.getString("ActivityText"), jsonObj.getString("ActivityCode"));
                                                        }
                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();

                                                    }

                                                }

                                                purpose = new String[purpStringArrayList.size()];
                                                purpose = purpStringArrayList.toArray(purpose);
                                                showSpinner();


                                            } else if (jsonObject.getJSONObject("Signs_UserActivityLogTypeListResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_UserActivityLogTypeListResult").getString("Status_Description"), "ERROR", ActivityLogScreen.this, new LoginScreen());


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_UserActivityLogTypeListResult").getString("Status_Description"), "ERROR");

                                            }

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "ERROR");

            }
        } catch (Exception e) {
            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "ERROR");

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();

        }
    }

    public void GetLogs(final String fromDate, final String toDate) {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    pd.show();
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    final String key = hashMap.get(purposeSelected);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sFromDate", fromDate);
                    params.put("sSigns_UserType", _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sToDate", toDate);
                    params.put("sFromTime", "0");
                    params.put("sToTime", "24");
                    params.put("sActivityLogId", key);
                    params.put("sServerSessionKey", md5);
                    Log.d("id--", key);
                    HttpsTrustManager.allowMySSL(ActivityLogScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.activityLogsURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_UserActivityLogListResult").getString("Status_Code").equals("00")) {

                                                logsArrayList = new ArrayList<>();
                                                logsArrayList.clear();
                                                listView.setAdapter(null);
                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_UserActivityLogListResult").getString("ActivityList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        Logs logs = new Logs();
                                                        logs.setMethod(jsonObj.getString("ActivityMethod"));
                                                        logs.setTypeDesc(jsonObj.getString("ActivityTypeDesc"));
                                                        logs.setTypeID(jsonObj.getString("ActivityTypeID"));
                                                        logs.setCountry(jsonObj.getString("Country"));
                                                        logs.setIpAdd(jsonObj.getString("IPAddress"));
                                                        logs.setLogDate(jsonObj.getString("LogDate"));
                                                        logs.setLogId(jsonObj.getString("LogID"));
                                                        logs.setLoginTime(jsonObj.getString("LoginTime"));
                                                        logs.setMacAdd(jsonObj.getString("MacAddress"));
                                                        logs.setPageName(jsonObj.getString("PageName"));
                                                        logs.setUser(jsonObj.getString("Signs_Username"));
                                                        logs.setDeviceName(jsonObj.getString("DeviceName"));
                                                        logsArrayList.add(logs);


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        pd.dismiss();
                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                                    }

                                                }

                                                ActivityLogCustomListAdapter activityLogCustomListAdapter = new ActivityLogCustomListAdapter(logsArrayList, ActivityLogScreen.this);
                                                listView.setAdapter(activityLogCustomListAdapter);
                                                pd.dismiss();
//                                            purpose = new String[purpStringArrayList.size()];
//                                            purpose = purpStringArrayList.toArray(purpose);
//                                            showSpinner();


                                            } else if (jsonObject.getJSONObject("Signs_UserActivityLogListResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_UserActivityLogListResult").getString("Status_Description"), "ERROR", ActivityLogScreen.this, new LoginScreen());


                                            } else {
                                                pd.dismiss();
                                                logsArrayList = new ArrayList<>();
                                                logsArrayList.clear();
                                                listView.setAdapter(null);
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_UserActivityLogListResult").getString("Status_Description"), "ERROR");

                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sFromDate", fromDate);
//                            params.put("sToDate", toDate);
//                            params.put("sFromTime", "0");
//                            params.put("sToTime", "24");
//                            params.put("sActivityLogId", key);
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };
                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "ERROR");

            }
        } catch (Exception e) {
            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "ERROR");

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
