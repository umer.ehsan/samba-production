package com.ceesolutions.samba.accessControl;

/**
 * Created by ceeayaz on 2/23/18.
 */

import android.os.Bundle;

import android.content.Intent;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.Adapters.FragmentAdapter;

/**
 * Created by ayaz on 12/21/17.
 */

public class OldRegistrationScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.old_registration_screen);
        getSupportActionBar().hide();
        TextView textView = (TextView) findViewById(R.id.text);
        textView.bringToFront();

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("CNIC"));
        tabLayout.addTab(tabLayout.newTab().setText("SCNIC"));
        tabLayout.addTab(tabLayout.newTab().setText("NTN"));
        tabLayout.addTab(tabLayout.newTab().setText("PASSPORT"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final FragmentAdapter adapter = new FragmentAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(OldRegistrationScreen.this, LoginScreen.class);
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
    }
}