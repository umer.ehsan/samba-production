package com.ceesolutions.samba.accessControl.NonSambaUser;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;

/**
 * Created by ayaz on 1/9/18.
 */

public class NonSambaCompletionScreen extends AppCompatActivity {

    private TextView validateBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.non_samba_completion_screen);
        initViews();
        getSupportActionBar().hide();
    }

    public void initViews() {

        validateBtn = (TextView) findViewById(R.id.validateBtn);

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(NonSambaCompletionScreen.this, LoginScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //Do Nothing
    }
}
