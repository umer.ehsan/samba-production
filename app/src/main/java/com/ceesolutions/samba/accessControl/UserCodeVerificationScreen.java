package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ayaz on 12/21/17.
 */

public class UserCodeVerificationScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private EditText editSmsCode, editEmailCode;
    private TextView validateBtn, regenerateBtn, errorMessageForSms, errorMessageForEmail;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private String identityValue, fullName, T24_CIF, mobileNo, email, smsCode, emailCode, identityType, type, isFP;
    private Dialog pd;
    private Constants constants;
    private TextView text2;
    private Utils utils;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_code_verification);
        initViews();
        getSupportActionBar().hide();
    }

    public void initViews() {

        Intent intent = getIntent();
        String backPressed = intent.getStringExtra("BackPressed");
        type = intent.getStringExtra("Type");
        isFP = intent.getStringExtra("FP");

        validateBtn = (TextView) findViewById(R.id.validateBtn);
        text2 = (TextView) findViewById(R.id.text2);
        regenerateBtn = (TextView) findViewById(R.id.regenerateBtn);
        errorMessageForSms = (TextView) findViewById(R.id.errorMessageForSms);
        errorMessageForEmail = (TextView) findViewById(R.id.errorMessageForEmail);
        editSmsCode = (EditText) findViewById(R.id.editSmsCode);
        editEmailCode = (EditText) findViewById(R.id.editEmailCode);
        utils = new Utils(UserCodeVerificationScreen.this);
        constants = Constants.getConstants(UserCodeVerificationScreen.this);
        webService = new WebService();
        helper = Helper.getHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        pd = new Dialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        String txt = "<font COLOR=\'#034ea2\'><b>" + "One Time Authentication Code (OTAC) " +"<font COLOR=\'#353535\'>" + "has been sent to your mobile number and email address. Please enter OTAC in the respective fields below to Proceed. OTAC is valid only for next 4 minutes." + "</font>";
        String txt = "<font COLOR=\'#353535\'><b>" + "One Time Authentication Code (OTAC) " + "</b></font>" + "<font COLOR=\'#353535\'>" + "has been sent to your mobile number and email address." + "<br></font>" + "<font COLOR=\'#353535\'><br>" + " Please enter OTAC in the respective fields below to Proceed. OTAC is valid only for next 4 minutes." + "</font>";

        text2.setText(Html.fromHtml(txt));

        appPreferences = new AppPreferences(UserCodeVerificationScreen.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

        if (!TextUtils.isEmpty(isFP) && isFP.equals("FP")) {

            identityType = constants.forgotPasswordCredentials.getString("identityType", "N/A");
            identityValue = constants.forgotPasswordCredentials.getString("identityValue", "N/A");
            email = constants.forgotPasswordCredentials.getString("email", "N/A");
            mobileNo = constants.forgotPasswordCredentials.getString("mobileNo", "N/A");
            fullName = constants.forgotPasswordCredentials.getString("fullName", "N/A");
//            mobileNo = "0000";

        } else {
//        if (!TextUtils.isEmpty(backPressed)) {
//            //Do Nothing
//        } else {
            identityType = constants.credentialsSharedPreferences.getString("identityType", "N/A");
//            if (identityType.equals("NTN")) {
//                identityValue = constants.credentialsSharedPreferences.getString("ntn", "N/A");
//            } else if (identityType.equals("SCNIC")) {
//                identityValue = constants.credentialsSharedPreferences.getString("scnic", "N/A");
//            } else if (identityType.equals("PASSPORT")) {
//                identityValue = constants.credentialsSharedPreferences.getString("passport", "N/A");
//            } else if (identityType.equals("CNIC")) {
//                identityValue = constants.credentialsSharedPreferences.getString("cnic", "N/A");
//
//            }
            identityValue = constants.credentialsSharedPreferences.getString("identityValue", "N/A");
            fullName = intent.getStringExtra("fullName");
            T24_CIF = intent.getStringExtra("T24_CIF");
            mobileNo = intent.getStringExtra("mobileNo");
            email = intent.getStringExtra("email");

        }

        smsCode = editSmsCode.getText().toString().trim();
        emailCode = editEmailCode.getText().toString().trim();


        editSmsCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForSms.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessageForSms.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                smsCode = editable.toString();
            }
        });

        editEmailCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessageForEmail.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessageForEmail.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                emailCode = editable.toString();
            }
        });


        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                FirebaseAnalytics.getInstance(UserCodeVerificationScreen.this).logEvent("Reset_Password_OTAC_Pressed", new Bundle());

                smsCode = editSmsCode.getText().toString().trim();
                emailCode = editEmailCode.getText().toString().trim();

                if (TextUtils.isEmpty(smsCode) || smsCode.length() < 3) {
                    errorMessageForSms.setVisibility(View.VISIBLE);
                    errorMessageForSms.bringToFront();
                    errorMessageForSms.setError("");
                    errorMessageForSms.setText("Please enter valid OTAC (Mobile)");

                } else if (!helper.validateInputForSC(smsCode) && smsCode.contains(" ")) {

                    errorMessageForSms.setVisibility(View.VISIBLE);
                    errorMessageForSms.bringToFront();
                    errorMessageForSms.setError("");
                    errorMessageForSms.setText("OTAC (Mobile) cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                }

                if (TextUtils.isEmpty(emailCode) || emailCode.length() < 3) {
                    errorMessageForEmail.setVisibility(View.VISIBLE);
                    errorMessageForEmail.bringToFront();
                    errorMessageForEmail.setError("");
                    errorMessageForEmail.setText("Please enter valid OTAC (Email)");
                } else if (!helper.validateInputForSC(emailCode) && emailCode.contains(" ")) {

                    errorMessageForEmail.setVisibility(View.VISIBLE);
                    errorMessageForEmail.bringToFront();
                    errorMessageForEmail.setError("");
                    errorMessageForEmail.setText("OTAC (Email) cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                }

                if ((!TextUtils.isEmpty(smsCode) && smsCode.length() == 3 && helper.validateInputForSC(smsCode) && !smsCode.contains(" ")) && (!TextUtils.isEmpty(emailCode) && emailCode.length() == 3) && helper.validateInputForSC(emailCode) && !emailCode.contains(" ")) {

//                    Intent intent = new Intent(UserCodeVerificationScreen.this, UserConfirmationScreen.class);
//                    startActivity(intent);
                    validateBtn.setEnabled(false);
                    pd.show();
                    UserCodeValidation();
                }

            }
        });

        regenerateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                regenerateBtn.setEnabled(false);
                pd.show();
                RegeneratePin();
            }
        });

    }

    public void UserCodeValidation() {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();
                String combineString = identityValue + smsCode + emailCode;
                final String pinData = helper.md5(combineString);

//                webService.UserCodeValidation(pinData, identityValue, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getValCodeReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//                                pd.dismiss();
////                                Toast.makeText(UserCodeVerificationScreen.this, "Invalid OTAC!", Toast.LENGTH_LONG).show();
//                                utils.showDilogForError("Invalid OTAC!", "ERROR");
//                                validateBtn.setEnabled(true);
//
//                                clearViews();
//
//
//                            } else {
//
//
//                                pd.dismiss();
//
//                                if (!TextUtils.isEmpty(isFP) && isFP.equals("FP")) {
//
////                                    Toast.makeText(UserCodeVerificationScreen.this, "OTAC Verified Successfully.", Toast.LENGTH_LONG).show();
//                                    clearViews();
//                                    Intent intent = new Intent(UserCodeVerificationScreen.this, UpdateUserPasswordScreen.class);
//                                    startActivity(intent);
//                                    overridePendingTransition(0, 0);
//                                    finish();
//                                    validateBtn.setEnabled(true);
//                                } else {
//
////                                    Toast.makeText(UserCodeVerificationScreen.this, "OTAC Verified Successfully.", Toast.LENGTH_LONG).show();
//                                    clearViews();
//                                    Intent intent = new Intent(UserCodeVerificationScreen.this, UserConfirmationScreen.class);
//                                    intent.putExtra("fullName", fullName);
////                                if (identityType.equals("NTN")) {
//                                    intent.putExtra("identityValue", identityValue);
////                                } else if (identityType.equals("SCNIC")) {
//
////                                    intent.putExtra("scnic", identityValue);
////                                } else if (identityType.equals("PASSPORT")) {
////
////                                    intent.putExtra("passport", identityValue);
////                                } else if (identityType.equals("CNIC")) {
////                                    intent.putExtra("cnic", identityValue);
////
////                                }
//                                    intent.putExtra("identityType", identityType);
//                                    intent.putExtra("Type", type);
//                                    intent.putExtra("T24_CIF", T24_CIF);
//                                    intent.putExtra("mobileNo", mobileNo);
//                                    intent.putExtra("email", email);
//                                    startActivity(intent);
//                                    overridePendingTransition(0, 0);
//                                    finish();
//                                    validateBtn.setEnabled(true);
//                                }
//
//
//                            }
//                        } else {
////                            Toast.makeText(UserCodeVerificationScreen.this, "Invalid OTAC!", Toast.LENGTH_LONG).show();
//                            validateBtn.setEnabled(true);
//                            pd.dismiss();
//                            utils.showDilogForError("Invalid OTAC!", "ERROR");
//                            clearViews();
//
//
//                        }
//                    }
//                }, 5000);
                final String md5 = helper.md5("" + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", constants.Old_Shared_Key);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sLegalIdentityValue", identityValue);
                params.put("sPinData", pinData);
                params.put("sSigns_Username", "");
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(UserCodeVerificationScreen.this);
                CryptLib _crypt = new CryptLib();
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.usercodeverificationURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_ValidateRegister_OTACResult").getString("Status_Code").equals("00")) {
                                            pd.dismiss();
//
                                            if (!TextUtils.isEmpty(isFP) && isFP.equals("FP")) {

//                                    Toast.makeText(UserCodeVerificationScreen.this, "OTAC Verified Successfully.", Toast.LENGTH_LONG).show();
                                                clearViews();
                                                Intent intent = new Intent(UserCodeVerificationScreen.this, UpdateUserPasswordScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                finish();
                                                validateBtn.setEnabled(true);
                                            } else {

//                                    Toast.makeText(UserCodeVerificationScreen.this, "OTAC Verified Successfully.", Toast.LENGTH_LONG).show();
                                                clearViews();
                                                Intent intent = new Intent(UserCodeVerificationScreen.this, UserConfirmationScreen.class);
                                                intent.putExtra("fullName", fullName);
//                                if (identityType.equals("NTN")) {
                                                intent.putExtra("identityValue", identityValue);
//                                } else if (identityType.equals("SCNIC")) {

//                                    intent.putExtra("scnic", identityValue);
//                                } else if (identityType.equals("PASSPORT")) {
//
//                                    intent.putExtra("passport", identityValue);
//                                } else if (identityType.equals("CNIC")) {
//                                    intent.putExtra("cnic", identityValue);
//
//                                }
                                                intent.putExtra("identityType", identityType);
                                                intent.putExtra("Type", type);
                                                intent.putExtra("T24_CIF", T24_CIF);
                                                intent.putExtra("mobileNo", mobileNo);
                                                intent.putExtra("email", email);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                finish();
                                                validateBtn.setEnabled(true);
                                            }
//
//
//                            }
                                        } else {
                                            validateBtn.setEnabled(true);
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_ValidateRegister_OTACResult").getString("Status_Description"), "ERROR");
                                            clearViews();
                                        }
                                    } else {

                                        validateBtn.setEnabled(true);
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        clearViews();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    validateBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Invalid OTAC!", "ERROR");
                                    clearViews();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    validateBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Invalid OTAC!", "ERROR");
                                    clearViews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    validateBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Invalid OTAC!", "ERROR");
                                    clearViews();
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", constants.Old_Shared_Key);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sLegalIdentityValue", identityValue);
//                        params.put("sPinData", pinData);
//                        params.put("sSigns_Username", "");
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(UserCodeVerificationScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                validateBtn.setEnabled(true);
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                clearViews();

            }
        } catch (Exception e) {

//            Toast.makeText(UserCodeVerificationScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            validateBtn.setEnabled(true);
            pd.dismiss();
            utils.showDilogForError("Invalid OTAC!", "ERROR");
            clearViews();


        }
    }


    public void RegeneratePin() {

        try {

            if (helper.isNetworkAvailable()) {

//                webService.RegeneratePinForRegistration(email, mobileNo, identityType, identityValue, constants.version, constants.model, constants.dateTime, constants.address, fullName, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getRegeneratePinRegistrationReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(UserCodeVerificationScreen.this, "OTAC has been sent!", Toast.LENGTH_LONG).show();
//                                regenerateBtn.setEnabled(true);
//                                pd.dismiss();
//                                utils.showDilogForError("Failed to generate OTAC. Please try again later.", "SUCCESS");
//                                clearViews();
//
//
//                            } else {
//
//
//                                pd.dismiss();
////                                Toast.makeText(UserCodeVerificationScreen.this, "OTAC has been sent.", Toast.LENGTH_LONG).show();
//                                regenerateBtn.setEnabled(true);
//                                utils.showDilogForError("OTAC has been sent.", "SUCCESS");
//                                clearViews();
//
//
//                            }
//                        } else {
////                            Toast.makeText(UserCodeVerificationScreen.this, "OTAC has been sent!", Toast.LENGTH_LONG).show();
//                            regenerateBtn.setEnabled(true);
//                            pd.dismiss();
//                            utils.showDilogForError("Failed to generate OTAC. Please try again later.", "SUCCESS");
//                            clearViews();
//
//
//                        }
//                    }
//                }, 5000);
                final String md5 = helper.md5("" + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", constants.Old_Shared_Key);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sEmailAddress", email);
                params.put("sMobileNumber", mobileNo);
                params.put("sIdentityType", identityType);
                params.put("sLegalIdentityValue", identityValue);
                params.put("sFullName", fullName);
                params.put("sSigns_Username", "");
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                params.put("sOptionalIpinGenerater", "Y");
                HttpsTrustManager.allowMySSL(UserCodeVerificationScreen.this);
                CryptLib _crypt = new CryptLib();
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinRegistrationURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_GenerateRegister_OTACResult").getString("Status_Code").equals("00")) {
                                            pd.dismiss();
////                                Toast.makeText(UserCodeVerificationScreen.this, "OTAC has been sent.", Toast.LENGTH_LONG).show();
                                            regenerateBtn.setEnabled(true);
                                            utils.showDilogForError("OTAC has been sent on your registered mobile and email address.", "SUCCESS");
                                            clearViews();
                                        } else {
                                            regenerateBtn.setEnabled(true);
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_GenerateRegister_OTACResult").getString("Status_Description"), "SUCCESS");
                                            clearViews();
                                        }
                                    } else {

                                        regenerateBtn.setEnabled(true);
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        clearViews();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    regenerateBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Failed to generate OTAC. Please try again later.", "SUCCESS");
                                    clearViews();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    regenerateBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Failed to generate OTAC. Please try again later.", "SUCCESS");
                                    clearViews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    regenerateBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Failed to generate OTAC. Please try again later.", "SUCCESS");
                                    clearViews();
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", constants.Old_Shared_Key);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sEmailAddress", email);
//                        params.put("sMobileNumber", mobileNo);
//                        params.put("sIdentityType", identityType);
//                        params.put("sLegalIdentityValue", identityValue);
//                        params.put("sFullName", fullName);
//                        params.put("sSigns_Username", "");
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        params.put("sOptionalIpinGenerater", "Y");
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(UserCodeVerificationScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                regenerateBtn.setEnabled(true);
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                clearViews();

            }
        } catch (Exception e) {

//            Toast.makeText(UserCodeVerificationScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            regenerateBtn.setEnabled(true);
            pd.dismiss();
            utils.showDilogForError("Failed to generate OTAC. Please try again later.", "SUCCESS");
            clearViews();


        }
    }

    public void clearViews() {

        editEmailCode.setText("");
        editSmsCode.setText("");
        editSmsCode.requestFocus();
    }

    @Override
    public void onBackPressed() {


        Intent intent = new Intent(UserCodeVerificationScreen.this, LoginScreen.class);
        constants.forgotPasswordEditor.clear().commit();
        constants.forgotPasswordEditor.apply();
        constants.credentialsEditor.clear().commit();
        constants.credentialsEditor.apply();
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();


    }

    @Override
    public void onSupportActionModeStarted(ActionMode mode) {
        super.onSupportActionModeStarted(mode);
        mode.finish();
    }
}
