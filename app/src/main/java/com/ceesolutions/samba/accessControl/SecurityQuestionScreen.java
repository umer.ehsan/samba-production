package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/18/18.
 */

public class SecurityQuestionScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    ArrayList<String> questionList = new ArrayList<String>();
    String token, target, userName, questionSelected;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private TextView validateBtn, message, doneBtn, heading, text2;
    private EditText editQuestionAnswer;
    private Spinner questionSpinner;
    private String[] quesitons;
    private String questionAnswer;
    private TextView errorMessage;
    private String questionCode;
    private ImageView backBtn, notificationBtn;
    private TextView headingTxt;
    private ImageView info;
    private Dialog dialog;
    private Utils utils;
    private AppPreferences appPreferences;
    private String location, legalID;
    private String securityCheck;
    private Intent intent1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secret_question);
        initViews();
        getSupportActionBar().hide();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(securityCheck) && securityCheck.equals("securityQuestion")) {

                } else
                    finish();
            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SecurityQuestionScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
//                finishAffinity();
            }
        });
    }


    public void initViews() {

        try {


            // new log
            FirebaseAnalytics.getInstance(SecurityQuestionScreen.this).logEvent("Security_Guide_Pressed", new Bundle());



            intent1 = getIntent();
            CryptLib _crypt = new CryptLib();
            headingTxt = (TextView) findViewById(R.id.headingTxt);
            text2 = (TextView) findViewById(R.id.text2);
            securityCheck = intent1.getStringExtra("securityCheck");
            info = (ImageView) findViewById(R.id.info);
            validateBtn = (TextView) findViewById(R.id.validateBtn);
            editQuestionAnswer = (EditText) findViewById(R.id.editQuestionAnswer);
            errorMessage = (TextView) findViewById(R.id.errorMessage);
            utils = new Utils(SecurityQuestionScreen.this);
            questionSpinner = (Spinner) findViewById(R.id.questionSpinner);
            requestQueue = Volley.newRequestQueue(SecurityQuestionScreen.this);
            backBtn = (ImageView) findViewById(R.id.backBtn);
            notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
            helper = Helper.getHelper(SecurityQuestionScreen.this);
            constants = Constants.getConstants(SecurityQuestionScreen.this);
            token = constants.sharedPreferences.getString("fcmToken", "N/A");
            target = _crypt.decrypt(constants.sharedPreferences.getString("T24_Target", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            userName = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            pd = new Dialog(SecurityQuestionScreen.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            appPreferences = new AppPreferences(this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");


            GetSecretQuestions();
            legalID = _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    showInfo();
                }
            });


            editQuestionAnswer.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    errorMessage.setVisibility(View.GONE);
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    errorMessage.setVisibility(View.GONE);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            if (!TextUtils.isEmpty(securityCheck) && securityCheck.equals("securityQuestion")) {
                backBtn.setVisibility(View.GONE);
                notificationBtn.setVisibility(View.INVISIBLE);
                validateBtn.setText("Validate");
                headingTxt.setText("Validate Identity");
                text2.setText("For security reasons, additional authentication is required to access your account.\n\nPlease select and input answer of your secret question");
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                        WindowManager.LayoutParams.FLAG_SECURE);
            }

            validateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    FirebaseAnalytics.getInstance(SecurityQuestionScreen.this).logEvent("Secret_Question_Update_Pressed",new Bundle());
                    questionAnswer = editQuestionAnswer.getText().toString().trim();

                    if (!TextUtils.isEmpty(securityCheck) && securityCheck.equals("securityQuestion")) {
                        if (questionSelected.equals("Select Question")) {

//                    Toast.makeText(getActivity(), "Please select any one question", Toast.LENGTH_SHORT).show();

                            utils.showDilogForError("Please select any one question.", "WARNING");
                        } else {

                            if (TextUtils.isEmpty(questionAnswer)) {
                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.bringToFront();
                                errorMessage.setError("");
                                errorMessage.setText("Please enter valid answer");

                            } else if (!helper.validateInputForSC(questionAnswer)) {

                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.bringToFront();
                                errorMessage.setError("");
                                errorMessage.setText("Answer cannot contains <,>,\",',%,(,),&,+,\\,~");

                            } else {
                                validateBtn.setEnabled(false);

                                if (questionSelected.equals("What is your favourite car?")) {

                                    questionCode = "SQ001";

                                } else if (questionSelected.equals("Who is your childhood best friend?")) {

                                    questionCode = "SQ002";

                                } else if (questionSelected.equals("What was the name of your first pet?")) {

                                    questionCode = "SQ003";
                                }

                                if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                                    longitude = Double.valueOf(longitude1);
                                    latitude = Double.valueOf(latitude1);
                                    if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                                    } else {
                                        location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                                    }
                                } else {

                                    longitude = 0;
                                    latitude = 0;
                                    location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
                                }
                                SubmitSecretQuestion(questionCode);
                                validateBtn.setEnabled(false);

                            }
                        }
                    } else {
                        if (questionSelected.equals("Select Question")) {

//                    Toast.makeText(getActivity(), "Please select any one question", Toast.LENGTH_SHORT).show();

                            utils.showDilogForError("Please select any one question.", "WARNING");
                        } else {

                            if (TextUtils.isEmpty(questionAnswer)) {
                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.bringToFront();
                                errorMessage.setError("");
                                errorMessage.setText("Please enter valid answer");

                            } else if (!helper.validateInputForSC(questionAnswer)) {

                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.bringToFront();
                                errorMessage.setError("");
                                errorMessage.setText("Answer cannot contains <,>,\",',%,(,),&,+,\\,~");

                            } else {
                                validateBtn.setEnabled(false);

                                if (questionSelected.equals("What is your favourite car?")) {

                                    questionCode = "SQ001";

                                } else if (questionSelected.equals("Who is your childhood best freind?")) {

                                    questionCode = "SQ002";

                                } else if (questionSelected.equals("What was the name of your first pet?")) {

                                    questionCode = "SQ003";
                                }

                                if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                                    longitude = Double.valueOf(longitude1);
                                    latitude = Double.valueOf(latitude1);
                                    if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                                    } else {
                                        location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                                    }
                                } else {

                                    longitude = 0;
                                    latitude = 0;
                                    location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
                                }
                                GenerateiPIN();
                                validateBtn.setEnabled(false);

                            }
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (!TextUtils.isEmpty(securityCheck) && securityCheck.equals("securityQuestion")) {

        } else {

            finish();
        }
    }

    public void GetSecretQuestions() {

        try {
            pd.show();

            if (helper.isNetworkAvailable()) {
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(SecurityQuestionScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getQuestionsURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_GetSecretQuestionResult").getString("Status_Code").equals("00")) {


                                            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_GetSecretQuestionResult").getString("SecretQuestionList"));
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                try {

                                                    JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                    questionList.add(jsonObj.getString("Question_Description"));

                                                } catch (Exception ex) {
                                                    ex.printStackTrace();
                                                    pd.dismiss();
                                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                    validateBtn.setEnabled(true);
                                                }

                                            }

                                            quesitons = new String[questionList.size()];
                                            quesitons = questionList.toArray(quesitons);
                                            showSpinner();

                                        } else if (jsonObject.getJSONObject("Signs_GetSecretQuestionResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_GetSecretQuestionResult").getString("Status_Description"), "ERROR", SecurityQuestionScreen.this, new LoginScreen());
                                            validateBtn.setEnabled(true);

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_GetSecretQuestionResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                        }

                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();

            } else {
//                Toast.makeText(SecurityQuestionScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
            }
        } catch (Exception e) {

//            Toast.makeText(SecurityQuestionScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
        }
    }

    public void AddSecretQuestionAnswer() {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                final String md5Answer = helper.md5(userName.toUpperCase() + questionCode + questionAnswer.toUpperCase());
//                webService.AddSecretQuestionAnswer(userName, questionCode, md5Answer, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getSetQuestionAnswerReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(SecurityQuestionScreen.this, "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                showDilogForError("Question cannot be added. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
//                                editQuestionAnswer.setText("");
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(SecurityQuestionScreen.this, "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        ProfileSetupScreen.pager.setCurrentItem(3);
//                                        ProfileSetupScreen.currentItem++;
//                                        headingTxt.setText("DONE");
//                                        validateBtn.setEnabled(true);
//                                        editQuestionAnswer.setText("");
//                                    } else {
////                                        Toast.makeText(SecurityQuestionScreen.this, "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        showDilogForError("Question cannot be added. Please try again later.", "ERROR");
//                                        validateBtn.setEnabled(true);
//                                        editQuestionAnswer.setText("");
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    showDilogForError("Question cannot be added. Please try again later.", "ERROR");
//                                    validateBtn.setEnabled(true);
//                                    editQuestionAnswer.setText("");
//                                }
//                            }
//                        } else {
////                            Toast.makeText(SecurityQuestionScreen.this, "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            showDilogForError("Question cannot be added. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//                            editQuestionAnswer.setText("");
//
//                        }
//                    }
//                }, 3000);

                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.setQuestionAnswerURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getJSONObject("Signs_SetSecretQuestionResult").getString("Status_Code").equals("00")) {

                                        pd.dismiss();
//                                        Intent intent = new Intent(SecurityQuestionScreen.this, SettingsScreen.class);
//                                        overridePendingTransition(0, 0);
//                                        startActivity(intent);
                                        finish();
//                                        finishAffinity();


                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForError(jsonObject.getJSONObject("Signs_SetSecretQuestionResult").getString("Status_Description"), "ERROR");
                                        validateBtn.setEnabled(true);
                                        editQuestionAnswer.setText("");
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    editQuestionAnswer.setText("");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {

                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    editQuestionAnswer.setText("");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    editQuestionAnswer.setText("");
                                }
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("sService_Username", constants.signsUserName);
                        params.put("sService_Password", constants.signsPassword);
                        params.put("sIPAddress", Utils.getIPAddress(true));
                        params.put("sMacAddress", constants.address);
                        params.put("sSigns_Username", userName);
                        params.put("sOS_Version", constants.version);
                        params.put("sDeviceName", constants.model);
                        params.put("sAppVersion", constants.appVersion);
                        params.put("sService_SharedKey", Constants.Shared_KEY);
                        params.put("sTimeStamp", constants.dateTime);
                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                        params.put("sDeviceLongitude", String.valueOf(longitude));
                        params.put("sQuestionCode", questionCode);
                        params.put("sAnswerText", md5Answer);
                        return params;
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(SecurityQuestionScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
                editQuestionAnswer.setText("");

            }
        } catch (Exception e) {

//            Toast.makeText(SecurityQuestionScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Question cannot be added. Please try again later.", "ERROR");
            doneBtn.setEnabled(true);
            editQuestionAnswer.setText("");

        }
    }

    public void showSpinner() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                spinnerAdapter questionAdapter = new spinnerAdapter(SecurityQuestionScreen.this, R.layout.custom_textview_grey);
                questionAdapter.addAll(quesitons);
                questionAdapter.add("Select Question");
                questionSpinner.setAdapter(questionAdapter);
                questionSpinner.setSelection(questionAdapter.getCount());
                questionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        try {


                            // Get select item
                            if (questionSpinner.getSelectedItem() == "Select Question") {
                                questionSelected = questionSpinner.getSelectedItem().toString();
                                Log.d("range", "---" + questionSpinner);

                            } else {

                                questionSelected = questionSpinner.getSelectedItem().toString();

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub
                    }
                });
                pd.dismiss();
            }

        }, 1000);
    }

    public void showInfo() {

        dialog = new Dialog(SecurityQuestionScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        heading.setText("Information");
        message.setText("Select a security question from question drop-down menu. Type an answer into the corresponding answer field. The answers to question is not case sensitive.");
        heading.setBackgroundColor(getResources().getColor(R.color.grey));
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }


    public void GenerateiPIN() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.RegeneratePin(constants.sharedPreferences.getString("userName", "N/A"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getRegeneratePinReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//                                pd.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "Invalid Username/iPIN!", Toast.LENGTH_LONG).show();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
//
//
//                            } else {
//                                
//                                pd.dismiss();
//                                Intent intent = new Intent(ConfirmPasscodeScreen.this, PinVerificationScreen.class);
//                                intent.putExtra("passcode", passcode);
//                                intent.putExtra("change", "passcode");
//                                constants.passCode = "";
//                                   editQuestionAnswer.setText("");
//                                validateBtn.setEnabled(true);
//                                overridePendingTransition(0, 0);
//                                startActivity(intent);
//
//
//                            }
//                        } else {
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 10000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sSigns_Username", user);
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(SecurityQuestionScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("00")) {
                                            pd.dismiss();
                                            Intent intent = new Intent(SecurityQuestionScreen.this, PinVerificationScreen.class);
                                            intent.putExtra("code", questionCode);
                                            intent.putExtra("answer", questionAnswer);
                                            intent.putExtra("change", "question");
                                            overridePendingTransition(0, 0);
                                            validateBtn.setEnabled(true);
                                            editQuestionAnswer.setText("");
                                            startActivity(intent);
                                        } else if (jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR", SecurityQuestionScreen.this, new LoginScreen());
                                            validateBtn.setEnabled(true);
                                            editQuestionAnswer.setText("");

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_GenerateOTPResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                            editQuestionAnswer.setText("");
                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                        editQuestionAnswer.setText("");
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    editQuestionAnswer.setText("");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    editQuestionAnswer.setText("");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    editQuestionAnswer.setText("");
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sServerSessionKey", md5);
//
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
                editQuestionAnswer.setText("");

            }
        } catch (Exception e) {
            pd.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            validateBtn.setEnabled(true);
            editQuestionAnswer.setText("");

        }
    }

    public void SubmitSecretQuestion(String questionCode) {

        final String md5Answer = helper.md5(userName.toUpperCase() + questionCode + questionAnswer.toUpperCase());
        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();

                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sSigns_Username", user);
                params.put("sServerSessionKey", md5);
                params.put("sLegalIdentityValue", legalID);
                params.put("sQuestionCode", questionCode);
                params.put("sAnswerText", md5Answer);
                HttpsTrustManager.allowMySSL(SecurityQuestionScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.sqvalidateURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_CompareSecretQuestionResult").getString("Status_Code").equals("00")) {
                                            pd.dismiss();
                                            Intent intent;
                                            String isRooted = intent1.getStringExtra("isRooted");
                                            String isNotification = intent1.getStringExtra("isNotification");
                                            String isFirst = intent1.getStringExtra("isFirstLogin");
                                            String isFirstLogin = intent1.getStringExtra("firstLogin");

                                            if (!TextUtils.isEmpty(isFirstLogin) && isFirstLogin.equals("First")) {

                                                if (!TextUtils.isEmpty(isRooted) && isRooted.toUpperCase().equals("N"))
                                                    intent = new Intent(SecurityQuestionScreen.this, TermsNConditionsScreen.class);
                                                else if (!TextUtils.isEmpty(isNotification) && isNotification.toUpperCase().equals("TRUE")) {

                                                    intent = new Intent(SecurityQuestionScreen.this, ProfileSetupScreen.class);
                                                    intent.putExtra("isFirst", "Yes");

                                                } else {
                                                    intent = new Intent(SecurityQuestionScreen.this, NotificationScreen.class);

                                                }

                                                intent.putExtra("username", intent1.getStringExtra("username"));
                                                intent.putExtra("login", "login");
                                                intent.putExtra("isFirstLogin", intent1.getStringExtra("isFirstLogin"));
                                                overridePendingTransition(0, 0);
                                                startActivity(intent);
                                                finish();
                                                finishAffinity();

                                            } else if (!TextUtils.isEmpty(isFirstLogin) && isFirstLogin.equals("No")) {

                                                if (!TextUtils.isEmpty(isRooted) && isRooted.toUpperCase().equals("N"))
                                                    intent = new Intent(SecurityQuestionScreen.this, TermsNConditionsScreen.class);
                                                else
                                                    intent = new Intent(SecurityQuestionScreen.this, NotificationScreen.class);

                                                intent.putExtra("username", intent1.getStringExtra("username"));
                                                intent.putExtra("login", "login");
                                                intent.putExtra("isFirstLogin", intent1.getStringExtra("isFirstLogin"));
                                                overridePendingTransition(0, 0);
                                                startActivity(intent);
                                                finish();
                                                finishAffinity();

                                            } else if (!TextUtils.isEmpty(isFirstLogin) && isFirstLogin.equals("Never")) {

                                                intent = new Intent(SecurityQuestionScreen.this, HomeScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                startActivity(intent);
                                                finish();
                                                finishAffinity();

                                            }
                                            validateBtn.setEnabled(true);
                                            editQuestionAnswer.setText("");

                                        } else if (jsonObject.getJSONObject("Signs_CompareSecretQuestionResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_CompareSecretQuestionResult").getString("Status_Description"), "ERROR", SecurityQuestionScreen.this, new LoginScreen());
                                            validateBtn.setEnabled(true);
                                            editQuestionAnswer.setText("");

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_CompareSecretQuestionResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                            editQuestionAnswer.setText("");
                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                        editQuestionAnswer.setText("");
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    editQuestionAnswer.setText("");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    editQuestionAnswer.setText("");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    editQuestionAnswer.setText("");
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sServerSessionKey", md5);
//
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
                editQuestionAnswer.setText("");

            }
        } catch (Exception e) {
            pd.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            validateBtn.setEnabled(true);
            editQuestionAnswer.setText("");

        }
    }
}