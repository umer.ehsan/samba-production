package com.ceesolutions.samba.accessControl.Adapters;

/**
 * Created by ceeayaz on 2/20/18.
 */

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ceesolutions.samba.accessControl.Fragments.ConfirmPasscodeFragment;
import com.ceesolutions.samba.accessControl.Fragments.PasscodeFragment;
import com.ceesolutions.samba.accessControl.Fragments.ProfilePictureFragment;
import com.ceesolutions.samba.accessControl.Fragments.SecurityQuestionFragment;

/**
 * Created by ayaz on 1/5/18.
 */

public class ProfileSetupFragmentAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public ProfileSetupFragmentAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
//                LanguageFragment languageFragment = new LanguageFragment();
//                return languageFragment;
                PasscodeFragment passcodeFragment = new PasscodeFragment();
                return passcodeFragment;
            case 1:
                ConfirmPasscodeFragment confirmPasscodeFragment = new ConfirmPasscodeFragment();
                return confirmPasscodeFragment;

            case 2:
                SecurityQuestionFragment securityQuestionFragment = new SecurityQuestionFragment();
                return securityQuestionFragment;
            case 3:

                ProfilePictureFragment profilePictureFragment = new ProfilePictureFragment();
                return profilePictureFragment;


            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}