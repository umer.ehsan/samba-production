package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ayaz on 1/30/18.
 */

public class UpdateUserPasswordScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private EditText editPassword, editConfrimPassword;
    private RequestQueue requestQueue;
    private WebService webService;
    private TextView validateBtn, errorMessageForConfirmPassword, errorMessageForPassword, message, doneBtn, heading;
    private Helper helper;
    private String password, confirmPassword, userName, identityValue, identityType;
    private Dialog pd;
    private Pattern passwordPattern, passwordMatchpattern, passwordPattern1, passwordPattern2, passwordPattern3, passwordPattern4;
    private Matcher passwordMatcher;
    private Constants constants;
    private Utils utils;
    private String strengthType = "";
    private ImageView info, info1;
    private Dialog dialog;
    private AppPreferences appPreferences;
    private String location;

    private static boolean checkForCapitalLetter(String str) {
        char ch;
        boolean capitalFlag = false;
        boolean lowerCaseFlag = false;
        boolean numberFlag = false;
        for (int i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isDigit(ch)) {
                numberFlag = true;
            } else if (Character.isUpperCase(ch)) {
                capitalFlag = true;
            } else if (Character.isLowerCase(ch)) {
                lowerCaseFlag = true;
            }
            if (numberFlag && capitalFlag && lowerCaseFlag)
                return true;
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_user_password);
        initViews();
        getSupportActionBar().hide();

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showInfo();
            }
        });

        info1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showInfo();
            }
        });
    }

    public void initViews() {

        validateBtn = (TextView) findViewById(R.id.validateBtn);
        errorMessageForConfirmPassword = (TextView) findViewById(R.id.errorMessageForConfirmPassword);
        errorMessageForPassword = (TextView) findViewById(R.id.errorMessageForPassword);
        editPassword = (EditText) findViewById(R.id.editPassword);
        editConfrimPassword = (EditText) findViewById(R.id.editConfirmPassword);
        info = (ImageView) findViewById(R.id.info);
        info1 = (ImageView) findViewById(R.id.info1);

        webService = new WebService();
        helper = Helper.getHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(UpdateUserPasswordScreen.this);
        utils = new Utils(UpdateUserPasswordScreen.this);

        identityType = constants.forgotPasswordCredentials.getString("identityType", "N/A");
//        if (identityType.equals("NTN")) {
//            identityValue = constants.credentialsSharedPreferences.getString("ntn", "N/A");
//        } else if (identityType.equals("SCNIC")) {
//            identityValue = constants.credentialsSharedPreferences.getString("scnic", "N/A");
//        } else if (identityType.equals("PASSPORT")) {
//            identityValue = constants.credentialsSharedPreferences.getString("passport", "N/A");
//        } else if (identityType.equals("CNIC")) {
        identityValue = constants.forgotPasswordCredentials.getString("identityValue", "N/A");
//
//        }

        userName = constants.forgotPasswordCredentials.getString("username", "N/A");

        pd = new Dialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        appPreferences = new AppPreferences(UpdateUserPasswordScreen.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

        password = editPassword.getText().toString().trim();
        confirmPassword = editConfrimPassword.getText().toString().trim();

        passwordPattern = Pattern.compile(constants.PASSWORD_PATTERN);
        passwordPattern1 = Pattern.compile(constants.allowedPassCharactersRegex);
        passwordPattern2 = Pattern.compile(constants.bannedPassCharactersRegex);
        passwordPattern3 = Pattern.compile(constants.allowedPassNumericCharactersRegex);
        passwordPattern4 = Pattern.compile(constants.allowedPassAlphaCharactersRegex);

        passwordMatchpattern = Pattern.compile(constants.EXCLUDE_PASSWORD_PATTERN);

        editPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {

                    errorMessageForConfirmPassword.setVisibility(View.GONE);
                    editConfrimPassword.getText().clear();

//                    Toast.makeText(UserConfirmationScreen.this, "Focus Lose", Toast.LENGTH_SHORT).show();
                } else {


//                    Toast.makeText(UserConfirmationScreen.this, "Get Focus", Toast.LENGTH_SHORT).show();
                }

            }
        });
        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessageForPassword.setVisibility(View.GONE);
                errorMessageForPassword.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessageForPassword.setVisibility(View.GONE);
                errorMessageForPassword.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                password = editable.toString().trim();
                if (password.length() == 0) {


                    //Do Nothing
                } else {

                    if (passwordMatchpattern.matcher(password).find()) {

                        errorMessageForPassword.setVisibility(View.VISIBLE);
                        errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                        errorMessageForPassword.setText("Password Strength: Weak");


                    } else {

                        if (!checkForCapitalLetter(password)) {

                            errorMessageForPassword.setVisibility(View.VISIBLE);
                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                            errorMessageForPassword.setText("Password Strength: Weak");


                        } else {

                            if (password.length() < 16) {
                                passwordMatcher = passwordPattern.matcher(password);


                                if (password.length() > 7) {

                                    if (passwordMatcher.matches()) {


                                        if (password.length() > 10) {

                                            errorMessageForPassword.setVisibility(View.VISIBLE);
                                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                            errorMessageForPassword.setText("Password Strength: Strong");

                                        } else {

                                            errorMessageForPassword.setVisibility(View.VISIBLE);
                                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                            errorMessageForPassword.setText("Password Strength: Good");

                                        }


                                    } else {

                                        if (password.length() > 7 && password.length() < 10) {

                                            errorMessageForPassword.setVisibility(View.VISIBLE);
                                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                            errorMessageForPassword.setText("Password Strength: Good");

                                        } else {

                                            errorMessageForPassword.setVisibility(View.VISIBLE);
                                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                            errorMessageForPassword.setText("Password Strength: Bad");
                                        }

                                    }
                                } else {

                                    if (password.length() > 7 && password.length() < 10) {

                                        errorMessageForPassword.setVisibility(View.VISIBLE);
                                        errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                        errorMessageForPassword.setText("Password Strength: Good");

                                    } else {

                                        errorMessageForPassword.setVisibility(View.VISIBLE);
                                        errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                        errorMessageForPassword.setText("Password Strength: Bad");
                                    }
                                }

                            }
                        }

                    }
                }
            }
        });

        editConfrimPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

//                errorMessageForConfirmPassword.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

//                errorMessageForConfirmPassword.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                confirmPassword = editable.toString();
            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                FirebaseAnalytics.getInstance(UpdateUserPasswordScreen.this).logEvent("Reset_Password_Confirm_Pressed", new Bundle());

                password = editPassword.getText().toString().trim();
                confirmPassword = editConfrimPassword.getText().toString().trim();

                errorMessageForPassword.setTextColor(getResources().getColor(R.color.tabunderline));
                if (TextUtils.isEmpty(password)) {
                    errorMessageForPassword.setVisibility(View.VISIBLE);
                    errorMessageForPassword.bringToFront();
                    errorMessageForPassword.setError("");
                    errorMessageForPassword.setText("Please enter valid password");
                    clearViews();

                } else if (!helper.validateInputForSC(password) || password.contains(" ")) {

                    errorMessageForPassword.setVisibility(View.VISIBLE);
                    errorMessageForPassword.bringToFront();
                    errorMessageForPassword.setError("");
                    errorMessageForPassword.setText("Password cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                    clearViews();

                } else if (!checkForCapitalLetter(password)) {

                    errorMessageForPassword.setVisibility(View.VISIBLE);
                    errorMessageForPassword.bringToFront();
                    errorMessageForPassword.setError("");
                    errorMessageForPassword.setText("Password must contains a capital letter");
                    clearViews();

                }

                if (TextUtils.isEmpty(confirmPassword)) {
                    errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                    errorMessageForConfirmPassword.bringToFront();
                    errorMessageForConfirmPassword.setError("");
                    errorMessageForConfirmPassword.setText("Please enter valid password");
                    clearViews();

                } else if (!helper.validateInputForSC(confirmPassword) || confirmPassword.contains(" ")) {


                    errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                    errorMessageForConfirmPassword.bringToFront();
                    errorMessageForConfirmPassword.setError("");
                    errorMessageForConfirmPassword.setText("Confirm Password cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                    clearViews();
                }

                if (confirmPassword.equals(password)) {

                    if ((!TextUtils.isEmpty(password) && password.length() >= 10 && helper.validateInputForSC(password) && !password.contains(" ") && checkForCapitalLetter(password)) && (!TextUtils.isEmpty(confirmPassword) && confirmPassword.length() >= 10 && helper.validateInputForSC(confirmPassword) && !confirmPassword.contains(" "))) {

                        if (passwordMatchpattern.matcher(editPassword.getText().toString()).find()) {

                            clearViews();
                            errorMessageForPassword.setVisibility(View.VISIBLE);
                            errorMessageForPassword.bringToFront();
                            errorMessageForPassword.setError("");
                            errorMessageForPassword.setText("Password <,>,\",',%,(,),&,+,\\,~");
                            errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                            errorMessageForConfirmPassword.bringToFront();
                            errorMessageForConfirmPassword.setError("");
                            errorMessageForConfirmPassword.setText("Confirm Password <,>,\",',%,(,),&,+,\\,~");
//                            clearViews();
                        } else {

                            passwordMatcher = passwordPattern.matcher(password);

                            if (passwordMatcher.matches()) {

//                                editPassword.setText(editPassword.getText().toString());


                                validateBtn.setEnabled(false);
                                UpdateUserPassword();
                                pd.show();


//

                            } else {
                                clearViews();
                                errorMessageForPassword.setVisibility(View.VISIBLE);
                                errorMessageForPassword.bringToFront();
                                errorMessageForPassword.setError("");
                                errorMessageForPassword.setText("Password policy does not match");
                                errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                                errorMessageForConfirmPassword.bringToFront();
                                errorMessageForConfirmPassword.setError("");
                                errorMessageForConfirmPassword.setText("Password policy does not match");
//                                clearViews();
                            }

                        }

                    } else {
                        clearViews();
                        errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                        errorMessageForConfirmPassword.bringToFront();
                        errorMessageForConfirmPassword.setError("");
                        errorMessageForConfirmPassword.setText("Please enter valid password");
                        errorMessageForPassword.setVisibility(View.VISIBLE);
                        errorMessageForPassword.bringToFront();
                        errorMessageForPassword.setError("");
                        errorMessageForPassword.setText("Please enter valid password");
//                        clearViews();
                    }


                } else {

                    if (TextUtils.isEmpty(password) || password.length() < 8) {
                        clearViews();
                        errorMessageForPassword.setVisibility(View.VISIBLE);
                        errorMessageForPassword.bringToFront();
                        errorMessageForPassword.setError("");
                        errorMessageForPassword.setText("Please enter valid password");
//                        clearViews();
                    }

                    if (TextUtils.isEmpty(confirmPassword) || confirmPassword.length() < 8) {
                        clearViews();
                        errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                        errorMessageForConfirmPassword.bringToFront();
                        errorMessageForConfirmPassword.setError("");
                        errorMessageForConfirmPassword.setText("Please enter valid password");

                    }

                    if (!TextUtils.isEmpty(password) && password.length() >= 8 && !TextUtils.isEmpty(confirmPassword) && confirmPassword.length() >= 8) {
                        clearViews();
                        errorMessageForPassword.setVisibility(View.VISIBLE);
                        errorMessageForPassword.bringToFront();
                        errorMessageForPassword.setError("");
                        errorMessageForPassword.setText("Password/Confirm Password does not match");
                        errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                        errorMessageForConfirmPassword.bringToFront();
                        errorMessageForConfirmPassword.setError("");
                        errorMessageForConfirmPassword.setText("Password/Confirm Password does not match");


                    }


                }
            }
        });


    }

    public void UpdateUserPassword() {

        validateBtn.setEnabled(false);

        if (password.equals(confirmPassword)) {

            try {

                if (helper.isNetworkAvailable()) {

                    final String newPasswordMd5 = helper.md5(userName.toUpperCase() + password);
//                    webService.UpdateUserPassword(userName, newPasswordMd5, identityType, identityValue, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                    Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        public void run() {
//
//                            JSONObject jsonObject = webService.getUpdatePasswordReqResponse();
//                            Log.d("Response", "Object----" + jsonObject);
//
//                            if (jsonObject != null) {
//
//                                if (jsonObject.has("Error")) {
//
////                                    Toast.makeText(UpdateUserPasswordScreen.this, "Password is not valid!", Toast.LENGTH_LONG).show();
//                                    validateBtn.setEnabled(true);
//                                    pd.dismiss();
//                                    utils.showDilogForError("Password is not valid.", "ERROR");
//                                    clearViews();
//
//                                } else {
//
//
////                                    Toast.makeText(UpdateUserPasswordScreen.this, "Password has been updated Successfully.", Toast.LENGTH_LONG).show();
//                                    Intent intent = new Intent(UpdateUserPasswordScreen.this, ForgotPasswordConfirmationScreen.class);
//                                    pd.dismiss();
//                                    constants.forgotPasswordEditor.clear();
//                                    constants.forgotPasswordEditor.commit();
//                                    constants.forgotPasswordEditor.apply();
//                                    constants.credentialsEditor.clear();
//                                    constants.credentialsEditor.commit();
//                                    constants.credentialsEditor.apply();
//                                    startActivity(intent);
//                                    finish();
//                                    overridePendingTransition(0, 0);
//                                    validateBtn.setEnabled(true);
//                                    clearViews();
//
//                                }
//                            } else {
////                                Toast.makeText(UpdateUserPasswordScreen.this, "Password is not valid!", Toast.LENGTH_LONG).show();
//                                validateBtn.setEnabled(true);
//                                pd.dismiss();
//                                utils.showDilogForError("Password is not valid.", "ERROR");
//                                clearViews();
//
//                            }
//                        }
//                    }, 10000);
                    final String md5 = helper.md5(userName.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", constants.Old_Shared_Key);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sSigns_Username", userName);
                    params.put("sNewPassword", newPasswordMd5);
                    params.put("sLegalIdentityType", identityType);
                    params.put("sLegalIdentityValue", identityValue);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(UpdateUserPasswordScreen.this);
                    CryptLib _crypt = new CryptLib();
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.forgotPasswordupdateURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_UserUpdatePasswordResult").getString("Status_Code").equals("00")) {
                                                Intent intent = new Intent(UpdateUserPasswordScreen.this, ForgotPasswordConfirmationScreen.class);
                                                pd.dismiss();
                                                constants.forgotPasswordEditor.clear();
                                                constants.forgotPasswordEditor.commit();
                                                constants.forgotPasswordEditor.apply();
                                                constants.credentialsEditor.clear();
                                                constants.credentialsEditor.commit();
                                                constants.credentialsEditor.apply();
                                                startActivity(intent);
                                                finish();
                                                overridePendingTransition(0, 0);
                                                validateBtn.setEnabled(true);
                                                clearViews();
                                            } else {
                                                validateBtn.setEnabled(true);
                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_UserUpdatePasswordResult").getString("Status_Description"), "ERROR");
                                                clearViews();
                                            }
                                        } else {

                                            validateBtn.setEnabled(true);
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            clearViews();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();

                                        FirebaseAnalytics.getInstance(UpdateUserPasswordScreen.this).logEvent("Error", new Bundle());

                                        validateBtn.setEnabled(true);
                                        pd.dismiss();
                                        utils.showDilogForError("Password is not valid.", "ERROR");
                                        clearViews();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        validateBtn.setEnabled(true);
                                        pd.dismiss();
                                        utils.showDilogForError("Password is not valid.", "ERROR");
                                        clearViews();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        validateBtn.setEnabled(true);
                                        pd.dismiss();
                                        utils.showDilogForError("Password is not valid.", "ERROR");
                                        clearViews();
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", constants.Old_Shared_Key);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sSigns_Username", userName);
//                            params.put("sNewPassword", newPasswordMd5);
//                            params.put("sLegalIdentityType", identityType);
//                            params.put("sLegalIdentityValue", identityValue);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } else {
//                    Toast.makeText(UpdateUserPasswordScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                    validateBtn.setEnabled(true);
                    pd.dismiss();
                    utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                    clearViews();

                }
            } catch (Exception e) {

//                Toast.makeText(UpdateUserPasswordScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
                validateBtn.setEnabled(true);
                pd.dismiss();
                utils.showDilogForError("Password is not valid.", "ERROR");
                clearViews();

            }
        } else {

//            Toast.makeText(UpdateUserPasswordScreen.this, "Password and Confirm Password does not match !", Toast.LENGTH_LONG).show();
            validateBtn.setEnabled(true);
            pd.dismiss();
            utils.showDilogForError("Password and Confirm Password does not match.", "WARNING");

            clearViews();
        }
    }

    public void clearViews() {

        editConfrimPassword.getText().clear();
        editPassword.getText().clear();
        editPassword.requestFocus();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UpdateUserPasswordScreen.this, LoginScreen.class);
        constants.forgotPasswordEditor.clear().commit();
        constants.forgotPasswordEditor.apply();
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
    }

    public void showInfo() {

        dialog = new Dialog(UpdateUserPasswordScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.password_policy);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        heading.setText("PASSWORD POLICY");
        message.setText(Constants.PASSWORDPOLICY);
        heading.setBackgroundColor(getResources().getColor(R.color.grey));
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    @Override
    public void onSupportActionModeStarted(ActionMode mode) {
        super.onSupportActionModeStarted(mode);
        mode.finish();
    }
}
