package com.ceesolutions.samba.accessControl;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.utils.PopupDialogFingerPrint;


public class FingerPrintNotificationScreen extends AppCompatActivity {

    private PopupDialogFingerPrint popupDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        getSupportActionBar().hide();

        popupDialog = new PopupDialogFingerPrint();
        popupDialog.setCancelable(false);
        popupDialog.show(getSupportFragmentManager(),"dialog");

    }

    @Override
    public void onBackPressed() {
        //Do Nothing
    }
}
