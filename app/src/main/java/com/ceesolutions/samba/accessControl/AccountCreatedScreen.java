package com.ceesolutions.samba.accessControl;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by ayaz on 1/9/18.
 */

public class AccountCreatedScreen extends AppCompatActivity {

    private TextView doneBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_created_screen);
        initViews();
        getSupportActionBar().hide();
    }

    public void initViews() {

        doneBtn = (TextView) findViewById(R.id.doneBtn);
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FirebaseAnalytics.getInstance(AccountCreatedScreen.this).logEvent("User_Registration_Done", new Bundle());

                Intent intent = new Intent(AccountCreatedScreen.this, LoginScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Log.d("Do Nothing", "----");
    }
}
