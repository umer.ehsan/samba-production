package com.ceesolutions.samba.accessControl.NonSambaUser;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.accessControl.UserCodeVerificationScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by ayaz on 1/9/18.
 */

public class NonSambaUserValidationScreen extends AppCompatActivity {


    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public static double latitude;
    public static double longitude;
    final int DATE_PICKER_ID = 1;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public String longitude1;
    public String latitude1;
    String motherName, userName, dob, mobileNo, email, idCardNumber, nicExpire, identityType, monthName, yearWithTwoDigits, identityValue;
    private EditText editName, editNicExpire, editDOB, editEmail, editMobileNumber, maidentEditTxt;
    private TextView errorMessageForMother, errorMessageForUsername, errorMessageForNic, errorMessageForDOB, errorMessageForMobNumber, errorMessageForIDCard, errorMessageForEmail, nextBtn;
    private int year, day, month;
    private Dialog pd;
    private WebService webService;
    private RequestQueue requestQueue;
    private TextView takePhoto, editIDCardNumber;
    private Helper helper;
    private Constants constants;
    private boolean isValid = false;
    private boolean isDob = false;
    private Bitmap bitmap;
    private String imgString;
    private TextView text2;
    private CountryCodePicker ccpLoadNumber;
    private Utils utils;
    private AppPreferences appPreferences;
    private String location;
    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {


            yearWithTwoDigits = String.valueOf(selectedYear);
            monthName = getMonth(selectedMonth + 1);
            day = selectedDay;


            if (!isDob) {

                // Show selected date
                isDob = true;
                editNicExpire.setText(new StringBuilder().append(day)
                        .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)));

                nicExpire = editNicExpire.getText().toString();

            } else {

                // Show selected date
                editDOB.setText(new StringBuilder().append(day)
                        .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)));

                dob = editDOB.getText().toString();
                editEmail.requestFocus();
//                DatePickerDialog dialog2 = new DatePickerDialog(NonSambaUserValidationScreen.this, this,
//                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
//                        calendar.get(Calendar.DAY_OF_MONTH));
//                dialog2.show();

            }

        }
    };

    private static Bitmap getScaledBitmap(Bitmap bm, int bmOriginalWidth, int bmOriginalHeight, double originalWidthToHeightRatio, double originalHeightToWidthRatio, int maxHeight, int maxWidth) {
        if (bmOriginalWidth > maxWidth || bmOriginalHeight > maxHeight) {
//            Log.v(TAG, format("RESIZING bitmap FROM %sx%s ", bmOriginalWidth, bmOriginalHeight));

            if (bmOriginalWidth > bmOriginalHeight) {
                bm = scaleDeminsFromWidth(bm, maxWidth, bmOriginalHeight, originalHeightToWidthRatio);
            } else {
                bm = scaleDeminsFromHeight(bm, maxHeight, bmOriginalHeight, originalWidthToHeightRatio);
            }

//            Log.v("", format("RESIZED bitmap TO %sx%s ", bm.getWidth(), bm.getHeight()));
        }
        return bm;
    }

    private static Bitmap scaleDeminsFromHeight(Bitmap bm, int maxHeight, int bmOriginalHeight, double originalWidthToHeightRatio) {
        int newHeight = (int) Math.min(maxHeight, bmOriginalHeight * .55);
        int newWidth = (int) (newHeight * originalWidthToHeightRatio);
        bm = Bitmap.createScaledBitmap(bm, newWidth, newHeight, true);
        return bm;
    }

    private static Bitmap scaleDeminsFromWidth(Bitmap bm, int maxWidth, int bmOriginalWidth, double originalHeightToWidthRatio) {
        //scale the width
        int newWidth = (int) Math.min(maxWidth, bmOriginalWidth * .75);
        int newHeight = (int) (newWidth * originalHeightToWidthRatio);
        bm = Bitmap.createScaledBitmap(bm, newWidth, newHeight, true);
        return bm;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.non_samba_validation_screen);
        initViews();
        getSupportActionBar().hide();
    }

    public void initViews() {

        nextBtn = (TextView) findViewById(R.id.validateBtn);
        takePhoto = (TextView) findViewById(R.id.takePhoto);
        editIDCardNumber = (TextView) findViewById(R.id.editIDCardNumber);
        editNicExpire = (EditText) findViewById(R.id.editNicExpire);
        editDOB = (EditText) findViewById(R.id.editDOB);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editMobileNumber = (EditText) findViewById(R.id.editMobileNumber);
        maidentEditTxt = (EditText) findViewById(R.id.maidentEditTxt);
        editName = (EditText) findViewById(R.id.editName);
        ccpLoadNumber = (CountryCodePicker) findViewById(R.id.ccp_loadFullNumber);

        errorMessageForUsername = (TextView) findViewById(R.id.errorMessageForUsername);
        errorMessageForMother = (TextView) findViewById(R.id.errorMessageForMother);
        errorMessageForEmail = (TextView) findViewById(R.id.errorMessageForEmail);
        errorMessageForMobNumber = (TextView) findViewById(R.id.errorMessageForMobNumber);
        errorMessageForNic = (TextView) findViewById(R.id.errorMessageForNic);
        errorMessageForIDCard = (TextView) findViewById(R.id.errorMessageForIDCard);
        errorMessageForDOB = (TextView) findViewById(R.id.errorMessageForDOB);
        text2 = (TextView) findViewById(R.id.text2);
        webService = new WebService();
        helper = Helper.getHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        utils = new Utils(NonSambaUserValidationScreen.this);
        pd = new Dialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        constants = Constants.getConstants(NonSambaUserValidationScreen.this);

        utils.showDilogForError("You are requested to provide some personal information for registration on SambaSmart\n" +
                "\n" +
                " \n" +
                "Protection of personal information is foundation of customer trust for Samba Bank. We ensure that all of your information will be kept strictly confidential and never disclosed with anyone. ","INFORMATION");
        identityType = constants.credentialsSharedPreferences.getString("identityType", "N/A");
        identityValue = constants.credentialsSharedPreferences.getString("identityValue", "N/A");
        String txt = "<font COLOR=\'#353535\'>" + "Seems like you dont have an account with us. Please provide the required information for the registering on SambaSmart against " + "</font>"
                + "<font COLOR=\'#034ea2\'><b>" + identityType + ": " + identityValue;
        text2.setText(Html.fromHtml(txt));
        userName = editName.getText().toString().trim();
        dob = editDOB.getText().toString().trim();
        mobileNo = editMobileNumber.getText().toString().trim();
        email = editEmail.getText().toString().trim();
        nicExpire = editNicExpire.getText().toString().trim();

        appPreferences = new AppPreferences(NonSambaUserValidationScreen.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

        editMobileNumber.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);

        takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                errorMessageForIDCard.setVisibility(View.GONE);
                selectImage();
            }
        });

        editDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isDob = true;
                SelectDate();
                errorMessageForDOB.setVisibility(View.GONE);
            }
        });

        editNicExpire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isDob = false;
                SelectDate();
                errorMessageForNic.setVisibility(View.GONE);
            }
        });

        editName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForUsername.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForUsername.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                userName = editable.toString();

            }
        });

        maidentEditTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForMother.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForMother.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                motherName = editable.toString();

            }
        });

        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForEmail.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForEmail.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                email = editable.toString();

            }
        });


        editMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForMobNumber.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForMobNumber.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                mobileNo = editable.toString();
            }
        });


        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    userName = editName.getText().toString().trim();
                    dob = editDOB.getText().toString().trim();
                    mobileNo = editMobileNumber.getText().toString().trim();
                    email = editEmail.getText().toString().trim();
                    nicExpire = editNicExpire.getText().toString().trim();
                    idCardNumber = editIDCardNumber.getText().toString().trim();
                    motherName = maidentEditTxt.getText().toString().trim();

                    if (TextUtils.isEmpty(email)) {

                        errorMessageForEmail.setVisibility(View.VISIBLE);
                        errorMessageForEmail.bringToFront();
                        errorMessageForEmail.setError("");
                        errorMessageForEmail.setText("Please enter valid email address");
                    } else {

                        if (!email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") || email.contains(" ")) {
                            errorMessageForEmail.setVisibility(View.VISIBLE);
                            errorMessageForEmail.bringToFront();
                            errorMessageForEmail.setError("");
                            isValid = false;
                            errorMessageForEmail.setText("Invalid email address");
                        } else {

                            isValid = true;
                            email = editEmail.getText().toString().trim();

                        }
                    }

                    if (TextUtils.isEmpty(userName)) {
                        errorMessageForUsername.setVisibility(View.VISIBLE);
                        errorMessageForUsername.bringToFront();
                        errorMessageForUsername.setError("");
                        errorMessageForUsername.setText("Please enter valid username");

                    } else if (!helper.validateInputForSC(userName)) {

                        errorMessageForUsername.setVisibility(View.VISIBLE);
                        errorMessageForUsername.bringToFront();
                        errorMessageForUsername.setError("");
                        errorMessageForUsername.setText("Username number cannot contains <,>,\",',%,(,),&,+,\\,~");

                    } else {
                        userName = editName.getText().toString().trim();


                    }

                    if (TextUtils.isEmpty(motherName)) {
                        errorMessageForMother.setVisibility(View.VISIBLE);
                        errorMessageForMother.bringToFront();
                        errorMessageForMother.setError("");
                        errorMessageForMother.setText("Please enter valid mother name");

                    } else if (!helper.validateInputForSC(motherName)) {

                        errorMessageForMother.setVisibility(View.VISIBLE);
                        errorMessageForMother.bringToFront();
                        errorMessageForMother.setError("");
                        errorMessageForMother.setText("Mother name cannot contains <,>,\",',%,(,),&,+,\\,~");

                    } else {
                        motherName = maidentEditTxt.getText().toString().trim();


                    }

                    if (TextUtils.isEmpty(mobileNo) || mobileNo.equals("92")) {
                        errorMessageForMobNumber.setVisibility(View.VISIBLE);
                        errorMessageForMobNumber.bringToFront();
                        errorMessageForMobNumber.setError("");
                        errorMessageForMobNumber.setText("Please enter valid mobile number");

                    } else if (!helper.validateInputForSC(mobileNo) || mobileNo.contains(" ")) {

                        errorMessageForMobNumber.setVisibility(View.VISIBLE);
                        errorMessageForMobNumber.bringToFront();
                        errorMessageForMobNumber.setError("");
                        errorMessageForMobNumber.setText("Mobile number cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                    } else {

                        mobileNo = mobileNo.startsWith("0") ? mobileNo.substring(1) : mobileNo;
                        mobileNo = (ccpLoadNumber.getFullNumber() + mobileNo).replaceAll("-", "").trim();
                    }


                    if (TextUtils.isEmpty(idCardNumber) || idCardNumber.equals("ID Card Number")) {

                        errorMessageForIDCard.setVisibility(View.VISIBLE);
                        errorMessageForIDCard.bringToFront();
                        errorMessageForIDCard.setError("");
                        errorMessageForIDCard.setText("Please attach ID card image");

                    }
                    if (TextUtils.isEmpty(dob)) {
                        errorMessageForDOB.setVisibility(View.VISIBLE);
                        errorMessageForDOB.bringToFront();
                        errorMessageForDOB.setError("");
                        errorMessageForDOB.setText("Please enter valid date of birth");

                    }

                    if (TextUtils.isEmpty(nicExpire)) {
                        errorMessageForNic.setVisibility(View.VISIBLE);
                        errorMessageForNic.bringToFront();
                        errorMessageForNic.setError("");
                        errorMessageForNic.setText("Please enter valid ID card expiry date");

                    }

                    if ((!TextUtils.isEmpty(nicExpire)) && (!TextUtils.isEmpty(dob)) && (!TextUtils.isEmpty(idCardNumber)) && (!TextUtils.isEmpty(mobileNo) && helper.validateInputForSC(mobileNo) && !mobileNo.contains(" ")) && ((!TextUtils.isEmpty(userName) && helper.validateInputForSC(userName))) && ((!TextUtils.isEmpty(motherName) && helper.validateInputForSC(motherName))) && ((!TextUtils.isEmpty(email) && isValid == true && helper.validateInputForSC(email) && !email.contains(" "))) && (!TextUtils.isEmpty(idCardNumber) && !idCardNumber.equals("ID Card Number"))) {
//                    Intent intent = new Intent(NonSambaUserValidationScreen.this, UserCodeVerificationScreen.class);
//                    startActivity(intent);
//                    finish();
                        nextBtn.setEnabled(false);
                        UserValidation();

                    }

                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        });


    }

    public void UserValidation() {

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.RegeneratePinForRegistration(email, mobileNo, identityType, identityValue, constants.version, constants.model, constants.dateTime, constants.address, userName, requestQueue);
//
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getRegeneratePinRegistrationReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(NonSambaUserValidationScreen.this, "Given information is not valid!", Toast.LENGTH_LONG).show();
//                                nextBtn.setEnabled(true);
//                                pd.dismiss();
//                                utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                clearViews();
//
//                            } else {
//
//                                try {
////                                    Log.d("TAG", "--->" + jsonObject);
////                                    Toast.makeText(NonSambaUserValidationScreen.this, "User Information Verified Successfully.", Toast.LENGTH_LONG).show();
//                                    nextBtn.setEnabled(true);
//                                    Intent intent = new Intent(NonSambaUserValidationScreen.this, UserCodeVerificationScreen.class);
//                                    identityType = constants.credentialsSharedPreferences.getString("identityType", "N/A");
//                                    identityValue = constants.credentialsSharedPreferences.getString("identityValue", "N/A");
//                                    intent.putExtra("identityValue", identityValue);
//                                    intent.putExtra("fullName", userName);
//                                    constants.nonSambaCredentialsEditor.putString("username", userName);
//                                    constants.nonSambaCredentialsEditor.putString("email", email);
//                                    constants.nonSambaCredentialsEditor.putString("mobile", mobileNo);
//                                    constants.nonSambaCredentialsEditor.putString("nicexpiry", nicExpire);
//                                    constants.nonSambaCredentialsEditor.putString("image", imgString);
//                                    constants.nonSambaCredentialsEditor.putString("dob", dob);
//                                    constants.nonSambaCredentialsEditor.putString("motherName", motherName.toUpperCase());
//                                    constants.nonSambaCredentialsEditor.commit();
//                                    constants.nonSambaCredentialsEditor.apply();
//                                    intent.putExtra("identityType", identityType);
//                                    intent.putExtra("Type", "NonSamba");
//                                    intent.putExtra("mobileNo", mobileNo);
//                                    intent.putExtra("email", email);
//                                    clearViews();
//                                    pd.dismiss();
//                                    startActivity(intent);
//                                    finish();
//
//
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                    nextBtn.setEnabled(true);
//                                    pd.dismiss();
//                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                    clearViews();
//                                }
//                            }
//
//                        } else {
////                            Toast.makeText(NonSambaUserValidationScreen.this, "Given information is not valid!", Toast.LENGTH_LONG).show();
//                            nextBtn.setEnabled(true);
//                            pd.dismiss();
//                            utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                            clearViews();
//
//                        }
//                    }
//                }, 5000);
                final String md5 = helper.md5("" + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", constants.Old_Shared_Key);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sEmailAddress", email);
                params.put("sMobileNumber", mobileNo);
                params.put("sIdentityType", identityType);
                params.put("sLegalIdentityValue", identityValue);
                params.put("sFullName", userName);
                params.put("sSigns_Username", "");
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(NonSambaUserValidationScreen.this);
                CryptLib _crypt = new CryptLib();
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.regeneratepinRegistrationURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_GenerateRegister_OTACResult").getString("Status_Code").equals("00")) {
                                            nextBtn.setEnabled(true);
                                            Intent intent = new Intent(NonSambaUserValidationScreen.this, UserCodeVerificationScreen.class);
                                            identityType = constants.credentialsSharedPreferences.getString("identityType", "N/A");
                                            identityValue = constants.credentialsSharedPreferences.getString("identityValue", "N/A");
                                            intent.putExtra("identityValue", identityValue);
                                            intent.putExtra("fullName", userName);
                                            constants.nonSambaCredentialsEditor.putString("username", userName);
                                            constants.nonSambaCredentialsEditor.putString("email", email);
                                            constants.nonSambaCredentialsEditor.putString("mobile", mobileNo);
                                            constants.nonSambaCredentialsEditor.putString("nicexpiry", nicExpire);
                                            constants.nonSambaCredentialsEditor.putString("image", imgString.replaceAll("\\+", "@PLUS@"));
                                            constants.nonSambaCredentialsEditor.putString("dob", dob);
                                            constants.nonSambaCredentialsEditor.putString("motherName", motherName.toUpperCase());
                                            constants.nonSambaCredentialsEditor.commit();
                                            constants.nonSambaCredentialsEditor.apply();
                                            intent.putExtra("identityType", identityType);
                                            intent.putExtra("Type", "NonSamba");
                                            intent.putExtra("mobileNo", mobileNo);
                                            intent.putExtra("email", email);
                                            clearViews();
                                            pd.dismiss();
                                            startActivity(intent);
                                            finish();
                                        } else {

                                            nextBtn.setEnabled(true);
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_GenerateRegister_OTACResult").getString("Status_Description"), "ERROR");
                                            clearViews();
                                        }
                                    } else {

                                        nextBtn.setEnabled(true);
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        clearViews();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();

                                    nextBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    clearViews();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    nextBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    clearViews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    nextBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    clearViews();
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", constants.Old_Shared_Key);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sEmailAddress", email);
//                        params.put("sMobileNumber", mobileNo);
//                        params.put("sIdentityType", identityType);
//                        params.put("sLegalIdentityValue", identityValue);
//                        params.put("sFullName", userName);
//                        params.put("sSigns_Username", "");
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(NonSambaUserValidationScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                nextBtn.setEnabled(true);
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                clearViews();

            }
        } catch (Exception e) {

//            Toast.makeText(NonSambaUserValidationScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            nextBtn.setEnabled(true);
            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            clearViews();
        }
    }

    private void SelectDate() {

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        if (!isDob) {

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, pickerListener, year, month, day);
            datePickerDialog.show();

        } else {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            DatePickerDialog dialog1 = new DatePickerDialog(NonSambaUserValidationScreen.this, pickerListener,
                    calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH));
            Calendar today = Calendar.getInstance();
            Date currentDate = today.getTime();
            dialog1.getDatePicker().setMaxDate(currentDate.getTime());
            dialog1.show();
        }

//        showDialog(DATE_PICKER_ID);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:


                DatePickerDialog datePickerDialog = new DatePickerDialog(this, pickerListener, year, month, day);
                return datePickerDialog;

        }
        return null;
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    public void clearViews() {

        editName.setText("");
        editDOB.setText("");
        editEmail.setText("");
        editMobileNumber.setText("");
        editNicExpire.setText("");
        editIDCardNumber.setText("ID Card Number");

        editName.requestFocus();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(NonSambaUserValidationScreen.this, LoginScreen.class);
        constants.credentialsEditor.clear().commit();
        constants.credentialsEditor.apply();
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(NonSambaUserValidationScreen.this,
                android.Manifest.permission.CAMERA);
        int readStoragePermission = ContextCompat.checkSelfPermission(NonSambaUserValidationScreen.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStoragePermission = ContextCompat.checkSelfPermission(NonSambaUserValidationScreen.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }
        if (writeStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(NonSambaUserValidationScreen.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("Login", "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Login", "sms & location services permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("Login", "Some permissions are not granted ask again ");
                        //permission is denied (NonSambaUserValidationScreen.this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(NonSambaUserValidationScreen.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(NonSambaUserValidationScreen.this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(NonSambaUserValidationScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            showDialogOK("Camera and Read/Write Storage Services Permission required for NonSambaUserValidationScreen.this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(NonSambaUserValidationScreen.this, "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(NonSambaUserValidationScreen.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    private void selectImage() {
        try {
            PackageManager pm = NonSambaUserValidationScreen.this.getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, NonSambaUserValidationScreen.this.getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(NonSambaUserValidationScreen.this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(NonSambaUserValidationScreen.this, "Camera Permission error", Toast.LENGTH_LONG).show();
            checkAndRequestPermissions();
        } catch (Exception e) {
            Toast.makeText(NonSambaUserValidationScreen.this, "Camera Permission error", Toast.LENGTH_LONG).show();
            checkAndRequestPermissions();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 85, bytes);
                editIDCardNumber.setText(identityValue + ".JPG");
                idCardNumber = editIDCardNumber.getText().toString();
                Log.e("Activity", "Pick from Camera::>>> ");
                int bmOriginalWidth = bitmap.getWidth();
                int bmOriginalHeight = bitmap.getHeight();
                double originalWidthToHeightRatio = 1.0 * bmOriginalWidth / bmOriginalHeight;
                double originalHeightToWidthRatio = 1.0 * bmOriginalHeight / bmOriginalWidth;
                //choose a maximum height
                int maxHeight = 160;
                //choose a max width
                int maxWidth = 160;
                //call the method to get the scaled bitmap
                bitmap = getResizedBitmap(bitmap, 600);
//                Bitmap bitmap1 = decodeUri(NonSambaUserValidationScreen.this,selectedImage,120);
//                circleImageView.setVisibility(View.VISIBLE);
////                circleImageView.setImageBitmap(getCroppedBitmap(bitmap));
//                RequestOptions options = new RequestOptions();
//                options.centerCrop();
//                options.placeholder(R.drawable.ic_launcher_background);
//                Glide.with(NonSambaUserValidationScreen.this)
//                        .load(getCroppedBitmap(bitmap))
//                        .apply(options)
//                        .into(circleImageView);
                imgString = Base64.encodeToString(getBytesFromBitmap(bitmap),
                        Base64.NO_WRAP);
                Log.d("img--", imgString);
//                deleteLastPhotoFromGallery();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {

            try {
                Uri selectedImage = data.getData();
                Log.d("File", "---" + selectedImage.getPath());
                File file = new File(getRealPathFromURI(selectedImage));
                Log.d("File", "---" + file.length());
                long fileSizeInKB = file.length() / 1024;
                long fileSizeInMB = fileSizeInKB / 1024;

                if (fileSizeInMB > 4) {

                    utils.showDilogForError("You cannot upload file greater than 4MB.", "ERROR");

                } else {

                    bitmap = MediaStore.Images.Media.getBitmap(NonSambaUserValidationScreen.this.getContentResolver(), selectedImage);
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
//                Bitmap out = Bitmap.createScaledBitmap(bitmap, 120, 120, false);
//                Bitmap bitmap1 = resize(bitmap,120,120);
                    editIDCardNumber.setText(identityValue + ".JPG");
                    idCardNumber = editIDCardNumber.getText().toString();

                    int bmOriginalWidth = bitmap.getWidth();
                    int bmOriginalHeight = bitmap.getHeight();
                    double originalWidthToHeightRatio = 1.0 * bmOriginalWidth / bmOriginalHeight;
                    double originalHeightToWidthRatio = 1.0 * bmOriginalHeight / bmOriginalWidth;
                    //choose a maximum height
                    int maxHeight = 140;
                    //choose a max width
                    int maxWidth = 140;
                    //call the method to get the scaled bitmap
                    bitmap = getResizedBitmap(bitmap, 600);
                    Log.e("Activity", "Pick from Gallery::>>> ");
//                imageBtn.setVisibility(View.INVISIBLE);
//                circleImageView.setVisibility(View.VISIBLE);
//                circleImageView.setImageBitmap(getCroppedBitmap(bitmap));
//                RequestOptions options = new RequestOptions();
//                options.centerCrop();
//                options.placeholder(R.drawable.ic_launcher_background);
//                Glide.with(NonSambaUserValidationScreen.this)
//                        .load(bitmap)
//                        .apply(options)
//                        .into(circleImageView);
                    new AsyncCaller().execute(bitmap);

                    Log.d("img--", imgString);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        return stream.toByteArray();
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void deleteLastPhotoFromGallery() {

        String[] projection = new String[]{
                MediaStore.Images.ImageColumns._ID,
                MediaStore.Images.ImageColumns.DATA,
                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATE_TAKEN,
                MediaStore.Images.ImageColumns.MIME_TYPE};

        final Cursor cursor = managedQuery(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null,
                MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

        if (cursor != null) {
            cursor.moveToFirst();
            int column_index_data =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            String image_path = cursor.getString(column_index_data);

            File file = new File(image_path);
            if (file.exists()) {
                file.delete();
            }
            // you will find the last taken picture here and can delete that
        } else {
            Log.d("No--", "Image found");
        }


    }

    @Override
    public void onSupportActionModeStarted(ActionMode mode) {
        super.onSupportActionModeStarted(mode);
        mode.finish();
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {

        int width = bm.getWidth();

        int height = bm.getHeight();

        float scaleWidth = ((float) newWidth) / width;

        float scaleHeight = ((float) newHeight) / height;

        // create a matrix for the manipulation

        Matrix matrix = new Matrix();

        // resize the bit map

        matrix.postScale(scaleWidth, scaleHeight);

        // recreate the new Bitmap

        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

        return resizedBitmap;

    }

    private class AsyncCaller extends AsyncTask<Bitmap, Void, String> {

        public Bitmap bitmap1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

//            pd.show();

        }

        @Override
        protected String doInBackground(Bitmap... params) {

            //this method will be running on a background thread so don't update UI from here
            //do your long-running http tasks here, you don't want to pass argument and u can access the parent class' variable url over here
            try {

                bitmap1 = params[0];
                imgString = Base64.encodeToString(getBytesFromBitmap(bitmap1),
                        Base64.NO_WRAP);

            } catch (Exception e) {

                e.printStackTrace();

            }

            return imgString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


        }

    }

}
