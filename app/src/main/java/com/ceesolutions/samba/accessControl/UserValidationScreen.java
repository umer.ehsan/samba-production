package com.ceesolutions.samba.accessControl;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.hbb20.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ayaz on 12/21/17.
 */

public class UserValidationScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    final int DATE_PICKER_ID = 1;
    public String longitude1;
    public String latitude1;
    String accountNumber, dob, mobileNo, email, motherName, identityValue, monthName, yearWithTwoDigits, identityType;
    private EditText editAccountNum, editMobNumber, editDOB, editEmail, editMotherMaiden;
    private TextView errorMessageForAccount, errorMessageForMobNumber, errorMessageForEmail, errorMessageForMother, errorMessageForDOB, nextBtn;
    private int year, day, month;
    private Dialog pd;
    private WebService webService;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private boolean isValid = false;
    private TextView text2;
    private CountryCodePicker ccpLoadNumber;
    private Utils utils;
    private AppPreferences appPreferences;
    private String location;
    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {


            yearWithTwoDigits = String.valueOf(selectedYear);
            monthName = getMonth(selectedMonth + 1);
            day = selectedDay;

            if (day < 10) {

                editDOB.setText(new StringBuilder().append("0").append(day)
                        .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)));

            } else {

                editDOB.setText(new StringBuilder().append(day)
                        .append(" ").append(monthName.toUpperCase().substring(0, 3)).append(" ").append(yearWithTwoDigits.substring(2, 4)));

            }
            // Show selected date

            dob = editDOB.getText().toString();
            editMobNumber.requestFocus();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_validation_screen);
        initViews();
        getSupportActionBar().hide();
    }

    public void initViews() {

        nextBtn = (TextView) findViewById(R.id.validateBtn);
        editAccountNum = (EditText) findViewById(R.id.editAccountNum);
        editMobNumber = (EditText) findViewById(R.id.editMobNumber);
        editDOB = (EditText) findViewById(R.id.editDOB);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editMotherMaiden = (EditText) findViewById(R.id.editMotherMaiden);
        text2 = (TextView) findViewById(R.id.text2);
        ccpLoadNumber = (CountryCodePicker) findViewById(R.id.ccp_loadFullNumber);
        errorMessageForAccount = (TextView) findViewById(R.id.errorMessageForAccount);
        errorMessageForMobNumber = (TextView) findViewById(R.id.errorMessageForMobNumber);
        errorMessageForEmail = (TextView) findViewById(R.id.errorMessageForEmail);
        errorMessageForMother = (TextView) findViewById(R.id.errorMessageForMother);
        errorMessageForDOB = (TextView) findViewById(R.id.errorMessageForDOB);


        webService = new WebService();
        helper = Helper.getHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(UserValidationScreen.this);

        identityType = constants.credentialsSharedPreferences.getString("identityType", "N/A");
        identityValue = constants.credentialsSharedPreferences.getString("identityValue", "N/A");
        utils = new Utils(UserValidationScreen.this);
        pd = new Dialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        appPreferences = new AppPreferences(UserValidationScreen.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

        accountNumber = editAccountNum.getText().toString().replaceAll("-", "").trim();
        dob = editDOB.getText().toString();
        mobileNo = editMobNumber.getText().toString();
        email = editEmail.getText().toString().trim();
        motherName = editMotherMaiden.getText().toString().trim();

        String txt = "<font COLOR=\'#353535\'>" + "Please provide the required information for registration on SambaSmart against " + "</font>"
                + "<font COLOR=\'#034ea2\'><b>" + identityType + ": " + identityValue + ".";
        text2.setText(Html.fromHtml(txt));

        editMobNumber.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
        editAccountNum.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);

        editDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SelectDate();
                errorMessageForDOB.setVisibility(View.GONE);
            }
        });

        editAccountNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForAccount.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForAccount.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                accountNumber = editable.toString();
                accountNumber = accountNumber.replaceAll("-", "").trim();

            }
        });


        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForEmail.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForEmail.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                email = editable.toString().trim();

            }
        });


        editMobNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForMobNumber.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForMobNumber.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                mobileNo = editable.toString();

//                mobileNo = mobileNo.startsWith("+923") ? mobileNo.replace("+923", "3") : mobileNo.startsWith("03") ? mobileNo.replace("03", "3") : mobileNo.startsWith("923") ? mobileNo.replace("923", "3") : mobileNo;
                mobileNo = mobileNo.replaceAll("-", "").trim();
            }
        });

//        editMobNumber.setText(ccpLoadNumber.getFormattedFullNumber());

        editMotherMaiden.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForMother.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForMother.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                motherName = editable.toString().trim();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FirebaseAnalytics.getInstance(UserValidationScreen.this).logEvent("Registration_Details_Submitted_For_Verif", new Bundle());

                accountNumber = editAccountNum.getText().toString().replaceAll("-", "").trim();
                dob = editDOB.getText().toString();
                mobileNo = editMobNumber.getText().toString().trim();
                email = editEmail.getText().toString().trim();
                motherName = editMotherMaiden.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {

                    errorMessageForEmail.setVisibility(View.VISIBLE);
                    errorMessageForEmail.bringToFront();
                    errorMessageForEmail.setError("");
                    errorMessageForEmail.setText("Please enter valid email address");
                } else {

                    if (!email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") || email.contains(" ")) {
                        errorMessageForEmail.setVisibility(View.VISIBLE);
                        errorMessageForEmail.bringToFront();
                        errorMessageForEmail.setError("");
                        isValid = false;
                        errorMessageForEmail.setText("Please enter valid email address");
                    } else {

                        isValid = true;
                        email = editEmail.getText().toString().trim();

                    }
                }

                if (TextUtils.isEmpty(accountNumber)) {
                    errorMessageForAccount.setVisibility(View.VISIBLE);
                    errorMessageForAccount.bringToFront();
                    errorMessageForAccount.setError("");
                    errorMessageForAccount.setText("Please enter valid Samba account number");

                } else {
                    if (accountNumber.length() < 10) {
                        errorMessageForAccount.setVisibility(View.VISIBLE);
                        errorMessageForAccount.bringToFront();
                        errorMessageForAccount.setError("");
                        errorMessageForAccount.setText("Please enter valid Samba account number");

                    } else if (!helper.validateInputForSC(accountNumber) || accountNumber.contains(" ")) {

                        errorMessageForAccount.setVisibility(View.VISIBLE);
                        errorMessageForAccount.bringToFront();
                        errorMessageForAccount.setError("");
                        errorMessageForAccount.setText("Account number cannot contains <,>,\",',%,(,),&,+,\\,~,space");


                    } else
                        accountNumber = editAccountNum.getText().toString().trim();

                }

                if (TextUtils.isEmpty(mobileNo) || mobileNo.equals("92")) {
                    errorMessageForMobNumber.setVisibility(View.VISIBLE);
                    errorMessageForMobNumber.bringToFront();
                    errorMessageForMobNumber.setError("");
                    errorMessageForMobNumber.setText("Please enter valid mobile number");

                } else if (!helper.validateInputForSC(mobileNo) || mobileNo.contains(" ")) {

                    errorMessageForMobNumber.setVisibility(View.VISIBLE);
                    errorMessageForMobNumber.bringToFront();
                    errorMessageForMobNumber.setError("");
                    errorMessageForMobNumber.setText("Mobile number cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                } else {
                    mobileNo = mobileNo.startsWith("0") ? mobileNo.substring(1) : mobileNo;
                    mobileNo = (ccpLoadNumber.getFullNumber() + mobileNo).replaceAll("-", "").trim();
                }

                if (TextUtils.isEmpty(motherName)) {
                    errorMessageForMother.setVisibility(View.VISIBLE);
                    errorMessageForMother.bringToFront();
                    errorMessageForMother.setError("");
                    errorMessageForMother.setText("Please enter valid mother name");

                } else if (!helper.validateInputForSC(motherName)) {

                    errorMessageForMother.setVisibility(View.VISIBLE);
                    errorMessageForMother.bringToFront();
                    errorMessageForMother.setError("");
                    errorMessageForMother.setText("Mother name cannot contains <,>,\",',%,(,),&,+,\\,~");

                } else
                    motherName = editMotherMaiden.getText().toString().trim();


                if (TextUtils.isEmpty(dob)) {
                    errorMessageForDOB.setVisibility(View.VISIBLE);
                    errorMessageForDOB.bringToFront();
                    errorMessageForDOB.setError("");
                    errorMessageForDOB.setText("Please enter valid date of birth");

                }

                if ((!TextUtils.isEmpty(dob)) && (!TextUtils.isEmpty(motherName) && helper.validateInputForSC(motherName)) && (!TextUtils.isEmpty(mobileNo) && helper.validateInputForSC(mobileNo) && !mobileNo.contains(" ")) && ((!TextUtils.isEmpty(accountNumber) && accountNumber.length() == 10 && helper.validateInputForSC(accountNumber) && !accountNumber.contains(" "))) && ((!TextUtils.isEmpty(email) && isValid == true && helper.validateInputForSC(email) && !email.contains(" ")))) {
//                    Intent intent = new Intent(UserValidationScreen.this, UserCodeVerificationScreen.class);
//                    startActivity(intent);
                    nextBtn.setEnabled(false);
                    UserValidation();

                }


            }
        });


    }

    public void UserValidation() {

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.UserValidation(accountNumber, dob, mobileNo, email, motherName.toUpperCase(), identityValue, identityType, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getValReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(UserValidationScreen.this, "Given information is not valid!", Toast.LENGTH_LONG).show();
//                                nextBtn.setEnabled(true);
//                                pd.dismiss();
//                                utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                clearViews();
//
//                            } else {
//
//                                try {
//                                    if (jsonObject.get("Status_Code").equals("00")) {
//
//                                        nextBtn.setEnabled(true);
//                                        Intent intent = new Intent(UserValidationScreen.this, UserCodeVerificationScreen.class);
//                                        intent.putExtra("identityType", identityType);
//                                        intent.putExtra("identityValue", identityValue);
//                                        intent.putExtra("Type", "Samba");
//                                        intent.putExtra("fullName", jsonObject.getString("T24_FullName"));
//                                        intent.putExtra("T24_CIF", jsonObject.getString("T24_CIF"));
//                                        intent.putExtra("mobileNo", mobileNo);
//                                        intent.putExtra("email", email);
//                                        overridePendingTransition(0, 0);
//                                        pd.dismiss();
//                                        clearViews();
//                                        startActivity(intent);
//                                        finish();
//
//                                    } else if (jsonObject.get("Status_Code").equals("17")) {
//
////                                        Toast.makeText(RobotScreen.this, "User is already registered!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        nextBtn.setEnabled(true);
//                                        utils.showDilogForError("You cannot perform registration on Dormant account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
//
//                                    } else if (jsonObject.get("Status_Code").equals("18")) {
//
////                                        Toast.makeText(RobotScreen.this, "User is already registered!", Toast.LENGTH_LONG).show();
//
//                                        pd.dismiss();
//                                        nextBtn.setEnabled(true);
//
//                                        utils.showDilogForError("You cannot perform registration on Joint account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
//
//                                    } else if (jsonObject.get("Status_Code").equals("19")) {
//
////                                        Toast.makeText(RobotScreen.this, "User is already registered!", Toast.LENGTH_LONG).show();
//
//                                        pd.dismiss();
//                                        nextBtn.setEnabled(true);
//
//                                        utils.showDilogForError("You cannot perform registration on Deceased customer. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
//
//
//                                    }
////                                    Toast.makeText(UserValidationScreen.this, "User Information Verified Successfully.", Toast.LENGTH_LONG).show();
//
//
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                    nextBtn.setEnabled(true);
//                                    pd.dismiss();
//                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                    clearViews();
//                                }
//                            }
//
//                        } else {
////                            Toast.makeText(UserValidationScreen.this, "Given information is not valid!", Toast.LENGTH_LONG).show();
//                            nextBtn.setEnabled(true);
//                            pd.dismiss();
//                            utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                            clearViews();
//
//                        }
//                    }
//                }, 8000);

                final String md5 = helper.md5("" + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", constants.Old_Shared_Key);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sAccountNumber", accountNumber);
                params.put("sDateOfBirth", dob);
                params.put("sMobileNumber", mobileNo);
                params.put("sMotherMaidenName", motherName.toUpperCase());
                params.put("sEmailAddress", email);
                params.put("sIdentityValue", identityValue);
                params.put("sIdentityType", identityType);
                params.put("sSigns_Username", "");
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(UserValidationScreen.this);
                CryptLib _crypt = new CryptLib();
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.userinformationvalidationURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Code").equals("00")) {
//                                        JsonObj.put("T24_FullName", jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("T24_FullName"));
//                                        JsonObj.put("T24_CIF", jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("T24_CIF"));
//                                        JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Code"));
                                            nextBtn.setEnabled(true);
                                            Intent intent = new Intent(UserValidationScreen.this, UserCodeVerificationScreen.class);
                                            intent.putExtra("identityType", identityType);
                                            intent.putExtra("identityValue", identityValue);
                                            intent.putExtra("Type", "Samba");
                                            intent.putExtra("fullName", jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("T24_FullName"));
                                            intent.putExtra("T24_CIF", jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("T24_CIF"));
                                            intent.putExtra("mobileNo", mobileNo);
                                            intent.putExtra("email", email);
                                            overridePendingTransition(0, 0);
                                            pd.dismiss();
                                            clearViews();
                                            startActivity(intent);
                                            finish();

                                        } else if (jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Code").equals("17")) {

                                            pd.dismiss();
                                            nextBtn.setEnabled(true);
                                            utils.showDilogForError("You cannot perform registration on Dormant account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");

                                        } else if (jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Code").equals("18")) {

                                            pd.dismiss();
                                            nextBtn.setEnabled(true);
                                            utils.showDilogForError("You cannot perform registration on Joint account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");

                                        } else if (jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Code").equals("19")) {

                                            pd.dismiss();
                                            nextBtn.setEnabled(true);
                                            utils.showDilogForError("You cannot perform registration on Deceased customer. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");

                                        } else {
                                            nextBtn.setEnabled(true);
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_T24CustomerVerificationResult").getString("Status_Description"), "ERROR");
                                            clearViews();
                                        }

                                    } else {
                                        nextBtn.setEnabled(true);
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        clearViews();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    nextBtn.setEnabled(true);
                                    pd.dismiss();
                                    FirebaseAnalytics.getInstance(UserValidationScreen.this).logEvent("Error", new Bundle());
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    clearViews();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    nextBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    clearViews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    nextBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    clearViews();
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", constants.Old_Shared_Key);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sAccountNumber", accountNumber);
//                        params.put("sDateOfBirth", dob);
//                        params.put("sMobileNumber", mobileNo);
//                        params.put("sMotherMaidenName", motherName.toUpperCase());
//                        params.put("sEmailAddress", email);
//                        params.put("sIdentityValue", identityValue);
//                        params.put("sIdentityType", identityType);
//                        params.put("sSigns_Username", "");
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(UserValidationScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                nextBtn.setEnabled(true);
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                clearViews();

            }
        } catch (Exception e) {

//            Toast.makeText(UserValidationScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            nextBtn.setEnabled(true);
            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            clearViews();

        }
    }

    private void SelectDate() {

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


        showDialog(DATE_PICKER_ID);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_PICKER_ID:


                DatePickerDialog datePickerDialog = new DatePickerDialog(this, pickerListener, year, month, day);
                Calendar today = Calendar.getInstance();
                Date currentDate = today.getTime();
                datePickerDialog.getDatePicker().setMaxDate(currentDate.getTime());
                return datePickerDialog;
        }
        return null;
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }

    public void clearViews() {

        editAccountNum.setText("");
        editDOB.setText("");
        editEmail.setText("");
        editMobNumber.setText("");
        editMotherMaiden.setText("");

        editAccountNum.requestFocus();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UserValidationScreen.this, LoginScreen.class);
        constants.credentialsEditor.clear().commit();
        constants.credentialsEditor.apply();
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSupportActionModeStarted(ActionMode mode) {
        super.onSupportActionModeStarted(mode);
        mode.finish();
    }

}