package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.NonSambaUser.NonSambaUserValidationScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by ayaz on 1/9/18.
 */

public class RobotScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    Map<String, Integer> imagesHashMap = new HashMap<>();
    boolean isSelected = false;
    int[] randomImages = {
            R.drawable.lightbulb_icon,
            R.drawable.phone_icon,
            R.drawable.samba_icon,
            R.drawable.face_icon,
            R.drawable.bank_icon,
            R.drawable.airplane_icon
    };
    List<Integer> imagesList;
    private ImageButton bulbIcon, phoneIcon, faceIcon, bankIcon, sambaIcon, planeIcon;
    private TextView nextBtn, message, doneBtn, heading;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private String userType;
    private String identityType, identityValue, isFP;
    private Utils utils;
    private Dialog dialog;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.robot_screen);
        initViews();
        getSupportActionBar().hide();
    }

    public void initViews() {


        imagesList = new ArrayList<>();
        imagesList.add(R.drawable.lightbulb_icon);
        imagesList.add(R.drawable.phone_icon);
        imagesList.add(R.drawable.samba_icon);
        imagesList.add(R.drawable.face_icon);
        imagesList.add(R.drawable.bank_icon);
        imagesList.add(R.drawable.airplane_icon);
        utils = new Utils(RobotScreen.this);
        requestQueue = Volley.newRequestQueue(RobotScreen.this);
        webService = new WebService();
        helper = Helper.getHelper(RobotScreen.this);
        constants = Constants.getConstants(RobotScreen.this);
        pd = new Dialog(RobotScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        appPreferences = new AppPreferences(RobotScreen.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        Intent intent = getIntent();
        isFP = intent.getStringExtra("FP");

        identityType = constants.credentialsSharedPreferences.getString("identityType", "N/A");
        identityValue = constants.credentialsSharedPreferences.getString("identityValue", "N/A");
        userType = constants.credentialsSharedPreferences.getString("userType", "N/A");
//        if (identityType.equals("NTN")) {
//            identityValue = constants.credentialsSharedPreferences.getString("ntn", "N/A");
//        } else if (identityType.equals("SCNIC")) {
//            identityValue = constants.credentialsSharedPreferences.getString("scnic", "N/A");
//        } else if (identityType.equals("PASSPORT")) {
//            identityValue = constants.credentialsSharedPreferences.getString("passport", "N/A");
//        } else if (identityType.equals("CNIC")) {
//            identityValue = constants.credentialsSharedPreferences.getString("cnic", "N/A");
//
//        }

        imagesHashMap.put("lightbulb_icon", R.drawable.lightbulb);
        imagesHashMap.put("phone_icon", R.drawable.phone_outline);
        imagesHashMap.put("samba_icon", R.drawable.samba_outline);
        imagesHashMap.put("face_icon", R.drawable.face_outline);
        imagesHashMap.put("bank_icon", R.drawable.bank_outline);
        imagesHashMap.put("airplane_icon", R.drawable.airplane_outline);

        bulbIcon = (ImageButton) findViewById(R.id.bulbIcon);
        phoneIcon = (ImageButton) findViewById(R.id.phoneIcon);
        faceIcon = (ImageButton) findViewById(R.id.faceIcon);
        bankIcon = (ImageButton) findViewById(R.id.bankIcon);
        sambaIcon = (ImageButton) findViewById(R.id.sambaIcon);
        planeIcon = (ImageButton) findViewById(R.id.planeIcon);
        nextBtn = (TextView) findViewById(R.id.nextBtn);

        ImageButton[] imageButton = {
                bulbIcon, phoneIcon, faceIcon, bankIcon, sambaIcon, planeIcon
        };
        Random generator = new Random();
        int randomImageId;
        final Map<String, String> hashMap = new HashMap<>();

        for (int i = 0; i < randomImages.length; i++) {

            int id = generator.nextInt(imagesList.size());
            randomImageId = imagesList.get(id);
            imageButton[i].setImageResource(randomImageId);
            imageButton[i].setTag(randomImageId);
            String name = getResources().getResourceEntryName(randomImageId);
            hashMap.put(imageButton[i].getId() + "", name);
            imagesList.remove(id);

        }


        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                if (isSelected) {


                    if (TextUtils.isEmpty(isFP)) {

                        FirebaseAnalytics.getInstance(RobotScreen.this).logEvent("Registration_Captcha_Submitted", new Bundle());
                        pd.show();
                        nextBtn.setEnabled(false);
                        Registration();

                    } else {

                        FirebaseAnalytics.getInstance(RobotScreen.this).logEvent("Forget_Password_Captcha_Submitted", new Bundle());
                        Intent intent = new Intent(RobotScreen.this, ForgotPasswordValidation.class);
                        nextBtn.setEnabled(false);
                        startActivity(intent);
                        finish();
                        nextBtn.setEnabled(true);

                    }


                } else {

//                    Toast.makeText(RobotScreen.this, "Invalid selection!", Toast.LENGTH_LONG).show();
                    showDilogForErrorForLogin("Invalid Selection", "ERROR", "0");

                }
            }
        });

        bulbIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (Map.Entry<String, String> mapEntry : hashMap.entrySet()) {
                    String key = mapEntry.getKey();
                    String value = mapEntry.getValue();

                    ImageButton imageButton1 = (ImageButton) findViewById(Integer.parseInt(key));
                    int resID = getResources().getIdentifier(value, "drawable", getPackageName());
                    imageButton1.setImageResource(resID);
                }

                String id = bulbIcon.getId() + "";

                String value = hashMap.get(id);
                if (value.equals("samba_icon")) {

                    isSelected = true;
                } else {
                    isSelected = false;

                }

                int secondId = imagesHashMap.get(value);

                bulbIcon.setImageResource(secondId);

            }
        });

        phoneIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for (Map.Entry<String, String> mapEntry : hashMap.entrySet()) {
                    String key = mapEntry.getKey();
                    String value = mapEntry.getValue();

                    ImageButton imageButton1 = (ImageButton) findViewById(Integer.parseInt(key));
                    int resID = getResources().getIdentifier(value, "drawable", getPackageName());
                    imageButton1.setImageResource(resID);
                }

                String id = phoneIcon.getId() + "";

                String value = hashMap.get(id);
                if (value.equals("samba_icon")) {

                    isSelected = true;
                } else {
                    isSelected = false;

                }
                int secondId = imagesHashMap.get(value);

                phoneIcon.setImageResource(secondId);

            }
        });

        faceIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (Map.Entry<String, String> mapEntry : hashMap.entrySet()) {
                    String key = mapEntry.getKey();
                    String value = mapEntry.getValue();

                    ImageButton imageButton1 = (ImageButton) findViewById(Integer.parseInt(key));
                    int resID = getResources().getIdentifier(value, "drawable", getPackageName());
                    imageButton1.setImageResource(resID);
                }

                String id = faceIcon.getId() + "";

                String value = hashMap.get(id);
                if (value.equals("samba_icon")) {

                    isSelected = true;
                } else {
                    isSelected = false;

                }
                int secondId = imagesHashMap.get(value);

                faceIcon.setImageResource(secondId);

            }
        });

        bankIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (Map.Entry<String, String> mapEntry : hashMap.entrySet()) {
                    String key = mapEntry.getKey();
                    String value = mapEntry.getValue();

                    ImageButton imageButton1 = (ImageButton) findViewById(Integer.parseInt(key));
                    int resID = getResources().getIdentifier(value, "drawable", getPackageName());
                    imageButton1.setImageResource(resID);
                }

                String id = bankIcon.getId() + "";

                String value = hashMap.get(id);
                if (value.equals("samba_icon")) {

                    isSelected = true;
                } else {
                    isSelected = false;

                }
                int secondId = imagesHashMap.get(value);

                bankIcon.setImageResource(secondId);

            }
        });


        sambaIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for (Map.Entry<String, String> mapEntry : hashMap.entrySet()) {
                    String key = mapEntry.getKey();
                    String value = mapEntry.getValue();

                    ImageButton imageButton1 = (ImageButton) findViewById(Integer.parseInt(key));
                    int resID = getResources().getIdentifier(value, "drawable", getPackageName());
                    imageButton1.setImageResource(resID);
                }

                String id = sambaIcon.getId() + "";

                String value = hashMap.get(id);
                if (value.equals("samba_icon")) {

                    isSelected = true;
                } else {
                    isSelected = false;

                }
                int secondId = imagesHashMap.get(value);

                sambaIcon.setImageResource(secondId);

            }
        });

        planeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for (Map.Entry<String, String> mapEntry : hashMap.entrySet()) {
                    String key = mapEntry.getKey();
                    String value = mapEntry.getValue();

                    ImageButton imageButton1 = (ImageButton) findViewById(Integer.parseInt(key));
                    int resID = getResources().getIdentifier(value, "drawable", getPackageName());
                    imageButton1.setImageResource(resID);
                }

                String id = planeIcon.getId() + "";

                String value = hashMap.get(id);
                if (value.equals("samba_icon")) {

                    isSelected = true;
                } else {
                    isSelected = false;

                }
                int secondId = imagesHashMap.get(value);

                planeIcon.setImageResource(secondId);

            }
        });


    }


    public void Registration() {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

//                webService.Registration(identityValue, constants.version, constants.model, constants.dateTime, constants.address, identityType, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getRegReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(RobotScreen.this, "Information is not valid!", Toast.LENGTH_LONG).show();
//
//
//                                showDilogForErrorForLogin("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR", "Error");
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
//
////                                        Toast.makeText(RobotScreen.this, "Information Validate Successfully", Toast.LENGTH_LONG).show();
//                                        Intent intent = new Intent(RobotScreen.this, UserValidationScreen.class);
//                                        pd.dismiss();
//                                        RobotScreen.this.overridePendingTransition(0, 0);
//                                        intent.putExtra("Type", "Samba");
//                                        startActivity(intent);
//                                        nextBtn.setEnabled(true);
//                                        finish();
//                                    } else if (jsonObject.get("Status_Code").equals("13")) {
//
////                                        Toast.makeText(RobotScreen.this, "User is already registered!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        showDilogForErrorForLogin("User already exists against this Legal Identity.", "ERROR", "13");
//                                        nextBtn.setEnabled(true);
//
//                                    } else if (jsonObject.get("Status_Code").equals("11")) {
//
////                                        Toast.makeText(RobotScreen.this, "User is already registered!", Toast.LENGTH_LONG).show();
//                                        Intent intent = new Intent(RobotScreen.this, NonSambaUserValidationScreen.class);
//                                        pd.dismiss();
//                                        RobotScreen.this.overridePendingTransition(0, 0);
//                                        intent.putExtra("Type", "NonSamba");
//                                        startActivity(intent);
//                                        nextBtn.setEnabled(true);
//                                        finish();
//
//                                    } else if (jsonObject.get("Status_Code").equals("17")) {
//
////                                        Toast.makeText(RobotScreen.this, "User is already registered!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        nextBtn.setEnabled(true);
//                                        utils.showDilogForError("You cannot perform registration on Dormant account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
//
//                                    } else if (jsonObject.get("Status_Code").equals("18")) {
//
////                                        Toast.makeText(RobotScreen.this, "User is already registered!", Toast.LENGTH_LONG).show();
//
//                                        pd.dismiss();
//                                        nextBtn.setEnabled(true);
//
//                                        utils.showDilogForError("You cannot perform registration on Joint account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
//
//                                    } else if (jsonObject.get("Status_Code").equals("19")) {
//
////                                        Toast.makeText(RobotScreen.this, "User is already registered!", Toast.LENGTH_LONG).show();
//
//                                        pd.dismiss();
//                                        nextBtn.setEnabled(true);
//
//                                        utils.showDilogForError("You cannot perform registration on Deceased customer. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
//
//
//                                    } else if (jsonObject.get("Status_Code").equals("20")) {
//
////                                        Toast.makeText(RobotScreen.this, "User is already registered!", Toast.LENGTH_LONG).show();
//
//                                        pd.dismiss();
//                                        nextBtn.setEnabled(true);
//
//                                        showDilogForErrorForLogin("Legal Identity Number does not match with Samba records. Please ensure you have selected the correct Identity Type/Number combination. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR", "ERROR");
//
//
//                                    } else {
//                                        pd.dismiss();
//                                        nextBtn.setEnabled(true);
//                                        showDilogForErrorForLogin("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR", "Error");
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    nextBtn.setEnabled(true);
//                                    showDilogForErrorForLogin("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR", "Error");
//
//                                }
//                            }
//                        } else {
//                            pd.dismiss();
//                            nextBtn.setEnabled(true);
//                            showDilogForErrorForLogin("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR", "Error");
//
//
//                        }
//                    }
//                }, 10000);
                final String md5 = helper.md5("" + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", constants.Old_Shared_Key);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sIdentityValue", identityValue);
                params.put("sIdentityType", identityType);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                params.put("sSigns_Username", "");
                HttpsTrustManager.allowMySSL(RobotScreen.this);
                CryptLib _crypt = new CryptLib();
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.uservalidationURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("00")) {

                                            if (userType.equals("Samba")) {
                                                Intent intent = new Intent(RobotScreen.this, UserValidationScreen.class);
                                                pd.dismiss();
                                                RobotScreen.this.overridePendingTransition(0, 0);
                                                intent.putExtra("Type", "Samba");
                                                startActivity(intent);
                                                nextBtn.setEnabled(true);
                                                finish();
                                            } else {

                                                pd.dismiss();
                                                showDilogForErrorForLogin("It seems like you have an account with us. Please registrator as a Samba customer.", "ERROR", "13");
                                                nextBtn.setEnabled(true);

                                            }

                                        } else if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("13")) {

                                            pd.dismiss();
                                            showDilogForErrorForLogin("User already exists against this Legal Identity.", "ERROR", "13");
                                            nextBtn.setEnabled(true);

                                        } else if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("11")) {

                                            if (userType.equals("Samba")) {

                                                pd.dismiss();
                                                showDilogForErrorForLogin("Legal Identity does not exist in our record. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR", "13");
                                                nextBtn.setEnabled(true);

                                            } else {

                                                Intent intent = new Intent(RobotScreen.this, NonSambaUserValidationScreen.class);
                                                pd.dismiss();
                                                RobotScreen.this.overridePendingTransition(0, 0);
                                                intent.putExtra("Type", "NonSamba");
                                                startActivity(intent);
                                                nextBtn.setEnabled(true);
                                                finish();
                                            }


                                        } else if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("17")) {

                                            pd.dismiss();
                                            nextBtn.setEnabled(true);
                                            utils.showDilogForError("You cannot perform registration on Dormant account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");

                                        } else if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("18")) {

                                            pd.dismiss();
                                            nextBtn.setEnabled(true);
                                            utils.showDilogForError("You cannot perform registration on Joint account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");

                                        } else if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("19")) {

                                            pd.dismiss();
                                            nextBtn.setEnabled(true);
                                            utils.showDilogForError("You cannot perform registration on Deceased customer. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");

                                        } else if (jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Code").equals("20")) {

                                            pd.dismiss();
                                            nextBtn.setEnabled(true);
                                            showDilogForErrorForLogin("Legal Identity Number does not match with Samba records. Please ensure you have selected the correct Identity Type/Number combination. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR", "ERROR");

                                        } else {

                                            pd.dismiss();
                                            nextBtn.setEnabled(true);
                                            showDilogForErrorForLogin(jsonObject.getJSONObject("Signs_ValidateIdentityResult").getString("Status_Description"), "ERROR", "Error");

                                        }
                                    } else {

                                        pd.dismiss();
                                        nextBtn.setEnabled(true);
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");

                                    }
                                } catch (JSONException e) {
                                    pd.dismiss();
                                    nextBtn.setEnabled(true);
                                    showDilogForErrorForLogin("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR", "Error");

                                }

                            }
                        },
                        new Response.ErrorListener()

                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    nextBtn.setEnabled(true);
                                    showDilogForErrorForLogin("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR", "Error");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    nextBtn.setEnabled(true);
                                    showDilogForErrorForLogin("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR", "Error");

                                }
                            }
                        })

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", constants.Old_Shared_Key);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sIdentityValue", identityValue);
//                        params.put("sIdentityType", identityType);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        params.put("sSigns_Username", "");
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };
                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                pd.dismiss();
//                Toast.makeText(RobotScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                nextBtn.setEnabled(true);


            }
        } catch (Exception e) {

            pd.dismiss();
//                Toast.makeText(RobotScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            nextBtn.setEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {


        Intent intent = new Intent(RobotScreen.this, LoginScreen.class);
        RobotScreen.this.overridePendingTransition(0, 0);
        startActivity(intent);
        nextBtn.setEnabled(true);
        finish();


    }

    public void showDilogForErrorForLogin(String msg, String header, final String code) {

        dialog = new Dialog(RobotScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                switch (code) {


                    case "13": {
                        Intent intent = new Intent(RobotScreen.this, LoginScreen.class);
                        pd.dismiss();
                        dialog.dismiss();
                        RobotScreen.this.overridePendingTransition(0, 0);
                        startActivity(intent);
                        nextBtn.setEnabled(true);
                        constants.credentialsEditor.clear();
                        constants.credentialsEditor.commit();
                        constants.credentialsEditor.apply();
                        finish();
                        break;
                    }
                    case "Error": {

                        Intent intent = new Intent(RobotScreen.this, LoginScreen.class);
                        pd.dismiss();
                        dialog.dismiss();
                        RobotScreen.this.overridePendingTransition(0, 0);
                        startActivity(intent);
                        nextBtn.setEnabled(true);
                        constants.credentialsEditor.clear();
                        constants.credentialsEditor.commit();
                        constants.credentialsEditor.apply();
                        finish();

                        break;
                    }

                    case "0":

                        if (TextUtils.isEmpty(isFP)) {
                            Intent intent2 = new Intent(RobotScreen.this, RobotScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(intent2);
                            finish();
                        } else {
                            Intent intent2 = new Intent(RobotScreen.this, RobotScreen.class);
                            overridePendingTransition(0, 0);
                            intent2.putExtra("FP", isFP);
                            startActivity(intent2);
                            finish();
                        }
                        break;

                    default: {

                        Intent intent = new Intent(RobotScreen.this, LoginScreen.class);
                        pd.dismiss();
                        dialog.dismiss();
                        RobotScreen.this.overridePendingTransition(0, 0);
                        startActivity(intent);
                        nextBtn.setEnabled(true);
                        constants.credentialsEditor.clear();
                        constants.credentialsEditor.commit();
                        constants.credentialsEditor.apply();
                        finish();
                    }
                }


            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();


    }
}

