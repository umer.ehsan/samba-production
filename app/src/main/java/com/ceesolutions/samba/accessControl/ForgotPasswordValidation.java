package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ayaz on 1/30/18.
 */

public class ForgotPasswordValidation extends AppCompatActivity {


    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    String userName, email, motherMaiden, identityType, identityValue;
    private EditText editName, editEmail, editMotherMaiden;
    private TextView errorMessageForUsername, errorMessageForMother, errorMessageForEmail, nextBtn, message, heading, doneBtn;
    private Dialog pd;
    private WebService webService;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private boolean isValid = false;
    private EditText editIdentity;
    private Spinner identitiesSpinner;
    private String identitySelected;
    private String[] identities = {
            "CNIC",
            "NICOP",
            "NTN",
            "PASSPORT",
            "POC"

    };
    private String identityAnswer, isFP;
    private TextView errorMessage;
    private TextInputLayout identityTxtInput;
    private Utils utils;
    private Dialog dialog;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_validation);
        initViews();
        getSupportActionBar().hide();
    }

    public void initViews() {

        nextBtn = (TextView) findViewById(R.id.validateBtn);
        editMotherMaiden = (EditText) findViewById(R.id.editMotherMaiden);
        editEmail = (EditText) findViewById(R.id.editEmail);
        editName = (EditText) findViewById(R.id.editName);


        errorMessageForUsername = (TextView) findViewById(R.id.errorMessageForUsername);
        errorMessageForEmail = (TextView) findViewById(R.id.errorMessageForEmail);
        errorMessageForMother = (TextView) findViewById(R.id.errorMessageForMother);

        webService = new WebService();
        helper = Helper.getHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        pd = new Dialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        constants = Constants.getConstants(ForgotPasswordValidation.this);
        utils = new Utils(ForgotPasswordValidation.this);
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        userName = editName.getText().toString().trim();
        email = editEmail.getText().toString().trim();
        motherMaiden = editMotherMaiden.getText().toString().trim();
        identityTxtInput = (TextInputLayout) findViewById(R.id.identityTxtInput);
        errorMessage = (TextView) findViewById(R.id.errorMessage);
        editIdentity = (EditText) findViewById(R.id.editIdentity);
        identitiesSpinner = (Spinner) findViewById(R.id.identitiesSpinner);
        spinnerAdapter questionAdapter = new spinnerAdapter(ForgotPasswordValidation.this, R.layout.custom_textview_fp);
        identityAnswer = editIdentity.getText().toString().trim();
        questionAdapter.addAll(identities);
        questionAdapter.add("Identity Type");
        identitiesSpinner.setAdapter(questionAdapter);
        identitiesSpinner.setSelection(0);
        identitiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                try {


                    // Get select item
                    if (identitiesSpinner.getSelectedItem() == "Identity Type") {
                        identitySelected = identitiesSpinner.getSelectedItem().toString();
                        Log.d("identity", "---" + identitiesSpinner);
                        identityTxtInput.setHint("Identity Value");
                    } else {

                        identitySelected = identitiesSpinner.getSelectedItem().toString();

                        errorMessage.setVisibility(View.INVISIBLE);
//                        editIdentity.requestFocus();
//                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                        imm.showSoftInput(editIdentity, InputMethodManager.SHOW_IMPLICIT);

                        if (identitySelected.equals("CNIC")) {
                            editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                            editIdentity.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
                            identityTxtInput.setVisibility(View.VISIBLE);
                            identityTxtInput.setHint("CNIC Number");
                            editIdentity.setText("");
                            int maxLengthofEditText = 13;
                            editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});

                        } else if (identitySelected.equals("PASSPORT")) {


                            editIdentity.setInputType(InputType.TYPE_CLASS_TEXT);
                            identityTxtInput.setVisibility(View.VISIBLE);
                            identityTxtInput.setHint("Passport Number");
                            editIdentity.setHint("");
                            editIdentity.setText("");
                            int maxLengthofEditText = 15;
                            editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});

                        } else if (identitySelected.equals("NTN")) {

                            //&#8208; (Replace Hyphen with this)
                            editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                            editIdentity.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
                            identityTxtInput.setVisibility(View.VISIBLE);
                            identityTxtInput.setHint("NTN Number");
                            editIdentity.setHint("");
                            editIdentity.setText("");
                            int maxLengthofEditText = 13;
                            editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});


                        } else if (identitySelected.equals("POC")) {


                            editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                            identityTxtInput.setVisibility(View.VISIBLE);
                            identityTxtInput.setHint("POC Number");
                            editIdentity.setHint("");
                            editIdentity.setText("");
                            int maxLengthofEditText = 13;
                            editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});

                        } else if (identitySelected.equals("NICOP")) {


                            editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                            editIdentity.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
                            identityTxtInput.setVisibility(View.VISIBLE);
                            identityTxtInput.setHint("NICOP Number");
                            editIdentity.setHint("");
                            editIdentity.setText("");
                            int maxLengthofEditText = 13;
                            editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        editIdentity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                identityAnswer = editable.toString().trim();

            }
        });

        editName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForUsername.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForUsername.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                userName = editable.toString();

            }
        });


        editEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForEmail.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForEmail.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                email = editable.toString();

            }
        });


        editMotherMaiden.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForMother.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForMother.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                motherMaiden = editable.toString();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                FirebaseAnalytics.getInstance(ForgotPasswordValidation.this).logEvent("Details_For_Reset_Password_Entered", new Bundle());

                userName = editName.getText().toString().trim();
                email = editEmail.getText().toString().trim();
                motherMaiden = editMotherMaiden.getText().toString().trim();
                identityAnswer = editIdentity.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {

                    errorMessageForEmail.setVisibility(View.VISIBLE);
                    errorMessageForEmail.bringToFront();
                    errorMessageForEmail.setError("");
                    errorMessageForEmail.setText("Please enter valid email address");
                } else {

                    if (!email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") || email.contains(" ")) {
                        errorMessageForEmail.setVisibility(View.VISIBLE);
                        errorMessageForEmail.bringToFront();
                        errorMessageForEmail.setError("");
                        isValid = false;
                        errorMessageForEmail.setText("Please enter valid email address");
                    } else {

                        isValid = true;
                        email = editEmail.getText().toString().trim();

                    }
                }

                if (TextUtils.isEmpty(userName)) {

                    errorMessageForUsername.setVisibility(View.VISIBLE);
                    errorMessageForUsername.bringToFront();
                    errorMessageForUsername.setError("");
                    errorMessageForUsername.setText("Please enter valid username");

                } else if (!helper.validateInputForSC(userName) || userName.contains(" ")) {


                    errorMessageForUsername.setVisibility(View.VISIBLE);
                    errorMessageForUsername.bringToFront();
                    errorMessageForUsername.setError("");
                    errorMessageForUsername.setText("Username cannot contains <,>,\",',%,(,),&,+,\\,~,space");


                } else {
                    userName = editName.getText().toString().trim();
                }


                if (TextUtils.isEmpty(motherMaiden)) {
                    errorMessageForMother.setVisibility(View.VISIBLE);
                    errorMessageForMother.bringToFront();
                    errorMessageForMother.setError("");
                    errorMessageForMother.setText("Please enter valid mother maiden name");

                } else if (!helper.validateInputForSC(motherMaiden)) {

                    errorMessageForMother.setVisibility(View.VISIBLE);
                    errorMessageForMother.bringToFront();
                    errorMessageForMother.setError("");
                    errorMessageForMother.setText("Mother's Name cannot contains <,>,\",',%,(,),&,+,\\,~");
                } else {

                    motherMaiden = editMotherMaiden.getText().toString().trim();
                }


                if (TextUtils.isEmpty(identityAnswer)) {
                    if (identitySelected.equals("CNIC")) {
                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("Please enter valid CNIC number");

                    } else if (identitySelected.equals("PASSPORT")) {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("Please enter valid Passport number");

                    } else if (identitySelected.equals("NTN")) {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("Please enter valid NTN number");

                    } else if (identitySelected.equals("POC")) {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("Please enter valid POC number");

                    } else if (identitySelected.equals("NICOP")) {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("Please enter valid NICOP number");

                    } else {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("Please enter valid identity value");
                    }

                } else if (!helper.validateInputForSC(identityAnswer) || identityAnswer.contains(" ")) {

                    if (identitySelected.equals("CNIC")) {
                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("CNIC number cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                    } else if (identitySelected.equals("PASSPORT")) {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("Passport number cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                    } else if (identitySelected.equals("NTN")) {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("NTN number cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                    } else if (identitySelected.equals("POC")) {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("POC number cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                    } else if (identitySelected.equals("NICOP")) {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("NICOP number cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                    } else {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.bringToFront();
                        errorMessage.setError("");
                        errorMessage.setText("Identity value cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                    }

                } else {

                    if (identitySelected.equals("CNIC")) {

                        if (identityAnswer.length() != 13) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("Please enter valid CNIC number");
                        } else if ((!TextUtils.isEmpty(motherMaiden) && helper.validateInputForSC(motherMaiden)) && ((!TextUtils.isEmpty(userName) && helper.validateInputForSC(userName) && !userName.contains(" "))) && ((!TextUtils.isEmpty(email) && isValid == true && !email.contains(" "))) && (!TextUtils.isEmpty(identityAnswer) && helper.validateInputForSC(identityAnswer) && !identityAnswer.contains(" ") && identityAnswer.length() == 13)) {

                            nextBtn.setEnabled(false);
                            ForgotPasswordValidation();


                        }


                    } else if (identitySelected.equals("PASSPORT")) {


                        if (identityAnswer.length() < 1) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("Please enter valid Passport number");
                        } else if ((!TextUtils.isEmpty(motherMaiden) && helper.validateInputForSC(motherMaiden)) && ((!TextUtils.isEmpty(userName) && helper.validateInputForSC(userName) && !userName.contains(" "))) && ((!TextUtils.isEmpty(email) && isValid == true && !email.contains(" "))) && (!TextUtils.isEmpty(identityAnswer) && helper.validateInputForSC(identityAnswer) && !identityAnswer.contains(" ") && identityAnswer.length() > 1)) {

                            nextBtn.setEnabled(false);
                            ForgotPasswordValidation();


                        }

                    } else if (identitySelected.equals("NTN")) {

                        if (identityAnswer.length() < 1) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("Please enter valid NTN number");
                        } else if ((!TextUtils.isEmpty(motherMaiden) && helper.validateInputForSC(motherMaiden)) && ((!TextUtils.isEmpty(userName) && helper.validateInputForSC(userName) && !userName.contains(" "))) && ((!TextUtils.isEmpty(email) && isValid == true && !email.contains(" "))) && (!TextUtils.isEmpty(identityAnswer) && helper.validateInputForSC(identityAnswer) && !identityAnswer.contains(" ") && identityAnswer.length() > 1)) {

                            nextBtn.setEnabled(false);
                            ForgotPasswordValidation();
                        }


                    } else if (identitySelected.equals("POC")) {

                        if (identityAnswer.length() < 1) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("Please enter valid POC number");
                        } else if ((!TextUtils.isEmpty(motherMaiden) && helper.validateInputForSC(motherMaiden)) && ((!TextUtils.isEmpty(userName) && helper.validateInputForSC(userName) && !userName.contains(" "))) && ((!TextUtils.isEmpty(email) && isValid == true && !email.contains(" "))) && (!TextUtils.isEmpty(identityAnswer) && helper.validateInputForSC(identityAnswer) && !identityAnswer.contains(" ") && identityAnswer.length() > 1)) {

                            nextBtn.setEnabled(false);
                            ForgotPasswordValidation();


                        }

                    } else if (identitySelected.equals("NICOP")) {

                        if (identityAnswer.length() != 13) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("Please enter valid NICOP number");

                        } else if ((!TextUtils.isEmpty(motherMaiden) && helper.validateInputForSC(motherMaiden)) && ((!TextUtils.isEmpty(userName) && helper.validateInputForSC(userName) && !userName.contains(" "))) && ((!TextUtils.isEmpty(email) && isValid == true && !email.contains(" "))) && (!TextUtils.isEmpty(identityAnswer) && helper.validateInputForSC(identityAnswer) && !identityAnswer.contains(" ") && identityAnswer.length() == 13)) {

                            nextBtn.setEnabled(false);
                            ForgotPasswordValidation();


                        }


                    }

                }


//                if ((!TextUtils.isEmpty(motherMaiden) && helper.validateInputForSC(motherMaiden)) && ((!TextUtils.isEmpty(userName) && helper.validateInputForSC(userName) && !userName.contains(" "))) && ((!TextUtils.isEmpty(email) && isValid == true && !email.contains(" "))) && (!TextUtils.isEmpty(identityAnswer) && helper.validateInputForSC(identityAnswer) && !identityAnswer.contains(" "))) {
////                    Intent intent = new Intent(ForgotPasswordValidation.this, UserCodeVerificationScreen.class);
////                    startActivity(intent);
////                    finish();
//                    nextBtn.setEnabled(false);
//                    ForgotPasswordValidation();
//
//
//                }


            }
        });


    }

    public void ForgotPasswordValidation() {

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.ForgotPasswordValidation(userName, email, motherMaiden.toUpperCase(), identitySelected, identityAnswer, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getForgotPassValidationReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//                                pd.dismiss();
////                                Toast.makeText(ForgotPasswordValidation.this, "Given information is not valid!", Toast.LENGTH_LONG).show();
//                                utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//
//                                nextBtn.setEnabled(true);
//
//                                clearViews();
//
//                            } else {
//
//
//                                try {
//
//                                    if (jsonObject.get("Status_Code").equals("17")) {
//                                        pd.dismiss();
//                                        nextBtn.setEnabled(true);
//                                        utils.showDilogForError("You cannot perform forgot password on Dormant account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                        clearViews();
//
//                                    } else if (jsonObject.get("Status_Code").equals("18")) {
//                                        pd.dismiss();
//                                        nextBtn.setEnabled(true);
//                                        utils.showDilogForError("You cannot perform forgot password on Joint account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                        clearViews();
//
//
//                                    } else if (jsonObject.get("Status_Code").equals("19")) {
//                                        pd.dismiss();
//                                        nextBtn.setEnabled(true);
//                                        utils.showDilogForError("You cannot perform forgot password on Deceased customer. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                        clearViews();
//
//                                    } else if (jsonObject.get("Status_Code").equals("20")) {
//                                        pd.dismiss();
//                                        nextBtn.setEnabled(true);
//                                        utils.showDilogForError("Legal Identity Number does not match with Samba records. Please ensure you have selected the correct Identity Type/Number combination. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                        clearViews();
//
//                                    } else if (jsonObject.get("Status_Code").equals("00")) {
////                                    Log.d("TAG", "--->" + jsonObject);
////                                    Toast.makeText(ForgotPasswordValidation.this, "User Information Verified Successfully.", Toast.LENGTH_LONG).show();
//                                        nextBtn.setEnabled(true);
//                                        Intent intent = new Intent(ForgotPasswordValidation.this, UserCodeVerificationScreen.class);
//                                        intent.putExtra("FP", "FP");
//                                        constants.forgotPasswordEditor.putString("identityType", identitySelected);
//                                        constants.forgotPasswordEditor.putString("identityValue", identityAnswer);
//                                        constants.forgotPasswordEditor.putString("fullName", jsonObject.getString("FullName"));
//                                        constants.forgotPasswordEditor.putString("username", userName);
//                                        constants.forgotPasswordEditor.putString("email", email);
//                                        constants.forgotPasswordEditor.putString("mobileNo", jsonObject.getString("Mobile_Number"));
//                                        constants.forgotPasswordEditor.putString("motherMaiden", motherMaiden);
//                                        constants.forgotPasswordEditor.commit();
//                                        constants.forgotPasswordEditor.apply();
//                                        clearViews();
//                                        pd.dismiss();
//                                        startActivity(intent);
//                                        finish();
//
//                                    }
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                    nextBtn.setEnabled(true);
//                                    pd.dismiss();
//                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                    clearViews();
//                                }
//                            }
//
//                        } else {
//                            pd.dismiss();
////                            Toast.makeText(ForgotPasswordValidation.this, "Given information is not valid!", Toast.LENGTH_LONG).show();
//                            utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//
//                            nextBtn.setEnabled(true);
//
//                            clearViews();
//
//                        }
//                    }
//                }, 5000);
                final String md5 = helper.md5(userName.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", constants.Old_Shared_Key);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sUserMotherMaidenName", motherMaiden.toUpperCase());
                params.put("sEmailAddress", email);
                params.put("sLegalIdentityType", identitySelected);
                params.put("sLegalIdentityValue", identityAnswer);
                params.put("sSigns_Username", userName);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(ForgotPasswordValidation.this);
                CryptLib _crypt = new CryptLib();
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.forgotPasswordvalidationURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code").equals("00")) {

                                            nextBtn.setEnabled(true);
                                            Intent intent = new Intent(ForgotPasswordValidation.this, UserCodeVerificationScreen.class);
                                            intent.putExtra("FP", "FP");
                                            constants.forgotPasswordEditor.putString("identityType", identitySelected);
                                            constants.forgotPasswordEditor.putString("identityValue", identityAnswer);
                                            constants.forgotPasswordEditor.putString("fullName", jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("FullName"));
                                            constants.forgotPasswordEditor.putString("username", userName);
                                            constants.forgotPasswordEditor.putString("email", email);
                                            constants.forgotPasswordEditor.putString("mobileNo", jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Mobile_Number"));
                                            constants.forgotPasswordEditor.putString("motherMaiden", motherMaiden);
                                            constants.forgotPasswordEditor.commit();
                                            constants.forgotPasswordEditor.apply();
                                            clearViews();
                                            pd.dismiss();
                                            startActivity(intent);
                                            finish();

                                        } else if (jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code").equals("17")) {
                                            pd.dismiss();
                                            nextBtn.setEnabled(true);
                                            utils.showDilogForError("You cannot perform forgot password on Dormant account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                            clearViews();


                                        } else if (jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code").equals("18")) {
                                            pd.dismiss();
                                            nextBtn.setEnabled(true);
                                            utils.showDilogForError("You cannot perform forgot password on Joint account. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                            clearViews();


                                        } else if (jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code").equals("19")) {
                                            pd.dismiss();
                                            nextBtn.setEnabled(true);
                                            utils.showDilogForError("You cannot perform forgot password on Deceased customer. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                            clearViews();


                                        } else if (jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Code").equals("20")) {
                                            pd.dismiss();
                                            nextBtn.setEnabled(true);
                                            utils.showDilogForError("Legal Identity Number does not match with Samba records. Please ensure you have selected the correct Identity Type/Number combination. Kindly contact SambaPhone Banking at +92-21-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                            clearViews();


                                        } else {
                                            pd.dismiss();
                                            nextBtn.setEnabled(true);
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_ForgotPasswordUserVerificationResult").getString("Status_Description"), "ERROR");
                                            clearViews();
                                        }
                                    } else {

                                        pd.dismiss();
                                        nextBtn.setEnabled(true);
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        clearViews();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    nextBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    clearViews();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    nextBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    clearViews();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    nextBtn.setEnabled(true);
                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    clearViews();
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", constants.Old_Shared_Key);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sUserMotherMaidenName", motherMaiden.toUpperCase());
//                        params.put("sEmailAddress", email);
//                        params.put("sLegalIdentityType", identitySelected);
//                        params.put("sLegalIdentityValue", identityAnswer);
//                        params.put("sSigns_Username", userName);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(ForgotPasswordValidation.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                nextBtn.setEnabled(true);
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                clearViews();

            }
        } catch (Exception e) {

            nextBtn.setEnabled(true);
            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");
            clearViews();
        }
    }


    public void clearViews() {

        editName.setText("");
        editEmail.setText("");
        editMotherMaiden.setText("");
        editIdentity.setText("");
        editIdentity.requestFocus();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ForgotPasswordValidation.this, LoginScreen.class);
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSupportActionModeStarted(ActionMode mode) {
        super.onSupportActionModeStarted(mode);
        mode.finish();
    }

}
