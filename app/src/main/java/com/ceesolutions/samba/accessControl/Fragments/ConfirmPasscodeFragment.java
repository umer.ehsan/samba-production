package com.ceesolutions.samba.accessControl.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.core.content.res.ResourcesCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.accessControl.ProfileSetupScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.PinView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 2/21/18.
 */

public class ConfirmPasscodeFragment extends Fragment {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private TextView validateBtn;
    private TextView errorMessage, text2;
    private String token, target, userName, passcode = "", legalID;
    private ImageView backBtn;
    private TextView headingTxt;
    private Utils utils;
    private PinView pinView;
    private AppPreferences appPreferences;
    private String location;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.set_passcode_screen, container, false);

        initViews(convertView);

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                passcode = pinView.getText().toString();

                if (!passcode.isEmpty()) {

                    if (!helper.validateInputForSC(passcode) || passcode.contains(" ")) {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.setText("Passcode cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                        errorMessage.setError("");

                    } else {
                        if (passcode.length() < 1 || passcode.length() < 6) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.setText("Please Enter 6 digits passcode");
                            errorMessage.setError("");

                        } else if (passcode.equals(constants.passCode)) {

                            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                                longitude = Double.valueOf(longitude1);
                                latitude = Double.valueOf(latitude1);
                                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                                } else {
                                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                                }
                            } else {

                                longitude = 0;
                                latitude = 0;
                                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
                            }
                            SetPassCode();


                        } else {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.setText("Passcode and Confirm passcode does not match");
                            errorMessage.setError("");
                        }
                    }
                } else {

                    errorMessage.setVisibility(View.VISIBLE);
                    errorMessage.setText("Please Enter 6 digits passcode");
                    errorMessage.setError("");
                }

            }
        });


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProfileSetupScreen.pager.setCurrentItem(0);
                headingTxt.setVisibility(View.INVISIBLE);
                backBtn.setVisibility(View.INVISIBLE);
                pinView.setText("");
            }
        });

        return convertView;
    }

    public void initViews(View convertView) {

        try {

            CryptLib _crypt = new CryptLib();
            ProfileSetupScreen activity = (ProfileSetupScreen) getActivity();
            backBtn = (ImageView) activity.findViewById(R.id.backBtn);
            headingTxt = (TextView) activity.findViewById(R.id.headingTxt);
            text2 = (TextView) convertView.findViewById(R.id.text2);
            text2.setText("Confirm Passcode");
            validateBtn = (TextView) convertView.findViewById(R.id.validateBtn);
            errorMessage = (TextView) convertView.findViewById(R.id.errorMessage);
            pinView = (PinView) convertView.findViewById(R.id.pinView);
            requestQueue = Volley.newRequestQueue(getActivity());
            webService = new WebService();
            utils = new Utils(getActivity());
            helper = Helper.getHelper(getActivity());
            constants = Constants.getConstants(getActivity());
            token = constants.sharedPreferences.getString("fcmToken", "N/A");
            target = _crypt.decrypt(constants.sharedPreferences.getString("T24_Target", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            userName = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            legalID = _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            pd = new Dialog(getActivity());
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            appPreferences = new AppPreferences(getActivity());
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            pinView.setTextColor(
                    ResourcesCompat.getColor(getResources(), R.color.colorAccent, getActivity().getTheme()));
            pinView.setTextColor(
                    ResourcesCompat.getColorStateList(getResources(), R.color.colorPrimary, getActivity().getTheme()));
            pinView.setLineColor(
                    ResourcesCompat.getColor(getResources(), R.color.colorPrimary, getActivity().getTheme()));
            pinView.setLineColor(
                    ResourcesCompat.getColorStateList(getResources(), R.color.line_colors, getActivity().getTheme()));
            pinView.setItemCount(6);
            pinView.setAnimationEnable(true);// start animation when adding text
            pinView.setCursorVisible(false);

            pinView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    errorMessage.setVisibility(View.GONE);
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    errorMessage.setVisibility(View.GONE);
                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (s.length() == 6) {

                        passcode = s.toString();
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(pinView.getWindowToken(), 0);

                    } else {

                        passcode = s.toString();
                        // Do Nothing
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetPassCode() {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                final String md5 = Helper.md5(userName.toUpperCase() + passcode);
//                webService.SetDevicePasscode(userName, legalID, md5, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getSetPasscodeReqResponse();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Information is not valid!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                pinView.setText("");
//                                validateBtn.setEnabled(true);
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
//
////                                        Toast.makeText(getActivity(), "Passcode has been set Successfully !", Toast.LENGTH_LONG).show();
//
//                                        pd.dismiss();
//                                        ProfileSetupScreen.pager.setCurrentItem(2);
//                                        ProfileSetupScreen.currentItem++;
//                                        headingTxt.setVisibility(View.VISIBLE);
//                                        backBtn.setVisibility(View.INVISIBLE);
//                                        pinView.setText("");
//                                        validateBtn.setEnabled(true);
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Passcode can not be set. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                        pinView.setText("");
//                                        validateBtn.setEnabled(true);
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                                    pinView.setText("");
//                                    validateBtn.setEnabled(true);
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Device cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
//                            pinView.setText("");
//                            validateBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 3000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md51 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", userName);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sLegalIdentityValue", legalID);
                params.put("sPassCodeValue", md5);
                params.put("sServerSessionKey", md51);
                HttpsTrustManager.allowMySSL(getActivity());
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.setDevicePasscodeURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md51)) {
                                        try {

                                            CryptLib _crypt = new CryptLib();
                                            if (jsonObject.getJSONObject("Signs_SetDevicePasscodeResult").getString("Status_Code").equals("00")) {
                                                JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_SetDevicePasscodeResult").getString("Status_Code"));

                                                pd.dismiss();
                                                ProfileSetupScreen.pager.setCurrentItem(2);
                                                ProfileSetupScreen.currentItem++;
                                                headingTxt.setVisibility(View.VISIBLE);
                                                backBtn.setVisibility(View.INVISIBLE);
                                                pinView.setText("");
                                                validateBtn.setEnabled(true);
                                                constants.editor.putString("UserProgress", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_SetDevicePasscodeResult").getString("UserProgress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.editor.commit();
                                                constants.editor.apply();
                                            } else if (jsonObject.getJSONObject("Signs_SetDevicePasscodeResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_SetDevicePasscodeResult").getString("Status_Description"), "ERROR", getActivity(), new LoginScreen());
                                                pinView.setText("");
                                                validateBtn.setEnabled(true);

                                            } else

                                            {
                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_SetDevicePasscodeResult").getString("Status_Description"), "ERROR");
                                                pinView.setText("");
                                                validateBtn.setEnabled(true);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        pinView.setText("");
                                        validateBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    pinView.setText("");
                                    validateBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {

                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    pinView.setText("");
                                    validateBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
                                    pinView.setText("");
                                    validateBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", userName);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLegalIdentityValue", legalID);
//                        params.put("sPassCodeValue", md5);
//                        params.put("sServerSessionKey", md51);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");

                pinView.setText("");
                validateBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Sorry, the provided information is not correct. Please contact SambaPhone Banking at +92-11-11-72622 (SAMBA) for assistance.", "ERROR");
            pinView.setText("");
            validateBtn.setEnabled(true);

        }
    }

}