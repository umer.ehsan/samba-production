package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.NonSambaUser.NonSambaCompletionScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ayaz on 12/21/17.
 */

public class UserConfirmationScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private EditText editUsername, editPassword, editConfrimPassword;
    private RequestQueue requestQueue;
    private WebService webService;
    private TextView validateBtn, errorMessageForConfirmPassword, errorMessageForPassword, errorMessageForUsername, message, doneBtn, heading;
    private Helper helper;
    private String motherName, userName, password, confirmPassword, identityValue, fullName, T24_CIF, mobileNo, email, type, identityType, nicExpire, dob, userImage;
    private Dialog pd;
    private Pattern passwordPattern, passwordMatchpattern, userNameMatcherPattern;
    private Matcher passwordMatcher, userMatcher;
    private Constants constants;
    private Utils utils;
    private ImageView info, info1, check;
    private Dialog dialog;
    private AppPreferences appPreferences;
    private String location;

    private static boolean checkForCapitalLetter(String str) {
        char ch;
        boolean capitalFlag = false;
        boolean lowerCaseFlag = false;
        boolean numberFlag = false;
        for (int i = 0; i < str.length(); i++) {
            ch = str.charAt(i);
            if (Character.isDigit(ch)) {
                numberFlag = true;
            } else if (Character.isUpperCase(ch)) {
                capitalFlag = true;
            } else if (Character.isLowerCase(ch)) {
                lowerCaseFlag = true;
            }
            if (numberFlag && capitalFlag && lowerCaseFlag)
                return true;
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_confirmation);
        initViews();
        getSupportActionBar().hide();

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showInfo();
            }
        });

        info1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showInfo();
            }
        });


        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showInfoUser();
            }
        });

        editUsername.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    userName = editUsername.getText().toString();

                    if (TextUtils.isEmpty(userName)) {

                        check.setVisibility(View.VISIBLE);
                        check.setEnabled(true);
                        check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                        errorMessageForUsername.setVisibility(View.VISIBLE);
                        errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                        errorMessageForUsername.bringToFront();
                        errorMessageForUsername.setError("");
                        errorMessageForUsername.setText("Please enter valid username");

                    } else if (!helper.validateInputForSC(userName) || userName.contains(" ")) {

                        check.setVisibility(View.VISIBLE);
                        check.setEnabled(true);
                        check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                        errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                        errorMessageForUsername.setVisibility(View.VISIBLE);
                        errorMessageForUsername.bringToFront();
                        errorMessageForUsername.setError("");
                        errorMessageForUsername.setText("Username number cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                    } else if (userName.length() < 6 || userName.length() > 20) {

                        check.setVisibility(View.VISIBLE);
                        check.setEnabled(true);
                        check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                        errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                        errorMessageForUsername.setVisibility(View.VISIBLE);
                        errorMessageForUsername.bringToFront();
                        errorMessageForUsername.setError("");
                        errorMessageForUsername.setText("Username must be between 6-20 characters");

                    } else if (!checkUsername(userName)) {

                        check.setVisibility(View.VISIBLE);
                        check.setEnabled(true);
                        check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                        errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                        errorMessageForUsername.setVisibility(View.VISIBLE);
                        errorMessageForUsername.bringToFront();
                        errorMessageForUsername.setError("");
                        errorMessageForUsername.setText("Username can only contain Alphanumeric and _@. characters");

                    } else {

                        userName = editUsername.getText().toString();
                        IsValidUser(userName);

                    }
                    return true;
                }
                return false;
            }
        });
    }

    public void initViews() {

        Intent intent = getIntent();
        type = intent.getStringExtra("Type");


        validateBtn = (TextView) findViewById(R.id.validateBtn);
        errorMessageForConfirmPassword = (TextView) findViewById(R.id.errorMessageForConfirmPassword);
        errorMessageForUsername = (TextView) findViewById(R.id.errorMessageForUsername);
        errorMessageForPassword = (TextView) findViewById(R.id.errorMessageForPassword);
        editUsername = (EditText) findViewById(R.id.editUsername);
        editPassword = (EditText) findViewById(R.id.editPassword);
        editConfrimPassword = (EditText) findViewById(R.id.editConfirmPassword);
        info = (ImageView) findViewById(R.id.info);
        info1 = (ImageView) findViewById(R.id.info1);
        check = (ImageView) findViewById(R.id.check);
        check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
        utils = new Utils(UserConfirmationScreen.this);
        webService = new WebService();
        helper = Helper.getHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(UserConfirmationScreen.this);
        pd = new Dialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        appPreferences = new AppPreferences(UserConfirmationScreen.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

        if (!TextUtils.isEmpty(type) && type.equals("Samba")) {


            identityType = constants.credentialsSharedPreferences.getString("identityType", "N/A");
            identityValue = constants.credentialsSharedPreferences.getString("identityValue", "N/A");
            fullName = intent.getStringExtra("fullName");
            T24_CIF = intent.getStringExtra("T24_CIF");
            mobileNo = intent.getStringExtra("mobileNo");
            email = intent.getStringExtra("email");

        } else {

            fullName = constants.nonSambaCredentials.getString("username", "N/A");
            email = constants.nonSambaCredentials.getString("email", "N/A");
            mobileNo = constants.nonSambaCredentials.getString("mobile", "N/A");
            nicExpire = constants.nonSambaCredentials.getString("nicexpiry", "N/A");
            dob = constants.nonSambaCredentials.getString("dob", "N/A");
            userImage = constants.nonSambaCredentials.getString("image", "N/A");
            motherName = constants.nonSambaCredentials.getString("motherName", "N/A");
            identityType = constants.credentialsSharedPreferences.getString("identityType", "N/A");
            identityValue = constants.credentialsSharedPreferences.getString("identityValue", "N/A");

        }

        userName = editUsername.getText().toString().trim();
        password = editPassword.getText().toString().trim();
        confirmPassword = editConfrimPassword.getText().toString().trim();

        passwordPattern = Pattern.compile(constants.PASSWORD_PATTERN);
        passwordMatchpattern = Pattern.compile(constants.EXCLUDE_PASSWORD_PATTERN);
        userNameMatcherPattern = Pattern.compile(constants.USERNAME_PATTERN);

        editUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessageForUsername.setVisibility(View.GONE);
                check.setVisibility(View.VISIBLE);
                errorMessageForUsername.setError(null);

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessageForUsername.setVisibility(View.GONE);
                check.setVisibility(View.VISIBLE);
                errorMessageForUsername.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                userName = editable.toString().trim();
            }
        });

        editUsername.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {

                    userName = editUsername.getText().toString();

                    if (TextUtils.isEmpty(userName)) {

                        check.setVisibility(View.VISIBLE);
                        check.setEnabled(true);
                        check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                        errorMessageForUsername.setVisibility(View.VISIBLE);
                        errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                        errorMessageForUsername.bringToFront();
                        errorMessageForUsername.setError("");
                        errorMessageForUsername.setText("Please enter valid username");

                    } else if (!helper.validateInputForSC(userName) || userName.contains(" ")) {

                        check.setVisibility(View.VISIBLE);
                        check.setEnabled(true);
                        check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                        errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                        errorMessageForUsername.setVisibility(View.VISIBLE);
                        errorMessageForUsername.bringToFront();
                        errorMessageForUsername.setError("");
                        errorMessageForUsername.setText("Username cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                    } else if (userName.length() < 6 || userName.length() > 20) {

                        check.setVisibility(View.VISIBLE);
                        check.setEnabled(true);
                        check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                        errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                        errorMessageForUsername.setVisibility(View.VISIBLE);
                        errorMessageForUsername.bringToFront();
                        errorMessageForUsername.setError("");
                        errorMessageForUsername.setText("Username must be between 6-20 characters");

                    } else if (!checkUsername(userName)) {

                        check.setVisibility(View.VISIBLE);
                        check.setEnabled(true);
                        check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                        errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                        errorMessageForUsername.setVisibility(View.VISIBLE);
                        errorMessageForUsername.bringToFront();
                        errorMessageForUsername.setError("");
                        errorMessageForUsername.setText("Username can only contain Alphanumeric and _@. characters");

                    } else {

                        userName = editUsername.getText().toString();
                        IsValidUser(userName);

                    }

//                    Toast.makeText(UserConfirmationScreen.this, "Focus Lose", Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(UserConfirmationScreen.this, "Get Focus", Toast.LENGTH_SHORT).show();
                }

            }
        });
        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessageForPassword.setVisibility(View.GONE);
                errorMessageForPassword.setError(null);

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessageForPassword.setVisibility(View.GONE);
                errorMessageForPassword.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                password = editable.toString().trim();
                if (password.length() == 0) {


                    //Do Nothing
                } else {

                    if (passwordMatchpattern.matcher(password).find()) {

                        errorMessageForPassword.setVisibility(View.VISIBLE);
                        errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                        errorMessageForPassword.setText("Password Strength: Weak");


                    } else {

                        if (!checkForCapitalLetter(password)) {

                            errorMessageForPassword.setVisibility(View.VISIBLE);
                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                            errorMessageForPassword.setText("Password Strength: Weak");


                        } else {

                            if (password.length() < 16) {
                                passwordMatcher = passwordPattern.matcher(password);


                                if (password.length() > 7) {

                                    if (passwordMatcher.matches()) {


                                        if (password.length() > 10) {

                                            errorMessageForPassword.setVisibility(View.VISIBLE);
                                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                            errorMessageForPassword.setText("Password Strength: Strong");

                                        } else {

                                            errorMessageForPassword.setVisibility(View.VISIBLE);
                                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                            errorMessageForPassword.setText("Password Strength: Good");

                                        }


                                    } else {

                                        if (password.length() > 7 && password.length() < 10) {

                                            errorMessageForPassword.setVisibility(View.VISIBLE);
                                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                            errorMessageForPassword.setText("Password Strength: Good");

                                        } else {

                                            errorMessageForPassword.setVisibility(View.VISIBLE);
                                            errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                            errorMessageForPassword.setText("Password Strength: Bad");
                                        }

                                    }
                                } else {

                                    if (password.length() > 7 && password.length() < 10) {

                                        errorMessageForPassword.setVisibility(View.VISIBLE);
                                        errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                        errorMessageForPassword.setText("Password Strength: Good");

                                    } else {

                                        errorMessageForPassword.setVisibility(View.VISIBLE);
                                        errorMessageForPassword.setTextColor(getResources().getColor(R.color.passwordgreen));
                                        errorMessageForPassword.setText("Password Strength: Bad");
                                    }
                                }

                            }
                        }

                    }
                }
            }
        });
        editPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {

                    errorMessageForConfirmPassword.setVisibility(View.GONE);
                    editConfrimPassword.getText().clear();

//                    Toast.makeText(UserConfirmationScreen.this, "Focus Lose", Toast.LENGTH_SHORT).show();
                } else {


//                    Toast.makeText(UserConfirmationScreen.this, "Get Focus", Toast.LENGTH_SHORT).show();
                }

            }
        });
        editConfrimPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForConfirmPassword.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForConfirmPassword.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                confirmPassword = editable.toString();
            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                FirebaseAnalytics.getInstance(UserConfirmationScreen.this).logEvent("User_ID_Setup_For_Registration", new Bundle());


                userName = editUsername.getText().toString().trim();
                password = editPassword.getText().toString().trim();
                confirmPassword = editConfrimPassword.getText().toString().trim();

                errorMessageForPassword.setTextColor(getResources().getColor(R.color.tabunderline));

                if (TextUtils.isEmpty(userName)) {
                    errorMessageForUsername.setVisibility(View.VISIBLE);
                    errorMessageForUsername.bringToFront();
                    errorMessageForUsername.setError("");
                    errorMessageForUsername.setText("Please enter valid username");

                } else if (!helper.validateInputForSC(userName) || userName.contains(" ")) {


                    errorMessageForUsername.setVisibility(View.VISIBLE);
                    errorMessageForUsername.bringToFront();
                    errorMessageForUsername.setError("");
                    errorMessageForUsername.setText("Username cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                } else if (userName.length() < 6 || userName.length() > 20) {

                    check.setVisibility(View.VISIBLE);
                    check.setEnabled(true);
                    check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                    errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                    errorMessageForUsername.setVisibility(View.VISIBLE);
                    errorMessageForUsername.bringToFront();
                    errorMessageForUsername.setError("");
                    errorMessageForUsername.setText("Username must be between 6-20 characters");

                } else if (!checkUsername(userName)) {

                    check.setVisibility(View.VISIBLE);
                    check.setEnabled(true);
                    check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                    errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                    errorMessageForUsername.setVisibility(View.VISIBLE);
                    errorMessageForUsername.bringToFront();
                    errorMessageForUsername.setError("");
                    errorMessageForUsername.setText("Username can only contain Alphanumeric and _@. characters");

                } else {

                    userName = editUsername.getText().toString().trim();


                }

                if (TextUtils.isEmpty(password)) {
                    errorMessageForPassword.setVisibility(View.VISIBLE);
                    errorMessageForPassword.bringToFront();
                    errorMessageForPassword.setError("");
                    errorMessageForPassword.setText("Please enter valid password");
                    clearPasswordViews();

                } else if (!helper.validateInputForSC(password) || password.contains(" ")) {

                    errorMessageForPassword.setVisibility(View.VISIBLE);
                    errorMessageForPassword.bringToFront();
                    errorMessageForPassword.setError("");
                    errorMessageForPassword.setText("Password cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                    clearPasswordViews();

                } else if (!checkForCapitalLetter(password)) {

                    errorMessageForPassword.setVisibility(View.VISIBLE);
                    errorMessageForPassword.bringToFront();
                    errorMessageForPassword.setError("");
                    errorMessageForPassword.setText("Please must contains a capital letter");
                    clearPasswordViews();

                }

                if (TextUtils.isEmpty(confirmPassword)) {
                    errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                    errorMessageForConfirmPassword.bringToFront();
                    errorMessageForConfirmPassword.setError("");
                    errorMessageForConfirmPassword.setText("Please enter valid password");
                    clearPasswordViews();

                } else if (!helper.validateInputForSC(confirmPassword) || confirmPassword.contains(" ")) {


                    errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                    errorMessageForConfirmPassword.bringToFront();
                    errorMessageForConfirmPassword.setError("");
                    errorMessageForConfirmPassword.setText("Confirm Password cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                    clearPasswordViews();
                }

                if (confirmPassword.equals(password)) {

                    if ((!TextUtils.isEmpty(password) && password.length() >= 10 && helper.validateInputForSC(password) && !password.contains(" ") && checkForCapitalLetter(password)) && (!TextUtils.isEmpty(confirmPassword) && confirmPassword.length() >= 10 && helper.validateInputForSC(confirmPassword) && !confirmPassword.contains(" ")) && (!TextUtils.isEmpty(userName) && helper.validateInputForSC(userName) && !userName.contains(" "))) {

                        if (passwordMatchpattern.matcher(editPassword.getText().toString()).find()) {
                            clearPasswordViews();
                            errorMessageForPassword.setVisibility(View.VISIBLE);
                            errorMessageForPassword.bringToFront();
                            errorMessageForPassword.setError("");
                            errorMessageForPassword.setText("Password <,>,\",',%,(,),&,+,\\,~");
                            errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                            errorMessageForConfirmPassword.bringToFront();
                            errorMessageForConfirmPassword.setError("");
                            errorMessageForConfirmPassword.setText("Confirm Password <,>,\",',%,(,),&,+,\\,~");


                        } else {

                            passwordMatcher = passwordPattern.matcher(password);

                            if (passwordMatcher.matches()) {

                                editPassword.setText(editPassword.getText().toString());
                                if (type.equals("Samba")) {

                                    if (userName.equals(editPassword.getText().toString())) {
                                        utils.showDilogForError("Username and Password cannot be the same.", "ERROR");
                                    } else {
                                        validateBtn.setEnabled(false);
                                        UserConfirmation();
                                    }

                                } else {

                                    if (userName.equals(editPassword.getText().toString())) {

                                        utils.showDilogForError("Username and Password cannot be the same.", "ERROR");
                                    } else {
                                        validateBtn.setEnabled(false);
                                        UserConfirmationForNonSamba();
                                    }


                                }
                            } else {
                                clearPasswordViews();
                                errorMessageForPassword.setVisibility(View.VISIBLE);
                                errorMessageForPassword.bringToFront();
                                errorMessageForPassword.setError("");
                                errorMessageForPassword.setText("Password policy does not match");
                                errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                                errorMessageForConfirmPassword.bringToFront();
                                errorMessageForConfirmPassword.setError("");
                                errorMessageForConfirmPassword.setText("Password policy does not match");
//                                clearPasswordViews();
                            }

                        }

                    } else {
                        clearPasswordViews();
                        errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                        errorMessageForConfirmPassword.bringToFront();
                        errorMessageForConfirmPassword.setError("");
                        errorMessageForConfirmPassword.setText("Please enter valid password");
                        errorMessageForPassword.setVisibility(View.VISIBLE);
                        errorMessageForPassword.bringToFront();
                        errorMessageForPassword.setError("");
                        errorMessageForPassword.setText("Please enter valid password");
//                        clearPasswordViews();
                    }


                } else {


                    if (TextUtils.isEmpty(password) || password.length() < 8) {
                        clearPasswordViews();
                        errorMessageForPassword.setVisibility(View.VISIBLE);
                        errorMessageForPassword.bringToFront();
                        errorMessageForPassword.setError("");
                        errorMessageForPassword.setText("Please enter valid password");
//                        clearPasswordViews();
                    }

                    if (TextUtils.isEmpty(confirmPassword) || confirmPassword.length() < 8) {
                        clearPasswordViews();
                        errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                        errorMessageForConfirmPassword.bringToFront();
                        errorMessageForConfirmPassword.setError("");
                        errorMessageForConfirmPassword.setText("Please enter valid password");
//                        clearPasswordViews();
                    }

                    if (!TextUtils.isEmpty(password) && password.length() >= 8 && !TextUtils.isEmpty(confirmPassword) && confirmPassword.length() >= 8) {
                        clearPasswordViews();
                        errorMessageForPassword.setVisibility(View.VISIBLE);
                        errorMessageForPassword.bringToFront();
                        errorMessageForPassword.setError("");
                        errorMessageForPassword.setText("Password/Confirm Password does not match");
                        errorMessageForConfirmPassword.setVisibility(View.VISIBLE);
                        errorMessageForConfirmPassword.bringToFront();
                        errorMessageForConfirmPassword.setError("");
                        errorMessageForConfirmPassword.setText("Password/Confirm Password does not match");
//                        clearPasswordViews();
                    }


                }

            }
        });


    }

    public void UserConfirmation() {

        validateBtn.setEnabled(false);

        if (password.equals(confirmPassword)) {

            if (userName.length() > 6 && userName.length() < 20) {


                try {

                    if (helper.isNetworkAvailable()) {

                        pd.show();
                        final String md5 = helper.md5(userName.toUpperCase() + password);


                        final String md51 = helper.md5(userName.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("sService_Username", constants.signsUserName);
                        params.put("sService_Password", constants.signsPassword);
                        params.put("sIPAddress", Utils.getIPAddress(true));
                        params.put("sMacAddress", constants.address);
                        params.put("sSigns_Username", userName);
                        params.put("sSigns_Password", md5);
                        params.put("sOS_Version", constants.version);
                        params.put("sDeviceName", constants.model);
                        params.put("sAppVersion", constants.appVersion);
                        params.put("sService_SharedKey", constants.Old_Shared_Key);
                        params.put("sTimeStamp", constants.dateTime);
                        params.put("sT24_LegalIdentityValue", identityValue);
                        params.put("sT24_LegalIdentityType", identityType);
                        params.put("sT24_FullName", fullName);
                        params.put("sT24_CIF", T24_CIF);
                        params.put("sT24_EmailAddress", email);
                        params.put("sT24_MobileNumber", mobileNo);
                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                        params.put("sDeviceLongitude", String.valueOf(longitude));
                        params.put("sServerSessionKey", md51);
                        HttpsTrustManager.allowMySSL(UserConfirmationScreen.this);
                        CryptLib _crypt = new CryptLib();
                        final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                        StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.userlogincreateURL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        // response

                                        Log.d("Response---", response);

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            JSONObject object = new JSONObject();
                                            if (jsonObject.getString("ServerKey").equals(md51)) {
                                                if (jsonObject.getJSONObject("Signs_RegisterUserResult").getString("Status_Code").equals("00")) {
                                                    Intent intent = new Intent(UserConfirmationScreen.this, AccountCreatedScreen.class);
                                                    pd.dismiss();
                                                    constants.credentialsEditor.clear();
                                                    constants.credentialsEditor.commit();
                                                    constants.credentialsEditor.apply();
                                                    finish();
                                                    startActivity(intent);
                                                    overridePendingTransition(0, 0);
                                                    validateBtn.setEnabled(true);
                                                    clearViews();
                                                } else if (jsonObject.getJSONObject("Signs_RegisterUserResult").getString("Status_Code").equals("13")) {
                                                    validateBtn.setEnabled(true);
                                                    pd.dismiss();
                                                    utils.showDilogForError("Username not available. Please choose another username.", "ERROR");
                                                    clearViews();
                                                } else {
                                                    validateBtn.setEnabled(true);
                                                    pd.dismiss();
                                                    utils.showDilogForError(jsonObject.getJSONObject("Signs_RegisterUserResult").getString("Status_Description"), "ERROR");
                                                    clearViews();
                                                }
                                            } else {

                                                validateBtn.setEnabled(true);
                                                pd.dismiss();
                                                utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                                clearViews();
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            validateBtn.setEnabled(true);
                                            pd.dismiss();
                                            utils.showDilogForError("Username not available. Please choose another username.", "ERROR");
                                            clearViews();
                                        }

                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        // error
                                        Log.d("Error.Response", "---" + error.getMessage());
                                        try {
                                            validateBtn.setEnabled(true);
                                            pd.dismiss();
                                            utils.showDilogForError("Username not available. Please choose another username.", "ERROR");
                                            clearViews();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            validateBtn.setEnabled(true);
                                            pd.dismiss();
                                            utils.showDilogForError("Username not available. Please choose another username.", "ERROR");
                                            clearViews();
                                        }
                                    }
                                })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", userName);
//                            params.put("sSigns_Password", md5);
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", constants.Old_Shared_Key);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sT24_LegalIdentityValue", identityValue);
//                            params.put("sT24_LegalIdentityType", identityType);
//                            params.put("sT24_FullName", fullName);
//                            params.put("sT24_CIF", T24_CIF);
//                            params.put("sT24_EmailAddress", email);
//                            params.put("sT24_MobileNumber", mobileNo);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md51);
//                            return params;
//                        }
//                    };
                        {

                            @Override
                            public byte[] getBody() throws AuthFailureError {
                                try {
                                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                                } catch (UnsupportedEncodingException uee) {
                                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                    return null;
                                }
                            }
                        };

                        postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                                0,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                        postRequest.setShouldCache(false);
                        requestQueue.add(postRequest);
                        requestQueue.getCache().clear();
                    } else {
//                    Toast.makeText(UserConfirmationScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                        validateBtn.setEnabled(true);
                        pd.dismiss();
                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                        clearViews();

                    }
                } catch (Exception e) {

//                Toast.makeText(UserConfirmationScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
                    validateBtn.setEnabled(true);
                    pd.dismiss();
                    utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                    clearViews();

                }
            } else {
                validateBtn.setEnabled(true);
                pd.dismiss();
                utils.showDilogForError("Please enter valid username.", "WARNING");
                clearViews();
            }
        } else {

//            Toast.makeText(UserConfirmationScreen.this, "Password and Confirm Password does not match !", Toast.LENGTH_LONG).show();
            validateBtn.setEnabled(true);
            pd.dismiss();
            utils.showDilogForError("Password and Confirm Password does not match.", "WARNING");
            clearViews();
        }
    }

    public void UserConfirmationForNonSamba() {

        validateBtn.setEnabled(false);

        if (password.equals(confirmPassword)) {

            if (userName.length() > 6 && userName.length() < 20) {
                try {

                    if (helper.isNetworkAvailable()) {

                        pd.show();
                        final String md5 = helper.md5(userName.toUpperCase() + password);
//                    webService.UserConfirmationForNonSamba(userName, md5, identityValue, identityType, userName, nicExpire, email, mobileNo, userImage, dob, motherName, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                    Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        public void run() {
//
//                            JSONObject jsonObject = webService.getValConfirmReqNonSambaResponse();
//                            Log.d("Response", "Object----" + jsonObject);
//
//                            if (jsonObject != null) {
//
//                                if (jsonObject.has("Error")) {
//
////                                    Toast.makeText(UserConfirmationScreen.this, "Username/Password is not valid!", Toast.LENGTH_LONG).show();
//                                    validateBtn.setEnabled(true);
//                                    pd.dismiss();
//                                    utils.showDilogForError("Username/Password is not valid.", "ERROR");
//                                    clearViews();
//
//                                } else {
//
//                                    try {
//
//                                        if (jsonObject.getString("Status_Code").equals("00")) {
//
//                                            pd.dismiss();
//                                            constants.nonSambaCredentialsEditor.clear();
//                                            constants.nonSambaCredentialsEditor.commit();
//                                            constants.nonSambaCredentialsEditor.apply();
//                                            constants.credentialsEditor.clear();
//                                            constants.credentialsEditor.commit();
//                                            constants.credentialsEditor.apply();
//                                            Intent intent = new Intent(UserConfirmationScreen.this, NonSambaCompletionScreen.class);
//                                            startActivity(intent);
//                                            finish();
//                                            validateBtn.setEnabled(true);
//                                            clearViews();
//                                        } else if (jsonObject.getString("Status_Code").equals("13")) {
//
//                                            validateBtn.setEnabled(true);
//                                            pd.dismiss();
//                                            utils.showDilogForError("Username not available. Please choose another username.", "ERROR");
//                                            clearViews();
//                                        } else {
//
//                                            validateBtn.setEnabled(true);
//                                            pd.dismiss();
//                                            utils.showDilogForError("Username not available. Please choose another username.", "ERROR");
//                                            clearViews();
//
//                                        }
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                        validateBtn.setEnabled(true);
//                                        pd.dismiss();
//                                        utils.showDilogForError("Username not available. Please choose another username.", "ERROR");
//                                        clearViews();
//                                    }
////                                    Toast.makeText(UserConfirmationScreen.this, "User Created Successfully.", Toast.LENGTH_LONG).show();
//
//
//                                }
//                            } else {
////                                Toast.makeText(UserConfirmationScreen.this, "Username/Password is not valid!", Toast.LENGTH_LONG).show();
//                                validateBtn.setEnabled(true);
//                                pd.dismiss();
//                                utils.showDilogForError("Username/Password is not valid.", "WARNING");
//                                clearViews();
//
//                            }
//                        }
//                    }, 8000);
                        final String md51 = helper.md5(userName.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("sService_Username", constants.signsUserName);
                        params.put("sService_Password", constants.signsPassword);
                        params.put("sIPAddress", Utils.getIPAddress(true));
                        params.put("sMacAddress", constants.address);
                        params.put("sSigns_Username", userName);
                        params.put("sSigns_Password", md5);
                        params.put("sOS_Version", constants.version);
                        params.put("sDeviceName", constants.model);
                        params.put("sAppVersion", constants.appVersion);
                        params.put("sService_SharedKey", constants.Old_Shared_Key);
                        params.put("sTimeStamp", constants.dateTime);
                        params.put("sT24_EmailAddress", email);
                        params.put("sT24_MobileNumber", mobileNo);
                        params.put("sLegalId_Capture", userImage);
                        params.put("sLegalIdExpiry", nicExpire);
                        params.put("sFullName", fullName);
                        params.put("sT24_LegalIdentityValue", identityValue);
                        params.put("sT24_LegalIdentityType", identityType);
                        params.put("sDateOfBirth", dob);
                        params.put("sMotherMaidenName", motherName);
                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                        params.put("sDeviceLongitude", String.valueOf(longitude));
                        params.put("sServerSessionKey", md51);
                        HttpsTrustManager.allowMySSL(UserConfirmationScreen.this);
                        CryptLib _crypt = new CryptLib();
                        final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                        StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.userinformationnonsambavalidationURL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        // response

                                        Log.d("Response---", response);

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            JSONObject object = new JSONObject();
                                            if (jsonObject.getString("ServerKey").equals(md51)) {
                                                if (jsonObject.getJSONObject("Signs_RegisterNonSambaUserResult").getString("Status_Code").equals("00")) {
                                                    pd.dismiss();
                                                    constants.nonSambaCredentialsEditor.clear();
                                                    constants.nonSambaCredentialsEditor.commit();
                                                    constants.nonSambaCredentialsEditor.apply();
                                                    constants.credentialsEditor.clear();
                                                    constants.credentialsEditor.commit();
                                                    constants.credentialsEditor.apply();
                                                    Intent intent = new Intent(UserConfirmationScreen.this, NonSambaCompletionScreen.class);
                                                    startActivity(intent);
                                                    finish();
                                                    validateBtn.setEnabled(true);
                                                    clearViews();
                                                } else if (jsonObject.getJSONObject("Signs_RegisterNonSambaUserResult").getString("Status_Code").equals("13")) {
                                                    validateBtn.setEnabled(true);
                                                    pd.dismiss();
                                                    utils.showDilogForError(jsonObject.getJSONObject("Signs_RegisterNonSambaUserResult").getString("Status_Description"), "ERROR");
                                                    clearViews();

                                                } else {
                                                    validateBtn.setEnabled(true);
                                                    pd.dismiss();
                                                    utils.showDilogForError(jsonObject.getJSONObject("Signs_RegisterNonSambaUserResult").getString("Status_Description"), "WARNING");
                                                    clearViews();

                                                }
                                            } else {

                                                validateBtn.setEnabled(true);
                                                pd.dismiss();
                                                utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                                clearViews();

                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            validateBtn.setEnabled(true);
                                            pd.dismiss();
                                            utils.showDilogForError("Username/Password is not valid.", "WARNING");
                                            clearViews();
                                        }

                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        // error
                                        Log.d("Error.Response", "---" + error.getMessage());
                                        try {
                                            validateBtn.setEnabled(true);
                                            pd.dismiss();
                                            utils.showDilogForError("Username/Password is not valid.", "WARNING");
                                            clearViews();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            validateBtn.setEnabled(true);
                                            pd.dismiss();
                                            utils.showDilogForError("Username/Password is not valid.", "WARNING");
                                            clearViews();
                                        }
                                    }
                                })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", userName);
//                            params.put("sSigns_Password", md5);
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", constants.Old_Shared_Key);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sT24_EmailAddress", email);
//                            params.put("sT24_MobileNumber", mobileNo);
//                            params.put("sLegalId_Capture", userImage);
//                            params.put("sLegalIdExpiry", nicExpire);
//                            params.put("sFullName", fullName);
//                            params.put("sT24_LegalIdentityValue", identityValue);
//                            params.put("sT24_LegalIdentityType", identityType);
//                            params.put("sDateOfBirth", dob);
//                            params.put("sMotherMaidenName", motherName);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md51);
//                            return params;
//                        }
//                    };
                        {

                            @Override
                            public byte[] getBody() throws AuthFailureError {
                                try {
                                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                                } catch (UnsupportedEncodingException uee) {
                                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                    return null;
                                }
                            }
                        };

                        postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                                0,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                        postRequest.setShouldCache(false);
                        requestQueue.add(postRequest);
                        requestQueue.getCache().clear();
                    } else {
//                    Toast.makeText(UserConfirmationScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                        validateBtn.setEnabled(true);
                        pd.dismiss();
                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                        clearViews();

                    }
                } catch (Exception e) {

//                Toast.makeText(UserConfirmationScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
                    validateBtn.setEnabled(true);
                    pd.dismiss();
                    utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                    clearViews();

                }
            } else {
                validateBtn.setEnabled(true);
                pd.dismiss();
                utils.showDilogForError("Please enter valid username.", "WARNING");
                clearViews();
            }
        } else {

//            Toast.makeText(UserConfirmationScreen.this, "Password and Confirm Password does not match !", Toast.LENGTH_LONG).show();
            validateBtn.setEnabled(true);
            pd.dismiss();
            utils.showDilogForError("Password and Confirm Password does not match.", "WARNING");
            clearViews();
        }
    }

    public void clearViews() {

        editConfrimPassword.setText("");
        editPassword.setText("");
        editUsername.setText("");

        editUsername.requestFocus();
        check.setVisibility(View.VISIBLE);
        check.setEnabled(true);
        check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
    }

    public void clearPasswordViews() {


        editPassword.getText().clear();
        editUsername.getText().clear();

        editPassword.requestFocus();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UserConfirmationScreen.this, LoginScreen.class);
        constants.credentialsEditor.clear().commit();
        constants.credentialsEditor.apply();
        constants.nonSambaCredentialsEditor.clear().commit();
        constants.nonSambaCredentialsEditor.apply();
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
    }

    public void showInfo() {

        dialog = new Dialog(UserConfirmationScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.password_policy);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        heading.setText("PASSWORD POLICY");
        message.setText("- Password must contain atleast one numeric character." +
                "\n - Password must contain atleast one special character.\n - Password should not contain special characters ( <>%()&+\\\\* ). \n - Password must contain a capital letter.\n - Maximium length of password must be less then 16 characters");
        heading.setBackgroundColor(getResources().getColor(R.color.grey));
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void showInfoUser() {

        dialog = new Dialog(UserConfirmationScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.password_policy);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        heading.setText("USERNAME POLICY");
        message.setText("- The length of User ID must be from 5 characters to 20 characters. \n - Discretion of choosing any character combination i.e. numeric, alpha must exist for users but inclusion of all type of characters in user id should not be mandatory. \n - Only the following special characters can be used: _ @. (underscore, at the rate, full stop). \n - The User ID is not case sensitive - ABC001 is the same User ID as abc001.  \n - It must be unique for all users.");
        heading.setBackgroundColor(getResources().getColor(R.color.grey));
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void IsValidUser(final String UserName) {


        try {
            if (helper.isNetworkAvailable()) {

                final String md51 = helper.md5(UserName.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", UserName);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sT24_LegalIdentityValue", identityValue);
                params.put("sT24_LegalIdentityType", identityType);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md51);
                HttpsTrustManager.allowMySSL(UserConfirmationScreen.this);
                CryptLib _crypt = new CryptLib();
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.userVerificationURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("ServerKey").equals(md51)) {
                                        if (jsonObject.getJSONObject("Signs_UserExistResult").getString("Status_Code").equals("00")) {

                                            check.setVisibility(View.VISIBLE);
                                            check.setEnabled(false);
                                            check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.check_grey));
                                            errorMessageForUsername.setVisibility(View.VISIBLE);
                                            errorMessageForUsername.setTextColor(getResources().getColor(R.color.passwordgreen));
                                            errorMessageForUsername.setText("Username is valid");
                                            errorMessageForUsername.setError(null);
                                            pd.dismiss();
                                            editPassword.requestFocus();

                                        } else if (jsonObject.getJSONObject("Signs_UserExistResult").getString("Status_Code").equals("13")) {

                                            errorMessageForUsername.setVisibility(View.VISIBLE);
                                            check.setVisibility(View.VISIBLE);
                                            check.setEnabled(true);
                                            check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                                            errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                                            errorMessageForUsername.bringToFront();
                                            errorMessageForUsername.setError("");
                                            errorMessageForUsername.setText("Username already register or exist");
                                            pd.dismiss();
                                        } else {
                                            errorMessageForUsername.setVisibility(View.VISIBLE);
                                            check.setVisibility(View.VISIBLE);
                                            check.setEnabled(true);
                                            check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                                            errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                                            errorMessageForUsername.bringToFront();
                                            errorMessageForUsername.setError("");
                                            errorMessageForUsername.setText("Username already register or exist");
                                            pd.dismiss();
                                        }

                                    } else {

//                                        errorMessageForUsername.setVisibility(View.VISIBLE);
                                        check.setVisibility(View.VISIBLE);
                                        check.setEnabled(true);
                                        check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
//                                        errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
//                                        errorMessageForUsername.bringToFront();
//                                        errorMessageForUsername.setError("");
//                                        errorMessageForUsername.setText("Username already register or exist");
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    errorMessageForUsername.setVisibility(View.VISIBLE);
                                    check.setVisibility(View.VISIBLE);
                                    check.setEnabled(true);
                                    check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                                    errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                                    errorMessageForUsername.bringToFront();
                                    errorMessageForUsername.setError("");
                                    errorMessageForUsername.setText("Username already register or exist");
                                    pd.dismiss();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    errorMessageForUsername.setVisibility(View.VISIBLE);
                                    check.setVisibility(View.VISIBLE);
                                    check.setEnabled(true);
                                    check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
                                    errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
                                    errorMessageForUsername.bringToFront();
                                    errorMessageForUsername.setError("");
                                    errorMessageForUsername.setText("Username already register or exist");
                                    pd.dismiss();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", UserName);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sT24_LegalIdentityValue", identityValue);
//                        params.put("sT24_LegalIdentityType", identityType);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md51);
//                        return params;
//
//
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                    Toast.makeText(UserConfirmationScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                validateBtn.setEnabled(true);
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                clearViews();

            }
        } catch (Exception e) {
            errorMessageForUsername.setVisibility(View.VISIBLE);
            check.setVisibility(View.VISIBLE);
            check.setEnabled(true);
            check.setBackground(ContextCompat.getDrawable(UserConfirmationScreen.this, R.drawable.infogrey));
            errorMessageForUsername.setTextColor(getResources().getColor(R.color.tabunderline));
            errorMessageForUsername.bringToFront();
            errorMessageForUsername.setError("");
            errorMessageForUsername.setText("Service is currently unavailable. Please try again later.");
            pd.dismiss();
        }
    }

    @Override
    public void onSupportActionModeStarted(ActionMode mode) {
        super.onSupportActionModeStarted(mode);
        mode.finish();
    }

    private boolean checkUsername(String input) {
        boolean isValid = true;
        //let specialCharacters = ["<",">","\"","\'","%","(",")","&","+","\\","~"]
        String[] specialCharacters = {"!", "\"", "#", "$", "%", "&", "\'", "(", ")", "*", "+", ",", "-", "/", ":", ";", "<", "=", ">", "?", "[", "\\", "]", "^", "`", "{", "|", "}", "~"};
        for (int i = 0; i < specialCharacters.length; i++) {
            if (input.contains(specialCharacters[i])) {
                isValid = false;
            }
        }
        return isValid;
    }
}
