package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.Adapters.ProfileSetupFragmentAdapter;
import com.ceesolutions.samba.utils.CircleIndicator;
import com.ceesolutions.samba.utils.CustomViewPager;
import com.ceesolutions.samba.utils.Utils;


/**
 * Created by ceeayaz on 2/20/18.
 */

public class ProfileSetupScreen extends AppCompatActivity {

    public static CustomViewPager pager;
    public ImageView backBtn;
    public TextView headingTxt,message,doneBtn,heading;
    public static int currentItem = 0;
    private Utils utils;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_setup_screen);
        getSupportActionBar().hide();

        final CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);

        utils = new Utils(ProfileSetupScreen.this);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        backBtn.setVisibility(View.INVISIBLE);
        headingTxt = (TextView) findViewById(R.id.headingTxt);
        headingTxt.setVisibility(View.INVISIBLE);
        pager = (CustomViewPager) findViewById(R.id.pager);
        FragmentManager fm = getSupportFragmentManager();
        ProfileSetupFragmentAdapter pagerAdapter = new ProfileSetupFragmentAdapter(fm, 4);
        // Here you would declare which page to visit on creation
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(0);
        indicator.setViewPager(pager);

        pager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                pager.setCurrentItem(pager.getCurrentItem());
                return true;
            }
        });


        headingTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentItem++;
                pager.setCurrentItem(currentItem);
                indicator.setViewPager(pager);
                if (currentItem == 1 || currentItem == 2) {

                    headingTxt.setVisibility(View.INVISIBLE);
                } else {

                    headingTxt.setVisibility(View.VISIBLE);
//                    headingTxt.setText("SKIP");
                    if (currentItem == 3 && currentItem == 4) {
                        headingTxt.setText("DONE");

                    } else
                        headingTxt.setText("SKIP");

                    if (currentItem > 3) {

                        Intent intent = new Intent(ProfileSetupScreen.this,HomeScreen.class);
                        intent.putExtra("isFirst",getIntent().getStringExtra("isFirst"));
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        finish();

                    }
                }

            }
        });

        pager.disableScroll(true);
//        backBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                pager.setCurrentItem(1);
//                indicator.setViewPager(pager);
//                backBtn.setVisibility(View.INVISIBLE);
//            }
//        });
    }

    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(ProfileSetupScreen.this,LoginScreen.class);
//        overridePendingTransition(0,0);
//        startActivity(intent);
//        finish();
        //Do Nothing
    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(ProfileSetupScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                headingTxt.setVisibility(View.INVISIBLE);
                Intent intent = new Intent(ProfileSetupScreen.this, HomeScreen.class);
                ProfileSetupScreen.currentItem = 0;
                startActivity(intent);
                finish();
            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    @Override
    public void onSupportActionModeStarted(ActionMode mode) {
        super.onSupportActionModeStarted(mode);
        mode.finish();
    }

}
