package com.ceesolutions.samba.accessControl;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ceesolutions.samba.R;
import com.google.firebase.analytics.FirebaseAnalytics;

import net.cachapa.expandablelayout.ExpandableLayout;

/**
 * Created by ayaz on 2/1/18.
 */

public class FAQsScreen extends AppCompatActivity {

    private ExpandableLayout expandableLayout0;
    private ExpandableLayout expandableLayout1;
    private ExpandableLayout expandableLayout2;
    private ExpandableLayout expandableLayout3;
    private ExpandableLayout expandableLayout4;
    private ExpandableLayout expandableLayout5;
    private ExpandableLayout expandableLayout6;
    private ExpandableLayout expandableLayout7;
    private ExpandableLayout expandableLayout8;
    private ExpandableLayout expandableLayout9;
    private ExpandableLayout expandableLayout10;
    private ExpandableLayout expandableLayout11;
    private ExpandableLayout expandableLayout12;
    private ExpandableLayout expandableLayout13;
    private ExpandableLayout expandableLayout14;
    private ExpandableLayout expandableLayout15;
    private ExpandableLayout expandableLayout16;
    private ExpandableLayout expandableLayout17;
    private ExpandableLayout expandableLayout18;
    private ExpandableLayout expandableLayout19;
    private ExpandableLayout expandableLayout20;
    private ExpandableLayout expandableLayout21;
    private ExpandableLayout expandableLayout22;
    private ExpandableLayout expandableLayout23;
    private ExpandableLayout expandableLayout24;
    private ExpandableLayout expandableLayout25;
    private ExpandableLayout expandableLayout26;
    private ExpandableLayout expandableLayout27;
    private ExpandableLayout expandableLayout28;
    private ExpandableLayout expandableLayout29;
    private ExpandableLayout expandableLayout30;
    private ExpandableLayout expandableLayout31;


    private RelativeLayout firstRow, secondRow, thirdRow, fourthRow, fifthRow, sixthRow, seventhRow, eigthRow, ninthRow, tenRow, elevenRow, twelveRow, thirteenRow, fourteenRow, fifteenRow, sixteenRow, seventeenRow, eighteenRow, nineteenRow, twentyRow, twenty1Row, twenty2Row, twenty3Row, twenty4Row, twenty5Row, twenty6Row, twenty7Row, twenty8Row, twenty9Row, thirtyRow, thirty1Row,thirty2Row;
    private ImageView backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faqs);
        initViews();
        getSupportActionBar().hide();

    }

    public void initViews() {

        // new log
        FirebaseAnalytics.getInstance(FAQsScreen.this).logEvent("FAQs_Pressed", new Bundle());


        firstRow = (RelativeLayout) findViewById(R.id.firstRow);
        firstRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.expand();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();

            }
        });
        secondRow = (RelativeLayout) findViewById(R.id.secondRow);
        secondRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.expand();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        thirdRow = (RelativeLayout) findViewById(R.id.thirdRow);
        thirdRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.expand();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        fourthRow = (RelativeLayout) findViewById(R.id.fourthRow);
        fourthRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.expand();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        fifthRow = (RelativeLayout) findViewById(R.id.fifthRow);
        fifthRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.expand();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        sixthRow = (RelativeLayout) findViewById(R.id.sixthRow);
        sixthRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.expand();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        seventhRow = (RelativeLayout) findViewById(R.id.seventhRow);
        seventhRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.expand();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        eigthRow = (RelativeLayout) findViewById(R.id.eigththRow);
        eigthRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.expand();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        ninthRow = (RelativeLayout) findViewById(R.id.ninthRow);
        ninthRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.expand();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        tenRow = (RelativeLayout) findViewById(R.id.tenthRow);
        tenRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.expand();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        elevenRow = (RelativeLayout) findViewById(R.id.elventhRow);
        elevenRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.expand();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        twelveRow = (RelativeLayout) findViewById(R.id.twelvethRow);
        twelveRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.expand();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        thirteenRow = (RelativeLayout) findViewById(R.id.thirteenRow);
        thirteenRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.expand();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        fourteenRow = (RelativeLayout) findViewById(R.id.fourteenRow);
        fourteenRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.expand();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        fifteenRow = (RelativeLayout) findViewById(R.id.fifteenRow);
        fifteenRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.expand();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        sixteenRow = (RelativeLayout) findViewById(R.id.sixteenRow);
        sixteenRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.expand();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        seventeenRow = (RelativeLayout) findViewById(R.id.seventeenRow);
        seventeenRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.expand();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.expand();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        eighteenRow = (RelativeLayout) findViewById(R.id.eightteenRow);
        eighteenRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.expand();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        nineteenRow = (RelativeLayout) findViewById(R.id.nineteenRow);
        nineteenRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.expand();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        twentyRow = (RelativeLayout) findViewById(R.id.twentyRow);
        twentyRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.expand();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        twenty1Row = (RelativeLayout) findViewById(R.id.twentyoneRow);
        twenty1Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.expand();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        twenty2Row = (RelativeLayout) findViewById(R.id.twentytwoRow);
        twenty2Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.expand();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        twenty3Row = (RelativeLayout) findViewById(R.id.twentythreeRow);
        twenty3Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.expand();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        twenty4Row = (RelativeLayout) findViewById(R.id.twentyfourRow);
        twenty4Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.expand();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        twenty5Row = (RelativeLayout) findViewById(R.id.twentyfiveRow);
        twenty5Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.expand();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        twenty6Row = (RelativeLayout) findViewById(R.id.twentysixRow);
        twenty6Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.expand();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        twenty7Row = (RelativeLayout) findViewById(R.id.twentysevenRow);
        twenty7Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.expand();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        twenty8Row = (RelativeLayout) findViewById(R.id.twentyeightRow);
        twenty8Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.expand();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        twenty9Row = (RelativeLayout) findViewById(R.id.twentynineRow);
        twenty9Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.expand();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });
        thirtyRow = (RelativeLayout) findViewById(R.id.thirtyRow);
        thirtyRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.expand();
                expandableLayout30.collapse();
                expandableLayout31.collapse();
            }
        });

        thirty1Row = (RelativeLayout) findViewById(R.id.thirtyoneRow);
        thirty1Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.expand();
                expandableLayout31.collapse();
            }
        });


        thirty2Row = (RelativeLayout) findViewById(R.id.thirtytwoRow);
        thirty2Row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
                expandableLayout5.collapse();
                expandableLayout6.collapse();
                expandableLayout7.collapse();
                expandableLayout8.collapse();
                expandableLayout9.collapse();
                expandableLayout10.collapse();
                expandableLayout11.collapse();
                expandableLayout12.collapse();
                expandableLayout13.collapse();
                expandableLayout14.collapse();
                expandableLayout15.collapse();
                expandableLayout16.collapse();
                expandableLayout17.collapse();
                expandableLayout18.collapse();
                expandableLayout19.collapse();
                expandableLayout20.collapse();
                expandableLayout21.collapse();
                expandableLayout22.collapse();
                expandableLayout23.collapse();
                expandableLayout24.collapse();
                expandableLayout25.collapse();
                expandableLayout26.collapse();
                expandableLayout27.collapse();
                expandableLayout28.collapse();
                expandableLayout29.collapse();
                expandableLayout30.collapse();
                expandableLayout31.expand();
            }
        });


        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        expandableLayout1 = (ExpandableLayout) findViewById(R.id.expandable_layout_1);
        expandableLayout2 = (ExpandableLayout) findViewById(R.id.expandable_layout_2);
        expandableLayout3 = (ExpandableLayout) findViewById(R.id.expandable_layout_3);
        expandableLayout4 = (ExpandableLayout) findViewById(R.id.expandable_layout_4);
        expandableLayout5 = (ExpandableLayout) findViewById(R.id.expandable_layout_5);
        expandableLayout6 = (ExpandableLayout) findViewById(R.id.expandable_layout_6);
        expandableLayout7 = (ExpandableLayout) findViewById(R.id.expandable_layout_7);
        expandableLayout8 = (ExpandableLayout) findViewById(R.id.expandable_layout_8);
        expandableLayout9 = (ExpandableLayout) findViewById(R.id.expandable_layout_9);
        expandableLayout10 = (ExpandableLayout) findViewById(R.id.expandable_layout_10);
        expandableLayout11 = (ExpandableLayout) findViewById(R.id.expandable_layout_11);
        expandableLayout12 = (ExpandableLayout) findViewById(R.id.expandable_layout_12);
        expandableLayout13 = (ExpandableLayout) findViewById(R.id.expandable_layout_13);
        expandableLayout14 = (ExpandableLayout) findViewById(R.id.expandable_layout_14);
        expandableLayout15 = (ExpandableLayout) findViewById(R.id.expandable_layout_15);
        expandableLayout16 = (ExpandableLayout) findViewById(R.id.expandable_layout_16);
        expandableLayout17 = (ExpandableLayout) findViewById(R.id.expandable_layout_17);
        expandableLayout18 = (ExpandableLayout) findViewById(R.id.expandable_layout_18);
        expandableLayout19 = (ExpandableLayout) findViewById(R.id.expandable_layout_19);
        expandableLayout20 = (ExpandableLayout) findViewById(R.id.expandable_layout_20);
        expandableLayout21 = (ExpandableLayout) findViewById(R.id.expandable_layout_21);
        expandableLayout22 = (ExpandableLayout) findViewById(R.id.expandable_layout_22);
        expandableLayout23 = (ExpandableLayout) findViewById(R.id.expandable_layout_23);
        expandableLayout24 = (ExpandableLayout) findViewById(R.id.expandable_layout_24);
        expandableLayout25 = (ExpandableLayout) findViewById(R.id.expandable_layout_25);
        expandableLayout26 = (ExpandableLayout) findViewById(R.id.expandable_layout_26);
        expandableLayout27 = (ExpandableLayout) findViewById(R.id.expandable_layout_27);
        expandableLayout28 = (ExpandableLayout) findViewById(R.id.expandable_layout_28);
        expandableLayout29 = (ExpandableLayout) findViewById(R.id.expandable_layout_29);
        expandableLayout30 = (ExpandableLayout) findViewById(R.id.expandable_layout_30);
        expandableLayout31 = (ExpandableLayout) findViewById(R.id.expandable_layout_31);





//        expandableLayout0.setOnExpansionUpdateListener(new ExpandableLayout.OnExpansionUpdateListener() {
//            @Override
//            public void onExpansionUpdate(float expansionFraction, int state) {
//
//
//            }
//
//        });
//
//        expandableLayout1.setOnExpansionUpdateListener(new ExpandableLayout.OnExpansionUpdateListener() {
//            @Override
//            public void onExpansionUpdate(float expansionFraction, int state) {
//                Log.d("ExpandableLayout1", "State: " + state);
//            }
//        });


        backBtn = (ImageView) findViewById(R.id.backBtn);


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                backBtn.setEnabled(false);

                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        backBtn.setEnabled(false);
        finish();
    }


}