package com.ceesolutions.samba.accessControl.Model;

import proguard.annotation.Keep;

/**
 * Created by ceeayaz on 4/5/18.
 */
@Keep
public class NavDrawerItem {
    private String title;


    public NavDrawerItem(){}

    public NavDrawerItem(String title){
        this.title = title;
    }

    public NavDrawerItem(String title, int icon, boolean isCounterVisible, String count){
        this.title = title;

    }

    public String getTitle(){
        return this.title;
    }

    public void setTitle(String title){
        this.title = title;
    }

}