package com.ceesolutions.samba.accessControl.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.res.ResourcesCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.ProfileSetupScreen;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.PinView;

/**
 * Created by ceeayaz on 2/20/18.
 */

public class PasscodeFragment extends Fragment {

    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private TextView validateBtn;
    private TextView errorMessage;
    private String passcode = "";
    private ImageView backBtn;
    private TextView headingTxt;
    private Utils utils;
    private PinView pinView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.set_passcode_screen, container, false);

        initViews(convertView);

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                passcode = pinView.getText().toString();

                if (!passcode.isEmpty()) {

                    if (!helper.validateInputForSC(passcode) || passcode.contains(" ")) {

                        errorMessage.setVisibility(View.VISIBLE);
                        errorMessage.setText("Passcode cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                        errorMessage.setError("");

                    } else {
                        if (passcode.length() < 1 || passcode.length() < 6) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.setText("Please Enter 6 digits passcode");
                            errorMessage.setError("");
                        } else {

                            constants.passCode = passcode;
                            ProfileSetupScreen.pager.setCurrentItem(1);
                            ProfileSetupScreen.currentItem++;
                            backBtn.setVisibility(View.VISIBLE);
                            headingTxt.setVisibility(View.INVISIBLE);
                            pinView.setText("");
                        }
                    }
                } else {

                    errorMessage.setVisibility(View.VISIBLE);
                    errorMessage.setText("Please Enter 6 digits passcode");
                    errorMessage.setError("");
                }
            }
        });

        return convertView;
    }

    public void initViews(View convertView) {

        ProfileSetupScreen activity = (ProfileSetupScreen) getActivity();
        backBtn = (ImageView) activity.findViewById(R.id.backBtn);
        headingTxt = (TextView) activity.findViewById(R.id.headingTxt);
//        backBtn.setVisibility(View.INVISIBLE);
//        headingTxt.setVisibility(View.INVISIBLE);
        validateBtn = (TextView) convertView.findViewById(R.id.validateBtn);
        errorMessage = (TextView) convertView.findViewById(R.id.errorMessage);
        pinView = (PinView) convertView.findViewById(R.id.pinView);
        requestQueue = Volley.newRequestQueue(getActivity());
        webService = new WebService();
        utils = new Utils(getActivity());
        helper = Helper.getHelper(getActivity());
        constants = Constants.getConstants(getActivity());
        pd = new Dialog(getActivity());
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        pinView.setTextColor(
                ResourcesCompat.getColor(getResources(), R.color.colorAccent, getActivity().getTheme()));
        pinView.setTextColor(
                ResourcesCompat.getColorStateList(getResources(), R.color.colorPrimary, getActivity().getTheme()));
        pinView.setLineColor(
                ResourcesCompat.getColor(getResources(), R.color.colorPrimary, getActivity().getTheme()));
        pinView.setLineColor(
                ResourcesCompat.getColorStateList(getResources(), R.color.line_colors, getActivity().getTheme()));
        pinView.setItemCount(6);
        pinView.setAnimationEnable(true);// start animation when adding text
        pinView.setCursorVisible(false);

        pinView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() == 6) {

                    passcode = s.toString();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(pinView.getWindowToken(), 0);

                } else {

                    passcode = s.toString();
                    // Do Nothing
                }
            }
        });

    }

}

