package com.ceesolutions.samba.accessControl.NonSambaUser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.UserConfirmationScreen;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.WebService;

import org.json.JSONObject;

/**
 * Created by ayaz on 1/9/18.
 */

public class UserPinCodeValidation extends AppCompatActivity {

    private EditText editSmsCode;
    private TextView validateBtn, regenerateBtn, errorMessageForSms, mobileNumberTxt;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private String cnic, mobileNo, smsCode, identityType, identityValue;
    private ProgressDialog pd;
    private Constants constants;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pincode_validation_screen);
        initViews();
        getSupportActionBar().hide();
    }

    public void initViews() {

        Intent intent = getIntent();
        String backPressed = intent.getStringExtra("BackPressed");

        if (!TextUtils.isEmpty(backPressed)) {
            //Do Nothing
        }else {
            identityType = intent.getStringExtra("identityType");
            if (identityType.equals("NTN")) {
                identityValue = intent.getStringExtra("ntn");
            } else if (identityType.equals("SCNIC")) {

                identityValue = intent.getStringExtra("scnic");
            } else if (identityType.equals("PASSPORT")) {

                identityValue = intent.getStringExtra("passport");
            } else if (identityType.equals("CNIC")) {
                identityValue = intent.getStringExtra("cnic");

            }
            mobileNo = intent.getStringExtra("mobileNo");
        }

        validateBtn = (TextView) findViewById(R.id.validateBtn);
        regenerateBtn = (TextView) findViewById(R.id.regenerateBtn);
        errorMessageForSms = (TextView) findViewById(R.id.errorMessageForSms);
        mobileNumberTxt = (TextView) findViewById(R.id.mobileNumberTxt);
        editSmsCode = (EditText) findViewById(R.id.editPincode);

        mobileNumberTxt.setText(mobileNo);
        constants = Constants.getConstants(UserPinCodeValidation.this);
        webService = new WebService();
        helper = Helper.getHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        pd = new ProgressDialog(UserPinCodeValidation.this);


        smsCode = editSmsCode.getText().toString();

        editSmsCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessageForSms.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessageForSms.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                smsCode = editable.toString();
            }
        });


        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (TextUtils.isEmpty(smsCode) || smsCode.length() < 6) {
                    errorMessageForSms.setVisibility(View.VISIBLE);
                    errorMessageForSms.bringToFront();
                    errorMessageForSms.setError("");
                    errorMessageForSms.setText("Must be 6 digits");

                }

                if ((!TextUtils.isEmpty(smsCode) && smsCode.length() == 6)) {

//                    Intent intent = new Intent(UserPinCodeValidation.this, UserConfirmationScreen.class);
//                    startActivity(intent);
//                    finish();
                    validateBtn.setEnabled(false);
                    pd = new ProgressDialog(UserPinCodeValidation.this);
                    pd.setMessage("Validating Please Wait...");
                    pd.setCancelable(false);
                    pd.show();
                    UserCodeValidation();
                }

            }
        });


        regenerateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
    }

    public void UserCodeValidation() {

        try {

            if (helper.isNetworkAvailable()) {

                String combineString = identityValue + smsCode;
                String pinData = helper.md5(combineString);

                webService.UserCodeValidationForNonSamba(pinData, identityValue, identityType, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                        JSONObject jsonObject = webService.getValCodeReNonSambaqResponse();
                        Log.d("Response", "Object----" + jsonObject);

                        if (jsonObject != null) {

                            if (jsonObject.has("Error")) {

                                Toast.makeText(UserPinCodeValidation.this, "SMS Code is not valid!", Toast.LENGTH_LONG).show();
                                validateBtn.setEnabled(true);
                                pd.dismiss();
                                clearViews();


                            } else {


                                pd.dismiss();
                                Toast.makeText(UserPinCodeValidation.this, "SMS Code Verified Successfully.", Toast.LENGTH_LONG).show();
                                clearViews();
                                Intent intent = new Intent(UserPinCodeValidation.this, UserConfirmationScreen.class);
                                if (identityType.equals("NTN")) {
                                    intent.putExtra("ntn", identityValue);
                                } else if (identityType.equals("SCNIC")) {

                                    intent.putExtra("scnic", identityValue);
                                } else if (identityType.equals("PASSPORT")) {

                                    intent.putExtra("passport", identityValue);
                                } else if (identityType.equals("CNIC")) {
                                    intent.putExtra("cnic", identityValue);

                                }
                                intent.putExtra("identityType", identityType);
                                intent.putExtra("mobileNo", mobileNo);
                                intent.putExtra("Type", "NonSamba");
                                startActivity(intent);
                                finish();
                                validateBtn.setEnabled(true);


                            }
                        } else {
                            Toast.makeText(UserPinCodeValidation.this, "SMS Code is not valid!", Toast.LENGTH_LONG).show();
                            validateBtn.setEnabled(true);
                            pd.dismiss();
                            clearViews();


                        }
                    }
                }, 5000);
            } else {
                Toast.makeText(UserPinCodeValidation.this, "Please check your internet connection!", Toast.LENGTH_LONG).show();
                validateBtn.setEnabled(true);
                pd.dismiss();
                clearViews();

            }
        } catch (Exception e) {

            Toast.makeText(UserPinCodeValidation.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
            validateBtn.setEnabled(true);
            pd.dismiss();
            clearViews();


        }
    }

    public void clearViews() {

        editSmsCode.setText("");
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UserPinCodeValidation.this, NonSambaUserValidationScreen.class);
        intent.putExtra("BackPressed", "BackPressed");
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
    }
}
