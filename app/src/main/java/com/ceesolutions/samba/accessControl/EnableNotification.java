package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.transactions.Model.Account;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.KeyGenerator;

/**
 * Created by ceeayaz on 6/1/18.
 */

public class EnableNotification extends AppCompatActivity {


    static final String DEFAULT_KEY_NAME = "default_key";
    private static final String TAG = EnableNotification.class.getSimpleName();
    private static final String DIALOG_FRAGMENT_TAG = "myFragment";
    private static final String SECRET_MESSAGE = "Very secret message";
    private static final String KEY_NAME_NOT_INVALIDATED = "key_not_invalidated";
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private EditText editQuestionAnswer;
    private TextView validateBtn, doneBtn, message, textView, heading;
    private SwitchCompat switchCompat;
    private ImageView notificationBtn, backBtn;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Utils utils;
    private ArrayList<Account> accounts;
    private Dialog pd, dialog1;
    private String flag;
    private KeyStore mKeyStore;
    private KeyGenerator mKeyGenerator;
    private SharedPreferences mSharedPreferences;
    private String isCheck;
    private boolean changeCompact = false;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enable_notification);
        initViews();
        getSupportActionBar().hide();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EnableNotification.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
//                finishAffinity();
            }
        });
    }


    public void initViews() {

        try {


            CryptLib _crypt = new CryptLib();
            helper = Helper.getHelper(this);
            requestQueue = Volley.newRequestQueue(this);
            constants = Constants.getConstants(EnableNotification.this);
            utils = new Utils(EnableNotification.this);
            pd = new Dialog(EnableNotification.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            appPreferences = new AppPreferences(EnableNotification.this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }
            editQuestionAnswer = (EditText) findViewById(R.id.editQuestionAnswer);
            validateBtn = (TextView) findViewById(R.id.validateBtn);
            switchCompat = (SwitchCompat) findViewById(R.id.switchCompat);
            backBtn = (ImageView) findViewById(R.id.backBtn);
            notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
            isCheck = _crypt.decrypt(constants.sharedPreferences.getString("NotificationEnable", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            validateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FirebaseAnalytics.getInstance(EnableNotification.this).logEvent("Notification_Update_Pressed",new Bundle());
                    FirebaseAnalytics.getInstance(EnableNotification.this).logEvent("Notification_Enabled_" + flag, new Bundle());

                    if (changeCompact) {

                        validateBtn.setEnabled(false);
                        AddTrustedDevice(flag);
                    } else {

                        if (!TextUtils.isEmpty(isCheck) && !isCheck.equals("N/A")) {


                            if (isCheck.equals("True")) {

                                utils.showDilogForError("Notifications is already enabled.", "WARNING");

                            } else {

                                utils.showDilogForError("Notifications is already disabled.", "WARNING");
                            }
                        }
                    }

                }
            });


            if (!TextUtils.isEmpty(isCheck) && !isCheck.equals("N/A")) {


                if (isCheck.equals("True")) {

                    switchCompat.setChecked(true);
                    editQuestionAnswer.setText("Enable");

                } else {

                    switchCompat.setChecked(false);
                    editQuestionAnswer.setText("Disable");
                }
            }


//        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//                if (isChecked) {
//
//                    flag = "1";
//                    editQuestionAnswer.setText("Enable");
////                    AccountVisibility(position, "1", switchCompat, isChecked);
//                } else {
//                    flag = "0";
//                    editQuestionAnswer.setText("Disable");
//                }
//            }
//        });

            switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    changeCompact = !changeCompact;

                    if (isChecked) {

                        flag = "Y";
                        editQuestionAnswer.setText("Enable");
//                    AccountVisibility(position, "1", switchCompat, isChecked);
                    } else {

                        flag = "N";
                        editQuestionAnswer.setText("Disable");
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void AddTrustedDevice(final String flag1) {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                final CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sDeviceMacAddress", constants.address);
                params.put("sFcm_Token", constants.fcmSharedPref.getString("fcmToken", "fcmToken"));
                params.put("sDeviceType", _crypt.decrypt(constants.sharedPreferences.getString("T24_Target", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("iUserID", "0");
                params.put("sNotificationFlag", flag1);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(EnableNotification.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.trustedDeviceURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObject = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_RegisterTrustedDeviceResult").getString("Status_Code").equals("00")) {
                                            try {
                                                pd.dismiss();
                                                if (flag1.equals("Y")) {


                                                    showDilogForErrorForLogin("Notifications is enabled successfully.", "SUCCESS");
                                                    constants.editor.putString("NotificationEnable", _crypt.encryptForParams("True", constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.editor.commit();
                                                    constants.editor.apply();
                                                    validateBtn.setEnabled(true);

                                                } else {

                                                    showDilogForErrorForLogin("Notifications is disabled successfully.", "SUCCESS");
                                                    constants.editor.putString("NotificationEnable", _crypt.encryptForParams("False", constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.editor.commit();
                                                    constants.editor.apply();
                                                    validateBtn.setEnabled(true);
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else if (jsonObject.getJSONObject("Signs_RegisterTrustedDeviceResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_RegisterTrustedDeviceResult").getString("Status_Description"), "ERROR", EnableNotification.this, new LoginScreen());
                                            validateBtn.setEnabled(true);

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_RegisterTrustedDeviceResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }
                            }

                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceMacAddress", constants.address);
//                        params.put("sFcm_Token", constants.fcmSharedPref.getString("fcmToken", "fcmToken"));
//                        params.put("sDeviceType", constants.sharedPreferences.getString("T24_Target", "N/A"));
//                        params.put("iUserID", "0");
//                        params.put("sNotificationFlag", flag1);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };
                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_LONG).show();

                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);

        }
    }


    public void showDilogForErrorForLogin(String msg, String header) {

        dialog1 = new Dialog(EnableNotification.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        message = (TextView) dialog1.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog1.findViewById(R.id.doneBtn);
        heading = (TextView) dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog1.dismiss();
                finish();

            }
        });
        message.setText(msg);
        heading.setText(header);
        dialog1.show();


    }
}