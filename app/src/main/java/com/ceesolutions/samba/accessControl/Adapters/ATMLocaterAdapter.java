package com.ceesolutions.samba.accessControl.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ceesolutions.samba.accessControl.Fragments.ATMFragment;
import com.ceesolutions.samba.accessControl.Fragments.BranchesFragment;

/**
 * Created by ceeayaz on 2/26/18.
 */

public class ATMLocaterAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public ATMLocaterAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ATMFragment atmFragment = new ATMFragment();
                return atmFragment;
            case 1:

                BranchesFragment branchesFragment = new BranchesFragment();
                return branchesFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}