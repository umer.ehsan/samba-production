package com.ceesolutions.samba.accessControl.Fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.core.content.res.ResourcesCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.ProfileSetupScreen;
import com.ceesolutions.samba.accessControl.RobotScreen;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.WebService;

/**
 * Created by ceeayaz on 2/20/18.
 */

public class LanguageFragment extends Fragment {

    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private String cnic,isFP;
    private TextView englishTxt,validateBtn,urduTxt;
    private View englishView,urduView;
    private Typeface typeface;
    private ImageView backBtn;
    private TextView headingTxt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.language_fragment, container, false);

        initViews(convertView);

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ProfileSetupScreen.pager.setCurrentItem(1);
                constants.languageEditor.putString("lang","eng");
                constants.languageEditor.commit();
                constants.languageEditor.apply();
                ProfileSetupScreen.currentItem++;
                backBtn.setVisibility(View.INVISIBLE);
                headingTxt.setVisibility(View.INVISIBLE);


            }
        });

        englishTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                urduTxt.setTextColor(getResources().getColor(R.color.urduColor));
                urduView.setVisibility(View.GONE);
                englishTxt.setTextColor(getResources().getColor(R.color.tabunderline));
                englishView.setVisibility(View.VISIBLE);
                constants.languageEditor.putString("lang","eng");
                constants.languageEditor.commit();
                constants.languageEditor.apply();
            }
        });

        urduTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                englishTxt.setTextColor(getResources().getColor(R.color.urduColor));
                englishView.setVisibility(View.GONE);
                urduTxt.setTextColor(getResources().getColor(R.color.tabunderline));
                urduView.setVisibility(View.VISIBLE);
                constants.languageEditor.putString("lang","urdu");
                constants.languageEditor.commit();
                constants.languageEditor.apply();
            }
        });



//        headingTxt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                headingTxt.setVisibility(View.INVISIBLE);
//                ProfileSetupScreen.pager.setCurrentItem(1);
//                constants.languageEditor.putString("lang","eng");
//                constants.languageEditor.commit();
//                constants.languageEditor.apply();
//
//            }
//        });

        return convertView;
    }

    public void initViews(View convertView) {

        ProfileSetupScreen activity = (ProfileSetupScreen)getActivity();
        backBtn = (ImageView) activity.findViewById(R.id.backBtn);
        headingTxt = (TextView) activity.findViewById(R.id.headingTxt);
//        backBtn.setVisibility(View.INVISIBLE);
//        headingTxt.setVisibility(View.VISIBLE);
        typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Jameel Noori Nastaleeq.ttf");
        validateBtn = (TextView) convertView.findViewById(R.id.validateBtn);
        englishTxt = (TextView) convertView.findViewById(R.id.englishTxt);
        urduTxt = (TextView) convertView.findViewById(R.id.urduTxt);
        urduTxt.setTypeface(typeface);
        englishView = (View) convertView.findViewById(R.id.englishView);
        urduView = (View) convertView.findViewById(R.id.urduView);
        requestQueue = Volley.newRequestQueue(getActivity());
        webService = new WebService();
        helper = Helper.getHelper(getActivity());
        constants = Constants.getConstants(getActivity());
        pd = new Dialog(getActivity());
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }

    public void CheckForRegistration(){

        Intent intent = new Intent(getActivity(), RobotScreen.class);
        constants.credentialsEditor.putString("cnic",cnic);
        constants.credentialsEditor.putString("identityType","CNIC");
        constants.credentialsEditor.commit();
        constants.credentialsEditor.apply();
        pd.dismiss();
        getActivity().overridePendingTransition(0, 0);
        startActivity(intent);
        getActivity().finish();
        validateBtn.setEnabled(true);
    }

}