package com.ceesolutions.samba.accessControl.Fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.ATMLocatorScreen;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by ceeayaz on 2/25/18.
 */

public class ATMFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ArrayList<Marker> markerList;
    private HashMap<String, LatLng> mapMarkers;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.atm_fragment, container, false);
        markerList = new ArrayList<Marker>();
        mapMarkers = new HashMap<>();
        FirebaseAnalytics.getInstance(getContext()).logEvent("Atm_Pressed",new Bundle());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        if (mapFragment != null) {
            mapFragment.getMapAsync(this);

        }

        return convertView;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {


            mMap = googleMap;
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            } else {
// Show rationale and request permission.
            }
            if (ATMLocatorScreen.isPermissionGranted) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(ATMLocatorScreen.latitude, ATMLocatorScreen.longitude)));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(13.0f));
                    }

                }, 5000);
            }

//            Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                public void run() {
                    mapMarkers.put("Fountain Branch Saddar", new LatLng(24.853996, 67.02732));
                    mapMarkers.put("DHA Ittehad Branch", new LatLng(24.831521, 67.073732));
                    mapMarkers.put("Hyderi Branch", new LatLng(24.932723, 67.039228));
                    mapMarkers.put("Rashid Minhas Road Branch", new LatLng(24.906632, 67.111918));
                    mapMarkers.put("DHA Phase VI Branch", new LatLng(24.79686, 67.050979));
                    mapMarkers.put("Bahria Complex - 1", new LatLng(24.845479, 66.996629));
                    mapMarkers.put("Shahrah-e-Faisal Branch", new LatLng(24.866044, 67.078494));
                    mapMarkers.put("Gulshan-e-Iqbal Branch", new LatLng(24.91078, 67.08683));
                    mapMarkers.put("Clifton Branch", new LatLng(24.815917, 67.021583));
                    mapMarkers.put("Bahadurabad Branch", new LatLng(24.883738, 67.064107));
                    mapMarkers.put("SMCHS", new LatLng(24.864379, 67.055213));
                    mapMarkers.put("DHA Lahore", new LatLng(31.47329, 74.378846));
                    mapMarkers.put("Mall Road Branch", new LatLng(31.563559, 74.317009));
                    mapMarkers.put("Gulberg Branch", new LatLng(31.517488, 74.345221));
                    mapMarkers.put("Allama Iqbal Town Branch", new LatLng(31.523205, 74.289626));
                    mapMarkers.put("Johar Town Branch", new LatLng(31.458469, 74.276658));
                    mapMarkers.put("Cavalry Ground Branch", new LatLng(31.50148, 74.365795));
                    mapMarkers.put("New Garden Town Branch", new LatLng(31.502654, 74.324629));
                    mapMarkers.put("Sarwar Road Branch", new LatLng(31.526361, 74.37628));
                    mapMarkers.put("Jinnah Avenue Branch", new LatLng(33.722575, 73.081091));
                    mapMarkers.put("F-11 Branch", new LatLng(33.68334, 72.989253));
                    mapMarkers.put("F-7 Branch", new LatLng(33.719341, 73.052437));
                    mapMarkers.put("Rawalpindi Cantt. Branch", new LatLng(33.595875, 73.058297));
                    mapMarkers.put("Saddar Road Branch", new LatLng(34.001846, 71.545195));
                    mapMarkers.put("Paris Road Branch", new LatLng(32.506865, 74.538376));
                    mapMarkers.put("Liaquat Road Branch", new LatLng(31.426392, 73.07422));
                    mapMarkers.put("Nusrat Road Branch", new LatLng(30.194315, 71.441874));
                    mapMarkers.put("G.T. Road Branch", new LatLng(32.182453, 74.182033));
                    mapMarkers.put("PICT (off-site ATM)", new LatLng(24.824859, 66.975683));
                    mapMarkers.put("Faisal Town Branch", new LatLng(31.468215, 74.307617));
                    mapMarkers.put("Bahria Town Branch", new LatLng(33.521502, 73.101124));
                    mapMarkers.put("DHA Phase V ", new LatLng(31.462841, 74.414605));
                    mapMarkers.put("Saba Avenue", new LatLng(24.786875, 67.056726));
                    mapMarkers.put("Bagh AJK", new LatLng(33.979525, 73.773853));
                    mapMarkers.put("DHA Phase II", new LatLng(33.530841, 73.157622));
                    mapMarkers.put("Badami Bagh", new LatLng(31.588901, 74.318975));
                    mapMarkers.put("DHA Tauheed", new LatLng(24.7936797, 67.064331));
                    mapMarkers.put("Wah Cantt", new LatLng(33.777484, 72.744024));

                    Iterator it = mapMarkers.entrySet().iterator();
                    BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.mipmap.pin);

                    while (it.hasNext()) {

                        Map.Entry pair = (Map.Entry) it.next();
                        markerList.add(mMap.addMarker(new MarkerOptions().position((LatLng) pair.getValue()).title(pair.getKey().toString()).icon(icon)));
                        System.out.println(pair.getKey() + " = " + pair.getValue());

                    }
//                }
//
//            }, 2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
