package com.ceesolutions.samba.accessControl.Model;

import proguard.annotation.Keep;

/**
 * Created by ceeayaz on 7/16/18.
 */
@Keep
public class Channels {


    private String channelDes;
    private String channelID;
    private String channelName;

    public String getChannelDes() {
        return channelDes;
    }

    public void setChannelDes(String channelDes) {
        this.channelDes = channelDes;
    }

    public String getChannelID() {
        return channelID;
    }

    public void setChannelID(String channelID) {
        this.channelID = channelID;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
}
