package com.ceesolutions.samba.accessControl;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.views.CircleImageView;
import com.ceesolutions.samba.views.RoundImageView;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ceeayaz on 4/18/18.
 */

public class EditProfileScreen extends AppCompatActivity {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public static double latitude;
    public static double longitude;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public String longitude1;
    public String latitude1;
    public boolean isPic = false;
    String imgString;
    CircleImageView circleImageView;
    int maxHeight = 400;
    //choose a max width
    int maxWidth = 400;
    private CircleImageView profile_image;
    private EditText editAddress, editCnic, editEmailCode, editMobileNumber;
    private ImageView cameraBtn, backBtn, notificationBtn;
    private TextView userName, errorMessageForAddress, errorMessageForEmail, errorMessageForMobile, validateBtn, validateBtn1;
    private String address, mobile, email;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private TextView heading, textView, doneBtn;
    private Bitmap bitmap;
    private ImageView imageBtn;
    private Dialog dialog, dialog1;
    private RelativeLayout relativeLayout4;
    private TextView headingTxt;
    private Utils utils;
    private ScrollView scrollView;
    private RoundImageView title;
    private TextView titleTxt;
    private int color;
    private AppPreferences appPreferences;
    private String location;

    public static Bitmap decodeUri(Context c, Uri uri, final int requiredSize)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);
        initViews();
        getSupportActionBar().hide();


        editAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                errorMessageForAddress.setVisibility(View.GONE);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessageForAddress.setVisibility(View.GONE);

            }

            @Override
            public void afterTextChanged(Editable s) {

                address = s.toString();

            }
        });

        editEmailCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                errorMessageForEmail.setVisibility(View.GONE);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessageForEmail.setVisibility(View.GONE);

            }

            @Override
            public void afterTextChanged(Editable s) {

                email = s.toString();

            }
        });


        editMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


                errorMessageForMobile.setVisibility(View.GONE);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessageForMobile.setVisibility(View.GONE);

            }

            @Override
            public void afterTextChanged(Editable s) {

                mobile = s.toString();

            }
        });
    }

    public void initViews() {

        try {

            CryptLib _crypt = new CryptLib();

            titleTxt = (TextView) findViewById(R.id.titleTxt);
            title = (RoundImageView) findViewById(R.id.title);
            titleTxt.bringToFront();
            profile_image = (CircleImageView) findViewById(R.id.profile_image);
            Intent intent = getIntent();
            String type = intent.getStringExtra("view");
            headingTxt = (TextView) findViewById(R.id.headingTxt);
            editAddress = (EditText) findViewById(R.id.editAddress);
            editCnic = (EditText) findViewById(R.id.editCnic);
            editEmailCode = (EditText) findViewById(R.id.editEmailCode);
            editMobileNumber = (EditText) findViewById(R.id.editMobileNumber);
            cameraBtn = (ImageView) findViewById(R.id.cameraBtn);
            backBtn = (ImageView) findViewById(R.id.backBtn);
            notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
            userName = (TextView) findViewById(R.id.userName);
            relativeLayout4 = (RelativeLayout) findViewById(R.id.relativeLayout4);
            validateBtn1 = (TextView) findViewById(R.id.validateBtn1);
            errorMessageForAddress = (TextView) findViewById(R.id.errorMessageForAddress);
            errorMessageForEmail = (TextView) findViewById(R.id.errorMessageForEmail);
            errorMessageForMobile = (TextView) findViewById(R.id.errorMessageForMobile);
            validateBtn = (TextView) findViewById(R.id.validateBtn);
            scrollView = (ScrollView) findViewById(R.id.scrollView);
            utils = new Utils(EditProfileScreen.this);
            helper = Helper.getHelper(this);
            requestQueue = Volley.newRequestQueue(this);
            constants = Constants.getConstants(EditProfileScreen.this);
            pd = new Dialog(this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            appPreferences = new AppPreferences(EditProfileScreen.this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }
            String name = _crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            String s = name.substring(0, 1).toUpperCase();

            switch (s.toUpperCase()) {

                case "A":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "B":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "C":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "D":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "E":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "F":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "G":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "H":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "I":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "J":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "K":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "L":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "M":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "N":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "O":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "P":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "Q":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "R":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "S":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "T":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "U":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "V":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "W":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "X":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "Y":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "Z":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                default:
                    color = getResources().getColor(R.color.defaultColor);
                    break;
            }

            String image = _crypt.decrypt(constants.sharedPreferences.getString("ProfilePic", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

            Log.d("img--", image);
            try {


                if (!TextUtils.isEmpty(image) && !image.equals("N/A") && !image.equals("")) {
                    image = image.replaceAll("%2B", "+");
                    titleTxt.setVisibility(View.INVISIBLE);
                    title.setVisibility(View.INVISIBLE);
                    byte[] imageByteArray = Base64.decode(image, Base64.DEFAULT);
                    RequestOptions options = new RequestOptions();
                    options.centerCrop();
                    Glide.with(EditProfileScreen.this)
                            .load(imageByteArray)
                            .apply(options)
                            .into(profile_image);
                } else {

                    profile_image.setVisibility(View.INVISIBLE);
                    title.setBackgroundTintList(ColorStateList.valueOf((color)));
                    titleTxt.setText(s);
                }

            } catch (Exception e) {

                e.printStackTrace();

            }
            editAddress.setText(_crypt.decrypt(constants.sharedPreferences.getString("UserAddress", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            editMobileNumber.setText(_crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            editEmailCode.setText(_crypt.decrypt(constants.sharedPreferences.getString("EmailAddress", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            userName.setText(_crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            editCnic.setText(_crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            cameraBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    selectImage();
                }
            });

            titleTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectImage();
                }
            });
            profile_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    selectImage();
                }
            });

            validateBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FirebaseAnalytics.getInstance(EditProfileScreen.this).logEvent("Profile_Picture_Updated_Pressed",new Bundle());
                    mobile = editMobileNumber.getText().toString();
                    email = editEmailCode.getText().toString();
                    address = editAddress.getText().toString();

                    if (isPic) {
                        validateBtn.setEnabled(false);
                        SetProfileImage();
                    } else {
                        utils.showDilogForError("Please take a profile picture from camera or choose from gallery.", "WARNING");
                    }


                }
            });

            validateBtn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    mobile = editMobileNumber.getText().toString();
                    email = editEmailCode.getText().toString();
                    address = editAddress.getText().toString();

                    validateBtn.setEnabled(false);
                    SetProfileImage();


                }
            });


            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    finish();
                }
            });

            notificationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(EditProfileScreen.this, MessageCenter.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
//                finish();
//                finishAffinity();
                }
            });

            if (type.equals("view")) {

                relativeLayout4.setVisibility(View.GONE);
                validateBtn1.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
                cameraBtn.setVisibility(View.INVISIBLE);
                profile_image.setEnabled(false);
                validateBtn.setVisibility(View.GONE);
                headingTxt.setText("View Profile");
            } else if (type.equals("edit")) {

                cameraBtn.setVisibility(View.VISIBLE);
                profile_image.setEnabled(true);
                cameraBtn.setVisibility(View.VISIBLE);
                validateBtn.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.VISIBLE);
                relativeLayout4.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void selectImage() {
        try {
            PackageManager pm = EditProfileScreen.this.getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, EditProfileScreen.this.getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(EditProfileScreen.this);
                builder.setTitle("Select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(EditProfileScreen.this, "Camera Permission error", Toast.LENGTH_LONG).show();
            checkAndRequestPermissions();
        } catch (Exception e) {
            Toast.makeText(EditProfileScreen.this, "Camera Permission error", Toast.LENGTH_LONG).show();
            checkAndRequestPermissions();
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);

                int width = profile_image.getWidth();
                int height = profile_image.getHeight();
                RequestOptions options = new RequestOptions();
                options.centerInside();
                options.override(width, height);
                titleTxt.setVisibility(View.INVISIBLE);
                title.setVisibility(View.INVISIBLE);
                profile_image.setVisibility(View.VISIBLE);
                options.placeholder(R.drawable.ic_launcher_background);
                Glide.with(EditProfileScreen.this)
                        .load(bitmap)
                        .apply(options)
                        .into(profile_image);
                int bmOriginalWidth = bitmap.getWidth();
                int bmOriginalHeight = bitmap.getHeight();
                double originalWidthToHeightRatio = 1.0 * bmOriginalWidth / bmOriginalHeight;
                double originalHeightToWidthRatio = 1.0 * bmOriginalHeight / bmOriginalWidth;
//                bitmap = getResizedBitmap(helper.getScaledBitmap(bitmap, bmOriginalWidth, bmOriginalHeight,
//                        originalWidthToHeightRatio, originalHeightToWidthRatio,
//                        500, 500), 400);
                bitmap = getCroppedBitmap(bitmap);
                isPic = true;
                Log.e("Activity", "Pick from Camera::>>> ");

//                circleImageView.setImageBitmap(getCroppedBitmap(bitmap));

                imgString = Base64.encodeToString(getBytesFromBitmap(bitmap),
                        Base64.NO_WRAP);
//                deleteLastPhotoFromGallery();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            try {
                Uri selectedImage = data.getData();
                Log.d("File", "---" + selectedImage.getPath());
                File file = new File(getRealPathFromURI(selectedImage));
                Log.d("File", "---" + file.length());
                long fileSizeInKB = file.length() / 1024;
                long fileSizeInMB = fileSizeInKB / 1024;

                if (fileSizeInMB > 4) {

                    utils.showDilogForError("You cannot upload file greater than 4MB.", "ERROR");
                } else {
                    try {


                        bitmap = MediaStore.Images.Media.getBitmap(EditProfileScreen.this.getContentResolver(), selectedImage);
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

                        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
                        int width = profile_image.getWidth();
                        int height = profile_image.getHeight();
                        RequestOptions options = new RequestOptions();
                        options.centerInside();
                        options.override(width, height);
                        profile_image.setVisibility(View.VISIBLE);
                        options.placeholder(R.drawable.ic_launcher_background);
                        Glide.with(EditProfileScreen.this)
                                .load((bitmap))
                                .apply(options)
                                .into(profile_image);
                        titleTxt.setVisibility(View.INVISIBLE);
                        title.setVisibility(View.INVISIBLE);
                        int bmOriginalWidth = bitmap.getWidth();
                        int bmOriginalHeight = bitmap.getHeight();
                        double originalWidthToHeightRatio = 1.0 * bmOriginalWidth / bmOriginalHeight;
                        double originalHeightToWidthRatio = 1.0 * bmOriginalHeight / bmOriginalWidth;
                        bitmap = getCroppedBitmap(getResizedBitmap(bitmap, 400));
                        Log.e("Activity", "Pick from Gallery::>>> ");
                        isPic = true;
//                circleImageView.setImageBitmap(getCroppedBitmap(bitmap));
                        imgString = Base64.encodeToString(getBytesFromBitmap(bitmap),
                                Base64.NO_WRAP);
//                        new AsyncCaller().execute(bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
//private void selectImage() {
//    try {
//        PackageManager pm = EditProfileScreen.this.getPackageManager();
//        int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, EditProfileScreen.this.getPackageName());
//        if (hasPerm == PackageManager.PERMISSION_GRANTED) {
//            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
//            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(EditProfileScreen.this);
//            builder.setTitle("Select Option");
//            builder.setItems(options, new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int item) {
//                    if (options[item].equals("Take Photo")) {
//                        dialog.dismiss();
//                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                        startActivityForResult(intent, PICK_IMAGE_CAMERA);
//                    } else if (options[item].equals("Choose From Gallery")) {
//                        dialog.dismiss();
//                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
//                    } else if (options[item].equals("Cancel")) {
//                        dialog.dismiss();
//                    }
//                }
//            });
//            builder.show();
//        } else
//            Toast.makeText(EditProfileScreen.this, "Camera Permission error", Toast.LENGTH_LONG).show();
//        checkAndRequestPermissions();
//    } catch (Exception e) {
//        Toast.makeText(EditProfileScreen.this, "Camera Permission error", Toast.LENGTH_LONG).show();
//        checkAndRequestPermissions();
//        e.printStackTrace();
//    }
//}
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == PICK_IMAGE_CAMERA) {
//            try {
//                Uri selectedImage = data.getData();
//                bitmap = (Bitmap) data.getExtras().get("data");
//                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
//
//
//                isPic = true;
//                Log.e("Activity", "Pick from Camera::>>> ");
//
////                circleImageView.setImageBitmap(getCroppedBitmap(bitmap));
//                RequestOptions options = new RequestOptions();
//                options.centerInside();
//                titleTxt.setVisibility(View.INVISIBLE);
//                title.setVisibility(View.INVISIBLE);
//                profile_image.setVisibility(View.VISIBLE);
//                options.placeholder(R.drawable.ic_launcher_background);
//                Glide.with(EditProfileScreen.this)
//                        .load(bitmap)
//                        .apply(options)
//                        .into(profile_image);
//                imgString = Base64.encodeToString(getBytesFromBitmap(bitmap),
//                        Base64.NO_WRAP);
////                deleteLastPhotoFromGallery();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } else if (requestCode == PICK_IMAGE_GALLERY) {
//            try {
//                Uri selectedImage = data.getData();
//
//                bitmap = MediaStore.Images.Media.getBitmap(EditProfileScreen.this.getContentResolver(), selectedImage);
//                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
//                Log.e("Activity", "Pick from Gallery::>>> ");
//                isPic = true;
////                circleImageView.setImageBitmap(getCroppedBitmap(bitmap));
//                RequestOptions options = new RequestOptions();
//                options.centerInside();
//                profile_image.setVisibility(View.VISIBLE);
//                options.placeholder(R.drawable.ic_launcher_background);
//                Glide.with(EditProfileScreen.this)
//                        .load(bitmap)
//                        .apply(options)
//                        .into(profile_image);
//                titleTxt.setVisibility(View.INVISIBLE);
//                title.setVisibility(View.INVISIBLE);
//                imgString = Base64.encodeToString(getBytesFromBitmap(getResizedBitmap(bitmap, 400)),
//                        Base64.NO_WRAP);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }

    public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
        // TODO Auto-generated method stub
        int targetWidth = 500;
        int targetHeight = 500;
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,

                targetHeight, Bitmap.Config.ARGB_8888);


        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth) / 2,
                ((float) targetHeight) / 2,
                (Math.min(((float) targetWidth),
                        ((float) targetHeight)) / 2),
                Path.Direction.CCW);


        canvas.clipPath(path);
        Bitmap sourceBitmap = scaleBitmapImage;
        canvas.drawBitmap(sourceBitmap,
                new Rect(0, 0, sourceBitmap.getWidth(),
                        sourceBitmap.getHeight()),
                new Rect(0, 0, targetWidth,
                        targetHeight), null);
        return targetBitmap;
    }


    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        try {


            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            return stream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        try {


            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
            canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                    bitmap.getWidth() / 2, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
            //return _bmp;
            return output;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void SetProfileImage() {

//        Toast.makeText(RegistrationScreen.EditProfileScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                CryptLib _crypt = new CryptLib();
                String img = imgString.replaceAll("\\+", "@PLUS@");
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sBase64Image", img);
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(EditProfileScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.setProfileImageURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        try {
                                            CryptLib _crypt = new CryptLib();

                                            if (jsonObject.getJSONObject("Signs_SetProfileAvatarResult").getString("Status_Code").equals("00")) {

                                                pd.dismiss();
                                                Intent intent = new Intent(EditProfileScreen.this, HomeScreen.class);
                                                overridePendingTransition(0, 0);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                                finish();
                                                finishAffinity();
                                                validateBtn.setEnabled(true);
                                                constants.editor.putString("ProfilePic", _crypt.encryptForParams(imgString, constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.editor.putString("UserProgress", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_SetProfileAvatarResult").getString("UserProgress"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                constants.editor.commit();
                                                constants.editor.apply();

                                            } else if (jsonObject.getJSONObject("Signs_SetProfileAvatarResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_SetProfileAvatarResult").getString("Status_Description"), "ERROR", EditProfileScreen.this, new LoginScreen());
                                                validateBtn.setEnabled(true);

                                            } else if (jsonObject.getJSONObject("Signs_SetProfileAvatarResult").getString("Status_Code").equals("91")) {

                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_SetProfileAvatarResult").getString("Status_Description"), "ERROR", EditProfileScreen.this, new LoginScreen());
                                                validateBtn.setEnabled(true);


                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_SetSecretQuestionResult").getString("Status_Description"), "ERROR");
                                                validateBtn.setEnabled(true);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Profile Picture can not be set. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Profile Picture can not be set. Please try again later.", "ERROR");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Profile Picture can not be set. Please try again later.", "ERROR");
                                }
                            }
                        })

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sBase64Image", imgString);
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(EditProfileScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(EditProfileScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            pd.dismiss();
            utils.showDilogForError("Profile Picture can not be set. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
        }
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(EditProfileScreen.this,
                android.Manifest.permission.CAMERA);
        int readStoragePermission = ContextCompat.checkSelfPermission(EditProfileScreen.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStoragePermission = ContextCompat.checkSelfPermission(EditProfileScreen.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }
        if (writeStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(EditProfileScreen.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("Login", "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Login", "sms & location services permission granted");
                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("Login", "Some permissions are not granted ask again ");
                        //permission is denied (EditProfileScreen.this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileScreen.this, Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(EditProfileScreen.this, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(EditProfileScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            showDialogOK("Camera and Read/Write Storage Services Permission required for EditProfileScreen.this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(EditProfileScreen.this, "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(EditProfileScreen.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

//    private void deleteLastPhotoFromGallery() {
//
//        String[] projection = new String[]{
//                MediaStore.Images.ImageColumns._ID,
//                MediaStore.Images.ImageColumns.DATA,
//                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
//                MediaStore.Images.ImageColumns.DATE_TAKEN,
//                MediaStore.Images.ImageColumns.MIME_TYPE};
//
//        final Cursor cursor = EditProfileScreen.this.getContentResolver().query(
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
//                null, null, MediaStore.Images.ImageColumns.DATE_TAKEN + "DESC");
//
//        if (cursor != null) {
//            cursor.moveToFirst();
//
//            int column_index_data =
//                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//
//            String image_path = cursor.getString(column_index_data);
//
//            File file = new File(image_path);
//            if (file.exists()) {
//                file.delete();
//            }
//        }
//    }

    public void showDilogForError(String msg, String header) {

        dialog1 = new Dialog(EditProfileScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog1.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog1.findViewById(R.id.doneBtn);
        heading = (TextView) dialog1.findViewById(R.id.heading);
//        dialog.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
                ProfileSetupScreen.currentItem = 0;
                Intent intent = new Intent(EditProfileScreen.this, HomeScreen.class);
                startActivity(intent);
                EditProfileScreen.this.finish();
                validateBtn.setEnabled(true);
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog1.show();

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        try {


            int width = image.getWidth();
            int height = image.getHeight();

            float bitmapRatio = (float) width / (float) height;
            if (bitmapRatio > 1) {
                width = maxSize;
                height = (int) (width / bitmapRatio);
            } else {
                height = maxSize;
                width = (int) (height * bitmapRatio);
            }
            return Bitmap.createScaledBitmap(image, width, height, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {
        try {


            final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            final Canvas canvas = new Canvas(output);

            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(rect);

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            canvas.drawOval(rectF, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            bitmap.recycle();
            return output;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private class AsyncCaller extends AsyncTask<Bitmap, Void, String> {

        public Bitmap bitmap1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread

//            pd.show();

        }

        @Override
        protected String doInBackground(Bitmap... params) {

            //this method will be running on a background thread so don't update UI from here
            //do your long-running http tasks here, you don't want to pass argument and u can access the parent class' variable url over here
            try {

                bitmap1 = params[0];
                imgString = Base64.encodeToString(getBytesFromBitmap(bitmap1),
                        Base64.NO_WRAP);

            } catch (Exception e) {

                e.printStackTrace();

            }

            return imgString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


        }

    }
}
