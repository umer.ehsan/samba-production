package com.ceesolutions.samba.accessControl.Fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.RobotScreen;
import com.ceesolutions.samba.accessControl.UserValidationScreen;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.WebService;

import org.json.JSONObject;

/**
 * Created by ayaz on 1/5/18.
 */

public class PassportFragment extends Fragment {

    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private String passport,isFP;
    private EditText editPassport;
    private TextView errorMessage,validateBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View convertView = inflater.inflate(R.layout.passport_fragment, container, false);

        initViews(convertView);

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(passport)) {
                    errorMessage.setVisibility(View.VISIBLE);
                    errorMessage.bringToFront();
                    errorMessage.setError("");
                    errorMessage.setText("Passport can not be empty");

                } else {

                    validateBtn.setEnabled(false);
//                    Registration();
                    if(TextUtils.isEmpty(isFP)){

                        CheckForRegistration();


                    }else{

                        CheckForFP();

                    }
                }

            }
        });


        editPassport.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                passport = editable.toString();
            }
        });


        return convertView;
    }

    public void initViews(View convertView) {

        validateBtn = (TextView) convertView.findViewById(R.id.validateBtn);
        errorMessage = (TextView) convertView.findViewById(R.id.errorMessage);
        editPassport = (EditText) convertView.findViewById(R.id.editPassport);
        requestQueue = Volley.newRequestQueue(getActivity());
        webService = new WebService();
        helper = Helper.getHelper(getActivity());
        constants = Constants.getConstants(getActivity());
        pd = new Dialog(getActivity());
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Intent intent = getActivity().getIntent();
        isFP = intent.getStringExtra("FP");
    }

    public void CheckForRegistration(){

        passport = editPassport.getText().toString();
        passport = passport.replaceAll("\\\\-\\W", "");
        passport = passport.replaceAll("-","").trim();
//        Toast.makeText(getActivity(), "CNIC Verified Successfully.", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(getActivity(), RobotScreen.class);
        constants.credentialsEditor.putString("passport",passport);
        constants.credentialsEditor.putString("identityType","PASSPORT");
        constants.credentialsEditor.commit();
        constants.credentialsEditor.apply();
        pd.dismiss();
        editPassport.setText("");
        getActivity().overridePendingTransition(0, 0);
        startActivity(intent);
        getActivity().finish();
        validateBtn.setEnabled(true);
    }

    public void CheckForFP(){

        passport = editPassport.getText().toString();
        passport = passport.replaceAll("\\\\-\\W", "");
        passport = passport.replaceAll("-","").trim();
//        Toast.makeText(getActivity(), "CNIC Verified Successfully.", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(getActivity(), RobotScreen.class);
        intent.putExtra("FP",isFP);
        constants.credentialsEditor.putString("passport",passport);
        constants.credentialsEditor.putString("identityType","PASSPORT");
        constants.credentialsEditor.commit();
        constants.credentialsEditor.apply();
        pd.dismiss();
        editPassport.setText("");
        getActivity().overridePendingTransition(0, 0);
        startActivity(intent);
        getActivity().finish();
        validateBtn.setEnabled(true);
    }

    public void Registration() {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                passport = editPassport.getText().toString();
                passport = passport.replaceAll("\\\\-\\W", "");
                passport = passport.replaceAll("-","").trim();
                webService.Registration(passport, constants.version, constants.model, constants.dateTime, constants.address, "PASSPORT", requestQueue);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                        JSONObject jsonObject = webService.getRegReqResponse();
                        Log.d("Response", "Object----" + jsonObject);

                        if (jsonObject != null) {

                            if (jsonObject.has("Error")) {

                                Toast.makeText(getActivity(), "Passport number is not valid!", Toast.LENGTH_LONG).show();
                                validateBtn.setEnabled(true);
                                pd.dismiss();
                                editPassport.setText("");


                            } else {


                                Toast.makeText(getActivity(), "Passport Verified Successfully.", Toast.LENGTH_LONG).show();
                                editPassport.setText("");
                               Intent intent = new Intent(getActivity(), UserValidationScreen.class);
                                intent.putExtra("passport", passport);
                                intent.putExtra("identityType", "PASSPORT");
                                constants.credentialsEditor.putString("passport",passport);
                                constants.credentialsEditor.putString("identityType","PASSPORT");
                                constants.credentialsEditor.commit();
                                constants.credentialsEditor.apply();
                                pd.dismiss();
                                getActivity().overridePendingTransition(0, 0);
                                startActivity(intent);
                                getActivity().finish();
                                validateBtn.setEnabled(true);


                            }
                        } else {
                            Toast.makeText(getActivity(), "Passport number is not valid!", Toast.LENGTH_LONG).show();
                            validateBtn.setEnabled(true);
                            pd.dismiss();
                            editPassport.setText("");

                        }
                    }
                }, 15000);
            } else {
                Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_LONG).show();
                validateBtn.setEnabled(true);
                pd.dismiss();
                editPassport.setText("");

            }
        } catch (Exception e) {

            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_LONG).show();
            validateBtn.setEnabled(true);
            pd.dismiss();
            editPassport.setText("");

        }
    }
}