package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.PinView;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by ceeayaz on 4/23/18.
 */

public class PasswordScreen extends AppCompatActivity {

    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd, dialog;
    private TextView validateBtn;
    private TextView errorMessageForPassword, heading, message, textView, doneBtn;
    private EditText editPassword;
    private String password = "";
    private ImageView backBtn, notificationBtn;
    private TextView headingTxt;
    private Utils utils;
    private PinView pinView;
    private ImageView info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.password);
        initViews();
        getSupportActionBar().hide();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(PasswordScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
//                finishAffinity();
            }
        });

        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessageForPassword.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                errorMessageForPassword.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

                password = s.toString();
            }
        });

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                FirebaseAnalytics.getInstance(PasswordScreen.this).logEvent("Current_Password_Entered", new Bundle());

                password = editPassword.getText().toString().trim();

                if (TextUtils.isEmpty(password)) {
                    errorMessageForPassword.setVisibility(View.VISIBLE);
                    errorMessageForPassword.bringToFront();
                    errorMessageForPassword.setError("");
                    errorMessageForPassword.setText("Please enter valid password");
//                    editPassword.setText("");

                } else if (!helper.validateInputForSC(password) || password.contains(" ")) {

                    errorMessageForPassword.setVisibility(View.VISIBLE);
                    errorMessageForPassword.bringToFront();
                    errorMessageForPassword.setError("");
                    errorMessageForPassword.setText("Password cannot contains <,>,\",',%,(,),&,+,\\,~,space");
//                    editPassword.setText("");

                } else {


                    Intent intent = new Intent(PasswordScreen.this, ConfirmPasswordScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("password",password);
                    editPassword.getText().clear();
                    startActivity(intent);
                }


            }
        });

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showInfo();
            }
        });

    }


    public void initViews() {


        validateBtn = (TextView) findViewById(R.id.validateBtn);
        requestQueue = Volley.newRequestQueue(PasswordScreen.this);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        editPassword = (EditText) findViewById(R.id.editPassword);
        utils = new Utils(PasswordScreen.this);
        helper = Helper.getHelper(PasswordScreen.this);
        constants = Constants.getConstants(PasswordScreen.this);
        pd = new Dialog(PasswordScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        info = (ImageView) findViewById(R.id.info);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        errorMessageForPassword = (TextView) findViewById(R.id.errorMessageForPassword);


    }


    public void showInfo() {

        dialog = new Dialog(PasswordScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.password_policy);
        message = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        heading.setText("PASSWORD POLICY");
        message.setText(Constants.PASSWORDPOLICY);
        heading.setBackgroundColor(getResources().getColor(R.color.grey));
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }


}
