package com.ceesolutions.samba.accessControl.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ceesolutions.samba.accessControl.Fragments.CNICFragment;
import com.ceesolutions.samba.accessControl.Fragments.NTNFragment;
import com.ceesolutions.samba.accessControl.Fragments.PassportFragment;
import com.ceesolutions.samba.accessControl.Fragments.SCNICFragment;

/**
 * Created by ayaz on 1/5/18.
 */

public class FragmentAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public FragmentAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                CNICFragment cnicFragment = new CNICFragment();
                return cnicFragment;
            case 1:

                SCNICFragment scnicFragment = new SCNICFragment();
                return scnicFragment;

            case 2:
                NTNFragment ntnFragment = new NTNFragment();
                return ntnFragment;
            case 3:
                PassportFragment passportFragment = new PassportFragment();
                return passportFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}