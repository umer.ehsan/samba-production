package com.ceesolutions.samba.accessControl;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ceesolutions.samba.R;

/**
 * Created by ceeayaz on 2/23/18.
 */

public class SecurityRecommmendationScreen extends AppCompatActivity {

    private TextView doneBtn, headingTxt, subHeading;
    private ImageView backBtn;
    private TextView text;
    private String string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.disclaimer);
        initViews();
        getSupportActionBar().hide();

        Intent intent = getIntent();

        string = intent.getStringExtra("Login");

        if (!TextUtils.isEmpty(string) && string.equals("es")) {


            headingTxt.setText("E-Statement Terms & Condition");
            text.setText("I hereby instruct and authorize the Bank to send my E-Statement by email .\n" +
                    "\n" +
                    "Information relating to my access to the \"E-Statement Services\" be sent to my email address, and I agree that the risk of non-receipt and/ disclosure of the Security Codes / my account information to third parties shall be borne by me.\n" +
                    "\n" +
                    "In consideration of my instruction for account statement(s) and advice(s) to be sent to the e-mail address specified at the requested frequency, any existing instruction/s to hold mail or to send all statements and advice by post or courier services are cancelled. I agree that all statements or advices sent (by the Bank) shall be deemed to be accurate and correct unless I give written notice to the Bank of any inaccuracies or errors, so reflected on the statement or advice within fifteen (15) days of the date of the statement or advice. I hereby expressly waive any legal rights, past or future, that I may have against the Bank for providing \"E-Statement Services\" and fully accept the risks and responsibilities associated with the statements and advices being transmitted electronically. I acknowledge that the Bank would not be held liable against any external factors affecting the privacy and/ or security of e-mails during Internet transmission. The Bank shall also not be liable or responsible for data corruption, delay, interception and unauthorized amendment of information transmitted electronically from time to time and at any time.\n" +
                    "\n" +
                    "I further agree to keep the Bank indemnified against all actions, proceedings, liabilities and claims, cases, damages, costs and expenses in relation to or arising out of the Bank accepting my request and transmitting statements and information through electronic means. I agree to pay all fees and charges which the Bank may impose from time to time pertaining to \"E-Statement Services\". I warrant that all the information provided by me, at the time of applying for the service is correct, accurate and absolute in all aspects.\n" +
                    "\n" +
                    "I hereby confirm that any change in my email address or any request for discontinuation of this facility will be immediately informed by me to the Bank Customer Service through phone banking. The Account holder shall also inform the Bank in writing, in case of change in the email address or if the account holder does not want the statement of account to be sent by email. Failure to receive any Service due to change of email address not being communicated to the Bank will not excuse me from claiming non-receipt of the Statements. The Bank shall not be liable or responsible for data corruption, delay, interception and unauthorized amendment of the information so given and the Bank also reserves it’s right to update and vary such information from time to time and at any time.\n" +
                    "\n" +
                    "This agreement is in addition to and not in substitution for any other agreements, mandates, terms and conditions relating to the customer’s account(s) with the Bank.\n" +
                    "\n" +
                    "I, do, hereby confirm having read the above Terms & Conditions and be bound by them.");

            subHeading.setText("E-Statement Terms & Condition");

        }

        if (!TextUtils.isEmpty(string) && string.equals("login")) {

            int mainTxt = getResources().getDimensionPixelSize(R.dimen._4sdp);
            subHeading.setGravity(Gravity.CENTER);
            subHeading.setTextSize(mainTxt);
            subHeading.setTextColor(getResources().getColor(R.color.colorPrimary));
            subHeading.setText("SambaSmart Mobile Banking App is both convenient and secure, just while using it, be sure to practice good, safe behaviors and keep track of your mobile devices.");
            text.setText("1. Protect your mobile device with security and tracking software.\n\n" +
                    "2. Never leave the Handset unattended while you are using SambaSmart. Set PINs and passwords that lock your mobile phone devices.\n\n" +
                    "3. Never give your Handset to anyone for using SambaSmart.\n\n" +
                    "4. Be sure that you are using only Samba Bank’s Official App – SambaSmart.\n\n" +
                    "5. Don’t forget to install updates for your device operating system and SambaSmart \n\n" +
                    "6. Avoid installing untrusted Applications or Games on your phone which has SambaSmart installed in it\n\n" +
                    "7. Don’t keep your financial information in your mobile phone, like PIN, passwords etc.\n\n" +
                    "8. Don’t fall for Phishing bait, SMS messages are used to lure users and ask login in unwanted links\n\n" +
                    "9. Avoid risky Wi-Fi available in public places, specially while using Samba Mobile's App \n\n" +
                    "10. Enable Wi-Fi and Bluetooth when you need to, and disable soon after it\n\n" +
                    "11. If you receive any suspicious emails or texts, always inform Samba Bank’s Phone Banking immediately on +92 21 11 11 SAMBA (72622)");

//            subHeading.setText("E-Statement Terms & Condition");

        }

        if(TextUtils.isEmpty(string)) {
            int mainTxt = getResources().getDimensionPixelSize(R.dimen._4sdp);
            subHeading.setTextSize(mainTxt);
            subHeading.setText("Security Guide");
            subHeading.setVisibility(View.GONE);
            headingTxt.setText("Security Guide");
            subHeading.setTextColor(getResources().getColor(R.color.colorPrimary));
            String txt = "<font COLOR=\'#0064b2\'><b>" + "Protect your mobile device with security and tracking software " + "</b><br></font>"
                    + "<font COLOR=\'#73808a\'><br>" + "Install mobile security software and keep it updated." + "</br>" +
                    "<br><br>                Don’t forget to install updates for your device operating system and SambaSmart App as they become available.</br></br>" +
                    " <br><br>                Never give your Handset to anyone for using the SambaSmart. In case of any breach due to this act the Bank shall not be liable for any loss and/or damage which may consequently occur.</br></br>" +
                    " <br><br>                You’re more vulnerable to evolving threats if you have outdated versions of apps and software.</br></br>" +
                    "                " + "</font>"
                    + "<font COLOR=\'#0064b2\'><b><br><br>" + "Back up your data so that you can recover it if necessary. " + "</b><br><br></font>"
                    + "<font COLOR=\'#0064b2\'><b>" + "Get a theft tracker for your mobile device, so that if it’s lost or stolen you can locate it via GPS, remotely lock it and wipe your data. " + "</b><br><br></font>" +
                    "<font COLOR=\'#0064b2\'><b>" + "Set PINs and passwords " + "</b><br></font>"
                    + "<font COLOR=\'#73808a\'><br>" + "Customers can login to SambaSmart Mobile App through their User ID, Password and iPIN, whereas for customer convenience, Finger Print authentication has also been provided." + "</br>" +
                    "<br><br>                Protect your phone/tablet with a strong password and set up a SIM card PIN so that it can’t be used in another device.</br></br>" +
                    " <br><br>                Make sure you don’t use the same numbers as your ATM PIN.</br></br>" +
                    " <br><br>                Use strong passwords for your bank account login & regularly change them.</br></br>" +
                    " <br><br>                Do not disclose your mobile banking password and iPIN to anyone.</br></br>" +
                    "                " + "</font>"
                    + "<font COLOR=\'#0064b2\'><b><br><br>" + "Protecting Personal Information on Mobile Phone " + "</b><br></font>"
                    + "<font COLOR=\'#73808a\'><br>" + "Don’t store account log-in details, any passwords or account numbers on your mobile device. Keep them safely stored elsewhere." + "</br>" +
                    "<br><br>                Don’t store any sensitive personal data on your device.</br></br>" +
                    " <br><br>                If you’re recycling your phone or passing it on to someone, make sure you delete all such personal information first.</br></br>" +
                    "</font>" +
                    "<font COLOR=\'#0064b2\'><b><br><br>" + "Use only Samba Bank’s official Mobile Banking App " + "</b><br></font>"
                    + "<font COLOR=\'#73808a\'><br>" + "SambaSmart Mobile App should be downloaded from Google Play or Apple Store, not from any other third party stores or websites, this may pose a serious threat to your mobile banking account." + "</br>" +
                    "<br><br>                Using Samba Bank’s app avoids the risk of you logging on to fake sites.</br></br>" +
                    " <br><br>                Verify that a banking app is official Samba Bank’s App before you download and install it.</br></br>" +
                    " <br><br>                If you have any doubts, check with Samba Bank’s Phone Banking on +92 21 11 11 SAMBA (72622).</br></br>" +
                    "</font>" +
                    "<font COLOR=\'#0064b2\'><b><br><br>" + "Don’t fall for phishing bait " + "</b><br></font>"
                    + "<font COLOR=\'#73808a\'><br>" + "Beware of unsolicited texts or emails asking you to disclose your secure PIN, passwords or user names as they will probably be phishing attempts." + "</br>" +
                    "<br><br>                Samba Bank’s emails will always be sent through \"Samba.com.pk\" domain.</br></br>" +
                    "</font>"
                    + "<font COLOR=\'#0064b2\'><b><br><br>" + "Don’t click on dubious links in emails or texts" + "</b><br></font>"
                    + "<font COLOR=\'#0064b2\'><b><br>" + "Never send financial information by unencrypted email" + "</b><br></font>"
                    + "<font COLOR=\'#0064b2\'><b><br>" + "Remember to log out of the app or mobile site when you’ve finished" + "</b><br></font>"
                    + "<font COLOR=\'#0064b2\'><b><br>" + "Avoid risky Wi-Fi" + "</b><br></font>"
                    + "<font COLOR=\'#73808a\'><br>" + "Don’t carry out sensitive and financial transactions using public Wi-Fi or unknown networks. " + "</br>" +
                    "<br><br>                The open nature of public networks makes them vulnerable. You never know who may be digging around and watching what you’re doing online.</br></br>" +
                    "<br><br>                Make sure you use a secure connection when making any financial transaction or communicating with your bank.</br></br>" +
                    "</font>" + "<font COLOR=\'#0064b2\'><b><br><br>" + "It’s good practice anyway to turn off Wi-Fi and Bluetooth when you’re not using them" + "</b><br></font>"
                    + "<font COLOR=\'#73808a\'><br>" + "You can get around these issues by disabling Wi-Fi on your device and using your cell network instead." + "</br>" +
                    "</font>";
            text.setText(Html.fromHtml(txt));

        }
    }

    public void initViews() {

        doneBtn = (TextView) findViewById(R.id.nextBtn);
        headingTxt = (TextView) findViewById(R.id.headingTxt);
        subHeading = (TextView) findViewById(R.id.subHeading);
        text = (TextView) findViewById(R.id.text);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        Intent intent = getIntent();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!TextUtils.isEmpty(string) && string.equals("login")) {

                    doneBtn.setEnabled(false);
                    backBtn.setEnabled(false);
//                    Intent intent = new Intent(SecurityRecommmendationScreen.this, LoginScreen.class);
//                    startActivity(intent);
                    finish();

                } else if (!TextUtils.isEmpty(string) && string.equals("es")) {

                    doneBtn.setEnabled(false);
                    backBtn.setEnabled(false);
                    headingTxt.setText("E-Statement Terms & Condition");
//                    Intent intent = new Intent(SecurityRecommmendationScreen.this, LoginScreen.class);
//                    startActivity(intent);
                    finish();

                } else {

                    doneBtn.setEnabled(false);
                    backBtn.setEnabled(false);
                    Intent intent = new Intent(SecurityRecommmendationScreen.this, LoginScreen.class);
                    startActivity(intent);
                    finish();

                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (!TextUtils.isEmpty(string) && string.equals("login")) {

            doneBtn.setEnabled(false);
            backBtn.setEnabled(false);
//                    Intent intent = new Intent(SecurityRecommmendationScreen.this, LoginScreen.class);
//                    startActivity(intent);
            finish();

        } else if (!TextUtils.isEmpty(string) && string.equals("es")) {

            doneBtn.setEnabled(false);
            backBtn.setEnabled(false);
            headingTxt.setText("E-Statement Terms & Condition");
//                    Intent intent = new Intent(SecurityRecommmendationScreen.this, LoginScreen.class);
//                    startActivity(intent);
            finish();

        } else {

            doneBtn.setEnabled(false);
            backBtn.setEnabled(false);
            Intent intent = new Intent(SecurityRecommmendationScreen.this, LoginScreen.class);
            startActivity(intent);
            finish();

        }
    }
}
