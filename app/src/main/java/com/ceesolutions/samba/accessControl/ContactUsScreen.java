package com.ceesolutions.samba.accessControl;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ceesolutions.samba.R;
import com.google.firebase.analytics.FirebaseAnalytics;

import net.cachapa.expandablelayout.ExpandableLayout;

/**
 * Created by ayaz on 2/1/18.
 */

public class ContactUsScreen extends AppCompatActivity {

    private ExpandableLayout expandableLayout0;
    private ExpandableLayout expandableLayout1;
    private ExpandableLayout expandableLayout2;
    private ExpandableLayout expandableLayout3;
    private ExpandableLayout expandableLayout4;
    private RelativeLayout firstRow, secondRow, thirdRow, fourthRow, fifthRow;
    private ImageView backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us_screen);
        initViews();
        getSupportActionBar().hide();

    }

    public void initViews() {

        // new log
        FirebaseAnalytics.getInstance(ContactUsScreen.this).logEvent("ContactUs_Pressed", new Bundle());

        firstRow = (RelativeLayout) findViewById(R.id.firstRow);
        firstRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.expand();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
            }
        });
        secondRow = (RelativeLayout) findViewById(R.id.secondRow);
        secondRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.expand();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
            }
        });
        thirdRow = (RelativeLayout) findViewById(R.id.thirdRow);
        thirdRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.expand();
                expandableLayout3.collapse();
            }
        });
        fourthRow = (RelativeLayout) findViewById(R.id.fourthRow);
        fourthRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.collapse();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.expand();
            }
        });
        fifthRow = (RelativeLayout) findViewById(R.id.fifthRow);
        fifthRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout4.expand();
                expandableLayout0.collapse();
                expandableLayout1.collapse();
                expandableLayout2.collapse();
                expandableLayout3.collapse();
            }
        });
        expandableLayout0 = (ExpandableLayout) findViewById(R.id.expandable_layout_0);
        expandableLayout1 = (ExpandableLayout) findViewById(R.id.expandable_layout_1);
        expandableLayout2 = (ExpandableLayout) findViewById(R.id.expandable_layout_2);
        expandableLayout3 = (ExpandableLayout) findViewById(R.id.expandable_layout_3);
        expandableLayout4 = (ExpandableLayout) findViewById(R.id.expandable_layout_4);

//        expandableLayout0.setOnExpansionUpdateListener(new ExpandableLayout.OnExpansionUpdateListener() {
//            @Override
//            public void onExpansionUpdate(float expansionFraction, int state) {
//
//
//            }
//
//        });
//
//        expandableLayout1.setOnExpansionUpdateListener(new ExpandableLayout.OnExpansionUpdateListener() {
//            @Override
//            public void onExpansionUpdate(float expansionFraction, int state) {
//                Log.d("ExpandableLayout1", "State: " + state);
//            }
//        });


        backBtn = (ImageView) findViewById(R.id.backBtn);


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                backBtn.setEnabled(false);
                finish();
            }
        });


    }

    @Override
    public void onBackPressed() {
        backBtn.setEnabled(false);
        finish();
    }


}
