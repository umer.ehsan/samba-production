package com.ceesolutions.samba.accessControl.Model;

import proguard.annotation.Keep;

/**
 * Created by ceeayaz on 4/30/18.
 */
@Keep
public class Message {

    private String keyword;
    private String message;
    private String target;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
