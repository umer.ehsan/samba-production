package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputLayout;

import androidx.percentlayout.widget.PercentRelativeLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;

import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by ayaz on 12/21/17.
 */

public class RegistrationScreen extends AppCompatActivity {

    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private TextView validateBtn;
    private EditText editIdentity;
    private Spinner identitiesSpinner, userSpinner;
    private String identitySelected;
    private String[] identities = {
            "CNIC",
            "NICOP",
            "NTN",
            "PASSPORT",
            "POC"

    };

    private String[] nonSambaIdentities = {
            "CNIC",
            "NICOP"

    };

    private String userType;
    private String[] userSelection = new String[2];
    private String identityAnswer, isFP;
    private TextView errorMessage;
    private TextInputLayout identityTxtInput;
    private boolean isUserSelection = false;
    private String userSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reg_fragment_screen);
        getSupportActionBar().hide();
        initViews();


        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


        editIdentity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                identityAnswer = editable.toString().trim();

            }
        });

        identityAnswer = editIdentity.getText().toString().trim();

        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (userType.equalsIgnoreCase("Samba")) {
                    FirebaseAnalytics.getInstance(RegistrationScreen.this).logEvent("Samba_Details_Entered_Registration", new Bundle());

                } else {
                    FirebaseAnalytics.getInstance(RegistrationScreen.this).logEvent("Non_Samba_Details_Entered_Registration", new Bundle());

                }
                FirebaseAnalytics.getInstance(RegistrationScreen.this).logEvent("Details_Entered_For_Registration", new Bundle());

                identityAnswer = editIdentity.getText().toString().trim();

                if (!TextUtils.isEmpty(userSelected) && !userSelected.equals("Select Customer Type")) {


                    if (TextUtils.isEmpty(identityAnswer)) {
                        if (identitySelected.equals("CNIC")) {
                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("Please enter valid CNIC number");

                        } else if (identitySelected.equals("PASSPORT")) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("Please enter valid Passport number");

                        } else if (identitySelected.equals("NTN")) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("Please enter valid NTN number");

                        } else if (identitySelected.equals("POC")) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("Please enter valid POC number");

                        } else if (identitySelected.equals("NICOP")) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("Please enter valid NICOP number");

                        }

                    } else if (!helper.validateInputForSC(identityAnswer) || identityAnswer.contains(" ")) {

                        if (identitySelected.equals("CNIC")) {
                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("CNIC number cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                        } else if (identitySelected.equals("PASSPORT")) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("Passport number cannot contains <,>,\",',%,(,),&,+,\\,~,#,*,space");

                        } else if (identitySelected.equals("NTN")) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("NTN number cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                        } else if (identitySelected.equals("POC")) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("POC number cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                        } else if (identitySelected.equals("NICOP")) {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("NICOP number cannot contains <,>,\",',%,(,),&,+,\\,~,space");

                        } else {

                            errorMessage.setVisibility(View.VISIBLE);
                            errorMessage.bringToFront();
                            errorMessage.setError("");
                            errorMessage.setText("Identity value cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                        }

                    } else {

                        if (identitySelected.equals("CNIC")) {

                            if (identityAnswer.length() != 13) {

                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.bringToFront();
                                errorMessage.setError("");
                                errorMessage.setText("Please enter valid CNIC number");
                            } else {

                                validateBtn.setEnabled(false);
//                    Registration();


                                CheckForRegistration();

                            }

                        } else if (identitySelected.equals("PASSPORT")) {


                            if (identityAnswer.length() < 1) {

                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.bringToFront();
                                errorMessage.setError("");
                                errorMessage.setText("Please enter valid Passport number");
                            } else if (identityAnswer.contains("*") || identityAnswer.contains("#") || identityAnswer.contains("/")) {

                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.bringToFront();
                                errorMessage.setError("");
                                errorMessage.setText("Please enter valid Passport number");
                            } else {

                                validateBtn.setEnabled(false);
//                    Registration();


                                CheckForRegistration();

                            }

                        } else if (identitySelected.equals("NTN")) {

                            if (identityAnswer.length() < 1) {

                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.bringToFront();
                                errorMessage.setError("");
                                errorMessage.setText("Please enter valid NTN number");
                            } else {

                                validateBtn.setEnabled(false);
//                    Registration();


                                CheckForRegistration();

                            }


                        } else if (identitySelected.equals("POC")) {

                            if (identityAnswer.length() < 1) {

                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.bringToFront();
                                errorMessage.setError("");
                                errorMessage.setText("Please enter valid POC number");

                            } else {

                                validateBtn.setEnabled(false);
//                    Registration();


                                CheckForRegistration();

                            }

                        } else if (identitySelected.equals("NICOP")) {

                            if (identityAnswer.length() != 13) {

                                errorMessage.setVisibility(View.VISIBLE);
                                errorMessage.bringToFront();
                                errorMessage.setError("");
                                errorMessage.setText("Please enter valid NICOP number");


                            } else {

                                validateBtn.setEnabled(false);
//                    Registration();


                                CheckForRegistration();

                            }

                        }


                    }

//                Intent intent = new Intent(getActivity(), UserValidationScreen.class);
//                intent.putExtra("cnic", cnic);
//                intent.putExtra("identityType", "CNIC");
//                startActivity(intent);
//                getActivity().finish();

                }
            }
        });


    }

    public void initViews() {

        String txt = "<font COLOR=\'#737373\'>" + "I " + "</font>"
                + "<font COLOR=\'#d81e00\'><b>" + "DO NOT " + "</b></font>" + "<font COLOR=\'#737373\'>" + "have a Samba account";

        userSelection[0] = "I have a Samba account";
        userSelection[1] = String.valueOf(Html.fromHtml(txt));
        validateBtn = (TextView) findViewById(R.id.validateBtn);
        editIdentity = (EditText) findViewById(R.id.editIdentity);
        identityTxtInput = (TextInputLayout) findViewById(R.id.identityTxtInput);
        errorMessage = (TextView) findViewById(R.id.errorMessage);
        identitiesSpinner = (Spinner) findViewById(R.id.identitiesSpinner);
        userSpinner = (Spinner) findViewById(R.id.userSpinner);
        requestQueue = Volley.newRequestQueue(RegistrationScreen.this);
        webService = new WebService();
        helper = Helper.getHelper(RegistrationScreen.this);
        constants = Constants.getConstants(RegistrationScreen.this);
        pd = new Dialog(RegistrationScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Intent intent = getIntent();
        isFP = intent.getStringExtra("FP");
        if (!isUserSelection) {
            spinnerAdapter userSelectionAdapter = new spinnerAdapter(RegistrationScreen.this, R.layout.custom_textview_grey);
            userSelectionAdapter.addAll(userSelection);
            userSelectionAdapter.add("Select Customer Type");
            userSpinner.setAdapter(userSelectionAdapter);
            userSpinner.setSelection(userSelectionAdapter.getCount());
            userSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    try {


                        // Get select item
                        if (userSpinner.getSelectedItem() == "Select Customer Type") {
                            userSelected = userSpinner.getSelectedItem().toString();
                            Log.d("identity", "---" + userSpinner);


                        } else {

                            userSelected = userSpinner.getSelectedItem().toString();

                            if (userSelected.equals("I have a Samba account")) {

                                userType = "Samba";
                                editIdentity.setText("");
                                identitiesSpinner.setVisibility(View.VISIBLE);
                                spinnerAdapter questionAdapter = new spinnerAdapter(RegistrationScreen.this, R.layout.custom_textview_grey);
                                questionAdapter.addAll(identities);
                                questionAdapter.add("Identity Type");
                                identitiesSpinner.setAdapter(questionAdapter);
                                identitiesSpinner.setSelection(questionAdapter.getCount());
                                identitiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view,
                                                               int position, long id) {
                                        try {


                                            // Get select item
                                            if (identitiesSpinner.getSelectedItem() == "Identity Type") {
                                                identitySelected = identitiesSpinner.getSelectedItem().toString();
                                                Log.d("identity", "---" + identitiesSpinner);

                                            } else {

                                                identitySelected = identitiesSpinner.getSelectedItem().toString();

                                                errorMessage.setVisibility(View.INVISIBLE);
                                                editIdentity.requestFocus();
                                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                                imm.showSoftInput(editIdentity, InputMethodManager.SHOW_IMPLICIT);

                                                if (identitySelected.equals("CNIC")) {


                                                    editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                                                    editIdentity.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
                                                    identityTxtInput.setVisibility(View.VISIBLE);
                                                    identityTxtInput.setHint("CNIC Number");
                                                    editIdentity.setText("");
                                                    int maxLengthofEditText = 13;
                                                    editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});

                                                } else if (identitySelected.equals("PASSPORT")) {


                                                    editIdentity.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                                                    identityTxtInput.setVisibility(View.VISIBLE);
                                                    identityTxtInput.setHint("Passport Number");
                                                    editIdentity.setHint("");
                                                    editIdentity.setText("");
                                                    int maxLengthofEditText = 15;
                                                    editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});

                                                } else if (identitySelected.equals("NTN")) {

                                                    //&#8208; (Replace Hyphen with this)
                                                    editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                                                    editIdentity.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
                                                    identityTxtInput.setVisibility(View.VISIBLE);
                                                    identityTxtInput.setHint("NTN Number");
                                                    editIdentity.setHint("");
                                                    editIdentity.setText("");
                                                    int maxLengthofEditText = 13;
                                                    editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});


                                                } else if (identitySelected.equals("POC")) {


                                                    editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                                                    identityTxtInput.setVisibility(View.VISIBLE);
                                                    identityTxtInput.setHint("POC Number");
                                                    editIdentity.setHint("");
                                                    editIdentity.setText("");
                                                    int maxLengthofEditText = 13;
                                                    editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});

                                                } else if (identitySelected.equals("NICOP")) {


                                                    editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                                                    editIdentity.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
                                                    identityTxtInput.setVisibility(View.VISIBLE);
                                                    identityTxtInput.setHint("NICOP Number");
                                                    editIdentity.setHint("");
                                                    editIdentity.setText("");
                                                    int maxLengthofEditText = 13;
                                                    editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});

                                                }

                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                        // TODO Auto-generated method stub
                                    }
                                });
                            } else {

                                userType = "NonSamba";
                                editIdentity.setText("");
                                identitiesSpinner.setVisibility(View.VISIBLE);
                                spinnerAdapter questionAdapter = new spinnerAdapter(RegistrationScreen.this, R.layout.custom_textview_grey);
                                questionAdapter.addAll(nonSambaIdentities);
                                questionAdapter.add("Identity Type");
                                identitiesSpinner.setAdapter(questionAdapter);
                                identitiesSpinner.setSelection(questionAdapter.getCount());
                                identitiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view,
                                                               int position, long id) {
                                        try {


                                            // Get select item
                                            if (identitiesSpinner.getSelectedItem() == "Identity Type") {
                                                identitySelected = identitiesSpinner.getSelectedItem().toString();
                                                Log.d("identity", "---" + identitiesSpinner);

                                            } else {

                                                identitySelected = identitiesSpinner.getSelectedItem().toString();

                                                errorMessage.setVisibility(View.INVISIBLE);
                                                editIdentity.requestFocus();
                                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                                imm.showSoftInput(editIdentity, InputMethodManager.SHOW_IMPLICIT);

                                                if (identitySelected.equals("CNIC")) {


                                                    editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                                                    editIdentity.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
                                                    identityTxtInput.setVisibility(View.VISIBLE);
                                                    identityTxtInput.setHint("CNIC Number");
                                                    editIdentity.setText("");
                                                    int maxLengthofEditText = 13;
                                                    editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});

                                                } else if (identitySelected.equals("PASSPORT")) {


                                                    editIdentity.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                                                    identityTxtInput.setVisibility(View.VISIBLE);
                                                    identityTxtInput.setHint("Passport Number");
                                                    editIdentity.setHint("");
                                                    editIdentity.setText("");
                                                    int maxLengthofEditText = 15;
                                                    editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});

                                                } else if (identitySelected.equals("NTN")) {

                                                    //&#8208; (Replace Hyphen with this)
                                                    editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                                                    editIdentity.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
                                                    identityTxtInput.setVisibility(View.VISIBLE);
                                                    identityTxtInput.setHint("NTN Number");
                                                    editIdentity.setHint("");
                                                    editIdentity.setText("");
                                                    int maxLengthofEditText = 13;
                                                    editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});


                                                } else if (identitySelected.equals("POC")) {


                                                    editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                                                    identityTxtInput.setVisibility(View.VISIBLE);
                                                    identityTxtInput.setHint("POC Number");
                                                    editIdentity.setHint("");
                                                    editIdentity.setText("");
                                                    int maxLengthofEditText = 13;
                                                    editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});

                                                } else if (identitySelected.equals("NICOP")) {


                                                    editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                                                    editIdentity.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
                                                    identityTxtInput.setVisibility(View.VISIBLE);
                                                    identityTxtInput.setHint("NICOP Number");
                                                    editIdentity.setHint("");
                                                    editIdentity.setText("");
                                                    int maxLengthofEditText = 13;
                                                    editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});

                                                }

                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }


                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                        // TODO Auto-generated method stub
                                    }
                                });

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub
                }
            });
        }
//        spinnerAdapter questionAdapter = new spinnerAdapter(RegistrationScreen.this, R.layout.custom_textview_grey);
//        questionAdapter.addAll(identities);
//        questionAdapter.add("Identity Type");
//        identitiesSpinner.setAdapter(questionAdapter);
//        identitiesSpinner.setSelection(questionAdapter.getCount());
//        identitiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//                try {
//
//
//                    // Get select item
//                    if (identitiesSpinner.getSelectedItem() == "Identity Type") {
//                        identitySelected = identitiesSpinner.getSelectedItem().toString();
//                        Log.d("identity", "---" + identitiesSpinner);
//
//                    } else {
//
//                        identitySelected = identitiesSpinner.getSelectedItem().toString();
//
//                        errorMessage.setVisibility(View.INVISIBLE);
//                        editIdentity.requestFocus();
//                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                        imm.showSoftInput(editIdentity, InputMethodManager.SHOW_IMPLICIT);
//
//                        if (identitySelected.equals("CNIC")) {
//
//
//                            editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
//                            editIdentity.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
//                            identityTxtInput.setVisibility(View.VISIBLE);
//                            identityTxtInput.setHint("CNIC Number");
//                            editIdentity.setText("");
//                            int maxLengthofEditText = 13;
//                            editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});
//
//                        } else if (identitySelected.equals("PASSPORT")) {
//
//
//                            editIdentity.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
//                            identityTxtInput.setVisibility(View.VISIBLE);
//                            identityTxtInput.setHint("Passport Number");
//                            editIdentity.setHint("");
//                            editIdentity.setText("");
//                            int maxLengthofEditText = 15;
//                            editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});
//
//                        } else if (identitySelected.equals("NTN")) {
//
//                            //&#8208; (Replace Hyphen with this)
//                            editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
//                            editIdentity.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
//                            identityTxtInput.setVisibility(View.VISIBLE);
//                            identityTxtInput.setHint("NTN Number");
//                            editIdentity.setHint("");
//                            editIdentity.setText("");
//                            int maxLengthofEditText = 13;
//                            editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});
//
//
//                        } else if (identitySelected.equals("POC")) {
//
//
//                            editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
//                            identityTxtInput.setVisibility(View.VISIBLE);
//                            identityTxtInput.setHint("POC Number");
//                            editIdentity.setHint("");
//                            editIdentity.setText("");
//                            int maxLengthofEditText = 13;
//                            editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});
//
//                        } else if (identitySelected.equals("NICOP")) {
//
//
//                            editIdentity.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
//                            editIdentity.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
//                            identityTxtInput.setVisibility(View.VISIBLE);
//                            identityTxtInput.setHint("NICOP Number");
//                            editIdentity.setHint("");
//                            editIdentity.setText("");
//                            int maxLengthofEditText = 13;
//                            editIdentity.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});
//
//                        }
//
//                    }
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                // TODO Auto-generated method stub
//            }
//        });

    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RegistrationScreen.this, LoginScreen.class);
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
    }

    public void CheckForRegistration() {

        identityAnswer = editIdentity.getText().toString().trim();
//        if (identitySelected.equals("NTN")) {
//            identityAnswer = identityAnswer.replaceAll("\\\\-\\W", "&#8208");
//            identityAnswer = identityAnswer.replaceAll("-", "&#8208").trim();
//        } else {
//

//        }
        Intent intent = new Intent(RegistrationScreen.this, RobotScreen.class);
        constants.credentialsEditor.putString("identityValue", identityAnswer);
        constants.credentialsEditor.putString("identityType", identitySelected);
        constants.credentialsEditor.putString("userType", userType);
        constants.credentialsEditor.commit();
        constants.credentialsEditor.apply();
        pd.dismiss();
        editIdentity.setText("");
        RegistrationScreen.this.overridePendingTransition(0, 0);
        startActivity(intent);
        RegistrationScreen.this.finish();
        validateBtn.setEnabled(true);
    }

    @Override
    public void onSupportActionModeStarted(ActionMode mode) {
        super.onSupportActionModeStarted(mode);
        mode.finish();
    }
}