package com.ceesolutions.samba.accessControl.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.Model.Logs;
import com.ceesolutions.samba.transactions.Model.TransactionDetails;
import com.ceesolutions.samba.transactions.TransactionDetailsViewScreen;
import com.ceesolutions.samba.views.RoundImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ceeayaz on 5/25/18.
 */

public class ActivityLogCustomListAdapter extends BaseAdapter {

    ArrayList<Logs> transactionDetails;
    Context context;
    TextView dailyTxt, todayTxt, remainTxt;
    TextView title, cashTxt, beforeTxt, afterTxt;
    ImageView icon;
    View view;
    ArrayList<String> arrayList;


    public ActivityLogCustomListAdapter(ArrayList<Logs> transactionDetailsArrayList, Context mContext) {

        transactionDetails = transactionDetailsArrayList;
        context = mContext;
        arrayList = new ArrayList<>();
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return transactionDetails.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.activity_log_item, parent, false);
        }

        title = (TextView) convertView.findViewById(R.id.title);
        cashTxt = (TextView) convertView.findViewById(R.id.cashTxt);
        view = (View) convertView.findViewById(R.id.view1);
        beforeTxt = (TextView) convertView.findViewById(R.id.beforeTxt);
        afterTxt = (TextView) convertView.findViewById(R.id.afterTxt);

        try {


            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
            SimpleDateFormat formatDate = new SimpleDateFormat("hh:mm a");
//            title.setText(transactionDetails.get(position).getTypeDesc());
            cashTxt.setText(transactionDetails.get(position).getTypeDesc());
            beforeTxt.setText(transactionDetails.get(position).getCountry());
            afterTxt.setText(transactionDetails.get(position).getDeviceName());
            Date date = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", Locale.US).parse(transactionDetails.get(position).getLogDate());
            long timeInMilliseconds = date.getTime();
            String formattedDate = sdf.format(new Date(timeInMilliseconds));
            String formattedTime = formatDate.format(new Date(timeInMilliseconds));
            String[] separated = formattedDate.split("-");
            String txt = "<font COLOR=\'#034ea2\'><Big><b>" + separated[0] + "</Big></b><br></font>"
                    + "<font COLOR=\'#034ea2\'><Medium>" + separated[1] + "'" + separated[2].substring(2, 4) + "</Medium><br></font>"
                    + "<font COLOR=\'#034ea2\'><Medium>" + formattedTime + "</Medium><br></font>";
            title.setText(Html.fromHtml(txt));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

}