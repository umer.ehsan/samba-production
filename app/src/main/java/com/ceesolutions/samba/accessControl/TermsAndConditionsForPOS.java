package com.ceesolutions.samba.accessControl;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.Utils;

public class TermsAndConditionsForPOS extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private TextView doneBtn, headingTxt, headingTxt1, securityHeading, text;
    private ImageView backBtn;
    private String string, isFP, login;
    private Dialog pd, dialog1;
    private RequestQueue requestQueue;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private AppPreferences appPreferences;
    private String location;
    private Intent intent1;
    private RelativeLayout header1, header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.disclaimer);
        initViews();
        getSupportActionBar().hide();

    }

    public void initViews() {

        doneBtn = (TextView) findViewById(R.id.nextBtn);
        headingTxt = (TextView) findViewById(R.id.headingTxt);
        headingTxt1 = (TextView) findViewById(R.id.headingTxt1);
        securityHeading = (TextView) findViewById(R.id.subHeading);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        text = (TextView) findViewById(R.id.text);
        pd = new Dialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        headingTxt.setText("Terms & Conditions");
        headingTxt1.setText("Terms & Conditions");
        securityHeading.setText("Terms & Conditions");
        Intent intent = getIntent();
        intent1 = intent;
        requestQueue = Volley.newRequestQueue(this);
        header = (RelativeLayout) findViewById(R.id.header);
        header1 = (RelativeLayout) findViewById(R.id.header1);
        constants = Constants.getConstants(TermsAndConditionsForPOS.this);
        utils = new Utils(TermsAndConditionsForPOS.this);
        helper = Helper.getHelper(this);
        appPreferences = new AppPreferences(this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");

        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

        securityHeading.setVisibility(View.GONE);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {


                String txt = "<font COLOR=\'#0064b2\'><b>" + "Definitions" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "In this document, the following words and phrases shall have the meanings as set below unless the context indicates otherwise:" + "</br>" +
                        "<br><br>                \"Account(s)\" refers to the Customer’s bank account and/ or Breakfree account and/ or auto loan account and/or personal loan account and/ or any other type of account (each account and collectively all accounts so maintained with Samba Bank Limited which are eligible accounts for operations through the use of SambaSmart/Mobile App.</br></br>" +
                        " <br><br>                \"Account Information\" means information pertaining to the Account(s) maintained by the Customer with the Bank / with other domestic banks.</br></br>" +
                        " <br><br>                \"Affiliate\" means SBL business partners and vendors.</br></br>" +
                        " <br><br>                \"Alerts\" means the Account(s) information provided by SBL to the Customer through the Customer’s email address or mobile phone (based on SMS) generated and sent to the Customer by SBL at the specific  request of the Customer which request shall be made by the Customer using the  SambaSmart/Mobile App Services of SBL.</br></br>" +
                        " <br><br>                \"Bank and SBL\" refers to Samba Bank Limited, which are to be used interchangeably.</br></br>" +
                        " <br><br>                \"Customer(s)\" refers to the SBL Account holders authorized to use SambaSmart/Mobile App. In case of the Customer being a minor, the legal guardian of such minor may be permitted to use SambaSmart/Mobile App subject to necessary approvals and conditions.</br></br>" +
                        " <br><br>                In this document all references to the Customer shall be deemed to include both genders.</br></br>" +
                        " <br><br>                SambaSmart/Mobile App refers to the SambaSmart/Mobile App Service offered by SBL to the Customers, as defined below.</br></br>" +
                        " <br><br>                SambaSmart/Mobile App Services is one of the fastest and the most convenient way to access SBL Accounts on the Mobile phones. Customers can view balances, request funds, request bill payments, add friends, transfer funds, pay bills online and other services as SBL may decide to provide from time to time. The availability/ non-availability of particular services shall be at the sole discretion of SBL.</br></br>" +
                        " <br><br>                \"Payment Instructions\" shall mean any instruction given by the Customer to debit funds via SBL SambaSmart/Mobile App. These may include but not limited to bill payments, purchasing vouchers / mobile top-ups, transferring funds from the Account held by the Customer to accounts held by other approved Customers with SBL or other banks. The Bank may in its sole and exclusive discretion confine this facility only to certain permitted Customers or may extend it from time to time to be generally available to all Customers.</br></br>" +
                        " <br><br>                \"Personal Information\" refers to the information provided by the Customer to SBL.</br></br>" +
                        " <br><br>                \"Samba Digital Banking/Online Banking\" means the internet banking services accessible on the Bank’s website through which the Customer(s) can electronically access a number of services connected with his/her/their Account.</br></br>" +
                        " <br><br>               \"SMS\" means short message services, which includes the storage, routing and delivery of alphanumeric messages over GSM or other telecommunication systems.</br></br>" +
                        " <br><br>                \"Terms\" refer to terms and conditions herein use of the SambaSmart/Mobile App Services.</br></br>" +
                        "                " + "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Applicability of Terms" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "These terms form a contract between the Customer and SBL for SambaSmart/Mobile App. By registering for SambaSmart/Mobile App for the first time, the Customer acknowledges and accepts these Terms. These terms and Conditions are in addition to those which are agreed by the Customer along with the account opening form and Samba Digital Banking/OnlineBanking." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "SambaSmart/Mobile App Service" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "The SambaSmart/Mobile App can only be used on Apple IOS and Android OS devices (smartphones and tablets/IPads) that have WIFI or GPRS services enabled.  The Bank may at its sole discretion require Customers to register the device for SambaSmart/Mobile App services." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "SambaSmart/Mobile App Accessibility" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "The Customer irrevocably and unconditionally undertakes to ensure that the password is kept strictly confidential and to not let any person have access to the mobile device while the Customer is accessing the SambaSmart/Mobile App." + "</br>" +
                        "<font COLOR=\'#73808a\'><br><br>" + "The Customer agrees and acknowledges that SBL shall in no way be held responsible or liable if the Customer incurs any loss, expenses, costs, damages, claims (including claims from any third party) as a result of use of the SambaSmart/Mobile App log by the Customer nor shall  SBL  be liable for information being  disclosed by SBL regarding his Account(s) as provided and required to be disclosed by law or disclosed to any party with the Customer’s consent pursuant to the access  of the SambaSmart/Mobile App and the Customer shall fully  indemnify and hold SBL harmless  in respect  of the above." + "</br></font>" +
                        "<font COLOR=\'#73808a\'><br><br>" + "The Customer shall maintain the secrecy of all information of confidential nature and shall ensure that the same is not disclosed to any person voluntarily, accidentally or by mistake." + "</br></font>" +
                        "<font COLOR=\'#73808a\'><br><br>" + "The Customer accepts and undertakes that it is the Customer’s responsibility to ensure that the security of the Customer’s mobile device and details of beneficiaries or transactions or any other relevant information registered by the Customer are not compromised. The Customer undertakes not to hold SBL liable or responsible for any loss or damage that may be suffered by the Customer due to transactions through the SambaSmart/Mobile App and shall keep SBL indemnified against all such losses or damages." + "</br></font>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Unauthorized Access" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "The Customer shall take all necessary precautions to prevent unauthorized and illegal use of SambaSmart/Mobile App and unauthorized access to his Accounts, or any incidental information thereto, accessed through SambaSmart/Mobile App by refraining from sharing Account information." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Customer maybe required to change his password on a frequent basis. The Customer understands and acknowledges that SBL will provide the authentic version of SambaSmart/Mobile App on its website for download and shall not be responsible for any consequences arising out of download of the SambaSmart/Mobile App application by the Customer from any third party application store." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Maintenance of Sufficient Balance" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "The Customer shall ensure that there are sufficient funds at all material times in the Account for transactions to take effect through the SambaSmart/Mobile App, and SBL shall not be liable for any consequences arising out of the Customer failure to ensure adequacy of funds." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Funds Transfer through SambaSmart/Mobile App" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "SBL shall use its best efforts to effect funds transfer transaction received through SambaSmart/Mobile App subject to availability of sufficient funds in the Account and subject to any laws of Pakistan in force at the time." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "SBL shall specify and update from time to time the limit for carrying out the various kinds of funds transfer or bill payment facilities available to the customer through the SambaSmart/Mobile App. The said facility(ies) will be provided in accordance with the arrangement between SBL and the Customers and as per conditions specified by SBL from time to time. SBL shall not be liable for any omission to make all or any of the payments or for late payments for whatsoever cause howsoever arising." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Accuracy of Information" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "The Customer is responsible for the correctness and accuracy of any information supplied to SBL by the Customer for use of SambaSmart/Mobile App. SBL accepts no liability for any consequences whether arising out of erroneous information or the accuracy, reliability or completeness of information supplied by the Customer or otherwise." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "If the Customer notices an error in the information supplied to SBL either in the registration form or any other communication, he shall immediately intimate SBL in writing." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Instructions" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "All instructions for operating SambaSmart/Mobile App shall be given through the internet by the Customer in the manner indicated by SBL." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Customer shall be solely responsible for the accuracy and authenticity of the Payment Instruction provided to SBL and/or its Affiliates and the same shall be considered to be sufficient to operate / use the SambaSmart/Mobile App." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "SBL shall not be required to independently verify the Payment Instruction; an instruction is effective unless countermanded by further instructions. SBL shall have no liability if it does not or is unable to stop or prevent the implementation of any instruction." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "Where SBL considers the instructions to be inconsistent or contradictory it may seek clarification from the Customer before acting on any instruction of the Customer or act upon any such instruction as it deems fit." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "SBL may refuse to comply with the instructions without assigning any reason and shall not be under any duty to assess the prudence or otherwise of any instruction and have the right to suspend the operations through the SambaSmart/Mobile App if it has reason to believe that the Customer’s instructions will lead or expose to direct or indirect loss or may require an indemnity from the Customer before continuing to operate/ use the SambaSmart/Mobile App." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "In pursuit to comply with applicable laws and regulations, SBL may intercept and investigate any payment messages and other information or communications sent to or by the Customer or on the Customer’s behalf through other bank. This process may involve making future inquiries." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Customer shall be free to give Payment Instruction for transfer of funds for such purpose as he/she shall deem fit. The Customer however agrees not to use or permit the Payment Instruction or any related services for any illegal or improper purposes." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "For this, the Customer ensures that:" + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "He has full right, power and/or authority to access and avail the services obtained and the goods purchased and shall observe and comply with all applicable laws and regulations in each jurisdiction in applicable territories." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "He shall provide SBL such information and / or assistance as is required by SBL for the performance of the services and /or any other obligations of SBL under these Terms." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "He shall not at any time provide to any person, with any details of the accounts held by him with SBL including, the passwords, Account  number, card numbers and PIN which may be assigned to him by SBL from time to time." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "He shall have access to and use the SambaSmart/Mobile App from a secure mobile device and that any compromise of the Customer Account resulting from an attempt to access it from an unsecure/ compromised device, will be the sole responsibility of the Customer." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "RISKS" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "The Customer hereby acknowledges that he utilizes SambaSmart/Mobile App Services at his own risk. These risks may include, but not limited to the following:" + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Customer acknowledges that in case any third person obtains access to the Account, relevant information and the registered device, such third person would be able to instruct fund transfers and provide Payment Instructions." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Internet is susceptible to a number of frauds, misuse, hacking and other actions that could affect Payment Instructions to SBL. Whilst SBL shall aim to provide security to prevent the same, there cannot be any guarantee from such Internet frauds, hacking and other actions that could affect Payment instructions to SBL. The Customer separately indemnifies SBL against all risks arising out of the same." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The transfer of funds to third party accounts shall require proper, accurate and complete details. The Customer shall be required to fill in the account number of the person to whom the funds are to be transferred. In the event of any inaccuracy in this regard, the funds may be transferred to incorrect accounts. In such an event, SBL shall not be liable for any loss or damage caused." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The transaction(s) for transfer of funds as per Customer’s instruction may not be completed for some reasons. In such cases, the Customer shall not hold SBL responsible in any manner whatsoever in the said transaction(s) and contracts and the Customer’s sole recourse in this regard shall be with the beneficiary of the transaction." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The technology for enabling the transfer of funds and other services offered by SBL could be effected by virus or other malicious, destructive or corrupting code, program or macro. It may also be possible that the system may require maintenance and during such time it may not be able to process the request of the Customer. This could result in delays in the processing of instructions or failure in the processing of instructions and other such failures and inability. In such cases, the Customer shall not hold SBL responsible in any manner whatsoever in the said transaction(s). " + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Customer understands that SBL disclaims all and any liability, whether direct or indirect, whether arising out of loss of profit or otherwise arising out of any failure or inability by SBL to honor any Customer instruction for whatsoever reason. The Customer understands and accepts that SBL shall not be responsible for any of the aforesaid risks and SBL shall disclaim all liability in respect of the said risks." + "</br>" + "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Authority to SBL for SambaSmart/Mobile App" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "The Customer irrevocably and unconditionally authorizes SBL to access all his Account(s) for effecting banking or other transactions performed by the Customer through the SambaSmart/Mobile App. The right to access shall also include the right at SBL’s sole discretion to consolidate or merge any or all accounts of the Customer with SBL and the right to set off any amounts owing to SBL without prior notice." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The instructions of the Customer shall be effected only after authentication of the Customer in accordance with the prescribed procedure for SambaSmart/Mobile App by SBL or any one authorized by SBL. SBL shall have no obligation to verify the authenticity of any transaction received from the user other than by login/password." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The email confirmation, if any, that is received by the Customer at the time of operation of the SambaSmart/Mobile App by the Customer shall be accepted as conclusive and binding for all purposes." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "All the records of SBL generated by the transactions arising out of the use of the SambaSmart/Mobile App, including the time the transaction recorded shall be conclusive proof of the genuineness and accuracy of the transaction." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "While SBL and the Affiliates shall endeavor to carry out the instructions promptly, they shall not be responsible for any delay in carrying on the instructions due to any reason whatsoever, including due to failure of operational systems or any requirement of law." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Liability of the Customer" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "Neither SBL nor the Affiliates shall be liable for any unauthorized transactions occurring  through SambaSmart/Mobile App and the Customer hereby full indemnifies and holds SBL and its Affiliates harmless against any action, suit, proceeding initiated against it or any loss, costs or damages incurred by the customer as a result thereof." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "SBL shall under no circumstance be held liable to the Customer if the SambaSmart/Mobile App is not available in the desired manner for reasons including but not limited to natural calamities, legal restraints, faults in the telecommunication network or network failure, or any other reason beyond the control of SBL. Under no circumstances shall SBL be liable for any damages whatsoever, whether such damages are direct, indirect, incidental, consequential and irrespective of whether any claim is based on loss of revenue, interruption of business or any loss of any character or nature whatsoever and whether sustained by the Customer or by any other person." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Customer is liable to keep his phone and access to the SambaSmart/Mobile App secure and therefore do not jailbreak or root his phone, which is the process of removing software restrictions and limitations imposed by the official operating system of the device. Such activity can make the phone vulnerable to malware/viruses/malicious programs, compromise your phone’s security features and it could mean that the SBL SambaSmart/Mobile App will not work properly or at all. Illegal or improper use of the SambaSmart/Mobile App shall render the Customer liable for payment of financial charges as decided by SBL at its sole discretion and /or will result in suspension of the Customer’s operations through SambaSmart/Mobile App." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Customer undertakes to comply with all applicable laws and regulations governing the account of the Customer. For the avoidance of doubt, the governing law is the substantive and procedural law of the Islamic Republic of Pakistan." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Protecting against unauthorized logons" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "The SambaSmart/Mobile App allows the Customer to optionally use the fingerprints he has stored on his device to logon to the SambaSmart/Mobile App Services." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "Once the Customer has nominated how he will logon, he will not be necessarily required to enter his login ID and password into the SambaSmart/Mobile App. He will only be required to use biometric verification to logon." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "Where the customer has opted to enable biometric login, any of the fingerprints he has stored on his device can logon and can authorize any transactions in SambaSmart/Mobile App Services. Customer should ensure that only his fingerprint/s are stored on his device. When he logs on using biometric login, he instructs the Bank to perform the transactions." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "He can delete any of the fingerprints he has stored on his device at any time by going into his device settings." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "We also recommend that he setup a password on his device itself, so as to prevent unauthorized access to his device and the App." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Customer agrees that he will: " + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "Not leave the device unattended after logging on to the SambaSmart/Mobile App;" + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "Lock his device or take other steps necessary to stop unauthorized use of the device and SambaSmart/Mobile App;" + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "Not act fraudulently or maliciously in relation to the App or software e.g. you will not copy, modify, adversely affect, reverse engineer, hack into or insert malicious code into the SambaSmart/Mobile App or its software;" + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "Let us know immediately by calling SambaPhone Banking at (9221)11-11-72622(SAMBA); in case of any unusual/ unauthorized transaction is noticed." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Charges" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "The Customer hereby agrees to bear the charges as may be stipulated by SBL from time to time for availing the SambaSmart/Mobile App Services." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Customer shall be clearly notified of the charges through schedule of charges of the Bank or through information posted on the website of the Bank or through emails sent to the Customer’s registered email address with the bank." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Customer hereby authorizes SBL to recover the service charge by debiting one of the Accounts of the Customer or by sending a bill to the Customer who will be liable to make the payment within a specified period. Failure to do so shall result in recovery of the service charge by SBL in any manner as may deem fit along with such interest, if any, and / or withdrawal of the SambaSmart/Mobile App without any liability to SBL." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Applicability to Future Accounts" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "SBL and the Customer agree that if the Customer opens further Accounts with SBL and/ or subscribes to any of the products /services of SBL or any of the Affiliates, and SBL extends SambaSmart/Mobile App to such Accounts or products or services and the Customer opts for use thereof, then these Terms shall apply to the further use of SambaSmart/Mobile App by the Customer." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Indemnity" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "In consideration of SBL providing SambaSmart/Mobile App to the Customer, the Customer shall indemnify and hold SBL and /or SBL’s Affiliates, as the case may be, including both their officers, employees and agents, indemnified against all losses and expenses on full indemnity basis which SBL may incur, sustain , suffer or is likely to suffer in connection with SBL or Affiliates execution  of the Customer’s instructions and against all action, claims , demands, proceedings, losses, damages, costs, charges and expenses as a consequence or by reason of providing a service through SambaSmart/Mobile App for any action taken or omitted  to be taken by SBL and/ or the Affiliates, its officers, employees or agents, on the instructions of the Customer." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Customer will pay SBL and or its Affiliates such amount as may be determined by SBL at its sole discretion to be sufficient to indemnify SBL against any loss or expenses even though they may not have arisen or are contingent in nature." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Customer shall take all necessary precautions to ensure that there are no mistakes and errors and that the information given, instructed or provided to SBL is error free, accurate, proper, complete and up to date at all points of time. On the other hand in the event of the Customer’s Account receiving an incorrect credit by reason of a mistake committed by any other person, SBL shall be entitled to reserve the incorrect credit at any time whatsoever without the prior consent of the Customer. The Customer shall be liable and responsible to Bank and accede to accept the Bank’s instructions without questions for any unfair or unjust gain obtained by the Customer as a result of the same." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Disclosure of information" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "The Customer accepts and agrees that SBL and /or its Affiliates or their contractors may hold and process his personal information and all other information concerning his Account(s) on computer or otherwise in connection with SambaSmart/Mobile App as well as for analysis, credit scoring and marketing pursuant to being instructed by the Customer in writing and as provided under applicable law." + "</br>" +
                        "</font>" + "<font COLOR=\'#73808a\'><br><br>" + "The Customer also agrees that SBL may disclose, as provided under current Pakistan Law, to other institutions personal information as may be reasonably necessary for reason inclusive of but not limited to participation in any telecommunication or electronic clearing network, in compliance with a legal directive, for credit rating by recognized credit scoring agencies, for fund prevention purposes." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Change of Terms" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "SBL shall have absolute discretion to amend or supplement or delete any of the Terms contained herein at any time and will endeavor to give prior notice of thirty days such changes. Such change to the Terms shall be communicated to the Customer through its website/branch notice board or through email/sms sent to the Customer’s registered email address and mobile number. By continuing to use any existing or new services as may be introduced by SBL, the Customer shall be deemed to have accepted the amended Terms." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Non-Transferability" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "The grant of SambaSmart/Mobile App to a Customer is purely personal in nature and not transferable under any circumstance and shall be used only by the customer." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Notices" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "SBL may publish notices of general nature, which are applicable to all Customers in newspapers or on its website or through emails sent to Customers registered email address. Such notices will have the same effect as a notice served individually to each Customer." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "General" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "All costs incurred by the user, including but not limited to any telecommunication costs to use SambaSmart/Mobile App, would be borne by the Customer." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Assignment" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "SBL shall be entitled to sell, assign or transfer SBL’s right and obligations under the Terms and any security in favor of SBL (including all guarantees/s) to any person of SBL’s choice in whole or in part and in such manner and on such terms and conditions as SBL may decide. Any such sale, assignment or transfer shall conclusively bind the Customer and all other persons." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#73808a\'><br>" + "The Customers, his heirs, legal representatives, executors, administrators and successors are bound by the Terms and the Customer shall not be entitled to transfer or assign any of his rights and obligations." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Governing Law and Jurisdiction" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "The Terms will be construed and enforced in accordance with, and the rights of the parties hereto will be governed by, the law of the Islamic Republic of Pakistan. Any and all disputes arising under the Terms, whether as to interpretation, performance or otherwise, will be subject to the exclusive jurisdiction of the courts at Karachi, Pakistan  and each of the parties hereto hereby irrevocably attorns to the exclusive  jurisdiction of such courts." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#73808a\'><br><br>" + "The Parties hereby agree that any legal action or proceedings arising out of the Terms for SambaSmart/Mobile App shall be brought in the courts and to irrevocably submit themselves to the jurisdiction of such courts." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#73808a\'><br><br>" + "SBL may, however, in its absolute discretion, commence any legal action or proceedings arising out of the Terms for SambaSmart/Mobile App in any other court, tribunal or other appropriate forum, and the Customer hereby consents to that jurisdiction." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#73808a\'><br><br>" + "Any provision of the Terms for SambaSmart/Mobile App which is prohibited or unenforceable in any jurisdiction shall, as to such jurisdiction, be ineffective to the extent of prohibition or unenforceability but shall not invalidate the remaining the Customer shall not be entitled to transfer or assign any of his rights and obligations." + "</br>" +
                        "</font>"
                        + "<font COLOR=\'#0064b2\'><b><br><br>" + "Termination" + "</b><br></font>"
                        + "<font COLOR=\'#73808a\'><br>" + "SBL reserves the right to terminate the facility of the SambaSmart/Mobile App, either partially or in totality, at any time whatsoever, without prior notice. SBL also reserves the right at any time without prior notice to add/ alter/ modify/ change or vary all of these Terms and Conditions." + "</br>" +
                        "</font>";
                text.setText(Html.fromHtml(txt));
                if ((!TextUtils.isEmpty(string) && string.equals("signup")) || (!TextUtils.isEmpty(isFP) && isFP.equals("FP")) || (!TextUtils.isEmpty(login) && login.equals("login"))) {

                    doneBtn.setVisibility(View.VISIBLE);

                }


            }

        }, 500);


        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();


            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
    }


    @Override
    public void onBackPressed() {


        backBtn.setEnabled(false);
        finish();
    }
}