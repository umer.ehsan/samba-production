package com.ceesolutions.samba.accessControl;

/**
 * Created by ceeayaz on 2/26/18.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.ceesolutions.samba.eatMubarak.EatMubarakMainScreen;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.Bookme.BookMeMainScreen;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.Model.Channels;
import com.ceesolutions.samba.accessControl.Model.Message;
import com.ceesolutions.samba.beneficiariesManagement.ManageBeneficiaresScreen;
import com.ceesolutions.samba.beneficiariesManagement.Model.UserModel;
import com.ceesolutions.samba.beneficiariesManagement.SelectBankScreen;
import com.ceesolutions.samba.billPayments.ManageBillerScreen;
import com.ceesolutions.samba.billPayments.Model.Biller;
import com.ceesolutions.samba.billPayments.PayBillsScreen;
import com.ceesolutions.samba.billPayments.SelectCategoryScreen;
import com.ceesolutions.samba.cardManagement.ManageCardsScreen;
import com.ceesolutions.samba.chatMessenger.Adapter.ChatMessageAdapter;
import com.ceesolutions.samba.chatMessenger.Model.ChatMessage;
import com.ceesolutions.samba.friends.ImportFromAddressBook;
import com.ceesolutions.samba.friends.ManageFriends;
import com.ceesolutions.samba.masterCardIntegration.MainActivity;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.transactions.AccountsScreen;
import com.ceesolutions.samba.transactions.FundTransferScreen;
import com.ceesolutions.samba.transactions.ManageAccountsScreen;
import com.ceesolutions.samba.transactions.Model.Account;
import com.ceesolutions.samba.transactions.Model.TransactionDetails;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.BuilderManager;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.TapTarget;
import com.ceesolutions.samba.utils.TapTargetSequence;
import com.ceesolutions.samba.utils.TapTargetView;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.HorizontalListView;
import com.ceesolutions.samba.views.RoundImageView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.SimpleCircleButton;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.OnBoomListenerAdapter;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;
//import com.techworks.eatmubaraklibrary.EatMubarakWidget;
//import com.techworks.eatmubaraklibrary.UserInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

/**
 * Created by ayaz on 1/9/18.
 */

public class HomeScreen extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    public static final String TAG = "Tozny";
    public static double latitude;
    public static double longitude;
    private static boolean PASSWORD_BASED_KEY = true;
    private static String EXAMPLE_PASSWORD = "0123456789";
    public ArrayList<String> stringArrayList, purposesList;
    public CustomHorizontalListAdapter horizontalListAdapter;
    public AccoutsCustomHorizontalListAdapter accoutsCustomHorizontalListAdapter;
    public DestinationAccountHorizontalListAdapter destinationAccountHorizontalListAdapter;
    public AccountsForBillerCustomHorizontalListAdapter accountsForBillerCustomHorizontalListAdapter;
    public BillListAdapter billListAdapter;
    public boolean isMini = false;
    public boolean isFt = false;
    public String isType;
    public String ftType;
    public boolean isSource = false;
    public ProgressBar progress;
    public String longitude1;
    public String latitude1;
    private HashMap<String, String> billerDistributorList;
    private String[] purpose;
    private ArrayList<String> purpStringArrayList, purpPrepaidStringArrayList, purpBillStringArrayList, channelStringArrayList;
    private HashMap<String, String> channelsArrayList;
    private RecyclerView mRecyclerView;
    private Button mButtonSend;
    private EditText mEditTextMessage, editText;
    private ImageView shareBtn, sambaBtn, doneBtn, cancelBtn, cancelBtnConfirmation, canceBtnTag;
    private ChatMessageAdapter mAdapter;
    private RelativeLayout relativeLayout, relativeLayout2, tagshorizontalListViewLayout, horizontalListViewLayout, confirmationLayout;
    private HorizontalListView listView, accountListView;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Constants constants;
    private TextView sambaHeader, progressTxt, confirmBtn, cancel, logout, headerTxt;
    private Dialog pd;
    private Utils utils;
    private ArrayList<Account> accounts;
    private ArrayList<Message> messages;
    private ArrayList<UserModel> userModels;
    private ArrayList<UserModel> userModelsWithinSamba;
    private ArrayList<TransactionDetails> transactionDetailsList;
    private ArrayList<String> sameDate;
    private BoomMenuButton bmb;
    private Dialog dialog;
    private TextView heading, btn, textView, notiText;
    private String menu;
    private NavigationView navigationView;
    private Menu navMenu;
    private String list, list1, list2, list3, list4, list5;
    private DrawerLayout drawer;
    private int count = 0;
    private boolean amount = false, isShown = false;
    private String notiCount, userType, sourceTile, sourceNumber, sourceAmount, sourceCurrency, sourceBranchCode, destinationTitle, destinationAccount, destionationCurrency, remarks, totalAmount, destinationBankCode, destinationCurrency, destinationBal, branchCode, bankCode, beneType, beneID, email, mobile, sourceBankCode, destinationAlias,SourceProductType,DestProductType;
    private LinkedHashMap<Integer, LinkedHashMap<String, Integer>> chatHashMap;
    private boolean doubleBackToExitPressedOnce = false;
    private String nonSamba = "This service is only avaliable for Samba Customers only.";
    private String accountType;
    private ArrayList<Biller> billers;
    private boolean isPay = false;
    private int color;
    private long timeInMilliseconds = 0, currentTimeToMili = 0;
    private String payableAmount, amountAfterDueDate, amountBeforeDueDate;
    private AppPreferences appPreferences;
    private String location;
    private String sorry = "Oops! Service is not responding. Please try again later.";
    private String address;
    private LinkedHashMap<String, Integer> unsortedHashMap;
    private String encryptedMsg = "";
    private String password = "123456789";
    private String message = "This is signs test string";
    private boolean isBill = false;
    private ImageView helpBtn;
    private String coachMarks, userName;
    private CryptLib _crypt;
    private String isFirstLogin;
    public boolean isSeen = false;
    List<Address> addresses;
    private int intentCode = 101;

    BroadcastReceiver analyticsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBundleExtra("EVENT_LOGIN") != null) {
                String userName = intent.getBundleExtra("EVENT_LOGIN")
                        .getString("UserName");
                String userPhone = intent.getBundleExtra("EVENT_LOGIN")
                        .getString("UserPhone");
                String timeStamp = intent.getBundleExtra("EVENT_LOGIN")
                        .getString("TimeStamp");
                //Add your code...
            } else if (intent.getBundleExtra("EVENT_BEGINCHECKOUT") != null) {
                String restName = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("RestName");
                String branchAddress = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("BranchAddress");
                String userName = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("UserName");
                String userPhone = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("UserPhone");
                String subtotal = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("Subtotal");
                String timeStamp = intent.getBundleExtra("EVENT_BEGINCHECKOUT")
                        .getString("TimeStamp");
                //Add your code...
            } else if (intent.getBundleExtra("EVENT_RESTAURANTVIEWED") != null) {
                String restName = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("RestName");
                String branchAddress = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("BranchAddress");
                String userName = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("UserName");
                String userPhone = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("UserPhone");
                String timeStamp = intent.getBundleExtra("EVENT_RESTAURANTVIEWED")
                        .getString("TimeStamp");
                //Add your code...
            }
        }
    };


    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        View view = findViewById(R.id.message);
        helpBtn = (ImageView) toolbar.findViewById(R.id.helpBtn);
        constants = Constants.getConstants(HomeScreen.this);
        Log.d("----", "---" + constants.sharedPreferences.getString("userName", "N/A"));
        try {

            String img = constants.sharedPreferences.getString("ProfilePic", "N/A");
            Log.d("img--", "-" + img);
            _crypt = new CryptLib();
            String imgg = _crypt.decrypt(constants.sharedPreferences.getString("ProfilePic", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            Log.d("img1--", "-" + imgg);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initViews(view, toolbar);
        Handler handler4 = new Handler();


        //            AesCbcWithIntegrity.SecretKeys keys = AesCbcWithIntegrity.generateKey();
//            AesCbcWithIntegrity.CipherTextIvMac cipherTextIvMac = AesCbcWithIntegrity.encrypt("This is signs test string", keys);
//            //store or send to server
//            String ciphertextString = cipherTextIvMac.toString();
//            Log.d("encrypt",ciphertextString);
//            AesCbcWithIntegrity.CipherTextIvMac cipherTextIvMac2 = new AesCbcWithIntegrity.CipherTextIvMac(ciphertextString);
//            String plainText = AesCbcWithIntegrity.decryptString(cipherTextIvMac2, keys);
//            Log.d("decrypt",plainText);
//
//            String src = "This is signs test string";
//            String encrypted = AESUtil.encrypt(src);
//            String decrypted = AESUtil.decrypt(encrypted);
//            System.out.println("src: " + src);
//            System.out.println("encrypted: " + encrypted);
//            System.out.println("decrypted: " + decrypted);
//            String encData= encrypt("0123000000000215".getBytes("UTF-16LE"), ("This is signs test string").getBytes("UTF-16LE"));
//           Log.d("encrypted1: ","" + encData);
//            String decData= decrypt("0123000000000215",Base64.decode(encData.getBytes("UTF-16LE"), Base64.DEFAULT));
//            Log.d("decrypted2: ","" + decData);
//            byte[] theByteArray = message.getBytes();
////            String str = new String(encrypt(theByteArray,"OFRna73m*aze01xY",keyGenerator()), "UTF-8");
//            String base64 = Base64.encodeToString(encrypt(theByteArray,"0000000000000000",keyGenerator()), Base64.DEFAULT);
//            Log.d("enc--",""+base64);
//            byte[] theByteArray1 = base64.getBytes();
//            String str1 = new String(encrypt(theByteArray1,"OFRna73m*aze01xY",keyGenerator()), "UTF-8");
//            Log.d("dec--",""+str1);
//
//            String key = "123456789";
//
//            //iv string must be max 16 char
//            String iv = "OFRna73m*aze01xY";
//
//            //message string must be utf-8
//
//
//            //ecnrypted string
//            String encrypted3 = Crypto.encrypts(message,key,iv);
//
//            Log.d("Crypto",encrypted);
//
//            //decrypted string
//            String decrypted3 = Crypto.decrypts(encrypted3,key,iv);
//
//            Log.d("Crypto",decrypted);
//
//            try {
//                CryptLib _crypt = new CryptLib();
//                String output= "";
//                String enc1 = AESHelper.encrypt(message);
//                Log.d("en-","--"+enc1);
//                String dec = AESHelper.decrypt(enc1);
//                Log.d("dec-","--"+dec);
//                try {
//                    CryptLib _crypt1 = new CryptLib();
//                    String output1= "";
//                    String plainText1 = "This is the text to be encrypted.";
//                    String key1 = "12345678912345678912345678912345";//32 bytes = 256 bit
//                    String iv1 = "0000000000000000"; //16 bytes = 128 bit
//                    output = _crypt1.encrypt(message, key1, iv1); //encrypt
//                    Log.d("encrypted text1=","" + output);
//                    output1 = _crypt1.decrypt(output, key1,iv1); //decrypt
//                    Log.d("decrypted text1=","" + output1);
//                } catch (Exception e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//                String  cipherText = _crypt.encrypt(message, "123456789","OFRna73m*aze01xY");
//                Log.d("encrypted text=","" + cipherText);
//                String decryptedString = _crypt.decrypt(cipherText, "123456789","OFRna73m*aze01xY");
//                Log.d("decryptedString text=","" + decryptedString);
//
//            } catch (Exception e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//
//
//
//        try {
//            encryptedMsg = AESCrypt.encrypt(password, message);
//            Log.d("encrypt2",encryptedMsg);
//        }catch (GeneralSecurityException e){
//            //handle error
//        }
//
//
//
//        try {
//            String messageAfterDecrypt = AESCrypt.decrypt(password, encryptedMsg);
//            Log.d("decrypt2",messageAfterDecrypt);
//        }catch (GeneralSecurityException e){
//            //handle error - could be due to incorrect password or tampered encryptedMsg
//        }

        try {

//            CryptLib _crypt = new CryptLib();
//            userName = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
//            Log.d("name--", "--" + userName);
            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }

            menu = constants.sharedPreferences.getString("Menus", "N/A");
            if (!TextUtils.isEmpty(menu) && !menu.equals("N/A") && !menu.equals("")) {


            } else {

                menu = "N/A";
            }
            address = _crypt.decrypt(constants.sharedPreferences.getString("UserAddress", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(address) && !address.equals("N/A") && !address.equals("")) {


            } else {

                address = "N/A";
            }
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            navMenu = navigationView.getMenu();
            navigationView.setItemIconTintList(null);
//        navMenu.add(0,R.id.home,0,)
            navigationView.setNavigationItemSelectedListener(this);
//            setMenus();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    setMenus();
                }
            }, 500);


            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            ImageView notificationBtn = (ImageView) toolbar.findViewById(R.id.notificationBtn);

            notiText = (TextView) toolbar.findViewById(R.id.notiText);
            notiText.setVisibility(View.INVISIBLE);
            handler4.postDelayed(new Runnable() {
                public void run() {
                    GetCount();
                }
            }, 100);
            notificationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(HomeScreen.this, MessageCenter.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                    notiText.setVisibility(View.INVISIBLE);
//                finish();
                }
            });


            View headerLayout = navigationView.getHeaderView(0);
            TextView textView = (TextView) headerLayout.findViewById(R.id.profileName);
            ImageView profile = (ImageView) headerLayout.findViewById(R.id.profile_image);


            progress = (ProgressBar) headerLayout.findViewById(R.id.progress);
            progressTxt = (TextView) headerLayout.findViewById(R.id.progressTxt);

            String str = _crypt.decrypt(constants.sharedPreferences.getString("UserProgress", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

            progressTxt.setText(str + "% Profile Completed");

            int pro = Integer.valueOf(str);
            progress.setProgress(pro);

            String image = _crypt.decrypt(constants.sharedPreferences.getString("ProfilePic", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            TextView titleTxt = (TextView) headerLayout.findViewById(R.id.titleTxt);
            RoundImageView titleImage = (RoundImageView) headerLayout.findViewById(R.id.titleImage);
            String T24_FullName = _crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            String s = T24_FullName.substring(0, 1).toUpperCase();
            switch (s.toUpperCase()) {

                case "A":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "B":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "C":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "D":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "E":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "F":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "G":
                    color = getResources().getColor(R.color.friendColor1);
                    break;

                case "H":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "I":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "J":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "K":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "L":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "M":
                    color = getResources().getColor(R.color.friendColor2);
                    break;

                case "N":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "O":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "P":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "Q":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "R":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "S":
                    color = getResources().getColor(R.color.friendColor3);
                    break;

                case "T":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "U":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "V":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "W":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "X":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "Y":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                case "Z":
                    color = getResources().getColor(R.color.friendColor4);
                    break;

                default:
                    color = getResources().getColor(R.color.defaultColor);
                    break;
            }

            try {

                Log.d("---0", "" + image);
                if (!TextUtils.isEmpty(image) && !image.equals("N/A")) {
                    image = image.replaceAll("%2B", "+");
                    titleTxt.setVisibility(View.INVISIBLE);
                    titleImage.setVisibility(View.INVISIBLE);
                    byte[] imageByteArray = Base64.decode(image, Base64.DEFAULT);
                    RequestOptions options = new RequestOptions();
                    options.centerCrop();
                    Glide.with(HomeScreen.this)
                            .load(imageByteArray)
                            .apply(options)
                            .into(profile);
                } else {

                    profile.setVisibility(View.INVISIBLE);
                    titleImage.setBackgroundTintList(ColorStateList.valueOf((color)));
                    titleTxt.setText(s);
                }
            } catch (Exception e) {

                e.printStackTrace();
            }


            ImageView imageView = (ImageView) headerLayout.findViewById(R.id.settings);
            textView.setText(_crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
            RelativeLayout relativeLayout = (RelativeLayout) headerLayout.findViewById(R.id.relativeLayout);
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                    Intent intent = new Intent(HomeScreen.this, EditProfileScreen.class);
                    intent.putExtra("view", "view");
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }
            });

            profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                    Intent intent = new Intent(HomeScreen.this, EditProfileScreen.class);
                    intent.putExtra("view", "edit");
                    overridePendingTransition(0, 0);
                    startActivity(intent);

                }
            });

            titleTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                    Intent intent = new Intent(HomeScreen.this, EditProfileScreen.class);
                    intent.putExtra("view", "edit");
                    overridePendingTransition(0, 0);
                    startActivity(intent);

                }
            });

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                    Intent intent = new Intent(HomeScreen.this, SettingsScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
//                    finish();

                }
            });

            editText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {

                        userMessage();

                    }
                    return false;
                }
            });

            doneBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    userMessage();
                }
            });


            sambaBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    bmb.boom();
                }
            });

            shareBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//
                    try {


                        Intent intent = new Intent("android.intent.action.VIEW");

                        /** creates an sms uri */
                        Uri data = Uri.parse("sms:");

                        /** Setting sms uri to the intent */
                        intent.setData(data);
                        intent.putExtra("sms_body", "Download Samba Mobile App and register yourself. Click https://bit.ly/2KB0nfG for details");

                        /** Initiates the SMS compose screen, because the activity contain ACTION_VIEW and sms uri */
                        startActivity(intent);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//             if (!backPressed) {

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                System.exit(0);
                return;
            } else {
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_LONG).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }

        }
    }

    private void addBuilder() {
        bmb.addBuilder(new SimpleCircleButton.Builder()
                .normalImageRes(BuilderManager.getImageResource())
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
//                        textViewForButton.setText("No." + index + " boom-button is clicked!");
                    }
                }));
    }

    public void initViews(final View view, final Toolbar toolbar) {

        try {


            _crypt = new CryptLib();
            userName = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            Log.d("name--", "--" + userName);

            try {
                FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("userName", userName);

                String fingerPrint = _crypt.decrypt(constants.sharedPreferences.getString("FingerPrintEnabled", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                if (fingerPrint.equals("True")) {
                    FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("FingerPrint", "enabled");
                } else {
                    FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("FingerPrint", "not enabled");
                }

                String user = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                if (!TextUtils.isEmpty(user) && !user.equals("N/A")) {

                    if (user.equals("NON_SAMBA")) {
                        FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("isUser", "NON_SAMBA");
                    } else {
                        FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("isUser", "SAMBA");
                    }
                }

                if (ConfigAPI.URL.contains("mobilebanking.samba")) {
                    FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("isLive", String.valueOf(true));
                } else {
                    FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("isLive", String.valueOf(false));
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            String first = constants.sharedPreferences.getString("Is_FirstLogin", "N/A");
            Log.d("first--", "-" + first);
            if (!TextUtils.isEmpty(first) && !first.equals("null") && !first.equals(null) && !first.equals("N/A"))
                isFirstLogin = _crypt.decrypt(constants.sharedPreferences.getString("Is_FirstLogin", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());


            chatHashMap = new LinkedHashMap<>();
            unsortedHashMap = new LinkedHashMap<>();
            billerDistributorList = new HashMap<>();
            billers = new ArrayList<>();
            messages = new ArrayList<>();
            requestQueue = Volley.newRequestQueue(HomeScreen.this);
            utils = new Utils(HomeScreen.this);
            helper = Helper.getHelper(HomeScreen.this);
            constants = Constants.getConstants(HomeScreen.this);
            pd = new Dialog(HomeScreen.this);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_layout);
            sambaHeader = (TextView) view.findViewById(R.id.samba);
            headerTxt = (TextView) view.findViewById(R.id.headerTxt);
            canceBtnTag = (ImageView) view.findViewById(R.id.cancelBtnTags);
            cancelBtn = (ImageView) view.findViewById(R.id.cancelBtn);

            pd.setCancelable(false);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            purpStringArrayList = new ArrayList<>();
            channelsArrayList = new HashMap<>();
            purpPrepaidStringArrayList = new ArrayList<>();
            purpBillStringArrayList = new ArrayList<>();
            userModelsWithinSamba = new ArrayList<>();
            channelStringArrayList = new ArrayList<>();
            cancel = (TextView) view.findViewById(R.id.cancel);
            confirmationLayout = (RelativeLayout) view.findViewById(R.id.confirmationLayout);
            confirmBtn = (TextView) view.findViewById(R.id.confirmBtn);
            cancelBtnConfirmation = (ImageView) view.findViewById(R.id.cancelBtnConfirmation);
            Handler handler = new Handler();
            bmb = (BoomMenuButton) view.findViewById(R.id.bmb);
            assert bmb != null;
            bmb.setButtonEnum(ButtonEnum.TextOutsideCircle);
            bmb.setBackgroundColor(Color.TRANSPARENT);
            bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_9_1);
            bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_9_1);
            logout = (TextView) findViewById(R.id.logout);
            appPreferences = new AppPreferences(this);
            longitude1 = appPreferences.getString("longitude", "N/A");
            latitude1 = appPreferences.getString("latitude", "N/A");
            location = appPreferences.getString("location", "N/A");
            coachMarks = constants.coachMarks.getString("homeScreen", "N/A");

            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                getAddress(latitude, longitude);
            }


            if (!TextUtils.isEmpty(coachMarks) && coachMarks.equals("N/A")) {
                constants.coachMarksEditor.putString("homeScreen", "1");
                constants.coachMarksEditor.commit();
                constants.coachMarksEditor.apply();
                int mainTxt = getResources().getDimensionPixelSize(R.dimen._7sdp);
                int des = getResources().getDimensionPixelSize(R.dimen._4sdp);
                final TapTargetSequence sequence = new TapTargetSequence(HomeScreen.this)
                        .targets(
                                TapTarget.forView(view.findViewById(R.id.shareBtn), "About this icon!!!\n", "From here you can share SambaSmart Application with your friends.")
                                        .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                        .outerCircleAlpha(0.90f)            // Specify the alpha amount for the outer circle
                                        // Specify a color for the target circle
                                        .titleTextSize(mainTxt)                  // Specify the size (in sp) of the title text
                                        .titleTextColor(R.color.white)      // Specify the color of the title text
                                        .descriptionTextSize(des)            // Specify the size (in sp) of the description text
                                        .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                        .textColor(R.color.white)            // Specify a color for both the title and description text
                                        .textTypeface(Typeface.SANS_SERIF)        // If set, will dim behind the view with 30% opacity of the given color
                                        .drawShadow(true)                   // Whether to draw a drop shadow or not
                                        .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                        .tintTarget(true)                   // Whether to tint the target view's color
                                        .transparentTarget(false),
                                TapTarget.forView(toolbar.findViewById(R.id.helpBtn), "Know this icon!!!", "Tap on this icon whenever you see it on any screen for quick help for that page.")
                                        .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                        .outerCircleAlpha(0.90f)            // Specify the alpha amount for the outer circle
                                        // Specify a color for the target circle
                                        .titleTextSize(mainTxt)                  // Specify the size (in sp) of the title text
                                        .titleTextColor(R.color.white)      // Specify the color of the title text
                                        .descriptionTextSize(des)            // Specify the size (in sp) of the description text
                                        .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                        .textColor(R.color.white)            // Specify a color for both the title and description text
                                        .textTypeface(Typeface.SANS_SERIF)        // If set, will dim behind the view with 30% opacity of the given color
                                        .drawShadow(true)                   // Whether to draw a drop shadow or not
                                        .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                        .tintTarget(true)                   // Whether to tint the target view's color
                                        .transparentTarget(false))
                        .listener(new TapTargetSequence.Listener() {
                            // This listener will tell us when interesting(tm) events happen in regards
                            // to the sequence
                            @Override
                            public void onSequenceFinish() {
                                // Yay
                                String isCheck = constants.isFingerCheck.getString("isCheck", "N/A");
                                if (!TextUtils.isEmpty(isCheck) && isCheck.equals("True")) {
                                    Intent intent = new Intent(HomeScreen.this, FingerPrintNotificationScreen.class);
                                    intent.putExtra("isFirst", getIntent().getStringExtra("isFirst"));
                                    startActivity(intent);
                                } else if (!TextUtils.isEmpty(getIntent().getStringExtra("isFirst")) && getIntent().getStringExtra("isFirst").equals("Yes")) {
                                    GetManageAccountsList();
                                }
//

                            }

                            @Override
                            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                            }


                            @Override
                            public void onSequenceCanceled(TapTarget lastTarget) {
                                // Boo
                            }
                        });


                TapTargetView.showFor(HomeScreen.this,                 // `this` is an Activity
                        TapTarget.forView(view.findViewById(R.id.sambaBtn), "About this icon!!!\n\nSmart menu", "Tap here on the smart Menu to quickly navigate to desired functionality.")
                                // All options below are optional
                                .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                .outerCircleAlpha(0.90f)
                                // Specify the alpha amount for the outer circle
                                // Specify a color for the target circle
                                .titleTextSize(mainTxt)// Specify the size (in sp) of the title text
                                .titleTextColor(R.color.white)      // Specify the color of the title text
                                .descriptionTextSize(des)            // Specify the size (in sp) of the description text
                                .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                .textColor(R.color.white)            // Specify a color for both the title and description text
                                .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                                // If set, will dim behind the view with 30% opacity of the given color
                                .drawShadow(true)                   // Whether to draw a drop shadow or not
                                .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                .tintTarget(true)                   // Whether to tint the target view's color
                                .transparentTarget(false),           // Specify whether the target is transparent (displays the content underneath)
                        // Specify a custom drawable to draw as the target
                        // Specify the target radius (in dp)
                        new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                            @Override
                            public void onTargetClick(TapTargetView view) {
                                super.onTargetClick(view);      // This call is optional
//                        doSomething();
                                sequence.start();
                            }
                        });
            } else if (!TextUtils.isEmpty(getIntent().getStringExtra("isFirst")) && getIntent().getStringExtra("isFirst").equals("Yes")) {

                String isCheck = constants.isFingerCheck.getString("isCheck", "N/A");
                if (!TextUtils.isEmpty(isCheck) && isCheck.equals("True")) {
                    Intent intent = new Intent(HomeScreen.this, FingerPrintNotificationScreen.class);
                    intent.putExtra("isFirst", getIntent().getStringExtra("isFirst"));
                    startActivity(intent);
                } else if (!TextUtils.isEmpty(getIntent().getStringExtra("isFirst")) && getIntent().getStringExtra("isFirst").equals("Yes")) {
                    GetManageAccountsList();
                }
                //                constants.coachMarksEditor.putString("homeScreen", "1");
//                constants.coachMarksEditor.commit();
//                constants.coachMarksEditor.apply();
//                int mainTxt = getResources().getDimensionPixelSize(R.dimen._7sdp);
//                int des = getResources().getDimensionPixelSize(R.dimen._4sdp);
//                final TapTargetSequence sequence = new TapTargetSequence(HomeScreen.this)
//                        .targets(
//                                TapTarget.forView(view.findViewById(R.id.shareBtn), "About this icon!!!\n", "From here you can share SambaSmart Application with your friends.")
//                                        .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
//                                        .outerCircleAlpha(0.90f)            // Specify the alpha amount for the outer circle
//                                        // Specify a color for the target circle
//                                        .titleTextSize(mainTxt)                  // Specify the size (in sp) of the title text
//                                        .titleTextColor(R.color.white)      // Specify the color of the title text
//                                        .descriptionTextSize(des)            // Specify the size (in sp) of the description text
//                                        .descriptionTextColor(R.color.white)  // Specify the color of the description text
//                                        .textColor(R.color.white)            // Specify a color for both the title and description text
//                                        .textTypeface(Typeface.SANS_SERIF)        // If set, will dim behind the view with 30% opacity of the given color
//                                        .drawShadow(true)                   // Whether to draw a drop shadow or not
//                                        .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
//                                        .tintTarget(true)                   // Whether to tint the target view's color
//                                        .transparentTarget(false),
//                                TapTarget.forView(toolbar.findViewById(R.id.helpBtn), "Know this icon!!!", "Tap on this icon whenever you see it on any screen for quick help for that page.")
//                                        .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
//                                        .outerCircleAlpha(0.90f)            // Specify the alpha amount for the outer circle
//                                        // Specify a color for the target circle
//                                        .titleTextSize(mainTxt)                  // Specify the size (in sp) of the title text
//                                        .titleTextColor(R.color.white)      // Specify the color of the title text
//                                        .descriptionTextSize(des)            // Specify the size (in sp) of the description text
//                                        .descriptionTextColor(R.color.white)  // Specify the color of the description text
//                                        .textColor(R.color.white)            // Specify a color for both the title and description text
//                                        .textTypeface(Typeface.SANS_SERIF)        // If set, will dim behind the view with 30% opacity of the given color
//                                        .drawShadow(true)                   // Whether to draw a drop shadow or not
//                                        .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
//                                        .tintTarget(true)                   // Whether to tint the target view's color
//                                        .transparentTarget(false))
//                        .listener(new TapTargetSequence.Listener() {
//                            // This listener will tell us when interesting(tm) events happen in regards
//                            // to the sequence
//                            @Override
//                            public void onSequenceFinish() {
//                                // Yay
//                                String isCheck = constants.isFingerCheck.getString("isCheck", "N/A");
//
//
//                            }
//
//                            @Override
//                            public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {
//
//                            }
//
//
//                            @Override
//                            public void onSequenceCanceled(TapTarget lastTarget) {
//                                // Boo
//                            }
//                        });
//
//
//                TapTargetView.showFor(HomeScreen.this,                 // `this` is an Activity
//                        TapTarget.forView(view.findViewById(R.id.sambaBtn), "About this icon!!!\n\nSmart menu", "Tap here on the smart Menu to quickly navigate to desired functionality.")
//                                // All options below are optional
//                                .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
//                                .outerCircleAlpha(0.90f)
//                                // Specify the alpha amount for the outer circle
//                                // Specify a color for the target circle
//                                .titleTextSize(mainTxt)// Specify the size (in sp) of the title text
//                                .titleTextColor(R.color.white)      // Specify the color of the title text
//                                .descriptionTextSize(des)            // Specify the size (in sp) of the description text
//                                .descriptionTextColor(R.color.white)  // Specify the color of the description text
//                                .textColor(R.color.white)            // Specify a color for both the title and description text
//                                .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
//                                // If set, will dim behind the view with 30% opacity of the given color
//                                .drawShadow(true)                   // Whether to draw a drop shadow or not
//                                .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
//                                .tintTarget(true)                   // Whether to tint the target view's color
//                                .transparentTarget(false),           // Specify whether the target is transparent (displays the content underneath)
//                        // Specify a custom drawable to draw as the target
//                        // Specify the target radius (in dp)
//                        new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
//                            @Override
//                            public void onTargetClick(TapTargetView view) {
//                                super.onTargetClick(view);      // This call is optional
////                        doSomething();
//                                sequence.start();
//                            }
//                        });
            } else {
                String isCheck = constants.isFingerCheck.getString("isCheck", "N/A");
                if (!TextUtils.isEmpty(isCheck) && isCheck.equals("True")) {
                    Intent intent = new Intent(HomeScreen.this, FingerPrintNotificationScreen.class);
                    intent.putExtra("isFirst", getIntent().getStringExtra("isFirst"));
                    startActivity(intent);
                }
            }


            helpBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int mainTxt = getResources().getDimensionPixelSize(R.dimen._7sdp);
                    int des = getResources().getDimensionPixelSize(R.dimen._4sdp);
                    final TapTargetSequence sequence1 = new TapTargetSequence(HomeScreen.this)
                            .targets(
                                    TapTarget.forView(view.findViewById(R.id.shareBtn), "About this icon!!!\n", "From here you can share SambaSmart Application with your friends.")
                                            .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                            .outerCircleAlpha(0.90f)            // Specify the alpha amount for the outer circle
                                            // Specify a color for the target circle
                                            .titleTextSize(mainTxt)                  // Specify the size (in sp) of the title text
                                            .titleTextColor(R.color.white)      // Specify the color of the title text
                                            .descriptionTextSize(des)            // Specify the size (in sp) of the description text
                                            .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                            .textColor(R.color.white)            // Specify a color for both the title and description text
                                            .textTypeface(Typeface.SANS_SERIF)        // If set, will dim behind the view with 30% opacity of the given color
                                            .drawShadow(true)                   // Whether to draw a drop shadow or not
                                            .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                            .tintTarget(true)                   // Whether to tint the target view's color
                                            .transparentTarget(false),
                                    TapTarget.forView(toolbar.findViewById(R.id.helpBtn), "Know this icon!!!", "Tap on this icon whenever you see it on any screen for quick help for that page.")
                                            .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                            .outerCircleAlpha(0.90f)            // Specify the alpha amount for the outer circle
                                            // Specify a color for the target circle
                                            .titleTextSize(mainTxt)                  // Specify the size (in sp) of the title text
                                            .titleTextColor(R.color.white)      // Specify the color of the title text
                                            .descriptionTextSize(des)            // Specify the size (in sp) of the description text
                                            .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                            .textColor(R.color.white)            // Specify a color for both the title and description text
                                            .textTypeface(Typeface.SANS_SERIF)        // If set, will dim behind the view with 30% opacity of the given color
                                            .drawShadow(true)                   // Whether to draw a drop shadow or not
                                            .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                            .tintTarget(true)                   // Whether to tint the target view's color
                                            .transparentTarget(false))
                            .listener(new TapTargetSequence.Listener() {
                                // This listener will tell us when interesting(tm) events happen in regards
                                // to the sequence
                                @Override
                                public void onSequenceFinish() {
                                    // Yay
                                }

                                @Override
                                public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                                }


                                @Override
                                public void onSequenceCanceled(TapTarget lastTarget) {
                                    // Boo
                                }
                            });

                    TapTargetView.showFor(HomeScreen.this,                 // `this` is an Activity
                            TapTarget.forView(view.findViewById(R.id.sambaBtn), "About this icon!!!\n\nSmart menu", "Tap here on the smart Menu to quickly navigate to desired functionality.")
                                    // All options below are optional
                                    .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                    .outerCircleAlpha(0.90f)
                                    // Specify the alpha amount for the outer circle
                                    // Specify a color for the target circle
                                    .titleTextSize(mainTxt)// Specify the size (in sp) of the title text
                                    .titleTextColor(R.color.white)      // Specify the color of the title text
                                    .descriptionTextSize(des)            // Specify the size (in sp) of the description text
                                    .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                    .textColor(R.color.white)            // Specify a color for both the title and description text
                                    .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                                    // If set, will dim behind the view with 30% opacity of the given color
                                    .drawShadow(true)                   // Whether to draw a drop shadow or not
                                    .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                    .tintTarget(true)                   // Whether to tint the target view's color
                                    .transparentTarget(false),           // Specify whether the target is transparent (displays the content underneath)
                            // Specify a custom drawable to draw as the target
                            // Specify the target radius (in dp)
                            new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                                @Override
                                public void onTargetClick(TapTargetView view) {
                                    super.onTargetClick(view);      // This call is optional
//                        doSomething();
                                    sequence1.start();
                                }
                            });
                }
            });
            if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
                longitude = Double.valueOf(longitude1);
                latitude = Double.valueOf(latitude1);
                if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


                } else {
                    location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
                }
            } else {

                longitude = 0;
                latitude = 0;
                location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
            }

            String fingerPrint = _crypt.decrypt(constants.sharedPreferences.getString("FingerPrintEnabled", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
            String fingerPrintUserName = constants.fingerPrint.getString("userName", "N/A");

            if (!TextUtils.isEmpty(fingerPrint) && !fingerPrint.equals("N/A")) {

                if (TextUtils.isEmpty(fingerPrintUserName) || fingerPrintUserName.equals("N/A")) {

                    if (fingerPrint.equals("True")) {

                        SetFingerPrint();
                    }
                } else if (!fingerPrintUserName.equals(_crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()))) {

                    if (fingerPrint.equals("True")) {

                        SetFingerPrint();
                    }

                }
            }

            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    }
                    pd.show();
                    showDialog("Are you sure you want to Logout?");
                }
            });

            bmb.setOnBoomListener(new OnBoomListenerAdapter() {
                @Override
                public void onBoomWillShow() {
                    super.onBoomWillShow();
                    // logic here
                }
            });


            bmb.setOnBoomListener(new OnBoomListener() {
                @Override
                public void onClicked(int index, BoomButton boomButton) {
                    // If you have implement listeners for boom-buttons in builders,
                    // then you shouldn't add any listener here for duplicate callbacks.
                    try {
                        switch (index) {


                            case 0:

                                break;

                            case 1:

                                if (!menu.equals("N/A")) {

                                    if (menu.toLowerCase().contains("accounts")) {
                                        showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                    } else {

                                        Intent intent = new Intent(HomeScreen.this, AccountsScreen.class);
                                        overridePendingTransition(0, 0);
                                        startActivity(intent);
//                                    finish();
                                    }
                                } else {
                                    Intent intent = new Intent(HomeScreen.this, AccountsScreen.class);
                                    overridePendingTransition(0, 0);
                                    startActivity(intent);
//                                finish();

                                }

                                break;

                            case 2:

                                try {


                                    Intent transfers = new Intent(HomeScreen.this, FundTransferScreen.class);
                                    String user = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                    if (!TextUtils.isEmpty(user) && !user.equals("N/A")) {

                                        if (user.equals("NON_SAMBA")) {

                                            transfers.putExtra("requestFunds", "nonSamba");
                                        }
                                    }
                                    overridePendingTransition(0, 0);
                                    startActivity(transfers);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//                            finish();
                                break;


                            case 3:

                                if (!menu.equals("N/A")) {

                                    if (menu.toLowerCase().contains("biller management")) {
                                        showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                    } else {
                                        String user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                        Intent payments = new Intent(HomeScreen.this, PayBillsScreen.class);
                                        overridePendingTransition(0, 0);
                                        user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                        if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                            if (user1.equals("NON_SAMBA")) {

                                                payments.putExtra("requestbill", "nonSamba");
                                            }
                                        }
                                        startActivity(payments);
//                                    finish();
                                    }
                                } else {
                                    Intent payments = new Intent(HomeScreen.this, PayBillsScreen.class);
                                    overridePendingTransition(0, 0);
                                    String user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                    user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                                    if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                        if (user1.equals("NON_SAMBA")) {

                                            payments.putExtra("requestbill", "nonSamba");
                                        }
                                    }
                                    startActivity(payments);
//                                finish();
                                }

                                break;


                            case 4:

                                if (!menu.equals("N/A")) {

                                    if (menu.toLowerCase().contains("beneficiary management")) {
                                        showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                    } else {

                                        if (userType.equals("NON_SAMBA")) {
                                            showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                        } else {
                                            Intent bene = new Intent(HomeScreen.this, ManageBeneficiaresScreen.class);
                                            overridePendingTransition(0, 0);
                                            startActivity(bene);
//                                    finish();
                                        }
                                    }
                                } else {
                                    if (userType.equals("NON_SAMBA")) {
                                        showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                    } else {
                                        Intent bene = new Intent(HomeScreen.this, ManageBeneficiaresScreen.class);
                                        overridePendingTransition(0, 0);
                                        startActivity(bene);
//                                    finish();
                                    }
//                                finish();
                                }

                                break;


                            case 5:
                                if (userType.equals("NON_SAMBA"))
                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                else {
                                    Intent intent = new Intent(HomeScreen.this, MainActivity.class);
                                    overridePendingTransition(0, 0);
                                    startActivity(intent);
                                }


//                                if (!menu.equals("N/A")) {
//
//                                    if (menu.toLowerCase().contains("friends management")) {
//                                        showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                                    } else {
//
//                                        Intent friends = new Intent(HomeScreen.this, ManageFriends.class);
//                                        overridePendingTransition(0, 0);
//                                        startActivity(friends);
////                                    finish();
//                                    }
//                                } else {
//                                    Intent friends = new Intent(HomeScreen.this, ManageFriends.class);
//                                    overridePendingTransition(0, 0);
//                                    startActivity(friends);
////                                finish();
//                                }

                                break;


                            case 6:

                                try {

                                    CryptLib _crypt = new CryptLib();

                                    String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                                    if (userType.equals("NON_SAMBA"))
                                        showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                    else {
                                        Intent intent = new Intent(HomeScreen.this, EatMubarakMainScreen.class);
                                        overridePendingTransition(0, 0);
                                        startActivity(intent);
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                break;



//                                if (!menu.equals("N/A")) {
//
//                                    if (menu.toLowerCase().contains("cards management")) {
//                                        showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                                    } else {
//                                        if (userType.equals("NON_SAMBA")) {
//                                            showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                                        } else {
//
////                                            Intent cards = new Intent(HomeScreen.this, ManageCardsScreen.class);
////                                            overridePendingTransition(0, 0);
////                                            startActivity(cards);
//
//                                            Intent cards = new Intent(HomeScreen.this, EatMubarakMainScreen.class);
//                                            overridePendingTransition(0, 0);
//                                            startActivity(cards);
//
//
////                                    finish();
//                                        }
//
////                                    finish();
//                                    }
//                                } else {
//                                    if (userType.equals("NON_SAMBA")) {
//                                        showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                                    } else {
//
//                                        Intent cards = new Intent(HomeScreen.this, EatMubarakMainScreen.class);
//                                        overridePendingTransition(0, 0);
//                                        startActivity(cards);
//
//
////                                        Intent cards = new Intent(HomeScreen.this, ManageCardsScreen.class);
////                                        overridePendingTransition(0, 0);
////                                        startActivity(cards);
////                                    finish();
//                                    }
////                                finish();
//                                }


                            case 7:
                                if (userType.equals("NON_SAMBA"))
                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
                                else {
                                    Intent atm = new Intent(HomeScreen.this, BookMeMainScreen.class);
                                    overridePendingTransition(0, 0);
                                    startActivity(atm);
                                }

//                        finish();
                                break;

                            case 8:
                                pd.show();
                                showDialog("Are you sure you want to Logout?");
                                break;


                            default: {

                                break;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onBackgroundClick() {
//                textViewForAnimation.setText("Click background!!!");
                }

                @Override
                public void onBoomWillHide() {
                    Log.d("BMB", "onBoomWillHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will RE-BOOM!!!");
                }

                @Override
                public void onBoomDidHide() {
                    Log.d("BMB", "onBoomDidHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did RE-BOOM!!!");
                }

                @Override
                public void onBoomWillShow() {
                    Log.d("BMB", "onBoomWillShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will BOOM!!!");
                }

                @Override
                public void onBoomDidShow() {
                    Log.d("BMB", "onBoomDidShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did BOOM!!!");
                }
            });

            setBoomMenus();
//        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++)
//            bmb.addBuilder(BuilderManager.getSimpleCircleButtonBuilder());


//        bmb.setButtonEnum(ButtonEnum.TextOutsideCircle);
//        bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_1);
//        bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_1);
//        bmb.addBuilder(BuilderManager.getTextOutsideCircleButtonBuilder());
//            list = constants.purposeBill.getString("purposeList", "N/A");
//            list1 = constants.purposeList.getString("purposeList", "N/A");
//            list2 = constants.puposePrepaid.getString("purposeList", "N/A");
//            accountType = constants.accountList.getString("accounts", "N/A");


            stringArrayList = new ArrayList<>();
            accounts = new ArrayList<>();
            userModels = new ArrayList<>();
            transactionDetailsList = new ArrayList<>();
            sameDate = new ArrayList<>();
            stringArrayList.add("Own Accounts");
            stringArrayList.add("Transfer within Samba");
            stringArrayList.add("Other Banks");

            listView = (HorizontalListView) view.findViewById(R.id.tagshorizontalListView);
            accountListView = (HorizontalListView) view.findViewById(R.id.horizontalListView);
            horizontalListAdapter = new CustomHorizontalListAdapter(stringArrayList, HomeScreen.this);

            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    horizontalListViewLayout.setVisibility(View.GONE);
                    relativeLayout2.setVisibility(View.VISIBLE);
                    amount = false;
                    isFt = false;
                    isPay = false;
                    isSource = false;
                    horizontalListAdapter = new CustomHorizontalListAdapter(stringArrayList, HomeScreen.this);
//                    if (isSource) {
//
//
                    Handler handler1 = new Handler();
                    handler1.postDelayed(new Runnable() {
                        public void run() {


                            sendMessage("Is there anything else i can help you with ?", "");
                        }
                    }, 1000);
//                    } else {
//                        Handler handler1 = new Handler();
//                        handler1.postDelayed(new Runnable() {
//                            public void run() {
//
//
//                                sendMessage("Is there anything else i can help you with ?", "");
//                            }
//                        }, 1000);
//
//                    }

                }
            });

            cancelBtnConfirmation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    stringArrayList.add("Own Accounts");
                    stringArrayList.add("Transfer within Samba");
                    stringArrayList.add("Other Banks");
                    horizontalListAdapter = new CustomHorizontalListAdapter(stringArrayList, HomeScreen.this);
                    listView.setAdapter(horizontalListAdapter);
                    editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                    confirmationLayout.setVisibility(View.GONE);
                    relativeLayout2.setVisibility(View.VISIBLE);
                    amount = false;
                    isPay = false;
                    isFt = false;
                    isSource = false;
                    Handler handler1 = new Handler();
                    handler1.postDelayed(new Runnable() {
                        public void run() {


                            sendMessage("Is there anything else i can help you with ?", "");

                        }
                    }, 700);


                }
            });
            confirmBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    amount = false;
                    isFt = false;
                    isSource = false;
//                    isPay = false;
                    stringArrayList.add("Own Accounts");
                    stringArrayList.add("Transfer within Samba");
                    stringArrayList.add("Other Banks");
                    horizontalListAdapter = new CustomHorizontalListAdapter(stringArrayList, HomeScreen.this);
                    editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);

                    Handler handler2 = new Handler();
                    handler2.postDelayed(new Runnable() {
                        public void run() {
//                            sendMessage("Ok. Please wait...", "");
                            confirmationLayout.setVisibility(View.GONE);
                            relativeLayout2.setVisibility(View.VISIBLE);
//                            LatestBalance(sourceNumber);
                        }
                    }, 700);


                    Handler handler3 = new Handler();
                    handler3.postDelayed(new Runnable() {
                        public void run() {

                            if (isPay) {

                                LatestBalance(sourceNumber);

                            } else {
                                LatestBalance(sourceNumber);
                            }
                        }
                    }, 1000);


                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    stringArrayList.add("Own Accounts");
                    stringArrayList.add("Transfer within Samba");
                    stringArrayList.add("Other Banks");
                    horizontalListAdapter = new CustomHorizontalListAdapter(stringArrayList, HomeScreen.this);
                    listView.setAdapter(horizontalListAdapter);
                    editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                    amount = false;
                    isFt = false;
                    isSource = false;
                    isPay = false;
                    confirmationLayout.setVisibility(View.GONE);
                    relativeLayout2.setVisibility(View.VISIBLE);
                    Handler handler1 = new Handler();
                    handler1.postDelayed(new Runnable() {
                        public void run() {


                            sendMessage("Is there anything else i can help you with ?", "");

                        }
                    }, 700);
                }
            });

            canceBtnTag.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    stringArrayList.add("Own Accounts");
                    stringArrayList.add("Transfer within Samba");
                    stringArrayList.add("Other Banks");
                    horizontalListAdapter = new CustomHorizontalListAdapter(stringArrayList, HomeScreen.this);
                    listView.setAdapter(horizontalListAdapter);
                    editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                    amount = false;
                    isFt = false;
                    isSource = false;
                    isPay = false;
                    tagshorizontalListViewLayout.setVisibility(View.GONE);
                    confirmationLayout.setVisibility(View.GONE);
                    relativeLayout2.setVisibility(View.VISIBLE);
                    Handler handler1 = new Handler();
                    handler1.postDelayed(new Runnable() {
                        public void run() {


                            sendMessage("Is there anything else i can help you with ?", "");

                        }
                    }, 700);
                }
            });

            listView.setAdapter(horizontalListAdapter);
            mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
//        mButtonSend = (Button) findViewById(R.id.btn_send);
            mEditTextMessage = (EditText) view.findViewById(R.id.chatBoxText);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.relativeLayout);
            relativeLayout2 = (RelativeLayout) view.findViewById(R.id.relativeLayout2);
            editText = (EditText) view.findViewById(R.id.edittext);
            shareBtn = (ImageView) view.findViewById(R.id.shareBtn);
            sambaBtn = (ImageView) view.findViewById(R.id.sambaBtn);
            tagshorizontalListViewLayout = (RelativeLayout) view.findViewById(R.id.tagshorizontalListViewLayout);
            horizontalListViewLayout = (RelativeLayout) view.findViewById(R.id.horizontalListViewLayout);
            doneBtn = (ImageView) view.findViewById(R.id.doneBtn);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

            mEditTextMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!amount) {
                        relativeLayout.setVisibility(View.VISIBLE);
                        relativeLayout2.setVisibility(View.INVISIBLE);
                        editText.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);

                    } else {

                        relativeLayout.setVisibility(View.VISIBLE);
                        relativeLayout2.setVisibility(View.INVISIBLE);
                        editText.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                        editText.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                        editText.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
                        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(7)});
                    }
                }
            });

            mAdapter = new ChatMessageAdapter(this, new ArrayList<ChatMessage>());
            mRecyclerView.setAdapter(mAdapter);

            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat mdformat = new SimpleDateFormat("kk:mm:ss aa");
            String strDate = mdformat.format(calendar.getTime()).replaceAll("\\.", "").toUpperCase();
            String hour[] = strDate.split(":");
            String amPm = strDate.substring(strDate.length() - 2);
            Integer currentHour = Integer.valueOf(hour[0]);
            String val = "";

            if (currentHour >= 0 && currentHour < 12 && strDate.contains("AM")) {

                val = "Good Morning";

            } else if (currentHour > 0 && currentHour >= 12 && currentHour < 18 && strDate.contains("PM")) {

                val = "Good Afternoon";


            } else if (currentHour > 0 && currentHour >= 18 && strDate.contains("PM")) {

                val = "Good Evening";


            } else
                val = "Good Morning";


            String lasLocation = _crypt.decrypt(constants.sharedPreferences.getString("LastLoginLocation", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());


            if (!TextUtils.isEmpty(isFirstLogin) && !isFirstLogin.equals("N/A") && isFirstLogin.equals("Y")) {


                sendMessage("Hey " + _crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()) + "\n" +

                                val + " , welcome to SambaSmart.\n" +

                                "\n"

                                + "Type HELP and you will get all the information needed!"

                        , "");
            } else {
//            if (!TextUtils.isEmpty(lasLocation) && !lasLocation.equals("N/A") && !lasLocation.equals("-") && !lasLocation.equals("null") && !lasLocation.equals(null)) {
//                sendMessage("Hey " + constants.sharedPreferences.getString("T24_FullName", "N/A") + "\n" +
//                        val + " , welcome back to SIGNS.\n" +
//                        "\n" +
//                        "What can I do for you today?" + "\n\n" + "Your last login was from " + constants.sharedPreferences.getString("LastLoginLocation", "N/A") + ".", "");
//            } else {
//
//                sendMessage("Hey " + constants.sharedPreferences.getString("T24_FullName", "N/A") + "\n" +
//                        val + " , welcome back to SIGNS.\n" +
//                        "\n" +
//                        "What can I do for you today?" + "\n\n" + "Your last login was from " + "Unknown Location" + ".", "");
//            }

                if (!TextUtils.isEmpty(lasLocation) && !lasLocation.equals("N/A") && !lasLocation.equals("-") && !lasLocation.equals("null") && !lasLocation.equals(null) && !lasLocation.equals("")) {

                    sendMessage("Hey " + _crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()) + "\n" +

                                    val + " , welcome back to SambaSmart.\n" +

                                    "\n"

                                    + "Your last login was from " + _crypt.decrypt(constants.sharedPreferences.getString("LastLoginLocation", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()) + ".\n\n"

                                    + "Type HELP and you will get all the information needed!"

                            , "");

                } else {


                    sendMessage("Hey " + _crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()) + "\n" +

                                    val + " , welcome back to SambaSmart.\n" +

                                    "\n" +

                                    "Your last login was from " + "Unrecognized Location" + ".\n\n"

                                    + "Type HELP and you will get all the information needed!"

                            , "");

                }
            }

            new Handler().postDelayed(new Runnable() {
                public void run() {
                    ChatMessages();
                }
            }, 500);

        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        list = constants.purposeBill.getString("purposeList", "N/A");
        list1 = constants.purposeList.getString("purposeList", "N/A");
        list2 = constants.puposePrepaid.getString("purposeList", "N/A");
        list5 = constants.puposePrepaid.getString("purposeListIBFT", "N/A");
        list3 = constants.channelList.getString("limits", "N/A");
        accountType = constants.accountList.getString("accounts", "N/A");
        list4 = constants.billerList.getString("billerListCategoryFor1Bill", "N/A");

        Handler handler1 = new Handler();
        Handler handler2 = new Handler();
        Handler handler3 = new Handler();
        Handler handler4 = new Handler();
        Handler handler5 = new Handler();
        Handler handler6 = new Handler();
        Handler handler7 = new Handler();
        Handler handler8 = new Handler();
        Handler handler9 = new Handler();

        if (accountType.equals("N/A")) {
            handler5.postDelayed(new Runnable() {
                public void run() {
                    GetAccountsList1();
                }
            }, 500);
        }


        handler6.postDelayed(new Runnable() {
            public void run() {
                GetCount();
            }
        }, 500);

        if (list.equals("N/A")) {
            handler2.postDelayed(new Runnable() {
                public void run() {

                    GetPurposeBillList("");
                }
            }, 200);
        }

        if (list1.equals("N/A")) {
            handler3.postDelayed(new Runnable() {
                public void run() {
                    GetPurposeList("");
                }
            }, 200);
        }

        if (list2.equals("N/A")) {
            handler4.postDelayed(new Runnable() {
                public void run() {

                    GetPurposePrepaidList();
                }
            }, 200);
        }

        if (list3.equals("N/A")) {
            handler7.postDelayed(new Runnable() {
                public void run() {

                    GetLimitsList();
                }
            }, 200);
        }


        if (list4.equals("N/A")) {
            handler8.postDelayed(new Runnable() {
                public void run() {

                    GetbillerDistributorList();
                }
            }, 200);
        }

        if (list5.equals("N/A")) {
            handler9.postDelayed(new Runnable() {
                public void run() {

                    GetPurposeListIBFT("");
                }
            }, 200);
        }
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        confirmationLayout.setVisibility(View.GONE);
        tagshorizontalListViewLayout.setVisibility(View.GONE);
        horizontalListViewLayout.setVisibility(View.GONE);
        relativeLayout2.setVisibility(View.VISIBLE);
        amount = false;
        isPay = false;
        isFt = false;
        isSource = false;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.home) {

//            item.setVisible(false);
            // Handle the camera action
        } else if (id == R.id.accounts) {

            Intent intent = new Intent(HomeScreen.this, AccountsScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);
//            finish();

        } else if (id == R.id.qr_payment) {

            if (userType.equals("NON_SAMBA")) {
                showDilogForError("This feature is only available for Samba Users.", "WARNING");
            } else {
                Intent intent = new Intent(HomeScreen.this, MainActivity.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
            }
//            finish();

        } else if (id == R.id.manageBene) {

            if (userType.equals("NON_SAMBA")) {
                showDilogForError("This feature is only available for Samba Users.", "WARNING");
            } else {
                Intent intent1 = new Intent(HomeScreen.this, ManageBeneficiaresScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent1);
//            finish();
            }
        } else if (id == R.id.manageFriends) {

            Intent intent2 = new Intent(HomeScreen.this, ManageFriends.class);
            overridePendingTransition(0, 0);
            startActivity(intent2);
//            finish();

        } else if (id == R.id.billPayments) {

            Intent intent2 = new Intent(HomeScreen.this, ManageBillerScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent2);
//            finish();

        } else if (id == R.id.cardManagement) {

            if (userType.equals("NON_SAMBA")) {
                showDilogForError("This feature is only available for Samba Users.", "WARNING");
            } else {
                Intent intent2 = new Intent(HomeScreen.this, ManageCardsScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent2);
//            finish();
            }


        } else if (id == R.id.feedback) {

            Intent intent2 = new Intent(HomeScreen.this, FeedbackScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent2);
//            finish();
        } else if (id == R.id.contact) {

            Intent intent2 = new Intent(HomeScreen.this, ContactUsScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent2);
//            finish();
        } else if (id == R.id.location) {

            Intent intent2 = new Intent(HomeScreen.this, ATMLocatorScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent2);
//            finish();
        } else if (id == R.id.ticket) {
            if (userType.equals("NON_SAMBA"))
                showDilogForError("This feature is only available for Samba Users.", "WARNING");
            else {
                Intent intent = new Intent(HomeScreen.this, BookMeMainScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);

            }
//            finish();
        }
        else if (id == R.id.food) {
            if (userType.equals("NON_SAMBA"))
                showDilogForError("This feature is only available for Samba Users.", "WARNING");
            else {
                Intent intent = new Intent(HomeScreen.this, EatMubarakMainScreen.class);
                overridePendingTransition(0, 0);
                startActivity(intent);

            }
//            finish();
        }
        else if (id == R.id.qr_payment) {
            if (userType.equals("NON_SAMBA"))
                showDilogForError("This feature is only available for Samba Users.", "WARNING");
            else {
                Intent intent = new Intent(HomeScreen.this, MainActivity.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
            }
//            finish();

        } else if (id == R.id.legal) {
            Intent intent = new Intent(HomeScreen.this, SettingsScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void GetPurposeList(final String chat) {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sPurposeGroupCode", "FT");
                    params.put("sServerSessionKey", md5);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getPuposesURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("00")) {


                                                purpStringArrayList = new ArrayList<>();
                                                purpStringArrayList.clear();
                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("POTList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        purpStringArrayList.add(jsonObj.getString("Name"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();

                                                        if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                                            Error();
                                                        }
                                                    }

                                                }

                                                if (!amount) {
                                                    Gson gson = new Gson();
                                                    String json = gson.toJson(purpStringArrayList);
                                                    constants.purposeListEditor.putString("purposeList", json);
                                                    constants.purposeListEditor.commit();
                                                    constants.purposeListEditor.apply();
                                                    constants.purposeList.getString("purposeList", "N/A");
                                                } else {


                                                    relativeLayout.setVisibility(View.GONE);
                                                    relativeLayout2.setVisibility(View.VISIBLE);
                                                    tagshorizontalListViewLayout.setVisibility(View.VISIBLE);
                                                    horizontalListAdapter = new CustomHorizontalListAdapter(purpStringArrayList, HomeScreen.this);
                                                    listView.setAdapter(horizontalListAdapter);

                                                }
                                            } else if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("54")) {

                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else {

                                                if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                                    Error();
                                                }


                                            }

                                        } else {

                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            isShown = true;

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                            Error();
                                        }
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                            Error();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                            Error();
                                        }
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sPurposeGroupCode", "FT");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };
                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                        Error();
                    }
                }
            } else {
                if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                    Error();
                }
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {
            if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                Error();
            }
//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();

        }
    }

    public void GetPurposeListIBFT(final String chat) {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sPurposeGroupCode", "EFT");
                    params.put("sServerSessionKey", md5);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getPuposesURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("00")) {


                                                purpStringArrayList = new ArrayList<>();
                                                purpStringArrayList.clear();
                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("POTList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        purpStringArrayList.add(jsonObj.getString("Name"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();

                                                        if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                                            Error();
                                                        }
                                                    }

                                                }

                                                if (!amount) {
                                                    Gson gson = new Gson();
                                                    String json = gson.toJson(purpStringArrayList);
                                                    constants.purposeListEditor.putString("purposeListIBFT", json);
                                                    constants.purposeListEditor.commit();
                                                    constants.purposeListEditor.apply();
                                                    constants.purposeList.getString("purposeListIBFT", "N/A");
                                                } else {


                                                    relativeLayout.setVisibility(View.GONE);
                                                    relativeLayout2.setVisibility(View.VISIBLE);
                                                    tagshorizontalListViewLayout.setVisibility(View.VISIBLE);
                                                    horizontalListAdapter = new CustomHorizontalListAdapter(purpStringArrayList, HomeScreen.this);
                                                    listView.setAdapter(horizontalListAdapter);

                                                }
                                            } else if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("54")) {

                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else {

                                                if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                                    Error();
                                                }


                                            }

                                        } else {

                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            isShown = true;

                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                            Error();
                                        }
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                            Error();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                            Error();
                                        }
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sPurposeGroupCode", "FT");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };
                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                        Error();
                    }
                }
            } else {
                if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                    Error();
                }
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {
            if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                Error();
            }
//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();

        }
    }

    public void GetPurposePrepaidList() {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sPurposeGroupCode", "PV");
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getPuposesURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("POTList"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        purpPrepaidStringArrayList.add(jsonObj.getString("Name"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();

                                                    }

                                                }

                                                Gson gson = new Gson();
                                                String json = gson.toJson(purpPrepaidStringArrayList);
                                                constants.purposePrepaidList.putString("purposeList", json);
                                                constants.purposePrepaidList.commit();
                                                constants.purposePrepaidList.apply();

                                            } else if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("54")) {

                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else {


                                            }
                                        } else {

                                            if (!isShown) {

                                                utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            }
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sPurposeGroupCode", "PV");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };
                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();

        }
    }

    public void GetLimitsList() {

        try {

            if (helper.isNetworkAvailable()) {

                try {

                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.limitsURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_LimitsChannelResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_LimitsChannelResult").getString("LimitChannel"));
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        Channels channels = new Channels();
                                                        channels.setChannelDes(jsonObj.getString("ChannelDescription"));
                                                        channels.setChannelID(jsonObj.getString("ChannelID"));
                                                        channels.setChannelName(jsonObj.getString("ChannelName"));


                                                        channelsArrayList.put(jsonObj.getString("ChannelName"), jsonObj.getString("ChannelID"));
//                                                    if (i == 0) {
//                                                        channelStringArrayList.add("Select Channel to view limits");
//                                                    }
                                                        channelStringArrayList.add(jsonObj.getString("ChannelName"));

                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();

                                                    }

                                                }

                                                Gson gson = new Gson();
                                                String json = gson.toJson(channelsArrayList);
                                                String json1 = gson.toJson(channelStringArrayList);
                                                constants.channelListEditor.putString("limits", json);
                                                constants.channelListEditor.putString("channels", json1);
                                                constants.channelListEditor.commit();
                                                constants.channelListEditor.apply();

                                            } else if (jsonObject.getJSONObject("Signs_LimitsChannelResult").getString("Status_Code").equals("54")) {

                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_LimitsChannelResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else {


                                            }
                                        } else {

                                            if (!isShown) {

                                                utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            }
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();

        }
    }

    public void SaveChatMessage(final String message) {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sChatText", message);
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.writeChatURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ChatHashMapKnowledgeBaseResult").getString("Status_Code").equals("00")) {


                                            } else if (jsonObject.getJSONObject("Signs_ChatHashMapKnowledgeBaseResult").getString("Status_Code").equals("54")) {

                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ChatHashMapKnowledgeBaseResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else {


                                            }

                                        } else {
                                            if (!isShown) {

                                                utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sChatText", message);
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();

        }
    }

    public void GetCount() {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    CryptLib _crypt = new CryptLib();
                    String legalId = _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sLegalIdentityValue", legalId);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSigns_Username", user);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getUnreadURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("get_unread_messagesResult").getString("Status_Code").equals("00")) {

                                                notiCount = jsonObject.getJSONObject("get_unread_messagesResult").getString("messages");
                                                if (!TextUtils.isEmpty(notiCount) && !notiCount.equals("0")) {
                                                    notiText.setVisibility(View.VISIBLE);
                                                    notiText.setText(notiCount);
                                                }
                                            } else if (jsonObject.getJSONObject("get_unread_messagesResult").getString("Status_Code").equals("54")) {

                                                utils.showDilogForLogout(jsonObject.getJSONObject("get_unread_messagesResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else {


                                            }
                                        } else {

                                            if (!isShown) {

                                                utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            }
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }) {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {

//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();

        }
    }

    public void userMessage() {

        try {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Message_Sent_Pressed", new Bundle());

            boolean keyFound = false;
            String message = editText.getText().toString().trim();
            if (TextUtils.isEmpty(message)) {
                relativeLayout2.setVisibility(View.VISIBLE);
                relativeLayout.setVisibility(View.GONE);
                return;
            }
            sendMessage(message);
            SaveChatMessage(message);
            editText.setText("");
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
            String soundex = "";
            int counter = 0;

            message = message.replaceAll(" ", " " + constants.strSpaceReplaceVal + " ");
            String[] singleArray = message.split(" ");

            for (int i = 0; i < singleArray.length; i++) {

                String single = singleArray[i];
                String soundex1 = soundex(single);

                if (counter == 0) {
                    soundex = soundex1;
                    counter++;
                } else
                    soundex = soundex + " " + soundex1;
            }
//            soundex = soundex(message);
            String mess = null;
            int val = 0;

            List keys = new ArrayList(chatHashMap.keySet());
            Collections.sort(keys);

            int intMatchCount = 0;
            boolean isCheck = false;
            for (int i = 0; i < keys.size(); i++) {

                intMatchCount = 0;
                if (isCheck) {

                    break;
                }
                HashMap<String, Integer> value = chatHashMap.get(keys.get(i));
                Iterator it = value.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    ++val;

                    String[] strSplitSoundex = pair.getKey().toString().split(" ");

                    for (String strSoundExVal : strSplitSoundex) //(int intIdx=0; intIdx<splitSoundex.count;intIdx++)
                    {
                        if (soundex.contains(strSoundExVal)) {
                            intMatchCount = intMatchCount + 1;
                        } else {
                            break;
                        }

                        if (intMatchCount == strSplitSoundex.length) {
                            //obj = chat_message_list[value]
                            keyFound = true;
                            isCheck = true;
                        }

                    }
                    if (keyFound) {
//if (soundex.contains(pair.getKey().toString()))
                        {

                            count = 0;
                            final Message chatMessage = messages.get(value.get((pair.getKey().toString())));

                            if (!TextUtils.isEmpty(chatMessage.getTarget()) && !chatMessage.getTarget().equals(null)) {

                                String target = chatMessage.getTarget().toLowerCase();
                                mess = chatMessage.getMessage();
                                CheckForTarget(target, mess);

                            } else {
//
                                Handler handler = new Handler();
                                final String finalMess = chatMessage.getMessage();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        String[] multiMessages = finalMess.split(";");
                                        final String randomStr = multiMessages[new Random().nextInt(multiMessages.length)];
                                        sendMessage(ReplacingVariables(randomStr), "");
                                        relativeLayout2.setVisibility(View.VISIBLE);
                                        relativeLayout.setVisibility(View.GONE);
                                    }
                                }, 1000);

                            }
                            isCheck = true;
                            break;
                        }

                    } else {


                        if (chatHashMap.size() == (val)) {

                            if (amount) {

                                if (isPay) {

                                    count = 0;
                                    totalAmount = message;
                                    relativeLayout2.setVisibility(View.VISIBLE);
                                    relativeLayout.setVisibility(View.GONE);
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            sendMessage("Please select a purpose for your payment.", "");

//                        count = 0;
                                        }
                                    }, 1000);

                                    Handler handler1 = new Handler();
                                    handler1.postDelayed(new Runnable() {
                                        public void run() {
                                            GetPurposeBillList("chat");
//                        relativeLayout2.setVisibility(View.VISIBLE);
//                        relativeLayout.setVisibility(View.GONE);

                                        }
                                    }, 2000);
                                } else {
                                    count = 0;
                                    totalAmount = message;
                                    relativeLayout2.setVisibility(View.VISIBLE);
                                    relativeLayout.setVisibility(View.GONE);
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            sendMessage("Please select a purpose for your transfer.", "");

//                        count = 0;
                                        }
                                    }, 1000);

                                    Handler handler1 = new Handler();
                                    handler1.postDelayed(new Runnable() {
                                        public void run() {
                                            if (isType.equals("OB")) {
                                                GetPurposeListIBFT("chat");
                                            } else
                                                GetPurposeListIBFT("chat");
//                        relativeLayout2.setVisibility(View.VISIBLE);
//                        relativeLayout.setVisibility(View.GONE);

                                        }
                                    }, 2000);

                                }


                            } else {
                                if (count == 0) {
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            sendMessage("Sorry, I didn’t get that. Type help and you will get all the information needed.", "");
                                            relativeLayout2.setVisibility(View.VISIBLE);
                                            relativeLayout.setVisibility(View.GONE);
                                            count++;
                                        }
                                    }, 700);

                                }

                                if (count == 1) {
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            sendMessage("Sorry, I am still learning. Type help and you will get all the information needed.", "");
                                            relativeLayout2.setVisibility(View.VISIBLE);
                                            relativeLayout.setVisibility(View.GONE);
                                            count++;
                                        }
                                    }, 700);

                                }


                                if (count == 2) {
                                    Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        public void run() {
                                            sendMessage("Sorry, I am still learning. Type help and you will get all the information needed.", "");
                                            relativeLayout2.setVisibility(View.VISIBLE);
                                            relativeLayout.setVisibility(View.GONE);
                                            count = 0;
                                        }
                                    }, 700);

                                }

                            }

                        }
                    }
                }


//
//
//        String[] userMessages = message.split(" ");
//        String mess = null;
//        for (int i = 0; i < userMessages.length; i++) {
//
//            if (chatHashMap.containsKey(userMessages[i].toLowerCase())) {
//
//                count = 0;
//                final Message chatMessage = messages.get(chatHashMap.get(userMessages[i].toLowerCase()));
//
//                if (!TextUtils.isEmpty(chatMessage.getTarget()) && !chatMessage.getTarget().equals(null)) {
//
//                    String target = chatMessage.getTarget().toLowerCase();
//                    mess = chatMessage.getMessage();
//                    CheckForTarget(target, mess);
//
//                } else {
//
//                    Handler handler = new Handler();
//                    final String finalMess = chatMessage.getMessage();
//                    handler.postDelayed(new Runnable() {
//                        public void run() {
//                            String[] multiMessages = finalMess.split(";");
//                            final String randomStr = multiMessages[new Random().nextInt(multiMessages.length)];
//                            sendMessage(ReplacingVariables(randomStr), "");
//                            relativeLayout2.setVisibility(View.VISIBLE);
//                            relativeLayout.setVisibility(View.GONE);
//                        }
//                    }, 1000);
//
//                }
//                break;
//
//
//            } else {
//
//                if (userMessages.length == (i + 1)) {
//
//                    if (amount) {
//
//                        if (isPay) {
//
//                            count = 0;
//                            totalAmount = message;
//                            relativeLayout2.setVisibility(View.VISIBLE);
//                            relativeLayout.setVisibility(View.GONE);
//                            Handler handler = new Handler();
//                            handler.postDelayed(new Runnable() {
//                                public void run() {
//                                    sendMessage("Please select a purpose for your payment.", "");
//
////                        count = 0;
//                                }
//                            }, 1000);
//
//                            Handler handler1 = new Handler();
//                            handler1.postDelayed(new Runnable() {
//                                public void run() {
//                                    GetPurposeBillList();
////                        relativeLayout2.setVisibility(View.VISIBLE);
////                        relativeLayout.setVisibility(View.GONE);
//
//                                }
//                            }, 2000);
//                        } else {
//                            count = 0;
//                            totalAmount = message;
//                            relativeLayout2.setVisibility(View.VISIBLE);
//                            relativeLayout.setVisibility(View.GONE);
//                            Handler handler = new Handler();
//                            handler.postDelayed(new Runnable() {
//                                public void run() {
//                                    sendMessage("Please select a purpose for your transfer.", "");
//
////                        count = 0;
//                                }
//                            }, 1000);
//
//                            Handler handler1 = new Handler();
//                            handler1.postDelayed(new Runnable() {
//                                public void run() {
//                                    GetPurposeList();
////                        relativeLayout2.setVisibility(View.VISIBLE);
////                        relativeLayout.setVisibility(View.GONE);
//
//                                }
//                            }, 2000);
//
//                        }
//
//
//                    } else {
//                        if (count == 0) {
//                            Handler handler = new Handler();
//                            handler.postDelayed(new Runnable() {
//                                public void run() {
//                                    sendMessage("Sorry, I didn’t get that. Please try again.", "");
//                                    relativeLayout2.setVisibility(View.VISIBLE);
//                                    relativeLayout.setVisibility(View.GONE);
//                                    count++;
//                                }
//                            }, 700);
//
//                        }
//
//                        if (count == 1) {
//                            Handler handler = new Handler();
//                            handler.postDelayed(new Runnable() {
//                                public void run() {
//                                    sendMessage("Well, this is embarrassing..\n" +
//                                            "I wasn't able to understand that last one. Try again?", "");
//                                    relativeLayout2.setVisibility(View.VISIBLE);
//                                    relativeLayout.setVisibility(View.GONE);
//                                    count++;
//                                }
//                            }, 700);
//
//                        }
//
//
//                        if (count == 2) {
//                            Handler handler = new Handler();
//                            handler.postDelayed(new Runnable() {
//                                public void run() {
//                                    sendMessage("Sorry, I am still learning. Please use Menu on top left corner for more options.", "");
//                                    relativeLayout2.setVisibility(View.VISIBLE);
//                                    relativeLayout.setVisibility(View.GONE);
//                                    count = 0;
//                                }
//                            }, 700);
//
//                        }
//
//                    }
//
//                }
//            }
//        }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    public void userMessageWorking() {
//
//        try {
//
//
//            String message = editText.getText().toString().trim();
//            if (TextUtils.isEmpty(message)) {
//                relativeLayout2.setVisibility(View.VISIBLE);
//                relativeLayout.setVisibility(View.GONE);
//                return;
//            }
//            sendMessage(message);
//            SaveChatMessage(message);
//            editText.setText("");
//            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
//            String soundex = "";
//            int counter = 0;
//
//            String[] singleArray = message.split(" ");
//
//            for (int i = 0; i < singleArray.length; i++) {
//
//                String single = singleArray[i];
//                String soundex1 = soundex(single);
//
//                if (counter == 0) {
//                    soundex = soundex1;
//                    counter++;
//                } else
//                    soundex = soundex + " " + soundex1;
//            }
////            soundex = soundex(message);
//            String mess = null;
//            int val = 0;
//
//            List keys = new ArrayList(chatHashMap.keySet());
//            Collections.sort(keys);
//
//            boolean isCheck = false;
//            for (int i = 0; i < keys.size(); i++) {
//
//                if (isCheck) {
//
//                    break;
//                }
//                HashMap<String, Integer> value = chatHashMap.get(keys.get(i));
//                Iterator it = value.entrySet().iterator();
//                while (it.hasNext()) {
//                    Map.Entry pair = (Map.Entry) it.next();
//                    System.out.println(pair.getKey() + " = " + pair.getValue());
//                    ++val;
//
//                    if (soundex.contains(pair.getKey().toString())) {
//
//                        count = 0;
//                        final Message chatMessage = messages.get(value.get((pair.getKey().toString())));
//
//                        if (!TextUtils.isEmpty(chatMessage.getTarget()) && !chatMessage.getTarget().equals(null)) {
//
//                            String target = chatMessage.getTarget().toLowerCase();
//                            mess = chatMessage.getMessage();
//                            CheckForTarget(target, mess);
//
//                        } else {
////
//                            Handler handler = new Handler();
//                            final String finalMess = chatMessage.getMessage();
//                            handler.postDelayed(new Runnable() {
//                                public void run() {
//                                    String[] multiMessages = finalMess.split(";");
//                                    final String randomStr = multiMessages[new Random().nextInt(multiMessages.length)];
//                                    sendMessage(ReplacingVariables(randomStr), "");
//                                    relativeLayout2.setVisibility(View.VISIBLE);
//                                    relativeLayout.setVisibility(View.GONE);
//                                }
//                            }, 1000);
//
//                        }
//                        isCheck = true;
//                        break;
//                    } else {
//
//
//                        if (chatHashMap.size() == (val)) {
//
//                            if (amount) {
//
//                                if (isPay) {
//
//                                    count = 0;
//                                    totalAmount = message;
//                                    relativeLayout2.setVisibility(View.VISIBLE);
//                                    relativeLayout.setVisibility(View.GONE);
//                                    Handler handler = new Handler();
//                                    handler.postDelayed(new Runnable() {
//                                        public void run() {
//                                            sendMessage("Please select a purpose for your payment.", "");
//
////                        count = 0;
//                                        }
//                                    }, 1000);
//
//                                    Handler handler1 = new Handler();
//                                    handler1.postDelayed(new Runnable() {
//                                        public void run() {
//                                            GetPurposeBillList("chat");
////                        relativeLayout2.setVisibility(View.VISIBLE);
////                        relativeLayout.setVisibility(View.GONE);
//
//                                        }
//                                    }, 2000);
//                                } else {
//                                    count = 0;
//                                    totalAmount = message;
//                                    relativeLayout2.setVisibility(View.VISIBLE);
//                                    relativeLayout.setVisibility(View.GONE);
//                                    Handler handler = new Handler();
//                                    handler.postDelayed(new Runnable() {
//                                        public void run() {
//                                            sendMessage("Please select a purpose for your transfer.", "");
//
////                        count = 0;
//                                        }
//                                    }, 1000);
//
//                                    Handler handler1 = new Handler();
//                                    handler1.postDelayed(new Runnable() {
//                                        public void run() {
//                                            GetPurposeList("chat");
////                        relativeLayout2.setVisibility(View.VISIBLE);
////                        relativeLayout.setVisibility(View.GONE);
//
//                                        }
//                                    }, 2000);
//
//                                }
//
//
//                            } else {
//                                if (count == 0) {
//                                    Handler handler = new Handler();
//                                    handler.postDelayed(new Runnable() {
//                                        public void run() {
//                                            sendMessage("Sorry, I didn’t get that. Please try again.", "");
//                                            relativeLayout2.setVisibility(View.VISIBLE);
//                                            relativeLayout.setVisibility(View.GONE);
//                                            count++;
//                                        }
//                                    }, 700);
//
//                                }
//
//                                if (count == 1) {
//                                    Handler handler = new Handler();
//                                    handler.postDelayed(new Runnable() {
//                                        public void run() {
//                                            sendMessage("Well, this is embarrassing..\n" +
//                                                    "I wasn't able to understand that last one. Try again?", "");
//                                            relativeLayout2.setVisibility(View.VISIBLE);
//                                            relativeLayout.setVisibility(View.GONE);
//                                            count++;
//                                        }
//                                    }, 700);
//
//                                }
//
//
//                                if (count == 2) {
//                                    Handler handler = new Handler();
//                                    handler.postDelayed(new Runnable() {
//                                        public void run() {
//                                            sendMessage("Sorry, I am still learning. Please use Menu on top left corner for more options.", "");
//                                            relativeLayout2.setVisibility(View.VISIBLE);
//                                            relativeLayout.setVisibility(View.GONE);
//                                            count = 0;
//                                        }
//                                    }, 700);
//
//                                }
//
//                            }
//
//                        }
//                    }
//                }
//
//
////
////
////        String[] userMessages = message.split(" ");
////        String mess = null;
////        for (int i = 0; i < userMessages.length; i++) {
////
////            if (chatHashMap.containsKey(userMessages[i].toLowerCase())) {
////
////                count = 0;
////                final Message chatMessage = messages.get(chatHashMap.get(userMessages[i].toLowerCase()));
////
////                if (!TextUtils.isEmpty(chatMessage.getTarget()) && !chatMessage.getTarget().equals(null)) {
////
////                    String target = chatMessage.getTarget().toLowerCase();
////                    mess = chatMessage.getMessage();
////                    CheckForTarget(target, mess);
////
////                } else {
////
////                    Handler handler = new Handler();
////                    final String finalMess = chatMessage.getMessage();
////                    handler.postDelayed(new Runnable() {
////                        public void run() {
////                            String[] multiMessages = finalMess.split(";");
////                            final String randomStr = multiMessages[new Random().nextInt(multiMessages.length)];
////                            sendMessage(ReplacingVariables(randomStr), "");
////                            relativeLayout2.setVisibility(View.VISIBLE);
////                            relativeLayout.setVisibility(View.GONE);
////                        }
////                    }, 1000);
////
////                }
////                break;
////
////
////            } else {
////
////                if (userMessages.length == (i + 1)) {
////
////                    if (amount) {
////
////                        if (isPay) {
////
////                            count = 0;
////                            totalAmount = message;
////                            relativeLayout2.setVisibility(View.VISIBLE);
////                            relativeLayout.setVisibility(View.GONE);
////                            Handler handler = new Handler();
////                            handler.postDelayed(new Runnable() {
////                                public void run() {
////                                    sendMessage("Please select a purpose for your payment.", "");
////
//////                        count = 0;
////                                }
////                            }, 1000);
////
////                            Handler handler1 = new Handler();
////                            handler1.postDelayed(new Runnable() {
////                                public void run() {
////                                    GetPurposeBillList();
//////                        relativeLayout2.setVisibility(View.VISIBLE);
//////                        relativeLayout.setVisibility(View.GONE);
////
////                                }
////                            }, 2000);
////                        } else {
////                            count = 0;
////                            totalAmount = message;
////                            relativeLayout2.setVisibility(View.VISIBLE);
////                            relativeLayout.setVisibility(View.GONE);
////                            Handler handler = new Handler();
////                            handler.postDelayed(new Runnable() {
////                                public void run() {
////                                    sendMessage("Please select a purpose for your transfer.", "");
////
//////                        count = 0;
////                                }
////                            }, 1000);
////
////                            Handler handler1 = new Handler();
////                            handler1.postDelayed(new Runnable() {
////                                public void run() {
////                                    GetPurposeList();
//////                        relativeLayout2.setVisibility(View.VISIBLE);
//////                        relativeLayout.setVisibility(View.GONE);
////
////                                }
////                            }, 2000);
////
////                        }
////
////
////                    } else {
////                        if (count == 0) {
////                            Handler handler = new Handler();
////                            handler.postDelayed(new Runnable() {
////                                public void run() {
////                                    sendMessage("Sorry, I didn’t get that. Please try again.", "");
////                                    relativeLayout2.setVisibility(View.VISIBLE);
////                                    relativeLayout.setVisibility(View.GONE);
////                                    count++;
////                                }
////                            }, 700);
////
////                        }
////
////                        if (count == 1) {
////                            Handler handler = new Handler();
////                            handler.postDelayed(new Runnable() {
////                                public void run() {
////                                    sendMessage("Well, this is embarrassing..\n" +
////                                            "I wasn't able to understand that last one. Try again?", "");
////                                    relativeLayout2.setVisibility(View.VISIBLE);
////                                    relativeLayout.setVisibility(View.GONE);
////                                    count++;
////                                }
////                            }, 700);
////
////                        }
////
////
////                        if (count == 2) {
////                            Handler handler = new Handler();
////                            handler.postDelayed(new Runnable() {
////                                public void run() {
////                                    sendMessage("Sorry, I am still learning. Please use Menu on top left corner for more options.", "");
////                                    relativeLayout2.setVisibility(View.VISIBLE);
////                                    relativeLayout.setVisibility(View.GONE);
////                                    count = 0;
////                                }
////                            }, 700);
////
////                        }
////
////                    }
////
////                }
////            }
////        }
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    public void GetPurposeBillList(final String chat) {

        try {

            if (helper.isNetworkAvailable()) {
                try {

                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sPurposeGroupCode", "BP");
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getPuposesURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("POTList"));
                                                purpBillStringArrayList = new ArrayList<>();
                                                purpBillStringArrayList.clear();
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {

                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        purpBillStringArrayList.add(jsonObj.getString("Name"));


                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                        if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                                            Error();
                                                        }

                                                    }

                                                }

                                                if (!amount) {
                                                    Gson gson = new Gson();
                                                    String json = gson.toJson(purpBillStringArrayList);
                                                    constants.purposeBillList.putString("purposeList", json);
                                                    constants.purposeBillList.commit();
                                                    constants.purposeBillList.apply();
                                                } else {


                                                    relativeLayout.setVisibility(View.GONE);
                                                    relativeLayout2.setVisibility(View.VISIBLE);
                                                    tagshorizontalListViewLayout.setVisibility(View.VISIBLE);
                                                    horizontalListAdapter = new CustomHorizontalListAdapter(purpBillStringArrayList, HomeScreen.this);
                                                    listView.setAdapter(horizontalListAdapter);

                                                }


                                            } else if (jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Code").equals("54")) {

                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_PurposeOfTransactionResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else {
                                                if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                                    Error();
                                                }

                                            }

                                        } else {

                                            if (!isShown) {

                                                utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                            Error();
                                        }
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                            Error();
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                                            Error();
                                        }
                                    }
                                }
                            })

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sPurposeGroupCode", "BP");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };
                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                        Error();
                    }
                }
            } else {
                if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                    Error();
                }
//                Toast.makeText(HomeScreen.this, "Please check your internet connectivity!", Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {
            if (!TextUtils.isEmpty(chat) && chat.equals("chat")) {

                Error();
            }
//            Toast.makeText(HomeScreen.this, "Please check your internet connectivity", Toast.LENGTH_LONG).show();

        }
    }

    public void GetAccountsList() {

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {
                //135821 ammar
                //181936 hasan
                //constants.sharedPreferences.getString("T24_CIF", "N/A")

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sT24_CIF", _crypt.decrypt(constants.sharedPreferences.getString("T24_CIF", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sLegalIdentityType", _crypt.decrypt(constants.sharedPreferences.getString("LegalType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sSigns_UserType", _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sIsAllAccounts", "1");
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAccountListURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("00")) {

                                                int count = 0;
                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("AccountList"));

                                                if (jsonArray.length() > 0) {

                                                    accounts = new ArrayList<>();
                                                    accounts.clear();

//                                                accountListView.setAdapter(null);


                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                        try {

                                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
                                                            Account account = new Account();
                                                            account.setAccountNumber(jsonObj.getString("AccountID"));
                                                            account.setAccountName(jsonObj.getString("AccountTitle"));
                                                            account.setUserName(jsonObj.getString("AccountTitle"));
                                                            account.setBranchName(jsonObj.getString("BranchName"));
                                                            account.setAccountType(jsonObj.getString("Type"));
                                                            account.setAvailBal(jsonObj.getString("AvailBal"));
                                                            account.setBankCode(jsonObj.getString("BankCode"));
                                                            account.setBankID(jsonObj.getString("BankCode"));
                                                            account.setBranchCode(jsonObj.getString("BranchCode"));
                                                            account.setCurrency(jsonObj.getString("Currency"));
                                                            account.setCurrencyType(jsonObj.getString("Currency"));
                                                            account.setProductType(jsonObj.getString("Product"));
                                                            account.setBankColor(jsonObj.getString("BankColorCode"));
                                                            account.setIsActive(jsonObj.getString("Active"));
                                                            account.setId(jsonObj.getString("ID"));
                                                            account.setBranchShortName(jsonObj.getString("BranchNameShort"));
                                                            accounts.add(account);


                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                            pd.dismiss();
//                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                            Error();
                                                        }


                                                    }

                                                    if (isPay) {

                                                        accountsForBillerCustomHorizontalListAdapter = new AccountsForBillerCustomHorizontalListAdapter(accounts, HomeScreen.this);
                                                        accountListView.setAdapter(accountsForBillerCustomHorizontalListAdapter);
                                                        accountsForBillerCustomHorizontalListAdapter.notifyDataSetChanged();
                                                        horizontalListViewLayout.setVisibility(View.VISIBLE);
                                                        pd.dismiss();
                                                    } else {
                                                        accoutsCustomHorizontalListAdapter = new AccoutsCustomHorizontalListAdapter(accounts, HomeScreen.this);
                                                        accountListView.setAdapter(accoutsCustomHorizontalListAdapter);
                                                        accoutsCustomHorizontalListAdapter.notifyDataSetChanged();
                                                        horizontalListViewLayout.setVisibility(View.VISIBLE);
                                                        pd.dismiss();
                                                    }
                                                } else {
                                                    pd.dismiss();
                                                    Error();

                                                }
                                            } else if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("49")) {
                                                pd.dismiss();
//                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "WARNING");
                                                Error("Account", "You don't have any account linked on Samba Digital Banking. Please go to Manage Accounts to link accounts.");
                                            } else {
                                                pd.dismiss();
//                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "WARNING");
                                                Error();
                                            }
                                        } else {
                                            pd.dismiss();
                                            if (!isShown) {

                                                utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            }
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
//                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                        Error();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
//                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                        Error();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
//                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                        Error();
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sT24_CIF", constants.sharedPreferences.getString("T24_CIF", "N/A"));
//                            params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                            params.put("sLegalIdentityType", constants.sharedPreferences.getString("LegalType", "N/A"));
//                            params.put("sSigns_UserType", constants.sharedPreferences.getString("type", "N/A"));
//                            params.put("sIsAllAccounts", "1");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
//                    utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                    Error();
                }

            } else {
                pd.dismiss();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                Error();
            }
        } catch (Exception e) {


            pd.dismiss();
//            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            Error();
        }
    }

    public void GetAccountsAndBeneList() {

        try {

            pd.show();
            if (helper.isNetworkAvailable()) {
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Username", user);
                params.put("sT24_CIF", _crypt.decrypt(constants.sharedPreferences.getString("T24_CIF", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(HomeScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAccountNBenelistURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Status_Code").equals("00")) {

                                            try {

                                                JSONArray beneArray = new JSONArray(jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Local_BenefeciaryList"));

                                                if (beneArray.length() > 0) {

                                                    userModels = new ArrayList<>();
                                                    userModelsWithinSamba = new ArrayList<>();
                                                    userModels.clear();
                                                    userModelsWithinSamba.clear();

//                                                accountListView.setAdapter(null);

                                                    for (int i = 0; i < beneArray.length(); i++) {

                                                        try {


                                                            JSONObject jsonObj = new JSONObject(beneArray.get(i).toString());


                                                            UserModel userModel = new UserModel();
                                                            userModel.setBankName(jsonObj.getString("Bene_BankName"));
                                                            userModel.setAccountNumber(jsonObj.getString("Bene_AccountNumber"));
                                                            userModel.setUserName(jsonObj.getString("Bene_Alias"));
                                                            userModel.setBankCode(jsonObj.getString("Bene_BankCode"));
                                                            userModel.setCurrency(jsonObj.getString("Bene_CurrencyCode"));
                                                            userModel.setBeneType(jsonObj.getString("Bene_Type"));
                                                            userModel.setUserTitle(jsonObj.getString("Bene_AccountTitle"));
                                                            userModel.setMobileNumber(jsonObj.getString("Bene_Mobile"));
                                                            userModel.setEmail(jsonObj.getString("Bene_Email"));
                                                            userModel.setBeneID(jsonObj.getString("Bene_ID"));
                                                            userModel.setAccountTitle(jsonObj.getString("Bene_AccountTitle"));
                                                            userModel.setBankShortCode(jsonObj.getString("Bene_BankShortCode"));
                                                            userModel.setBankColor(jsonObj.getString("Bene_BankColorCode"));
                                                            String firstName = jsonObj.getString("Bene_Alias");
                                                            String s = firstName.substring(0, 1);
                                                            userModel.setFirstLetter(s.toUpperCase());
                                                            if (isType.equals("TWS")) {

                                                                if (userModel.getBankName().equals("Samba Bank Limited")) {

                                                                    userModelsWithinSamba.add(userModel);

                                                                } else {

//                                                                userModels.add(userModel);
                                                                }

                                                            } else {

                                                                if (userModel.getBankName().equals("Samba Bank Limited")) {

//                                                                userModelsWithinSamba.add(userModel);

                                                                } else {

                                                                    userModels.add(userModel);
                                                                }
//                                                            userModels.add(userModel);
                                                            }
//                                                        userModels.add(userModel);


                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                            pd.dismiss();
//                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                            Error();
                                                        }

                                                    }
                                                    sambaHeader.setText("SELECT BENEFICIARY");

                                                    if (isType.equals("TWS")) {

                                                        destinationAccountHorizontalListAdapter = new DestinationAccountHorizontalListAdapter(userModelsWithinSamba, HomeScreen.this);
                                                        accountListView.setAdapter(destinationAccountHorizontalListAdapter);
                                                        destinationAccountHorizontalListAdapter.notifyDataSetChanged();
                                                    } else {

                                                        destinationAccountHorizontalListAdapter = new DestinationAccountHorizontalListAdapter(userModels, HomeScreen.this);
                                                        accountListView.setAdapter(destinationAccountHorizontalListAdapter);
                                                        destinationAccountHorizontalListAdapter.notifyDataSetChanged();
                                                    }

                                                    horizontalListViewLayout.setVisibility(View.VISIBLE);
                                                    relativeLayout2.setVisibility(View.GONE);
                                                    relativeLayout.setVisibility(View.GONE);
                                                    pd.dismiss();
                                                } else {

                                                    pd.dismiss();
                                                    Error();

                                                }

                                            } catch (JSONException ex) {

                                                ex.printStackTrace();
                                                pd.dismiss();
//                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                Error();
                                            }
                                        } else if (jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                        } else if (jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Status_Code").equals("14")) {
                                            pd.dismiss();
//                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "WARNING");
                                            Error("Bene", "You don't have any beneficiary added on Samba Digital Banking. Please go to Manage Beneficiaries to add.");
                                        } else {
                                            pd.dismiss();
//                                        utils.showDilogForError(jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Status_Description"), "ERROR");
                                            Error();
                                        }

                                    } else {
                                        pd.dismiss();
                                        if (!isShown) {

                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    Error();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {

                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    Error();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    Error();
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sT24_CIF", constants.sharedPreferences.getString("T24_CIF", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };
                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                pd.dismiss();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                Error();

            }
        } catch (Exception e) {

            pd.dismiss();
//            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            Error();
        }
    }

    public void AccountDetails(final String accountNumber, final String branchCode) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Username", user);
                params.put("sT24_AccountNumber", accountNumber);
                params.put("sBranchCode", branchCode);
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(HomeScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.miniStatementURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("Status_Code").equals("00")) {

                                            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("MiniStatement"));
                                            String checkDate;
                                            int count = 0;
                                            if (jsonArray.length() > 0) {

                                                sameDate = new ArrayList<>();
                                                sameDate.clear();
                                                transactionDetailsList = new ArrayList<>();
                                                transactionDetailsList.clear();
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    try {
                                                        JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                        TransactionDetails transactionDetails = new TransactionDetails();
                                                        transactionDetails.setTitle(jsonObj.getString("Description"));
                                                        Long date = Long.valueOf(jsonObj.getString("DateTime"));
                                                        String value = jsonObj.getString("ValueDate");
                                                        String year = value.substring(0, 4);
                                                        String month = value.substring(4, 6);
                                                        String datetype = value.substring(6, 8);
                                                        String formatedDate = year + "-" + month + "-" + datetype;
                                                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
                                                        Date mDate;
                                                        long timeInMilliseconds = 0;
                                                        try {

                                                            mDate = sdf1.parse(formatedDate);
                                                            timeInMilliseconds = mDate.getTime();

                                                        } catch (Exception e) {

                                                            e.printStackTrace();
                                                            pd.dismiss();
                                                            Error();
                                                        }

                                                        Long valueDate = Long.valueOf(jsonObj.getString("ValueDate"));
                                                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                                                        String date1 = getDate(date, "dd-MMM-yyyy");
                                                        transactionDetails.setDate(date1);
                                                        transactionDetails.setCurrency(jsonObj.getString("Currency"));
                                                        transactionDetails.setAccNumber(jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("AccountID"));
                                                        transactionDetails.setValueDate(getDate(timeInMilliseconds, "dd-MMM-yyyy"));
                                                        transactionDetails.setAfterAmount(jsonObj.getString("Balance"));
                                                        transactionDetails.setDeposit(jsonObj.getString("Deposits"));
                                                        transactionDetails.setWithDraw(jsonObj.getString("Withdrawals"));
                                                        transactionDetails.setRequestNumber(jsonObj.getString("Reference"));
                                                        checkDate = getDate(timeInMilliseconds, "dd-MMM-yyyy");
                                                        if (count == 0) {
//                                                            if (transactionDetails.getRequestNumber().startsWith("FT")) {
//                                                                if (!TextUtils.isEmpty(transactionDetails.getWithDraw())) {
//
//                                                                    sameDate.add("FT DEBIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getWithDraw() + " on " + transactionDetails.getValueDate());
//                                                                } else {
//
//                                                                    sameDate.add("FT CREDIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getDeposit() + " on " + transactionDetails.getValueDate());
//
//                                                                }
//                                                            } else {
//
//                                                                if (!TextUtils.isEmpty(transactionDetails.getWithDraw())) {
//
//                                                                    sameDate.add("IBFT DEBIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getWithDraw() + " on " + transactionDetails.getValueDate());
//                                                                } else {
//
//                                                                    sameDate.add("IBFT CREDIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getDeposit() + " on " + transactionDetails.getValueDate());
//
//                                                                }
//                                                            }

                                                            if (transactionDetails.getDeposit().equals("")) {

                                                                sameDate.add(transactionDetails.getTitle() + " Debit: " + transactionDetails.getCurrency() + " " + transactionDetails.getWithDraw() + " on " + transactionDetails.getValueDate());

                                                            } else {
                                                                sameDate.add(transactionDetails.getTitle() + " Credit: " + transactionDetails.getCurrency() + " " + transactionDetails.getDeposit() + " on " + transactionDetails.getValueDate());

                                                            }
                                                        } else {
//                                                            if (transactionDetailsList.get(0).getValueDate().equals(transactionDetails.getValueDate())) {
//
//
//                                                                if (transactionDetails.getRequestNumber().startsWith("FT")) {
//                                                                    if (!TextUtils.isEmpty(transactionDetails.getWithDraw())) {
//
//                                                                        sameDate.add("FT DEBIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getWithDraw() + " on " + transactionDetails.getValueDate());
//                                                                    } else {
//
//                                                                        sameDate.add("FT CREDIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getDeposit() + " on " + transactionDetails.getValueDate());
//
//                                                                    }
//                                                                } else {
//
//                                                                    if (!TextUtils.isEmpty(transactionDetails.getWithDraw())) {
//
//                                                                        sameDate.add("IBFT DEBIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getWithDraw() + " on " + transactionDetails.getValueDate());
//                                                                    } else {
//
//                                                                        sameDate.add("IBFT CREDIT: " + transactionDetails.getCurrency() + " " + transactionDetails.getDeposit() + " on " + transactionDetails.getValueDate());
//
//                                                                    }
//                                                                }
//                                                            }
                                                            if (transactionDetails.getDeposit().equals("")) {

                                                                sameDate.add(transactionDetails.getTitle() + " Debit: " + transactionDetails.getCurrency() + " " + transactionDetails.getWithDraw() + " on " + transactionDetails.getValueDate());

                                                            } else {
                                                                sameDate.add(transactionDetails.getTitle() + " Credit: " + transactionDetails.getCurrency() + " " + transactionDetails.getDeposit() + " on " + transactionDetails.getValueDate());

                                                            }
                                                        }
                                                        transactionDetailsList.add(transactionDetails);
                                                        count++;
                                                    } catch (JSONException ex) {

                                                        ex.printStackTrace();
                                                        pd.dismiss();
//                                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        Error();
                                                    }

                                                }
                                                pd.dismiss();

                                                String combined = "";
                                                int sameCount = 0;
                                                for (int i = 0; i < sameDate.size(); i++) {

                                                    if (sameCount == 0) {

                                                        combined = sameDate.get(i);
                                                        sameCount++;
                                                    } else {


                                                        combined = combined + "\n" + sameDate.get(i);
                                                    }

                                                }

                                                sendMessage("Ok, here are your most recent transaction\n" +
                                                        "of  " + utils.getMaskedString(accountNumber) + "\n\n" + combined, "");
                                            } else {

                                                pd.dismiss();
                                                Error();
                                            }

                                        } else if (jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24MiniStatementResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                        } else {
                                            pd.dismiss();
//                                        utils.showDilogForError(jsonObject.getJSONObject("Signs_T24MiniStatement").getString("Status_Decription"), "ERROR");
                                            Error();

                                        }

                                    } else {
                                        pd.dismiss();
                                        if (!isShown) {

                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    Error();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    Error();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    Error();
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sT24_AccountNumber", accountNumber);
//                        params.put("sBranchCode", branchCode);
//                        params.put("sServerSessionKey", md5);
//
//
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                pd.dismiss();
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                Error();
            }
        } catch (Exception e) {


            pd.dismiss();
//            utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            Error();
        }
    }

    public void LatestBalance(final String acountNumber, final int position, final String currency) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sT24_AccountNumber", acountNumber);
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getLatestBalURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code").equals("00")) {


                                                pd.dismiss();

                                                sendMessage("Balance of a/c is " + currency + " " + jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"), "");


//                                            int amount = Integer.valueOf(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"));

                                            } else if (jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else {
                                                pd.dismiss();
//                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Description"), "ERROR");
                                                Error();
                                            }
                                        } else {
                                            pd.dismiss();
                                            if (!isShown) {

                                                utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        Error();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        Error();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        Error();
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sT24_AccountNumber", acountNumber);
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
//                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    Error();
                }

            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                Error();

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
//            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            Error();

        }
    }

    public void LatestBalance(final String accountNumber) {

        try {

            if (helper.isNetworkAvailable()) {
                pd.show();
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sT24_AccountNumber", accountNumber);
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getLatestBalURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code").equals("00")) {


                                                pd.dismiss();
                                                sourceAmount = jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal");
                                                Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    public void run() {

                                                        if (isPay) {

                                                            Intent intent = new Intent(HomeScreen.this, PayBillsScreen.class);
                                                            intent.putExtra("requestbill", "payBillFromHome");
                                                            intent.putExtra("sourceTile", sourceTile);
                                                            intent.putExtra("sourceBankCode", sourceBankCode);
                                                            intent.putExtra("sourceBranchCode", sourceBranchCode);
                                                            intent.putExtra("sourceNumber", sourceNumber);
                                                            intent.putExtra("sourceCurrency", sourceCurrency);
                                                            intent.putExtra("amount", totalAmount);
                                                            intent.putExtra("remarks", remarks);
                                                            intent.putExtra("sourceAmount", sourceAmount);
                                                            overridePendingTransition(0, 0);
                                                            amount = false;
                                                            isFt = false;
                                                            isSource = false;
                                                            isPay = false;
                                                            startActivity(intent);
                                                        } else {
                                                            if (isType.equals("OA") && ftType.equals("MM")) {

                                                                Intent intent = new Intent(HomeScreen.this, FundTransferScreen.class);
                                                                intent.putExtra("requestFunds", "moveMoneyFromHome");
                                                                intent.putExtra("sourceTile", sourceTile);
                                                                intent.putExtra("sourceBankCode", sourceBankCode);
                                                                intent.putExtra("sourceBranchCode", sourceBranchCode);
                                                                intent.putExtra("sourceNumber", sourceNumber);
                                                                intent.putExtra("sourceCurrency", sourceCurrency);
                                                                intent.putExtra("amount", totalAmount);
                                                                intent.putExtra("remarks", remarks);
                                                                intent.putExtra("sourceAmount", sourceAmount);
                                                                intent.putExtra("destinationAccount", destinationAccount);
                                                                intent.putExtra("destinationBal", destinationBal);
                                                                intent.putExtra("destinationBankCode", destinationBankCode);
                                                                intent.putExtra("destinationCurrency", destinationCurrency);
                                                                intent.putExtra("destinationTitle", destinationTitle);
                                                                intent.putExtra("sourceproduct",SourceProductType);
                                                                intent.putExtra("destproduct",DestProductType);


                                                                overridePendingTransition(0, 0);
                                                                amount = false;
                                                                isFt = false;
                                                                isSource = false;
//                                                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                                                        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                                                                startActivity(intent);
//                                                        finish();

                                                            } else {

                                                                Intent intent = new Intent(HomeScreen.this, FundTransferScreen.class);
                                                                intent.putExtra("requestFunds", "sendMoneyFromHome");
                                                                intent.putExtra("sourceTile", sourceTile);
                                                                intent.putExtra("sourceBankCode", sourceBankCode);
                                                                intent.putExtra("sourceBranchCode", sourceBranchCode);
                                                                intent.putExtra("sourceNumber", sourceNumber);
                                                                intent.putExtra("sourceAmount", sourceAmount);
                                                                intent.putExtra("sourceCurrency", sourceCurrency);
                                                                intent.putExtra("amount", totalAmount);
                                                                intent.putExtra("remarks", remarks);
                                                                intent.putExtra("destinationAccount", destinationAccount);
                                                                intent.putExtra("destinationBal", destinationBal);
                                                                intent.putExtra("destinationBankCode", destinationBankCode);
                                                                intent.putExtra("destinationCurrency", destinationCurrency);
                                                                intent.putExtra("destinationTitle", destinationTitle);
                                                                intent.putExtra("alias", destinationAlias);
                                                                intent.putExtra("beneType", beneType);
                                                                intent.putExtra("beneID", beneID);
                                                                intent.putExtra("email", email);
                                                                intent.putExtra("mobileNumber", mobile);
                                                                amount = false;
                                                                isFt = false;
                                                                isSource = false;
                                                                overridePendingTransition(0, 0);
//                                                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                                                        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                                                                startActivity(intent);
//                                                        finish();

                                                            }
                                                        }


                                                    }
                                                }, 1000);
//                                            sendMessage("Balance of a/c is " + accounts.get(position).getCurrency() + " " + jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"), "");


//                                            int amount = Integer.valueOf(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("AvailBal"));

                                            } else if (jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else {
                                                pd.dismiss();
//                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountBasicDetailResult").getString("Status_Description"), "ERROR");
                                                amount = false;
                                                editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                                                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
                                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                                                Error();
                                            }

                                        } else {
                                            pd.dismiss();
                                            if (!isShown) {

                                                utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            }
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        amount = false;
                                        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                                        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
                                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                                        Error();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        amount = false;
                                        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                                        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
                                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                                        Error();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        amount = false;
                                        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                                        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
                                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                                        Error();
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sT24_AccountNumber", accountNumber);
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
//                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    amount = false;
                    editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                    Error();
                }

            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                amount = false;
                editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                Error();

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
//            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            amount = false;
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
            Error();

        }
    }

    private String getFormatedAmount(int amount) {
        return NumberFormat.getNumberInstance(Locale.US).format(amount);
    }


    private void sendMessage(String message) {
        ChatMessage chatMessage = new ChatMessage(message, true, false);
        mAdapter.add(chatMessage);
        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
//        mimicOtherMessage(message);
    }

    private void mimicOtherMessage(String message) {
        ChatMessage chatMessage = new ChatMessage(message, false, false);
        mAdapter.add(chatMessage);
//        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);

    }

    private void sendMessage(String messag, String messsage) {
//        ChatMessage chatMessage = new ChatMessage("Hello", false, true);
//        mAdapter.add(chatMessage);
//
//        mimicOtherMessage();
        ChatMessage chatMessage = new ChatMessage(messag, false, true);
        mAdapter.add(chatMessage);

        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    private void mimicOtherMessage() {
        ChatMessage chatMessage = new ChatMessage(null, false, true);
        mAdapter.add(chatMessage);

        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
    }


    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(HomeScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        btn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public void setBoomMenus() {

        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder()
                    .listener(new OnBMClickListener() {
                        @Override
                        public void onBoomButtonClick(int index) {
                            // When the boom-button corresponding this builder is clicked.
//                            Toast.makeText(HomeScreen.this, "Clicked " + index, Toast.LENGTH_SHORT).show();
                        }
                    });

//            int margin = getResources().getDimensionPixelSize(R.dimen._4sdp);
            builder.textSize(12);

            switch (i) {

                case 0:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Home");
                    builder.unableText("Home");
                    builder.normalText("Home");
                    break;

                case 1:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Accounts");
                    builder.unableText("Accounts");
                    builder.normalText("Accounts");
                    break;

                case 2:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Transfers");
                    builder.unableText("Transfers");
                    builder.normalText("Transfers");
                    break;


                case 3:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Payments");
                    builder.unableText("Payments");
                    builder.normalText("Payments");
                    break;


                case 4:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Beneficiaries");
                    builder.unableText("Beneficiaries");
                    builder.normalText("Beneficiaries");
                    break;


                case 5:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("QR Payments");
                    builder.unableText("QR Payments");
                    builder.normalText("QR Payments");
                    break;


                case 6:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Food");
                    builder.unableText("Food");
                    builder.normalText("Food");

//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Cards");
//                    builder.unableText("Cards");
//                    builder.normalText("Cards");

                    break;


                case 7:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("E-Tickets");
                    builder.unableText("E-Tickets");
                    builder.normalText("E-Tickets");
                    break;

                case 8:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Logout");
                    builder.unableText("Logout");
                    builder.normalText("Logout");
                    break;


                default: {

                    break;
                }

            }

            bmb.addBuilder(builder);
        }
    }

    public void setMenus() {

        if (!menu.equals("N/A")) {

            if (menu.toLowerCase().contains("home")) {


            } else {
                MenuItem home = navMenu.add(0, R.id.home, 0, "Home");
                home.setIcon(R.drawable.home);

            }

            if (menu.toLowerCase().contains("accounts")) {

            } else {

                MenuItem accounts = navMenu.add(0, R.id.accounts, 0, "Accounts");
                accounts.setIcon(R.drawable.accounts);

                MenuItem qrPayment = navMenu.add(0, R.id.qr_payment, 0, "QR Payment");
                qrPayment.setIcon(R.drawable.qr);
            }


            if (menu.toLowerCase().contains("cards management")) {

            } else {

                MenuItem cardManagement = navMenu.add(0, R.id.cardManagement, 0, "Cards Management");
                cardManagement.setIcon(R.drawable.credit_card);

            }

            if (menu.toLowerCase().contains("beneficiary management")) {

            } else {

                MenuItem beneManagement = navMenu.add(0, R.id.manageBene, 0, "Beneficiary Management");
                beneManagement.setIcon(R.drawable.beneficiary);
            }

            if (menu.toLowerCase().contains("friends management")) {

            } else {

                MenuItem friends = navMenu.add(0, R.id.manageFriends, 0, "Friends Management");
                friends.setIcon(R.drawable.friends);
            }

            if (menu.toLowerCase().contains("biller management")) {

            } else {

                MenuItem billPayments = navMenu.add(0, R.id.billPayments, 0, "Biller Management");
                billPayments.setIcon(R.drawable.receipt);

            }

            if (menu.toLowerCase().contains("feedback")) {

            } else {

                MenuItem feedback = navMenu.add(0, R.id.feedback, 0, "Customer Feedback");
                feedback.setIcon(R.drawable.chat_smiley);

            }

        } else {

            Menu firstGroup = navigationView.getMenu();
            MenuItem home = firstGroup.add(R.id.first_group, R.id.home, 0, "Home");
            home.setIcon(R.drawable.home);

            MenuItem accounts = firstGroup.add(R.id.first_group, R.id.accounts, 0, "Accounts");
            accounts.setIcon(R.drawable.accounts);

            MenuItem qr = firstGroup.add(R.id.first_group, R.id.qr_payment, 0, "QR Payment");
            qr.setIcon(R.drawable.qr);

            MenuItem cardManagement = firstGroup.add(R.id.first_group, R.id.cardManagement, 0, "Cards Management");
            cardManagement.setIcon(R.drawable.credit_card);

            firstGroup.setGroupCheckable(R.id.first_group, true, true);
            firstGroup.setGroupVisible(R.id.first_group, true);

            Menu secondGroup = navigationView.getMenu();
            MenuItem ticket = secondGroup.add(R.id.second_group, R.id.ticket, 0, "E-Tickets");
            ticket.setIcon(R.drawable.ticket);

            MenuItem food = secondGroup.add(R.id.second_group, R.id.food, 0, "Food");
            food.setIcon(R.drawable.eat_mubarak_icon_left);

            MenuItem beneManagement = secondGroup.add(R.id.second_group, R.id.manageBene, 0, "Beneficiary Management");
            beneManagement.setIcon(R.drawable.beneficiary);

            MenuItem friends = secondGroup.add(R.id.second_group, R.id.manageFriends, 0, "Friends Management");
            friends.setIcon(R.drawable.friends);

            MenuItem billPayments = secondGroup.add(R.id.second_group, R.id.billPayments, 0, "Biller Management");
            billPayments.setIcon(R.drawable.receipt);
            secondGroup.setGroupCheckable(R.id.second_group, true, true);
            secondGroup.setGroupVisible(R.id.second_group, true);

            Menu thirdGroup = navigationView.getMenu();
            MenuItem location = thirdGroup.add(R.id.third_group, R.id.location, 0, "Locate Us");
            location.setIcon(R.drawable.location);

            Menu fourthGroup = navigationView.getMenu();
            MenuItem legal = fourthGroup.add(R.id.fourth_group, R.id.legal, 0, "Settings");
            legal.setIcon(R.drawable.settingsblack);

            MenuItem feedback = fourthGroup.add(R.id.fourth_group, R.id.feedback, 0, "Customer Feedback");
            feedback.setIcon(R.drawable.chat_smiley);

            MenuItem contact = fourthGroup.add(R.id.fourth_group, R.id.contact, 0, "Contact Us");
            contact.setIcon(R.drawable.phone);

        }
    }

    public void ChatMessages() {

        try {

            if (helper.isNetworkAvailable()) {
//                pd.show();
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSigns_UserType", _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.chatMessagesURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ChatHashMapResult").getString("Status_Code").equals("00")) {


                                                JSONArray chatMesasges = new JSONArray(jsonObject.getJSONObject("Signs_ChatHashMapResult").getString("ChatHashMap"));

                                                if (chatMesasges.length() > 0) {


                                                    int counter = 0;
                                                    for (int i = 0; i < chatMesasges.length(); i++) {


                                                        try {


                                                            JSONObject jsonObj = new JSONObject(chatMesasges.get(i).toString());

                                                            Message message = new Message();
                                                            message.setMessage(jsonObj.getString("Message"));
                                                            message.setKeyword(jsonObj.getString("Keyword"));
                                                            message.setTarget(jsonObj.getString("Target"));
                                                            messages.add(message);


//                                                        chatHashMap.put(jsonObj.getString("Keyword").toLowerCase(), messages.size() - 1);
                                                            String[] soundVal = jsonObj.getString("Advance_SoundVal").split(";");
                                                            for (int j = 0; j < soundVal.length; j++) {
                                                                ++counter;
                                                                unsortedHashMap = new LinkedHashMap<>();
                                                                unsortedHashMap.put(soundVal[j], i);
                                                                chatHashMap.put(counter, unsortedHashMap);
                                                            }


                                                        } catch (Exception e) {

                                                            e.printStackTrace();
//                                                        pd.dismiss();
//                                                        utils.showDilogForError(jsonObject.getJSONObject("Signs_ChatHashMapResult").getString("Status_Description"), "ERROR");

                                                        }
                                                    }


//                                                pd.dismiss();
                                                } else {

//                                                pd.dismiss();
                                                }


                                            } else if (jsonObject.getJSONObject("Signs_ChatHashMapResult").getString("Status_Code").equals("54")) {

                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ChatHashMapResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else {
//                                            pd.dismiss();
//                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_ChatHashMapResult").getString("Status_Description"), "ERROR");

                                            }

                                        } else {

                                            if (!isShown) {

                                                utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            }
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sSigns_UserType", constants.sharedPreferences.getString("type", "N/A"));
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };


                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
//                    pd.dismiss();
//                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }

            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
//                pd.dismiss();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

//            pd.dismiss();
//            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    public void CheckForTarget(String message, String mess) {

        String[] multiMessages = mess.split("\\;");
        final String randomStr = multiMessages[new Random().nextInt(multiMessages.length)];

        message = message.toLowerCase();

        Log.d("message", message);
        if (message.contains("fund transfer") || message.contains("funds transfer")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Funds_Transfer_Pressed", new Bundle());


            count = 0;
            relativeLayout2.setVisibility(View.VISIBLE);
            relativeLayout.setVisibility(View.GONE);
            Handler handler = new Handler();

            if (!userType.equals("N/A") && userType.equals("NON_SAMBA")) {

                handler.postDelayed(new Runnable() {
                    public void run() {

                        relativeLayout2.setVisibility(View.VISIBLE);
                        relativeLayout.setVisibility(View.GONE);

                        sendMessage(nonSamba, "");

                    }
                }, 1000);

            } else if (!userType.equals("N/A") && userType.equals("SAMBA")) {

                handler.postDelayed(new Runnable() {
                    public void run() {


                        sendMessage(ReplacingVariables(randomStr), "");


                    }
                }, 1000);

                Handler handler1 = new Handler();
                handler1.postDelayed(new Runnable() {
                    public void run() {

                        stringArrayList = new ArrayList<>();
                        stringArrayList.add("Own Accounts");
                        stringArrayList.add("Transfer within Samba");
                        stringArrayList.add("Other Banks");
                        horizontalListAdapter = new CustomHorizontalListAdapter(stringArrayList, HomeScreen.this);
                        listView.setAdapter(horizontalListAdapter);
                        tagshorizontalListViewLayout.setVisibility(View.VISIBLE);

                    }
                }, 2500);
            }
        } else if (message.contains("balance inquiry")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Balance_Inquiry_Pressed", new Bundle());
            count = 0;
            Handler handler = new Handler();

            if (!userType.equals("N/A") && userType.equals("NON_SAMBA")) {

                handler.postDelayed(new Runnable() {
                    public void run() {

                        relativeLayout2.setVisibility(View.VISIBLE);
                        relativeLayout.setVisibility(View.GONE);

                        sendMessage(nonSamba, "");

                    }
                }, 1000);

            } else if (!userType.equals("N/A") && userType.equals("SAMBA")) {
                handler.postDelayed(new Runnable() {
                    public void run() {
                        sendMessage(ReplacingVariables(randomStr), "");
                        relativeLayout2.setVisibility(View.VISIBLE);
                        relativeLayout.setVisibility(View.GONE);

                    }
                }, 1000);

                Handler handler1 = new Handler();
                handler1.postDelayed(new Runnable() {
                    public void run() {


                        GetAccountsList();
                    }
                }, 2500);

            }
        } else if (message.contains("pay bill")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Pay_Bill_Pressed", new Bundle());

            count = 0;
            relativeLayout2.setVisibility(View.VISIBLE);
            relativeLayout.setVisibility(View.GONE);
            Handler handler = new Handler();
            isPay = true;
            if (!userType.equals("N/A") && userType.equals("NON_SAMBA")) {

                handler.postDelayed(new Runnable() {
                    public void run() {

                        relativeLayout2.setVisibility(View.VISIBLE);
                        relativeLayout.setVisibility(View.GONE);

                        sendMessage(nonSamba, "");

                    }
                }, 1000);

            } else if (!userType.equals("N/A") && userType.equals("SAMBA")) {
                handler.postDelayed(new Runnable() {
                    public void run() {
                        sendMessage(ReplacingVariables(randomStr), "");
                        relativeLayout2.setVisibility(View.VISIBLE);
                        relativeLayout.setVisibility(View.GONE);

                    }
                }, 1000);

                Handler handler1 = new Handler();
                handler1.postDelayed(new Runnable() {
                    public void run() {


                        GetAccountsList();
                    }
                }, 2500);

            }
        } else if (message.contains("mini statement")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Mini_Statement_Pressed", new Bundle());

            count = 0;
            isMini = true;
            Handler handler = new Handler();

            if (!userType.equals("N/A") && userType.equals("NON_SAMBA")) {
                handler.postDelayed(new Runnable() {
                    public void run() {

                        relativeLayout2.setVisibility(View.VISIBLE);
                        relativeLayout.setVisibility(View.GONE);

                        sendMessage(nonSamba, "");

                    }
                }, 1000);

            } else if (!userType.equals("N/A") && userType.equals("SAMBA")) {
                handler.postDelayed(new Runnable() {
                    public void run() {
                        sendMessage(ReplacingVariables(randomStr), "");
                        relativeLayout2.setVisibility(View.VISIBLE);
                        relativeLayout.setVisibility(View.GONE);

                    }
                }, 1000);

                Handler handler1 = new Handler();
                handler1.postDelayed(new Runnable() {
                    public void run() {


                        GetAccountsList();
                    }
                }, 2500);

            }
        } else if (message.contains("atm locator")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Atm_Locator_Pressed", new Bundle());

            count = 0;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage(ReplacingVariables(randomStr), "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    Intent intent = new Intent(HomeScreen.this, ATMLocatorScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("chat", "chat");
                    startActivity(intent);

                }
            }, 2500);


        } else if (message.contains("link account")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Link_Account_Pressed", new Bundle());

            count = 0;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage(ReplacingVariables(randomStr), "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    Intent intent = new Intent(HomeScreen.this, ManageAccountsScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("chat", "chat");
                    startActivity(intent);

                }
            }, 2500);


        } else if (message.contains("contact us")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Contact_Us_Pressed", new Bundle());

            count = 0;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage(ReplacingVariables(randomStr), "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    Intent intent = new Intent(HomeScreen.this, ContactUsScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("chat", "chat");
                    startActivity(intent);

                }
            }, 2500);


        } else if (message.contains("change password")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Change_Password_Pressed", new Bundle());

            count = 0;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage(ReplacingVariables(randomStr), "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    Intent intent = new Intent(HomeScreen.this, PasswordScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);

                }
            }, 2500);


        } else if (message.contains("add biller")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Add_Biller_Pressed", new Bundle());

            count = 0;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage(ReplacingVariables(randomStr), "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    Intent intent = new Intent(HomeScreen.this, SelectCategoryScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);

                }
            }, 2500);


        } else if (message.contains("add bene")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Add_Bene_Pressed", new Bundle());

            count = 0;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage(ReplacingVariables(randomStr), "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    Intent intent = new Intent(HomeScreen.this, SelectBankScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);

                }
            }, 2500);


        } else if (message.contains("add friend")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Add_Friend_Pressed", new Bundle());
            count = 0;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage(ReplacingVariables(randomStr), "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    Intent intent = new Intent(HomeScreen.this, ImportFromAddressBook.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);

                }
            }, 2500);


        } else if (message.contains("request money")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Request_Money_Pressed", new Bundle());
            count = 0;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage(ReplacingVariables(randomStr), "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    Intent intent = new Intent(HomeScreen.this, FundTransferScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("requestFunds", "chatbot");
                    startActivity(intent);

                }
            }, 2500);


        } else if (message.contains("mobile top up")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Mobile_Topup_Pressed", new Bundle());
            count = 0;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage(ReplacingVariables(randomStr), "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    Intent intent = new Intent(HomeScreen.this, PayBillsScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("requestbill", "topup");
                    startActivity(intent);

                }
            }, 2500);


        } else if (message.contains("request mobile top up")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Request_Mobile_Topup_Pressed", new Bundle());
            count = 0;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage(ReplacingVariables(randomStr), "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    Intent intent = new Intent(HomeScreen.this, PayBillsScreen.class);
                    overridePendingTransition(0, 0);
                    intent.putExtra("requestbill", "requesttopup");
                    startActivity(intent);

                }
            }, 2500);


        } else if (message.contains("view cards")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_View_Cards_Pressed", new Bundle());
            count = 0;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage(ReplacingVariables(randomStr), "");
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);

                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {


                    Intent intent = new Intent(HomeScreen.this, ManageCardsScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);

                }
            }, 2500);


        } else if (message.contains("about this bot")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_About_The_Bot_Pressed", new Bundle());
            relativeLayout2.setVisibility(View.VISIBLE);
            relativeLayout.setVisibility(View.GONE);
            count = 0;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
//                    sendMessage("Here's what I can help you with:\n" +
//                            "\n" +
//                            " - View account balances and recent transaction\n" +
//                            " - Move money within own account or send money to your  beneficaries within Samba or other bank\n" +
//                            " - Find a bank branch or ATM \n" +
//                            "-  Inquire about product\n" +
//                            "\n" +
//                            "You can type your questions, but using my quick reply and menu navigation is sometimes more reliable.\n" +
//                            "\n" +
//                            "\n" +
//                            "I am still learning and i can provide you few features. But you can also access additional banking facilties quickly from burger menu on the top left of the application.", "");
                    sendMessage(ReplacingVariables(randomStr), "");

                }
            }, 1500);


        } else if (message.contains("logout") || message.contains("exit")) {

            FirebaseAnalytics.getInstance(HomeScreen.this).logEvent("Chat_Bot_Logout_Exit_Pressed", new Bundle());
            relativeLayout2.setVisibility(View.VISIBLE);
            relativeLayout.setVisibility(View.GONE);
            count = 0;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    sendMessage(ReplacingVariables(randomStr), "");


                }
            }, 1000);

            Handler handler1 = new Handler();
            handler1.postDelayed(new Runnable() {
                public void run() {

                    constants.editor.clear();
                    constants.editor.commit();
                    constants.editor.apply();
                    constants.nonSambaCredentialsEditor.clear();
                    constants.nonSambaCredentialsEditor.commit();
                    constants.nonSambaCredentialsEditor.apply();
                    constants.destinationAccountEditor.clear().commit();
                    constants.destinationAccountEditor.apply();
                    constants.sourceAccountEditor.clear().commit();
                    constants.sourceAccountEditor.apply();
                    constants.accountListEditor.clear().commit();
                    constants.accountListEditor.apply();
                    constants.accountsEditor.clear().commit();
                    constants.accountsEditor.apply();
                    constants.forgotPasswordEditor.clear().commit();
                    constants.forgotPasswordEditor.apply();
                    Intent intent = new Intent(HomeScreen.this, LoginScreen.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    finish();
                    finishAffinity();
                }
            }, 2000);

        } else {

            if (amount) {

                if (isPay) {

                    count = 0;
                    totalAmount = message;
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            sendMessage("Please select a purpose for your payment.", "");

//                        count = 0;
                        }
                    }, 1000);

                    Handler handler1 = new Handler();
                    handler1.postDelayed(new Runnable() {
                        public void run() {
                            GetPurposeBillList("chat");
//                        relativeLayout2.setVisibility(View.VISIBLE);
//                        relativeLayout.setVisibility(View.GONE);

                        }
                    }, 2500);
                } else {
                    count = 0;
                    totalAmount = message;
                    relativeLayout2.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.GONE);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            sendMessage("Please select a purpose for your transfer.", "");

//                        count = 0;
                        }
                    }, 1000);

                    Handler handler1 = new Handler();
                    handler1.postDelayed(new Runnable() {
                        public void run() {
                            if (isType.equals("OB")) {
                                GetPurposeListIBFT("chat");
                            } else
                                GetPurposeListIBFT("chat");
//                        relativeLayout2.setVisibility(View.VISIBLE);
//                        relativeLayout.setVisibility(View.GONE);

                        }
                    }, 2500);

                }
            } else {
                if (count == 0) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            sendMessage("Sorry, I didn’t get that. Please try again.", "");
                            relativeLayout2.setVisibility(View.VISIBLE);
                            relativeLayout.setVisibility(View.GONE);
                            count++;
                        }
                    }, 1000);

                }

                if (count == 1) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            sendMessage("Sorry, I am still learning. Please type help and you will get all the information needed.", "");
                            relativeLayout2.setVisibility(View.VISIBLE);
                            relativeLayout.setVisibility(View.GONE);
                            count++;
                        }
                    }, 1000);

                }


                if (count == 2) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            sendMessage("I still did not understand that. Please use Menu on top left corner for more options.", "");
                            relativeLayout2.setVisibility(View.VISIBLE);
                            relativeLayout.setVisibility(View.GONE);
                            count++;
                        }
                    }, 1000);

                }

            }
        }
    }

    private void showDialog(String message) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Logout:");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Logout();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                pd.dismiss();
            }
        });

        builder.show();
    }


    private String ReplacingVariables(String message) {

        String msg = null;
        try {

            String user = _crypt.decrypt(constants.sharedPreferences.getString("T24_FullName", " "), constants.getDeviceMac(), constants.getDeviceIMEI());
//            user = user.replaceAll("@br@", "\n");
            msg = message.replaceAll("@name@", user).replaceAll("@br@", "\n");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return msg;

    }

    public void SetFingerPrint() {

        try {

            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    final String userName = user;
                    final String md5Password = helper.md5(userName.toUpperCase() + constants.address);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sFingerPrintToken", md5Password);
                    params.put("sIsActive", "0");
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.setFingerprintURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_SetUserFingerPrintResult").getString("Status_Code").equals("00")) {


                                                constants.fingerPrintEditor.clear();
                                                constants.fingerPrintEditor.commit();
                                                constants.fingerPrintEditor.apply();
                                                constants.editor.putString("FingerPrintEnabled", "False");
                                                constants.editor.commit();
                                                constants.editor.apply();

                                            } else if (jsonObject.getJSONObject("Signs_SetUserFingerPrintResult").getString("Status_Code").equals("54")) {

                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_SetUserFingerPrintResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else {


                                            }
                                        } else {

                                            if (!isShown) {

                                                utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();


                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {


                                    } catch (Exception e) {
                                        e.printStackTrace();


                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sFingerPrintToken", md5Password);
//                            params.put("sIsActive", "0");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };
                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();

                }

            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();
            e.printStackTrace();


        }
    }

    public void GetAccountsList1() {

        try {
            if (helper.isNetworkAvailable()) {
                //135821 ammar
                //181936 hasan
                //constants.sharedPreferences.getString("T24_CIF", "N/A")

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sT24_CIF", _crypt.decrypt(constants.sharedPreferences.getString("T24_CIF", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sLegalIdentityType", _crypt.decrypt(constants.sharedPreferences.getString("LegalType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sSigns_UserType", _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sIsAllAccounts", "2");
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAccountListURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("00")) {

                                                int count = 0;
                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("AccountList"));

                                                boolean fcy = false;
                                                boolean lcy = false;


                                                if (jsonArray.length() > 0) {


                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                        try {

                                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
                                                            Account account = new Account();
                                                            account.setAccountNumber(jsonObj.getString("AccountID"));
                                                            account.setAccountName(jsonObj.getString("AccountTitle"));
                                                            account.setUserName(jsonObj.getString("AccountTitle"));
                                                            account.setBranchName(jsonObj.getString("BranchName"));
                                                            account.setAccountType(jsonObj.getString("Type"));
                                                            account.setAvailBal(jsonObj.getString("AvailBal"));
                                                            account.setBankCode(jsonObj.getString("BankCode"));
                                                            account.setBankID(jsonObj.getString("BankCode"));
                                                            account.setBranchCode(jsonObj.getString("BranchCode"));
                                                            account.setCurrency(jsonObj.getString("Currency"));
                                                            account.setCurrencyType(jsonObj.getString("Currency"));
                                                            account.setProductType(jsonObj.getString("Product"));
                                                            account.setBankColor(jsonObj.getString("BankColorCode"));
                                                            account.setIsActive(jsonObj.getString("Active"));
                                                            account.setId(jsonObj.getString("ID"));
                                                            account.setBranchShortName(jsonObj.getString("BranchNameShort"));
                                                            if (jsonObj.has("ACCOUNT_ALIAS")) {
                                                                account.setAlias(jsonObj.getString("ACCOUNT_ALIAS"));
                                                            }
                                                            constants.accountNumber = account.getAccountNumber();
                                                            accounts.add(account);

                                                            try {
                                                                if (i == 0) {
                                                                    String accountType = "";
                                                                    if (account.getAccountType() != null)
                                                                        accountType = account.getAccountType();
                                                                    FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("type", accountType);

                                                                    String productType = "";
                                                                    if (account.getProductType() != null)
                                                                        productType = account.getProductType();
                                                                    FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("Product", productType);

                                                                    String smsSubscribed = "";
                                                                    if (jsonObj.getString("SMSSubscribed") != null)
                                                                        smsSubscribed = jsonObj.getString("SMSSubscribed");
                                                                    if (smsSubscribed.isEmpty())
                                                                        smsSubscribed = "False";
                                                                    FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("SMSSubscribed", smsSubscribed);
                                                                }

                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }

                                                            try {
                                                                if (account.getCurrency().contains("PKR")) {
                                                                    lcy = true;
                                                                } else {
                                                                    fcy = true;
                                                                }
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }


//                                                        count++;

                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                        }

                                                    }

                                                    try {
                                                        FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("NumberOfAccounts", "" + accounts.size());
                                                        FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("LCY", "" + (lcy));
                                                        FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("FCY", "" + (fcy));
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }


                                                    try {
                                                        CryptLib _crypt = new CryptLib();
                                                        if (userType.equals("SAMBA")) {
                                                            if (constants.accountList.getString("accounts", "N/A") != null && constants.accountList.getString("accounts", "N/A") != "N/A") {

                                                            } else {
                                                                Gson gson = new Gson();
                                                                String json = gson.toJson(accounts);
                                                                constants.accountListEditor.putString("accounts", _crypt.encryptForParams(json, constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                                constants.accountListEditor.commit();
                                                                constants.accountListEditor.apply();
                                                            }
                                                        } else {
                                                            Gson gson = new Gson();
                                                            String json = gson.toJson(accounts);
                                                            if (constants.accountList.getString("accounts", "N/A") != null && constants.accountList.getString("accounts", "N/A") != "N/A") {

                                                            } else {
                                                                constants.accountListEditor.putString("accounts", _crypt.encryptForParams(json, constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                                constants.accountListEditor.commit();
                                                                constants.accountListEditor.apply();
                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                } else {


                                                }
                                            } else if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("54")) {

                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else {
                                            }

                                        } else {

                                            if (isShown) {

                                                utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();

                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {

                                    } catch (Exception e) {
                                        e.printStackTrace();

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sT24_CIF", constants.sharedPreferences.getString("T24_CIF", "N/A"));
//                            params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                            params.put("sLegalIdentityType", constants.sharedPreferences.getString("LegalType", "N/A"));
//                            params.put("sSigns_UserType", constants.sharedPreferences.getString("type", "N/A"));
//                            params.put("sIsAllAccounts", "2");
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();

                }

            } else {

            }
        } catch (Exception e) {


        }
    }

    public void GetbillerList() {


        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getBillerListURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_BillerListResult").getString("Status_Code").equals("00")) {

                                                billers = new ArrayList<>();
                                                billers.clear();
                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_BillerListResult").getString("Local_BillerList"));

                                                if (jsonArray.length() > 0) {

                                                    for (int i = 0; i < jsonArray.length(); i++) {

                                                        try {

                                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                            Biller biller = new Biller();
                                                            biller.setAlias(jsonObj.getString("Biller_Alias"));
                                                            biller.setAmountAfterDueDate(jsonObj.getString("Biller_AmountAfterDueDate"));
                                                            biller.setAmountBeforeDueDate(jsonObj.getString("Biller_AmountBeforeDueDate"));
                                                            biller.setBillStatus(jsonObj.getString("Biller_BillStatus"));
                                                            biller.setBillingMonth(jsonObj.getString("Biller_BillingMonth"));
                                                            biller.setAccountNumber(jsonObj.getString("Biller_Settlement_Accountno"));
                                                            biller.setCategoryName(jsonObj.getString("Biller_CategoryName"));
                                                            biller.setComments(jsonObj.getString("Biller_Comments"));
                                                            biller.setCompanyID(jsonObj.getString("Biller_CompanyID"));
                                                            biller.setCompanyName(jsonObj.getString("Biller_CompanyName"));
                                                            if (jsonObj.has("Biller_ConsumerName"))
                                                                biller.setConsumerName(jsonObj.getString("Biller_ConsumerName"));
                                                            else
                                                                biller.setConsumerName("N/A");

                                                            biller.setConsumerNumber(jsonObj.getString("Biller_ConsumerNumber"));
                                                            biller.setDueDate(jsonObj.getString("Biller_DueDate"));
                                                            biller.setBillerID(jsonObj.getString("Biller_ID"));
                                                            biller.setCategoryType(jsonObj.getString("Biller_TypeName"));
                                                            biller.setUserName(jsonObj.getString("Signs_Username"));
                                                            biller.setIsFavourite(jsonObj.getString("Biller_Favorite"));

//                                                        if (biller.getBillStatus().equals("Un-Paid") || biller.getBillStatus().equals("UnPaid") || biller.getBillStatus().equals("Partial Payment")) {

                                                            billers.add(biller);

//                                                        }


                                                        } catch (Exception ex) {

                                                            ex.printStackTrace();
                                                            pd.dismiss();
//                                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                            Error();
                                                        }

                                                    }


                                                    sambaHeader.setText("SELECT BILLER");

                                                    billListAdapter = new BillListAdapter(billers, HomeScreen.this);
                                                    accountListView.setAdapter(billListAdapter);
                                                    billListAdapter.notifyDataSetChanged();

                                                    horizontalListViewLayout.setVisibility(View.VISIBLE);
                                                    relativeLayout2.setVisibility(View.GONE);
                                                    relativeLayout.setVisibility(View.GONE);
                                                    pd.dismiss();


                                                } else {

                                                    pd.dismiss();
                                                    Error();
                                                }


                                            } else if (jsonObject.getJSONObject("Signs_BillerListResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillerListResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());

                                            } else if (jsonObject.getJSONObject("Signs_BillerListResult").getString("Status_Code").equals("14")) {
                                                pd.dismiss();
//                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "WARNING");
                                                Error("Biller", "You don't have any biller added on Samba Digital Banking. Please go to Manage Billers to add.");
                                            } else {

                                                pd.dismiss();
//                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_BillerListResult").getString("Status_Description"), "ERROR");
                                                Error();
                                            }

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        Error();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        Error();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        Error();
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
//                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    Error();
                }
            } else {
                pd.dismiss();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                Error();
            }
        } catch (Exception e) {


            pd.dismiss();
//            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            Error();
        }


    }

    public void BillInquiry(final int position, final Biller biller) {

        final int stanNumber = 6;

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sBillConsumerNumber", biller.getConsumerNumber());
                    params.put("sDistributorID", biller.getCompanyID());
                    params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.billInquiryURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            try {
                                                CryptLib _crypt = new CryptLib();

                                                if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("00")) {

                                                    constants.destinationAccountEditor.putString("isSelect", "Yes");
                                                    constants.destinationAccountEditor.putString("AmountAfterDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountAfterDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("AmountWithinDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountWithinDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillStatus", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillStatus"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillingMonth", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillingMonth"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("CompanyID", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("DueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("DueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("ConsumerName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("ConsumerName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("CompanyName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("RRN", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("RRN"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillConsumerNumber", _crypt.encryptForParams(biller.getConsumerNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("DistributorID", _crypt.encryptForParams(biller.getCompanyID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("distributorAccountNumber", _crypt.encryptForParams(biller.getAccountNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("alias", _crypt.encryptForParams(biller.getAlias(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("categoryType", _crypt.encryptForParams(biller.getCategoryType(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.commit();
                                                    constants.destinationAccountEditor.apply();
                                                    pd.dismiss();

                                                    amountAfterDueDate = jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountAfterDueDate");
                                                    amountBeforeDueDate = jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountWithinDueDate");
                                                    String dueDate = jsonObject.getJSONObject("Signs_BillInquiryResult").getString("DueDate");

                                                    sendMessage(biller.getAlias() + "\n" + biller.getConsumerNumber() + "\n" + biller.getCompanyName() + "\n" + "Status: " + jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillStatus"));
                                                    horizontalListViewLayout.setVisibility(View.GONE);
                                                    relativeLayout2.setVisibility(View.VISIBLE);

                                                    Calendar today = Calendar.getInstance();
                                                    today.set(Calendar.MILLISECOND, 0);
                                                    today.set(Calendar.SECOND, 0);
                                                    today.set(Calendar.MINUTE, 0);
                                                    today.set(Calendar.HOUR_OF_DAY, 0);
                                                    Date currentDate = today.getTime();
                                                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                                    Date mDate;

                                                    try {
                                                        if (!dueDate.equals("N/A")) {
                                                            mDate = sdf.parse(dueDate);
                                                            timeInMilliseconds = mDate.getTime();
                                                            currentTimeToMili = currentDate.getTime();
                                                            System.out.println("Date in milli :: " + timeInMilliseconds);
                                                        }
                                                    } catch (ParseException e) {
                                                        e.printStackTrace();
                                                        Error();
                                                    }

                                                    if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillStatus").toLowerCase().replaceAll("-", "").replaceAll(" ", "").equals("unpaid")) {

                                                        Handler handler9 = new Handler();
                                                        handler9.postDelayed(new Runnable() {
                                                            public void run() {

                                                                if (currentTimeToMili <= timeInMilliseconds) {


                                                                    payableAmount = amountBeforeDueDate;

                                                                } else if (currentTimeToMili > timeInMilliseconds) {

                                                                    payableAmount = amountAfterDueDate;

                                                                }
                                                                sendMessage("Your bill amount is " + payableAmount + ". Please confirm to proceed with Bill Payment.", "");
                                                                Handler handler10 = new Handler();
                                                                handler10.postDelayed(new Runnable() {
                                                                    public void run() {

                                                                        count = 0;
                                                                        amount = true;
                                                                        totalAmount = payableAmount;
                                                                        relativeLayout2.setVisibility(View.VISIBLE);
                                                                        relativeLayout.setVisibility(View.GONE);
                                                                        Handler handler = new Handler();
                                                                        handler.postDelayed(new Runnable() {
                                                                            public void run() {
                                                                                sendMessage("Please select a purpose for your payment.", "");

//                        count = 0;
                                                                            }
                                                                        }, 1500);

                                                                        Handler handler1 = new Handler();
                                                                        handler1.postDelayed(new Runnable() {
                                                                            public void run() {
                                                                                GetPurposeBillList("chat");
//                        relativeLayout2.setVisibility(View.VISIBLE);
//                        relativeLayout.setVisibility(View.GONE);

                                                                            }
                                                                        }, 2500);

                                                                    }
                                                                }, 1000);
                                                            }
                                                        }, 1000);

                                                    } else if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillStatus").toLowerCase().replaceAll("-", "").replaceAll(" ", "").equals("partialpayment")) {

                                                        Handler handler9 = new Handler();
                                                        handler9.postDelayed(new Runnable() {
                                                            public void run() {

                                                                if (currentTimeToMili <= timeInMilliseconds) {


                                                                    payableAmount = amountBeforeDueDate;

                                                                } else if (currentTimeToMili > timeInMilliseconds) {

                                                                    payableAmount = amountAfterDueDate;

                                                                }
                                                                sendMessage("Your bill amount is " + payableAmount + ". Please enter the amount you wish to pay.", "");
                                                                Handler handler2 = new Handler();
                                                                handler2.postDelayed(new Runnable() {
                                                                    public void run() {
                                                                        isSource = false;
                                                                        isFt = false;
                                                                        sendMessage("Ok. Please enter an amount.", "");
                                                                        relativeLayout2.setVisibility(View.VISIBLE);
                                                                        relativeLayout.setVisibility(View.GONE);
                                                                        amount = true;

                                                                    }
                                                                }, 1500);
                                                            }
                                                        }, 1000);


                                                    } else {

                                                        Handler handler8 = new Handler();
                                                        handler8.postDelayed(new Runnable() {
                                                            public void run() {


                                                                sendMessage("Your bill is either paid or blocked.", "");
                                                                Handler handler9 = new Handler();
                                                                handler9.postDelayed(new Runnable() {
                                                                    public void run() {


                                                                        sendMessage("Is there anything else i can help you with ?", "");
                                                                        amount = false;
                                                                        isFt = false;
                                                                        isSource = false;
                                                                        isPay = false;

                                                                    }
                                                                }, 1500);

                                                            }
                                                        }, 1000);


                                                    }


//                                            Handler handler1 = new Handler();
//                                            handler1.postDelayed(new Runnable() {
//                                                public void run() {
//
//
//                                                    GetAccountsList();
//                                                }
//                                            }, 500);


                                                } else if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("54")) {
                                                    pd.dismiss();
                                                    utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                                } else {

                                                    pd.dismiss();
//                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR");
                                                    Error();

                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        Error();

                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        Error();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                        Error();

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sBillConsumerNumber", biller.getConsumerNumber());
//                            params.put("sDistributorID", biller.getCompanyID());
//                            params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
//                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                    Error();

                }
            } else {
                pd.dismiss();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                Error();

            }
        } catch (Exception e) {


            pd.dismiss();
//            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            Error();

        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        isPay = false;
        isFt = false;
        isSource = false;
        amount = false;
        editText.setTypeface(Typeface.create("sans-serif", Typeface.NORMAL));
    }

    public String soundex(String s) {
        char[] x = s.toUpperCase().toCharArray();
        char firstLetter = x[0];
        if (s.equals(constants.strSpaceReplaceVal)) {

//            output.Append(SoundMatching_Test.clsConstants.strSpaceSoundEx);
            return constants.strSpaceSoundEx;

        } else {
            // convert letters to numeric code
            for (int i = 0; i < x.length; i++) {
                switch (x[i]) {
                    case 'B':
                    case 'F':
                    case 'P':
                    case 'V': {
                        x[i] = '1';
                        break;
                    }

                    case 'C':
                    case 'G':
                    case 'J':
                    case 'K':
                    case 'Q':
                    case 'S':
                    case 'X':
                    case 'Z': {
                        x[i] = '2';
                        break;
                    }

                    case 'D':
                    case 'T': {
                        x[i] = '3';
                        break;
                    }

                    case 'L': {
                        x[i] = '4';
                        break;
                    }

                    case 'M':
                    case 'N': {
                        x[i] = '5';
                        break;
                    }

                    case 'R': {
                        x[i] = '6';
                        break;
                    }

                    default: {
                        x[i] = '0';
                        break;
                    }
                }
            }

            // remove duplicates
            String output = "" + firstLetter;
            for (int i = 1; i < x.length; i++)
                if (x[i] != x[i - 1] && x[i] != '0')
                    output += x[i];

            // pad with 0's or truncate
            output = output + "000000";
            return output.substring(0, 6);
        }

//        return "";
    }


    public void Error() {

        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        confirmationLayout.setVisibility(View.GONE);
        tagshorizontalListViewLayout.setVisibility(View.GONE);
        horizontalListViewLayout.setVisibility(View.GONE);
        relativeLayout2.setVisibility(View.VISIBLE);
        amount = false;
        isPay = false;
        isFt = false;
        isSource = false;
        Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            public void run() {


                sendMessage(sorry, "");

            }
        }, 700);
    }

    public void Error(String type, final String msg) {

        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(100000000)});
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        confirmationLayout.setVisibility(View.GONE);
        tagshorizontalListViewLayout.setVisibility(View.GONE);
        horizontalListViewLayout.setVisibility(View.GONE);
        relativeLayout2.setVisibility(View.VISIBLE);
        amount = false;
        isPay = false;
        isFt = false;
        isSource = false;
        Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            public void run() {


                sendMessage(msg, "");

            }
        }, 700);
    }

    public void Logout() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSessionID", Constants.Shared_KEY);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.logoutURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Code").equals("00")) {
                                                constants.editor.clear();
                                                constants.editor.commit();
                                                constants.editor.apply();
                                                constants.nonSambaCredentialsEditor.clear();
                                                constants.nonSambaCredentialsEditor.commit();
                                                constants.nonSambaCredentialsEditor.apply();
                                                constants.destinationAccountEditor.clear().commit();
                                                constants.destinationAccountEditor.apply();
                                                constants.sourceAccountEditor.clear().commit();
                                                constants.sourceAccountEditor.apply();
                                                constants.accountListEditor.clear().commit();
                                                constants.accountListEditor.apply();
                                                constants.accountsEditor.clear().commit();
                                                constants.accountsEditor.apply();
                                                constants.forgotPasswordEditor.clear().commit();
                                                constants.forgotPasswordEditor.apply();
                                                constants.purposeBillList.clear().commit();
                                                constants.purposeBillList.apply();
                                                constants.purposePrepaidList.clear().commit();
                                                constants.purposePrepaidList.apply();
                                                constants.purposeListEditor.clear().commit();
                                                constants.purposeListEditor.apply();
                                                constants.reasonListEditor.clear().commit();
                                                constants.reasonListEditor.apply();
                                                constants.statusListEditor.clear().commit();
                                                constants.statusListEditor.apply();
                                                constants.channelListEditor.clear().commit();
                                                constants.channelListEditor.apply();

                                                pd.dismiss();
                                                Intent intent = new Intent(HomeScreen.this, LoginScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                finish();
                                                finishAffinity();


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());

                                            }

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sSessionID", Constants.Shared_KEY);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity", "WARNING");


            }
        } catch (Exception e) {

            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity", "WARNING");


        }
    }

    public class CustomHorizontalListAdapter extends BaseAdapter {
        ArrayList<String> billerList;
        LayoutInflater layoutInflater;
        View row;
        Context context;
        TextView userName;


        public CustomHorizontalListAdapter(ArrayList<String> billerArrayList, Context mContext) {
            billerList = billerArrayList;
            context = mContext;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return billerList.size();
        }

        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.hori_list_item, parent, false);
            }

            if (amount) {

                if (isPay) {
                    headerTxt.setText("PURPOSE OF PAYMENT");
                } else {
                    headerTxt.setText("PURPOSE OF TRANSFER");
                }
            } else
                headerTxt.setText("TRANSFER TYPE");

            userName = (TextView) convertView.findViewById(R.id.userName);
            userName.setText(billerList.get(position));


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (amount) {


                        tagshorizontalListViewLayout.setVisibility(View.GONE);
                        relativeLayout2.setVisibility(View.VISIBLE);
                        relativeLayout.setVisibility(View.GONE);
                        sendMessage(billerList.get(position));
                        remarks = billerList.get(position);

                        Handler handler3 = new Handler();
                        handler3.postDelayed(new Runnable() {
                            public void run() {
                                sendMessage("Ok. Please wait...", "");
                                amount = true;
                            }
                        }, 1000);

                        Handler handler2 = new Handler();
                        handler2.postDelayed(new Runnable() {
                            public void run() {
                                relativeLayout2.setVisibility(View.GONE);
                                confirmationLayout.setVisibility(View.VISIBLE);

                            }
                        }, 2500);


//
//
//                        relativeLayout.setVisibility(View.VISIBLE);
//                        relativeLayout2.setVisibility(View.INVISIBLE);
//                        editText.requestFocus();
//                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);

                    } else {

                        tagshorizontalListViewLayout.setVisibility(View.GONE);
                        relativeLayout2.setVisibility(View.VISIBLE);
                        relativeLayout.setVisibility(View.GONE);
                        sendMessage(billerList.get(position));
                        if (billerList.get(position).equals("Own Accounts")) {

                            isType = "OA";
                            ftType = "MM";

                        } else if (billerList.get(position).equals("Transfer within Samba")) {

                            isType = "TWS";
                            ftType = "SM";
                        } else if (billerList.get(position).equals("Other Banks")) {

                            isType = "OB";
                            ftType = "SM";
                        }

                        isFt = true;

//                    isSource = true;
                        Handler handler2 = new Handler();
                        handler2.postDelayed(new Runnable() {
                            public void run() {
                                sendMessage("Ok. Please select a Source Account.", "");

                            }
                        }, 1000);

                        Handler handler1 = new Handler();
                        handler1.postDelayed(new Runnable() {
                            public void run() {


                                GetAccountsList();
                            }
                        }, 2500);
                    }

                    billerList.clear();
                    notifyDataSetChanged();
                }
            });


            return convertView;
        }


    }

    public class DestinationAccountHorizontalListAdapter extends BaseAdapter {
        ArrayList<UserModel> userModels;
        LayoutInflater layoutInflater;
        View row;
        Context context;
        TextView userName;
        TextView accountTitleTxt, accountNumberTxt, branchTxt, accountType, samba, currencyType;
        RelativeLayout account;

        public DestinationAccountHorizontalListAdapter(ArrayList<UserModel> userModelArrayList, Context mContext) {
            userModels = userModelArrayList;
            context = mContext;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return userModels.size();
        }

        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.account_list_item, parent, false);
            }

            samba = (TextView) convertView.findViewById(R.id.samba);
            accountTitleTxt = (TextView) convertView.findViewById(R.id.accountTitleTxt);
            accountNumberTxt = (TextView) convertView.findViewById(R.id.accountNumberTxt);
            branchTxt = (TextView) convertView.findViewById(R.id.branchTxt);
            account = (RelativeLayout) convertView.findViewById(R.id.account);
            accountType = (TextView) convertView.findViewById(R.id.accountType);
            accountType.setVisibility(View.INVISIBLE);
            currencyType = (TextView) convertView.findViewById(R.id.currencyType);
            currencyType.setVisibility(View.GONE);

            accountTitleTxt.setText(userModels.get(position).getAccountTitle());
            if (userModels.get(position).getBankName().equals("Samba Bank Limited")) {

//                accountNumberTxt.setText(utils.getMaskedString(userModels.get(position).getAccountNumber()));
//                samba.setText(userModels.get(position).getBankName());
            } else {
                samba.setText(userModels.get(position).getBankShortCode());
                samba.setTextColor(Color.parseColor(userModels.get(position).getBankColor()));
//                accountNumberTxt.setText(utils.getMaskedStringForOther(userModels.get(position).getAccountNumber()));
            }

            if (userModels.get(position).getAccountNumber().startsWith("PK")) {

                accountNumberTxt.setText(utils.getMaskedStringForIBAN(userModels.get(position).getAccountNumber()));

            } else {

                if (userModels.get(position).getBankName().equals("Samba Bank Limited")) {

                    accountNumberTxt.setText(utils.getMaskedString(userModels.get(position).getAccountNumber()));

                } else {

                    accountNumberTxt.setText(userModels.get(position).getAccountNumber());
                }
            }

            branchTxt.setText(userModels.get(position).getUserName());
//            accountType.setText(accountArrayList.get(position).getProductType());

            account.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    destinationAccount = userModels.get(position).getAccountNumber();
                    destinationTitle = userModels.get(position).getUserTitle();
                    destinationCurrency = userModels.get(position).getCurrency();
                    destinationBankCode = userModels.get(position).getBankCode();
                    if (!TextUtils.isEmpty(userModels.get(position).getUserName())) {

                        destinationAlias = userModels.get(position).getUserName();

                    }
                    beneType = userModels.get(position).getBeneType();
                    beneID = userModels.get(position).getBeneID();
                    email = userModels.get(position).getEmail();
                    mobile = userModels.get(position).getMobileNumber();
                    sendMessage(utils.getMaskedString(userModels.get(position).getAccountNumber()) + "\n" + userModels.get(position).getAccountTitle() + "\n" + userModels.get(position).getBankName());
                    horizontalListViewLayout.setVisibility(View.GONE);
                    relativeLayout2.setVisibility(View.VISIBLE);
                    Handler handler2 = new Handler();
                    handler2.postDelayed(new Runnable() {
                        public void run() {
                            sendMessage("Ok. Please enter an amount.", "");
                            relativeLayout2.setVisibility(View.VISIBLE);
                            relativeLayout.setVisibility(View.GONE);
                            amount = true;

                        }
                    }, 1500);

//                    Handler handler1 = new Handler();
//                    handler1.postDelayed(new Runnable() {
//                        public void run() {
//
//
//                            GetAccountsList();
//                        }
//                    }, 2000);


                    userModels.clear();
                    notifyDataSetChanged();
                }
            });


            return convertView;
        }


    }

    public class BillListAdapter extends BaseAdapter {
        ArrayList<Biller> billerModels;
        LayoutInflater layoutInflater;
        View row;
        Context context;
        TextView userName;
        TextView accountTitleTxt, accountNumberTxt, branchTxt, accountType, samba, useAccount, currencyType;
        RelativeLayout account;

        public BillListAdapter(ArrayList<Biller> userModelArrayList, Context mContext) {
            billerModels = userModelArrayList;
            context = mContext;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return billerModels.size();
        }

        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.account_list_item, parent, false);
            }

            samba = (TextView) convertView.findViewById(R.id.samba);
            accountTitleTxt = (TextView) convertView.findViewById(R.id.accountTitleTxt);
            accountNumberTxt = (TextView) convertView.findViewById(R.id.accountNumberTxt);
            branchTxt = (TextView) convertView.findViewById(R.id.branchTxt);
            account = (RelativeLayout) convertView.findViewById(R.id.account);
            accountType = (TextView) convertView.findViewById(R.id.accountType);
            useAccount = (TextView) convertView.findViewById(R.id.useAccount);
            currencyType = (TextView) convertView.findViewById(R.id.currencyType);
            accountType.setVisibility(View.INVISIBLE);
            currencyType.setVisibility(View.GONE);

            samba.setText(billerModels.get(position).getAlias());
            accountNumberTxt.setText(billerModels.get(position).getConsumerNumber());
            accountTitleTxt.setText(billerModels.get(position).getCompanyName());
            branchTxt.setTextColor(getResources().getColor(R.color.lgreen));
            if (billerModels.get(position).getBillStatus().equals("Un-Paid") || billerModels.get(position).getBillStatus().equals("UnPaid") || billerModels.get(position).getBillStatus().equals("Partial Payment")) {

                branchTxt.setText("UNPAID");

            } else if (billerModels.get(position).getBillStatus().toLowerCase().equals("paid")) {

                branchTxt.setText("PAID");
            } else if (billerModels.get(position).getBillStatus().toLowerCase().equals("blocked")) {

                branchTxt.setTextColor(getResources().getColor(R.color.tabunderline));
                branchTxt.setText("BLOCKED");
            }
            useAccount.setText("Use This Biller");


//            accountTitleTxt.setText(userModels.get(position).getAccountTitle());
//            if (userModels.get(position).getBankName().equals("Samba Bank Limited")) {
//
////                accountNumberTxt.setText(utils.getMaskedString(userModels.get(position).getAccountNumber()));
////                samba.setText(userModels.get(position).getBankName());
//            } else {
//                samba.setText(userModels.get(position).getBankShortCode());
//                samba.setTextColor(Color.parseColor(userModels.get(position).getBankColor()));
////                accountNumberTxt.setText(utils.getMaskedStringForOther(userModels.get(position).getAccountNumber()));
//            }
//
//            if (userModels.get(position).getAccountNumber().startsWith("PK")) {
//
//                accountNumberTxt.setText(utils.getMaskedStringForIBAN(userModels.get(position).getAccountNumber()));
//
//            } else {
//
//                if (userModels.get(position).getBankName().equals("Samba Bank Limited")) {
//
//                    accountNumberTxt.setText(utils.getMaskedString(userModels.get(position).getAccountNumber()));
//
//                } else {
//
//                    accountNumberTxt.setText(userModels.get(position).getAccountNumber());
//                }
//            }

//            branchTxt.setText(userModels.get(position).getUserName());
//            accountType.setText(accountArrayList.get(position).getProductType());

            account.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    destinationAccount = userModels.get(position).getAccountNumber();
//                    destinationTitle = userModels.get(position).getUserTitle();
//                    destinationCurrency = userModels.get(position).getCurrency();
//                    destinationBankCode = userModels.get(position).getBankCode();
//                    beneType = userModels.get(position).getBeneType();
//                    beneID = userModels.get(position).getBeneID();
//                    email = userModels.get(position).getEmail();
//                    mobile = userModels.get(position).getMobileNumber();

                    Handler handler2 = new Handler();
                    handler2.postDelayed(new Runnable() {
                        public void run() {

                            BillInquiry(position, billerModels.get(position));
//                            sendMessage("Ok. Please enter an amount.", "");
//                            relativeLayout2.setVisibility(View.VISIBLE);
//                            relativeLayout.setVisibility(View.GONE);
//                            amount = true;

                        }
                    }, 200);

//                    Handler handler1 = new Handler();
//                    handler1.postDelayed(new Runnable() {
//                        public void run() {
//
//
//                            GetAccountsList();
//                        }
//                    }, 2000);


//                    billerModels.clear();
                    notifyDataSetChanged();
                }
            });


            return convertView;
        }


    }

    public class AccoutsCustomHorizontalListAdapter extends BaseAdapter {
        ArrayList<Account> accountArrayList;
        LayoutInflater layoutInflater;
        View row;
        Context context;
        TextView accountTitleTxt, accountNumberTxt, branchTxt, accountType, sambaTxt, currencyType;
        RelativeLayout account;


        public AccoutsCustomHorizontalListAdapter(ArrayList<Account> accounts, Context mContext) {
            accountArrayList = accounts;
            context = mContext;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return accountArrayList.size();
        }

        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.account_list_item, parent, false);
            }

            if (!TextUtils.isEmpty(isType) && !isType.equals("")) {
                if (isType.equals("OA")) {

                    if (isSource) {

                        sambaHeader.setText("SELECT BENEFICIARY");
                    } else {

                        sambaHeader.setText("SELECT ACCOUNT");
                    }


                } else {
                    sambaHeader.setText("SELECT ACCOUNT");

                }
            } else sambaHeader.setText("SELECT ACCOUNT");


            accountTitleTxt = (TextView) convertView.findViewById(R.id.accountTitleTxt);
            accountNumberTxt = (TextView) convertView.findViewById(R.id.accountNumberTxt);
            branchTxt = (TextView) convertView.findViewById(R.id.branchTxt);
            accountType = (TextView) convertView.findViewById(R.id.accountType);
            account = (RelativeLayout) convertView.findViewById(R.id.account);
            sambaTxt = (TextView) convertView.findViewById(R.id.samba);
            currencyType = (TextView) convertView.findViewById(R.id.currencyType);
            sambaTxt.setText("SAMBA");
            if (!TextUtils.isEmpty(accountArrayList.get(position).getCurrencyType()) && !accountArrayList.get(position).getCurrencyType().equals("") && !accountArrayList.get(position).getCurrencyType().equals(null))
                currencyType.setText(accountArrayList.get(position).getCurrencyType());
            else
                currencyType.setText(accountArrayList.get(position).getCurrency());

            sambaTxt.setTextColor(Color.parseColor("#034ea2"));
            accountTitleTxt.setText(accountArrayList.get(position).getAccountName());
            accountNumberTxt.setText(utils.getMaskedString(accountArrayList.get(position).getAccountNumber()));
            branchTxt.setText(accountArrayList.get(position).getBranchName());
            accountType.setVisibility(View.VISIBLE);
            accountType.setText(accountArrayList.get(position).getProductType());


            final View finalConvertView = convertView;
            account.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendMessage(utils.getMaskedString(accountArrayList.get(position).getAccountNumber()) + "\n" + accountArrayList.get(position).getAccountName() + "\n" + accountArrayList.get(position).getBranchName() + "\n" + accountArrayList.get(position).getProductType());
                    horizontalListViewLayout.setVisibility(View.GONE);
                    relativeLayout2.setVisibility(View.VISIBLE);
                    if (!isMini) {

                        if (isFt) {


                            if (isSource) {

//                                sendMessage(utils.getMaskedString(userModels.get(position).getAccountNumber()) + "\n" + userModels.get(position).getAccountTitle() + "\n" + userModels.get(position).getBankName());
//                                horizontalListViewLayout.setVisibility(View.GONE);
//                                relativeLayout2.setVisibility(View.VISIBLE);

                                destinationAccount = accountArrayList.get(position).getAccountNumber();
                                destinationTitle = accountArrayList.get(position).getAccountName();
                                destinationCurrency = accountArrayList.get(position).getCurrency();
                                destinationBankCode = accountArrayList.get(position).getBankCode();
                                DestProductType = accountArrayList.get(position).getProductType();

                                Handler handler2 = new Handler();
                                handler2.postDelayed(new Runnable() {
                                    public void run() {
                                        isSource = false;
                                        isFt = false;
                                        sendMessage("Ok. Please enter an amount.", "");
                                        relativeLayout2.setVisibility(View.VISIBLE);
                                        relativeLayout.setVisibility(View.GONE);
                                        amount = true;

                                    }
                                }, 1500);

                            } else {

                                sourceTile = accountArrayList.get(position).getAccountName();
                                sourceNumber = accountArrayList.get(position).getAccountNumber();
                                sourceBankCode = accountArrayList.get(position).getBankCode();
                                sourceCurrency = accountArrayList.get(position).getCurrencyType();
                                sourceBranchCode = accountArrayList.get(position).getBranchCode();
                                SourceProductType=accountArrayList.get(position).getProductType();
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    public void run() {

                                        sendMessage("Ok. Please select a Destination Account.", "");
                                    }
                                }, 1000);


                                if (isType.equals("OA")) {

                                    Handler handler1 = new Handler();
                                    handler1.postDelayed(new Runnable() {
                                        public void run() {

                                            isSource = true;
                                            GetAccountsList();
                                        }
                                    }, 2500);

                                } else if (isType.equals("TWS")) {
                                    isFt = false;
                                    Handler handler1 = new Handler();
                                    handler1.postDelayed(new Runnable() {
                                        public void run() {

                                            GetAccountsAndBeneList();
                                        }
                                    }, 2500);

                                } else if (isType.equals("OB")) {
                                    isFt = false;
                                    Handler handler1 = new Handler();
                                    handler1.postDelayed(new Runnable() {
                                        public void run() {

                                            GetAccountsAndBeneList();
                                        }
                                    }, 2500);

                                }
                            }

                        } else {

                            sourceNumber = accountArrayList.get(position).getAccountNumber();
                            sourceCurrency = accountArrayList.get(position).getCurrencyType();
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {

                                    sendMessage("Ok. Just a second", "");
                                }
                            }, 1000);

                            Handler handler6 = new Handler();
                            handler6.postDelayed(new Runnable() {
                                public void run() {

                                    LatestBalance(sourceNumber, position, sourceCurrency);

                                }
                            }, 2500);
                        }
                    } else {

                        sourceNumber = accountArrayList.get(position).getAccountNumber();
                        sourceBranchCode = accountArrayList.get(position).getBranchCode();
                        isMini = false;
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {

                                sendMessage("Ok. Just a second", "");
                            }
                        }, 1000);

                        Handler handler5 = new Handler();
                        handler5.postDelayed(new Runnable() {
                            public void run() {

                                AccountDetails(sourceNumber, sourceBranchCode);


                            }
                        }, 2500);
                    }

//                    Handler handler6 = new Handler();
//                    handler6.postDelayed(new Runnable() {
//                        public void run() {

                    accountArrayList.clear();
                    notifyDataSetChanged();

//                        }
//                    }, 1000);

                }
            });


            return convertView;
        }


    }

    public class AccountsForBillerCustomHorizontalListAdapter extends BaseAdapter {
        ArrayList<Account> accountArrayList;
        LayoutInflater layoutInflater;
        View row;
        Context context;
        TextView accountTitleTxt, accountNumberTxt, branchTxt, accountType, sambaTxt, currencyType;
        RelativeLayout account;


        public AccountsForBillerCustomHorizontalListAdapter(ArrayList<Account> accounts, Context mContext) {
            accountArrayList = accounts;
            context = mContext;
        }

        public int getCount() {
            // TODO Auto-generated method stub
            return accountArrayList.size();
        }

        public Object getItem(int arg0) {
            // TODO Auto-generated method stub
            return null;
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = LayoutInflater.from(context).
                        inflate(R.layout.account_list_item, parent, false);
            }


            sambaHeader.setText("SELECT ACCOUNT");


            accountTitleTxt = (TextView) convertView.findViewById(R.id.accountTitleTxt);
            accountNumberTxt = (TextView) convertView.findViewById(R.id.accountNumberTxt);
            branchTxt = (TextView) convertView.findViewById(R.id.branchTxt);
            accountType = (TextView) convertView.findViewById(R.id.accountType);
            currencyType = (TextView) convertView.findViewById(R.id.currencyType);
            account = (RelativeLayout) convertView.findViewById(R.id.account);
            sambaTxt = (TextView) convertView.findViewById(R.id.samba);
            sambaTxt.setText("SAMBA");
            sambaTxt.setTextColor(Color.parseColor("#034ea2"));
            accountTitleTxt.setText(accountArrayList.get(position).getAccountName());
            accountNumberTxt.setText(utils.getMaskedString(accountArrayList.get(position).getAccountNumber()));
            branchTxt.setText(accountArrayList.get(position).getBranchName());
            if (!TextUtils.isEmpty(accountArrayList.get(position).getCurrencyType()) && !accountArrayList.get(position).getCurrencyType().equals("") && !accountArrayList.get(position).getCurrencyType().equals(null))
                currencyType.setText(accountArrayList.get(position).getCurrencyType());
            else
                currencyType.setText(accountArrayList.get(position).getCurrency());

            accountType.setVisibility(View.VISIBLE);
            accountType.setText(accountArrayList.get(position).getProductType());

            account.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendMessage(utils.getMaskedString(accountArrayList.get(position).getAccountNumber()) + "\n" + accountArrayList.get(position).getAccountName() + "\n" + accountArrayList.get(position).getBranchName() + "\n" + accountArrayList.get(position).getProductType());
                    horizontalListViewLayout.setVisibility(View.GONE);
                    relativeLayout2.setVisibility(View.VISIBLE);


                    sourceTile = accountArrayList.get(position).getAccountName();
                    sourceNumber = accountArrayList.get(position).getAccountNumber();
                    sourceBankCode = accountArrayList.get(position).getBankCode();
                    sourceCurrency = accountArrayList.get(position).getCurrencyType();
                    sourceBranchCode = accountArrayList.get(position).getBranchCode();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            sendMessage("Ok. Please select a Biller.", "");
                            Handler handler1 = new Handler();
                            handler1.postDelayed(new Runnable() {
                                public void run() {

                                    GetbillerList();
                                }
                            }, 1500);
                        }
                    }, 1500);


//                    Handler handler6 = new Handler();
//                    handler6.postDelayed(new Runnable() {
//                        public void run() {

                    accountArrayList.clear();
                    notifyDataSetChanged();

//                        }
//                    }, 1000);

                }
            });


            return convertView;
        }


    }


    public void GetManageAccountsList() {

        try {
//            pd.show();
            if (helper.isNetworkAvailable()) {
                //135821
                //constants.sharedPreferences.getString("T24_CIF", "N/A")

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sT24_CIF", _crypt.decrypt(constants.sharedPreferences.getString("T24_CIF", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sLegalIdentityType", _crypt.decrypt(constants.sharedPreferences.getString("LegalType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sSigns_UserType", _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sIsAllAccounts", "1");
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                    HttpsTrustManager.allowMySSL(HomeScreen.this);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAccountListURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("00")) {


                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("AccountList"));

                                                if (jsonArray.length() > 1) {


//                                                    for (int i = 0; i < jsonArray.length(); i++) {
//                                                        try {
//
//                                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());
//                                                            Account account = new Account();
//                                                            account.setAccountNumber(jsonObj.getString("AccountID"));
//                                                            account.setAccountName(jsonObj.getString("AccountTitle"));
//                                                            account.setUserName(jsonObj.getString("AccountTitle"));
//                                                            account.setBranchName(jsonObj.getString("BranchName"));
//                                                            account.setAccountType(jsonObj.getString("Type"));
//                                                            account.setAvailBal(jsonObj.getString("AvailBal"));
//                                                            account.setBankCode(jsonObj.getString("BankCode"));
//                                                            account.setBankID(jsonObj.getString("BankCode"));
//                                                            account.setBranchCode(jsonObj.getString("BranchCode"));
//                                                            account.setCurrency(jsonObj.getString("Currency"));
//                                                            account.setCurrencyType(jsonObj.getString("Currency"));
//                                                            account.setProductType(jsonObj.getString("Product"));
//                                                            account.setBankColor(jsonObj.getString("BankColorCode"));
//                                                            account.setIsActive(jsonObj.getString("Active"));
//                                                            account.setId(jsonObj.getString("ID"));
//                                                            account.setBranchShortName(jsonObj.getString("BranchNameShort"));
//                                                            accounts.add(account);
//
//
//                                                        } catch (Exception ex) {
//                                                            ex.printStackTrace();
//                                                            pd.dismiss();
//                                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                                        }
//
//                                                    }
//                                                    manageAccountsCustomListAdapter = new ManageAccountsCustomListAdapter(accounts, ManageAccountsScreen.this);
//                                                    listView.setAdapter(manageAccountsCustomListAdapter);
//                                                    pd.dismiss();

                                                    pd.dismiss();
                                                    Intent intent = new Intent(HomeScreen.this, ManageAccountsScreen.class);
                                                    intent.putExtra("intent", "home");
                                                    startActivity(intent);
                                                } else {
                                                    pd.dismiss();

                                                }
                                            } else if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else if (jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Code").equals("49")) {
                                                pd.dismiss();
                                                if (userType.equals("NON_SAMBA")) {
                                                    Intent intent = new Intent(HomeScreen.this, AccountsScreen.class);
                                                    overridePendingTransition(0, 0);
                                                    startActivity(intent);
                                                } else {
                                                    Intent intent = new Intent(HomeScreen.this, ManageAccountsScreen.class);
                                                    intent.putExtra("intent", "home");
                                                    overridePendingTransition(0, 0);
                                                    startActivity(intent);
                                                }
                                            } else {
                                                pd.dismiss();
//                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_T24AccountListResult").getString("Status_Description"), "WARNING");
                                            }
                                        } else {
                                            pd.dismiss();
//                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
//                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
//                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
//                                        utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sT24_CIF", constants.sharedPreferences.getString("T24_CIF", "N/A"));
//                            params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                            params.put("sLegalIdentityType", constants.sharedPreferences.getString("LegalType", "N/A"));
//                            params.put("sSigns_UserType", type);
//                            params.put("sIsAllAccounts", "0");
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
//                    utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                }

            } else {
                pd.dismiss();
//                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
//            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }

    public void GetbillerDistributorList() {


        try {

            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sCompanyID", Constants.companyID);
                    params.put("sCompanyType", Constants.companyType);
                    params.put("sServerSessionKey", md5);
                    HttpsTrustManager.allowMySSL(HomeScreen.this);

                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getBillerDistributorListURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_BillDistributorResult").getString("Status_Code").equals("00")) {

                                                JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("Signs_BillDistributorResult").getString("Local_BillDistributorList"));

                                                if (jsonArray.length() > 0) {

                                                    for (int i = 0; i < jsonArray.length(); i++) {

                                                        try {

                                                            JSONObject jsonObj = new JSONObject(jsonArray.get(i).toString());

                                                            billerDistributorList.put(jsonObj.getString("Distributor_Name"), jsonObj.getString("Distributor_ID") + "~" + jsonObj.getString("Distributor_DistributorAccountNumber"));

                                                        } catch (Exception ex) {

                                                            ex.printStackTrace();
                                                            pd.dismiss();
                                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        }

                                                    }

                                                    Gson gson = new Gson();
                                                    String json = gson.toJson(billerDistributorList);
                                                    constants.billerListEditor.putString("billerListCategoryFor1Bill", json);
                                                    constants.billerListEditor.commit();
                                                    constants.billerListEditor.apply();
                                                    pd.dismiss();
                                                } else {

                                                    pd.dismiss();

                                                }


                                            } else if (jsonObject.getJSONObject("Signs_BillDistributorResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillDistributorResult").getString("Status_Description"), "ERROR", HomeScreen.this, new LoginScreen());


                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("Signs_BillDistributorResult").getString("Status_Description"), "ERROR");
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sCompanyID",intent.getStringExtra("categoryID"));
//                            params.put("sCompanyType",intent.getStringExtra("categoryCompanyType"));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }


    }


    public void getAddress(final double latitude, final double longitude) {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                final Geocoder geocoder;
                Address address;
                geocoder = new Geocoder(HomeScreen.this, Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    if (addresses != null) {
                        address = addresses.get(0);
                    } else {
                        address = null;
                    }

                    String city = "";
                    String country = "";
                    if (address != null) {
                        city = address.getLocality();
                        country = address.getCountryName();
                    } else {
                        city = "N/A";
                        country = "N/A";
                    }
                    try {
                        FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("city", city);
                        FirebaseAnalytics.getInstance(HomeScreen.this).setUserProperty("country", country);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


    }



}


