package com.ceesolutions.samba.beneficiariesManagement.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.beneficiariesManagement.BeneficiaryFormScreen;
import com.ceesolutions.samba.beneficiariesManagement.Model.BankModel;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.views.CircularTextView;
import com.google.firebase.analytics.FirebaseAnalytics;


import java.util.ArrayList;

/**
 * Created by ceeayaz on 3/6/18.
 */

public class CustomListAdapter extends BaseAdapter {
    ArrayList<BankModel> bankModels;
    LayoutInflater layoutInflater;
    View row;
    Context context;
    TextView bankName;
    CircularTextView title;
    String type;

    public CustomListAdapter(ArrayList<BankModel> userModelArrayList, Context mContext, String isType) {
        bankModels = userModelArrayList;
        context = mContext;
        type = isType;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return bankModels.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.bank_list_item, parent, false);
        }


        title = (CircularTextView) convertView.findViewById(R.id.title);
        bankName = (TextView) convertView.findViewById(R.id.bankName);

        title.setSolidColor(bankModels.get(position).getBankColor());
        title.setText(bankModels.get(position).getBankCode());

        bankName.setText(bankModels.get(position).getBankName());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAnalytics.getInstance(context).logEvent("Bank_Selection_Pressed", new Bundle());

                if (!TextUtils.isEmpty(type) && type.equals("addAccount")) {

                    Intent intent = new Intent(context, BeneficiaryFormScreen.class);
                    intent.putExtra("bankName", bankModels.get(position).getBankName());
                    intent.putExtra("bankCode", bankModels.get(position).getBankId());
                    intent.putExtra("length", bankModels.get(position).getLength());
                    intent.putExtra("display", bankModels.get(position).getDisplayText());
                    intent.putExtra("type", "addAccount");
                    context.startActivity(intent);


                } else {

                    Intent intent = new Intent(context, BeneficiaryFormScreen.class);
                    intent.putExtra("bankName", bankModels.get(position).getBankName());
                    intent.putExtra("bankCode", bankModels.get(position).getBankId());
                    intent.putExtra("length", bankModels.get(position).getLength());
                    intent.putExtra("display", bankModels.get(position).getDisplayText());
                    context.startActivity(intent);
//                    ((Activity)context).finish();

                }

            }
        });

        return convertView;
    }
}