package com.ceesolutions.samba.beneficiariesManagement;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;

import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.transactions.FundTransferScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class ReviewBeneficiaryScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private TextInputEditText editAlias, editAccountNumber, editMobileNumber, editEmailAddress, editAccountTitle, editBankName;
    private TextView transferFundsBtn, addToFavBtn, deleteBeneficiaryBtn, textView, doneBtn, heading;
    private Dialog dialog;
    private WebService webService;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private Intent intent1;
    private ImageView backBtn, notificationBtn;
    private String beneID, bankCode, isFav;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_beneficiary_info);
        getSupportActionBar().hide();
        initViews();

        transferFundsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                FirebaseAnalytics.getInstance(ReviewBeneficiaryScreen.this).logEvent("Bene_Fund_Transfer_Pressed", new Bundle());

                Intent intent = new Intent(ReviewBeneficiaryScreen.this, FundTransferScreen.class);
                intent.putExtra("requestFunds", "sendMoney");
                intent.putExtra("alias", intent1.getStringExtra("alias"));
                intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                intent.putExtra("email", intent1.getStringExtra("email"));
                intent.putExtra("bankName", intent1.getStringExtra("bankName"));
                intent.putExtra("accountTile", intent1.getStringExtra("accountTitle"));
                intent.putExtra("mobileNumber", intent1.getStringExtra("mobileNumber"));
                intent.putExtra("beneID", beneID);
                intent.putExtra("bankCode", bankCode);
                intent.putExtra("beneType", intent1.getStringExtra("beneType"));
                intent.putExtra("currency", intent1.getStringExtra("currency"));
                overridePendingTransition(0, 0);
                startActivity(intent);

//                finish();
            }
        });

        addToFavBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isFav.equals("1")) {

                    showDilogForError("Are you sure you want to remove the beneficiary from favourite?", "CONFIRMATION", "fav", "0");
                } else {

                    showDilogForError("Are you sure you want to mark the beneficiary as favourite?", "CONFIRMATION", "fav", "1");
                }

            }
        });

        deleteBeneficiaryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDilogForError("Are you sure you want to delete this beneficiary?", "CONFIRMATION", "delete", "");
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ReviewBeneficiaryScreen.this, ManageBeneficiaresScreen.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
                finish();
            }
        });

        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ReviewBeneficiaryScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ReviewBeneficiaryScreen.this, ManageBeneficiaresScreen.class);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void initViews() {

        intent1 = getIntent();

        editAlias = (TextInputEditText) findViewById(R.id.editAlias);
        editAccountNumber = (TextInputEditText) findViewById(R.id.editAccountNumber);
        editMobileNumber = (TextInputEditText) findViewById(R.id.editMobileNumber);
        editEmailAddress = (TextInputEditText) findViewById(R.id.editEmailAddress);
        editAccountTitle = (TextInputEditText) findViewById(R.id.editAccountTitle);
        editBankName = (TextInputEditText) findViewById(R.id.editBankName);
        transferFundsBtn = (TextView) findViewById(R.id.transferFundsBtn);
        addToFavBtn = (TextView) findViewById(R.id.addToFavBtn);
        deleteBeneficiaryBtn = (TextView) findViewById(R.id.deleteBeneficiaryBtn);
        webService = new WebService();
        backBtn = (ImageView) findViewById(R.id.backBtn);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        helper = Helper.getHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(ReviewBeneficiaryScreen.this);
        utils = new Utils(ReviewBeneficiaryScreen.this);
        pd = new Dialog(ReviewBeneficiaryScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        editAlias.setText(intent1.getStringExtra("alias"));
        editAccountNumber.setText(intent1.getStringExtra("accountNumber"));
        editEmailAddress.setText(intent1.getStringExtra("email"));
        editBankName.setText(intent1.getStringExtra("bankName"));
        editAccountTitle.setText(intent1.getStringExtra("accountTitle"));
        editMobileNumber.setText(intent1.getStringExtra("mobileNumber"));
        beneID = intent1.getStringExtra("beneID");
        bankCode = intent1.getStringExtra("bankCode");
        isFav = intent1.getStringExtra("isFav");
        if (isFav.equals("1")) {

            addToFavBtn.setText("Remove Favourite");
            addToFavBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.favoritefill, 0, 0, 0);
            addToFavBtn.setBackgroundResource(R.drawable.border_blue);
            addToFavBtn.setTextColor(getResources().getColor(R.color.notiheader));
        } else {

        }
        appPreferences = new AppPreferences(ReviewBeneficiaryScreen.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

    }


    public void DeleteBene() {


        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.DeletBene(constants.sharedPreferences.getString("userName", "N/A"), intent1.getStringExtra("beneId"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getRemoveBeneFav();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                doneBtn.setEnabled(true);
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        Intent intent = new Intent(ReviewBeneficiaryScreen.this, ManageBeneficiaresScreen.class);
//                                        startActivity(intent);
//                                        overridePendingTransition(0, 0);
//                                        finish();
//
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                        doneBtn.setEnabled(true);
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    doneBtn.setEnabled(true);
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            doneBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 8000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_BeneID", intent1.getStringExtra("beneId"));
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(ReviewBeneficiaryScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.removeBeneURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_RemoveBeneficiaryResult").getString("Status_Code").equals("00")) {
//                                        JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_RemoveBeneficiaryResult").getString("Status_Code"));
                                            pd.dismiss();
                                            Intent intent = new Intent(ReviewBeneficiaryScreen.this, ManageBeneficiaresScreen.class);
                                            intent.putExtra("addBene", "addBene");
                                            startActivity(intent);
                                            doneBtn.setEnabled(true);
                                            overridePendingTransition(0, 0);
                                            finish();

                                        } else if (jsonObject.getJSONObject("Signs_RemoveBeneficiaryResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_RemoveBeneficiaryResult").getString("Status_Description"), "ERROR", ReviewBeneficiaryScreen.this, new LoginScreen());
                                            doneBtn.setEnabled(true);

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_RemoveBeneficiaryResult").getString("Status_Description"), "ERROR");
                                            doneBtn.setEnabled(true);
                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        doneBtn.setEnabled(true);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_BeneID", intent1.getStringExtra("beneId"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                doneBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            doneBtn.setEnabled(true);

        }
    }

    public void showDilogForError(String msg, String header, final String type, final String sAction) {

        dialog = new Dialog(ReviewBeneficiaryScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                doneBtn.setEnabled(false);
                if (type.equals("fav")) {
                    dialog.dismiss();
                    AddBeneficiaryToFav(sAction);
                } else {
                    dialog.dismiss();
                    DeleteBene();
                }

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }

    public void AddBeneficiaryToFav(final String sAction) {


        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.AddBeneficiaryToFav(constants.sharedPreferences.getString("userName", "N/A"), intent1.getStringExtra("beneId"), "1", constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getAddBeneFav();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                doneBtn.setEnabled(true);
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        Intent intent = new Intent(ReviewBeneficiaryScreen.this, ManageBeneficiaresScreen.class);
//                                        startActivity(intent);
//                                        overridePendingTransition(0, 0);
//                                        finish();
//
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                        doneBtn.setEnabled(true);
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    doneBtn.setEnabled(true);
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            doneBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 8000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_BeneID", intent1.getStringExtra("beneId"));
                params.put("sSigns_BeneFavoriteFlag", sAction);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(ReviewBeneficiaryScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.addBeneFavURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_BeneficiaryFavorite_Mark_UnMarkResult").getString("Status_Code").equals("00")) {
//                                        JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_BeneficiaryFavorite_Mark_UnMarkResult").getString("Status_Code"));

                                            pd.dismiss();
                                            Intent intent = new Intent(ReviewBeneficiaryScreen.this, ManageBeneficiaresScreen.class);
                                            intent.putExtra("addBene", "addBene");
                                            startActivity(intent);
                                            doneBtn.setEnabled(true);
                                            overridePendingTransition(0, 0);
                                            finish();

                                        } else if (jsonObject.getJSONObject("Signs_BeneficiaryFavorite_Mark_UnMarkResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BeneficiaryFavorite_Mark_UnMarkResult").getString("Status_Description"), "ERROR", ReviewBeneficiaryScreen.this, new LoginScreen());
                                            doneBtn.setEnabled(true);

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_BeneficiaryFavorite_Mark_UnMarkResult").getString("Status_Description"), "ERROR");
                                            doneBtn.setEnabled(true);
                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        doneBtn.setEnabled(true);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_BeneID", intent1.getStringExtra("beneId"));
//                        params.put("sSigns_BeneFavoriteFlag", sAction);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                doneBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            doneBtn.setEnabled(true);

        }
    }
}
