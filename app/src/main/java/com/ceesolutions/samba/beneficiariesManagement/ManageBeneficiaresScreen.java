package com.ceesolutions.samba.beneficiariesManagement;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import com.ceesolutions.samba.eatMubarak.EatMubarakMainScreen;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.Bookme.BookMeMainScreen;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.beneficiariesManagement.Adapter.CustomHorizontalListAdapter;
import com.ceesolutions.samba.beneficiariesManagement.Adapter.CustomListBeneAdapter;
import com.ceesolutions.samba.beneficiariesManagement.Model.UserModel;
import com.ceesolutions.samba.billPayments.PayBillsScreen;
import com.ceesolutions.samba.cardManagement.ManageCardsScreen;
import com.ceesolutions.samba.masterCardIntegration.MainActivity;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.transactions.AccountsScreen;
import com.ceesolutions.samba.transactions.FundTransferScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.TapTarget;
import com.ceesolutions.samba.utils.TapTargetView;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.views.HorizontalListView;
import com.daimajia.swipe.util.Attributes;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.OnBoomListenerAdapter;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ceeayaz on 4/1/18.
 */

public class ManageBeneficiaresScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private ListView listView;
    private HorizontalListView horizontalListView;
    private CustomListBeneAdapter customListBeneAdapter;
    private CustomHorizontalListAdapter customHorizontalListAdapter;
    private ArrayList<UserModel> userModels;
    private ArrayList<UserModel> favouritesModels;
    private FloatingActionButton floatingAction;
    private Dialog pd, dialog;
    private WebService webService;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Utils utils;
    private String type, backType;
    private ImageView backBtn, notificationBtn, sambaBtn, shareBtn, helpBtn;
    private TextView heading, textView, btn;
    private String menu, userType;
    private BoomMenuButton bmb;
    private EditText chatBotText;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_beneficiary);
        initViews();
        getSupportActionBar().hide();
        try {
            CryptLib _crypt = new CryptLib();
            userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
            if (!TextUtils.isEmpty(userType) && !userType.equals("N/A") && !userType.equals("")) {


            } else {

                userType = "N/A";
            }
            menu = constants.sharedPreferences.getString("Menus", "N/A");
            if (!TextUtils.isEmpty(menu) && !menu.equals("N/A") && !menu.equals("")) {


            } else {

                menu = "N/A";
            }
            floatingAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FirebaseAnalytics.getInstance(ManageBeneficiaresScreen.this).logEvent("Add_Bene_Pressed", new Bundle());

                    Intent intent = new Intent(ManageBeneficiaresScreen.this, SelectBankScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }
            });


            backBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!TextUtils.isEmpty(backType) && backType.equals("addBene")) {
                        Intent intent = new Intent(ManageBeneficiaresScreen.this, HomeScreen.class);
                        overridePendingTransition(0, 0);
                        startActivity(intent);
                        finish();
                    } else if (TextUtils.isEmpty(backType) || TextUtils.isEmpty(type) && type.equals("notSendMoney")) {

                        finish();

                    } else {
                        Intent intent = new Intent(ManageBeneficiaresScreen.this, HomeScreen.class);
                        overridePendingTransition(0, 0);
                        startActivity(intent);
                        finish();
                    }
                }
            });

            notificationBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(ManageBeneficiaresScreen.this, MessageCenter.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
//                finish();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initViews() {

        listView = findViewById(R.id.listView);
        Intent intent = getIntent();
        type = intent.getStringExtra("source");
        backType = intent.getStringExtra("addBene");
        sambaBtn = findViewById(R.id.sambaBtn);
        shareBtn = findViewById(R.id.shareBtn);
        helpBtn = findViewById(R.id.helpBtn);
        chatBotText = findViewById(R.id.chatBoxText);

        if (!TextUtils.isEmpty(type) && !type.equals(null) && !type.equals("null")) {


        } else
            type = "notSendMoney";


        bmb = findViewById(R.id.bmb);
        assert bmb != null;
        bmb.setButtonEnum(ButtonEnum.TextOutsideCircle);
        bmb.setBackgroundColor(Color.TRANSPARENT);
        bmb.setPiecePlaceEnum(PiecePlaceEnum.DOT_9_1);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.SC_9_1);


        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {


                    Intent intent = new Intent("android.intent.action.VIEW");

                    /** creates an sms uri */
                    Uri data = Uri.parse("sms:");

                    /** Setting sms uri to the intent */
                    intent.setData(data);
                    intent.putExtra("sms_body", "Download Samba Mobile App and register yourself. Click https://bit.ly/2KB0nfG for details");

                    /** Initiates the SMS compose screen, because the activity contain ACTION_VIEW and sms uri */
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        chatBotText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showDilogForErrorForLogin(getResources().getString(R.string.message), "WARNING");
                Intent i = new Intent(ManageBeneficiaresScreen.this, HomeScreen.class);
                overridePendingTransition(0, 0);
                startActivity(i);
                finish();
                finishAffinity();
            }
        });

        notificationBtn = findViewById(R.id.notificationBtn);
        backBtn = findViewById(R.id.backBtn);
        horizontalListView = findViewById(R.id.horizontalListView);
        floatingAction = findViewById(R.id.floatingAction);
        userModels = new ArrayList<>();
        favouritesModels = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(ManageBeneficiaresScreen.this);
//        webService = new WebService();
        helper = Helper.getHelper(ManageBeneficiaresScreen.this);
        constants = Constants.getConstants(ManageBeneficiaresScreen.this);
        pd = new Dialog(ManageBeneficiaresScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        utils = new Utils(ManageBeneficiaresScreen.this);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                GetAccountsAndBeneList();
            }
        }, 500);
        appPreferences = new AppPreferences(ManageBeneficiaresScreen.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

        String coachMarks = constants.coachMarks.getString("manageBene", "N/A");

//        if (!TextUtils.isEmpty(coachMarks) && coachMarks.equals("N/A")) {
//            constants.coachMarksEditor.putString("manageBene", "1");
//            constants.coachMarksEditor.commit();
//            constants.coachMarksEditor.apply();
//            int mainTxt = getResources().getDimensionPixelSize(R.dimen._7sdp);
//            int des = getResources().getDimensionPixelSize(R.dimen._4sdp);
//            TapTargetView.showFor(ManageBeneficiaresScreen.this,                 // `this` is an Activity
//                    TapTarget.forView(findViewById(R.id.helpBtn), "", "- Use plus (+) button to register a new beneficiary\n\n" +
//                                    "- Swipe your registered beneficiary in the list on left to explore more options")
//                            // All options below are optional
//                            .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
//                            .outerCircleAlpha(0.90f)
//                            // Specify the alpha amount for the outer circle
//                            // Specify a color for the target circle
//                            .titleTextSize(mainTxt)// Specify the size (in sp) of the title text
//                            .titleTextColor(R.color.white)      // Specify the color of the title text
//                            .descriptionTextSize(des)            // Specify the size (in sp) of the description text
//                            .descriptionTextColor(R.color.white)  // Specify the color of the description text
//                            .textColor(R.color.white)            // Specify a color for both the title and description text
//                            .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
//                            // If set, will dim behind the view with 30% opacity of the given color
//                            .drawShadow(true)                   // Whether to draw a drop shadow or not
//                            .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
//                            .tintTarget(true)                   // Whether to tint the target view's color
//                            .transparentTarget(false),           // Specify whether the target is transparent (displays the content underneath)
//                    // Specify a custom drawable to draw as the target
//                    // Specify the target radius (in dp)
//                    new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
//                        @Override
//                        public void onTargetClick(TapTargetView view) {
//                            super.onTargetClick(view);      // This call is optional
////                        doSomething();
//                        }
//                    });
//        }

        helpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int mainTxt = getResources().getDimensionPixelSize(R.dimen._7sdp);
                int des = getResources().getDimensionPixelSize(R.dimen._4sdp);

                TapTargetView.showFor(ManageBeneficiaresScreen.this,                 // `this` is an Activity
                        TapTarget.forView(findViewById(R.id.helpBtn), "About this Screen!!!", "- Use plus (+) button to register a new beneficiary\n\n" +
                                "- Swipe your registered beneficiary in the list on left to explore more options")
                                // All options below are optional
                                .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                                .outerCircleAlpha(0.90f)
                                // Specify the alpha amount for the outer circle
                                // Specify a color for the target circle
                                .titleTextSize(mainTxt)// Specify the size (in sp) of the title text
                                .titleTextColor(R.color.white)      // Specify the color of the title text
                                .descriptionTextSize(des)            // Specify the size (in sp) of the description text
                                .descriptionTextColor(R.color.white)  // Specify the color of the description text
                                .textColor(R.color.white)            // Specify a color for both the title and description text
                                .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                                // If set, will dim behind the view with 30% opacity of the given color
                                .drawShadow(true)                   // Whether to draw a drop shadow or not
                                .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                                .tintTarget(true)                   // Whether to tint the target view's color
                                .transparentTarget(false),           // Specify whether the target is transparent (displays the content underneath)
                        // Specify a custom drawable to draw as the target
                        // Specify the target radius (in dp)
                        new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                            @Override
                            public void onTargetClick(TapTargetView view) {
                                super.onTargetClick(view);      // This call is optional
//                        doSomething();

                            }
                        });
            }
        });

        sambaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                bmb.boom();
            }
        });

        bmb.setOnBoomListener(new OnBoomListenerAdapter() {
            @Override
            public void onBoomWillShow() {
                super.onBoomWillShow();
                // logic here
            }
        });

        bmb.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {
                // If you have implement listeners for boom-buttons in builders,
                // then you shouldn't add any listener here for duplicate callbacks.

                switch (index) {


                    case 0:

                        Intent home = new Intent(ManageBeneficiaresScreen.this, HomeScreen.class);
                        overridePendingTransition(0, 0);
                        startActivity(home);
                        finish();
                        break;

                    case 1:

                        if (!menu.equals("N/A")) {

                            if (menu.toLowerCase().contains("accounts")) {
                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                            } else {

                                Intent intent = new Intent(ManageBeneficiaresScreen.this, AccountsScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Intent intent = new Intent(ManageBeneficiaresScreen.this, AccountsScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(intent);
                            finish();

                        }

                        break;

                    case 2:

                        try {

                            CryptLib _crypt = new CryptLib();
                            Intent transfers = new Intent(ManageBeneficiaresScreen.this, FundTransferScreen.class);
                            String user = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                            if (!TextUtils.isEmpty(user) && !user.equals("N/A")) {

                                if (user.equals("NON_SAMBA")) {

                                    transfers.putExtra("requestFunds", "nonSamba");
                                }
                            }
                            overridePendingTransition(0, 0);
                            startActivity(transfers);
//                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;


                    case 3:

                        try {

                            CryptLib _crypt;
                            if (!menu.equals("N/A")) {
                                _crypt = new CryptLib();
                                if (menu.toLowerCase().contains("biller management")) {
                                    showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
                                } else {
                                    String user1 = constants.sharedPreferences.getString("type", "N/A");
                                    Intent payments = new Intent(ManageBeneficiaresScreen.this, PayBillsScreen.class);
                                    overridePendingTransition(0, 0);
                                    user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                                    if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                        if (user1.equals("NON_SAMBA")) {

                                            payments.putExtra("requestbill", "nonSamba");
                                        }
                                    }
                                    startActivity(payments);
//                                    finish();
                                }
                            } else {
                                _crypt = new CryptLib();
                                Intent payments = new Intent(ManageBeneficiaresScreen.this, PayBillsScreen.class);
                                overridePendingTransition(0, 0);
                                String user1 = constants.sharedPreferences.getString("type", "N/A");
                                user1 = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                                if (!TextUtils.isEmpty(user1) && !user1.equals("N/A")) {

                                    if (user1.equals("NON_SAMBA")) {

                                        payments.putExtra("requestbill", "nonSamba");
                                    }
                                }
                                startActivity(payments);
//                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        break;


                    case 4:

//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("beneficiary management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//
//                                if (userType.equals("NON_SAMBA")) {
//                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                                } else {
//                                    Intent bene = new Intent(ManageBeneficiaresScreen.this, ManageBeneficiaresScreen.class);
//                                    overridePendingTransition(0, 0);
//                                    startActivity(bene);
////                                    finish();
//                                }
//                            }
//                        } else {
//                            if (userType.equals("NON_SAMBA")) {
//                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                            } else {
//                                Intent bene = new Intent(ManageBeneficiaresScreen.this, ManageBeneficiaresScreen.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(bene);
////                                    finish();
//                            }
////                                finish();
//                        }

                        break;


                    case 5:

                        if (userType.equals("NON_SAMBA"))
                            showDilogForError("This feature is only available for Samba Users.", "WARNING");
                        else {
                            Intent intent = new Intent(ManageBeneficiaresScreen.this, MainActivity.class);
                            overridePendingTransition(0, 0);
                            startActivity(intent);
                        }

//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("friends management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//
//                                Intent friends = new Intent(ManageBeneficiaresScreen.this, ManageFriends.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(friends);
////                                    finish();
//                            }
//                        } else {
//                            Intent friends = new Intent(ManageBeneficiaresScreen.this, ManageFriends.class);
//                            overridePendingTransition(0, 0);
//                            startActivity(friends);
////                                finish();
//                        }

                        break;


                    case 6:


                        try {

                            CryptLib _crypt = new CryptLib();

                            String userType = _crypt.decrypt(constants.sharedPreferences.getString("type", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());

                            if (userType.equals("NON_SAMBA"))
                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
                            else {
                                Intent intent = new Intent(ManageBeneficiaresScreen.this, EatMubarakMainScreen.class);
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


//                        if (!menu.equals("N/A")) {
//
//                            if (menu.toLowerCase().contains("cards management")) {
//                                showDilogForError("This service is currently disabled. Please try again later.", "WARNING");
//                            } else {
//                                if (userType.equals("NON_SAMBA")) {
//                                    showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                                } else {
//                                    Intent cards = new Intent(ManageBeneficiaresScreen.this, ManageCardsScreen.class);
//                                    overridePendingTransition(0, 0);
//                                    startActivity(cards);
////                                    finish();
//                                }
//
////                                    finish();
//                            }
//                        } else {
//                            if (userType.equals("NON_SAMBA")) {
//                                showDilogForError("This feature is only available for Samba Users.", "WARNING");
//                            } else {
//                                Intent cards = new Intent(ManageBeneficiaresScreen.this, ManageCardsScreen.class);
//                                overridePendingTransition(0, 0);
//                                startActivity(cards);
////                                    finish();
//                            }
////                                finish();
//                        }
                        break;

                    case 7:
                        if (userType.equals("NON_SAMBA")) {
                            showDilogForError("This feature is only available for Samba Users.", "WARNING");
                        } else {
                            Intent atm = new Intent(ManageBeneficiaresScreen.this, BookMeMainScreen.class);
                            overridePendingTransition(0, 0);
                            startActivity(atm);
//                        finish();
                        }
                        break;

                    case 8:

                        pd.show();
                        showDialog("Are you sure you want to Logout?");
                        break;


                    default: {

                        break;
                    }
                }
            }

            @Override
            public void onBackgroundClick() {
//                textViewForAnimation.setText("Click background!!!");
            }

            @Override
            public void onBoomWillHide() {
                Log.d("BMB", "onBoomWillHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will RE-BOOM!!!");
            }

            @Override
            public void onBoomDidHide() {
                Log.d("BMB", "onBoomDidHide: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did RE-BOOM!!!");
            }

            @Override
            public void onBoomWillShow() {
                Log.d("BMB", "onBoomWillShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Will BOOM!!!");
            }

            @Override
            public void onBoomDidShow() {
                Log.d("BMB", "onBoomDidShow: " + bmb.isBoomed() + " " + bmb.isReBoomed());
//                textViewForAnimation.setText("Did BOOM!!!");
            }
        });

//        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++)
//            bmb.addBuilder(BuilderManager.getSimpleCircleButtonBuilder());

        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder()
                    .listener(new OnBMClickListener() {
                        @Override
                        public void onBoomButtonClick(int index) {
                            // When the boom-button corresponding this builder is clicked.
//                            Toast.makeText(HomeScreen.this, "Clicked " + index, Toast.LENGTH_SHORT).show();
                        }
                    });

//            int margin = getResources().getDimensionPixelSize(R.dimen._4sdp);
            builder.textSize(12);

            switch (i) {

                case 0:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.home_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Home");
                    builder.unableText("Home");
                    builder.normalText("Home");
                    break;

                case 1:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.acounts_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Accounts");
                    builder.unableText("Accounts");
                    builder.normalText("Accounts");
                    break;

                case 2:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.ft_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Transfers");
                    builder.unableText("Transfers");
                    builder.normalText("Transfers");
                    break;


                case 3:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.bene_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Payments");
                    builder.unableText("Payments");
                    builder.normalText("Payments");
                    break;


                case 4:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.biller_bmb, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Beneficiaries");
                    builder.unableText("Beneficiaries");
                    builder.normalText("Beneficiaries");
                    break;


                case 5:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.qr_test, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("QR Payments");
                    builder.unableText("QR Payments");
                    builder.normalText("QR Payments");

//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.friends_bmb, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Friends");
//                    builder.unableText("Friends");
//                    builder.normalText("Friends");
                    break;


                case 6:

                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.eat_mubarak_icon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Food");
                    builder.unableText("Food");
                    builder.normalText("Food");


//                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.card_bmb, null));
//                    builder.normalColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
//                    builder.unableColor(getResources().getColor(R.color.notiheader));
//                    builder.highlightedText("Cards");
//                    builder.unableText("Cards");
//                    builder.normalText("Cards");
                    break;


                case 7:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.etickets, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("E-Tickets");
                    builder.unableText("E-Tickets");
                    builder.normalText("E-Tickets");
                    break;

                case 8:
                    builder.normalImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.unableImageDrawable(getResources().getDrawable(R.drawable.logouticon, null));
                    builder.normalColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedColor(getResources().getColor(R.color.notiheader));
                    builder.unableColor(getResources().getColor(R.color.notiheader));
                    builder.highlightedText("Logout");
                    builder.unableText("Logout");
                    builder.normalText("Logout");
                    break;


                default: {

                    break;
                }

            }
//            builder.normalImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.highlightedImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.unableImageDrawable(getResources().getDrawable(R.drawable.credit_card_white, null));
//            builder.normalColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedColor(getResources().getColor(R.color.notiheader));
//            builder.unableColor(getResources().getColor(R.color.notiheader));
//            builder.highlightedText("Cards");
//            builder.unableText("Cards");
//            builder.normalText("Cards");
//            builder.imagePadding(new Rect(40, 40, 40, 40));
            bmb.addBuilder(builder);
        }
    }

    public void GetAccountsAndBeneList() {

        try {

            pd.show();
            if (helper.isNetworkAvailable()) {
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", Constants.signsUserName);
                params.put("sService_Password", Constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", Constants.dateTime);
                params.put("sSigns_Username", user);
                params.put("sT24_CIF", _crypt.decrypt(constants.sharedPreferences.getString("T24_CIF", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI()));
                params.put("sDeviceLatitude", latitude + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(ManageBeneficiaresScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getAccountNBenelistURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Status_Code").equals("00")) {

                                            try {

                                                JSONArray beneArray = new JSONArray(jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Local_BenefeciaryList"));

                                                if (beneArray.length() > 0) {
                                                    for (int i = 0; i < beneArray.length(); i++) {
                                                        try {


                                                            JSONObject jsonObj = new JSONObject(beneArray.get(i).toString());

                                                            if (jsonObj.getString("Bene_Favorite").equals("1")) {

                                                                UserModel userModel = new UserModel();
                                                                userModel.setBankName(jsonObj.getString("Bene_BankName"));
                                                                userModel.setAccountNumber(jsonObj.getString("Bene_AccountNumber"));
                                                                userModel.setUserName(jsonObj.getString("Bene_Alias"));
                                                                String firstName = jsonObj.getString("Bene_Alias");
                                                                userModel.setBankCode(jsonObj.getString("Bene_BankCode"));
                                                                userModel.setCurrency(jsonObj.getString("Bene_CurrencyCode"));
                                                                userModel.setBeneType(jsonObj.getString("Bene_Type"));
                                                                userModel.setUserTitle(jsonObj.getString("Bene_AccountTitle"));
                                                                userModel.setMobileNumber(jsonObj.getString("Bene_Mobile"));
                                                                userModel.setEmail(jsonObj.getString("Bene_Email"));
                                                                userModel.setBeneID(jsonObj.getString("Bene_ID"));
                                                                userModel.setIsFavourite(jsonObj.getString("Bene_Favorite"));
                                                                userModel.setBankColor(jsonObj.getString("Bene_BankColorCode"));
                                                                userModel.setAccountTitle(jsonObj.getString("Bene_AccountTitle"));
                                                                String s = firstName.substring(0, 1);
                                                                userModel.setFirstLetter(s.toUpperCase());
                                                                userModels.add(userModel);
                                                                favouritesModels.add(userModel);


                                                            } else {

                                                                UserModel userModel = new UserModel();
                                                                userModel.setBankName(jsonObj.getString("Bene_BankName"));
                                                                userModel.setAccountNumber(jsonObj.getString("Bene_AccountNumber"));
                                                                userModel.setUserName(jsonObj.getString("Bene_Alias"));
                                                                userModel.setBankCode(jsonObj.getString("Bene_BankCode"));
                                                                userModel.setCurrency(jsonObj.getString("Bene_CurrencyCode"));
                                                                userModel.setBeneType(jsonObj.getString("Bene_Type"));
                                                                userModel.setUserTitle(jsonObj.getString("Bene_AccountTitle"));
                                                                userModel.setMobileNumber(jsonObj.getString("Bene_Mobile"));
                                                                userModel.setEmail(jsonObj.getString("Bene_Email"));
                                                                userModel.setBeneID(jsonObj.getString("Bene_ID"));
                                                                userModel.setAccountTitle(jsonObj.getString("Bene_AccountTitle"));
                                                                userModel.setBankColor(jsonObj.getString("Bene_BankColorCode"));
                                                                userModel.setIsFavourite(jsonObj.getString("Bene_Favorite"));
                                                                String firstName = jsonObj.getString("Bene_Alias");
                                                                String s = firstName.substring(0, 1);
                                                                userModel.setFirstLetter(s.toUpperCase());
                                                                userModels.add(userModel);
                                                            }


                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                        }

                                                    }
                                                    customListBeneAdapter = new CustomListBeneAdapter(userModels, ManageBeneficiaresScreen.this, type);
                                                    listView.setAdapter(customListBeneAdapter);
                                                    customListBeneAdapter.setMode(Attributes.Mode.Multiple);
                                                    listView.setAdapter(customListBeneAdapter);
                                                    listView.setSelected(false);

                                                    customHorizontalListAdapter = new CustomHorizontalListAdapter(favouritesModels, ManageBeneficiaresScreen.this, type);
                                                    horizontalListView.setAdapter(customHorizontalListAdapter);
                                                    pd.dismiss();
                                                } else {

                                                    pd.dismiss();

                                                }

                                            } catch (JSONException ex) {

                                                ex.printStackTrace();
                                                pd.dismiss();
                                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                            }
                                        } else if (jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Status_Description"), "ERROR", ManageBeneficiaresScreen.this, new LoginScreen());


                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_BeneficiaryListResult").getString("Status_Description"), "ERROR");
                                        }

                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {

                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sT24_CIF", constants.sharedPreferences.getString("T24_CIF", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ManageBeneficiaresScreen.this, HomeScreen.class);
        overridePendingTransition(0, 0);
        startActivity(intent);
        finish();
    }

    public void showDilogForError(String msg, String header) {

        dialog = new Dialog(ManageBeneficiaresScreen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = dialog.findViewById(R.id.validatingTxt);
        btn = dialog.findViewById(R.id.doneBtn);
        heading = dialog.findViewById(R.id.heading);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }


    private void showDialog(String message) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Logout:");
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

//                constants.editor.clear();
//                constants.editor.commit();
//                constants.editor.apply();
//                constants.nonSambaCredentialsEditor.clear();
//                constants.nonSambaCredentialsEditor.commit();
//                constants.nonSambaCredentialsEditor.apply();
//                constants.destinationAccountEditor.clear().commit();
//                constants.destinationAccountEditor.apply();
//                constants.sourceAccountEditor.clear().commit();
//                constants.sourceAccountEditor.apply();
//                constants.accountListEditor.clear().commit();
//                constants.accountListEditor.apply();
//                constants.accountsEditor.clear().commit();
//                constants.accountsEditor.apply();
//                constants.forgotPasswordEditor.clear().commit();
//                constants.forgotPasswordEditor.apply();
//                Handler handler1 = new Handler();
//                handler1.postDelayed(new Runnable() {
//                    public void run() {
//
//                        pd.dismiss();
//                        Intent intent = new Intent(ManageBeneficiaresScreen.this, LoginScreen.class);
//                        startActivity(intent);
//                        overridePendingTransition(0, 0);
//                        finish();
//                        finishAffinity();
//
//                    }
//                }, 1000);
                Logout();
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                pd.dismiss();
            }
        });

        builder.show();
    }

    public void Logout() {

        try {

            if (helper.isNetworkAvailable()) {
                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    final String md5 = Helper.md5(user.toUpperCase() + constants.address.toUpperCase() + Constants.CONSTANST + Constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", Constants.signsUserName);
                    params.put("sService_Password", Constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", Constants.dateTime);
                    params.put("sDeviceLatitude", latitude + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);
                    params.put("sSessionID", Constants.Shared_KEY);
                    HttpsTrustManager.allowMySSL(ManageBeneficiaresScreen.this);
                    final String requestBody = "sData=" + _crypt.encrypt(Helper.convertMapToString(params), Constants.getDeviceMac(), Constants.getDeviceIMEI());
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.logoutURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Code").equals("00")) {
                                                constants.editor.clear();
                                                constants.editor.commit();
                                                constants.editor.apply();
                                                constants.nonSambaCredentialsEditor.clear();
                                                constants.nonSambaCredentialsEditor.commit();
                                                constants.nonSambaCredentialsEditor.apply();
                                                constants.destinationAccountEditor.clear().commit();
                                                constants.destinationAccountEditor.apply();
                                                constants.sourceAccountEditor.clear().commit();
                                                constants.sourceAccountEditor.apply();
                                                constants.accountListEditor.clear().commit();
                                                constants.accountListEditor.apply();
                                                constants.accountsEditor.clear().commit();
                                                constants.accountsEditor.apply();
                                                constants.forgotPasswordEditor.clear().commit();
                                                constants.forgotPasswordEditor.apply();
                                                constants.purposeBillList.clear().commit();
                                                constants.purposeBillList.apply();
                                                constants.purposePrepaidList.clear().commit();
                                                constants.purposePrepaidList.apply();
                                                constants.purposeListEditor.clear().commit();
                                                constants.purposeListEditor.apply();


                                                pd.dismiss();
                                                Intent intent = new Intent(ManageBeneficiaresScreen.this, LoginScreen.class);
                                                startActivity(intent);
                                                overridePendingTransition(0, 0);
                                                finish();
                                                finishAffinity();


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("Signs_ApplicationLogoutResult").getString("Status_Description"), "ERROR", ManageBeneficiaresScreen.this, new LoginScreen());

                                            }

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(Constants.Invalid_Message, "ERROR");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                                    }
                                }
                            })
//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                            params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            params.put("sSessionID", Constants.Shared_KEY);
//                            return params;
//                        }
//                    };
                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            return requestBody == null ? null : requestBody.getBytes(StandardCharsets.UTF_8);
                        }
                    };

                    postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                            0,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "WARNING");

                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity", "WARNING");


            }
        } catch (Exception e) {

            pd.dismiss();
            utils.showDilogForError("Please check your internet connectivity", "WARNING");


        }
    }
}
