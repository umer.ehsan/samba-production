package com.ceesolutions.samba.beneficiariesManagement;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ceeayaz on 4/1/18.
 */

public class BeneficiaryVerifyScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private TextInputEditText editAlias, editAccountNumber, editMobileNumber, editEmailAddress, editAccountTitle, editBankName, editBeneficiaryDetail;
    private TextView doneBtn, heading, textView, btn;
    private WebService webService;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Dialog pd, dialog1;
    private Utils utils;
    private Intent intent1;
    private String accountType;
    private ImageView backBtn, notificationBtn;
    private String type;
    private View view5, view6;
    private Date currentDate;
    private String finalDate, currentTime, imgString;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beneficiary_verify);
        getSupportActionBar().hide();
        initViews();

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAnalytics.getInstance(BeneficiaryVerifyScreen.this).logEvent("Add_Bene_Review_Pressed", new Bundle());

                String edit = intent1.getStringExtra("edit");
                if (!TextUtils.isEmpty(edit) && edit.equals("edit")) {

                    doneBtn.setEnabled(false);
                    EditBeneficiary();

                } else {
                    if (!TextUtils.isEmpty(type) && type.equals("addAccount")) {

                        doneBtn.setEnabled(false);
                        AddNonSamba();
                    } else {

                        doneBtn.setEnabled(false);
                        AddBeneficiary();
                    }
                }

            }
        });


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                backBtn.setEnabled(false);
                finish();
            }
        });


        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(BeneficiaryVerifyScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
            }
        });

    }

    private void initViews() {

        editAlias = (TextInputEditText) findViewById(R.id.editAlias);
        editAccountNumber = (TextInputEditText) findViewById(R.id.editAccountNumber);
        editMobileNumber = (TextInputEditText) findViewById(R.id.editMobileNumber);
        editEmailAddress = (TextInputEditText) findViewById(R.id.editEmailAddress);
        editAccountTitle = (TextInputEditText) findViewById(R.id.editAccountTitle);
        editBeneficiaryDetail = (TextInputEditText) findViewById(R.id.editBeneficiaryDetail);
        editBankName = (TextInputEditText) findViewById(R.id.editBankName);
        view5 = (View) findViewById(R.id.view5);
        view6 = (View) findViewById(R.id.view6);
        doneBtn = (TextView) findViewById(R.id.doneBtn);
        backBtn = (ImageView) findViewById(R.id.backBtn);
        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        pd = new Dialog(BeneficiaryVerifyScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        webService = new WebService();
        helper = Helper.getHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(BeneficiaryVerifyScreen.this);
        utils = new Utils(BeneficiaryVerifyScreen.this);

        intent1 = getIntent();

        type = intent1.getStringExtra("type");
        if (!TextUtils.isEmpty(type) && type.equals("addAccount")) {

            editEmailAddress.setVisibility(View.GONE);
            editMobileNumber.setVisibility(View.GONE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            editBeneficiaryDetail.setText("Account Details");


        } else {

            //Do Nothing
        }
        editAlias.setText(intent1.getStringExtra("alias"));
        editAccountNumber.setText(intent1.getStringExtra("accountNumber"));
        editMobileNumber.setText(intent1.getStringExtra("mobileNumber"));
        editEmailAddress.setText(intent1.getStringExtra("email"));
        editBankName.setText(intent1.getStringExtra("bankName"));
        editAccountTitle.setText(intent1.getStringExtra("title"));

        accountType = intent1.getStringExtra("accountType");

        if (accountType.equals("IBAN")) {

            accountType = "0";
        } else {
            accountType = "1";
        }

        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.US);
        currentDate = Calendar.getInstance().getTime();
        formatter = new SimpleDateFormat("dd-MMM-yyyy");
        finalDate = formatter.format(currentDate);
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss aa");
        currentTime = sdf.format(currentDate);

        appPreferences = new AppPreferences(BeneficiaryVerifyScreen.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }


    }

    public void AddBeneficiary() {


        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.AddBeneficiary(constants.sharedPreferences.getString("userName", "N/A"), intent1.getStringExtra("alias"), accountType, intent1.getStringExtra("mobileNumber"), intent1.getStringExtra("email"), intent1.getStringExtra("bankCode"), constants.sharedPreferences.getString("MobileNumber", "N/A"), intent1.getStringExtra("currency"), intent1.getStringExtra("title"), constants.sharedPreferences.getString("LegalID", "N/A"), intent1.getStringExtra("bankName"), intent1.getStringExtra("accountNumber"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getAddBene();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                doneBtn.setEnabled(true);
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        Intent intent = new Intent(BeneficiaryVerifyScreen.this, BeneficiaryAddedConfirmationScreen.class);
//                                        intent.putExtra("alias", intent1.getStringExtra("alias"));
//                                        intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
//                                        intent.putExtra("accountType", intent1.getStringExtra("accountType"));
//                                        intent.putExtra("mobileNumber", intent1.getStringExtra("mobileNumber"));
//                                        intent.putExtra("email", intent1.getStringExtra("email"));
//                                        intent.putExtra("bankName", intent1.getStringExtra("bankName"));
//                                        intent.putExtra("beneType", intent1.getStringExtra("beneType"));
//                                        intent.putExtra("stan", intent1.getStringExtra("stan"));
//                                        intent.putExtra("rrn", intent1.getStringExtra("rrn"));
//                                        intent.putExtra("bankCode", intent1.getStringExtra("bankCode"));
//                                        intent.putExtra("beneId", jsonObject.getString("BeneficiaryID"));
//                                        intent.putExtra("currentDate",finalDate);
//                                        intent.putExtra("currentTime",currentTime);
//                                        overridePendingTransition(0, 0);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        startActivity(intent);
//                                        finish();
//                                        finishAffinity();
//
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                        doneBtn.setEnabled(true);
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    doneBtn.setEnabled(true);
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            doneBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 10000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Alias", intent1.getStringExtra("alias"));
                params.put("sSigns_AccountType", accountType);
                params.put("sSigns_AccountNumber", intent1.getStringExtra("accountNumber"));
                params.put("sSigns_UserMobileNo", intent1.getStringExtra("mobileNumber"));
                params.put("sSigns_UserEmailAddress", intent1.getStringExtra("email"));
                params.put("sSigns_BankID", intent1.getStringExtra("bankCode"));
                params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sSigns_CustomerMobileNo", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sBeneAccountTitle", intent1.getStringExtra("title"));
                params.put("sBeneCurrency", intent1.getStringExtra("currency"));
                params.put("sSigns_BankName", intent1.getStringExtra("bankName"));
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(BeneficiaryVerifyScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.addBeneficiaryURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Status_Code").equals("00")) {
//                                        JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Status_Code"));
//                                        JsonObj.put("BeneficiaryID", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("BeneficiaryID"));

                                            pd.dismiss();
                                            Intent intent = new Intent(BeneficiaryVerifyScreen.this, BeneficiaryAddedConfirmationScreen.class);
                                            intent.putExtra("alias", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Bene_Alias"));
                                            intent.putExtra("accountNumber", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Bene_AccountNumber"));
                                            intent.putExtra("accountType", intent1.getStringExtra("accountType"));
                                            intent.putExtra("mobileNumber", intent1.getStringExtra("mobileNumber"));
                                            intent.putExtra("email", intent1.getStringExtra("email"));
                                            intent.putExtra("bankName", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Bene_BankName"));
                                            intent.putExtra("beneType", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Bene_Type"));
                                            intent.putExtra("stan", intent1.getStringExtra("stan"));
                                            intent.putExtra("rrn", intent1.getStringExtra("rrn"));
                                            intent.putExtra("bankCode", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Bene_BankCode"));
                                            intent.putExtra("accountTitle", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Bene_AccountTitle"));
                                            intent.putExtra("beneId", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("BeneficiaryID"));
                                            intent.putExtra("bankColor", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Bene_BankColorCode"));
                                            intent.putExtra("currency", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Bene_CurrencyCode"));
                                            intent.putExtra("currentDate", finalDate);
                                            intent.putExtra("currentTime", currentTime);
                                            overridePendingTransition(0, 0);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                            finish();
                                            finishAffinity();
                                        } else if (jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Status_Description"), "ERROR", BeneficiaryVerifyScreen.this, new LoginScreen());
                                            doneBtn.setEnabled(true);

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Status_Description"), "ERROR");
                                            doneBtn.setEnabled(true);
                                        }

                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        doneBtn.setEnabled(true);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    FirebaseAnalytics.getInstance(BeneficiaryVerifyScreen.this).logEvent("Error", new Bundle());
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Alias", intent1.getStringExtra("alias"));
//                        params.put("sSigns_AccountType", accountType);
//                        params.put("sSigns_AccountNumber", intent1.getStringExtra("accountNumber"));
//                        params.put("sSigns_UserMobileNo", intent1.getStringExtra("mobileNumber"));
//                        params.put("sSigns_UserEmailAddress", intent1.getStringExtra("email"));
//                        params.put("sSigns_BankID", intent1.getStringExtra("bankCode"));
//                        params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sSigns_CustomerMobileNo", constants.sharedPreferences.getString("MobileNumber", "N/A"));
//                        params.put("sBeneAccountTitle", intent1.getStringExtra("title"));
//                        params.put("sBeneCurrency", intent1.getStringExtra("currency"));
//                        params.put("sSigns_BankName", intent1.getStringExtra("bankName"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                doneBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            doneBtn.setEnabled(true);

        }
    }

    public void EditBeneficiary() {


        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.AddBeneficiary(constants.sharedPreferences.getString("userName", "N/A"), intent1.getStringExtra("alias"), accountType, intent1.getStringExtra("mobileNumber"), intent1.getStringExtra("email"), intent1.getStringExtra("bankCode"), constants.sharedPreferences.getString("MobileNumber", "N/A"), intent1.getStringExtra("currency"), intent1.getStringExtra("title"), constants.sharedPreferences.getString("LegalID", "N/A"), intent1.getStringExtra("bankName"), intent1.getStringExtra("accountNumber"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getAddBene();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                doneBtn.setEnabled(true);
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        Intent intent = new Intent(BeneficiaryVerifyScreen.this, BeneficiaryAddedConfirmationScreen.class);
//                                        intent.putExtra("alias", intent1.getStringExtra("alias"));
//                                        intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
//                                        intent.putExtra("accountType", intent1.getStringExtra("accountType"));
//                                        intent.putExtra("mobileNumber", intent1.getStringExtra("mobileNumber"));
//                                        intent.putExtra("email", intent1.getStringExtra("email"));
//                                        intent.putExtra("bankName", intent1.getStringExtra("bankName"));
//                                        intent.putExtra("beneType", intent1.getStringExtra("beneType"));
//                                        intent.putExtra("stan", intent1.getStringExtra("stan"));
//                                        intent.putExtra("rrn", intent1.getStringExtra("rrn"));
//                                        intent.putExtra("bankCode", intent1.getStringExtra("bankCode"));
//                                        intent.putExtra("beneId", jsonObject.getString("BeneficiaryID"));
//                                        intent.putExtra("currentDate",finalDate);
//                                        intent.putExtra("currentTime",currentTime);
//                                        overridePendingTransition(0, 0);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        startActivity(intent);
//                                        finish();
//                                        finishAffinity();
//
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                        doneBtn.setEnabled(true);
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    doneBtn.setEnabled(true);
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            doneBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 10000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Alias", intent1.getStringExtra("alias"));
                params.put("sSigns_AccountType", accountType);
                params.put("sSigns_AccountNumber", intent1.getStringExtra("accountNumber"));
                params.put("sSigns_UserMobileNo", intent1.getStringExtra("mobileNumber"));
                params.put("sSigns_EmailAddress", intent1.getStringExtra("email"));
                params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sSigns_CustomerMobileNo", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sSigns_BeneID", intent1.getStringExtra("beneId"));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(BeneficiaryVerifyScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.editBeneURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_EditBeneficiaryResult").getString("Status_Code").equals("00")) {
//                                        JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("Status_Code"));
//                                        JsonObj.put("BeneficiaryID", jsonObject.getJSONObject("Signs_AddBeneficiaryResult").getString("BeneficiaryID"));

                                            pd.dismiss();
                                            showDilogForErrorForMenu("Beneficiary has been updated successfully.", "SUCCESS");
                                            doneBtn.setEnabled(true);

                                        } else if (jsonObject.getJSONObject("Signs_EditBeneficiaryResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_EditBeneficiaryResult").getString("Status_Description"), "ERROR", BeneficiaryVerifyScreen.this, new LoginScreen());
                                            doneBtn.setEnabled(true);

                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_EditBeneficiaryResult").getString("Status_Description"), "ERROR");
                                            doneBtn.setEnabled(true);
                                        }
                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        doneBtn.setEnabled(true);
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Alias", intent1.getStringExtra("alias"));
//                        params.put("sSigns_AccountType", accountType);
//                        params.put("sSigns_AccountNumber", intent1.getStringExtra("accountNumber"));
//                        params.put("sSigns_UserMobileNo", intent1.getStringExtra("mobileNumber"));
//                        params.put("sSigns_EmailAddress", intent1.getStringExtra("email"));
//                        params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sSigns_CustomerMobileNo", constants.sharedPreferences.getString("MobileNumber", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sSigns_BeneID", intent1.getStringExtra("beneId"));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                doneBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            doneBtn.setEnabled(true);

        }
    }


    public void AddNonSamba() {


        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.AddNonSamba(constants.sharedPreferences.getString("userName","N/A"), intent1.getStringExtra("alias"), accountType, intent1.getStringExtra("mobileNumber"), intent1.getStringExtra("email"), intent1.getStringExtra("bankCode"), constants.sharedPreferences.getString("MobileNumber","N/A"), intent1.getStringExtra("currency"), intent1.getStringExtra("title"),constants.sharedPreferences.getString("LegalID","N/A"),intent1.getStringExtra("bankName"),intent1.getStringExtra("accountNumber"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getAddNonSamba();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                doneBtn.setEnabled(true);
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        Intent intent = new Intent(BeneficiaryVerifyScreen.this, BeneficiaryAddedConfirmationScreen.class);
//                                        intent.putExtra("alias", intent1.getStringExtra("alias"));
//                                        intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
//                                        intent.putExtra("accountType", intent1.getStringExtra("accountType"));
//                                        intent.putExtra("mobileNumber", intent1.getStringExtra("mobileNumber"));
//                                        intent.putExtra("email", intent1.getStringExtra("email"));
//                                        intent.putExtra("bankName", intent1.getStringExtra("bankName"));
//                                        intent.putExtra("beneType", intent1.getStringExtra("beneType"));
//                                        intent.putExtra("stan", intent1.getStringExtra("stan"));
//                                        intent.putExtra("rrn", intent1.getStringExtra("rrn"));
//                                        intent.putExtra("bankCode", intent1.getStringExtra("bankCode"));
//                                        if(!TextUtils.isEmpty(type) && type.equals("addAccount")){
//                                            intent.putExtra("type", type);
//                                            intent.putExtra("beneId", jsonObject.getString("ID"));
//                                        }else{
//                                            intent.putExtra("beneId", jsonObject.getString("BeneficiaryID"));
//                                            intent.putExtra("type", type);
//                                        }
//
//                                        overridePendingTransition(0, 0);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        startActivity(intent);
//                                        finish();
//                                        finishAffinity();
//                                        doneBtn.setEnabled(true);
//
//
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                        doneBtn.setEnabled(true);
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    doneBtn.setEnabled(true);
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            doneBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 10000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_Alias", intent1.getStringExtra("alias"));
                params.put("sSigns_AccountType", accountType);
                params.put("sSigns_AccountNumber", intent1.getStringExtra("accountNumber"));
                params.put("sSigns_UserMobileNo", "-");
                params.put("sSigns_UserEmailAddress", "-");
                params.put("sSigns_BankID", intent1.getStringExtra("bankCode"));
                params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sLegalIdentityType", _crypt.decrypt(constants.sharedPreferences.getString("LegalType", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sSigns_CustomerMobileNo", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sSigns_BankName", intent1.getStringExtra("bankName"));
                params.put("sSigns_AccountTitle", intent1.getStringExtra("title"));
                params.put("sComments", "-");
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(BeneficiaryVerifyScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.addNonSambaURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_AddNonSambaAccountResult").getString("Status_Code").equals("00")) {
//                                        JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_AddNonSambaAccountResult").getString("Status_Code"));
//                                        JsonObj.put("ID", jsonObject.getJSONObject("Signs_AddNonSambaAccountResult").getString("ID"));

                                            pd.dismiss();
                                            Intent intent = new Intent(BeneficiaryVerifyScreen.this, BeneficiaryAddedConfirmationScreen.class);
                                            intent.putExtra("alias", intent1.getStringExtra("alias"));
                                            intent.putExtra("accountNumber", intent1.getStringExtra("accountNumber"));
                                            intent.putExtra("accountType", intent1.getStringExtra("accountType"));
                                            intent.putExtra("mobileNumber", intent1.getStringExtra("mobileNumber"));
                                            intent.putExtra("email", intent1.getStringExtra("email"));
                                            intent.putExtra("bankName", intent1.getStringExtra("bankName"));
                                            intent.putExtra("beneType", intent1.getStringExtra("beneType"));
                                            intent.putExtra("stan", intent1.getStringExtra("stan"));
                                            intent.putExtra("rrn", intent1.getStringExtra("rrn"));
                                            intent.putExtra("bankCode", intent1.getStringExtra("bankCode"));
                                            if (!TextUtils.isEmpty(type) && type.equals("addAccount")) {
                                                intent.putExtra("type", type);
                                                intent.putExtra("beneId", jsonObject.getJSONObject("Signs_AddNonSambaAccountResult").getString("ID"));
                                            }
//                                        else{
//                                            intent.putExtra("beneId", jsonObject.getJSONObject("Signs_AddNonSambaAccountResult").getString("BeneficiaryID"));
//                                            intent.putExtra("type", type);
//                                        }

                                            overridePendingTransition(0, 0);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                            finish();
                                            finishAffinity();
                                            doneBtn.setEnabled(true);

                                        } else if (jsonObject.getJSONObject("Signs_AddNonSambaAccountResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_AddNonSambaAccountResult").getString("Status_Description"), "ERROR", BeneficiaryVerifyScreen.this, new LoginScreen());
                                            doneBtn.setEnabled(true);

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_AddNonSambaAccountResult").getString("Status_Description"), "ERROR");
                                            doneBtn.setEnabled(true);
                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        doneBtn.setEnabled(true);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    FirebaseAnalytics.getInstance(BeneficiaryVerifyScreen.this).logEvent("Error", new Bundle());
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    doneBtn.setEnabled(true);
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_Alias", intent1.getStringExtra("alias"));
//                        params.put("sSigns_AccountType", accountType);
//                        params.put("sSigns_AccountNumber", intent1.getStringExtra("accountNumber"));
//                        params.put("sSigns_UserMobileNo", "-");
//                        params.put("sSigns_UserEmailAddress", "-");
//                        params.put("sSigns_BankID", intent1.getStringExtra("bankCode"));
//                        params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sLegalIdentityType", constants.sharedPreferences.getString("LegalType", "N/A"));
//                        params.put("sSigns_CustomerMobileNo", constants.sharedPreferences.getString("MobileNumber", "N/A"));
//                        params.put("sSigns_BankName", intent1.getStringExtra("bankName"));
//                        params.put("sSigns_AccountTitle", intent1.getStringExtra("title"));
//                        params.put("sComments", "-");
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                doneBtn.setEnabled(true);

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            doneBtn.setEnabled(true);

        }
    }

    public void showDilogForErrorForMenu(String msg, String header) {

        dialog1 = new Dialog(BeneficiaryVerifyScreen.this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog1.findViewById(R.id.validatingTxt);
        btn = (TextView) dialog1.findViewById(R.id.doneBtn);
        heading = (TextView) dialog1.findViewById(R.id.heading);
        dialog1.setCancelable(false);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog1.dismiss();
                Intent intent = new Intent(BeneficiaryVerifyScreen.this, ManageBeneficiaresScreen.class);
                overridePendingTransition(0, 0);
                intent.putExtra("addBene", "addBene");
                startActivity(intent);
                finish();
                finishAffinity();

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog1.show();

    }
}