package com.ceesolutions.samba.beneficiariesManagement.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.beneficiariesManagement.BeneficiaryFormScreen;
import com.ceesolutions.samba.beneficiariesManagement.ManageBeneficiaresScreen;
import com.ceesolutions.samba.beneficiariesManagement.Model.UserModel;
import com.ceesolutions.samba.beneficiariesManagement.ReviewBeneficiaryScreen;
import com.ceesolutions.samba.friends.ManageFriends;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.views.CircularTextView;
import com.ceesolutions.samba.views.RoundImageView;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by ceeayaz on 3/6/18.
 */

public class CustomListBeneAdapter extends BaseSwipeAdapter {
    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    ArrayList<UserModel> userModels;
    LayoutInflater layoutInflater;
    View row;
    Context context;
    ImageView edit, trash, fav, arrow;
    TextView bankName, userName, accountNumber, titleTxt, heading, doneBtn, textView;
    RoundImageView title;
    ImageView click;
    int[] androidColors;
    private String isType;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Dialog pd, dialog;
    private Utils utils;
    private AppPreferences appPreferences;
    private String location;

    public CustomListBeneAdapter(ArrayList<UserModel> userModelArrayList, Context mContext, String type) {
        userModels = userModelArrayList;
        context = mContext;
        constants = Constants.getConstants(context);
        isType = type;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return userModels.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View generateView(final int position, ViewGroup parent) {
        View convertView = LayoutInflater.from(context).inflate(R.layout.bene_list_item, null);


        requestQueue = Volley.newRequestQueue(context);
        constants = Constants.getConstants(context);
        helper = Helper.getHelper(context);
        utils = new Utils(context);
        pd = new Dialog(context);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        appPreferences = new AppPreferences(context);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }


        return convertView;
    }

    @Override
    public void fillValues(final int position, View convertView) {

        arrow = (ImageView) convertView.findViewById(R.id.image);
        click = (ImageView) convertView.findViewById(R.id.click);
        edit = (ImageView) convertView.findViewById(R.id.edit);
        trash = (ImageView) convertView.findViewById(R.id.trash);
        fav = (ImageView) convertView.findViewById(R.id.fav);
        androidColors = context.getResources().getIntArray(R.array.androidcolors);
        titleTxt = (TextView) convertView.findViewById(R.id.titleTxt);
        title = (RoundImageView) convertView.findViewById(R.id.title);
        titleTxt.bringToFront();
//        title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(userModels.get(position).getFriendColor())));
        titleTxt.setText(userModels.get(position).getFirstLetter());
//        title = (CircularTextView) convertView.findViewById(R.id.title);
        bankName = (TextView) convertView.findViewById(R.id.bankName);
        accountNumber = (TextView) convertView.findViewById(R.id.accountNumber);
        userName = (TextView) convertView.findViewById(R.id.userName);
//
//
        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
//
        String color = String.format("#%06X", (0xFFFFFF & randomAndroidColor));
//
        if (!TextUtils.isEmpty(userModels.get(position).getBankColor()) && !userModels.get(position).getBankColor().equals(""))
            title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(userModels.get(position).getBankColor())));
        else
            title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(color)));
//
//        title.setText(userModels.get(position).getFirstLetter());
//
        bankName.setText(userModels.get(position).getBankName());
        userName.setText(userModels.get(position).getUserName());
        accountNumber.setText(userModels.get(position).getAccountNumber());

        if (userModels.get(position).getIsFavourite().equals("1")) {

            fav.setImageResource(R.drawable.fav_);
        } else {

            fav.setImageResource(R.drawable.fav);
        }

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(((Activity) context), BeneficiaryFormScreen.class);
                intent.putExtra("alias", userModels.get(position).getUserName());
                intent.putExtra("edit", "edit");
                intent.putExtra("accountNumber", userModels.get(position).getAccountNumber());
                intent.putExtra("bankCode", userModels.get(position).getBankCode());
                intent.putExtra("mobileNumber", userModels.get(position).getMobileNumber());
                intent.putExtra("email", userModels.get(position).getEmail());
                intent.putExtra("beneId", userModels.get(position).getBeneID());
                intent.putExtra("beneType", userModels.get(position).getBeneType());
                intent.putExtra("bankName", userModels.get(position).getBankName());
                intent.putExtra("accountTitle", userModels.get(position).getUserTitle());
                intent.putExtra("beneID", userModels.get(position).getBeneID());
                intent.putExtra("currency", userModels.get(position).getCurrency());
                intent.putExtra("isFav", userModels.get(position).getIsFavourite());
                intent.putExtra("bankColor", userModels.get(position).getBankColor());
                ((Activity) context).overridePendingTransition(0, 0);
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        });
        trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDilogForError("Are you sure you want to delete the beneficiary?", "CONFIRMATION", "Delete", position, "");
            }
        });
        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userModels.get(position).getIsFavourite().equals("1")) {
                    showDilogForError("Are you sure you want to remove the beneficiary from favourite?", "CONFIRMATION", "Fav", position, "0");
                } else {
                    showDilogForError("Are you sure you want to mark the beneficiary as favourite?", "CONFIRMATION", "Fav", position, "1");
                }
            }
        });

        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {


                    click.setEnabled(false);
                    if (isType.equals("sendMoney")) {

                        CryptLib _crypt = new CryptLib();
                        constants.destinationAccountEditor.putString("isSelect", "Yes");
                        constants.destinationAccountEditor.putString("userName", _crypt.encryptForParams(userModels.get(position).getUserTitle(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("accountNumber", _crypt.encryptForParams(userModels.get(position).getAccountNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("bankCode", _crypt.encryptForParams(userModels.get(position).getBankCode(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("currency", _crypt.encryptForParams(userModels.get(position).getCurrency(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("mobileNumber", _crypt.encryptForParams(userModels.get(position).getMobileNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("email", _crypt.encryptForParams(userModels.get(position).getEmail(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("beneType", _crypt.encryptForParams(userModels.get(position).getBeneType(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("beneID", _crypt.encryptForParams(userModels.get(position).getBeneID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("alias", _crypt.encryptForParams(userModels.get(position).getUserName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.commit();
                        constants.destinationAccountEditor.apply();
                        ((Activity) context).finish();
                        click.setEnabled(true);
                    } else {

//                        FirebaseAnalytics.getInstance(context).logEvent("Friend_Selection_For_Transfer_Pressed", new Bundle());

                        Intent intent = new Intent(((Activity) context), ReviewBeneficiaryScreen.class);
                        intent.putExtra("alias", userModels.get(position).getUserName());
                        intent.putExtra("accountNumber", userModels.get(position).getAccountNumber());
                        intent.putExtra("bankCode", userModels.get(position).getBankCode());
                        intent.putExtra("mobileNumber", userModels.get(position).getMobileNumber());
                        intent.putExtra("email", userModels.get(position).getEmail());
                        intent.putExtra("beneId", userModels.get(position).getBeneID());
                        intent.putExtra("beneType", userModels.get(position).getBeneType());
                        intent.putExtra("bankName", userModels.get(position).getBankName());
                        intent.putExtra("accountTitle", userModels.get(position).getUserTitle());
                        intent.putExtra("beneID", userModels.get(position).getBeneID());
                        intent.putExtra("currency", userModels.get(position).getCurrency());
                        intent.putExtra("isFav", userModels.get(position).getIsFavourite());
                        ((Activity) context).overridePendingTransition(0, 0);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                        click.setEnabled(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {

                    CryptLib _crypt = new CryptLib();
                    arrow.setEnabled(false);
                    if (isType.equals("sendMoney")) {


                        constants.destinationAccountEditor.putString("isSelect", "Yes");
                        constants.destinationAccountEditor.putString("userName", _crypt.encryptForParams(userModels.get(position).getUserTitle(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("accountNumber", _crypt.encryptForParams(userModels.get(position).getAccountNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("bankCode", _crypt.encryptForParams(userModels.get(position).getBankCode(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("currency", _crypt.encryptForParams(userModels.get(position).getCurrency(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("mobileNumber", _crypt.encryptForParams(userModels.get(position).getMobileNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("email", _crypt.encryptForParams(userModels.get(position).getEmail(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("beneType", _crypt.encryptForParams(userModels.get(position).getBeneType(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("beneID", _crypt.encryptForParams(userModels.get(position).getBeneID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("alias", _crypt.encryptForParams(userModels.get(position).getUserName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.commit();
                        constants.destinationAccountEditor.apply();
                        ((Activity) context).finish();
                        arrow.setEnabled(true);
                    } else {

//                        FirebaseAnalytics.getInstance(context).logEvent("Friend_Selection_For_Transfer_Pressed", new Bundle());

                        Intent intent = new Intent(((Activity) context), ReviewBeneficiaryScreen.class);
                        intent.putExtra("alias", userModels.get(position).getUserName());
                        intent.putExtra("accountNumber", userModels.get(position).getAccountNumber());
                        intent.putExtra("bankCode", userModels.get(position).getBankCode());
                        intent.putExtra("mobileNumber", userModels.get(position).getMobileNumber());
                        intent.putExtra("email", userModels.get(position).getEmail());
                        intent.putExtra("beneId", userModels.get(position).getBeneID());
                        intent.putExtra("beneType", userModels.get(position).getBeneType());
                        intent.putExtra("bankName", userModels.get(position).getBankName());
                        intent.putExtra("accountTitle", userModels.get(position).getUserTitle());
                        intent.putExtra("beneID", userModels.get(position).getBeneID());
                        intent.putExtra("currency", userModels.get(position).getCurrency());
                        intent.putExtra("isFav", userModels.get(position).getIsFavourite());
                        ((Activity) context).overridePendingTransition(0, 0);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                        arrow.setEnabled(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void DeleteBene(final int position) {


        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.DeletBene(constants.sharedPreferences.getString("userName", "N/A"), intent1.getStringExtra("beneId"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getRemoveBeneFav();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        Intent intent = new Intent(ReviewBeneficiaryScreen.this, ManageBeneficiaresScreen.class);
//                                        startActivity(intent);
//                                        overridePendingTransition(0, 0);
//                                        finish();
//
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//
//                        }
//                    }
//                }, 8000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_BeneID", userModels.get(position).getBeneID());
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(context);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.removeBeneURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_RemoveBeneficiaryResult").getString("Status_Code").equals("00")) {

                                            pd.dismiss();
                                            Intent intent = new Intent(context, ManageBeneficiaresScreen.class);
                                            ((Activity) context).overridePendingTransition(0, 0);
                                            ((Activity) context).startActivity(intent);
                                            ((Activity) context).finish();

                                        } else if (jsonObject.getJSONObject("Signs_RemoveBeneficiaryResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_RemoveBeneficiaryResult").getString("Status_Description"), "ERROR", context, new LoginScreen());


                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_RemoveBeneficiaryResult").getString("Status_Description"), "ERROR");
                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_BeneID", userModels.get(position).getBeneID());
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };
                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    public void AddBeneficiaryToFav(final int position, final String favFlag) {


        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
//                webService.AddBeneficiaryToFav(constants.sharedPreferences.getString("userName", "N/A"), intent1.getStringExtra("beneId"), "1", constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getAddBeneFav();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//
//                        }
//                    }
//                }, 8000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sSigns_BeneID", userModels.get(position).getBeneID());
                params.put("sSigns_BeneFavoriteFlag", favFlag);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(context);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.addBeneFavURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_BeneficiaryFavorite_Mark_UnMarkResult").getString("Status_Code").equals("00")) {
                                            pd.dismiss();

                                            Intent intent = new Intent(context, ManageBeneficiaresScreen.class);
                                            ((Activity) context).overridePendingTransition(0, 0);
                                            ((Activity) context).startActivity(intent);
                                            ((Activity) context).finish();
                                        } else if (jsonObject.getJSONObject("Signs_BeneficiaryFavorite_Mark_UnMarkResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BeneficiaryFavorite_Mark_UnMarkResult").getString("Status_Description"), "ERROR", context, new LoginScreen());

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_BeneficiaryFavorite_Mark_UnMarkResult").getString("Status_Description"), "ERROR");

                                        }
                                    } else {

                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sSigns_BeneID", userModels.get(position).getBeneID());
//                        params.put("sSigns_BeneFavoriteFlag", favFlag);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };


                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    public void showDilogForError(String msg, String header, final String type, final int position, final String sAction) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.error_dialog);
        textView = (TextView) dialog.findViewById(R.id.validatingTxt);
        doneBtn = (TextView) dialog.findViewById(R.id.doneBtn);
        heading = (TextView) dialog.findViewById(R.id.heading);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (type.equals("Delete")) {
                    dialog.dismiss();
                    DeleteBene(position);
                } else {
                    dialog.dismiss();
                    AddBeneficiaryToFav(position, sAction);
                }

            }
        });
        textView.setText(msg);
        heading.setText(header);
        dialog.show();

    }
}