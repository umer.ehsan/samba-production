package com.ceesolutions.samba.beneficiariesManagement;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputEditText;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.messageCenter.MessageCenter;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.ceesolutions.samba.utils.spinnerAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class BeneficiaryFormScreen extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String edit, latitude1;
    private AppCompatTextView message, messageForAccountNumber, errorMessageForEmail, errorMessageForMobNumber, errorMessageForAlias, errorMessageForAccount;
    private ImageView backbtn;
    private TextInputEditText editAlias, editAccountNumber, editMobileNumber, editEmailAddress, editBankName, editBeneficiaryDetail;
    private String alias, accountNumber, mobileNumber, emailAddress, accountType, bankName;
    private Spinner accountTypeSpinner, bankNameSpinner;
    private AppCompatTextView validateBtn;
    private String[] accountTypes = {
            "Conventional",
            "IBAN"

    };
    private boolean isValid = false;
    private WebService webService;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private String bankCode, stan, rrn, beneID, bankColor;
    private String beneType = "SAMBA";
    private String type;
    private ImageView backBtn, notificationBtn;
    private TextView headingTxt;
    private boolean isIBAN = false;
    private boolean isAdd = false;
    private AppPreferences appPreferences;
    private String location;
    private boolean isEdit = false;
    private int length = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beneficiary_form_screen);
        getSupportActionBar().hide();
        initViews();

        editAlias.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessageForAlias.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessageForAlias.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

                alias = s.toString();
            }
        });

        editAccountNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessageForAccount.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                errorMessageForAccount.setVisibility(View.VISIBLE);

            }

            @Override
            public void afterTextChanged(Editable s) {
                accountNumber = s.toString();
                accountNumber = accountNumber.replaceAll("-", "").trim();
            }
        });

        editMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessageForMobNumber.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessageForMobNumber.setVisibility(View.GONE);

            }

            @Override
            public void afterTextChanged(Editable s) {
                mobileNumber = s.toString();
                mobileNumber = mobileNumber.replaceAll("-", "").trim();
            }
        });

        editEmailAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                errorMessageForEmail.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                errorMessageForEmail.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
                emailAddress = s.toString();
            }
        });


        validateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAnalytics.getInstance(BeneficiaryFormScreen.this).logEvent("Add_Bene_Verification_Pressed", new Bundle());

                alias = editAlias.getText().toString().trim();
                accountNumber = editAccountNumber.getText().toString().trim();
                mobileNumber = editMobileNumber.getText().toString().trim();
                emailAddress = editEmailAddress.getText().toString().trim();

                if (!TextUtils.isEmpty(type) && type.equals("addAccount")) {


                } else {

                    if (TextUtils.isEmpty(emailAddress)) {

                        emailAddress = "N/A";
                        isValid = true;
                    } else {

                        if (!emailAddress.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") || emailAddress.contains(" ")) {
                            errorMessageForEmail.setVisibility(View.VISIBLE);
                            errorMessageForEmail.bringToFront();
                            errorMessageForEmail.setError("");
                            isValid = false;
                            errorMessageForEmail.setText("Please enter valid email address");
                        } else {

                            isValid = true;
                            emailAddress = editEmailAddress.getText().toString().trim();

                        }
                    }

                    if (TextUtils.isEmpty(mobileNumber)) {
                        mobileNumber = "N/A";
                    } else if (!helper.validateInputForSC(mobileNumber) || mobileNumber.contains(" ")) {

                        errorMessageForMobNumber.setVisibility(View.VISIBLE);
                        errorMessageForMobNumber.bringToFront();
                        errorMessageForMobNumber.setError("");
                        errorMessageForMobNumber.setText("Mobile number cannot contains <,>,\",',%,(,),&,+,\\,~,space");
                    } else {
                        mobileNumber = editMobileNumber.getText().toString().trim();
                    }

                }


                if (TextUtils.isEmpty(accountNumber)) {
                    errorMessageForAccount.setVisibility(View.VISIBLE);
                    errorMessageForAccount.bringToFront();
                    errorMessageForAccount.setError("");
                    errorMessageForAccount.setTextColor(getResources().getColor(R.color.lgreen));
                    errorMessageForAccount.setText("Please enter valid account number");

                } else {
                    if (!helper.validateInputForSC(accountNumber) || accountNumber.contains(" ")) {

                        errorMessageForAccount.setVisibility(View.VISIBLE);
                        errorMessageForAccount.bringToFront();
                        errorMessageForAccount.setError("");
                        errorMessageForAccount.setTextColor(getResources().getColor(R.color.lgreen));
                        errorMessageForAccount.setText("Account number cannot contains <,>,\",',%,(,),&,+,\\,~,space");


                    } else
                        accountNumber = editAccountNumber.getText().toString().trim();

                }


                if (TextUtils.isEmpty(alias)) {
                    errorMessageForAlias.setVisibility(View.VISIBLE);
                    errorMessageForAlias.bringToFront();
                    errorMessageForAlias.setError("");
                    errorMessageForAlias.setText("Please enter valid alias");

                } else if (!helper.validateInputForSC(alias)) {

                    errorMessageForAlias.setVisibility(View.VISIBLE);
                    errorMessageForAlias.bringToFront();
                    errorMessageForAlias.setError("");
                    errorMessageForAlias.setText("Alias name cannot contains <,>,\",',%,(,),&,+,\\,~");


                }

                if (!TextUtils.isEmpty(type) && type.equals("addAccount")) {

                    if ((!TextUtils.isEmpty(alias) && helper.validateInputForSC(alias)) && ((!TextUtils.isEmpty(accountNumber) && helper.validateInputForSC(accountNumber) && !accountNumber.contains(" ")))) {
//                    Intent intent = new Intent(UserValidationScreen.this, UserCodeVerificationScreen.class);
//                    startActivity(intent);
                        validateBtn.setEnabled(false);
                        Verification();
                    }
                } else {

                    if ((!TextUtils.isEmpty(alias) && helper.validateInputForSC(alias)) && (!TextUtils.isEmpty(mobileNumber) && helper.validateInputForSC(mobileNumber) && !mobileNumber.contains(" ")) && ((!TextUtils.isEmpty(accountNumber) && helper.validateInputForSC(accountNumber) && !accountNumber.contains(" "))) && ((!TextUtils.isEmpty(emailAddress) && isValid == true && helper.validateInputForSC(emailAddress) && !emailAddress.contains(" ")))) {
//                    Intent intent = new Intent(UserValidationScreen.this, UserCodeVerificationScreen.class);
//                    startActivity(intent);
                        validateBtn.setEnabled(false);
                        Verification();
                    }
                }

            }
        });
    }


    private void initViews() {

        backBtn = (ImageView) findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(edit) && edit.equals("edit")) {

                    Intent intent = new Intent(BeneficiaryFormScreen.this, ManageBeneficiaresScreen.class);
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                    finish();
                } else
                    finish();
            }
        });

        notificationBtn = (ImageView) findViewById(R.id.notificationBtn);
        notificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(BeneficiaryFormScreen.this, MessageCenter.class);
                overridePendingTransition(0, 0);
                startActivity(intent);
//                finish();
            }
        });
        editAlias = (TextInputEditText) findViewById(R.id.editAlias);
        headingTxt = (TextView) findViewById(R.id.headingTxt);
        editBankName = (TextInputEditText) findViewById(R.id.editBankName);
        editAccountNumber = (TextInputEditText) findViewById(R.id.editAccountNumber);
        editMobileNumber = (TextInputEditText) findViewById(R.id.editMobileNumber);
        editBeneficiaryDetail = (TextInputEditText) findViewById(R.id.editBeneficiaryDetail);
        editEmailAddress = (TextInputEditText) findViewById(R.id.editEmailAddress);
        errorMessageForAccount = (AppCompatTextView) findViewById(R.id.errorMessageForAccount);
        errorMessageForEmail = (AppCompatTextView) findViewById(R.id.errorMessageForEmail);
        errorMessageForAlias = (AppCompatTextView) findViewById(R.id.errorMessageForAlias);
        errorMessageForMobNumber = (AppCompatTextView) findViewById(R.id.errorMessageForMobNumber);
        pd = new Dialog(BeneficiaryFormScreen.this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        webService = new WebService();
        helper = Helper.getHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(BeneficiaryFormScreen.this);
        utils = new Utils(BeneficiaryFormScreen.this);
        validateBtn = (AppCompatTextView) findViewById(R.id.validateBtn);
        accountTypeSpinner = (Spinner) findViewById(R.id.accountTypeSpinner);
        int stanNumber = 6;
        stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
        int rrnNumber = 12;
        rrn = String.valueOf(helper.generateRandom(rrnNumber));
//        purposesSpinner.setSelection(((ArrayAdapter<String>) purposesSpinner.getAdapter()).getPosition(constants.destinationAccount.getString("purpose", "N/A")));

        final Intent intent = getIntent();

        type = intent.getStringExtra("type");

        if (!TextUtils.isEmpty(type) && type.equals("addAccount")) {

            isAdd = true;
            editMobileNumber.setVisibility(View.GONE);
            editEmailAddress.setVisibility(View.GONE);
            headingTxt.setText("Add Account");
            editBeneficiaryDetail.setText("Account Details");


        } else {

            //Do Nothing
        }
        editBankName.setText(intent.getStringExtra("bankName"));
        bankName = intent.getStringExtra("bankName");
        bankCode = intent.getStringExtra("bankCode");
        try {


            length = Integer.valueOf(intent.getStringExtra("length"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        edit = intent.getStringExtra("edit");

        if (!TextUtils.isEmpty(edit) && edit.equals("edit")) {


            isEdit = true;
            headingTxt.setText("Edit Beneficiary");
            alias = intent.getStringExtra("alias");
            editAlias.setText(intent.getStringExtra("alias"));
            accountNumber = intent.getStringExtra("accountNumber");
            editAccountNumber.setText(intent.getStringExtra("accountNumber"));
            editAccountNumber.setFocusableInTouchMode(false);
            bankName = intent.getStringExtra("bankName");
            editBankName.setText(intent.getStringExtra("bankName"));
            editBankName.setFocusableInTouchMode(false);
            String mob = intent.getStringExtra("mobileNumber");
            if (!TextUtils.isEmpty(mob) && !mob.equals("NA"))
                editMobileNumber.setText(intent.getStringExtra("mobileNumber"));

//            editMobileNumber.setText(intent.getStringExtra("mobileNumber"));
            mobileNumber = intent.getStringExtra("mobileNumber");
            String email = intent.getStringExtra("email");
            if (!TextUtils.isEmpty(email) && !email.equals("NA"))
                editEmailAddress.setText(intent.getStringExtra("email"));

//            editEmailAddress.setText(intent.getStringExtra("email"));
            emailAddress = intent.getStringExtra("email");
            bankCode = intent.getStringExtra("bankCode");
            beneType = intent.getStringExtra("beneType");
            beneID = intent.getStringExtra("beneId");
            bankColor = intent.getStringExtra("bankColor");
//            errorMessageForAccount.setVisibility(View.GONE);
//            intent.putExtra("bankCode", userModels.get(position).getBankCode());
//            intent.putExtra("mobileNumber", userModels.get(position).getMobileNumber());
//            intent.putExtra("email", userModels.get(position).getEmail());
//            intent.putExtra("beneId", userModels.get(position).getBeneID());
//            intent.putExtra("beneType", userModels.get(position).getBeneType());
//            intent.putExtra("bankName", userModels.get(position).getBankName());
//            intent.putExtra("accountTitle", userModels.get(position).getUserTitle());
//            intent.putExtra("beneID", userModels.get(position).getBeneID());
//            intent.putExtra("currency", userModels.get(position).getCurrency());
//            intent.putExtra("isFav", userModels.get(position).getIsFavourite());
        }

        appPreferences = new AppPreferences(BeneficiaryFormScreen.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }

        spinnerAdapter accountAdapter = new spinnerAdapter(BeneficiaryFormScreen.this, R.layout.custom_textview_fp);
        accountAdapter.addAll(accountTypes);
        accountAdapter.add("Account Type");
        accountTypeSpinner.setAdapter(accountAdapter);
        accountTypeSpinner.setSelection(0);
        if (!TextUtils.isEmpty(edit) && edit.equals("edit")) {
            if (accountNumber.startsWith("PK")) {
                accountTypeSpinner.setSelection(((ArrayAdapter<String>) accountTypeSpinner.getAdapter()).getPosition("IBAN"));
                accountTypeSpinner.setClickable(false);
                accountTypeSpinner.setEnabled(false);
                accountType = "IBAN";

            } else {

                accountTypeSpinner.setSelection(((ArrayAdapter<String>) accountTypeSpinner.getAdapter()).getPosition("Conventional"));
                accountTypeSpinner.setClickable(false);
                accountTypeSpinner.setEnabled(false);
                accountType = "Conventional";

            }
        }

        accountTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (accountTypeSpinner.getSelectedItem() == "Account Type") {
                        accountType = accountTypeSpinner.getSelectedItem().toString();
                    } else {
                        accountType = accountTypeSpinner.getSelectedItem().toString();
                        editAlias.requestFocus();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(editAccountNumber, InputMethodManager.SHOW_IMPLICIT);
                        if (accountType.equals("IBAN")) {

                            int maxLengthofEditText;
                            isIBAN = true;

                            if (!TextUtils.isEmpty(bankCode) && bankCode.equals("69")) {
                                maxLengthofEditText = 24;
                                errorMessageForAccount.setVisibility(View.VISIBLE);
                                errorMessageForAccount.setError(null);
//                                errorMessageForAccount.setText(intent.getStringExtra("display"));
                                errorMessageForAccount.setText("Please enter 24 digit account number");
                                errorMessageForAccount.setTextColor(getResources().getColor(R.color.lgreen));

                            } else {
                                maxLengthofEditText = 24;
                                errorMessageForAccount.setVisibility(View.VISIBLE);
                                errorMessageForAccount.setError(null);
//                                errorMessageForAccount.setText(intent.getStringExtra("display"));
                                errorMessageForAccount.setText("Please enter 24 digit account number");
                                errorMessageForAccount.setTextColor(getResources().getColor(R.color.lgreen));
                            }

                            editAccountNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthofEditText)});
                            editAccountNumber.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
//                            editAccountNumber.setKeyListener(DigitsKeyListener.getInstance("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 "));

                            if (isEdit) {
                                errorMessageForAccount.setVisibility(View.GONE);
                            } else
                                editAccountNumber.setText("");

                        } else {
                            int maxLengthOfEditTextCon;
                            if (!TextUtils.isEmpty(bankCode) && bankCode.equals("69")) {

                                if (isAdd) {

                                } else {

                                    maxLengthOfEditTextCon = 10;
                                    editAccountNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthOfEditTextCon)});
                                    errorMessageForAccount.setVisibility(View.VISIBLE);
                                    errorMessageForAccount.setError(null);
                                    errorMessageForAccount.setText(intent.getStringExtra("display"));
                                    errorMessageForAccount.setTextColor(getResources().getColor(R.color.lgreen));

                                }

                            } else {

                                maxLengthOfEditTextCon = length;
                                editAccountNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthOfEditTextCon)});
                                errorMessageForAccount.setVisibility(View.VISIBLE);
                                errorMessageForAccount.setError(null);
                                errorMessageForAccount.setText(intent.getStringExtra("display"));
                                errorMessageForAccount.setTextColor(getResources().getColor(R.color.lgreen));
                            }
                            editAccountNumber.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_CLASS_NUMBER);
                            editAccountNumber.setKeyListener(DigitsKeyListener.getInstance("1234567890"));
                            isIBAN = false;
//                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                            imm.showSoftInput(editAccountNumber, InputMethodManager.SHOW_IMPLICIT);
                            if (isEdit) {
                                errorMessageForAccount.setVisibility(View.GONE);
                            } else
                                editAccountNumber.setText("");

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void onBackPressed() {
//        backbtn.setEnabled(false);
        if (!TextUtils.isEmpty(edit) && edit.equals("edit")) {

            Intent intent = new Intent(BeneficiaryFormScreen.this, ManageBeneficiaresScreen.class);
            overridePendingTransition(0, 0);
            startActivity(intent);
            finish();
        } else
            finish();
    }


    public void Verification() {


        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                int stanNumber = 6;
                stan = String.valueOf(helper.nDigitRandomNo(stanNumber));
                int rrnNumber = 12;
                rrn = String.valueOf(helper.generateRandom(rrnNumber));
//                webService.TitleFetch(constants.sharedPreferences.getString("userName", "N/A"), "2000252444", accountNumber, "PKR", bankCode, constants.sharedPreferences.getString("LegalID", "N/A"), stan, rrn, beneType, constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getTitleFetch();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
////                                Toast.makeText(getActivity(), "Question cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                validateBtn.setEnabled(true);
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
////                                        Toast.makeText(getActivity(), "Secret Question Answer has been added successfully !", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        Intent intent = new Intent(BeneficiaryFormScreen.this, BeneficiaryVerifyScreen.class);
//                                        intent.putExtra("alias", alias);
//                                        intent.putExtra("accountNumber", accountNumber);
//                                        intent.putExtra("accountType", accountType);
//                                        intent.putExtra("mobileNumber", mobileNumber);
//                                        intent.putExtra("email", emailAddress);
//                                        intent.putExtra("bankName", bankName);
//                                        intent.putExtra("beneType", beneType);
//                                        intent.putExtra("stan", stan);
//                                        intent.putExtra("rrn", rrn);
//                                        intent.putExtra("bankCode", bankCode);
//                                        if (!TextUtils.isEmpty(type) && type.equals("addAccount")) {
//                                            intent.putExtra("type", type);
//                                        } else {
//
//                                            intent.putExtra("type", type);
//                                        }
//                                        intent.putExtra("currency", "PKR");
//                                        intent.putExtra("title", jsonObject.getString("AccountTitle"));
//                                        intent.putExtra("currency", jsonObject.getString("Currency"));
//                                        overridePendingTransition(0, 0);
//                                        startActivity(intent);
//                                        validateBtn.setEnabled(true);
//
//
//
//                                    } else {
////                                        Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                        validateBtn.setEnabled(true);
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                    validateBtn.setEnabled(true);
//
//                                }
//                            }
//                        } else {
////                            Toast.makeText(getActivity(), "Secret Question Answer cannot be added. Please try again later!", Toast.LENGTH_LONG).show();
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                            validateBtn.setEnabled(true);
//
//
//                        }
//                    }
//                }, 15000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sSigns_Username", user);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sToAccountNumber", accountNumber);
                params.put("sBankID", bankCode);
                params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sSTAN", stan);
                params.put("sRRN", rrn);
                params.put("sBeneType", beneType);
                params.put("sFromAccountNumber", constants.accountNumber);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));
                params.put("sBeneCurrencyCode", "PKR");
                params.put("sServerSessionKey", md5);
                HttpsTrustManager.allowMySSL(BeneficiaryFormScreen.this);
                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.titleFetchURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Code").equals("00")) {

                                            pd.dismiss();
                                            JsonObj.put("Status_Code", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Code"));
                                            JsonObj.put("ChargeCode", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("ChargeCode"));
                                            JsonObj.put("ConversionRate", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("ConversionRate"));
                                            JsonObj.put("AccountTitle", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("AccountTitle"));
                                            JsonObj.put("Currency", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Currency"));
                                            Intent intent = new Intent(BeneficiaryFormScreen.this, BeneficiaryVerifyScreen.class);
                                            intent.putExtra("alias", alias);
                                            intent.putExtra("accountNumber", accountNumber);
                                            intent.putExtra("accountType", accountType);
                                            intent.putExtra("mobileNumber", mobileNumber);
                                            intent.putExtra("email", emailAddress);
                                            intent.putExtra("bankName", bankName);
                                            intent.putExtra("beneType", beneType);
                                            intent.putExtra("stan", stan);
                                            intent.putExtra("rrn", rrn);
                                            intent.putExtra("bankCode", bankCode);
                                            if (!TextUtils.isEmpty(type) && type.equals("addAccount")) {
                                                intent.putExtra("type", type);
                                            } else {

                                                intent.putExtra("type", type);
                                            }
                                            if (!TextUtils.isEmpty(edit) && edit.equals("edit")) {
                                                intent.putExtra("edit", edit);
                                                intent.putExtra("beneId", beneID);
                                                intent.putExtra("bankColor", bankColor);
                                            }
                                            intent.putExtra("currency", "PKR");
                                            intent.putExtra("title", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("AccountTitle"));
                                            intent.putExtra("currency", jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Currency"));
                                            overridePendingTransition(0, 0);
                                            startActivity(intent);
                                            editAlias.getText().clear();
                                            editEmailAddress.getText().clear();
                                            editAccountNumber.getText().clear();
                                            editAlias.getText().clear();
                                            editMobileNumber.getText().clear();
                                            validateBtn.setEnabled(true);
//                                        clearView();

                                        } else if (jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Code").equals("54")) {
                                            pd.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Description"), "ERROR", BeneficiaryFormScreen.this, new LoginScreen());
                                            validateBtn.setEnabled(true);
                                            clearView();

                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("Signs_TitleFetchResult").getString("Status_Description"), "ERROR");
                                            validateBtn.setEnabled(true);
                                            clearView();
                                        }
                                    } else {
                                        pd.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        validateBtn.setEnabled(true);
                                        clearView();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    clearView();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    clearView();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    pd.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    validateBtn.setEnabled(true);
                                    clearView();
                                }
                            }
                        })
//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                        params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sToAccountNumber", accountNumber);
//                        params.put("sBankID", bankCode);
//                        params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sSTAN", stan);
//                        params.put("sRRN", rrn);
//                        params.put("sBeneType", beneType);
//                        params.put("sFromAccountNumber", constants.accountNumber);
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        params.put("sBeneCurrencyCode", "PKR");
//                        params.put("sServerSessionKey", md5);
//                        return params;
//                    }
//                };
                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connectivity!", Toast.LENGTH_LONG).show();
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");
                validateBtn.setEnabled(true);
                clearView();

            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connectivity", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
            validateBtn.setEnabled(true);
            clearView();

        }
    }

    public void clearView() {


        editAlias.getText().clear();
        editEmailAddress.getText().clear();
        editAccountNumber.getText().clear();
        editAlias.getText().clear();
        editMobileNumber.getText().clear();
        editAlias.requestFocus();
    }

}
