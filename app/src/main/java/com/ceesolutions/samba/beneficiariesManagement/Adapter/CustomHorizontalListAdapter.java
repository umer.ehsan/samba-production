package com.ceesolutions.samba.beneficiariesManagement.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.beneficiariesManagement.Model.UserModel;
import com.ceesolutions.samba.beneficiariesManagement.ReviewBeneficiaryScreen;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.views.CircularTextView;
import com.ceesolutions.samba.views.RoundImageView;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ceeayaz on 4/1/18.
 */

public class CustomHorizontalListAdapter extends BaseAdapter {
    ArrayList<UserModel> userModels;
    LayoutInflater layoutInflater;
    View row;
    Context context;
    TextView userName, titleTxt;
    RoundImageView title;
    int[] androidColors;
    private Constants constants;
    private String isType;

    public CustomHorizontalListAdapter(ArrayList<UserModel> userModelArrayList, Context mContext, String type) {
        userModels = userModelArrayList;
        context = mContext;
        constants = Constants.getConstants(context);
        isType = type;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return userModels.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.favourite_list_item, parent, false);
        }

        androidColors = context.getResources().getIntArray(R.array.androidcolors);
        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];

        String color = String.format("#%06X", (0xFFFFFF & randomAndroidColor));
        titleTxt = (TextView) convertView.findViewById(R.id.titleTxt);
        title = (RoundImageView) convertView.findViewById(R.id.title);
        titleTxt.bringToFront();
        userName = (TextView) convertView.findViewById(R.id.userName);

        if (!TextUtils.isEmpty(userModels.get(position).getBankColor()) && !userModels.get(position).getBankColor().equals("")) {

            title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(userModels.get(position).getBankColor())));
            titleTxt.setText(userModels.get(position).getFirstLetter());
        } else {

            title.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(color)));
            titleTxt.setText(userModels.get(position).getFirstLetter());
        }
//        title.setSolidColor(color);
//
//        title.setText(userModels.get(position).getFirstLetter());


        userName.setText(userModels.get(position).getUserName());

        final View finalConvertView = convertView;
        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    CryptLib _crypt = new CryptLib();

                    title.setEnabled(false);
                    if (isType.equals("sendMoney")) {
                        constants.destinationAccountEditor.putString("isSelect", "Yes");
                        constants.destinationAccountEditor.putString("userName", _crypt.encryptForParams(userModels.get(position).getAccountTitle(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("accountNumber", _crypt.encryptForParams(userModels.get(position).getAccountNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("bankCode", _crypt.encryptForParams(userModels.get(position).getBankCode(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("currency", _crypt.encryptForParams(userModels.get(position).getCurrency(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("mobileNumber", _crypt.encryptForParams(userModels.get(position).getMobileNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("email", _crypt.encryptForParams(userModels.get(position).getEmail(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("beneType", _crypt.encryptForParams(userModels.get(position).getBeneType(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.putString("beneID", _crypt.encryptForParams(userModels.get(position).getBeneID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                        constants.destinationAccountEditor.commit();
                        constants.destinationAccountEditor.apply();
                        ((Activity) context).finish();
                        title.setEnabled(true);
                    } else {

//                        FirebaseAnalytics.getInstance(context).logEvent("Friend_Selection_For_Transfer_Pressed", new Bundle());

                        Intent intent = new Intent(((Activity) context), ReviewBeneficiaryScreen.class);
                        intent.putExtra("alias", userModels.get(position).getUserName());
                        intent.putExtra("accountNumber", userModels.get(position).getAccountNumber());
                        intent.putExtra("bankCode", userModels.get(position).getBankCode());
                        intent.putExtra("mobileNumber", userModels.get(position).getMobileNumber());
                        intent.putExtra("email", userModels.get(position).getEmail());
                        intent.putExtra("beneId", userModels.get(position).getBeneID());
                        intent.putExtra("beneType", userModels.get(position).getBeneType());
                        intent.putExtra("bankName", userModels.get(position).getBankName());
                        intent.putExtra("accountTitle", userModels.get(position).getAccountTitle());
                        intent.putExtra("beneID", userModels.get(position).getBeneID());
                        intent.putExtra("currency", userModels.get(position).getCurrency());
                        intent.putExtra("isFav", userModels.get(position).getIsFavourite());
                        ((Activity) context).overridePendingTransition(0, 0);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                        title.setEnabled(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        return convertView;
    }
}