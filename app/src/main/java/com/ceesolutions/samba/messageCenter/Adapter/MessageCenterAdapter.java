package com.ceesolutions.samba.messageCenter.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.messageCenter.AcceptFriendRequestScreen;
import com.ceesolutions.samba.messageCenter.Model.Message;
import com.ceesolutions.samba.transactions.FundTransferScreen;

import java.util.List;

/**
 * Created by ayaz on 1/17/18.
 */

public class MessageCenterAdapter extends RecyclerView.Adapter<MessageCenterAdapter.MyViewHolder> {

    private Context mContext;
    private List<Message> messageList;
    private View itemView;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, timeStamp, description,addFriend,reject;
        public View view1;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            description = (TextView) view.findViewById(R.id.description);
            addFriend = (TextView) view.findViewById(R.id.addFriend);
            reject = (TextView)view.findViewById(R.id.reject);
            view1 = (View) view.findViewById(R.id.view);
        }
    }


    public MessageCenterAdapter(Context mContext, List<Message> messageList) {
        this.mContext = mContext;
        this.messageList = messageList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Message Message = messageList.get(position);
        holder.title.setText(Message.getTitle());
        holder.description.setText(Message.getDescription());
        if(messageList.get(position).getMessageType().equals("information")){

            holder.addFriend.setVisibility(View.GONE);
            holder.reject.setVisibility(View.GONE);
            holder.view1.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            itemView.setEnabled(false);
        }else{

            itemView.setEnabled(true);
            holder.addFriend.setVisibility(View.VISIBLE);
            holder.reject.setVisibility(View.VISIBLE);
            holder.view1.setBackgroundColor(mContext.getResources().getColor(R.color.lgrey));
        }
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                itemView.setEnabled(false);
                if (messageList.get(position).getMessageType().equals("information")) {

                    itemView.setEnabled(true);

                } else {
                    if (messageList.get(position).getTargetFormId().equals("335")) {

                        Intent intent = new Intent(((Activity) mContext), AcceptFriendRequestScreen.class);
                        intent.putExtra("userName", messageList.get(position).getUserName());
                        intent.putExtra("friendName", messageList.get(position).getFriendName());
                        intent.putExtra("friendNumber", messageList.get(position).getFriendNumber());
                        intent.putExtra("userId", messageList.get(position).getUserId());
                        intent.putExtra("userNumber", messageList.get(position).getUserNumber());
                        intent.putExtra("messageId", messageList.get(position).getMessageId());
                        intent.putExtra("request_id", messageList.get(position).getRequestID());
                        ((Activity) mContext).overridePendingTransition(0, 0);
                        mContext.startActivity(intent);
                        ((Activity) mContext).finish();
                        itemView.setEnabled(true);

                    } else if (messageList.get(position).getTargetFormId().equals("160")) {

                        Intent intent = new Intent(((Activity) mContext), FundTransferScreen.class);
                        intent.putExtra("userName", messageList.get(position).getUserName());
                        intent.putExtra("friendName", messageList.get(position).getFriendName());
                        intent.putExtra("friendNumber", messageList.get(position).getFriendNumber());
                        intent.putExtra("userId", messageList.get(position).getUserId());
                        intent.putExtra("userNumber", messageList.get(position).getUserNumber());
                        intent.putExtra("messageId", messageList.get(position).getMessageId());
                        intent.putExtra("remarks", messageList.get(position).getRemarks());
                        intent.putExtra("purpose", messageList.get(position).getPurpose());
                        intent.putExtra("amount", messageList.get(position).getAmount());
                        intent.putExtra("accountNumber", messageList.get(position).getAccountNumber());
                        intent.putExtra("requestFunds", "requestMoney");
                        intent.putExtra("accountTitle", messageList.get(position).getAccountTitle());
                        intent.putExtra("bankCode", messageList.get(position).getBankCode());
                        intent.putExtra("friend_id", messageList.get(position).getFriendID());

                        ((Activity) mContext).overridePendingTransition(0, 0);
                        mContext.startActivity(intent);
                        ((Activity) mContext).finish();
                        itemView.setEnabled(true);

                    }
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }
}