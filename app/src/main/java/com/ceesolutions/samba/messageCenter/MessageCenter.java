package com.ceesolutions.samba.messageCenter;

/**
 * Created by ayaz on 12/18/17.
 */

import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.messageCenter.Adapter.CustomListAdapter;
import com.ceesolutions.samba.messageCenter.Adapter.MessageCenterAdapter;
import com.ceesolutions.samba.messageCenter.Model.Message;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.utils.WebService;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MessageCenter extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public String longitude1;
    public String latitude1;
    private ListView recyclerView;
    private ArrayList<Message> messageList;
    private MessageCenterAdapter adapter;
    private Dialog dialog;
    private RequestQueue requestQueue;
    private WebService webService;
    private Helper helper;
    private Utils utils;
    private Constants constants;
    private ImageView backBtn;
    private AppPreferences appPreferences;
    private String location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_center);

        getSupportActionBar().hide();
        backBtn = (ImageView) findViewById(R.id.backBtn);
        recyclerView = (ListView) findViewById(R.id.listView);
        requestQueue = Volley.newRequestQueue(this);
        constants = Constants.getConstants(MessageCenter.this);
        webService = new WebService();
        helper = Helper.getHelper(this);
        utils = new Utils(MessageCenter.this);
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        messageList = new ArrayList<>();
        appPreferences = new AppPreferences(MessageCenter.this);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");


        FirebaseAnalytics.getInstance(MessageCenter.this).logEvent("Message_Center_View_Msgs", new Bundle());


        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


        GetMessages();
//        prepareMessages();
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public void GetMessages() {

        //  Toast.makeText(LoginWithPassAndPinScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                dialog.show();
                //constants.sharedPreferences.getString("LegalID", "N/A")
//                webService.GetMessages(constants.sharedPreferences.getString("LegalID", "N/A"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getMessagesList();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//                                dialog.dismiss();
////                                Toast.makeText(LoginWithPassAndPinScreen.this, "Invalid Username/iPIN!", Toast.LENGTH_LONG).show();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                            } else {
//
//                                try {
//
//                                    JSONArray messages = new JSONArray(jsonObject.getString("messages"));
//
//                                    if (messages.length() > 0) {
//                                        for (int i = 0; i < messages.length(); i++) {
//                                            try {
//
//
//                                                JSONObject jsonObj = new JSONObject(messages.get(i).toString());
//                                                Message message = new Message();
//                                                if (jsonObj.has("target_form_id"))
//                                                    message.setTargetFormId(jsonObj.getString("target_form_id"));
//
//                                                if (jsonObj.has("message"))
//                                                    message.setDescription(jsonObj.getString("message"));
//
//                                                if (jsonObj.has("message_title"))
//                                                    message.setTitle(jsonObj.getString("message_title"));
//
//                                                if (jsonObj.has("friend_number"))
//                                                    message.setFriendNumber(jsonObj.getString("friend_number"));
//
//                                                if (jsonObj.has("friend_name"))
//                                                    message.setFriendName(jsonObj.getString("friend_name"));
//
//                                                if (jsonObj.has("friend_id"))
//                                                    message.setFriendID(jsonObj.getString("friend_id"));
//
//                                                if (jsonObj.has("user_id"))
//                                                    message.setUserId(jsonObj.getString("user_id"));
//
//                                                if (jsonObj.has("user_name"))
//                                                    message.setUserName(jsonObj.getString("user_name"));
//
//                                                if (jsonObj.has("messageId"))
//                                                    message.setMessageId(jsonObj.getString("messageId"));
//
//                                                if (jsonObj.has("user_number"))
//                                                    message.setUserNumber(jsonObj.getString("user_number"));
//
//                                                if (jsonObj.has("request_remarks"))
//                                                    message.setRemarks(jsonObj.getString("request_remarks"));
//
//                                                if (jsonObj.has("request_purpose"))
//                                                    message.setPurpose(jsonObj.getString("request_purpose"));
//
//                                                if (jsonObj.has("request_amount"))
//                                                    message.setAmount(jsonObj.getString("request_amount"));
//
//                                                if (jsonObj.has("to_account_number"))
//                                                    message.setAccountNumber(jsonObj.getString("to_account_number"));
//
//                                                if (jsonObj.has("to_account_title"))
//                                                    message.setAccountTitle(jsonObj.getString("to_account_title"));
//
//
//                                                if (jsonObj.has("bank_code"))
//                                                    message.setBankCode(jsonObj.getString("bank_code"));
//
//                                                if (jsonObj.has("request_id"))
//                                                    message.setRequestID(jsonObj.getString("request_id"));
//
//                                                if (jsonObj.has("message_type"))
//                                                    message.setMessageType(jsonObj.getString("message_type"));
//
//
//                                                if (jsonObj.has("request_bill_alias"))
//                                                    message.setBillAlias(jsonObj.getString("request_bill_alias"));
//
//
//                                                if (jsonObj.has("request_billing_month"))
//                                                    message.setBillingMonth(jsonObj.getString("request_billing_month"));
//
//                                                if (jsonObj.has("request_status"))
//                                                    message.setBillingStatus(jsonObj.getString("request_status"));
//
//                                                if (jsonObj.has("request_consumer_name"))
//                                                    message.setConsumerName(jsonObj.getString("request_consumer_name"));
//
//                                                if (jsonObj.has("request_consumer_number"))
//                                                    message.setConusmerNumber(jsonObj.getString("request_consumer_number"));
//
//                                                if (jsonObj.has("request_amount_after_due_date"))
//                                                    message.setAmountAfterDueDate(jsonObj.getString("request_amount_after_due_date"));
//
//                                                if (jsonObj.has("request_distributor_id"))
//                                                    message.setDistributorID(jsonObj.getString("request_distributor_id"));
//
//                                                if (jsonObj.has("request_due_date"))
//                                                    message.setDueDate(jsonObj.getString("request_due_date"));
//
//
//                                                if (jsonObj.has("category_type"))
//                                                    message.setCategoryType(jsonObj.getString("category_type"));
//
//
//                                                if (jsonObj.has("distributor_account_number"))
//                                                    message.setDistributorAccountNumber(jsonObj.getString("distributor_account_number"));
//
//                                                if (jsonObj.has("voucher_max_amount"))
//                                                    message.setMaxAmount(jsonObj.getString("voucher_max_amount"));
//
//
//                                                if (jsonObj.has("voucher_min_amount"))
//                                                    message.setMinAmount(jsonObj.getString("voucher_min_amount"));
//
//
//                                                messageList.add(message);
//
//
//                                            } catch (Exception ex) {
//                                                ex.printStackTrace();
//                                                dialog.dismiss();
//                                            }
//
//
//                                        }
//
////                                        adapter = new MessageCenterAdapter(MessageCenter.this, messageList);
////                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MessageCenter.this);
////                                        recyclerView.setLayoutManager(mLayoutManager);
////                                        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(0), true));
////                                        recyclerView.setItemAnimator(new DefaultItemAnimator());
////                                        recyclerView.setAdapter(adapter);
////                                        adapter.notifyDataSetChanged();
//
//                                        CustomListAdapter customListAdapter = new CustomListAdapter(messageList, MessageCenter.this);
//                                        recyclerView.setAdapter(customListAdapter);
//
//                                        dialog.dismiss();
//
//                                    } else {
//                                        dialog.dismiss();
//                                    }
//
//
//                                } catch (Exception ex) {
//                                    ex.printStackTrace();
//                                    dialog.dismiss();
//                                }
//                            }
//                        } else {
//                            dialog.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                        }
//                    }
//                }, 3000);
                CryptLib _crypt = new CryptLib();
                String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                Map<String, String> params = new HashMap<String, String>();
                params.put("sService_Username", constants.signsUserName);
                params.put("sService_Password", constants.signsPassword);
                params.put("sIPAddress", Utils.getIPAddress(true));
                params.put("sMacAddress", constants.address);
                params.put("sOS_Version", constants.version);
                params.put("sDeviceName", constants.model);
                params.put("sAppVersion", constants.appVersion);
                params.put("sService_SharedKey", Constants.Shared_KEY);
                params.put("sTimeStamp", constants.dateTime);
                params.put("sLegalIdentityValue", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                params.put("sServerSessionKey", md5);
                params.put("sSigns_Username", user);
                params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                params.put("sDeviceLongitude", String.valueOf(longitude));


                final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());


                HttpsTrustManager.allowMySSL(MessageCenter.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.getMessagesURL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // response

                                Log.d("Response---", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject JsonObj = new JSONObject();
                                    if (jsonObject.getString("ServerKey").equals(md5)) {
                                        if (jsonObject.getJSONObject("message_center_for_userResult").getString("Status_Code").equals("00")) {

                                            try {

                                                JSONArray messages = new JSONArray(jsonObject.getJSONObject("message_center_for_userResult").getString("messages"));

                                                if (messages.length() > 0) {
                                                    for (int i = 0; i < messages.length(); i++) {
                                                        try {


                                                            JSONObject jsonObj = new JSONObject(messages.get(i).toString());
                                                            Message message = new Message();
                                                            if (jsonObj.has("target_form_id")) {
                                                                message.setTargetFormId(jsonObj.getString("target_form_id"));

                                                            }

                                                            if (jsonObj.has("message"))
                                                                message.setDescription(jsonObj.getString("message"));

                                                            if (jsonObj.has("message_title"))
                                                                message.setTitle(jsonObj.getString("message_title"));

                                                            if (jsonObj.has("friend_number"))
                                                                message.setFriendNumber(jsonObj.getString("friend_number"));

                                                            if (jsonObj.has("friend_name"))
                                                                message.setFriendName(jsonObj.getString("friend_name"));

                                                            if (jsonObj.has("friend_id"))
                                                                message.setFriendID(jsonObj.getString("friend_id"));

                                                            if (jsonObj.has("user_id"))
                                                                message.setUserId(jsonObj.getString("user_id"));

                                                            if (jsonObj.has("friend_username"))
                                                                message.setFriendUserName(jsonObj.getString("friend_username"));

                                                            if (jsonObj.has("user_name"))
                                                                message.setUserName(jsonObj.getString("user_name"));

                                                            if (jsonObj.has("messageId"))
                                                                message.setMessageId(jsonObj.getString("messageId"));

                                                            if (jsonObj.has("request_form_id"))
                                                                message.setBankCode(jsonObj.getString("request_form_id"));

                                                            if (jsonObj.has("friend_image")) {
                                                                message.setFriendImage(jsonObj.getString("friend_image").replaceAll("@PLUS@", "\\+"));

                                                            } else {
                                                                if (message.getTargetFormId().equals("160")) {
                                                                    String imageUri = "drawable://" + R.drawable.transfer_request;
                                                                    message.setImage(getURLForResource(R.drawable.transfer_request));
                                                                } else if (message.getTargetFormId().equals("250") || message.getTargetFormId().equals("260")) {
                                                                    String imageUri = "drawable://" + R.drawable.bill_payment;
                                                                    message.setImage(getURLForResource(R.drawable.bill_payment));

                                                                } else if (message.getTargetFormId().equals("1001") || message.getTargetFormId().equals("335")) {


                                                                } else {
                                                                    String imageUri = "drawable://" + R.drawable.notification_center;
                                                                    message.setImage(getURLForResource(R.drawable.notification_center));

                                                                }

                                                            }

                                                            if (jsonObj.has("user_number"))
                                                                message.setUserNumber(jsonObj.getString("user_number"));

                                                            if (jsonObj.has("request_remarks"))
                                                                message.setRemarks(jsonObj.getString("request_remarks"));

                                                            if (jsonObj.has("request_purpose"))
                                                                message.setPurpose(jsonObj.getString("request_purpose"));

                                                            if (jsonObj.has("request_amount"))
                                                                message.setAmount(jsonObj.getString("request_amount"));

                                                            if (jsonObj.has("to_account_number"))
                                                                message.setAccountNumber(jsonObj.getString("to_account_number"));

                                                            if (jsonObj.has("to_account_title"))
                                                                message.setAccountTitle(jsonObj.getString("to_account_title"));


//                                                        if (jsonObj.has("bank_code"))
//                                                            message.setBankCode(jsonObj.getString("bank_code"));

                                                            if (jsonObj.has("request_id"))
                                                                message.setRequestID(jsonObj.getString("request_id"));

                                                            if (jsonObj.has("message_type"))
                                                                message.setMessageType(jsonObj.getString("message_type"));


                                                            if (jsonObj.has("request_bill_alias"))
                                                                message.setBillAlias(jsonObj.getString("request_bill_alias"));


                                                            if (jsonObj.has("request_billing_month"))
                                                                message.setBillingMonth(jsonObj.getString("request_billing_month"));

                                                            if (jsonObj.has("request_status"))
                                                                message.setBillingStatus(jsonObj.getString("request_status"));

                                                            if (jsonObj.has("request_consumer_name"))
                                                                message.setConsumerName(jsonObj.getString("request_consumer_name"));

                                                            if (jsonObj.has("request_consumer_number"))
                                                                message.setConusmerNumber(jsonObj.getString("request_consumer_number"));

                                                            if (jsonObj.has("request_amount_after_due_date"))
                                                                message.setAmountAfterDueDate(jsonObj.getString("request_amount_after_due_date"));

                                                            if (jsonObj.has("request_distributor_id"))
                                                                message.setDistributorID(jsonObj.getString("request_distributor_id"));

                                                            if (jsonObj.has("request_due_date"))
                                                                message.setDueDate(jsonObj.getString("request_due_date"));


                                                            if (jsonObj.has("category_type"))
                                                                message.setCategoryType(jsonObj.getString("category_type"));


                                                            if (jsonObj.has("distributor_account_number"))
                                                                message.setDistributorAccountNumber(jsonObj.getString("distributor_account_number"));

                                                            if (jsonObj.has("voucher_max_amount"))
                                                                message.setMaxAmount(jsonObj.getString("voucher_max_amount"));

                                                            if (jsonObj.has("currency")) {
                                                                String cur = jsonObj.getString("currency");
                                                                if (!TextUtils.isEmpty(cur) && !cur.equals("null") && !cur.equals(null) && !cur.equals("")) {
                                                                    message.setCurrency(jsonObj.getString("currency"));
                                                                } else
                                                                    message.setCurrency("PKR");

                                                            }
                                                            if (jsonObj.has("voucher_min_amount"))
                                                                message.setMinAmount(jsonObj.getString("voucher_min_amount"));

                                                            if (jsonObj.has("request_timestamp"))
                                                                message.setRequest_timestamp(jsonObj.getString("request_timestamp"));

                                                            if (jsonObj.has("show_account_number"))
                                                                message.setShowAccountNumber(jsonObj.getString("show_account_number"));

                                                            if (jsonObj.has("request_timedelta")) {
                                                                message.setDeltaTime(jsonObj.getString("request_timedelta"));
                                                                String[] messageTime = message.getDeltaTime().split(",");
                                                                if (messageTime[0].toLowerCase().contains("days") || messageTime[0].toLowerCase().contains("day") || messageTime[0].toLowerCase().contains("months") || messageTime[0].toLowerCase().contains("month") || messageTime[0].toLowerCase().contains("years") || messageTime[0].toLowerCase().contains("year")) {

                                                                    message.setRequest_timestamp(messageTime[0] + " " + "ago");

                                                                } else {

                                                                    Date date = new SimpleDateFormat("HH:mm:ss", Locale.US).parse(messageTime[0]);
                                                                    Calendar cal = Calendar.getInstance();
                                                                    cal.setTime(date);
                                                                    int hours = cal.get(Calendar.HOUR_OF_DAY);
                                                                    int mins = cal.get(Calendar.MINUTE);

                                                                    if (mins < 1) {


                                                                        message.setRequest_timestamp("Less than a minute ago");
                                                                    } else if (hours < 1) {
                                                                        message.setRequest_timestamp(mins + " minutes ago");

                                                                    } else {


                                                                        message.setRequest_timestamp(hours + " hours ago");
                                                                    }

//                long timeInMilliseconds = date.getTime();
//                String time = timeAgo.timeAgo(timeInMilliseconds);

                                                                }
                                                            }


                                                            messageList.add(message);


                                                        } catch (Exception ex) {
                                                            ex.printStackTrace();
                                                            dialog.dismiss();
                                                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                                        }


                                                    }


                                                    CustomListAdapter customListAdapter = new CustomListAdapter(messageList, MessageCenter.this);
                                                    recyclerView.setAdapter(customListAdapter);

                                                    dialog.dismiss();

                                                } else {
                                                    dialog.dismiss();
                                                }


                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                                dialog.dismiss();
                                            }
                                        } else if (jsonObject.getJSONObject("message_center_for_userResult").getString("Status_Code").equals("54")) {
                                            dialog.dismiss();
                                            utils.showDilogForLogout(jsonObject.getJSONObject("message_center_for_userResult").getString("Status_Description"), "ERROR", MessageCenter.this, new LoginScreen());


                                        } else {
                                            dialog.dismiss();
                                            utils.showDilogForError(jsonObject.getJSONObject("message_center_for_userResult").getString("Status_Message"), "ERROR");


                                        }
                                    } else {
                                        dialog.dismiss();
                                        utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");

                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("Error.Response", "---" + error.getMessage());
                                try {
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    dialog.dismiss();
                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                }
                            }
                        }
                )

//                {
//                    @Override
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<String, String>();
//                        params.put("sService_Username", constants.signsUserName);
//                        params.put("sService_Password", constants.signsPassword);
//                        params.put("sIPAddress", Utils.getIPAddress(true));
//                        params.put("sMacAddress", constants.address);
//                        params.put("sOS_Version", constants.version);
//                        params.put("sDeviceName", constants.model);
//                      params.put("sAppVersion", constants.appVersion);
//                        params.put("sService_SharedKey", Constants.Shared_KEY);
//                        params.put("sTimeStamp", constants.dateTime);
//                        params.put("sLegalIdentityValue", constants.sharedPreferences.getString("LegalID", "N/A"));
//                        params.put("sServerSessionKey", md5);
//                        params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                        params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                        params.put("sDeviceLongitude", String.valueOf(longitude));
//                        return params;
//                    }
//                };

                {

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return requestBody == null ? null : requestBody.getBytes("utf-8");
                        } catch (UnsupportedEncodingException uee) {
                            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                            return null;
                        }
                    }
                };

                postRequest.setRetryPolicy(new DefaultRetryPolicy(Constants.requestTimeout,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                postRequest.setShouldCache(false);
                requestQueue.add(postRequest);
                requestQueue.getCache().clear();
            } else {
//                Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.!", Toast.LENGTH_LONG).show();
                dialog.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {
            dialog.dismiss();
//            Toast.makeText(LoginWithPassAndPinScreen.this, "Please check your internet connectivity.", Toast.LENGTH_LONG).show();
            utils.showDilogForError("Please check your internet connectivity.", "WARNING");


        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    public String getURLForResource(int resourceId) {
        return Uri.parse("android.resource://" + R.class.getPackage().getName() + "/" + resourceId).toString();
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
}
