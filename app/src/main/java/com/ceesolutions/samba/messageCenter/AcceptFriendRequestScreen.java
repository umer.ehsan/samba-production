package com.ceesolutions.samba.messageCenter;

/**
 * Created by ceeayaz on 4/4/18.
 */

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.ceesolutions.samba.R;
import com.ceesolutions.samba.utils.PopupDialogFriend;

public class AcceptFriendRequestScreen extends AppCompatActivity {

    private PopupDialogFriend popupDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        getSupportActionBar().hide();

        popupDialog = new PopupDialogFriend();
        popupDialog.setCancelable(false);
        popupDialog.show(getSupportFragmentManager(),"dialog");

    }

    @Override
    public void onBackPressed() {
        //Do Nothing
    }
}