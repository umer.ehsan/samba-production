package com.ceesolutions.samba.messageCenter.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.ceesolutions.samba.R;
import com.ceesolutions.samba.accessControl.EnableNotification;
import com.ceesolutions.samba.accessControl.HomeScreen;
import com.ceesolutions.samba.accessControl.LoginScreen;
import com.ceesolutions.samba.beneficiariesManagement.Model.BankModel;
import com.ceesolutions.samba.billPayments.PayBillsScreen;
import com.ceesolutions.samba.friends.ManageFriends;
import com.ceesolutions.samba.messageCenter.AcceptFriendRequestScreen;
import com.ceesolutions.samba.messageCenter.Model.Message;
import com.ceesolutions.samba.transactions.FundTransferScreen;
import com.ceesolutions.samba.utils.AppPreferences;
import com.ceesolutions.samba.utils.ConfigAPI;
import com.ceesolutions.samba.utils.Constants;
import com.ceesolutions.samba.utils.CryptLib;
import com.ceesolutions.samba.utils.Helper;
import com.ceesolutions.samba.utils.HttpsTrustManager;
import com.ceesolutions.samba.utils.TimeAgo;
import com.ceesolutions.samba.utils.Utils;
import com.ceesolutions.samba.views.CircleImageView;
import com.ceesolutions.samba.views.CircularTextView;
import com.ceesolutions.samba.views.RoundImageView;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ceeayaz on 3/6/18.
 */

public class CustomListAdapter extends BaseAdapter {
    public static double latitude;
    public static double longitude;
    public TextView title, timeStamp, description, addFriend, reject;
    public View view1;
    public String longitude1;
    public String latitude1;
    ArrayList<Message> messageList;
    LayoutInflater layoutInflater;
    View row;
    Context context;
    TextView bankName;
    Intent intent;
    private RequestQueue requestQueue;
    private Helper helper;
    private Constants constants;
    private Dialog pd;
    private Utils utils;
    private ImageView background;
    private CircleImageView profile_image;
    private RoundImageView titleImage;
    private TextView titleTxt;
    private int color;
    private TimeAgo timeAgo;
    private AppPreferences appPreferences;
    private String location;

    public CustomListAdapter(ArrayList<Message> messageList2, Context mContext) {
        messageList = messageList2;
        context = mContext;
        requestQueue = Volley.newRequestQueue(context);
        constants = Constants.getConstants(context);
        helper = Helper.getHelper(context);
        utils = new Utils(context);
        pd = new Dialog(context);
        timeAgo = new TimeAgo(context);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_layout);
        pd.setCancelable(false);
        appPreferences = new AppPreferences(context);
        longitude1 = appPreferences.getString("longitude", "N/A");
        latitude1 = appPreferences.getString("latitude", "N/A");
        location = appPreferences.getString("location", "N/A");
        if (!TextUtils.isEmpty(longitude1) && !longitude1.equals("N/A") && !longitude1.equals("0.0") && !longitude1.equals("0") && !TextUtils.isEmpty(latitude1) && !latitude1.equals("N/A") && !latitude1.equals("0.0") && !latitude1.equals("0")) {
            longitude = Double.valueOf(longitude1);
            latitude = Double.valueOf(latitude1);
            if (!TextUtils.isEmpty(location) && !location.equals("null") && !location.equals(null) && !location.equals("N/A")) {


            } else {
                location = "-";

//                utils.showDilogForError("Please turn on location services.", "WARNING");
//                validateBtn.setEnabled(true);
            }
        } else {

            longitude = 0;
            latitude = 0;
            location = "-";

//            utils.showDilogForError("Please turn on location services.", "WARNING");
//            validateBtn.setEnabled(true);
        }
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return messageList.size();
    }

    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.list_item, parent, false);
        }
        try {

//            String[] messageTime = messageList.get(position).getDeltaTime().split(",");


//            Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US).parse(messageList.get(position).getRequest_timestamp());
//            long timeInMilliseconds = date.getTime();
//            String time = timeAgo.timeAgo(timeInMilliseconds);
            timeStamp = (TextView) convertView.findViewById(R.id.timeStamp);
            background = (ImageView) convertView.findViewById(R.id.background);
            profile_image = (CircleImageView) convertView.findViewById(R.id.profile_image);
            pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            title = (TextView) convertView.findViewById(R.id.title);
            description = (TextView) convertView.findViewById(R.id.description);
            addFriend = (TextView) convertView.findViewById(R.id.addFriend);
            reject = (TextView) convertView.findViewById(R.id.reject);
            view1 = (View) convertView.findViewById(R.id.view);
            titleTxt = (TextView) convertView.findViewById(R.id.titleTxt);
            titleImage = (RoundImageView) convertView.findViewById(R.id.titleImage);
            titleTxt.bringToFront();
            titleTxt.setVisibility(View.INVISIBLE);
            titleImage.setVisibility(View.INVISIBLE);
            title.setText(messageList.get(position).getTitle());
            timeStamp.setText(messageList.get(position).getRequest_timestamp());
//            if(messageTime.length == 1){
//
//                Date date = new SimpleDateFormat("hh:mm:ss", Locale.US).parse(messageList.get(position).getDeltaTime());
//                long timeInMilliseconds = date.getTime();
//                String time = timeAgo.timeAgo(timeInMilliseconds);
//                timeStamp.setText(time);
//            }else{
//
//
//            }


            description.setText(messageList.get(position).getDescription());

            if (messageList.get(position).getMessageType().equals("information")) {

                addFriend.setVisibility(View.GONE);
                reject.setVisibility(View.GONE);
                view1.setBackgroundColor(context.getResources().getColor(R.color.white));
                convertView.setEnabled(false);
            } else {

                convertView.setEnabled(true);
                addFriend.setVisibility(View.VISIBLE);
                reject.setVisibility(View.VISIBLE);
                view1.setBackgroundColor(Color.parseColor("#1e000000"));
            }

//            if (messageList.get(position).getTargetFormId().equals("160")) {
//
//
////            profile_image.setBackgroundResource();
//                Glide.with(context)
//                        .load(R.drawable.transfer_request)
//                        .into(profile_image);
////                profile_image.setImageResource(R.drawable.transfer_request);
//
//
//            } else if (messageList.get(position).getTargetFormId().equals("250") || messageList.get(position).getTargetFormId().equals("260")) {
//
//                Glide.with(context)
//                        .load(R.drawable.bill_payment)
//                        .into(profile_image);
////                profile_image.setImageResource(R.drawable.bill_payment);
//            } else if (messageList.get(position).getTargetFormId().equals("1001") || messageList.get(position).getTargetFormId().equals("335")) {

            String s = messageList.get(position).getFriendName().substring(0, 1).toUpperCase();

            switch (s.toUpperCase()) {

                case "A":
                    color = context.getResources().getColor(R.color.friendColor1);
                    break;

                case "B":
                    color = context.getResources().getColor(R.color.friendColor1);
                    break;

                case "C":
                    color = context.getResources().getColor(R.color.friendColor1);
                    break;

                case "D":
                    color = context.getResources().getColor(R.color.friendColor1);
                    break;

                case "E":
                    color = context.getResources().getColor(R.color.friendColor1);
                    break;

                case "F":
                    color = context.getResources().getColor(R.color.friendColor1);
                    break;

                case "G":
                    color = context.getResources().getColor(R.color.friendColor1);
                    break;

                case "H":
                    color = context.getResources().getColor(R.color.friendColor2);
                    break;

                case "I":
                    color = context.getResources().getColor(R.color.friendColor2);
                    break;

                case "J":
                    color = context.getResources().getColor(R.color.friendColor2);
                    break;

                case "K":
                    color = context.getResources().getColor(R.color.friendColor2);
                    break;

                case "L":
                    color = context.getResources().getColor(R.color.friendColor2);
                    break;

                case "M":
                    color = context.getResources().getColor(R.color.friendColor2);
                    break;

                case "N":
                    color = context.getResources().getColor(R.color.friendColor3);
                    break;

                case "O":
                    color = context.getResources().getColor(R.color.friendColor3);
                    break;

                case "P":
                    color = context.getResources().getColor(R.color.friendColor3);
                    break;

                case "Q":
                    color = context.getResources().getColor(R.color.friendColor3);
                    break;

                case "R":
                    color = context.getResources().getColor(R.color.friendColor3);
                    break;

                case "S":
                    color = context.getResources().getColor(R.color.friendColor3);
                    break;

                case "T":
                    color = context.getResources().getColor(R.color.friendColor4);
                    break;

                case "U":
                    color = context.getResources().getColor(R.color.friendColor4);
                    break;

                case "V":
                    color = context.getResources().getColor(R.color.friendColor4);
                    break;

                case "W":
                    color = context.getResources().getColor(R.color.friendColor4);
                    break;

                case "X":
                    color = context.getResources().getColor(R.color.friendColor4);
                    break;

                case "Y":
                    color = context.getResources().getColor(R.color.friendColor4);
                    break;

                case "Z":
                    color = context.getResources().getColor(R.color.friendColor4);
                    break;

                default:
                    color = context.getResources().getColor(R.color.defaultColor);
                    break;
            }

            if (!TextUtils.isEmpty(messageList.get(position).getImage())) {
                RequestOptions options = new RequestOptions();
                options.centerCrop();
                Glide.with(context)
                        .load(messageList.get(position).getImage())
                        .apply(options)
                        .into(profile_image);
                profile_image.setVisibility(View.VISIBLE);
            } else if (!TextUtils.isEmpty(messageList.get(position).getFriendImage())) {
                byte[] imageByteArray = Base64.decode(messageList.get(position).getFriendImage(), Base64.DEFAULT);
                RequestOptions options = new RequestOptions();
                options.centerCrop();
                Glide.with(context)
                        .load(imageByteArray)
                        .apply(options)
                        .into(profile_image);
                profile_image.setVisibility(View.VISIBLE);
            } else {
                titleTxt.setVisibility(View.VISIBLE);
                titleImage.setVisibility(View.VISIBLE);

                titleImage.setBackgroundTintList(ColorStateList.valueOf((color)));
                titleTxt.setText(s);
            }

//            } else {
//
//                Glide.with(context)
//                        .load(R.drawable.notification_center)
//                        .into(profile_image);
////                profile_image.setImageResource(R.drawable.notification_center);
//            }


            final View finalConvertView = convertView;

            reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    FirebaseAnalytics.getInstance(context).logEvent("Message_Center_Reject_Pressed", new Bundle());

                    if (messageList.get(position).getTargetFormId().equals("335")) {

                        FriendRequest(position);
                    } else {
                        RejectRequest(position);
                    }
                }
            });

            addFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        FirebaseAnalytics.getInstance(context).logEvent("Message_Center_Accept_Pressed", new Bundle());

                        CryptLib _crypt = new CryptLib();
//                finalConvertView.setEnabled(false);
                        if (messageList.get(position).getMessageType().equals("information")) {

//                    finalConvertView.setEnabled(true);

                        } else {
                            if (messageList.get(position).getTargetFormId().equals("335")) {

                                Intent intent = new Intent(((Activity) context), AcceptFriendRequestScreen.class);
                                intent.putExtra("userName", messageList.get(position).getUserName());
                                intent.putExtra("friendName", messageList.get(position).getFriendName());
                                intent.putExtra("friendNumber", messageList.get(position).getFriendNumber());
                                intent.putExtra("userId", messageList.get(position).getUserId());
                                intent.putExtra("userNumber", messageList.get(position).getUserNumber());
                                intent.putExtra("messageId", messageList.get(position).getMessageId());
                                intent.putExtra("targetFormId", messageList.get(position).getTargetFormId());
                                intent.putExtra("request_id", messageList.get(position).getRequestID());
                                intent.putExtra("friend_image", messageList.get(position).getFriendImage());
                                intent.putExtra("friend_username", messageList.get(position).getFriendUserName());
                                ((Activity) context).overridePendingTransition(0, 0);
                                context.startActivity(intent);
//                        ((Activity) context).finish();
//                        ((Activity) context).finishAffinity();
//                        finalConvertView.setEnabled(true);

                            } else if (messageList.get(position).getTargetFormId().equals("160")) {

                                Intent intent = new Intent(((Activity) context), FundTransferScreen.class);
                                intent.putExtra("userName", messageList.get(position).getUserName());
                                intent.putExtra("friendName", messageList.get(position).getFriendName());
                                intent.putExtra("friendNumber", messageList.get(position).getFriendNumber());
                                intent.putExtra("userId", messageList.get(position).getUserId());
                                intent.putExtra("userNumber", messageList.get(position).getUserNumber());
                                intent.putExtra("messageId", messageList.get(position).getMessageId());
                                intent.putExtra("remarks", messageList.get(position).getRemarks());
                                intent.putExtra("purpose", messageList.get(position).getPurpose());
                                intent.putExtra("amount", messageList.get(position).getAmount());
                                intent.putExtra("targetFormId", messageList.get(position).getTargetFormId());
                                intent.putExtra("accountNumber", messageList.get(position).getAccountNumber());
                                intent.putExtra("requestFunds", "requestMoney");
                                intent.putExtra("accountTitle", messageList.get(position).getAccountTitle());
                                intent.putExtra("bankCode", messageList.get(position).getBankCode());
                                intent.putExtra("friend_id", messageList.get(position).getFriendID());
                                intent.putExtra("show", messageList.get(position).getShowAccountNumber());
                                intent.putExtra("currency", messageList.get(position).getCurrency());

                                ((Activity) context).overridePendingTransition(0, 0);
                                context.startActivity(intent);
//                        ((Activity) context).finish();
//                        ((Activity) context).finishAffinity();
//                        finalConvertView.setEnabled(true);

                            } else if (messageList.get(position).getTargetFormId().equals("250")) {

//                        Intent intent = new Intent(((Activity) context), PayBillsScreen.class);
//                        intent.putExtra("userName", messageList.get(position).getUserName());
//                        intent.putExtra("friendName", messageList.get(position).getFriendName());
//                        intent.putExtra("friendNumber", messageList.get(position).getFriendNumber());
//                        intent.putExtra("userId", messageList.get(position).getUserId());
//                        intent.putExtra("userNumber", messageList.get(position).getUserNumber());
//                        intent.putExtra("messageId", messageList.get(position).getMessageId());
//                        intent.putExtra("remarks", messageList.get(position).getRemarks());
//                        intent.putExtra("purpose", messageList.get(position).getPurpose());
//                        intent.putExtra("amount", messageList.get(position).getAmount());
//                        intent.putExtra("accountNumber", messageList.get(position).getAccountNumber());
//                        intent.putExtra("requestFunds", "requestMoney");
//                        intent.putExtra("accountTitle", messageList.get(position).getAccountTitle());
//                        intent.putExtra("bankCode", messageList.get(position).getBankCode());
//                        intent.putExtra("billingStatus", messageList.get(position).getFriendID());
//                        intent.putExtra("billMonth", messageList.get(position).getFriendID());
//                        intent.putExtra("dueDate", messageList.get(position).getFriendID());
//                        intent.putExtra("friend_id", messageList.get(position).getFriendID());
//                        intent.putExtra("friend_id", messageList.get(position).getFriendID());
//                        intent.putExtra("friend_id", messageList.get(position).getFriendID());
//                        intent.putExtra("friend_id", messageList.get(position).getFriendID());
//
//
//                        ((Activity) context).overridePendingTransition(0, 0);
//                        context.startActivity(intent);
//                        ((Activity) context).finish();

                                BillInquiry(position);
//                        finalConvertView.setEnabled(true);

                            } else if (messageList.get(position).getTargetFormId().equals("260")) {

//                       Intent intent = new Intent(context, PayBillsScreen.class);
                                Intent intent = new Intent(context, PayBillsScreen.class);
                                constants.destinationAccountEditor.putString("consumerNumber", _crypt.encryptForParams(messageList.get(position).getConusmerNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                constants.destinationAccountEditor.putString("consumerName", _crypt.encryptForParams(messageList.get(position).getConsumerName(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                constants.destinationAccountEditor.putString("distributorID", _crypt.encryptForParams(messageList.get(position).getDistributorID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                constants.destinationAccountEditor.putString("distributorAccountNumber", _crypt.encryptForParams(messageList.get(position).getDistributorAccountNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                constants.destinationAccountEditor.putString("alias", _crypt.encryptForParams(messageList.get(position).getBillAlias(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                constants.destinationAccountEditor.putString("amount", _crypt.encryptForParams(messageList.get(position).getAmount(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                constants.destinationAccountEditor.putString("remarks", _crypt.encryptForParams(messageList.get(position).getRemarks(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                constants.destinationAccountEditor.putString("purpose", _crypt.encryptForParams(messageList.get(position).getPurpose(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                constants.destinationAccountEditor.putString("maxAmount", _crypt.encryptForParams(messageList.get(position).getMaxAmount(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                constants.destinationAccountEditor.putString("minAmount", _crypt.encryptForParams(messageList.get(position).getMinAmount(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                constants.destinationAccountEditor.putString("targetFormId", _crypt.encryptForParams(messageList.get(position).getTargetFormId(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                constants.destinationAccountEditor.putString("messageId", _crypt.encryptForParams(messageList.get(position).getMessageId(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                constants.destinationAccountEditor.putString("friendNumber", _crypt.encryptForParams(messageList.get(position).getFriendNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                constants.destinationAccountEditor.commit();
                                constants.destinationAccountEditor.apply();
                                pd.dismiss();
                                ((Activity) context).overridePendingTransition(0, 0);
                                intent.putExtra("requestbill", "requestvoucher");
                                ((Activity) context).startActivity(intent);
//                        ((Activity) context).finish();
//                        ((Activity) context).finishAffinity();

//                        BillInquiry(position);
//                        finalConvertView.setEnabled(true);

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }


    public void BillInquiry(final int position) {

        final int stanNumber = 6;

        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sBillConsumerNumber", messageList.get(position).getConusmerNumber());
                    params.put("sDistributorID", messageList.get(position).getDistributorID());
                    params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);


                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());


                    HttpsTrustManager.allowMySSL(context);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.billInquiryURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            try {
                                                CryptLib _crypt = new CryptLib();

                                                if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("00")) {


                                                    Intent intent = new Intent(context, PayBillsScreen.class);
                                                    constants.destinationAccountEditor.putString("AmountAfterDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountAfterDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("AmountWithinDueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("AmountWithinDueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillStatus", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillStatus"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillingMonth", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("BillingMonth"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("CompanyID", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyID"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("DueDate", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("DueDate"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("ConsumerName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("ConsumerName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("CompanyName", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("CompanyName"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("RRN", _crypt.encryptForParams(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("RRN"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("BillConsumerNumber", _crypt.encryptForParams(messageList.get(position).getConusmerNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("DistributorID", _crypt.encryptForParams(messageList.get(position).getDistributorID(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("distributorAccountNumber", _crypt.encryptForParams(messageList.get(position).getDistributorAccountNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("alias", _crypt.encryptForParams(messageList.get(position).getBillAlias(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("targetFormId", _crypt.encryptForParams(messageList.get(position).getTargetFormId(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("categoryType", _crypt.encryptForParams(messageList.get(position).getCategoryType(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("amount", _crypt.encryptForParams(messageList.get(position).getAmount(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("remarks", _crypt.encryptForParams(messageList.get(position).getRemarks(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("purpose", _crypt.encryptForParams(messageList.get(position).getPurpose(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("messageId", _crypt.encryptForParams(messageList.get(position).getMessageId(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("targetId", _crypt.encryptForParams(messageList.get(position).getTargetFormId(), constants.getDeviceMac(), constants.getDeviceIMEI()));
                                                    constants.destinationAccountEditor.putString("friendNumber", _crypt.encryptForParams(messageList.get(position).getFriendNumber(), constants.getDeviceMac(), constants.getDeviceIMEI()));

                                                    constants.destinationAccountEditor.commit();
                                                    constants.destinationAccountEditor.apply();
                                                    pd.dismiss();
                                                    ((Activity) context).overridePendingTransition(0, 0);
                                                    intent.putExtra("requestbill", "requestbill");
                                                    ((Activity) context).startActivity(intent);
//                                            ((Activity) context).finish();
//                                            ((Activity) context).finishAffinity();


                                                } else if (jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Code").equals("54")) {
                                                    pd.dismiss();
                                                    utils.showDilogForLogout(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR", context, new LoginScreen());


                                                } else {

                                                    pd.dismiss();
                                                    utils.showDilogForError(jsonObject.getJSONObject("Signs_BillInquiryResult").getString("Status_Description"), "ERROR");


                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");

                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sBillConsumerNumber", messageList.get(position).getConusmerNumber());
//                            params.put("sDistributorID", messageList.get(position).getDistributorID());
//                            params.put("sSTAN", String.valueOf(helper.nDigitRandomNo(stanNumber)));
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                    postRequest.setRetryPolicy(new RetryPolicy() {
                        @Override
                        public int getCurrentTimeout() {
                            return 50000;
                        }

                        @Override
                        public int getCurrentRetryCount() {
                            return 50000;
                        }

                        @Override
                        public void retry(VolleyError error) throws VolleyError {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }


    public void RejectRequest(final int position) {


        try {
            pd.show();
            if (helper.isNetworkAvailable()) {

                try {
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sSigns_Username", user);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sUser_name", user);
                    params.put("sUser_id", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sUser_number", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sAction", "0");
                    params.put("sFriend_number", messageList.get(position).getFriendNumber());
                    params.put("sFriend_username", messageList.get(position).getFriendUserName());
                    params.put("sMessage_id", messageList.get(position).getMessageId());
                    params.put("sTarget_form_id", messageList.get(position).getTargetFormId());
                    params.put("sAmount", messageList.get(position).getAmount());
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);


                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());


                    HttpsTrustManager.allowMySSL(context);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.rejectReqURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("action_message_requestResult").getString("Status_Code").equals("00")) {

                                                messageList.remove(position);
                                                notifyDataSetChanged();
                                                pd.dismiss();


                                            } else if (jsonObject.getJSONObject("action_message_requestResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("action_message_requestResult").getString("Status_Description"), "ERROR", context, new LoginScreen());


                                            } else {

                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("action_message_requestResult").getString("Status_Description"), "ERROR");


                                            }
                                        } else {

                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sUser_name", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sUser_id", constants.sharedPreferences.getString("LegalID", "N/A"));
//                            params.put("sUser_number", constants.sharedPreferences.getString("MobileNumber", "N/A"));
//                            params.put("sAction", "0");
//                            params.put("sFriend_number", messageList.get(position).getFriendNumber());
//                            params.put("sMessage_id", messageList.get(position).getMessageId());
//                            params.put("sTarget_form_id", messageList.get(position).getTargetFormId());
//                            params.put("sAmount", messageList.get(position).getAmount());
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };


                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                    postRequest.setRetryPolicy(new RetryPolicy() {
                        @Override
                        public int getCurrentTimeout() {
                            return 50000;
                        }

                        @Override
                        public int getCurrentRetryCount() {
                            return 50000;
                        }

                        @Override
                        public void retry(VolleyError error) throws VolleyError {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


                }
            } else {
                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {


            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");


        }
    }

    public void FriendRequest(final int position) {

//        Toast.makeText(RegistrationScreen.this, "Please wait while your request is processing....", Toast.LENGTH_LONG).show();

        try {

            if (helper.isNetworkAvailable()) {

                pd.show();
                try {
//                webService.ActionFriendRequest(constants.sharedPreferences.getString("LegalID", "N/A"), intent.getStringExtra("userName"), constants.sharedPreferences.getString("MobileNumber", "N/A"), intent.getStringExtra("friendNumber"), intent.getStringExtra("messageId"), flag, intent.getStringExtra("request_id"), constants.version, constants.model, constants.dateTime, constants.address, requestQueue);
//
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    public void run() {
//
//                        JSONObject jsonObject = webService.getActionFriend();
//                        Log.d("Response", "Object----" + jsonObject);
//
//                        if (jsonObject != null) {
//
//                            if (jsonObject.has("Error")) {
//
//                                pd.dismiss();
//                                utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//                            } else {
//
//                                try {
//
//
//                                    if (jsonObject.get("Status_Code").equals("00")) {
//
//                                        pd.dismiss();
//
//
//                                    } else {
//                                        pd.dismiss();
//                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//
//
//
//                                    }
//
//
//                                } catch (JSONException ex) {
//
//                                    ex.printStackTrace();
//                                    pd.dismiss();
//                                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                                }
//                            }
//                        } else {
//
//
//                            pd.dismiss();
//                            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
//                        }
//                    }
//                }, 5000);
                    CryptLib _crypt = new CryptLib();
                    String user = _crypt.decrypt(constants.sharedPreferences.getString("userName", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI());
                    final String md5 = helper.md5(user.toUpperCase() + constants.address.toUpperCase() + constants.CONSTANST + constants.dateTime);
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("sService_Username", constants.signsUserName);
                    params.put("sService_Password", constants.signsPassword);
                    params.put("sIPAddress", Utils.getIPAddress(true));
                    params.put("sMacAddress", constants.address);
                    params.put("sOS_Version", constants.version);
                    params.put("sDeviceName", constants.model);
                    params.put("sAppVersion", constants.appVersion);
                    params.put("sSigns_Username", user);
                    params.put("sService_SharedKey", Constants.Shared_KEY);
                    params.put("sTimeStamp", constants.dateTime);
                    params.put("sUser_id", _crypt.decrypt(constants.sharedPreferences.getString("LegalID", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sUser_number", _crypt.decrypt(constants.sharedPreferences.getString("MobileNumber", "N/A"), constants.getDeviceMac(), constants.getDeviceIMEI()));
                    params.put("sUser_name", messageList.get(position).getUserName());
                    params.put("sFriend_number", messageList.get(position).getFriendNumber());
                    params.put("sFriend_username", messageList.get(position).getFriendUserName());
                    params.put("sRequest_id", messageList.get(position).getRequestID());
                    params.put("sMessage_id", messageList.get(position).getMessageId());
                    params.put("sAction", "0");
                    params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
                    params.put("sDeviceLongitude", String.valueOf(longitude));
                    params.put("sServerSessionKey", md5);


                    final String requestBody = "sData=" + _crypt.encrypt(helper.convertMapToString(params), constants.getDeviceMac(), constants.getDeviceIMEI());

                    HttpsTrustManager.allowMySSL(context);
                    StringRequest postRequest = new StringRequest(Request.Method.POST, ConfigAPI.actionFriendRequestURL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    // response

                                    Log.d("Response---", response);

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        JSONObject JsonObj = new JSONObject();
                                        if (jsonObject.getString("ServerKey").equals(md5)) {
                                            if (jsonObject.getJSONObject("action_friend_requestResult").getString("Status_Code").equals("00")) {

                                                messageList.remove(position);
                                                notifyDataSetChanged();
                                                pd.dismiss();

                                            } else if (jsonObject.getJSONObject("action_friend_requestResult").getString("Status_Code").equals("54")) {
                                                pd.dismiss();
                                                utils.showDilogForLogout(jsonObject.getJSONObject("action_friend_requestResult").getString("Status_Description"), "ERROR", context, new LoginScreen());


                                            } else {
                                                pd.dismiss();
                                                utils.showDilogForError(jsonObject.getJSONObject("action_friend_requestResult").getString("Status_Description"), "ERROR");
                                            }
                                        } else {
                                            pd.dismiss();
                                            utils.showDilogForInvalidSession(constants.Invalid_Message, "ERROR");
                                        }


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response", "---" + error.getMessage());
                                    try {
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        pd.dismiss();
                                        utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                                    }
                                }
                            }
                    )

//                    {
//                        @Override
//                        protected Map<String, String> getParams() {
//                            Map<String, String> params = new HashMap<String, String>();
//                            params.put("sService_Username", constants.signsUserName);
//                            params.put("sService_Password", constants.signsPassword);
//                            params.put("sIPAddress", Utils.getIPAddress(true));
//                            params.put("sMacAddress", constants.address);
//                            params.put("sOS_Version", constants.version);
//                            params.put("sDeviceName", constants.model);
//                          params.put("sAppVersion", constants.appVersion);
//                            params.put("sSigns_Username", constants.sharedPreferences.getString("userName", "N/A"));
//                            params.put("sService_SharedKey", Constants.Shared_KEY);
//                            params.put("sTimeStamp", constants.dateTime);
//                            params.put("sUser_id", constants.sharedPreferences.getString("LegalID", "N/A"));
//                            params.put("sUser_number", constants.sharedPreferences.getString("MobileNumber", "N/A"));
//                            params.put("sUser_name", messageList.get(position).getUserName());
//                            params.put("sFriend_number", messageList.get(position).getFriendNumber());
//                            params.put("sRequest_id", messageList.get(position).getRequestID());
//                            params.put("sMessage_id", messageList.get(position).getMessageId());
//                            params.put("sAction", "0");
//                            params.put("sDeviceLatitude", String.valueOf(latitude) + "|" + location);
//                            params.put("sDeviceLongitude", String.valueOf(longitude));
//                            params.put("sServerSessionKey", md5);
//                            return params;
//                        }
//                    };

                    {

                        @Override
                        public byte[] getBody() throws AuthFailureError {
                            try {
                                return requestBody == null ? null : requestBody.getBytes("utf-8");
                            } catch (UnsupportedEncodingException uee) {
                                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                                return null;
                            }
                        }
                    };

                    postRequest.setShouldCache(false);
                    requestQueue.add(postRequest);
                    requestQueue.getCache().clear();
                    postRequest.setRetryPolicy(new RetryPolicy() {
                        @Override
                        public int getCurrentTimeout() {
                            return 50000;
                        }

                        @Override
                        public int getCurrentRetryCount() {
                            return 50000;
                        }

                        @Override
                        public void retry(VolleyError error) throws VolleyError {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                    utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");
                }
            } else {
//                Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_LONG).show();

                pd.dismiss();
                utils.showDilogForError("Please check your internet connectivity.", "WARNING");


            }
        } catch (Exception e) {

//            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_LONG).show();

            pd.dismiss();
            utils.showDilogForError("Service is currently unavailable. Please try again later.", "ERROR");

        }
    }


}